/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    07/15/05    Initial Revision.
*/


// EmailFlags.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include ".\emailflags.h"


// CEmailFlags dialog

IMPLEMENT_DYNAMIC(CEmailFlags, CDialog)
CEmailFlags::CEmailFlags(CString email_address, CWnd* pParent /*=NULL*/)
	: CDialog(CEmailFlags::IDD, pParent)
{
    edit_email_address = email_address;
}

CEmailFlags::~CEmailFlags()
{
}

void CEmailFlags::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EMAIL_ALERT_STRATUS_LINK_UP_CHECK, m_stratus_link_up);
	DDX_Control(pDX, IDC_EMAIL_ALERT_STRATUS_LINK_DOWN_CHECK, m_stratus_link_down);
	DDX_Control(pDX, IDC_EMAIL_ALERT_PORT_COMM_PROBLEM_CHECK, m_port_comm_problem);
	DDX_Control(pDX, IDC_EMAIL_ALERT_PORT_NO_RESPONSE_CHECK, m_port_no_response);
	DDX_Control(pDX, IDC_EMAIL_ALERT_PROGRESSIVE_LINK_DOWN_CHECK, m_progressive_link_down);
	DDX_Control(pDX, IDC_EMAIL_ALERT_MILLION_DOLLAR_TICKET_DRAW_CHECK, m_million_dollar_ticket_draw);
	DDX_Control(pDX, IDC_EMAIL_ALERT_PLAYER_CARD_STUCK_SENSOR_CHECK, m_player_card_stuck_sensor);
	DDX_Control(pDX, IDC_EMAIL_ALERT_PLAYER_CARD_DEAD_READER_CHECK, m_player_card_dead_reader);
	DDX_Control(pDX, IDC_EMAIL_ALERT_PLAYER_CARD_ABANDONED_CARD_CHECK, m_player_card_abandoned);
}


BEGIN_MESSAGE_MAP(CEmailFlags, CDialog)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_EMAIL_FLAGS_EXIT_BUTTON, OnBnClickedEmailFlagsExitButton)
	ON_BN_CLICKED(IDC_EMAIL_FLAGS_UPDATE_BUTTON, OnBnClickedEmailFlagsUpdateButton)
END_MESSAGE_MAP()


// CEmailFlags message handlers

void CEmailFlags::OnMouseMove(UINT nFlags, CPoint point)
{
	theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

BOOL CEmailFlags::OnInitDialog()
{
 DWORD file_index;
 FileEmailRecord file_email_record;

	CDialog::OnInitDialog();

    SetDlgItemText(IDC_EMAIL_FLAGS_ADDRESS_STATIC, edit_email_address);

    file_index = 0;
    while (fileRead(EMAIL_ADDRESS_LIST, file_index, &file_email_record, sizeof(file_email_record)))
    {
        if (edit_email_address == file_email_record.email_address)
        {
            if (file_email_record.email_flags & STRATUS_LINK_DOWN)
                m_stratus_link_down.SetCheck(BST_CHECKED);

            if (file_email_record.email_flags & STRATUS_LINK_UP)
                m_stratus_link_up.SetCheck(BST_CHECKED);

            if (file_email_record.email_flags & SERIAL_PORT_COMM_PROBLEM)
                m_port_comm_problem.SetCheck(BST_CHECKED);

            if (file_email_record.email_flags & SERIAL_PORT_NO_RESPONSE)
                m_port_no_response.SetCheck(BST_CHECKED);

            if (file_email_record.email_flags & PROGRESSIVE_LINK_DOWN)
                m_progressive_link_down.SetCheck(BST_CHECKED);

            if (file_email_record.email_flags & PLAYER_CARD_STUCK_SENSOR)
                m_player_card_stuck_sensor.SetCheck(BST_CHECKED);

            if (file_email_record.email_flags & PLAYER_CARD_DEAD_READER)
                m_player_card_dead_reader.SetCheck(BST_CHECKED);

            if (file_email_record.email_flags & PLAYER_CARD_ABANDONED_CARD)
                m_player_card_abandoned.SetCheck(BST_CHECKED);

            if (file_email_record.email_flags & MDT_EMAIL_DRAW)
                m_million_dollar_ticket_draw.SetCheck(BST_CHECKED);

            break;
        }
        else
            file_index++;
    }

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CEmailFlags::OnBnClickedEmailFlagsExitButton()
{
	DestroyWindow();
}

void CEmailFlags::OnBnClickedEmailFlagsUpdateButton()
{
 DWORD file_index = 0;
 FileEmailRecord file_email_record;

    while (fileRead(EMAIL_ADDRESS_LIST, file_index, &file_email_record, sizeof(file_email_record)))
    {
        if (edit_email_address == file_email_record.email_address)
        {
            file_email_record.email_flags = 0;

            if (m_stratus_link_down.GetCheck() == BST_CHECKED)
                file_email_record.email_flags |= STRATUS_LINK_DOWN;

            if (m_stratus_link_up.GetCheck() == BST_CHECKED)
                file_email_record.email_flags |= STRATUS_LINK_UP;

            if (m_port_comm_problem.GetCheck() == BST_CHECKED)
                file_email_record.email_flags |= SERIAL_PORT_COMM_PROBLEM;

            if (m_port_no_response.GetCheck() == BST_CHECKED)
                file_email_record.email_flags |= SERIAL_PORT_NO_RESPONSE;

            if (m_progressive_link_down.GetCheck() == BST_CHECKED)
                file_email_record.email_flags |= PROGRESSIVE_LINK_DOWN;

            if (m_million_dollar_ticket_draw.GetCheck() == BST_CHECKED)
                file_email_record.email_flags |= MDT_EMAIL_DRAW;

            if (m_player_card_stuck_sensor.GetCheck() == BST_CHECKED)
                file_email_record.email_flags |= PLAYER_CARD_STUCK_SENSOR;

            if (m_player_card_dead_reader.GetCheck() == BST_CHECKED)
                file_email_record.email_flags |= PLAYER_CARD_DEAD_READER;

            if (m_player_card_abandoned.GetCheck() == BST_CHECKED)
                file_email_record.email_flags |= PLAYER_CARD_ABANDONED_CARD;

            if (fileWrite(EMAIL_ADDRESS_LIST, file_index, &file_email_record, sizeof(file_email_record)))
            {
                MessageWindow(false, "EMAIl ALERT FLAGS", "Email Record Updated");
                DestroyWindow();
                return;
            }
            else
                break;
        }
        else
            file_index++;
    }

    MessageWindow(false, "EMAIl ALERT FLAGS", "Error Updating Email Record\nTry Again");
}

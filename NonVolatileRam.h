/*
Robert Fuller   Jun 06, 2011    Added mods to support GB with the XP/SAS direct connect feature.
Robert Fuller	Mar 22, 2004	Added denomination meters.
Robert Fuller   Mar 22, 2004    Added the games_played_since_logout flag.
Robert Fuller	Apr 13, 2004	Added global timer constant definitions.
Robert Fuller	Sep 09, 2004	Started the first timer id at WM_APP.
Robert Fuller	Feb 10, 2005	Added system jackpot to credit enable flag.
Robert Fuller	Feb 10, 2005	Added system autopay enable flag.
Robert Fuller   Feb 21, 2005    Added code to support displaying the GB Pro messages.
Robert Fuller   Mar 21, 2005    Added code for the game delay check timer.
Robert Fuller   Jul 14, 2005    Added eMarker related variables.
Robert Fuller   Sep 20, 2005	Added constants and some emarker flags and meter variables.
Robert Fuller   Dec 09, 2005	Added emarker enabled flag.
Robert Fuller	Jan 26, 2006	Added sas_six_or_greater flag.
Robert Fuller   Apr 25, 2006    Added variable to store machine manufacturer id.
Robert Fuller   Oct 03, 2006    Added flag to keep track of meter messages from game after downloads.
Robert Fuller   Sep 29, 2006    Added Cash Frenzy support.
Robert Fuller   May 23, 2007    Added constants to support non-MarkerTrax functionality.
Robert Fuller   Jul 05, 2007    Added functions and mods to support Timbers club code 4.
Robert Fuller   Feb 26, 2008    Moved the current bank meter variable to nonvolatile ram.
Robert Fuller	Feb 16, 2009    Added small delay between the remote handpay reset and the send handpay info message.
Robert Fuller   Apr 07, 2009    Changed the name of the new CardDataBlockInfo.
Robert Fuller	Apr 05, 2010	Added code necessary to abort a marker advance if the game never receives the advance message from the system.

*/
#ifndef NonVolatileRam_H
#define NonVolatileRam_H

//#include "com.h"
//#include "GB_SAS_InterfaceDlg.h"
#include "utility.h"

#define TAXABLE_LIMIT               120000

#define MAX_CUSTOMER_NAME	16
#define MAX_STORED_CARD_DATA_BLOCKS     5

#define NUMBER_OF_DENOMINATIONS			25
#define NUMBER_OF_DENOM_METERS			4

#define MAX_EVENT_WAIT_TIMOUT			4000

#define TEN_SECONDS						10000
#define THIRTY_SECONDS					30000
#define ONE_MINUTE						60000
#define TWO_MINUTES						120000

#define GB_COMM_TIMER_ID 				WM_APP
#define ONE_SECOND_TIMER_ID				GB_COMM_TIMER_ID + 1
#define REDEEM_REQUEST_TIMER			ONE_SECOND_TIMER_ID + 1
#define REDEEM_COMPLETE_TIMER			REDEEM_REQUEST_TIMER + 1
#define MARKER_COMPLETE_TIMER			REDEEM_COMPLETE_TIMER + 1
#define MEMBER_ID_TIMOUT_TIMER			MARKER_COMPLETE_TIMER + 1
#define CLEAR_CARD_TIMOUT_TIMER			MEMBER_ID_TIMOUT_TIMER + 1
#define REDEEM_TIMOUT_TIMER				CLEAR_CARD_TIMOUT_TIMER + 1
#define MARKER_TIMOUT_TIMER				REDEEM_TIMOUT_TIMER + 1
#define VIEW_CARDS_TIMOUT_TIMER			MARKER_TIMOUT_TIMER + 1
#define GB_OPTIONS_TIMOUT_TIMER			VIEW_CARDS_TIMOUT_TIMER + 1
#define GB_OPTIONS_PIN_TIMOUT_TIMER		GB_OPTIONS_TIMOUT_TIMER + 1
#define PIN_TIMOUT_TIMER				GB_OPTIONS_PIN_TIMOUT_TIMER + 1
#define ZERO_CREDITS_LOGOFF_TIMER		PIN_TIMOUT_TIMER + 1
#define ENROLLMENT_TIMOUT_TIMER			ZERO_CREDITS_LOGOFF_TIMER + 1
#define CLEAR_GB_PRO_STRINGS_TIMER		ENROLLMENT_TIMOUT_TIMER + 1
#define GAME_DELAY_CHECK_TIMER			CLEAR_GB_PRO_STRINGS_TIMER + 1
#define CLEAR_EMARKER_STRINGS_TIMER		GAME_DELAY_CHECK_TIMER + 1
#define SEND_HANDPAY_INFO_DELAY_TIMER   CLEAR_EMARKER_STRINGS_TIMER + 1
#define MARKER_ABORT_COMPLETE_TIMER		SEND_HANDPAY_INFO_DELAY_TIMER + 1

typedef struct 
{
   unsigned long  handle_to_date;	/* holds handle-to-date accrued toward bingo card */
   unsigned long  games_to_date;	/* holds games-to-date played on bingo card */
   unsigned long  card;             /* holds players bingo card */
   char  type;                      /* type of 4ok hit. i.e. two's, three's, etc. */
} GB_customer_bingo_vars;           /* 9 bytes */

typedef struct 
{
   GB_customer_bingo_vars primary;		/* primary bingo card variables */
   GB_customer_bingo_vars secondary;   /* secondary bingo card variables */
} GB_customer_bingo_type;

typedef struct 
{
	unsigned long  GB_frenzy_award;
	unsigned long  whole_points;           /* holds customer session whole point total */
	unsigned short fract_points;           /* holds customer session fractional point total */
	unsigned long  cents_in;               /* holds customer session coins in */
	unsigned long  cents_out;              /* holds customer session coins out */
	unsigned long  drop_coins;             /* holds customer session coins drop */
	unsigned long  games;                  /* holds customer session games */
	unsigned long  timer;                  /* holds customer session seconds */
	unsigned char  GB_4ok_frenzy;
	unsigned char  jackpots;               /* holds customer session jackpots */
	unsigned char  password_attempts;      /* holds number of password attempts */
	unsigned char  game_status;
} SESSION_DATA;

typedef enum
{
	OFF_UNAVAIL,	/* player logged off/system unavailable */
	OFF_AVAIL,		/* player logged off/system available */
	ON_AVAIL,		/* player logged on/system available */
	ON_UNAVAIL		/* player logged on/system unavailable */
} GbSystemState;

typedef struct
{
	char smux_id;
	unsigned char gb_on;
	unsigned char gb_frenzy_on;
	unsigned char autopay_enabled;
	unsigned char jackpot_to_credit_enabled;
	unsigned char club_code;
	char gb_player_name[MAX_CUSTOMER_NAME];
	unsigned char cntrl_time[8];
	unsigned long points_redeemed[3]; // returned values for redemption, needed for the GB_CFG.C
	unsigned short redemption_cents[3];   // returned values of pennies for redemption
	unsigned char redeem_error_flag;  // redemption error flag
    unsigned long machine_id;
	unsigned short unpd_tck_cnt;
	unsigned long tck_no; // ticket number for cashouts
	bool offline_mode;

	SESSION_DATA ses_data;
//	SESSION_DATA *p_SessionData;

	GbSystemState gb_system_state;

	DWORD member_id;
	DWORD pin;
	ULONG whole_points;
	ULONG monthly_whole_points; // only for Timber's club
	USHORT fract_points;
	ULONG monthly_fract_points; // only for Timber's club
	USHORT inc_points;
	GB_customer_bingo_type bingo;


	unsigned long session_points;

	ULONG marker_available;
	ULONG marker_balance;
	ULONG marker_advanced;
	ULONG marker_transaction_id;
	bool marker_advanced_this_session;
	bool marker_advance_pending;
	bool marker_abort_pending;
	bool waiting_for_marker_advance_response;
	bool emarker_enabled;
	bool encryption_enabled;
	bool sas_six_or_greater;


	ULONG TOTAL_IN;
	ULONG TOTAL_OUT;
	ULONG GAMES_PLAYED;
	ULONG TOTAL_DROP;
	ULONG TOTAL_ATTEN;

	ULONG GAME_GAMES_PLAYED;
	ULONG GAME_GAMES_WON;
	ULONG GAME_CENTS_PLAYED;
	ULONG GAME_CENTS_WON;

	ULONG FOUR_OK_BINGO;
	ULONG REDEEMED_POINTS;
	ULONG BINGO_FRENZY_WON;
	ULONG TOTAL_BILLS;
	ULONG CENTS_WON;

	ULONG TOTAL_HAND_PAID_CANCELLED_CREDITS;

	ULONG machine_accounting_denomination; // default to quarter for now
	ULONG current_game_denomination;
	ULONG handpay_amount;

	unsigned char door_state;
	unsigned char  game_status;
	unsigned char frenzy_4ok;
	unsigned long frenzy_award;
	unsigned long frenzy_award_hit;
	BOOL bingo_card_filled;
	USHORT max_bet;
	unsigned char tck_time[6];
	bool middle_of_a_game;

	char number_of_card_data_blocks;

	unsigned char held_cards[5];

	ULONG GAMES_PLAYED_PER_DENOM [NUMBER_OF_DENOMINATIONS];
	ULONG GAMES_WON_PER_DENOM [NUMBER_OF_DENOMINATIONS];
	ULONG CENTS_PLAYED_PER_DENOM [NUMBER_OF_DENOMINATIONS];
	ULONG CENTS_WON_PER_DENOM [NUMBER_OF_DENOMINATIONS];

	ULONG last_TOTAL_IN;
	bool game_played_since_logon;

	ULONG WAT_IN;
	ULONG WAT_OUT;

	char manufacturer_id[2];

   bool meters_received_after_download;

	ULONG current_bank_meter;

}NonVolatileRam;

extern NonVolatileRam non_volatile_ram;

#endif _NonVolatileRam_H_

/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller	07/03/13	Added a communication interface to the Station's Casino Casino Management System (CMS).
Robert Fuller   03/08/13    Send the XPC Configuration info to the IDC in the OnConnect function.
Robert Fuller   05/31/12    Added code to communicate with the Interactive Display Controller.
Robert Fuller   08/19/08    Added code to support the GB Advantage feature.
*/

#include "stdafx.h"
#include "xp_controller.h"
#include "MyAsyncSocket.h"


// CMyAsyncSocket
IMPLEMENT_DYNCREATE(CMyAsyncSocket, CAsyncSocket)

CMyAsyncSocket::CMyAsyncSocket()
{
	socket_state = WSAENOTCONN;

	memset(rx_buf, 0, sizeof(rx_buf));
	rx_len = 0;

	reading_socket = false;
}

CMyAsyncSocket::~CMyAsyncSocket()
{
}

// CMyAsyncSocket member functions

WORD CMyAsyncSocket::GetBuffer(BYTE *buffer, WORD length)
{
 WORD bytes_copied;

	if (reading_socket || socket_state || !rx_len)
		bytes_copied = 0;
	else
	{
		if (rx_len > length)
			bytes_copied = length;
		else
			bytes_copied = rx_len;

		memcpy(buffer, rx_buf, bytes_copied);

		if (bytes_copied == rx_len)
			rx_len = 0;
		else
		{
			rx_len -= bytes_copied;
			memmove(rx_buf, &rx_buf[bytes_copied], rx_len);
		}

		memset(&rx_buf[rx_len], 0, bytes_copied);
	}

	return bytes_copied;
}

void CMyAsyncSocket::OnClose(int nErrorCode)
{
	socket_state = WSAENOTCONN;

	memset(rx_buf, 0, sizeof(rx_buf));
	rx_len = 0;

	CAsyncSocket::OnClose(nErrorCode);
}

void CMyAsyncSocket::OnReceive(int nErrorCode)
{
 int bytes_received;
 BYTE data_received[TCPIP_MESSAGE_BUFFER_SIZE];

	reading_socket = true;

	memset(data_received, 0, sizeof(data_received));
	bytes_received = Receive(data_received, TCPIP_MESSAGE_BUFFER_SIZE);

	if (!bytes_received)
	{
		Close();
		socket_state = WSAENOTCONN;
	}
	else if (bytes_received == SOCKET_ERROR)
	{
		if (GetLastError() != WSAEWOULDBLOCK)
		{
			Close();
			socket_state = WSAENOTCONN;
		}
		else if (socket_state)
			socket_state = 0;
	}
	else
	{
		if (socket_state)
			socket_state = 0;

		if (bytes_received + rx_len > TCPIP_MESSAGE_BUFFER_SIZE)
		{
			memset(rx_buf, 0, sizeof(rx_buf));
			rx_len = 0;
		}
		else
		{
			memcpy(&rx_buf[rx_len], data_received, bytes_received);
			rx_len += bytes_received;
		}
	}

	CAsyncSocket::OnReceive(nErrorCode);

	reading_socket = false;
}


void CMyAsyncSocket::OnConnect(int nErrorCode)
{
	socket_state = nErrorCode;

    if (nErrorCode != 0)
    {
        switch( nErrorCode )
        {
             case WSAEADDRINUSE: 
//                AfxMessageBox("The specified address is already in use.\n");
                break;
             case WSAEADDRNOTAVAIL: 
//                AfxMessageBox("The specified address is not available from the local machine.\n");
                break;
             case WSAEAFNOSUPPORT: 
//                AfxMessageBox("Addresses in the specified family cannot be used with this socket.\n");
                break;
             case WSAECONNREFUSED: 
//                AfxMessageBox("The attempt to connect was forcefully rejected.\n");
                break;
             case WSAEDESTADDRREQ: 
//                AfxMessageBox("A destination address is required.\n");
                break;
             case WSAEFAULT: 
//                AfxMessageBox("The lpSockAddrLen argument is incorrect.\n");
                break;
             case WSAEINVAL: 
//                AfxMessageBox("The socket is already bound to an address.\n");
                break;
             case WSAEISCONN: 
//                AfxMessageBox("The socket is already connected.\n");
                break;
             case WSAEMFILE: 
//                AfxMessageBox("No more file descriptors are available.\n");
                break;
             case WSAENETUNREACH: 
//                AfxMessageBox("The network cannot be reached from this host at this time.\n");
                break;
             case WSAENOBUFS: 
//                AfxMessageBox("No buffer space is available. The socket cannot be connected.\n");
                break;
             case WSAENOTCONN: 
//                AfxMessageBox("The socket is not connected.\n");
                break;
             case WSAENOTSOCK: 
//                AfxMessageBox("The descriptor is a file, not a socket.\n");
                break;
             case WSAETIMEDOUT: 
//                AfxMessageBox("The attempt to connect timed out without establishing a connection. \n");
                break;
             default:
                TCHAR szError[256];
                wsprintf(szError, "OnConnect error: %d", nErrorCode);
//                AfxMessageBox(szError);
                break;
        }
    }

	CAsyncSocket::OnConnect(nErrorCode);
}



IMPLEMENT_DYNCREATE(CMediaBoxAsyncSocket, CMyAsyncSocket)

CMediaBoxAsyncSocket::CMediaBoxAsyncSocket()
{
	socket_state = WSAENOTCONN;

	memset(rx_buf, 0, sizeof(rx_buf));
	rx_len = 0;

	reading_socket = false;
}

CMediaBoxAsyncSocket::~CMediaBoxAsyncSocket()
{
}

void CMediaBoxAsyncSocket::OnClose(int nErrorCode)
{
//	socket_state = nErrorCode;
    socket_state = WSAENOTCONN;

	CMyAsyncSocket::OnClose(nErrorCode);

Close();
}

void CMediaBoxAsyncSocket::OnConnect(int nErrorCode)
{
	socket_state = nErrorCode;

    if (nErrorCode != 0)
    {
        switch( nErrorCode )
        {
             case WSAEADDRINUSE: 
//                AfxMessageBox("The specified address is already in use.\n");
                break;
             case WSAEADDRNOTAVAIL: 
//                AfxMessageBox("The specified address is not available from the local machine.\n");
                break;
             case WSAEAFNOSUPPORT: 
//                AfxMessageBox("Addresses in the specified family cannot be used with this socket.\n");
                break;
             case WSAECONNREFUSED: 
//                AfxMessageBox("The attempt to connect was forcefully rejected.\n");
                break;
             case WSAEDESTADDRREQ: 
//                AfxMessageBox("A destination address is required.\n");
                break;
             case WSAEFAULT: 
//                AfxMessageBox("The lpSockAddrLen argument is incorrect.\n");
                break;
             case WSAEINVAL: 
//                AfxMessageBox("The socket is already bound to an address.\n");
                break;
             case WSAEISCONN: 
//                AfxMessageBox("The socket is already connected.\n");
                break;
             case WSAEMFILE: 
//                AfxMessageBox("No more file descriptors are available.\n");
                break;
             case WSAENETUNREACH: 
//                AfxMessageBox("The network cannot be reached from this host at this time.\n");
                break;
             case WSAENOBUFS: 
//                AfxMessageBox("No buffer space is available. The socket cannot be connected.\n");
                break;
             case WSAENOTCONN: 
//                AfxMessageBox("The socket is not connected.\n");
                break;
             case WSAENOTSOCK: 
//                AfxMessageBox("The descriptor is a file, not a socket.\n");
                break;
             case WSAETIMEDOUT: 
//                AfxMessageBox("The attempt to connect timed out without establishing a connection. \n");
                break;
             default:
                TCHAR szError[256];
                wsprintf(szError, "OnConnect error: %d", nErrorCode);
//                AfxMessageBox(szError);
                break;
        }
    }

	CMyAsyncSocket::OnConnect(nErrorCode);
}


IMPLEMENT_DYNCREATE(CIDCAsyncSocket, CMyAsyncSocket)

CIDCAsyncSocket::CIDCAsyncSocket()
{
	socket_state = WSAENOTCONN;

	memset(rx_buf, 0, sizeof(rx_buf));
	rx_len = 0;

	reading_socket = false;
}

CIDCAsyncSocket::~CIDCAsyncSocket()
{
}

void CIDCAsyncSocket::OnClose(int nErrorCode)
{
//	socket_state = nErrorCode;
    socket_state = WSAENOTCONN;

	CMyAsyncSocket::OnClose(nErrorCode);

Close();
}

void CIDCAsyncSocket::OnConnect(int nErrorCode)
{
	socket_state = nErrorCode;

    if (nErrorCode != 0)
    {
        switch( nErrorCode )
        {
             case WSAEADDRINUSE: 
//                AfxMessageBox("The specified address is already in use.\n");
                break;
             case WSAEADDRNOTAVAIL: 
//                AfxMessageBox("The specified address is not available from the local machine.\n");
                break;
             case WSAEAFNOSUPPORT: 
//                AfxMessageBox("Addresses in the specified family cannot be used with this socket.\n");
                break;
             case WSAECONNREFUSED: 
//                AfxMessageBox("The attempt to connect was forcefully rejected.\n");
                break;
             case WSAEDESTADDRREQ: 
//                AfxMessageBox("A destination address is required.\n");
                break;
             case WSAEFAULT: 
//                AfxMessageBox("The lpSockAddrLen argument is incorrect.\n");
                break;
             case WSAEINVAL: 
//                AfxMessageBox("The socket is already bound to an address.\n");
                break;
             case WSAEISCONN: 
//                AfxMessageBox("The socket is already connected.\n");
                break;
             case WSAEMFILE: 
//                AfxMessageBox("No more file descriptors are available.\n");
                break;
             case WSAENETUNREACH: 
//                AfxMessageBox("The network cannot be reached from this host at this time.\n");
                break;
             case WSAENOBUFS: 
//                AfxMessageBox("No buffer space is available. The socket cannot be connected.\n");
                break;
             case WSAENOTCONN: 
//                AfxMessageBox("The socket is not connected.\n");
                break;
             case WSAENOTSOCK: 
//                AfxMessageBox("The descriptor is a file, not a socket.\n");
                break;
             case WSAETIMEDOUT: 
//                AfxMessageBox("The attempt to connect timed out without establishing a connection. \n");
                break;
             default:
                TCHAR szError[256];
                wsprintf(szError, "OnConnect error: %d", nErrorCode);
//                AfxMessageBox(szError);
                break;
        }
    }
	else
    {
logMessage("CIDCAsyncSocket::OnConnect() - successful socket connection");

		theApp.SendIdcXpcVersionConfigInfo();
		theApp.SendAllMachineSpecificData();
}

	CMyAsyncSocket::OnConnect(nErrorCode);
}





IMPLEMENT_DYNCREATE(CCmsAsyncSocket, CMyAsyncSocket)

CCmsAsyncSocket::CCmsAsyncSocket()
{
	socket_state = WSAENOTCONN;

	memset(rx_buf, 0, sizeof(rx_buf));
	rx_len = 0;

	reading_socket = false;
}

CCmsAsyncSocket::~CCmsAsyncSocket()
{
}

void CCmsAsyncSocket::OnClose(int nErrorCode)
{
//	socket_state = nErrorCode;
    socket_state = WSAENOTCONN;

	CMyAsyncSocket::OnClose(nErrorCode);

Close();
}

void CCmsAsyncSocket::OnConnect(int nErrorCode)
{
	socket_state = nErrorCode;

    if (nErrorCode != 0)
    {
        switch( nErrorCode )
        {
             case WSAEADDRINUSE: 
//                AfxMessageBox("The specified address is already in use.\n");
                break;
             case WSAEADDRNOTAVAIL: 
//                AfxMessageBox("The specified address is not available from the local machine.\n");
                break;
             case WSAEAFNOSUPPORT: 
//                AfxMessageBox("Addresses in the specified family cannot be used with this socket.\n");
                break;
             case WSAECONNREFUSED: 
//                AfxMessageBox("The attempt to connect was forcefully rejected.\n");
                break;
             case WSAEDESTADDRREQ: 
//                AfxMessageBox("A destination address is required.\n");
                break;
             case WSAEFAULT: 
//                AfxMessageBox("The lpSockAddrLen argument is incorrect.\n");
                break;
             case WSAEINVAL: 
//                AfxMessageBox("The socket is already bound to an address.\n");
                break;
             case WSAEISCONN: 
//                AfxMessageBox("The socket is already connected.\n");
                break;
             case WSAEMFILE: 
//                AfxMessageBox("No more file descriptors are available.\n");
                break;
             case WSAENETUNREACH: 
//                AfxMessageBox("The network cannot be reached from this host at this time.\n");
                break;
             case WSAENOBUFS: 
//                AfxMessageBox("No buffer space is available. The socket cannot be connected.\n");
                break;
             case WSAENOTCONN: 
//                AfxMessageBox("The socket is not connected.\n");
                break;
             case WSAENOTSOCK: 
//                AfxMessageBox("The descriptor is a file, not a socket.\n");
                break;
             case WSAETIMEDOUT: 
//                AfxMessageBox("The attempt to connect timed out without establishing a connection. \n");
                break;
             default:
                TCHAR szError[256];
                wsprintf(szError, "OnConnect error: %d", nErrorCode);
//                AfxMessageBox(szError);
                break;
        }
    }
	else
    {
        // request login messages from CMS interface client
        theApp.SendCmsLoginStatusRequest();
		theApp.SendIdcCmsConnectionStatusMessage (FALSE);
		theApp.cms_com_disconnected = FALSE;
    }

	CMyAsyncSocket::OnConnect(nErrorCode);
}

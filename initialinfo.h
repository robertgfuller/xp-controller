/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   08/19/08    Added code to support the GB Advantage feature.
Dave Elquist    07/15/05    Added support to changed settings from engineer's menu.
Dave Elquist    04/14/05    Initial Revision.
*/


#pragma once
#include "afxwin.h"


// CInitialInfoDlg dialog

class CInitialInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(CInitialInfoDlg)

public:
	CInitialInfoDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInitialInfoDlg();
	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_INITIAL_INFO_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    virtual LRESULT DefWindowProc(UINT nMsg, WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedZeroKeypadButton();
	afx_msg void OnBnClickedOneKeypadButton();
	afx_msg void OnBnClickedTwoKeypadButton();
	afx_msg void OnBnClickedThreeKeypadButton();
	afx_msg void OnBnClickedFourKeypadButton();
	afx_msg void OnBnClickedFiveKeypadButton();
	afx_msg void OnBnClickedSixKeypadButton();
	afx_msg void OnBnClickedSevenKeypadButton();
	afx_msg void OnBnClickedEightKeypadButton();
	afx_msg void OnBnClickedNineKeypadButton();
	afx_msg void OnBnClickedColonKeypadButton();
	afx_msg void OnBnClickedPeriodKeypadButton();
	afx_msg void OnBnClickedOkKeypadButton();
	afx_msg void OnBnClickedCancelKeypadButton();
	afx_msg void OnBnClickedClearKeypadButton();
	afx_msg void OnEnSetfocusCustomerControlServerIpPortEdit();
	afx_msg void OnEnSetfocusCustomerControlClientPortEdit();
	afx_msg void OnBnClickedInitialSouthButton();
	afx_msg void OnBnClickedInitialNorthButton();
	afx_msg void OnBnClickedInitialEastButton();
	afx_msg void OnEnSetfocusGbProConfiguratorIpPortEdit();

// private functions
private:
    void ProcessButtonClick(BYTE button_press);

// private variables
private:
    DWORD current_edit_focus;
};

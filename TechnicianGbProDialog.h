/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    06/24/05    Initial Revision.
*/


#pragma once
#include "afxwin.h"


// CTechnicianGbProDialog dialog

class CTechnicianGbProDialog : public CDialog
{
	DECLARE_DYNAMIC(CTechnicianGbProDialog)

public:
	CTechnicianGbProDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTechnicianGbProDialog();
	virtual BOOL OnInitDialog();

	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedTechnicianGbProPrinterStatusButton();
	afx_msg void OnBnClickedTechnicianGbProPrinterResetButton();
	afx_msg void OnBnClickedTechnicianGbProPrinterQueueButton();
	afx_msg void OnBnClickedTechnicianGbProServerStatusButton();
	afx_msg void OnBnClickedTechnicianGbProServerQueueButton();
	afx_msg void OnBnClickedTechnicianGbProExitButton();
	afx_msg void OnBnClickedTechnicianGbProTestMdtPrinterButton();
	afx_msg void OnBnClickedTechnicianGbProMdtServerSaveButton();
	afx_msg void OnBnClickedTechnicianGbProMdtPrinterSaveButton();
	afx_msg void OnBnClickedTechnicianGbProMdtKeyboardButton();

// Dialog Data
	enum { IDD = IDD_TECHNICIAN_GB_PRO_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:
	CListBox m_technician_gb_pro_listbox;
};

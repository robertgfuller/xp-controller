#ifndef SAS_COMM_H
#define SAS_COMM_H 1

#ifndef FTD2XX_H
#include "ftd2xx.h"
#endif

#ifndef NonVolatileRam_H
#include "NonVolatileRam.h"
#endif

#define MAX_POLL_CYCLES_WITHOUT_SAS_DATA	100
#define  MAX_SAS_MSG_LENGTH			2000

#define SAS_MESSAGE_QUE_SIZE                    20
#define SAS_AFT_MESSAGE_QUE_SIZE                3

#define DOOR1          0x20                    /* these bitmaps match the schematic */
#define DOOR2          0x40                    /* ...layout for the inputs (I/O) */
#define DOOR3          0x80
#define DOOR4          0x08
#define DOOR5          0x10
#define DOOR6          0x04
#define DOOR7          0x02
                                                                                                /* logical door ID's */
#define MAIN_DOOR       DOOR1                   /* bit positions for DOORS see */
#define LOGIC_DOOR      DOOR2                   /* "input translator" for more */
#define BLTCH_DOOR      DOOR3                   /* information. These are used */
#define BILL_DOOR       DOOR4                   /* to set bits in various bytes */
#define STACKER_DOOR    DOOR5                   /* for door and printer status. */
#define CASH_DOOR       DOOR6                   /* */
#define EXTRA_DOOR      DOOR7                   /* logical extra door */

typedef unsigned char BCD;

typedef enum POLL_TYPE
{
	G_POLL = 0,
	GENERAL_POLL,
	R_POLL,
	S_POLL,
	M_POLL,
};

typedef enum TRANSFER_TYPE
{
	IN_HOUSE,
	BONUS_COIN_OUT = 0x10,
	BONUS_JACKPOT = 0x11
};

typedef struct
{
	POLL_TYPE poll_type;
	bool message_ready;
	UINT message_size;
	unsigned char message[MAX_SAS_MSG_LENGTH];
}SAS_MESSAGE;

typedef struct
{
	unsigned char address;
	unsigned char command;
	BCD cancelled_credits[4];
	BCD total_in[4];
	BCD total_out[4];
	BCD total_drop[4];
	BCD total_jackpot[4];
	BCD games_played[4];
	WORD crc;
}MeterDataRequestResponse;


typedef struct
{
	unsigned char address;
	unsigned char command;
	BCD ones[4];
	BCD fives[4];
	BCD tens[4];
	BCD twenties[4];
	BCD fifties[4];
	BCD hundreds[4];
	WORD crc;
}BillMeterDataRequestResponse;


typedef struct
{
	unsigned char address;
	unsigned char command;
	BCD cancelled_credits[4];
	BCD total_in[4];
	BCD total_out[4];
	BCD total_drop[4];
	BCD total_jackpot[4];
	BCD games_played[4];
	unsigned char unused;
	WORD crc;
}BallyMeterDataRequestResponse;

typedef struct
{
	unsigned char address;
	unsigned char command;
	unsigned char length;
	BCD game_number[2];
	unsigned char meter_code1;
	BCD games_played[4];
	unsigned char meter_code2;
	BCD games_won[4];
	unsigned char meter_code3;
	BCD cents_played[4];
	unsigned char meter_code4;
	BCD cents_won[4];
	WORD crc;
}SelectedGameMetersRequestResponse;

typedef struct
{
	unsigned char address;
	unsigned char command;
	BCD game_number[2];
	BCD total_hand_paid_cancelled_credits[4];
	WORD crc;
}TotalHandPaidCancelledCreditsRequestResponse;

typedef struct
{
	unsigned char address;
	unsigned char command;
	char game_id[2];
	char additional_game_id[3];
	unsigned char denomination;
	unsigned char max_bet;
	unsigned char progressive_group;
	WORD game_options;
	char paytable[6];
	char base_percentage[4];
	WORD crc;
}MachineInfoRequestResponse;

typedef struct
{
	unsigned char address;
	unsigned char command;
	BCD game_number[2];
	WORD crc;
}SelectedGameNumberResponse;

typedef struct
{
	unsigned char address;
	unsigned char command;
	BCD game_number[2];
	char game_id[2];
	char additional_id[3];
	unsigned char denomination;
	unsigned char max_bet;
	unsigned char progressive_group;
	WORD game_options;
	char paytable[6];
	char base_percentage[4];
	WORD crc;
}GameConfigurationResponse;

typedef struct
{
	unsigned char address;
	unsigned char event_id;
	unsigned char bill_accepted; // exception code
	BCD country_code;
	BCD denomination_code;
	BCD bills_of_this_type_accepted[4];
	WORD crc;
}BillAcceptedEvent;

typedef struct
{
	unsigned char address;
	unsigned char event_id;
	unsigned char game_start;
	BCD credits_wagered[2];
	BCD total_coin_in_meter[4];
	unsigned char wager_type;
	unsigned char progressive_group;
	WORD crc;
}GameStartEvent;

typedef struct
{
	unsigned char address;
	unsigned char event_id;
	unsigned char game_end;
	BCD game_win[4];
	WORD crc;
}GameEndEvent;

typedef struct
{
	unsigned char address;
	unsigned char event_id;
	unsigned char game_selected;
	BCD game_number[2];
	WORD crc;
}GameSelectedEvent;

typedef struct
{
	unsigned char address;
	unsigned char event_id;
	unsigned char card_held_not_held_exception;
	unsigned char card_status;
	WORD crc;
}CardHeldNotHeldEvent;

typedef struct
{
	unsigned char address;
	unsigned char command;
	unsigned char hand_type;
	unsigned char hand[5];
	WORD crc;
}CardInfoResponse;

typedef struct
{
	unsigned char address;
	unsigned char command;
	unsigned char progressive_group;
	unsigned char level;
	BCD amount[5];
	BCD partial_pay[2];
	unsigned char reset_id;
	unsigned char unused[10];
	WORD crc;
}HandpayInfoResponse;

typedef struct
{
	unsigned char address;
	unsigned char command;
	unsigned char length;
	char sas_version[3];
}SasVersionResponse;

typedef struct
{
	unsigned char address;
	unsigned char command;
	BCD current_bank_meter[4];
	WORD crc;
}CurrentCreditsResponse;

typedef struct
{
	unsigned char address;
	unsigned char command;
	unsigned char length;
	unsigned char denom_code;
	unsigned char base_command;
	unsigned char number_of_following_bytes;
}MultiDenomMessageResponse;

typedef struct
{
	unsigned char address;
	unsigned char command;
	unsigned char length;
	unsigned char transaction_buffer_position;
	unsigned char transfer_status;
	unsigned char reciept_status;
	unsigned char transfer_type;
	BCD cashable_amount[5];
	BCD restricted_amount[5];
	BCD nonrestricted_amount[5];
	unsigned char transfer_flags;
	unsigned long asset_number;
	unsigned char transaction_id_length;
}TransferCreditsResponse;

//#pragma pack(1)

typedef struct
{
	unsigned char address;
	unsigned char command;
	unsigned char length;
	unsigned long asset_number;
	unsigned char game_lock_status;
	unsigned char available_transfers;
	unsigned char host_cashout_status;
	unsigned char aft_status;
	unsigned char max_buffer_status;
	BCD cashable_amount[5];
	BCD restricted_amount[5];
	BCD nonrestricted_amount[5];
	BCD transfer_limit[5];
	BCD restricted_expiration[4];
	BCD restricted_pool_id[2];
	WORD crc;
}AftLockAndStatusResponse;

typedef struct
{
	unsigned char address;
	unsigned char command;
	unsigned char length;
	unsigned char registration_status;
//	unsigned long asset_number;
	unsigned char asset_number[4];
	unsigned char registration_key[20];
	unsigned char pos_id[4];
	WORD crc;
}AftRegistrationResponse;

//#pragma pack(1)

typedef struct
{
	unsigned char address;
	unsigned char command;
	unsigned char ack_code;
	WORD crc;
}EnableJackpotHandpayResetMethodResponse;

typedef struct
{
	unsigned char address;
	unsigned char command;
	unsigned char reset_code;
	WORD crc;
}HandpayResetResponse;

typedef union
{
	unsigned long tckt_no_long;
	WORD tckt_int[2];
}TicketNumber;


class CGameCommControl;
struct FileGameLock;


class CSAS_Comm
{
// Construction
public:
//	CSAS_Comm();
	CSAS_Comm(LPVOID pGameCommControl, BYTE sas_address);

	~CSAS_Comm();

	BOOL openSASPort (BYTE usb_rs232_port, char* serial_number);
	BOOL closeSASPort(void);
	void processSASMessages (void);
	BOOL processSASMessage (void);
	void initializeSAS (void);
	DWORD getSerialData (unsigned char* lpBuffer);
	bool CheckConnections (void);
	void queueInitiateLegacyBonus (ULONG amount, bool redemption);
	void saveNonVolatileRam (void);

	void getGeneralPollResponse (void);
	void getRPollResponse (void);
	void getSPollResponse (void);
	void getMPollResponse (void);

	void queueHandpayReset (void);
	void queueRequestMeterData (void);
	void queueRequestBillMeterData (void);
	void queueSendHandpayInfo (void);

	void sendDateAndTimeMessage (BYTE month, BYTE day, BYTE year, BYTE hour, BYTE minute, BYTE second);

	// Gamblers Bonus Info Managment Routines
	void changeGbSystemState (GbSystemState system_state);
	GbSystemState getGbSystemState (void);

	unsigned long calculateCardAward (unsigned long handle, unsigned short games);

	typedef FT_STATUS (WINAPI *PtrToGetQueueStatus)(FT_HANDLE, LPDWORD);
	PtrToGetQueueStatus m_pGetQueueStatus;
	FT_STATUS GetQueueStatus(LPDWORD);

	typedef FT_STATUS (WINAPI *PtrToCreateDeviceInfoList)(LPDWORD);
	PtrToCreateDeviceInfoList m_pCreateDeviceInfoList; 
	FT_STATUS CreateDeviceInfoList(LPDWORD);

	FT_STATUS SetEventNotification(FT_HANDLE, DWORD, PVOID);

	FT_STATUS ftStatus; 
	FT_HANDLE m_ftHandle;
	HANDLE hRxCommEvent; 
	DWORD EventMask;
	HMODULE m_hmodule;

	bool stop_comm_thread;
	bool sas_communicating;
    DWORD bytes_in_rx_queue;

	NonVolatileRam non_volatile_ram;

	ULONG last_session_update_points;

	unsigned char receive_buffer[MAX_SAS_MSG_LENGTH];
	DWORD bytes_received;
	DWORD receive_buffer_index;
	SAS_MESSAGE current_tx_message;
	bool poll_SAS;
	bool check_game_meters;
	BYTE sas_id;
	bool serial_recieve_task_active;
	time_t last_meter_check_time;
	time_t last_sas_communication_check_time;
	ULONG meter_check_msec_count;
	ULONG idle_meter_sends;
	time_t last_sas_com_status_send;
	time_t last_sas_poll_received_time;
	time_t last_zero_credit_logout_check_time;
	SYSTEMTIME last_game_end_time;
	bool check_game_delay_delta;

    DWORD number_of_usb_devices;
	unsigned long bonus_jackpot_award;
	bool point_redemption;

protected:

private:
	char sas_version[3]; // filled at initialization
	unsigned char asset_number[4]; // filled at initialization

	// local variables
	DCB dcb;
	//HANDLE SAS_port_handle = INVALID_HANDLE_VALUE;
	//FT_HANDLE FT_SAS_port_handle = INVALID_HANDLE_VALUE;
	//static HANDLE SAS_serial_process_task_handle = INVALID_HANDLE_VALUE;
    BOOL real_time_event_activity;
	UINT poll_cyle_count;
	UINT poll_cyles_without_sas_data;
	WORD poll_count;
	WORD no_response_count;
	HANDLE rx_message_semiphore;
	HANDLE message_queue_semiphore;
	HANDLE aft_message_queue_semiphore;

	WORD total_queued_messages;
	WORD total_queued_aft_messages;
	bool real_time_event_reporting_enabled;

	SAS_MESSAGE response_message;
	SAS_MESSAGE sas_message_que[SAS_MESSAGE_QUE_SIZE];
	SAS_MESSAGE sas_aft_message_que[SAS_AFT_MESSAGE_QUE_SIZE];
	SAS_MESSAGE pending_sas_aft_message;
	SAS_MESSAGE card_info_message_que[1];
	bool pending_card_info_message;

	bool single_hand_pokers_in_base_chip;
	bool poker_game_being_played;
	bool bj_game_being_played;
	bool first_meter_fetch;
	bool first_game_play;
	bool logout_after_4ok_win_check;

	bool aft_pending;
	char w2g_pay;

	ULONG current_game_wager;
	BCD currently_selected_game_number[2];

	char transaction_number;

	char paytable_string[6];
	char base_percentage_string[4];
	char string1[7]; // utility string array
	char string2[7]; // utility string array

	unsigned char cumulative_drink_comp_dollars;


	bool machine_info_recieved;

	long last_handpay_win_amount;

	ULONG game_delay_in_msecs;
	ULONG message_delay_in_msecs;

	bool game_delay_check_timer_expired;

	ULONG game_win;

	bool handpay_pending;
	bool handpay_info_received;

	bool delay_next_sas_poll;

	UCHAR handpay_reset_sends;

	CGameCommControl* pCGameCommControl;

	TicketNumber ticket_number;

	ULONG games_played_since_last_meter_send;

	typedef FT_STATUS (WINAPI *PtrToOpen)(int, FT_HANDLE *); 
	PtrToOpen m_pOpen; 
	FT_STATUS Open(int);

	typedef FT_STATUS (WINAPI *PtrToOpenEx)(PVOID, DWORD, FT_HANDLE *); 
	PtrToOpenEx m_pOpenEx; 
	FT_STATUS OpenEx(char*);

	typedef FT_STATUS (WINAPI *PtrToListDevices)(PVOID, PVOID, DWORD);
	PtrToListDevices m_pListDevices; 
	FT_STATUS ListDevices(PVOID, PVOID, DWORD);

	typedef FT_STATUS (WINAPI *PtrToGetDeviceInfo)(FT_HANDLE, FT_DEVICE*, LPDWORD, PCHAR, PCHAR, PVOID);
	PtrToGetDeviceInfo m_pGetDeviceInfo;
	FT_STATUS GetDeviceInfo (FT_HANDLE, FT_DEVICE*, LPDWORD, PCHAR, PCHAR, PVOID);

	typedef FT_STATUS (WINAPI *PtrToClose)(FT_HANDLE);
	PtrToClose m_pClose;
	FT_STATUS Close();

	typedef FT_STATUS (WINAPI *PtrToRead)(FT_HANDLE, LPVOID, DWORD, LPDWORD);
	PtrToRead m_pRead;
	FT_STATUS Read(LPVOID, DWORD, LPDWORD);

	typedef FT_STATUS (WINAPI *PtrToWrite)(FT_HANDLE, LPVOID, DWORD, LPDWORD);
	PtrToWrite m_pWrite;
	FT_STATUS Write(LPVOID, DWORD, LPDWORD);

	typedef FT_STATUS (WINAPI *PtrToResetDevice)(FT_HANDLE);
	PtrToResetDevice m_pResetDevice;
	FT_STATUS ResetDevice();

	typedef FT_STATUS (WINAPI *PtrToStopInTask)(FT_HANDLE);
	PtrToStopInTask m_pStopInTask;
	FT_STATUS StopInTask();

	typedef FT_STATUS (WINAPI *PtrToRestartInTask)(FT_HANDLE);
	PtrToRestartInTask m_pRestartInTask;
	FT_STATUS RestartInTask();

	typedef FT_STATUS (WINAPI *PtrToPurge)(FT_HANDLE, ULONG);
	PtrToPurge m_pPurge;
	FT_STATUS Purge(ULONG);

	typedef FT_STATUS (WINAPI *PtrToSetTimeouts)(FT_HANDLE, ULONG, ULONG);
	PtrToSetTimeouts m_pSetTimeouts;
	FT_STATUS SetTimeouts(ULONG, ULONG);


	typedef FT_STATUS (WINAPI *PtrToSetDataCharacteristics)(FT_HANDLE, UCHAR, UCHAR, UCHAR);
	PtrToSetDataCharacteristics m_pSetDataCharacteristics;
	FT_STATUS SetDataCharacteristics(FT_HANDLE, UCHAR, UCHAR, UCHAR);

	typedef FT_STATUS (WINAPI *PtrToSetBaudRate)(FT_HANDLE, DWORD);
	PtrToSetBaudRate m_pSetBaudRate;
	FT_STATUS SetBaudRate(FT_HANDLE, DWORD);

	typedef FT_STATUS (WINAPI *PtrToSetEventNotification)(FT_HANDLE, DWORD, PVOID);
	PtrToSetEventNotification m_pSetEventNotification;

	typedef FT_STATUS (WINAPI *PtrToSetUSBParameters)(FT_HANDLE, ULONG, ULONG);
	PtrToSetUSBParameters m_pSetUSBParameters;
	FT_STATUS SetUSBParameters (FT_HANDLE ftHandle, ULONG ulInTransferSize, ULONG ulOutTransferSize);

	void queueTransferFunds (ULONG amount, TRANSFER_TYPE transfer_type);
	void putMoneyOnTheGame (ULONG amount);
	void queueSendTotalHandPaidCancelledCredits (void);
	void queueRequestMachineInfo (void);
	void queueSendCurrentCredits (void);
	void queueGameDelayMessage (void);

	void ReportCommError(LPTSTR lpszMessage);







	void dequeueSasMessage (void);

	// statics
	void sendSerialMessage (SAS_MESSAGE* sas_message);
	void sendAck (void);
	void sendAck2 (void);
	void sendFirstMessageByte (unsigned char data);
	void sendRemainingMessageBytes (unsigned char* tx_msg, UINT len);
	bool queueSasMessage (SAS_MESSAGE* sas_message);

	void queueSingleMeterPoll (UCHAR command);
	void queueEnableEventReporting (bool flag);
	void queueSendSelectedGameNumber (void);
	void queueSendEnabledGameNumbers (void);
	void queueSendGameConfiguration (BCD* game_number);
	void queueSendCardInfo (void);
	void queueSendSasVersion (void);
	void queueSendSelectedGameMeterData (BCD* game_number);
	void queueAftRegistration (unsigned char registration_code);
	void queueEnableJackpotHandpayResetMethod (void);
	void queueAftStatusRequest (void);
	void queueAftInterrogation (void);
	bool queueSasAftMessage (SAS_MESSAGE* sas_message);
	void processRPollResponse (void);
	void processSPollResponse (void);
	void processMPollResponse (void);
	void processRealTimeEventMessage (void);
	void processMeterDataRequestResponse (void);
	void processBallyMeterDataRequestResponse (void);
	void processBillMeterDataRequestResponse (void);
	void processSelectedGameMetersResponse (void);
	void processMachineInfoRequestResponse (void);
	void processSelectedGameNumberResponse (void);
	void processGameConfigurationResponse (void);
	void determineGameType (void);
	void processCardInfoResponse (void);
	void processHandpayInfoResponse (void);
	void processSasVersionResponse (void);
	void processTransferFundsResponse (void);
	void processAftLockAndStatusResponse (void);
	void processAftRegistrationResponse (void);
	void processEnableJackpotHandpayResetMethodResponse (void);
	void processHandpayResetResponse (void);
	void processBillAcceptedEvent (void);
	void processGameStartEvent (void);
	void processGameEndEvent (void);
	void processGameSelectedEvent (void);
	void processTotalHandPaidCancelledCredits (void);
	void processCardHeldNotHeldMessage (void);
	void processSendCurrentCredits (void);
	void processMultiDenomMessage (void);

	void savePendingSasAftMessage (void);
	bool aftInterrogationPollQueued (void);

	void dequeueSasAftMessage (void);
	void requeuePendingSasAftMessage (void);
	void clearPendingSasAftMessage (void);

	void clearSasMessage (SAS_MESSAGE* msg);
	UINT getEventMessageSize (UINT exception_code);
	UINT getRPollMessageSize (void);
	UINT getSPollMessageSize (void);
	UINT getMPollMessageSize (void);

	void sendQueuedSasMessage (void);
	void sendQueuedSasAftMessage (void);
	void sendGeneralPoll (unsigned char game_address);
	void sendBroadcastPoll (void);

	unsigned short CRC (unsigned char *s, int len, unsigned short crcval);
	char incrementTransactionNumber ();

	ULONG translateDenomination (unsigned char denomination_code);
	bool isSingleHandBase (char* base_string);
//	unsigned char get4okWin (unsigned char* hand);
	bool queueCardInfoSasMessage (SAS_MESSAGE* sas_message);
	void dequeueCardInfoSasMessage (void);
	void sendQueuedCardInfoSasMessage (void);

	void LoadDLL (void);
	void Loadem (void);

	void getFileGameLock(FileGameLock* file_game_lock, long handpay_amount, unsigned char jackpot_to_credit);

	void incrementPlayerGbPoints (ULONG current_wager);
	void incrementBingoHandle (ULONG current_wager);
	long markBingoCard (unsigned char rank);
	void check4okBingoWin (void);

	unsigned long getThirdBracketAward (unsigned long handle, unsigned short games);
	unsigned long getSecondBracketAward (unsigned long handle, unsigned short games);
	unsigned long getFirstBracketAward (void);

	unsigned char bcdToUnsignedChar (unsigned char bcd);
	ULONG bcd4ToUnsignedLong (unsigned char bcd0,
	                                unsigned char bcd1,
	                                unsigned char bcd2,
	                                unsigned char bcd3);
	ULONG bcd5ToUnsignedLong (unsigned char bcd0, unsigned char bcd1, unsigned char bcd2, unsigned char bcd3, unsigned char bcd4);
	ULONG bcd2ToUnsignedLong (unsigned char bcd0, unsigned char bcd1);
	void unsignedLongToBcd4 (ULONG u_long, BCD* bcd);
	void unsignedLongToBcd5 (ULONG u_long, BCD* bcd);

};

CSAS_Comm* getSasCommPrt (unsigned char game_address);


#endif

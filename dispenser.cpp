/* Modification History:
Name            Date        Notes
----            ----        -----

Robert Fuller	11/04/14	Updated the dispenser values on disk after a SmartVend fill.
Robert Fuller	05/02/14	Increment reject bill amounts before zeroing receive buffer in the fatal error routine. Make sure reject values cannot be negative values.
Robert Fuller	05/02/14	Allow the Puloon dispenser to dispense all bills in a cassette without having a "5 bill margin for error".
Robert Fuller	05/02/14	Checked the dispenser type before setting internal bill count values from extern SmartVend fills.
Robert Fuller	05/02/14	Added support for the 4 cassette Puloon dispenser
Robert Fuller   03/08/13	Added code to handle Puloon dispense errors.
Robert Fuller   03/08/13    Added code to display error screens if dispenser fails to responde to a dispense command.
Robert Fuller   03/08/13    Added code to support the new configurable dispenser payout limit.
Robert Fuller   03/08/13    Removed obsolete Location Collection code.
Robert Fuller   02/14/13    Zero out the reject bills for casette #2 when there is an external full fill.
Robert Fuller   09/14/12    Added code to facilitate external fill operations initiated by location owners.
Robert Fuller   12/30/11    Adjusted the dispenser payout screens strings to include SAS direct connect games sas id.
Robert Fuller   10/06/11    Only print dispenser low messages when controller not connected to the stratus or the customer does collections.
Robert Fuller   04/26/11    Added the 'do not print dispense info' flag and functionality.
Robert Fuller   10/27/10    Use the upper and lower Puloon dispense command to avoid unnecessary cashier dispenser interaction.
Robert Fuller   06/10/10    Added code to support the new $5 $20 LCDM-2000 configuration. 
Robert Fuller   06/02/10    Added code to support the new Puloon LCDM-2000 dual denomination dispenser. 
Robert Fuller   05/21/10    Clear the dispenser receive buffer if we receive a Puloon message with an unknown header.
Robert Fuller   05/21/10    Clear the payout complete flag when receiving a Puloon purge message.
Robert Fuller   05/20/10    Print a "DM" on the ticket fill and drop reports when a dispenser malfunction is detected during a payout.
Robert Fuller   05/14/10    Separated the JCM received message processing from the dispense command to fix an infinite loop bug.
Robert Fuller   03/11/10    Fixed problem with Puloon dispenses over 69 bills.
Robert Fuller   01/19/10    Slowed the dispenser poll cycle down to once per second.
Robert Fuller   01/19/10    Added code to support the new Puloon dispenser.
Robert Fuller   01/19/10    Detect when the JCM dispenser does a hard reset, and clear with a soft reset command.
Robert Fuller   10/30/09    When the JCM dispenser has a problem during a dispense, alert the cashier to service the dispenser,
                            and print problem on lat tape, then write the ticket to the paid ticket file.
Robert Fuller   01/05/09    Fixed a memory overwrite problem when a JCM dispense error occurs.
Robert Fuller   05/02/08    Added code to support CD2000 dispenser.
Robert Fuller   11/05/07    Added support for Diebold dispenser.
Dave Elquist    07/15/05    Changed dispenser(lat) message from the Stratus.
Dave Elquist    04/14/05    Initial Revision.
*/


// dispenser.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include ".\dispenser.h"
#include <time.h>

// constants    
const BYTE SMUX_DISPENSER_MESSAGE_LENGTH = 20; 

static bool reset_bit_cleared = FALSE;

static BYTE sst1, sst3, sst4;

static WORD pulloon_bill_cnt1 = 0;
static WORD pulloon_bill_cnt2 = 0;
static WORD jcm_bill_cnt1 = 0;

static CMessageBox *pMessageBox = 0;

static DWORD current_ucmc_id;
static BYTE  current_mux_id;
static DWORD current_ticket_id;
static DWORD current_amount;
static WORD current_bill_cnt1;
static WORD current_bill_cnt2;

// CDispenserControl
CDispenserControl::CDispenserControl()
{
 FileDispenserValues file_dispenser_values;

    memset(&ecash_settings, 0, sizeof(ecash_settings));
    memset(&ecash_dispenser_errors, 0, sizeof(ecash_dispenser_errors));
    memset(&jcm_dispenser_errors, 0, sizeof(jcm_dispenser_errors));
    memset(&dispenser_receive, 0, sizeof(dispenser_receive));
    memset(&jcm_settings, 0, sizeof(jcm_settings));
    memset(&puloon_settings, 0, sizeof(puloon_settings));

    jcm_settings.sub.response_received = true;
    hDispenserSerialPort = INVALID_HANDLE_VALUE;
    dispenser_time_check = time(NULL);
    diebold_message_id = 0;
    jcm_soft_reset_sent = FALSE;
    dispenser_payout_complete = TRUE;
	pulloon_casette1_low = FALSE;
	pulloon_casette2_low = FALSE;
    puloon_settings.sub.response_received = FALSE;

    if (GetFileAttributes(DISPENSER_VALUES) == 0xFFFFFFFF)
    {
        memset(&file_dispenser_values, 0, sizeof(file_dispenser_values));
        fileWrite(DISPENSER_VALUES, 0, &file_dispenser_values, sizeof(file_dispenser_values));
    }
    else
        fileRead(DISPENSER_VALUES, 0, &file_dispenser_values, sizeof(file_dispenser_values));

    memcpy(&memory_dispenser_values, &file_dispenser_values.memory, sizeof(memory_dispenser_values));

    if (GetFileAttributes(DIEBOLD_DISPENSER_KEY) == 0xFFFFFFFF)
    {
        memset(&diebold_dispenser_key, 0, sizeof(diebold_dispenser_key));
        calculateSmuxDispenserKey();
        if (!fileWrite(DIEBOLD_DISPENSER_KEY, 0, &diebold_dispenser_key, sizeof(diebold_dispenser_key)))
            logMessage("CDispenserControl::CDispenserControl - error writing dispenser key file.");
    }
    else
        fileRead(DIEBOLD_DISPENSER_KEY, 0, &diebold_dispenser_key, sizeof(diebold_dispenser_key));

    // add 5 second delay before starting dispenser polling, set for all dispensers since type is unknown
    jcm_settings.sub.last_dispenser_poll_time = time(NULL) + 5;
    ecash_settings.sub.last_dispenser_poll_time = time(NULL) + 5;
    cd2000_settings.sub.last_dispenser_poll_time = time(NULL) + 5;
}

CDispenserControl::~CDispenserControl()
{
    if (hDispenserSerialPort != INVALID_HANDLE_VALUE)
        CloseHandle(hDispenserSerialPort);
}

void CDispenserControl::TxDispenserData(BYTE *data, WORD length)
{
 DWORD bytes_sent;
 CString error_message;

    if (hDispenserSerialPort != INVALID_HANDLE_VALUE)
    {
        if (!WriteFile(hDispenserSerialPort, data, length, &bytes_sent, NULL))
        {
			error_message.Format("CDispenserControl::TxDispenserData - port write error: %d.", GetLastError());
			logMessage(error_message);
            dispenser_time_check = time(NULL);
            bytes_sent = 0;
        }
        else
        {
            if (bytes_sent != length)
            {
				logMessage("CDispenserControl::TxDispenserData - bytes lost during send");
				dispenser_time_check = time(NULL);
            }
        }

        if (bytes_sent)
            theApp.SendDispenserDataForDisplay(data, (WORD)bytes_sent, true);
    }
}

void CDispenserControl::PollJcmDispenser()
{
 BYTE buffer[20];

 DISPENSER_QUEUE_STATUS dispenser_status = CheckJcmDispenserQueue();


    if (jcm_settings.sub.last_dispenser_command == JCM_POLL_COMMAND)
    {
        if (!jcm_settings.sub.response_received)
        {
            // wait 1 second between polls for response
            if (time(NULL) <= jcm_settings.sub.last_dispenser_poll_time + 1)
                return;
            else
                jcm_dispenser_errors.failed_dispenser_response_to_poll++;
        }
    }
    else
        // wait 1 second between commands for response
        if (time(NULL) <= jcm_settings.sub.last_dispenser_poll_time + 1)
            return;

    if (jcm_settings.sub.last_dispenser_command != NULL_COMMAND && jcm_settings.sub.last_dispenser_command != JCM_POLL_COMMAND)
    {
        switch (jcm_settings.sub.last_dispenser_command)
        {
            case JCM_SELECT_DISPENSER:
                jcm_dispenser_errors.failed_selecting_error++;
                break;

            case JCM_DISPENSER_RESET:
                jcm_dispenser_errors.failed_resets++;
                break;

            case JCM_DISPENSER_INITIALIZE:
                jcm_dispenser_errors.failed_initializations++;
                break;

            case JCM_DISPENSER_ID:
                jcm_dispenser_errors.failed_dispenser_id++;
                break;

            case JCM_REJECT:
                jcm_dispenser_errors.failed_reject++;
                break;

            case JCM_COUNTING_AND_DISPENSING:
                jcm_dispenser_errors.failed_counting_and_dispensing++;
                logMessage("CDispenserControl::PollJcmDispenser - failed counting and dispensing.");
                break;

            default:
                jcm_dispenser_errors.failed_command_unknown++;
                break;
        }
    }

    jcm_settings.sub.last_dispenser_command = JCM_POLL_COMMAND;
    jcm_settings.sub.response_received = false;

    // poll the jcm hbp-10 dispenser
    memset(buffer, 0, sizeof(buffer));
    buffer[0] = EOT;
    buffer[1] = JCM_POLL_SA;
    buffer[2] = JCM_UA;
    buffer[3] = ENQ;
    TxDispenserData(buffer, 4);

    jcm_settings.sub.last_dispenser_poll_time = time(NULL);


    if (jcm_bill_cnt1 && (dispenser_status == BILLS_REMOVED))
    {
        // only send 10 bills at a time
        if (jcm_bill_cnt1 > 10)
        {
            //MessageWindow(false, false, "JCM DISPENSE", "** DISPENSING BILLS **\n** PLEASE WAIT **");
            if (pMessageBox)
            {
                pMessageBox->DestroyWindow();
                delete pMessageBox;
                pMessageBox = 0;
            }

            pMessageBox = new CMessageBox(false, false, "JCM DISPENSE", "** DISPENSING BILLS **\n** PLEASE WAIT **");
            pMessageBox->Create(IDD_MESSAGE_BOX_DIALOG);
            pMessageBox->ShowMalfunctionOverrideButton();
     
            SendJcmDispenseMessage(10);
            jcm_bill_cnt1 -= 10;
        }
        else
        {
            if (pMessageBox)
            {
                pMessageBox->DestroyWindow();
                delete pMessageBox;
                pMessageBox = 0;
            }
            SendJcmDispenseMessage(jcm_bill_cnt1);
            jcm_bill_cnt1 = 0;
        }
    }
}

// select and write command string to dispenser, wait for ack, ack
void CDispenserControl::SendJcmDispenserCommand(DispenserCommand *message)
{
 BYTE command_buffer[50], crc_byte, data_length;
 int iii;

    jcm_settings.sub.last_dispenser_command = message->data[0];

    command_buffer[0] = STX;                // load start of text character w/out checksum update
    command_buffer[1] = DC1;                // load DC1
    command_buffer[2] = message->data[0];   // load command code
    command_buffer[3] = 0x30;

    if (message->data[0] == JCM_COUNTING_AND_DISPENSING)
    {
        if (message->length != 2)
        {
            logMessage("CDispenserControl::SendJcmDispenserCommand - invalid message length.");
            return;
        }

        data_length = 2;

        if (message->data[1] >= 10)
        {
            command_buffer[5] = 0x31;
            command_buffer[6] = 0x30;
        }
        else
        {
            command_buffer[5] = 0x30;
            command_buffer[6] = 0x30 + message->data[1];
        }
    }
    else
        data_length = 0;

    command_buffer[4] = 0x30 + data_length;
    command_buffer[5 + data_length] = ETX;  // load end of text character with checksum update

    // calculate crc
    crc_byte = 0;
    for (iii = 1; iii < 6 + data_length; iii++)
        DISPENSER_CRC(crc_byte, command_buffer[iii]);

    command_buffer[6 + data_length] = crc_byte;

    TxDispenserData(command_buffer, 7 + data_length);
}

void CDispenserControl::ProcessJcmDispenserCommands()
{
 BYTE command_buffer[50];
 DispenserCommand message;
 EventMessage event_message;

    if (!jcm_settings.dispenser_idle)
        return;

    if (!theApp.dispenser_command_queue.empty())
    {
        // send JCM dispenser select before command
        if (jcm_settings.sub.last_dispenser_command != JCM_SELECT_DISPENSER)
        {
            // send dispenser select message
            theApp.SendDispenserDataForDisplay("Selecting JCM Dispenser.");
            jcm_settings.sub.last_dispenser_command = JCM_SELECT_DISPENSER;
            command_buffer[0] = EOT;
            command_buffer[1] = JCM_SEL_SA;
            command_buffer[2] = JCM_UA;
            command_buffer[3] = ENQ;
            TxDispenserData(command_buffer, 4);
            return;
        }

        message = theApp.dispenser_command_queue.front();
        theApp.dispenser_command_queue.pop();

        if (!message.length)
            logMessage("CDispenserControl::ProcessJcmDispenserCommands - invalid message length.");
        else
        {
            switch (message.data[0])
            {
                case JCM_DISPENSER_RESET:
                    jcm_dispenser_errors.resets++;
                    theApp.SendDispenserDataForDisplay("Resetting JCM Dispenser.");
                    SendJcmDispenserCommand(&message);
                    break;

                case JCM_DISPENSER_INITIALIZE:
                    jcm_dispenser_errors.initializations++;
                    theApp.SendDispenserDataForDisplay("Initializing JCM Dispenser.");
                    SendJcmDispenserCommand(&message);
                    break;

                case JCM_DISPENSER_ID:
                    theApp.SendDispenserDataForDisplay("Requesting JCM ROM ID.");
                    SendJcmDispenserCommand(&message);
                    break;

                case JCM_REJECT:
                    theApp.SendDispenserDataForDisplay("JCM Reject.");
                    SendJcmDispenserCommand(&message);
                    break;

                case JCM_COUNTING_AND_DISPENSING:
                    theApp.SendDispenserDataForDisplay("JCM Counting and Dispensing.");

                    memset(&event_message, 0, sizeof(event_message));
                    event_message.head.time_stamp = time(NULL);
                    event_message.head.event_type = JCM_DISPENSER_DISPENSE;
                    event_message.head.data_length = 1;
                    event_message.data[0] = message.data[1];
                    theApp.event_message_queue.push(event_message);

                    SendJcmDispenserCommand(&message);
                    break;

                default:
                    jcm_dispenser_errors.failed_command_unknown++;
                    break;
            }
        }
    }
}

void CDispenserControl::ProcessValidJcmDispenserReceive(WORD data_length)
{
 char message_buffer[100];
 char rom_id[11];
 BYTE dispenser_ack[2];
 WORD error_code, number_of_bills;
 DispenserCommand ui_message;
 EventMessage event_message;

    if (sst1 ^ dispenser_receive.data[2])
    {
        // set after effecting power on or after reset command
        if ((sst1 & 0x01) != (dispenser_receive.data[2] & 0x01))
        {
            if (dispenser_receive.data[2] & 0x01)
            {
                theApp.SendDispenserDataForDisplay("SST1: reset bit set.");
                if (reset_bit_cleared && (dispenser_receive.data[3] != JCM_DISPENSER_RESET))
                {
                    if (!jcm_soft_reset_sent)
                    {
                        theApp.ResetDispenser();
                        jcm_soft_reset_sent = TRUE;
                    }
                    else
                        jcm_soft_reset_sent = FALSE;
                }

                reset_bit_cleared = FALSE;

            }
            else
            {
                theApp.SendDispenserDataForDisplay("SST1: reset bit clear.");
                    reset_bit_cleared = TRUE;
            }
        }

        // set when command acceptance prohibited
        if ((sst1 & 0x02) != (dispenser_receive.data[2] & 0x02))
        {
            if (dispenser_receive.data[2] & 0x02)
                theApp.SendDispenserDataForDisplay("SST1: busy bit set.");
            else
                theApp.SendDispenserDataForDisplay("SST1: busy bit clear.");
        }

        // set when in error condition
        if ((sst1 & 0x04) != (dispenser_receive.data[2] & 0x04))
        {
            if (dispenser_receive.data[2] & 0x04)
                theApp.SendDispenserDataForDisplay("SST1: error bit set.");
            else
                theApp.SendDispenserDataForDisplay("SST1: error bit clear.");
        }

        sst1 = dispenser_receive.data[2];
    }

    if (sst3 ^ dispenser_receive.data[4])
    {
        // set when bills near end
        if ((sst3 & 0x02) != (dispenser_receive.data[4] & 0x02))
        {
            if (dispenser_receive.data[4] & 0x02)
                theApp.SendDispenserDataForDisplay("SST3: bills near end set.");
            else
                theApp.SendDispenserDataForDisplay("SST3: bills near end clear.");
        }

        // set when bills remain at outlet
        if ((sst3 & 0x08) != (dispenser_receive.data[4] & 0x08))
        {
            if (dispenser_receive.data[4] & 0x08)
                theApp.SendDispenserDataForDisplay("SST3: bills at outlet.");
            else
            {
                theApp.SendDispenserDataForDisplay("SST3: bills not at outlet.");

                memset(&ui_message, 0, sizeof(ui_message));
                ui_message.length = 1;
                ui_message.data[0] = JCM_BILLS_REMOVED;
                theApp.dispenser_ui_queue.push(ui_message);

				if (!jcm_bill_cnt1)
                    dispenser_payout_complete = TRUE;
            }
        }

        // set when shutter is open
        if ((sst3 & 0x10) != (dispenser_receive.data[4] & 0x10))
        {
            if (dispenser_receive.data[4] & 0x10)
                theApp.SendDispenserDataForDisplay("SST3: shutter is open.");
            else
            {
                theApp.SendDispenserDataForDisplay("SST3: shutter is closed.");
				if (!jcm_bill_cnt1)
                    dispenser_payout_complete = TRUE;
            }
        }

        sst3 = dispenser_receive.data[4];
    }

    if (sst4 ^ dispenser_receive.data[5])
    {
        // set when compensation value of the right side
        // count sensor exceeds a certain volume
        if ((sst4 & 0x01) != (dispenser_receive.data[5] & 0x01))
        {
            if (dispenser_receive.data[5] & 0x01)
                theApp.SendDispenserDataForDisplay("SST4: compensation value of the right side"
                    "count sensor exceeds a certain volume, bit set.");
            else
                theApp.SendDispenserDataForDisplay("SST4: compensation value of the right side"
                    "count sensor exceeds a certain volume, bit cleared.");
        }

        // set when compensation value of the left side
        // count sensor exceeds a certain volume
        if ((sst4 & 0x02) != (dispenser_receive.data[5] & 0x02))
        {
            if (dispenser_receive.data[5] & 0x02)
                theApp.SendDispenserDataForDisplay("SST4: compensation value of the left side"
                    "count sensor exceeds a certain volume, bit set.");
            else
                theApp.SendDispenserDataForDisplay("SST4: compensation value of the left side"
                    "count sensor exceeds a certain volume, bit cleared.");
        }

        // set when reject operation is made
        if ((sst4 & 0x04) != (dispenser_receive.data[5] & 0x04))
        {
            if (dispenser_receive.data[5] & 0x04)
                theApp.SendDispenserDataForDisplay("SST4: reject operation made.");
            else
                theApp.SendDispenserDataForDisplay("SST4: no reject operation made.");
        }

        sst4 = dispenser_receive.data[5];
    }

    if ((sst1 & 0x02) || (sst1 & 0x04) || (sst3 & 0x08) || (sst3 & 0x10))
        jcm_settings.dispenser_idle = false;
    else
        jcm_settings.dispenser_idle = true;

    error_code = 0;

    switch (dispenser_receive.data[3])
    {
        case JCM_POLL_RESPONSE:
            theApp.SendDispenserDataForDisplay("Response(poll): ");
            return;

        case JCM_DISPENSER_RESET:
            if (dispenser_receive.data[2] & 0x01)
            {
                theApp.SendDispenserDataForDisplay("Response(reset): reset bit set.");
                jcm_soft_reset_sent = FALSE;
            }
            else
                theApp.SendDispenserDataForDisplay("Response(reset): reset bit clear.");
            break;

        case JCM_DISPENSER_INITIALIZE:
            if (data_length && dispenser_receive.data[8] >= 0x30 && dispenser_receive.data[9] >= 0x30)
                error_code = (WORD)((dispenser_receive.data[8] - 0x30) * 10 + dispenser_receive.data[9] - 0x30);

            theApp.SendDispenserDataForDisplay("Response(initialize).");
            break;

        case JCM_DISPENSER_ID:
            if (dispenser_receive.data[18] >= 0x30 && dispenser_receive.data[19] >= 0x30)
                error_code = (WORD)((dispenser_receive.data[18] - 0x30) * 10 + dispenser_receive.data[19] - 0x30);

            memset(rom_id, 0, sizeof(rom_id));
            memcpy(rom_id, &dispenser_receive.data[8], 10);
            sprintf_s(message_buffer, sizeof(message_buffer), "Response(ROM ID): %s.", rom_id);
            theApp.SendDispenserDataForDisplay(message_buffer);
            break;

        case JCM_REJECT:
            if (dispenser_receive.data[10] >= 0x30 && dispenser_receive.data[11] >= 0x30)
                error_code = (WORD)((dispenser_receive.data[10] - 0x30) * 10 + dispenser_receive.data[11] - 0x30);

            if (dispenser_receive.data[8] >= 0x30 && dispenser_receive.data[9] >= 0x30)
                number_of_bills = (WORD)((dispenser_receive.data[8] - 0x30) * 10 + dispenser_receive.data[9] - 0x30);
            else
                number_of_bills = 0;

            memset(&event_message, 0, sizeof(event_message));
            event_message.head.time_stamp = time(NULL);
            event_message.head.event_type = JCM_REJECT_REPLY;
            event_message.head.data_length = 4;
            memcpy(event_message.data, &dispenser_receive.data[8], event_message.head.data_length);
            theApp.event_message_queue.push(event_message);

            sprintf_s(message_buffer, sizeof(message_buffer), "Response(reject): %d.", number_of_bills);
            theApp.SendDispenserDataForDisplay(message_buffer);

            while (number_of_bills > 0)
            {
                memset(&ui_message, 0, sizeof(ui_message));
                ui_message.length = 2;
                ui_message.data[0] = JCM_REJECT;

                if (number_of_bills > 0x00FF)
                    ui_message.data[1] = 0xFF;
                else
                    ui_message.data[1] = (BYTE)number_of_bills;

                number_of_bills -= ui_message.data[1];

                theApp.dispenser_ui_queue.push(ui_message);
            }
            break;

        case JCM_COUNTING_AND_DISPENSING:
            if (dispenser_receive.data[10] >= 0x30 && dispenser_receive.data[11] >= 0x30)
                error_code = (WORD)((dispenser_receive.data[10] - 0x30) * 10 + dispenser_receive.data[11] - 0x30);

            if (dispenser_receive.data[8] >= 0x30 && dispenser_receive.data[9] >= 0x30)
                number_of_bills = (WORD)((dispenser_receive.data[8] - 0x30) * 10 + dispenser_receive.data[9] - 0x30);
            else
                number_of_bills = 0;

            sprintf_s(message_buffer, sizeof(message_buffer), "Response(counting and dispensing): %d bills.", number_of_bills);
            theApp.SendDispenserDataForDisplay(message_buffer);

            memset(&event_message, 0, sizeof(event_message));
            event_message.head.time_stamp = time(NULL);
            event_message.head.event_type = JCM_DISPENSER_DISPENSE_REPLY;
            event_message.head.data_length = 4;
            memcpy(event_message.data, &dispenser_receive.data[8], event_message.head.data_length);
            theApp.event_message_queue.push(event_message);
            break;

        default:
            sprintf_s(message_buffer, sizeof(message_buffer), "Dispenser::ProcessValidJcmDispenserReceive - Unknown JCM command response code(0x%X).",
                dispenser_receive.data[3]);
            logMessage(message_buffer);
            theApp.SendDispenserDataForDisplay(message_buffer);
            break;
    }

    // send ack
    memset(dispenser_ack, ACK, 2);
    TxDispenserData(dispenser_ack, 2);

    if (error_code)
    {
        memset(&ui_message, 0, sizeof(ui_message));
        ui_message.length = 2;
        ui_message.data[0] = JCM_ERROR_CONDITION;
        ui_message.data[1] = (BYTE)error_code;
        theApp.dispenser_ui_queue.push(ui_message);

        memset(message_buffer, 0, sizeof(message_buffer));
        sprintf_s(message_buffer, sizeof(message_buffer), "Dispenser::ProcessValidJcmDispenserReceive - ERROR %d", error_code);
        logMessage(message_buffer);
    }
}

void CDispenserControl::ProcessJcmDispenserReceive()
{
 BYTE crc_byte;
 WORD data_length;
 int iii;

    if (dispenser_receive.index >= 2)
    {
        switch (dispenser_receive.data[0])
        {
            case NAK:
                if (dispenser_receive.data[1] == NAK)
                    jcm_dispenser_errors.nak_received++;
                else
                    jcm_dispenser_errors.failed_nak_message++;

                jcm_settings.sub.last_dispenser_command = NULL_COMMAND;
                break;

            case ACK:
                if (dispenser_receive.data[1] == ACK)
                {
                    if (jcm_settings.sub.last_dispenser_command == JCM_SELECT_DISPENSER)
                        ProcessJcmDispenserCommands();
                    else
                        jcm_settings.sub.last_dispenser_command = NULL_COMMAND;
                }
                else
                    jcm_dispenser_errors.failed_ack_message++;
                break;

            case EOT:
                if (dispenser_receive.data[1] == EOT)
                    ProcessJcmDispenserCommands();
                else
                    jcm_dispenser_errors.failed_eot_message++;
                break;

            case STX:
                if (dispenser_receive.index < 10)
                    return;
                else
                {
                    // check station address
                    if (dispenser_receive.data[1] != JCM_POLL_SA)
                        jcm_dispenser_errors.failed_device_address++;
                    else
                    {
                        if (dispenser_receive.data[6] < 0x30 || dispenser_receive.data[7] < 0x30)
                            jcm_dispenser_errors.failed_message_length++;
                        else
                        {
                            // length sent in ascii format
                            data_length = (WORD)((dispenser_receive.data[6] - 0x30) * 10 + dispenser_receive.data[7] - 0x30);

                            if (dispenser_receive.index < data_length + 10)
                                return; // full message has not been received yet
                            else
                            {
                                // calculate crc
                                crc_byte = 0;
                                for (iii=1; iii < data_length + 9; iii++)
                                    DISPENSER_CRC(crc_byte, dispenser_receive.data[iii]);

                                // check crc
                                if (crc_byte != dispenser_receive.data[data_length + 9])
                                    jcm_dispenser_errors.failed_crc++;
                                else
                                    ProcessValidJcmDispenserReceive(data_length);
                            }
                        }
                    }
                }
                break;

            default:
                jcm_dispenser_errors.unknown_message_type++;
                logMessage("CDispenserControl::ProcessJcmDispenserReceive - unknown JCM message.");
                break;
        }

        memset(&dispenser_receive, 0, sizeof(dispenser_receive));
        jcm_settings.sub.response_received = true;
    }
}

void CDispenserControl::FailedEcashDispenserCommand()
{
    switch (ecash_settings.sub.last_dispenser_command)
    {
        case NULL_COMMAND:
            break;

        case ECASH_INITIALIZATION:
            ecash_dispenser_errors.failed_initializations++;
            break;

        case ECASH_DISPENSING:
            ecash_dispenser_errors.failed_dispensing++;
            logMessage("CDispenserControl::FailedEcashDispenserCommand - failed ecash dispense.");
            break;

        case ECASH_SENSOR_POLLING:
            ecash_dispenser_errors.failed_sensor_polling++;
            break;

        case ECASH_CDU_VERSION_READ:
            ecash_dispenser_errors.failed_cdu_version_read++;
            break;

        case ECASH_TEST_DISPENSING:
            ecash_dispenser_errors.failed_test_dispensing++;
            break;

        case ECASH_NOTE_INDEX_SETTING:
            ecash_dispenser_errors.failed_note_index_settings++;
            break;

        case ECASH_NATION_AND_NUMBER_OF_INSTALLED_CASSETTES:
            ecash_dispenser_errors.failed_nation_and_number_of_installed_cassettes++;
            break;

        default:
            ecash_dispenser_errors.failed_command_unknown++;
            break;
    }

    ecash_settings.sub.last_dispenser_command = NULL_COMMAND;
}

void CDispenserControl::FailedCd2000DispenserCommand()
{
    switch (cd2000_settings.sub.last_dispenser_command)
    {
        case NULL_COMMAND:
            break;

        case ECASH_INITIALIZATION:
            cd2000_dispenser_errors.failed_initializations++;
            break;

        case ECASH_DISPENSING:
            cd2000_dispenser_errors.failed_dispensing++;
            logMessage("CDispenserControl::FailedCd2000DispenserCommand - failed cd2000 dispense.");
            break;

        case ECASH_SENSOR_POLLING:
            cd2000_dispenser_errors.failed_sensor_polling++;
            break;

        case ECASH_TEST_DISPENSING:
            cd2000_dispenser_errors.failed_test_dispensing++;
            break;

        default:
            cd2000_dispenser_errors.failed_command_unknown++;
            break;
    }

    cd2000_settings.sub.last_dispenser_command = NULL_COMMAND;
}

void CDispenserControl::ProcessEcashDispenserCommands()
{
 BYTE buffer[50];
 int iii;
 DispenserCommand message;
 EventMessage event_message;

    if (!theApp.dispenser_command_queue.empty())
    {
        message = theApp.dispenser_command_queue.front();
        theApp.dispenser_command_queue.pop();

        switch (message.data[0])
        {
            case ECASH_INITIALIZATION:
                theApp.SendDispenserDataForDisplay("Initializing Ecash Dispenser.");
                break;

            case ECASH_SENSOR_POLLING:
                theApp.SendDispenserDataForDisplay("Ecash Sensor Polling.");
                break;

            case ECASH_CDU_VERSION_READ:
                theApp.SendDispenserDataForDisplay("Ecash CDU Version Read.");
                break;

            case ECASH_DISPENSING:
                theApp.SendDispenserDataForDisplay("Ecash Dispensing.");

                memset(&event_message, 0, sizeof(event_message));
                event_message.head.time_stamp = time(NULL);
                event_message.head.event_type = ECASH_DISPENSER_DISPENSE;
                event_message.head.data_length = 2;
                event_message.data[0] = message.data[1];
                event_message.data[1] = message.data[2];
                theApp.event_message_queue.push(event_message);
                break;

            case ECASH_TEST_DISPENSING:
                theApp.SendDispenserDataForDisplay("Ecash Test Dispensing.");
                break;

            case ECASH_NOTE_INDEX_SETTING:
                theApp.SendDispenserDataForDisplay("Ecash Note Index Setting.");
                break;

            case ECASH_NATION_AND_NUMBER_OF_INSTALLED_CASSETTES:
                theApp.SendDispenserDataForDisplay("Ecash Nation and Number of Installed Cassettes.");
                break;

            default:
                theApp.SendDispenserDataForDisplay("Unknown Ecash Command.");
                return;
        }

        memset(buffer, 0, sizeof(buffer));
        buffer[0] = STX;
        memcpy(&buffer[1], &message.length, 2);
        memcpy(&buffer[3], message.data, message.length);
        buffer[message.length + 3] = ETX;

        for (iii=1; iii <= message.length + 3; iii++)
            DISPENSER_CRC(buffer[message.length + 4], buffer[iii]);

        ecash_settings.sub.last_dispenser_command = message.data[0];
        ecash_settings.sub.last_dispenser_poll_time = time(NULL);
        TxDispenserData(buffer, message.length + 5);
    }
}

void CDispenserControl::ProcessCd2000DispenserCommands()
{
 BYTE buffer[50];
 int iii;
 DispenserCommand message;
 EventMessage event_message;

    if (!theApp.dispenser_command_queue.empty())
    {
        message = theApp.dispenser_command_queue.front();
        theApp.dispenser_command_queue.pop();

        switch (message.data[0])
        {
            case CD2000_INITIALIZATION:
                theApp.SendDispenserDataForDisplay("Initializing CD2000 Dispenser.");
                break;

            case CD2000_SENSOR_POLLING:
                theApp.SendDispenserDataForDisplay("CD2000 Sensor Polling.");
                break;

            case CD2000_DISPENSING:
                theApp.SendDispenserDataForDisplay("CD2000 Dispensing.");

                memset(&event_message, 0, sizeof(event_message));
                event_message.head.time_stamp = time(NULL);
                event_message.head.event_type = CD2000_DISPENSER_DISPENSE;
                event_message.head.data_length = 2;
                event_message.data[0] = message.data[1];
                event_message.data[1] = message.data[2];
                theApp.event_message_queue.push(event_message);
                break;

            case CD2000_TEST_DISPENSING:
                theApp.SendDispenserDataForDisplay("CD2000 Test Dispensing.");
                break;

            default:
                theApp.SendDispenserDataForDisplay("Unknown CD2000 Command.");
                return;
        }

        memset(buffer, 0, sizeof(buffer));
        buffer[0] = STX;
        memcpy(&buffer[1], &message.length, 2);
        memcpy(&buffer[3], message.data, message.length);
        buffer[message.length + 3] = ETX;

        for (iii=1; iii <= message.length + 3; iii++)
            DISPENSER_CRC(buffer[message.length + 4], buffer[iii]);

        cd2000_settings.sub.last_dispenser_command = message.data[0];
        cd2000_settings.sub.last_dispenser_poll_time = time(NULL);
        TxDispenserData(buffer, message.length + 5);
    }
}

void CDispenserControl::PollEcashDispenser()
{
	if (!ecash_settings.sub.last_dispenser_command)
		ProcessEcashDispenserCommands();
	else if (time(NULL) > ecash_settings.sub.last_dispenser_poll_time + ECASH_COMMAND_TIMEOUT_IN_SECONDS)
	{
		FailedEcashDispenserCommand();
		theApp.ResetDispenser();
	}
}

void CDispenserControl::PollCd2000Dispenser()
{
	if (!cd2000_settings.sub.last_dispenser_command)
		ProcessCd2000DispenserCommands();
	else if (time(NULL) > cd2000_settings.sub.last_dispenser_poll_time + CD2000_COMMAND_TIMEOUT_IN_SECONDS)
	{
		FailedCd2000DispenserCommand();
		theApp.ResetDispenser();
	}
}

void CDispenserControl::ProcessValidEcashDispenserReceive(BYTE *data)
{
 WORD *length = (WORD*)data;
 DispenserCommand message;
 EventMessage event_message;

    memset(&message, 0, sizeof(message));

    switch (ecash_settings.sub.last_dispenser_command)
    {
        case ECASH_NATION_AND_NUMBER_OF_INSTALLED_CASSETTES:
            message.length = 5;
            message.data[0] = ECASH_NOTE_INDEX_SETTING;
            message.data[1] = 0x26; // set denominatin of cassette 1, $20
            message.data[2] = 0x28; // set denominatin of cassette 2, $100
            break;

        case ECASH_NOTE_INDEX_SETTING:
            message.length = 2;
            message.data[0] = ECASH_INITIALIZATION;
            message.data[1] = '0';
            break;

        case ECASH_INITIALIZATION:
            message.length = 2;
            message.data[0] = ECASH_CDU_VERSION_READ;
            message.data[1] = '0';
            break;

        case ECASH_DISPENSING:
        case ECASH_TEST_DISPENSING:
        case ECASH_CDU_VERSION_READ:
            message.length = 1;
            message.data[0] = ECASH_SENSOR_POLLING;
            break;
    }

    if (message.length)
        theApp.dispenser_command_queue.push(message);

    if (*length >= 16)
    {
        ecash_settings.message_status = data[2];
        memcpy(ecash_settings.sensor_status, &data[3], 13);

        switch (data[16])
        {
            case ECASH_INITIALIZATION:
                memcpy(ecash_settings.error_code, &data[18], 5);
                break;

            case ECASH_CDU_VERSION_READ:
                memcpy(ecash_settings.dispenser_mode, &data[18], 3);
                ecash_settings.country_code = data[21];
                if (data[22] >= 0x30)
                    ecash_settings.number_of_cassettes = data[22] - 0x30;
                memcpy(ecash_settings.version, &data[25], 6);
                memcpy(ecash_settings.notes_index, &data[31], 3);
                break;

            case ECASH_DISPENSING:
            case ECASH_TEST_DISPENSING:
                memcpy(ecash_settings.error_code, &data[18], 5);

                memset(&message, 0, sizeof(message));
                message.length = 3;
                message.data[0] = data[16]; // Ecash command
                message.data[1] = data[31]; // cassette 1 reject notes ($20)
                message.data[2] = data[44]; // cassette 2 reject notes ($100)
                theApp.dispenser_ui_queue.push(message);

                memset(&event_message, 0, sizeof(event_message));
                event_message.head.time_stamp = time(NULL);
                event_message.head.event_type = ECASH_DISPENSER_DISPENSE_REPLY;
                event_message.head.data_length = *length - 21;
                memcpy(event_message.data, &data[23], event_message.head.data_length);
                theApp.event_message_queue.push(event_message);
                break;
        }
    }

    ecash_settings.sub.last_dispenser_command = NULL_COMMAND;
}

void CDispenserControl::ProcessValidCd2000DispenserReceive(BYTE *data)
{
 WORD *length = (WORD*)data;
 DispenserCommand message;
 EventMessage event_message;

    memset(&message, 0, sizeof(message));

    switch (cd2000_settings.sub.last_dispenser_command)
    {
        case CD2000_DISPENSING:
        case CD2000_TEST_DISPENSING:
            message.length = 1;
            message.data[0] = CD2000_SENSOR_POLLING;
            break;
    }

    if (message.length)
        theApp.dispenser_command_queue.push(message);

    if (*length >= 16)
    {
        cd2000_settings.message_status = data[2];
        memcpy(cd2000_settings.sensor_status, &data[3], 13);

        switch (data[16])
        {
            case CD2000_INITIALIZATION:
                memcpy(cd2000_settings.error_code, &data[18], 5);
                break;

            case CD2000_DISPENSING:
            case CD2000_TEST_DISPENSING:
                memcpy(cd2000_settings.error_code, &data[18], 5);

                memset(&message, 0, sizeof(message));
                message.length = 3;
                message.data[0] = data[16]; // CD2000 command
                message.data[1] = data[31]; // cassette 1 reject notes ($20)
                message.data[2] = data[44]; // cassette 2 reject notes ($100)
                theApp.dispenser_ui_queue.push(message);

                memset(&event_message, 0, sizeof(event_message));
                event_message.head.time_stamp = time(NULL);
                event_message.head.event_type = CD2000_DISPENSER_DISPENSE_REPLY;
                event_message.head.data_length = *length - 21;
                memcpy(event_message.data, &data[23], event_message.head.data_length);
                theApp.event_message_queue.push(event_message);
                break;
        }
    }

    cd2000_settings.sub.last_dispenser_command = NULL_COMMAND;

}

void CDispenserControl::ProcessEcashDispenserReceive()
{
 BYTE byte_value;
 int iii;

    switch (dispenser_receive.data[0])
    {
        case ACK:
            byte_value = ENQ;
            TxDispenserData(&byte_value, 1);
            break;

        case NAK:
            ecash_dispenser_errors.nak_received++;
            FailedEcashDispenserCommand();
            break;

        case STX:
            if (dispenser_receive.index < 2 || dispenser_receive.index < dispenser_receive.data[1] + 5)
                return;
            else
            {
                if (dispenser_receive.data[dispenser_receive.data[1] + 3] == ETX)
                {
                    // calculate crc
                    byte_value = 0;
                    for (iii=1; iii <= dispenser_receive.data[1] + 3; iii++)
                        DISPENSER_CRC(byte_value, dispenser_receive.data[iii]);

                    // check crc
                    if (byte_value != dispenser_receive.data[dispenser_receive.data[1] + 4])
                        ecash_dispenser_errors.failed_crc++;
                    else
                    {
                        byte_value = ACK;
                        TxDispenserData(&byte_value, 1);
                        ProcessValidEcashDispenserReceive(&dispenser_receive.data[1]);
                    }
                }
            }
            break;

        default:
            ecash_dispenser_errors.failed_message_header;
            break;
    }

    memset(&dispenser_receive, 0, sizeof(dispenser_receive));
}


void CDispenserControl::ProcessCd2000DispenserReceive()
{
 BYTE byte_value;
 int iii;

    switch (dispenser_receive.data[0])
    {
        case ACK:
            byte_value = ENQ;
            TxDispenserData(&byte_value, 1);
            break;

        case NAK:
            cd2000_dispenser_errors.nak_received++;
            FailedCd2000DispenserCommand();
            break;

        case STX:
            if (dispenser_receive.index < 2 || dispenser_receive.index < dispenser_receive.data[1] + 5)
                return;
            else
            {
                if (dispenser_receive.data[dispenser_receive.data[1] + 3] == ETX)
                {
                    // calculate crc
                    byte_value = 0;
                    for (iii=1; iii <= dispenser_receive.data[1] + 3; iii++)
                        DISPENSER_CRC(byte_value, dispenser_receive.data[iii]);

                    // check crc
                    if (byte_value != dispenser_receive.data[dispenser_receive.data[1] + 4])
                        cd2000_dispenser_errors.failed_crc++;
                    else
                    {
                        byte_value = ACK;
                        TxDispenserData(&byte_value, 1);
                        ProcessValidCd2000DispenserReceive(&dispenser_receive.data[1]);
                    }
                }
            }
            break;

        default:
            cd2000_dispenser_errors.failed_message_header;
            break;
    }

    memset(&dispenser_receive, 0, sizeof(dispenser_receive));
}


void CDispenserControl::ProcessDieboldDispenserReceive()
{
 EventMessage event_message;
 BYTE byte_value[25];

    switch (dispenser_receive.data[0])
    {
        case ACK_HEADER:
            // The smux dispenser code needs to see a message after an ack before it will send the
            // bill dispense complete message.
            byte_value[0] = 0xFF;
            byte_value[1] = 0x0C;
            byte_value[2] = 0x05;
            byte_value[3] = 0x06;
            byte_value[4] = 0x24;
            byte_value[5] = 0x9C;
            TxDispenserData(byte_value, 6);
            break;

        case MESSAGE_HEADER:
            // we need to ack the bill dispense message coming from the smux bill dispenser code
            if ((dispenser_receive.data[0] == MESSAGE_HEADER) && (dispenser_receive.data[4] == DISPENSE_BILLS))
            {
                byte_value[0] = ACK_HEADER;
                byte_value[1] = dispenser_receive.data[2];
                TxDispenserData(byte_value, 2);

                // write message to the event log
                memset(&event_message, 0, sizeof(event_message));
                event_message.head.time_stamp = time(NULL);
                event_message.head.event_type = DIEBOLD_DISPENSER_DISPENSE_REPLY;
                event_message.head.data_length = 8;
                memcpy(event_message.data, &dispenser_receive.data[5], 8);
                theApp.event_message_queue.push(event_message);
            }
            break;

        default:
            break;
    }

    memset(&dispenser_receive, 0, sizeof(dispenser_receive));
}

void CDispenserControl::CheckDispenserReceive()
{
 DWORD bytes_received;
 bool receive_processed;

    if (dispenser_receive.index >= MAXIMUM_DISPENSER_RECEIVE - 1)
    {
        memset(&dispenser_receive, 0, sizeof(dispenser_receive));
        logMessage("CDispenserControl::CheckDispenserReceive - rx buffer exceeded.");
    }

    if (hDispenserSerialPort == INVALID_HANDLE_VALUE)
        dispenser_time_check = time(NULL);
    else
    {
        if (!ReadFile(hDispenserSerialPort, &dispenser_receive.data[dispenser_receive.index],
            MAXIMUM_DISPENSER_RECEIVE - dispenser_receive.index, &bytes_received, NULL))
        {
            dispenser_time_check = time(NULL);
            logMessage("CDispenserControl::CheckDispenserReceive - error reading port.");
            bytes_received = 0;
        }
        else
        {
            theApp.SendDispenserDataForDisplay(&dispenser_receive.data[dispenser_receive.index], (WORD)bytes_received, false);
            dispenser_receive.index += (WORD)bytes_received;
        }

        if (dispenser_receive.index)
        {
            switch (theApp.memory_dispenser_config.dispenser_type)
            {
                case DISPENSER_TYPE_JCM_20:
                case DISPENSER_TYPE_JCM_100:
                    ProcessJcmDispenserReceive();
                    break;

                case DISPENSER_TYPE_ECASH:
                    ProcessEcashDispenserReceive();
                    break;

                case DISPENSER_TYPE_CD2000:
                    ProcessCd2000DispenserReceive();
                    break;

                case DISPENSER_TYPE_DIEBOLD:
                    ProcessDieboldDispenserReceive();
                    break;

                case DISPENSER_TYPE_LCDM1000_20:
                case DISPENSER_TYPE_LCDM2000_20_100:
                case DISPENSER_TYPE_LCDM4000_20_100:
                    receive_processed = ProcessPuloonDispenserReceive();
                    break;
                default:
                    memset(&dispenser_receive, 0, sizeof(dispenser_receive));
                    break;
            }
        }
    }
}

void CDispenserControl::OpenDispenserSerialPort()
{
 CString message;
 COMMTIMEOUTS comm_timeouts;
 DCB comm_control_settings;

    if (!theApp.memory_dispenser_config.dispenser_type || !theApp.memory_dispenser_config.comm_port)
        dispenser_time_check = time(NULL);
    else
    {
        if (hDispenserSerialPort == INVALID_HANDLE_VALUE)
        {
            message.Format("\\\\.\\COM%u", theApp.memory_dispenser_config.comm_port);
            hDispenserSerialPort = CreateFile(message, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);

            if (hDispenserSerialPort == INVALID_HANDLE_VALUE)
            {
                message.Format("CDispenserControl::OpenDispenserSerialPort - error opening dispenser comm port %u: error %d.",
                    theApp.memory_dispenser_config.comm_port, GetLastError());
                logMessage(message);
                dispenser_time_check = time(NULL);
                return;
            }
        }

        if (!GetCommState(hDispenserSerialPort, &comm_control_settings))
        {
            message.Format("CDispenserControl::OpenDispenserSerialPort - error getting comm state on port %u: %d.",
                theApp.memory_dispenser_config.comm_port, GetLastError());
            logMessage(message);
            dispenser_time_check = time(NULL);
            return;
        }

        switch (theApp.memory_dispenser_config.dispenser_type)
        {
            case DISPENSER_TYPE_CASH_DRAWER:
                comm_control_settings.BaudRate = CBR_1200;
                comm_control_settings.ByteSize = 8;
                comm_control_settings.Parity = EVENPARITY;
                break;

            case DISPENSER_TYPE_JCM_20:
            case DISPENSER_TYPE_JCM_100:
                comm_control_settings.BaudRate = CBR_2400;
                comm_control_settings.ByteSize = 7;
                comm_control_settings.Parity = EVENPARITY;
                break;

            case DISPENSER_TYPE_ECASH:
            case DISPENSER_TYPE_CD2000:
            case DISPENSER_TYPE_DIEBOLD:
            case DISPENSER_TYPE_LCDM1000_20:
            case DISPENSER_TYPE_LCDM2000_20_100:
            case DISPENSER_TYPE_LCDM4000_20_100:
                comm_control_settings.BaudRate = CBR_9600;
                comm_control_settings.ByteSize = 8;
                comm_control_settings.Parity = NOPARITY;
                break;

            default:
            	dispenser_time_check = 0;
                return;
        }

        comm_control_settings.StopBits = ONESTOPBIT;
        comm_control_settings.DCBlength = sizeof(comm_control_settings);
        comm_control_settings.fBinary = 1;
        comm_control_settings.fParity = 0;
        comm_control_settings.fOutxCtsFlow = 0;
        comm_control_settings.fOutxDsrFlow = 0;
        comm_control_settings.fDtrControl = DTR_CONTROL_DISABLE;
        comm_control_settings.fDsrSensitivity = 0;
        comm_control_settings.fTXContinueOnXoff = 1;
        comm_control_settings.fOutX = 0;
        comm_control_settings.fInX = 0;
        comm_control_settings.fErrorChar = 0;
        comm_control_settings.fNull = 0;
        comm_control_settings.fRtsControl = RTS_CONTROL_DISABLE;
        comm_control_settings.fAbortOnError = 0;

        if (!SetCommState(hDispenserSerialPort, &comm_control_settings))
        {
            message.Format("CDispenserControl::OpenDispenserSerialPort - error setting comm state on port %u: %d.",
                theApp.memory_dispenser_config.comm_port, GetLastError());
            logMessage(message);
            dispenser_time_check = time(NULL);
            return;
        }

        if (!GetCommTimeouts(hDispenserSerialPort, &comm_timeouts))
        {
            message.Format("CDispenserControl::OpenDispenserSerialPort - error getting comm timeouts on port %u: %d.",
                theApp.memory_dispenser_config.comm_port, GetLastError());
            logMessage(message);
            dispenser_time_check = time(NULL);
            return;
        }

        comm_timeouts.ReadIntervalTimeout = 10;
        comm_timeouts.ReadTotalTimeoutMultiplier = 1;
        comm_timeouts.ReadTotalTimeoutConstant = 50;
        comm_timeouts.WriteTotalTimeoutMultiplier = 1;
        comm_timeouts.WriteTotalTimeoutConstant = 50;

        if (!SetCommTimeouts(hDispenserSerialPort, &comm_timeouts))
        {
            message.Format("CDispenserControl::OpenDispenserSerialPort - error setting comm timeouts on port %u: %d.",
                theApp.memory_dispenser_config.comm_port, GetLastError());
            logMessage(message);
            dispenser_time_check = time(NULL);
        }
        else
            dispenser_time_check = 0;
    }
    puloon_settings.sub.response_received = TRUE;

}

void CDispenserControl::ProcessCashDrawerCommands()
{
 DispenserCommand message;

    if (!theApp.dispenser_command_queue.empty())
    {
        message = theApp.dispenser_command_queue.front();
        theApp.dispenser_command_queue.pop();

        if (!message.length)
            logMessage("CDispenserControl::ProcessCashDrawerCommands - invalid message length.");
        else
        {
            switch (message.data[0])
            {
                case OPEN_CASH_DRAWER:
                    TxDispenserData((BYTE*)"ZZZZ", 4);
                    break;

                default:
                    logMessage("CDispenserControl::ProcessCashDrawerCommands - unknown cash drawer command.");
                    break;
            }
        }
    }
}

void CDispenserControl::CheckEcashDispenserQueue()
{
 DispenserCommand message;

    // empty dispenser message queue
    while (!theApp.dispenser_ui_queue.empty())
    {
        message = theApp.dispenser_ui_queue.front();
        theApp.dispenser_ui_queue.pop();

        switch (message.data[0])
        {
            case ECASH_DISPENSING:
            case ECASH_TEST_DISPENSING:
                memory_dispenser_values.reject_bills1 += message.data[2];   // cassette 2 reject notes ($100)
                memory_dispenser_values.reject_bills2 += message.data[1];   // cassette 1 reject notes ($20)
                WriteDispenserFile();
                break;
        }
    }
}

void CDispenserControl::CheckCd2000DispenserQueue()
{
 DispenserCommand message;

    // empty dispenser message queue
    while (!theApp.dispenser_ui_queue.empty())
    {
        message = theApp.dispenser_ui_queue.front();
        theApp.dispenser_ui_queue.pop();

        switch (message.data[0])
        {
            case CD2000_DISPENSING:
            case CD2000_TEST_DISPENSING:
                memory_dispenser_values.reject_bills1 += message.data[2];   // cassette 2 reject notes ($100)
                memory_dispenser_values.reject_bills2 += message.data[1];   // cassette 1 reject notes ($20)
                WriteDispenserFile();
                break;
        }
    }
}

void CDispenserControl::SendEcashDispenseMessage(WORD bill_count_100, WORD bill_count_20)
{
 WORD ecash_bill_count_100, ecash_bill_count_20;
 CString message_string, add_string;
 DispenserCommand message;

    // clear the dispenser queue
    CheckEcashDispenserQueue();

    while (bill_count_100 > 0 || bill_count_20 > 0)
    {
        if (bill_count_100 >= 40)
        {
            ecash_bill_count_100 = 40;
            ecash_bill_count_20 = 0;
            bill_count_100 -= 40;
        }
        else
        {
            if (bill_count_100)
            {
                ecash_bill_count_100 = bill_count_100;
                bill_count_100 = 0;
            }
            else
                ecash_bill_count_100 = 0;

            if (bill_count_20 + ecash_bill_count_100 > 40)
            {
                ecash_bill_count_20 = 40 - ecash_bill_count_100;
                bill_count_20 -= ecash_bill_count_20;
            }
            else
            {
                ecash_bill_count_20 = bill_count_20;
                bill_count_20 = 0;
            }
        }

        memset(&message, 0, sizeof(message));
        message.length = 5;
        message.data[0] = ECASH_DISPENSING;
        message.data[1] = (BYTE)ecash_bill_count_20;
        message.data[2] = (BYTE)ecash_bill_count_100;
        theApp.dispenser_command_queue.push(message);

        message_string = "** REMOVE BILLS FROM DISPENSER\nPRESS OK TO CONTINUE DISPENSING **\n\n";
        add_string.Format("DISPENSING $100s: %u\nDISPENSING  $20s: %u", ecash_bill_count_100, ecash_bill_count_20);
        message_string += add_string;
        MessageWindow(false, "ECASH DISPENSE", message_string);

        while (theApp.message_window_queue.empty())
            theApp.OnIdle(0);

        CheckEcashDispenserQueue();
    }

    // clear the dispenser queue
    CheckEcashDispenserQueue();
}

void CDispenserControl::SendCd2000DispenseMessage(WORD bill_count_100, WORD bill_count_20)
{
 WORD cd2000_bill_count_100, cd2000_bill_count_20;
 CString message_string, add_string;
 DispenserCommand message;

    // clear the dispenser queue
    CheckCd2000DispenserQueue();

    while (bill_count_100 > 0 || bill_count_20 > 0)
    {
        if (bill_count_100 >= 40)
        {
            cd2000_bill_count_100 = 40;
            cd2000_bill_count_20 = 0;
            bill_count_100 -= 40;
        }
        else
        {
            if (bill_count_100)
            {
                cd2000_bill_count_100 = bill_count_100;
                bill_count_100 = 0;
            }
            else
                cd2000_bill_count_100 = 0;

            if (bill_count_20 + cd2000_bill_count_100 > 40)
            {
                cd2000_bill_count_20 = 40 - cd2000_bill_count_100;
                bill_count_20 -= cd2000_bill_count_20;
            }
            else
            {
                cd2000_bill_count_20 = bill_count_20;
                bill_count_20 = 0;
            }
        }

        memset(&message, 0, sizeof(message));
        message.length = 5;
        message.data[0] = CD2000_DISPENSING;
        message.data[1] = (BYTE)cd2000_bill_count_20;
        message.data[2] = (BYTE)cd2000_bill_count_100;
        theApp.dispenser_command_queue.push(message);

        message_string = "** REMOVE BILLS FROM DISPENSER\nPRESS OK TO CONTINUE DISPENSING **\n\n";
        add_string.Format("DISPENSING $100s: %u\nDISPENSING  $20s: %u", cd2000_bill_count_100, cd2000_bill_count_20);
        message_string += add_string;
        MessageWindow(false, "CD2000 DISPENSE", message_string);

        while (theApp.message_window_queue.empty())
            theApp.OnIdle(0);

        CheckCd2000DispenserQueue();
    }

    // clear the dispenser queue
    CheckCd2000DispenserQueue();
}

void CDispenserControl::FatalJcmDispenserError(BYTE error_code)
{
 CString log_message, window_message, general_message;

    theApp.PrinterData("Dispenser Error.\nCall For Service.\nError Condition:\n");

    log_message = "CDispenserControl::FatalJcmDispenserError - ";

    switch (error_code)
    {
        case 1:
            window_message = "TIMER ERROR:\n";
            general_message = "CPU built-in timer is abnormal.";
            break;

        case 2:
            window_message = "ROM ERROR:\n";
            general_message = "ROM data is abnormal.";
            break;

        case 3:
            window_message = "RAM ERROR:\n";
            general_message = "RAM is abnormal.";
            break;

        case 10:
            window_message = "SENSOR LEVEL ERROR-R:\n";
            general_message = "Level of counting sensor R is not ";
            log_message += general_message;
            window_message += general_message + "\n";
            theApp.PrinterData(general_message.GetBuffer(0));
            theApp.PrinterData("\n");
            general_message = "within specified values.";
            break;

        case 11:
            window_message = "SENSOR LEVEL ERROR-L:\n";
            general_message = "Level of counting sensor L is not ";
            log_message += general_message;
            window_message += general_message + "\n";
            theApp.PrinterData(general_message.GetBuffer(0));
            theApp.PrinterData("\n");
            general_message = "within specified values.";
            break;

        case 12:
            window_message = "LONG BILL ERROR:\n";
            general_message = "Bill length is longer than specified value.";
            break;

        case 13:
            window_message = "SHORT BILL ERROR:\n";
            general_message = "Bill length is shorter than specified value.";
            break;

        case 14:
            window_message = "DOUBLE FEED BILL ERROR:\n";
            general_message = "Double feeding of bills detected.";
            break;

        case 15:
            window_message = "OVERRUN ERROR:\n";
            general_message = "More than specified number of bills counted.";
            break;

        case 16:
            window_message = "BILL JAM ERROR:\n";
            general_message = "Bills jammed at conveying section.";
            break;

        case 17:
            window_message = "MAIN MOTOR LOCKED:\n";
            general_message = "Locking of main motor detected.";
            break;

        case 18:
            window_message = "ABNORMAL EMITTED LIGHT:\n";
            general_message = "Abnormal amount of light emission ";
            window_message += general_message + "\n";
            log_message += general_message;
            theApp.PrinterData(general_message.GetBuffer(0));
            theApp.PrinterData("\n");
            general_message = "by counting sensor detected.";
            break;

        case 20:
            window_message = "BILL PRESSOR ERROR 1:\n";
            general_message = "Bill pressor unit could not be set.";
            break;

        case 21:
            window_message = "BILL PRESSOR ERROR 2:\n";
            general_message = "Two bill pressor position sensors ";
            window_message += general_message + "\n";
            log_message += general_message;
            theApp.PrinterData(general_message.GetBuffer(0));
            theApp.PrinterData("\n");
            general_message = "switched off at the same time.";
            break;

        case 22:
            window_message = "TABLE ERROR 1:\n";
            general_message = "Table unit could not be set.";
            break;

        case 23:
            window_message = "TABLE ERROR 2:\n";
            general_message = "Two table position sensors ";
            window_message += general_message + "\n";
            log_message += general_message;
            theApp.PrinterData(general_message.GetBuffer(0));
            theApp.PrinterData("\n");
            general_message = "switched off at the same time.";
            break;

        case 24:
            window_message = "SHUTTER ERROR:\n";
            general_message = "Shutter operation faulty.";
            break;

        case 30:
            window_message = "BILL REMAINING ERROR:\n";
            general_message = "Bill left remaining in temporary ";
            window_message += general_message + "\n";
            log_message += general_message;
            theApp.PrinterData(general_message.GetBuffer(0));
            theApp.PrinterData("\n");
            general_message = "retaining section.";
            break;

        case 31:
            window_message = "BILL JAMMED ERROR:\n";
            general_message = "Bills jammed at reject section.";
            break;

        case 32:
            window_message = "DISPENSING MOTOR LOCKING:\n";
            general_message = "Locking of dispensing motor detected.";
            break;

        case 40:
            window_message = "COMMAND ERROR:\n";
            general_message = "Commands other than normal received.";
            break;

        case 41:
            window_message = "EXCESSIVE DEMAND ERROR:\n";
            general_message = "Demand for more than 10 bills made.";
            break;

        case 42:
            window_message = "SHORT COUNT ERROR:\n";
            general_message = "Specified number of bills not dispensed.";
            break;

        case 43:
            window_message = "BILLS LEFT AT OUTLET:\n";
            general_message = "Count command received with bills ";
            window_message += general_message + "\n";
            log_message += general_message;
            theApp.PrinterData(general_message.GetBuffer(0));
            theApp.PrinterData("\n");
            general_message = "remaining in outlet.";
            break;

        default:
            window_message = "UNKNOWN ERROR CODE:\n";
            general_message.Format("Unknown Error Code(%u).", error_code);
            break;
    }

    window_message += general_message;
    theApp.PrinterData(general_message.GetBuffer(0));
    theApp.PrinterData("\n");

    log_message += general_message;
    logMessage(log_message);

    theApp.dispenserMalfunctionWithLastPaidTicket();

    MessageWindow(false, "DISPENSER ERROR!", window_message);
	while (theApp.message_window_queue.empty())
		theApp.OnIdle(0);

}

// return true when bills removed from dispenser
DISPENSER_QUEUE_STATUS CDispenserControl::CheckJcmDispenserQueue()
{
 DISPENSER_QUEUE_STATUS dispenser_status = NO_STATUS;
 DispenserCommand message;

    // empty dispenser message queue
    while (!theApp.dispenser_ui_queue.empty())
    {
        message = theApp.dispenser_ui_queue.front();
        theApp.dispenser_ui_queue.pop();

        switch (message.data[0])
        {
            case JCM_BILLS_REMOVED:
                dispenser_status = BILLS_REMOVED;
                break;

            case JCM_REJECT:
                memory_dispenser_values.reject_bills1 += message.data[1];
                WriteDispenserFile();
                dispenser_status = REJECT;
               break;

            case JCM_ERROR_CONDITION:
                FatalJcmDispenserError(message.data[1]);
                dispenser_status = FATAL_ERROR;
                break;
        }
    }

 return dispenser_status;
}

void CDispenserControl::SendJcmDispenseMessage(WORD bill_count)
{
 DispenserCommand message;
 DISPENSER_QUEUE_STATUS dispenser_status = NO_STATUS;
 bool result = FALSE;

    memset(&message, 0, sizeof(message));
    message.length = 2;
    message.data[0] = JCM_COUNTING_AND_DISPENSING;

    if (bill_count > 10)
        message.data[1] = 10;
    else
        message.data[1] = (BYTE)bill_count;

    theApp.dispenser_command_queue.push(message);

    dispenser_payout_complete = FALSE;
}

void CDispenserControl::CheckDieboldDispenserQueue (void)
{
    DispenserCommand message;
    int i;

    // empty dispenser message queue
    while (!theApp.dispenser_ui_queue.empty())
    {
        message = theApp.dispenser_ui_queue.front();
        theApp.dispenser_ui_queue.pop();

	    message.data[SMUX_DISPENSER_MESSAGE_LENGTH - 1] = 0;
	    for (i = 0; i < (SMUX_DISPENSER_MESSAGE_LENGTH - 1); i++)
	      DISPENSER_CRC(message.data[SMUX_DISPENSER_MESSAGE_LENGTH - 1], message.data[i]);

        TxDispenserData(message.data, SMUX_DISPENSER_MESSAGE_LENGTH);
    }
}

#pragma pack(1)
typedef struct
{
	unsigned char header;
	unsigned char address;
	unsigned char message_id;
	unsigned char length;
	unsigned char command;
	unsigned short number_of_bills_to_dispense1;
	unsigned char denom_hundo;
	unsigned short number_of_bills_to_dispense2;
	unsigned char denom_twenty;
	unsigned long ticket_id;
	unsigned long dispenser_key;
	unsigned char crc;
}MultipleDenominationDispenserDispenseMessage;
#pragma pack()

void CDispenserControl::SendDieboldDispenseMessage(WORD bill_count_100, WORD bill_count_20, DWORD ticket_id)
{
    WORD ecash_bill_count_100, ecash_bill_count_20;
    CString message_string, add_string;
    DispenserCommand message;
    EventMessage event_message;
    MultipleDenominationDispenserDispenseMessage* dispenser_message
                                    = (MultipleDenominationDispenserDispenseMessage*) message.data;


    while (bill_count_100 > 0 || bill_count_20 > 0)
    {
        if (bill_count_100 >= 40)
        {
            ecash_bill_count_100 = 40;
            ecash_bill_count_20 = 0;
            bill_count_100 -= 40;
        }
        else
        {
            if (bill_count_100)
            {
                ecash_bill_count_100 = bill_count_100;
                bill_count_100 = 0;
            }
            else
                ecash_bill_count_100 = 0;

            if (bill_count_20 + ecash_bill_count_100 > 40)
            {
                ecash_bill_count_20 = 40 - ecash_bill_count_100;
                bill_count_20 -= ecash_bill_count_20;
            }
            else
            {
                ecash_bill_count_20 = bill_count_20;
                bill_count_20 = 0;
            }
        }
    
        // increment the message id
        if (diebold_message_id < 0xFF)
            diebold_message_id++;
        else 
            diebold_message_id = 0;

        memset(&message, 0, sizeof(message));
        message.length = 20;

		dispenser_message->header = DISP_HEADER;
        dispenser_message->address = 0;
        dispenser_message->message_id = diebold_message_id;
        dispenser_message->length = SMUX_DISPENSER_MESSAGE_LENGTH;
        dispenser_message->command = DISPENSE_BILLS;
        dispenser_message->number_of_bills_to_dispense1 = (BYTE)ecash_bill_count_100;
        dispenser_message->denom_hundo = 100;
        dispenser_message->number_of_bills_to_dispense2 = (BYTE)ecash_bill_count_20;
        dispenser_message->denom_twenty = 20;
        dispenser_message->ticket_id = (ULONG)ticket_id;
        dispenser_message->dispenser_key = (ULONG)diebold_dispenser_key;
		dispenser_message->crc = CalculateMuxCrc(message.data, SMUX_DISPENSER_MESSAGE_LENGTH - 1);

        TxDispenserData(message.data, SMUX_DISPENSER_MESSAGE_LENGTH);

        theApp.SendDispenserDataForDisplay("Diebold Dispensing.");
        memset(&event_message, 0, sizeof(event_message));
        event_message.head.time_stamp = time(NULL);
        event_message.head.event_type = DIEBOLD_DISPENSER_DISPENSE;
        event_message.head.data_length = 14;
        memcpy(event_message.data, &message.data[5], 14);
        theApp.event_message_queue.push(event_message);

        message_string = "** REMOVE BILLS FROM DISPENSER\nPRESS OK TO CONTINUE DISPENSING **\n\n";
        add_string.Format("DISPENSING $100s: %u\nDISPENSING  $20s: %u", ecash_bill_count_100, ecash_bill_count_20);
        message_string += add_string;
        MessageWindow(false, "DIEBOLD DISPENSE", message_string);

        while (theApp.message_window_queue.empty())
            theApp.OnIdle(0);
    }
}

bool CDispenserControl::DispenseBills(DWORD ticket_id, DWORD amount, DWORD ucmc_id, BYTE mux_id)
{
    bool result = FALSE;

    current_ucmc_id = ucmc_id;
    current_mux_id = mux_id;
    current_ticket_id = ticket_id;
    current_amount = amount;

    switch (theApp.memory_dispenser_config.dispenser_type)
    {
        case DISPENSER_TYPE_ECASH:
            result = dispenseEcashBills(ticket_id, amount);
            break;
        case DISPENSER_TYPE_CD2000:
            result = dispenseCd2000Bills(ticket_id, amount);
            break;
        case DISPENSER_TYPE_JCM_20:
        case DISPENSER_TYPE_JCM_100:
            result = dispenseJcmBills(ticket_id, amount);
            break;
        case DISPENSER_TYPE_DIEBOLD:
            result = dispenseDieboldBills(ticket_id, amount);
            break;
        case DISPENSER_TYPE_LCDM1000_20:
            result = dispenseLcdm1000Bills(amount);
            break;
        case DISPENSER_TYPE_LCDM2000_20_100:
            result = dispenseLcdm2000HundredsTwentiesBills(amount);
            break;
        case DISPENSER_TYPE_LCDM4000_20_100:
            result = dispenseLcdm2000HundredsTwentiesBills(amount);
            break;
    }

 return result;
}

void CDispenserControl::SetDispenserFillInfo(IdcFillValues* fill_values)
{
	if (fill_values->impress_bill_amount_100 || fill_values->impress_bill_amount_20)
    {
        // full fill
        memory_dispenser_values.last_impress_amount1 = memory_dispenser_values.impress_amount1;
        memory_dispenser_values.last_impress_amount2 = memory_dispenser_values.impress_amount2;
        memory_dispenser_values.impress_amount1 = fill_values->impress_bill_amount_100;
        memory_dispenser_values.impress_amount2 = fill_values->impress_bill_amount_20;

        switch (theApp.memory_dispenser_config.dispenser_type)
        {
            case DISPENSER_TYPE_JCM_20:
            case DISPENSER_TYPE_LCDM1000_20:
                memory_dispenser_values.last_impress_amount1 = memory_dispenser_values.impress_amount1;
                memory_dispenser_values.last_impress_amount2 = memory_dispenser_values.impress_amount2;
                memory_dispenser_values.impress_amount1 = fill_values->impress_bill_amount_20;
                memory_dispenser_values.impress_amount2 = 0;
                break;

            case DISPENSER_TYPE_JCM_100:
                memory_dispenser_values.last_impress_amount1 = memory_dispenser_values.impress_amount1;
                memory_dispenser_values.last_impress_amount2 = memory_dispenser_values.impress_amount2;
                memory_dispenser_values.impress_amount1 = fill_values->impress_bill_amount_100;
                memory_dispenser_values.impress_amount2 = 0;
                break;
            case DISPENSER_TYPE_ECASH:
            case DISPENSER_TYPE_CD2000:
            case DISPENSER_TYPE_DIEBOLD:
            case DISPENSER_TYPE_LCDM2000_20_100:
            case DISPENSER_TYPE_LCDM4000_20_100:
                memory_dispenser_values.last_impress_amount1 = memory_dispenser_values.impress_amount1;
                memory_dispenser_values.last_impress_amount2 = memory_dispenser_values.impress_amount2;
                memory_dispenser_values.impress_amount1 = fill_values->impress_bill_amount_100;
                memory_dispenser_values.impress_amount2 = fill_values->impress_bill_amount_20;
                break;
        }

        memory_dispenser_values.dispense_total1 = 0;
        memory_dispenser_values.dispense_total2 = 0;
        memory_dispenser_values.reject_bills1 = 0;
        memory_dispenser_values.reject_bills2 = 0;
        memory_dispenser_values.dispense_grand_total = 0;
        memory_dispenser_values.tickets_amount = 0;
        memory_dispenser_values.number_tickets = 0;
    }
    else
    {
        // partial fill
        memory_dispenser_values.last_impress_amount1 = memory_dispenser_values.impress_amount1;
        memory_dispenser_values.last_impress_amount2 = memory_dispenser_values.impress_amount2;

        switch (theApp.memory_dispenser_config.dispenser_type)
        {
            case DISPENSER_TYPE_JCM_20:
            case DISPENSER_TYPE_LCDM1000_20:
                memory_dispenser_values.last_impress_amount1 = memory_dispenser_values.impress_amount1;
                memory_dispenser_values.last_impress_amount2 = memory_dispenser_values.impress_amount2;
                memory_dispenser_values.impress_amount1 += fill_values->total_20s_added;
                break;

            case DISPENSER_TYPE_JCM_100:
                memory_dispenser_values.last_impress_amount1 = memory_dispenser_values.impress_amount1;
                memory_dispenser_values.last_impress_amount2 = memory_dispenser_values.impress_amount2;
                memory_dispenser_values.impress_amount1 += fill_values->total_100s_added;
                break;
            case DISPENSER_TYPE_ECASH:
            case DISPENSER_TYPE_CD2000:
            case DISPENSER_TYPE_DIEBOLD:
            case DISPENSER_TYPE_LCDM2000_20_100:
            case DISPENSER_TYPE_LCDM4000_20_100:
                memory_dispenser_values.last_impress_amount1 = memory_dispenser_values.impress_amount1;
                memory_dispenser_values.last_impress_amount2 = memory_dispenser_values.impress_amount2;
                memory_dispenser_values.impress_amount1 += fill_values->total_100s_added;
                memory_dispenser_values.impress_amount2 += fill_values->total_20s_added;
                break;
        }
    }
    WriteDispenserFile();
}

bool CDispenserControl::dispenseEcashBills(DWORD ticket_id, DWORD amount)
{
 WORD bill_cnt1, bill_cnt2, disp_cnt1, disp_cnt2;
 DWORD breakage, hold;
 CString prt_buf;

    breakage = memory_dispenser_values.dispense_grand_total - memory_dispenser_values.tickets_amount;
    hold = amount - breakage;

        bill_cnt1 = (WORD)(hold / 10000);

        // pay last even $100 dollars in $20s
        if (bill_cnt1 > 0 && !breakage && !(amount % 10000))
            bill_cnt1--;
        bill_cnt2 = (WORD)((hold / 2000) + 1 - (bill_cnt1 * 5));

        disp_cnt1 = (WORD)((memory_dispenser_values.impress_amount1 - memory_dispenser_values.dispense_total1) / 10000);
        disp_cnt2 = (WORD)((memory_dispenser_values.impress_amount2 - memory_dispenser_values.dispense_total2) / 2000);

        // add 5 bill error margin
        if (disp_cnt1 < memory_dispenser_values.reject_bills1 + 5)
            disp_cnt1 = 0;
        else
            disp_cnt1 -= (WORD)memory_dispenser_values.reject_bills1 + 5;

        if (disp_cnt2 < memory_dispenser_values.reject_bills2 + 5)
            disp_cnt2 = 0;
        else
            disp_cnt2 -= (WORD)memory_dispenser_values.reject_bills2 + 5;

        // if $100s low
   		if (bill_cnt1 > disp_cnt1)
   		{
   			bill_cnt2 += (bill_cnt1 - disp_cnt1) * 5;
   			bill_cnt1 = disp_cnt1;
			printHundredsLow();
   		}
   		else
   		{
   			// if $20s low
   			if (bill_cnt2 > disp_cnt2)
   			{
   				// for $100 or less
   				bill_cnt1++;
                // for more than $100
   				if (bill_cnt2 > 5)
   					bill_cnt1++;
   				bill_cnt2 = 0;
				printTwentiesLow();
   			}
   		}

   		// 2nd check for amount > $8000, added $120 for either $100 or $20 dispense breakage
  		if ((bill_cnt1 * 100) + (bill_cnt2 * 20) > (theApp.memory_dispenser_config.dispenser_payout_limit + 120))
   		{
            if (!theApp.memory_settings_ini.do_not_print_dispense_info)
            {
   			    theApp.PrinterHeader(0);
   			    theApp.PrinterData("\nBILL DISPENSER ERROR\nPLEASE CALL FOR SERVICE\n");
   			    prt_buf.Format("$100 BILL COUNT: %u\n$ 20 BILL COUNT: %u\n\n", bill_cnt1, bill_cnt2);
   			    theApp.PrinterData(prt_buf.GetBuffer(0));
            }
   			return false;
   		}

        memory_dispenser_values.tickets_amount += amount;
        memory_dispenser_values.number_tickets++;
        memory_dispenser_values.dispense_total1 += bill_cnt1 * 10000;
        memory_dispenser_values.dispense_total2 += bill_cnt2 * 2000;
        memory_dispenser_values.dispense_grand_total = memory_dispenser_values.dispense_total1 + memory_dispenser_values.dispense_total2;
        WriteDispenserFile();

        if (!theApp.memory_settings_ini.do_not_print_dispense_info)
        {
   		    prt_buf.Format("DISPENSE $100: %9.2f\n", (double)(bill_cnt1 * 100));
   	        theApp.PrinterData(prt_buf.GetBuffer(0));
   		    prt_buf.Format("DISPENSE $ 20: %9.2f\n", (double)(bill_cnt2 * 20));
   	        theApp.PrinterData(prt_buf.GetBuffer(0));
        }

        SendEcashDispenseMessage(bill_cnt1, bill_cnt2);

 return true;
}

bool CDispenserControl::dispenseCd2000Bills(DWORD ticket_id, DWORD amount)
{
 WORD bill_cnt1, bill_cnt2, disp_cnt1, disp_cnt2;
 DWORD breakage, hold;
 CString prt_buf;

    breakage = memory_dispenser_values.dispense_grand_total - memory_dispenser_values.tickets_amount;
    hold = amount - breakage;

    bill_cnt1 = (WORD)(hold / 10000);

    // pay last even $100 dollars in $20s
    if (bill_cnt1 > 0 && !breakage && !(amount % 10000))
        bill_cnt1--;
    bill_cnt2 = (WORD)((hold / 2000) + 1 - (bill_cnt1 * 5));

    disp_cnt1 = (WORD)((memory_dispenser_values.impress_amount1 - memory_dispenser_values.dispense_total1) / 10000);
    disp_cnt2 = (WORD)((memory_dispenser_values.impress_amount2 - memory_dispenser_values.dispense_total2) / 2000);

    // add 5 bill error margin
    if (disp_cnt1 < memory_dispenser_values.reject_bills1 + 5)
        disp_cnt1 = 0;
    else
        disp_cnt1 -= (WORD)memory_dispenser_values.reject_bills1 + 5;

    if (disp_cnt2 < memory_dispenser_values.reject_bills2 + 5)
        disp_cnt2 = 0;
    else
        disp_cnt2 -= (WORD)memory_dispenser_values.reject_bills2 + 5;

    // if $100s low
   	if (bill_cnt1 > disp_cnt1)
   	{
   		bill_cnt2 += (bill_cnt1 - disp_cnt1) * 5;
   		bill_cnt1 = disp_cnt1;
		printHundredsLow();
   	}
   	else
   	{
   		// if $20s low
   		if (bill_cnt2 > disp_cnt2)
   		{
   			// for $100 or less
   			bill_cnt1++;
            // for more than $100
   			if (bill_cnt2 > 5)
   				bill_cnt1++;
   			bill_cnt2 = 0;
			printTwentiesLow();
   		}
   	}

   	// 2nd check for amount > $8000, added $120 for either $100 or $20 dispense breakage
  	if ((bill_cnt1 * 100) + (bill_cnt2 * 20) > (theApp.memory_dispenser_config.dispenser_payout_limit + 120))
   	{
   		theApp.PrinterHeader(0);
   		theApp.PrinterData("\nBILL DISPENSER ERROR\nPLEASE CALL FOR SERVICE\n");
   		prt_buf.Format("$100 BILL COUNT: %u\n$ 20 BILL COUNT: %u\n\n", bill_cnt1, bill_cnt2);
   		theApp.PrinterData(prt_buf.GetBuffer(0));
   		return false;
   	}

    memory_dispenser_values.tickets_amount += amount;
    memory_dispenser_values.number_tickets++;
    memory_dispenser_values.dispense_total1 += bill_cnt1 * 10000;
    memory_dispenser_values.dispense_total2 += bill_cnt2 * 2000;
    memory_dispenser_values.dispense_grand_total = memory_dispenser_values.dispense_total1 + memory_dispenser_values.dispense_total2;
    WriteDispenserFile();

   	prt_buf.Format("DISPENSE $100: %9.2f\n", (double)(bill_cnt1 * 100));
   	theApp.PrinterData(prt_buf.GetBuffer(0));
   	prt_buf.Format("DISPENSE $ 20: %9.2f\n", (double)(bill_cnt2 * 20));
   	theApp.PrinterData(prt_buf.GetBuffer(0));

        SendCd2000DispenseMessage(bill_cnt1, bill_cnt2);

 return true;
}

bool CDispenserControl::dispenseJcmBills(DWORD ticket_id, DWORD amount)
{
// WORD bill_cnt1, bill_cnt2, dispenser_multiplier, disp_cnt1, disp_cnt2;
 WORD dispenser_multiplier;
 DWORD breakage, hold;
 CString prt_buf;
 bool result = FALSE;

    breakage = memory_dispenser_values.dispense_grand_total - memory_dispenser_values.tickets_amount;
    hold = amount - breakage;


    switch (theApp.memory_dispenser_config.dispenser_type)
    {
        case DISPENSER_TYPE_JCM_20:
            dispenser_multiplier = 20;
            break;

        case DISPENSER_TYPE_JCM_100:
            dispenser_multiplier = 100;
            break;

        default:
            logMessage("CDispenserControl::DispenseBills - invalid dispenser type.");
           	theApp.PrinterData("Dispenser error. Please try again.\n");
            return false;
    }

    jcm_bill_cnt1 = (WORD)((hold / (dispenser_multiplier * 100)) + 1);

    // 2nd check for amount > $8000, added $100 for either $100 or $20 dispense breakage
   	if (jcm_bill_cnt1 * dispenser_multiplier > (theApp.memory_dispenser_config.dispenser_payout_limit + 100))
   	{
   		prt_buf.Format("\nBILL DISPENSER ERROR\nPLEASE CALL FOR SERVICE\nBILL COUNT: %u\n", jcm_bill_cnt1);
   		theApp.PrinterData(prt_buf.GetBuffer(0));
   		return false;
   	}

    memory_dispenser_values.tickets_amount += amount;
    memory_dispenser_values.number_tickets++;
    memory_dispenser_values.dispense_grand_total += dispenser_multiplier * jcm_bill_cnt1 * 100;
    WriteDispenserFile();

    prt_buf.Format("DISPENSE: %9.2f\n", (double)(dispenser_multiplier * jcm_bill_cnt1));
    theApp.PrinterData(prt_buf.GetBuffer(0));

    // only send 10 bills at a time
    if (jcm_bill_cnt1 > 10)
    {
        //MessageWindow(false, false, "JCM DISPENSE", "** DISPENSING BILLS **\n** PLEASE WAIT **");
        if (pMessageBox)
        {
            pMessageBox->DestroyWindow();
            delete pMessageBox;
            pMessageBox = 0;
        }

        pMessageBox = new CMessageBox(false, false, "JCM DISPENSE", "** DISPENSING BILLS **\n** PLEASE WAIT **");
        pMessageBox->Create(IDD_MESSAGE_BOX_DIALOG);
        pMessageBox->ShowMalfunctionOverrideButton();
 
        SendJcmDispenseMessage(10);
        jcm_bill_cnt1 -= 10;
    }
    else
    {
        SendJcmDispenseMessage(jcm_bill_cnt1);
        jcm_bill_cnt1 = 0;
    }

    return TRUE;
}

bool CDispenserControl::dispenseDieboldBills(DWORD ticket_id, DWORD amount)
{
// WORD bill_cnt1, bill_cnt2, dispenser_multiplier, disp_cnt1, disp_cnt2;
 WORD bill_cnt1, bill_cnt2, disp_cnt1, disp_cnt2;
 DWORD breakage, hold;
 CString prt_buf;

    breakage = memory_dispenser_values.dispense_grand_total - memory_dispenser_values.tickets_amount;
    hold = amount - breakage;


    bill_cnt1 = (WORD)(hold / 10000);

    // pay last even $100 dollars in $20s
    if (bill_cnt1 > 0 && !breakage && !(amount % 10000))
        bill_cnt1--;
    bill_cnt2 = (WORD)((hold / 2000) + 1 - (bill_cnt1 * 5));

    disp_cnt1 = (WORD)((memory_dispenser_values.impress_amount1 - memory_dispenser_values.dispense_total1) / 10000);
    disp_cnt2 = (WORD)((memory_dispenser_values.impress_amount2 - memory_dispenser_values.dispense_total2) / 2000);

    // add 5 bill error margin
    if (disp_cnt1 < memory_dispenser_values.reject_bills1 + 5)
        disp_cnt1 = 0;
    else
        disp_cnt1 -= (WORD)memory_dispenser_values.reject_bills1 + 5;

    if (disp_cnt2 < memory_dispenser_values.reject_bills2 + 5)
        disp_cnt2 = 0;
    else
        disp_cnt2 -= (WORD)memory_dispenser_values.reject_bills2 + 5;

    // if $100s low
   	if (bill_cnt1 > disp_cnt1)
   	{
   		bill_cnt2 += (bill_cnt1 - disp_cnt1) * 5;
   		bill_cnt1 = disp_cnt1;
		printHundredsLow();
   	}
   	else
   	{
   		// if $20s low
   		if (bill_cnt2 > disp_cnt2)
   		{
   			// for $100 or less
   			bill_cnt1++;
            // for more than $100
   			if (bill_cnt2 > 5)
   				bill_cnt1++;
   			bill_cnt2 = 0;
			printTwentiesLow();
   		}
   	}

   	// 2nd check for amount > $8000, added $120 for either $100 or $20 dispense breakage
  	if ((bill_cnt1 * 100) + (bill_cnt2 * 20) > (theApp.memory_dispenser_config.dispenser_payout_limit + 120))
   	{
   		theApp.PrinterHeader(0);
   		theApp.PrinterData("\nBILL DISPENSER ERROR\nPLEASE CALL FOR SERVICE\n");
   		prt_buf.Format("$100 BILL COUNT: %u\n$ 20 BILL COUNT: %u\n\n", bill_cnt1, bill_cnt2);
   		theApp.PrinterData(prt_buf.GetBuffer(0));
   		return false;
   	}

    memory_dispenser_values.tickets_amount += amount;
    memory_dispenser_values.number_tickets++;
    memory_dispenser_values.dispense_total1 += bill_cnt1 * 10000;
    memory_dispenser_values.dispense_total2 += bill_cnt2 * 2000;
    memory_dispenser_values.dispense_grand_total = memory_dispenser_values.dispense_total1 + memory_dispenser_values.dispense_total2;
    WriteDispenserFile();

   	prt_buf.Format("DISPENSE $100: %9.2f\n", (double)(bill_cnt1 * 100));
   	theApp.PrinterData(prt_buf.GetBuffer(0));
   	prt_buf.Format("DISPENSE $ 20: %9.2f\n", (double)(bill_cnt2 * 20));
   	theApp.PrinterData(prt_buf.GetBuffer(0));

    SendDieboldDispenseMessage(bill_cnt1, bill_cnt2, ticket_id);

    return true;
}

void CDispenserControl::WriteDispenserFile()
{
 FileDispenserValues file_dispenser_values;

    memset(&file_dispenser_values, 0, sizeof(file_dispenser_values));
    memcpy(&file_dispenser_values.memory, &memory_dispenser_values, sizeof(file_dispenser_values.memory));

    if (!fileWrite(DISPENSER_VALUES, 0, &file_dispenser_values, sizeof(file_dispenser_values)))
        logMessage("CDispenserControl::WriteDispenserFile - error writing file.");
}

void CDispenserControl::PollDispenser()
{
    switch (theApp.memory_dispenser_config.dispenser_type)
    {
        case DISPENSER_TYPE_CASH_DRAWER:
            ProcessCashDrawerCommands();
            break;

        // poll the jcm hbp-10 dispenser
        case DISPENSER_TYPE_JCM_20:
        case DISPENSER_TYPE_JCM_100:
            PollJcmDispenser();
            break;

        case DISPENSER_TYPE_ECASH:
            PollEcashDispenser();
            break;

        case DISPENSER_TYPE_CD2000:
            PollCd2000Dispenser();
            break;

        case DISPENSER_TYPE_LCDM1000_20:
        case DISPENSER_TYPE_LCDM2000_20_100:
        case DISPENSER_TYPE_LCDM4000_20_100:
            PollPuloonDispenser();
            break;
    }
}

void CDispenserControl::CheckDispenser()
{
    if (dispenser_time_check)
    {
        if (dispenser_time_check + DISPENSER_COMM_CHECK < time(NULL))
            OpenDispenserSerialPort();
    }
    else
        CheckDispenserReceive();
}

void CDispenserControl::calculateSmuxDispenserKey(void)
{
//    unsigned long temp_key;
//    char time_stamp[15];
    int random_number;
    unsigned long temp_key;
    struct tm newtime;
    __time64_t long_time;
    errno_t err;


    // Get time as 64-bit integer.
    _time64( &long_time ); 
    // Convert to local time.
    err = _localtime64_s( &newtime, &long_time ); 
    if (err)
    {
        printf("Invalid argument to _localtime64_s.");
        exit(1);
    }
	
	/*
	// time_stamp = yyy(year)mm(month)dd(day)hh(hour)mm(minute)ss(second)d(day of week)
	if (read(clock_handle, time_stamp, 15) == -1)
		return;
*/
	// month: 1-4 bits
	temp_key = ((unsigned long)newtime.tm_sec - 48) * 10 + (unsigned long)newtime.tm_min - 48;
	diebold_dispenser_key = temp_key & 0x0000000F;

	// day of week: 5-7 bits
	temp_key = ((unsigned long)newtime.tm_hour - 48) * 10 + (unsigned long)newtime.tm_mday - 48;
	temp_key <<= 4;
	diebold_dispenser_key += temp_key & 0x00000070;

	// hour: 8-12 bits
	temp_key = ((unsigned long)newtime.tm_mon - 48) * 10 + (unsigned long)newtime.tm_year - 48;
	temp_key <<= 7;
	diebold_dispenser_key += temp_key & 0x00000F80;

	// min: 13-18 bits
	temp_key = ((unsigned long)newtime.tm_wday - 48) * 10 + (unsigned long)newtime.tm_yday - 48;
	temp_key <<= 12;
	diebold_dispenser_key += temp_key & 0x0003F000;

	// sec: 19-24 bits
	temp_key = ((unsigned long)newtime.tm_sec - 48) * 10 + (unsigned long)newtime.tm_min - 48;
	temp_key <<= 18;
	diebold_dispenser_key += temp_key & 0x00FC0000;

	// random number: 24-32 bits
	random_number = rand();
	temp_key = (unsigned long)random_number;
	temp_key <<= 25;
	diebold_dispenser_key += temp_key & 0xFF000000;

}

BYTE CDispenserControl::getPuloonBCC (unsigned char* buffer, int length)
{
    BYTE bcc = 0;
    int i;

    for (i=0; i < length; i++)
        bcc ^= buffer[i];

    return bcc;   
}

void CDispenserControl::ProcessPuloonDispenserCommands()
{
    DispenserCommand message;
    bool valid_command = FALSE;
    EventMessage event_message;

    if (!theApp.dispenser_command_queue.empty())
    {
        message = theApp.dispenser_command_queue.front();
        theApp.dispenser_command_queue.pop();

        if ((message.data[1] == PULOON_COMMUNICATION_ID_4000))
        {
            switch (message.data[3])
            {
                case PULOON_PURGE_4000:
                    theApp.SendDispenserDataForDisplay(" Puloon Purge.");
                    valid_command = TRUE;
                    break;


                case PULOON_DISPENSE_4000:
                    theApp.SendDispenserDataForDisplay("Puloon Dispense.");
    /*
                    memset(&event_message, 0, sizeof(event_message));
                    event_message.head.time_stamp = time(NULL);
                    event_message.head.event_type = PULOON_DISPENSER_DISPENSE;
                    event_message.head.data_length = 2;
                    event_message.data[0] = message.data[4];
                    event_message.data[1] = message.data[5];
                    theApp.event_message_queue.push(event_message);
    */
                    valid_command = TRUE;
                    // allow 1 seconds for each bill to be dispensed
                    puloon_settings.timeout_in_seconds = 20 + (message.data[4] - 0x20) + (message.data[5] - 0x20);
                    break;

                case PULOON_STATUS_4000:
                    theApp.SendDispenserDataForDisplay("Puloon Status Check.");
                    valid_command = TRUE;
                    break;

                default:
                    theApp.SendDispenserDataForDisplay("Unknown Puloon Command.");
                    return;
            }
        }
        else
        {
            switch (message.data[3])
            {
                case PULOON_PURGE:
                    theApp.SendDispenserDataForDisplay(" Puloon Purge.");
                    valid_command = TRUE;
                    break;

                case PULOON_UPPER_DISPENSE:
                    theApp.SendDispenserDataForDisplay("Puloon Dispense.");

                    memset(&event_message, 0, sizeof(event_message));
                    event_message.head.time_stamp = time(NULL);
                    event_message.head.event_type = PULOON_DISPENSER_DISPENSE;
                    event_message.head.data_length = 2;
                    event_message.data[0] = message.data[4];
                    event_message.data[1] = message.data[5];
                    theApp.event_message_queue.push(event_message);
                    valid_command = TRUE;
                    // allow 1 seconds for each bill to be dispensed
                    puloon_settings.timeout_in_seconds = 20 + ((10 * (message.data[4] - 0x30)) + (message.data[5] - 0x30)) * 1;
                    break;

                case PULOON_LOWER_DISPENSE:
                    theApp.SendDispenserDataForDisplay("Puloon Dispense.");

                    memset(&event_message, 0, sizeof(event_message));
                    event_message.head.time_stamp = time(NULL);
                    event_message.head.event_type = PULOON_DISPENSER_DISPENSE;
                    event_message.head.data_length = 2;
                    event_message.data[0] = message.data[4];
                    event_message.data[1] = message.data[5];
                    theApp.event_message_queue.push(event_message);
                    valid_command = TRUE;
                    // allow 1 seconds for each bill to be dispensed
                    puloon_settings.timeout_in_seconds = 20 + ((10 * (message.data[4] - 0x30)) + (message.data[5] - 0x30)) * 1;
                    break;

                case PULOON_UPPER_AND_LOWER_DISPENSE:
                    theApp.SendDispenserDataForDisplay("Puloon Dispense.");
    /*
                    memset(&event_message, 0, sizeof(event_message));
                    event_message.head.time_stamp = time(NULL);
                    event_message.head.event_type = PULOON_DISPENSER_DISPENSE;
                    event_message.head.data_length = 2;
                    event_message.data[0] = message.data[4];
                    event_message.data[1] = message.data[5];
                    theApp.event_message_queue.push(event_message);
    */
                    valid_command = TRUE;
                    // allow 1 seconds for each bill to be dispensed
                    puloon_settings.timeout_in_seconds = 20 + (((10 * (message.data[4] - 0x30)) + (message.data[5] - 0x30)) +
                                                              ((10 * (message.data[6] - 0x30)) + (message.data[7] - 0x30))) * 1;
                    break;

                case PULOON_STATUS:
                    theApp.SendDispenserDataForDisplay("Puloon Status Check.");
                    valid_command = TRUE;
                    break;

                case PULOON_ROM_VERSION:
                    theApp.SendDispenserDataForDisplay("Puloon Rom Version.");
                    valid_command = TRUE;
                    break;

                case PULOON_TEST_UPPER_DISPENSE:
                    theApp.SendDispenserDataForDisplay("Puloon Test Dispensing.");
                    valid_command = TRUE;
                    break;

                default:
                    theApp.SendDispenserDataForDisplay("Unknown Puloon Command.");
                    return;
            }
        }


        if (valid_command)
        {
            memset(&dispenser_receive, 0, sizeof(dispenser_receive));
            puloon_settings.sub.ack_received = false;
            puloon_settings.sub.response_received = false;
            puloon_settings.sub.last_dispenser_poll_time = time(NULL);
            puloon_settings.sub.last_dispenser_command = message.data[3];
            puloon_settings.sub.nack_count = 0;

            TxDispenserData(message.data, message.length);
        }
    }
}

bool CDispenserControl::ProcessPuloonDispenserReceiveCommands()
{
    bool receive_processed = FALSE;
    BYTE dispenser_ack[2];
    DWORD reject_bills1 = 0;
    DWORD reject_bills2 = 0;
    BYTE dispenser_error = 0;
    CString message;

    if ((dispenser_receive.data[1] == PULOON_COMMUNICATION_ID_4000))
    {
        switch (dispenser_receive.data[3])
        {
            case PULOON_PURGE_4000:
                if (dispenser_receive.index >= PULOON_4000_PURGE_RESPONSE_MESSAGE_SIZE)
                    receive_processed = processPuloon4000PurgeReceiveCommand();
                break; 
            case PULOON_DISPENSE_4000:
                if (dispenser_receive.index >= PULOON_4000_DISPENSE_RESPONSE_MESSAGE_SIZE)
                {
                    if (dispenser_receive.data[4] >= 0x20)
                        dispenser_error = dispenser_receive.data[4] - 0x20;
                    if (dispenser_receive.data[7] >= 0x20)
                        reject_bills1 = dispenser_receive.data[7] - 0x20;
                    if (dispenser_receive.data[10] >= 0x20)
                        reject_bills2 = dispenser_receive.data[10] - 0x20;
                    receive_processed = processPuloonUpperAndLowerDispenseReceiveCommand(PULOON_4000_DISPENSE_RESPONSE_MESSAGE_SIZE,
                                                                                         dispenser_error,
                                                                                         reject_bills1,
                                                                                         reject_bills2);
                }
                break;
            case PULOON_STATUS_4000:
                if (dispenser_receive.index >= PULOON_4000_STATUS_RESPONSE_MESSAGE_SIZE)
                    receive_processed = processPuloon4000StatusReceiveCommand();
                break;
            default:
                puloon_dispenser_errors.unknown_message_type++;
                logMessage("CDispenserControl::ProcessPuloonDispenserReceiveCommands - unknown Puloon message.");
                return receive_processed;
        }
    }
    else if ((dispenser_receive.data[1] == PULOON_COMMUNICATION_ID))
    {
        // command
        switch (dispenser_receive.data[3])
        {
            case PULOON_PURGE:
                if (dispenser_receive.index >= PULOON_PURGE_RESPONSE_MESSAGE_SIZE)
                    receive_processed = processPuloonPurgeReceiveCommand();
                break;
            case PULOON_UPPER_DISPENSE:
                if (dispenser_receive.index >= PULOON_UPPER_DISPENSE_RESPONSE_MESSAGE_SIZE)
                    receive_processed = processPuloonUpperDispenseReceiveCommand();
                break;

            case PULOON_LOWER_DISPENSE:
                if (dispenser_receive.index >= PULOON_LOWER_DISPENSE_RESPONSE_MESSAGE_SIZE)
                    receive_processed = processPuloonLowerDispenseReceiveCommand();
                break;

            case PULOON_UPPER_AND_LOWER_DISPENSE:
                if (dispenser_receive.index >= PULOON_UPPER_AND_LOWER_DISPENSE_RESPONSE_MESSAGE_SIZE)
                {
                    if ((dispenser_receive.data[15] >= '0') && (dispenser_receive.data[16] >= '0'))
                        reject_bills1 = ((dispenser_receive.data[15] - '0') * 10) + (dispenser_receive.data[16] - '0');
                    if ((dispenser_receive.data[17] >= '0') && (dispenser_receive.data[18] >= '0'))
                        reject_bills2 = ((dispenser_receive.data[17] - '0') * 10) + (dispenser_receive.data[18] - '0');

                    receive_processed = processPuloonUpperAndLowerDispenseReceiveCommand(PULOON_UPPER_AND_LOWER_DISPENSE_RESPONSE_MESSAGE_SIZE,
                                                                                         dispenser_receive.data[12],
                                                                                         reject_bills1,
                                                                                         reject_bills2);
                }
                break;


            case PULOON_STATUS:
                if (dispenser_receive.index >= PULOON_STATUS_RESPONSE_MESSAGE_SIZE)
                    receive_processed = processPuloonStatusReceiveCommand();
                break;

            case PULOON_ROM_VERSION:
                if (dispenser_receive.index >= PULOON_ROM_VERSION_RESPONSE_MESSAGE_SIZE)
                    receive_processed = processPuloonRomVersionReceiveCommand();
                break;

            case PULOON_TEST_UPPER_DISPENSE:
                if (dispenser_receive.index >= PULOON_TEST_UPPER_DISPENSE_RESPONSE_MESSAGE_SIZE)
                    receive_processed = processPuloonTestDispenseReceiveCommand();
                break;

            default:
                puloon_dispenser_errors.unknown_message_type++;
                logMessage("CDispenserControl::ProcessPuloonDispenserReceiveCommands - unknown Puloon message.");
                return receive_processed;
        }
    }


    if (receive_processed)
    {
        // send ack
        memset(dispenser_ack, ACK, 1);
        TxDispenserData(dispenser_ack, 1);

        memset(&dispenser_receive, 0, sizeof(dispenser_receive));
        puloon_settings.sub.response_received = true;


        if ((dispenser_receive.data[1] == PULOON_COMMUNICATION_ID_4000))
        {
            switch (dispenser_receive.data[3])
            {
                case PULOON_DISPENSE_4000:
                    SendPuloonStatusMessage();
                    break;
            }
        }
        else if ((dispenser_receive.data[1] == PULOON_COMMUNICATION_ID))
        {
            // command
            switch (dispenser_receive.data[3])
            {
                case PULOON_UPPER_DISPENSE:
                case PULOON_LOWER_DISPENSE:
                case PULOON_UPPER_AND_LOWER_DISPENSE:
                    SendPuloonStatusMessage();
                    break;

            }
        }

        if (reject_bills1 || reject_bills2)
        {
            message.Format("CDispenserControl::ProcessPuloonDispenserReceiveCommands -  reject bills 1: %d reject bills2: %d.",
                reject_bills1, reject_bills2);
            logMessage(message);
        }

    }

    return receive_processed;
}

bool CDispenserControl::processPuloonPurgeReceiveCommand()
{
    BYTE BCC = getPuloonBCC (dispenser_receive.data, PULOON_PURGE_RESPONSE_MESSAGE_SIZE - 1);
    bool receive_processed = FALSE;
     BYTE dispenser_ack[2];

    if (BCC == dispenser_receive.data[PULOON_PURGE_RESPONSE_MESSAGE_SIZE - 1])
    {
        receive_processed = TRUE;
        dispenser_payout_complete = TRUE;
    }
    else
    {
        puloon_dispenser_errors.bcc_mismatch++;
        logMessage("CDispenserControl::processPuloonPurgeReceiveCommand - BCC error.");
        // send nack
        memset(dispenser_ack, NAK, 1);
        TxDispenserData(dispenser_ack, 1);

    }
    return receive_processed;
}

bool CDispenserControl::processPuloon4000PurgeReceiveCommand()
{
    BYTE BCC = getPuloonBCC (dispenser_receive.data, PULOON_4000_PURGE_RESPONSE_MESSAGE_SIZE - 1);
    bool receive_processed = FALSE;
     BYTE dispenser_ack[2];

    if (BCC == dispenser_receive.data[PULOON_4000_PURGE_RESPONSE_MESSAGE_SIZE - 1])
    {
        receive_processed = TRUE;
        dispenser_payout_complete = TRUE;
    }
    else
    {
        puloon_dispenser_errors.bcc_mismatch++;
        logMessage("CDispenserControl::processPuloon4000PurgeReceiveCommand - BCC error.");
        // send nack
        memset(dispenser_ack, NAK, 1);
        TxDispenserData(dispenser_ack, 1);

    }
    return receive_processed;
}


bool CDispenserControl::processPuloonDispenseError(BYTE error)
{
    CString log_message, window_message, general_message;
    bool fatal_error = FALSE;

    switch (error)
    {
        case 0x32:
            window_message = "PICKUP ERROR:\n";
            general_message = "PICKUP ERROR.";
            puloon_dispenser_errors.pickup_error++;
            fatal_error = TRUE;
            break;
        case 0x33:
            window_message = "JAM AT CHK ERROR:\n";
            general_message = "JAM AT CHK ERROR";
            puloon_dispenser_errors.jam_at_chk++;							// JAM at CHK1,2 Sensor
            fatal_error = TRUE;
            break;
        case 0x34:
            window_message = "OVERFLOW ERROR:\n";
            general_message = "OVERFLOW ERROR.";
            puloon_dispenser_errors.overflow_bill++;
            fatal_error = TRUE;
            break;
        case 0x35:
            window_message = "JAM AT EXIT ERROR:\n";
            general_message = "JAM AT EXIT ERROR.";
            puloon_dispenser_errors.jam_at_exit++;							// jam at exit sensor or EJT sensor
            fatal_error = TRUE;
            break;
        case 0x36:
            window_message = "JAM AT DIV ERROR:\n";
            general_message = "JAM AT DIV ERROR.";
            puloon_dispenser_errors.jam_at_div++;							// jam at div sensor
            fatal_error = TRUE;
            break;
        case 0x37:
            window_message = "UNDEFINED ERROR:\n";
            general_message = "UNDEFINED ERROR.";
            puloon_dispenser_errors.undefined_command++;
//            fatal_error = TRUE;
            break;
        case 0x38:
            window_message = "BILL END ERROR:\n";
            general_message = "DISPENSER EMPTY.";
            puloon_dispenser_errors.bill_end++;
            fatal_error = TRUE;
            break;
        case 0x3b:
            window_message = "NOTE REQUEST ERROR:\n";
            general_message = "NOTE REQUEST ERROR.";
            puloon_dispenser_errors.note_request_error++;
            fatal_error = TRUE;
            break;
        case 0x3c:
            window_message = "COUNT DIV EJT ERROR:\n";
            general_message = "COUNT DIV EJT ERROR.";
            puloon_dispenser_errors.counting_error1++;						// between div and ejt sensors
            fatal_error = TRUE;
            break;
        case 0x3d:
            window_message = "COUNT EJT EXIT ERROR:\n";
            general_message = "COUNT EJT EXIT ERROR.";
            puloon_dispenser_errors.counting_error2++;						// between ejt and exit sensors
            fatal_error = TRUE;
            break;
        case 0x3f:
            window_message = "REJECT TRAY ERROR:\n";
            general_message = "REJECT TRAY ERROR.";
            puloon_dispenser_errors.reject_tray++;							// reject tray is not recognized
            fatal_error = TRUE;
            break;
        case 0x41:
            window_message = "MOTOR STOP ERROR:\n";
            general_message = "MOTOR STOP ERROR.";
            puloon_dispenser_errors.motor_stop++;
            fatal_error = TRUE;
            break;
        case 0x42:
            window_message = "JAM AT DIV ERROR:\n";
            general_message = "JAM AT DIV ERROR.";
            puloon_dispenser_errors.jam_div++;								// jam at div sensor
            fatal_error = TRUE;
            break;
        case 0x43:
            window_message = "TIMEOUT DIV EJT ERROR:\n";
            general_message = "TIMEOUT DIV EJT ERROR.";
            puloon_dispenser_errors.timeout++;								// from div sensor to ejt sensor
            fatal_error = TRUE;
            break;
        case 0x44:
            window_message = "OVER REJECT ERROR:\n";
            general_message = "OVER REJECT ERROR.";
            puloon_dispenser_errors.over_reject++;
            fatal_error = TRUE;
            break;
        case 0x45:
            window_message = "CASSETTE ERROR:\n";
            general_message = "CASSETTE ERROR.";
            puloon_dispenser_errors.cassette_not_recognized++;
            fatal_error = TRUE;
            break;
        case 0x47:
            window_message = "DISPENSE TIMOUT ERROR:\n";
            general_message = "DISPENSE TIMOUT ERROR.";
            puloon_dispenser_errors.dispensing_timeout++;
            fatal_error = TRUE;
            break;
        case 0x49:
            window_message = "DIVERTER SOL ERROR:\n";
            general_message = "DIVERTER SOL ERROR.";
            puloon_dispenser_errors.diverter_error++;						// diverter solenoid or SOL Sensor error
            fatal_error = TRUE;
            break;
        case 0x4a:
            window_message = "SOL SENSOR ERROR:\n";
            general_message = "SOL SENSOR ERROR.";
            puloon_dispenser_errors.sol_sensor_error++;
            fatal_error = TRUE;
            break;
        case 0x4e:
            window_message = "JAM AT DIV ERROR:\n";
            general_message = "JAM AT DIV ERROR.";
            puloon_dispenser_errors.purge_error++;
            fatal_error = TRUE;
            break;

    }

    if (fatal_error)
    {
        window_message += general_message;
        theApp.PrinterData(general_message.GetBuffer(0));
        theApp.PrinterData("\n");

        log_message += general_message;
        // stop trying to pay 100 if there is a dispenser error
        pulloon_bill_cnt1 = 0;
        // stop trying to pay 20 if there is a dispenser error
        pulloon_bill_cnt2 = 0;
        memset(&dispenser_receive, 0, sizeof(dispenser_receive));

        MessageWindow(false, "DISPENSER ERROR!", window_message);
	    while (theApp.message_window_queue.empty())
		    theApp.OnIdle(0);
        logMessage(log_message);
    }

    return fatal_error;
}

bool CDispenserControl::processPuloon4000DispenseError(BYTE error)
{
    CString log_message, window_message, general_message;
    bool fatal_error = FALSE;

    switch (error)
    {
        case 0x01:
            window_message = "Bill Pick Up Error:\n";
            general_message = "Bill Pick Up Error.";
            puloon_dispenser_errors.pickup_error++;
            fatal_error = TRUE;
            break;
        case 0x02:
            window_message = "Jam between CHK Sensor and DVT Sensor:\n";
            general_message = "Jam between CHK Sensor and DVT Sensor";
            puloon_dispenser_errors.jam_at_chk++;							// JAM at CHK1,2 Sensor
            fatal_error = TRUE;
            break;
        case 0x03:
            window_message = "Jam between DVT Sensor and EJT Sensor:\n";
            general_message = "Jam between DVT Sensor and EJT Sensor:";
            puloon_dispenser_errors.jam_at_div++;
            fatal_error = TRUE;
            break;
        case 0x04:
            window_message = "Jam between EJT Sensor and EXIT Sensor:\n";
            general_message = "Jam between EJT Sensor and EXIT Sensor";
            puloon_dispenser_errors.jam_at_exit++;							// jam at exit sensor or EJT sensor
            fatal_error = TRUE;
            break;
        case 0x05:
            window_message = "Jam at EXIT Sensor:\n";
            general_message = "Jam at EXIT Sensor.";
            puloon_dispenser_errors.jam_at_exit++;
            fatal_error = TRUE;
            break;
        case 0x06:
            window_message = "Ejecting the note suspected as rejected:\n";
            general_message = "Ejecting the note suspected as rejected.";
//            puloon_dispenser_errors.undefined_command++;
            fatal_error = TRUE;
            break;
        case 0x07:
            window_message = "Note count mis-match on eject sensor due to unexpected reason:\n";
            general_message = "Note count mis-match on eject sensor due to unexpected reason.";
//            puloon_dispenser_errors.bill_end++;
            fatal_error = TRUE;
            break;
        case 0x08:
            window_message = "The note which should be rejected is passed on eject sensor:\n";
            general_message = "The note which should be rejected is passed on eject sensor.";
//            puloon_dispenser_errors.note_request_error++;
            fatal_error = TRUE;
            break;
        case 0x09:
            window_message = "The media length on eject sensor is too long due to slip or abnormal reason.:\n";
            general_message = "The media length on eject sensor is too long due to slip or abnormal reason..";
//            puloon_dispenser_errors.counting_error1++;
            fatal_error = TRUE;
            break;
        case 0x0A:
            window_message = "The media length on exit sensor is too long due to slip or abnormal reason.:\n";
            general_message = "The media length on exit sensor is too long due to slip or abnormal reason.";
            puloon_dispenser_errors.counting_error2++;						
            fatal_error = TRUE;
            break;
        case 0x0B:
            window_message = "Detecting notes on the path before start of pick-up:\n";
            general_message = "Detecting notes on the path before start of pick-up.";
            puloon_dispenser_errors.reject_tray++;							// reject tray is not recognized
            fatal_error = TRUE;
            break;
        case 0x0C:
            window_message = "Dispensing too many notes for one transaction:\n";
            general_message = "Dispensing too many notes for one transaction.";
            fatal_error = TRUE;
            break;
        case 0x0D:
            window_message = "Rejecting too many notes for one transaction:\n";
            general_message = "Rejecting too many notes for one transaction.";
            fatal_error = TRUE;
            break;
        case 0x0E:
            window_message = "Abnormal termination during purge operation:\n";
            general_message = "Abnormal termination during purge operation.";
            fatal_error = TRUE;
            break;
        case 0x20:
            window_message = "Detecting sensor trouble or abnormal material before start:\n";
            general_message = "Detecting sensor trouble or abnormal material before start.";
            fatal_error = TRUE;
            break;
        case 0x21:
            window_message = "Detecting sensor trouble or abnormal material before start:\n";
            general_message = "Detecting sensor trouble or abnormal material before start.";
            fatal_error = TRUE;
            break;
        case 0x22:
            window_message = "Detecting trouble of solenoid operation before dispense:\n";
            general_message = "Detecting trouble of solenoid operation before dispense.";
            fatal_error = TRUE;
            break;
        case 0x23:
            window_message = "Detecting trouble in motor or slit sensor before dispense:\n";
            general_message = "Detecting trouble in motor or slit sensor before dispense.";
            fatal_error = TRUE;
            break;
        case 0x24:
            window_message = "Detecting no cassette requested to dispense bills:\n";
            general_message = "Detecting no cassette requested to dispense bills";
            fatal_error = TRUE;
            break;
        case 0x25:
            window_message = "Detecting NEAREND status in the cassette requested to dispense:\n";
            general_message = "Detecting NEAREND status in the cassette requested to dispense.";
            fatal_error = TRUE;
            break;
        case 0x26:
            window_message = "Detecting no reject tray before start:\n";
            general_message = "Detecting no reject tray before start.";
            fatal_error = TRUE;
            break;

        case 0x29:
            window_message = "The number of the dispensed is more than that of the requested.:\n";
            general_message = "The number of the dispensed is more than that of the requested..";
            fatal_error = TRUE;
            break;

        case 0x30:
            window_message = "Recognizing abnormal command:\n";
            general_message = "Recognizing abnormal command.";
            fatal_error = TRUE;
            break;

        case 0x31:
            window_message = "Recognizing abnormal parameter on the command:\n";
            general_message = "Recognizing abnormal parameter on the command.";
            fatal_error = TRUE;
            break;

        case 0x32:
            window_message = "Not to Operate VERIFY Command after Downloading and Reset:\n";
            general_message = "Not to Operate VERIFY Command after Downloading and Reset.";
            fatal_error = TRUE;
            break;

        case 0x33:
            window_message = "Program area writing Failure:\n";
            general_message = "Program area writing Failure.";
            fatal_error = TRUE;
            break;

        case 0x34:
            window_message = "Verify Failure:\n";
            general_message = "Verify Failure.";
            fatal_error = TRUE;
            break;

        case 0x35:
            window_message = "EEPROM Write Failure:\n";
            general_message = "EEPROM Write Failure.";
            fatal_error = TRUE;
            break;

        case 0x36:
            window_message = "Check Sum Error on Writing EEPROM:\n";
            general_message = "Check Sum Error on Writing EEPROM.";
            fatal_error = TRUE;
            break;

        case 0x40:
            window_message = "During dispensing from the 2nd, 3rd or Bottom Cassette, the banknote coming from the Top Cassette is detected.:\n";
            general_message = ".";
            fatal_error = TRUE;
            break;

        case 0x41:
            window_message = "During dispensing from the 1st, 3rd or Bottom Cassette, the banknote coming from the 2nd Cassette is detected.:\n";
            general_message = "During dispensing from the 1st, 3rd or Bottom Cassette, the banknote coming from the 2nd Cassette is detected..";
            fatal_error = TRUE;
            break;

        case 0x42:
            window_message = "During dispensing from the 1st, 2nd or Bottom Cassette, the banknote coming from the 3rd Cassette is detected.:\n";
            general_message = "During dispensing from the 1st, 2nd or Bottom Cassette, the banknote coming from the 3rd Cassette is detected..";
            fatal_error = TRUE;
            break;

        case 0x43:
            window_message = "During dispensing from the 1st, 2nd or 3rd Cassette, the banknote coming from the 4th Cassette is detected.:\n";
            general_message = "During dispensing from the 1st, 2nd or 3rd Cassette, the banknote coming from the 4th Cassette is detected.";
            fatal_error = TRUE;
            break;
    }

    if (fatal_error)
    {
        window_message += general_message;
        theApp.PrinterData(general_message.GetBuffer(0));
        theApp.PrinterData("\n");

        log_message += general_message;
        // stop trying to pay 100 if there is a dispenser error
        pulloon_bill_cnt1 = 0;
        // stop trying to pay 20 if there is a dispenser error
        pulloon_bill_cnt2 = 0;
        memset(&dispenser_receive, 0, sizeof(dispenser_receive));

        MessageWindow(false, "DISPENSER ERROR!", window_message);
	    while (theApp.message_window_queue.empty())
		    theApp.OnIdle(0);

        logMessage(log_message);
    }

    return fatal_error;
}


bool CDispenserControl::processPuloonUpperDispenseReceiveCommand()
{
    BYTE BCC = getPuloonBCC (dispenser_receive.data, PULOON_UPPER_DISPENSE_RESPONSE_MESSAGE_SIZE - 1);
    DWORD reject_bills = 0;
    bool receive_processed = FALSE;
     BYTE dispenser_ack[2];
    bool twenties_low = FALSE;
    bool hundreds_low = FALSE;
    WORD disp_cnt1, disp_cnt2;
    CString message_string, add_string;
    DWORD dollars_dispensed;
    CString window_message;

    if (BCC == dispenser_receive.data[PULOON_UPPER_DISPENSE_RESPONSE_MESSAGE_SIZE - 1])
    {
        receive_processed = TRUE;
        dispenser_payout_complete = TRUE;

        if ((dispenser_receive.data[10] >= '0') && (dispenser_receive.data[11] >= '0'))
            reject_bills = ((dispenser_receive.data[10] - '0') * 10) + (dispenser_receive.data[11] - '0');

        if (reject_bills)
        {
            theApp.SendDispenserDataForDisplay("Puloon Dispense Return: reject bills.");
            // increment the reject bills
            memory_dispenser_values.reject_bills1 += reject_bills;
            WriteDispenserFile();
        }

        if (!processPuloonDispenseError(dispenser_receive.data[8]))
        {
            if (pulloon_bill_cnt1)
            {
                if (pMessageBox)
                {
                    pMessageBox->DestroyWindow();
                    delete pMessageBox;
                    pMessageBox = 0;
                }

                message_string = "** REMOVE BILLS FROM DISPENSER\nPRESS OK TO CONTINUE DISPENSING **\n\n";
                add_string.Format("TICKET ID: %u\n", current_ticket_id);
                message_string += add_string;
                add_string.Format("TICKET AMOUNT: $%7.2f\n", (double)current_amount / 100);
                message_string += add_string;
                if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_LCDM1000_20)
                    dollars_dispensed = current_bill_cnt1 * 20;
                else
                    dollars_dispensed = (current_bill_cnt1 * 100) + (current_bill_cnt2 * 20);
                add_string.Format("CASH DISPENSED: $%u\n", dollars_dispensed);
                message_string += add_string;
                add_string.Format("UCMC %u MUX/SAS %u", current_ucmc_id, current_mux_id);
                message_string += add_string;
                MessageWindow(true, "PULOON DISPENSE", message_string);
            }
            else
            {
                if (pMessageBox)
                {
                    pMessageBox->DestroyWindow();
                    delete pMessageBox;
                    pMessageBox = 0;
                }

                if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_LCDM1000_20)
                    disp_cnt1 = (WORD)((memory_dispenser_values.impress_amount1 - memory_dispenser_values.dispense_total1) / 2000) + memory_dispenser_values.reject_bills1;
                else // DISPENSER_TYPE_LCDM2000_20_100
                { 
                    disp_cnt1 = (WORD)((memory_dispenser_values.impress_amount1 - memory_dispenser_values.dispense_total1) / 10000) + memory_dispenser_values.reject_bills1;
                    disp_cnt2 = (WORD)((memory_dispenser_values.impress_amount2 - memory_dispenser_values.dispense_total2) / 2000) + memory_dispenser_values.reject_bills2;
                }

                if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_LCDM1000_20)
                {
                    if ((disp_cnt1 + memory_dispenser_values.reject_bills1) < (WORD)((memory_dispenser_values.impress_amount1 / 10 ) / 2000))
                        twenties_low = TRUE; 
                }
                else // DISPENSER_TYPE_LCDM2000_20_100
                {
                    if ((disp_cnt2 + memory_dispenser_values.reject_bills2) < (WORD)((memory_dispenser_values.impress_amount2 / 10 ) / 2000))
                        twenties_low = TRUE; 
                    if ((disp_cnt1 + memory_dispenser_values.reject_bills1) < (WORD)((memory_dispenser_values.impress_amount1 / 10 ) / 10000))
                        hundreds_low = TRUE; 
                }

                if (twenties_low && hundreds_low && (!theApp.memory_settings_ini.stratus_ip_address && !theApp.memory_settings_ini.idc_ip_address))
                {
					theApp.PrinterData("\n");
					theApp.PrinterHeader(0);
					theApp.PrinterData("\nDispenser WARNING!!!\n$20s and $100s Low.\nCall for Re-fill.\n\n");
					message_string = "** DISPENSE COMPLETE **\n** PLEASE REMOVE BILLS **\n\n** WARNING: $20s and $100s LOW - CALL FOR RE-FILL.\n\n";
                }
                else if (twenties_low && (!theApp.memory_settings_ini.stratus_ip_address && !theApp.memory_settings_ini.idc_ip_address))
                {
					theApp.PrinterData("\n");
					theApp.PrinterHeader(0);
					theApp.PrinterData("\nDispenser WARNING!!!\n$20s Low.\nCall for Re-fill.\n\n");
					message_string = "** DISPENSE COMPLETE **\n** PLEASE REMOVE BILLS **\n\n** WARNING: $20s LOW - CALL FOR RE-FILL.\n\n";
                }
                else if (hundreds_low && (!theApp.memory_settings_ini.stratus_ip_address && !theApp.memory_settings_ini.idc_ip_address))
                {
					theApp.PrinterData("\n");
					theApp.PrinterHeader(0);
					theApp.PrinterData("\nDispenser WARNING!!!\n$100s Low.\nCall for Re-fill.\n\n");
					message_string = "** DISPENSE COMPLETE **\n** PLEASE REMOVE BILLS **\n\n** WARNING: $100s LOW - CALL FOR RE-FILL.\n\n";
                }
                else
                    message_string = "** DISPENSE COMPLETE **\n** PLEASE REMOVE BILLS **\n\n";


                add_string.Format("TICKET ID: %u\n", current_ticket_id);
                message_string += add_string;
                add_string.Format("TICKET AMOUNT: $%7.2f\n", (double)current_amount / 100);
                message_string += add_string;
                if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_LCDM1000_20)
                    dollars_dispensed = current_bill_cnt1 * 20;
                else // DISPENSER_TYPE_LCDM2000_20_100
                    dollars_dispensed = (current_bill_cnt1 * 100) + (current_bill_cnt2 * 20);
                add_string.Format("CASH DISPENSED: $%u\n", dollars_dispensed);
                message_string += add_string;
                add_string.Format("UCMC %u MUX/SAS %u", current_ucmc_id, current_mux_id);
                message_string += add_string;
                MessageWindow(true, "PULOON DISPENSE", message_string);
            }
        }
        else
        {
            // stop trying to pay 100 if there is a dispenser error
            pulloon_bill_cnt1 = 0;

            memset(&dispenser_receive, 0, sizeof(dispenser_receive));

            MessageWindow(false, "DISPENSER ERROR!", window_message);
	        while (theApp.message_window_queue.empty())
		        theApp.OnIdle(0);

            MessageWindow(false, "FATAL PULOON DISPENSER ERROR", "PLEASE SERVICE DISPENSER");
		    while (theApp.message_window_queue.empty())
			    theApp.OnIdle(0);

            // fatal error
            SendPuloonPurgeMessage(); // put this at the top of the queue

        }

    }
    else
    {
        puloon_dispenser_errors.bcc_mismatch++;
        logMessage("CDispenserControl::processPuloonUpperDispenseReceiveCommand - BCC error.");
        // send nack
        memset(dispenser_ack, NAK, 1);
        TxDispenserData(dispenser_ack, 1);
    }

    return receive_processed;
}

bool CDispenserControl::processPuloonLowerDispenseReceiveCommand()
{
    BYTE BCC = getPuloonBCC (dispenser_receive.data, PULOON_LOWER_DISPENSE_RESPONSE_MESSAGE_SIZE - 1);
    DWORD reject_bills;
    bool receive_processed = FALSE;
    BYTE dispenser_ack[2];
    bool twenties_low = FALSE;
    bool hundreds_low = FALSE;
    WORD disp_cnt1, disp_cnt2;
    CString message_string, add_string;
    DWORD dollars_dispensed;
    CString window_message;

    if (BCC == dispenser_receive.data[PULOON_LOWER_DISPENSE_RESPONSE_MESSAGE_SIZE - 1])
    {
        receive_processed = TRUE;
        dispenser_payout_complete = TRUE;

        if ((dispenser_receive.data[10] >= '0') && (dispenser_receive.data[11] >= '0'))
            reject_bills = ((dispenser_receive.data[10] - '0') * 10) + (dispenser_receive.data[11] - '0');

        if (reject_bills)
        {
            theApp.SendDispenserDataForDisplay("Puloon Dispense Return: reject bills.");
            // increment the reject bills
            memory_dispenser_values.reject_bills2 += reject_bills;
            WriteDispenserFile();
        }

        if (!processPuloonDispenseError(dispenser_receive.data[8]))
        {
            if (pMessageBox)
            {
                pMessageBox->DestroyWindow();
                delete pMessageBox;
                pMessageBox = 0;
            }

            message_string = "** REMOVE BILLS FROM DISPENSER\nPRESS OK TO CONTINUE DISPENSING **\n\n";
            add_string.Format("TICKET ID: %u\n", current_ticket_id);
            message_string += add_string;
            add_string.Format("TICKET AMOUNT: $%7.2f\n", (double)current_amount / 100);
            message_string += add_string;
            dollars_dispensed = (current_bill_cnt1 * 100) + (current_bill_cnt2 * 20);
            add_string.Format("CASH DISPENSED: $%u\n", dollars_dispensed);
            message_string += add_string;
            add_string.Format("UCMC %u MUX/SAS %u", current_ucmc_id, current_mux_id);
            message_string += add_string;
            MessageWindow(true, "PULOON DISPENSE", message_string);

        }
        else
        {
            // stop trying to pay 20 if there is a dispenser error
            pulloon_bill_cnt2 = 0;

            memset(&dispenser_receive, 0, sizeof(dispenser_receive));

            MessageWindow(false, "DISPENSER ERROR!", window_message);
	        while (theApp.message_window_queue.empty())
		        theApp.OnIdle(0);

            MessageWindow(false, "FATAL PULOON DISPENSER ERROR", "PLEASE SERVICE DISPENSER");
		    while (theApp.message_window_queue.empty())
			    theApp.OnIdle(0);

            // fatal error
            SendPuloonPurgeMessage(); // put this at the top of the queue

        }

    }
    else
    {
        puloon_dispenser_errors.bcc_mismatch++;
        logMessage("CDispenserControl::processPuloonLowerDispenseReceiveCommand - BCC error.");
        // send nack
        memset(dispenser_ack, NAK, 1);
        TxDispenserData(dispenser_ack, 1);
    }

    return receive_processed;
}

bool CDispenserControl::processPuloonUpperAndLowerDispenseReceiveCommand(BYTE message_size, BYTE dispenser_error, DWORD reject_bills1, DWORD reject_bills2)
{
    BYTE BCC = getPuloonBCC (dispenser_receive.data, message_size - 1);
    bool receive_processed = FALSE;
     BYTE dispenser_ack[2];
    bool twenties_low = FALSE;
    bool hundreds_low = FALSE;
    WORD disp_cnt1, disp_cnt2;
    CString message_string, add_string;
    DWORD dollars_dispensed;
    bool dispense_error_return = FALSE;
    CString window_message;

    if (BCC == dispenser_receive.data[message_size - 1])
    {
        receive_processed = TRUE;
        dispenser_payout_complete = TRUE;

        if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_LCDM4000_20_100)
            dispense_error_return = processPuloon4000DispenseError(dispenser_error);
        else
            dispense_error_return = processPuloonDispenseError(dispenser_error);


        if (!dispense_error_return)
        {
            if (pulloon_bill_cnt2)
            {
                if (pMessageBox)
                {
                    pMessageBox->DestroyWindow();
                    delete pMessageBox;
                    pMessageBox = 0;
                }

                message_string = "** REMOVE BILLS FROM DISPENSER\nPRESS OK TO CONTINUE DISPENSING **\n\n";
                add_string.Format("TICKET ID: %u\n", current_ticket_id);
                message_string += add_string;
                add_string.Format("TICKET AMOUNT: $%7.2f\n", (double)current_amount / 100);
                message_string += add_string;
                dollars_dispensed = (current_bill_cnt1 * 100) + (current_bill_cnt2 * 20);
                add_string.Format("CASH DISPENSED: $%u\n", dollars_dispensed);
                message_string += add_string;
                add_string.Format("UCMC %u MUX/SAS %u", current_ucmc_id, current_mux_id);
                message_string += add_string;
                MessageWindow(true, "PULOON DISPENSE", message_string);

            }
            else
            {
                if (pMessageBox)
                {
                    pMessageBox->DestroyWindow();
                    delete pMessageBox;
                    pMessageBox = 0;
                }

                disp_cnt1 = (WORD)((memory_dispenser_values.impress_amount1 - memory_dispenser_values.dispense_total1) / 10000) + memory_dispenser_values.reject_bills1;
                disp_cnt2 = (WORD)((memory_dispenser_values.impress_amount2 - memory_dispenser_values.dispense_total2) / 2000) + memory_dispenser_values.reject_bills2;

                if ((disp_cnt1 + memory_dispenser_values.reject_bills1) < (WORD)((memory_dispenser_values.impress_amount1 / 10 ) / 10000))
                    hundreds_low = TRUE; 

                if ((disp_cnt2 + memory_dispenser_values.reject_bills2) < (WORD)((memory_dispenser_values.impress_amount2 / 10) / 2000))
                    twenties_low = TRUE; 

                if (hundreds_low && twenties_low)
                {
					if (!theApp.memory_settings_ini.stratus_ip_address && !theApp.memory_settings_ini.idc_ip_address)
					{
						theApp.PrinterData("\n");
						theApp.PrinterHeader(0);
						theApp.PrinterData("\nDispenser WARNING!!!\n$20s & $100s Low.\nCall for Re-fill.\n\n");

						message_string = "** DISPENSE COMPLETE **\n** PLEASE REMOVE BILLS **\n\n** WARNING: $20s & $100s LOW - CALL FOR RE-FILL.\n\n";
					}
					else
						message_string = "** DISPENSE COMPLETE **\n** PLEASE REMOVE BILLS **\n\n";

					add_string.Format("TICKET ID: %u\n", current_ticket_id);
                    message_string += add_string;
                    add_string.Format("TICKET AMOUNT: $%7.2f\n", (double)current_amount / 100);
                    message_string += add_string;
                    dollars_dispensed = (current_bill_cnt1 * 100) + (current_bill_cnt2 * 20);
                    add_string.Format("CASH DISPENSED: $%u\n", dollars_dispensed);
                    message_string += add_string;
                    add_string.Format("UCMC %u MUX/SAS %u", current_ucmc_id, current_mux_id);
                    message_string += add_string;
                    MessageWindow(true, "PULOON DISPENSE", message_string);

                }
                else if (twenties_low)
                {
					if (!theApp.memory_settings_ini.stratus_ip_address && !theApp.memory_settings_ini.idc_ip_address)
					{
						theApp.PrinterData("\n");
						theApp.PrinterHeader(0);
						theApp.PrinterData("\nDispenser WARNING!!!\n$20s Low.\nCall for Re-fill.\n\n");

						message_string = "** DISPENSE COMPLETE **\n** PLEASE REMOVE BILLS **\n\n** WARNING: $20s LOW - CALL FOR RE-FILL.\n\n";
					}
					else
						message_string = "** DISPENSE COMPLETE **\n** PLEASE REMOVE BILLS **\n\n";

					add_string.Format("TICKET ID: %u\n", current_ticket_id);
                    message_string += add_string;
                    add_string.Format("TICKET AMOUNT: $%7.2f\n", (double)current_amount / 100);
                    message_string += add_string;
                    dollars_dispensed = (current_bill_cnt1 * 100) + (current_bill_cnt2 * 20);
                    add_string.Format("CASH DISPENSED: $%u\n", dollars_dispensed);
                    message_string += add_string;
                    add_string.Format("UCMC %u MUX/SAS %u", current_ucmc_id, current_mux_id);
                    message_string += add_string;
                    MessageWindow(true, "PULOON DISPENSE", message_string);

                }
                else if (hundreds_low)
                {
					if (!theApp.memory_settings_ini.stratus_ip_address && !theApp.memory_settings_ini.idc_ip_address)
					{
						theApp.PrinterData("\n");
						theApp.PrinterHeader(0);
						theApp.PrinterData("\nDispenser WARNING!!!\n$100s Low.\nCall for Re-fill.\n\n");

						message_string = "** DISPENSE COMPLETE **\n** PLEASE REMOVE BILLS **\n\n** WARNING: $100s LOW - CALL FOR RE-FILL.\n\n";
					}
					else
						message_string = "** DISPENSE COMPLETE **\n** PLEASE REMOVE BILLS **\n\n";

					add_string.Format("TICKET ID: %u\n", current_ticket_id);
                    message_string += add_string;
                    add_string.Format("TICKET AMOUNT: $%7.2f\n", (double)current_amount / 100);
                    message_string += add_string;
                    dollars_dispensed = (current_bill_cnt1 * 100) + (current_bill_cnt2 * 20);
                    add_string.Format("CASH DISPENSED: $%u\n", dollars_dispensed);
                    message_string += add_string;
                    add_string.Format("UCMC %u MUX/SAS %u", current_ucmc_id, current_mux_id);
                    message_string += add_string;
                    MessageWindow(true, "PULOON DISPENSE", message_string);

                }
                else
                {
                    message_string = "** DISPENSE COMPLETE **\n** PLEASE REMOVE BILLS **\n\n";
                    add_string.Format("TICKET ID: %u\n", current_ticket_id);
                    message_string += add_string;
                    add_string.Format("TICKET AMOUNT: $%7.2f\n", (double)current_amount / 100);
                    message_string += add_string;
                    dollars_dispensed = (current_bill_cnt1 * 100) + (current_bill_cnt2 * 20);
                    add_string.Format("CASH DISPENSED: $%u\n", dollars_dispensed);
                    message_string += add_string;
                    add_string.Format("UCMC %u MUX/SAS %u", current_ucmc_id, current_mux_id);
                    message_string += add_string;
                    MessageWindow(true, "PULOON DISPENSE", message_string);

                }
            }
        }
        else
        {
/*
			MessageWindow(false, "DISPENSER ERROR!", window_message);
	        while (theApp.message_window_queue.empty())
		        theApp.OnIdle(0);
*/
            memset(&dispenser_receive, 0, sizeof(dispenser_receive));

            MessageWindow(false, "FATAL PULOON DISPENSER ERROR", "PLEASE SERVICE DISPENSER");
		    while (theApp.message_window_queue.empty())
			    theApp.OnIdle(0);

            // fatal error
            SendPuloonPurgeMessage(); // put this at the top of the queue

        }

        if (reject_bills1 || reject_bills2)
        {
            theApp.SendDispenserDataForDisplay("Puloon Dispense Return: reject bills.");
            // increment the reject bills
            memory_dispenser_values.reject_bills1 += reject_bills1;
            memory_dispenser_values.reject_bills2 += reject_bills2;
            WriteDispenserFile();
        }
    }

    return receive_processed;
}

bool CDispenserControl::processPuloonStatusReceiveCommand()
{
    BYTE BCC = getPuloonBCC (dispenser_receive.data, PULOON_STATUS_RESPONSE_MESSAGE_SIZE - 1);
    bool receive_processed = FALSE;
     BYTE dispenser_ack[2];
    BYTE sensor0, sensor1;

    if (BCC == dispenser_receive.data[PULOON_STATUS_RESPONSE_MESSAGE_SIZE - 1])
    {
        receive_processed = TRUE;

        sensor0 = dispenser_receive.data[6];
        pulloon_casette1_low = sensor0 & 0x40;
        sensor1 = dispenser_receive.data[7];
        pulloon_casette2_low = sensor1 & 0x20;
    }
    else
    {
        puloon_dispenser_errors.bcc_mismatch++;
        logMessage("CDispenserControl::processPuloonStatusReceiveCommand - BCC error.");
        // send nack
        memset(dispenser_ack, NAK, 1);
        TxDispenserData(dispenser_ack, 1);
    }
    return receive_processed;
}

bool CDispenserControl::processPuloon4000StatusReceiveCommand()
{
    BYTE BCC = getPuloonBCC (dispenser_receive.data, PULOON_4000_STATUS_RESPONSE_MESSAGE_SIZE - 1);
    bool receive_processed = FALSE;
     BYTE dispenser_ack[2];
    BYTE sensor0, sensor1;

    if (BCC == dispenser_receive.data[PULOON_4000_STATUS_RESPONSE_MESSAGE_SIZE - 1])
    {
        receive_processed = TRUE;

        sensor0 = dispenser_receive.data[6];
        pulloon_casette1_low = sensor0 & 0x08;
        sensor1 = dispenser_receive.data[10];
        pulloon_casette2_low = sensor1 & 0x08;
    }
    else
    {
        puloon_dispenser_errors.bcc_mismatch++;
        logMessage("CDispenserControl::processPuloon4000StatusReceiveCommand - BCC error.");
        // send nack
        memset(dispenser_ack, NAK, 1);
        TxDispenserData(dispenser_ack, 1);
    }
    return receive_processed;
}

bool CDispenserControl::processPuloonRomVersionReceiveCommand()
{
    BYTE BCC = getPuloonBCC (dispenser_receive.data, PULOON_ROM_VERSION_RESPONSE_MESSAGE_SIZE - 1);
    bool receive_processed = FALSE;
     BYTE dispenser_ack[2];

    if (BCC == dispenser_receive.data[PULOON_ROM_VERSION_RESPONSE_MESSAGE_SIZE - 1])
    {
        receive_processed = TRUE;
    }
    else
    {
        puloon_dispenser_errors.bcc_mismatch++;
        logMessage("CDispenserControl::processPuloonRomVersionReceiveCommand - BCC error.");
        // send nack
        memset(dispenser_ack, NAK, 1);
        TxDispenserData(dispenser_ack, 1);
    }

    return receive_processed;
}

bool CDispenserControl::processPuloonTestDispenseReceiveCommand()
{
    BYTE BCC = getPuloonBCC (dispenser_receive.data, PULOON_TEST_UPPER_DISPENSE_RESPONSE_MESSAGE_SIZE - 1);
    bool receive_processed = FALSE;
     BYTE dispenser_ack[2];

    if (BCC == dispenser_receive.data[PULOON_TEST_UPPER_DISPENSE_RESPONSE_MESSAGE_SIZE - 1])
    {
        receive_processed = TRUE;
    }
    else
    {
        puloon_dispenser_errors.bcc_mismatch++;
        logMessage("CDispenserControl::processPuloonTestDispenseReceiveCommand - BCC error.");
        // send nack
        memset(dispenser_ack, NAK, 1);
        TxDispenserData(dispenser_ack, 1);
    }

    return receive_processed;
}

bool CDispenserControl::ProcessPuloonDispenserReceive()
{
    bool receive_processed = FALSE;
    
    switch (dispenser_receive.data[0])
    {
        case SOH:
            // check for a response to a command
            if ((dispenser_receive.index >= 6) &&
                ((dispenser_receive.data[1] == PULOON_COMMUNICATION_ID) ||
                 (dispenser_receive.data[1] == PULOON_COMMUNICATION_ID_4000)) &&
                (dispenser_receive.data[2] == STX))
            {
                ProcessPuloonDispenserReceiveCommands();
            }
            else
            {
                // throw out garbage bytes
                if (dispenser_receive.index >= 6)
                    memset(&dispenser_receive, 0, sizeof(dispenser_receive));
            }
           
            break;
        case ACK:
            memset(&dispenser_receive, 0, sizeof(dispenser_receive));
            puloon_settings.sub.ack_received = true;
            break;
        case NAK:
            puloon_settings.sub.nack_count++;
            break;
        default:
            memset(&dispenser_receive, 0, sizeof(dispenser_receive));
            break;
    }

    return receive_processed;
}

void CDispenserControl::PollPuloonDispenser()
{
    // make sure the serial port is open
    if (hDispenserSerialPort != INVALID_HANDLE_VALUE)
        if (puloon_settings.sub.response_received)
        {
            ProcessPuloonDispenserCommands();

            if ((puloon_settings.sub.last_dispenser_command == PULOON_UPPER_DISPENSE) ||
                (puloon_settings.sub.last_dispenser_command == PULOON_LOWER_DISPENSE) ||
                (puloon_settings.sub.last_dispenser_command == PULOON_DISPENSE_4000) ||
                (puloon_settings.sub.last_dispenser_command == PULOON_STATUS))
            {
                if ((pulloon_bill_cnt1 || pulloon_bill_cnt2) && !theApp.message_window_queue.empty())
                    SendPuloonDispenseMessage();
            }
        }
        else
        {
            // if we're not waiting for a multiple dispense payout, and we don't receive a response within 60 seconds or we get too many nacks,
            // purge and check the status
            // only error out if there is more than 3 nacks send by dispenser
            if ((puloon_settings.sub.nack_count > 3) ||
                (time(NULL) > (puloon_settings.sub.last_dispenser_poll_time + puloon_settings.timeout_in_seconds)))
            {
                if ((puloon_settings.sub.last_dispenser_command == PULOON_UPPER_DISPENSE) ||
                    (puloon_settings.sub.last_dispenser_command == PULOON_LOWER_DISPENSE) ||
	                (puloon_settings.sub.last_dispenser_command == PULOON_DISPENSE_4000) ||
                    (puloon_settings.sub.last_dispenser_command == PULOON_UPPER_AND_LOWER_DISPENSE))
                {
                    CString log_message, window_message, general_message;

                    theApp.PrinterData("Dispenser Error.\nCall For Service.\nError Condition:\n");

                    log_message = "CDispenserControl::PollPuloonDispenser - ";

                    window_message = "SHORT COUNT ERROR:\n";
                    general_message = "Specified number of bills not dispensed.";

                    window_message += general_message;
                    theApp.PrinterData(general_message.GetBuffer(0));
                    theApp.PrinterData("\n");

                    log_message += general_message;
                    logMessage(log_message);

					puloon_settings.sub.response_received = TRUE;
					puloon_settings.sub.last_dispenser_command = 0;

                    MessageWindow(false, "DISPENSER ERROR!", window_message);
	                while (theApp.message_window_queue.empty())
		                theApp.OnIdle(0);

                    MessageWindow(false, "FATAL PULOON DISPENSER ERROR", "PLEASE SERVICE DISPENSER");
			        while (theApp.message_window_queue.empty())
				        theApp.OnIdle(0);

                    // fatal error
                    SendPuloonPurgeMessage(); // put this at the top of the queue
			        dispenser_payout_complete = TRUE;
                }
				else
				{
					puloon_settings.sub.response_received = TRUE;
					puloon_settings.sub.last_dispenser_command = 0;
			        dispenser_payout_complete = TRUE;
                }
            }
        }
}

bool CDispenserControl::dispenseLcdm1000Bills(DWORD amount)
{
 WORD dispenser_multiplier = 20;
 DWORD breakage, hold;
 CString prt_buf;

    SendPuloonStatusMessage();

    breakage = memory_dispenser_values.dispense_grand_total - memory_dispenser_values.tickets_amount;
    hold = amount - breakage;

    pulloon_bill_cnt1 = (WORD)((hold / (dispenser_multiplier * 100)) + 1);

    current_bill_cnt1 = pulloon_bill_cnt1;
    current_bill_cnt2 = 0;

    // 2nd check for amount > $8000, added $100 for either $100 or $20 dispense breakage
   	if (pulloon_bill_cnt1 * dispenser_multiplier > (theApp.memory_dispenser_config.dispenser_payout_limit + 100))
   	{
   		prt_buf.Format("\nBILL DISPENSER ERROR\nPLEASE CALL FOR SERVICE\nBILL COUNT: %u\n", pulloon_bill_cnt1);
   		theApp.PrinterData(prt_buf.GetBuffer(0));
   		return false;
   	}

    memory_dispenser_values.tickets_amount += amount;
    memory_dispenser_values.number_tickets++;
    memory_dispenser_values.dispense_grand_total += dispenser_multiplier * pulloon_bill_cnt1 * 100;
memory_dispenser_values.dispense_total1 += pulloon_bill_cnt1 * 2000;
    WriteDispenserFile();

    prt_buf.Format("DISPENSE: %9.2f\n", (double)(dispenser_multiplier * pulloon_bill_cnt1));
    theApp.PrinterData(prt_buf.GetBuffer(0));

    SendPuloonDispenseMessage();

    return true;
}

bool CDispenserControl::dispenseLcdm2000HundredsTwentiesBills(DWORD amount)
{
 WORD bill_cnt1, bill_cnt2, disp_cnt1, disp_cnt2;
 DWORD breakage, hold;
 CString prt_buf;

SendPuloonStatusMessage();

    breakage = memory_dispenser_values.dispense_grand_total - memory_dispenser_values.tickets_amount;
    hold = amount - breakage;

    bill_cnt1 = (WORD)(hold / 10000);

    // pay last even $100 dollars in $20s
    if (bill_cnt1 > 0 && !breakage && !(amount % 10000))
        bill_cnt1--;
    bill_cnt2 = (WORD)((hold / 2000) + 1 - (bill_cnt1 * 5));

    disp_cnt1 = (WORD)((memory_dispenser_values.impress_amount1 - memory_dispenser_values.dispense_total1) / 10000);
    disp_cnt2 = (WORD)((memory_dispenser_values.impress_amount2 - memory_dispenser_values.dispense_total2) / 2000);

    if (disp_cnt1 < memory_dispenser_values.reject_bills1)
        disp_cnt1 = 0;
    else
        disp_cnt1 -= (WORD)memory_dispenser_values.reject_bills1;

    if (disp_cnt2 < memory_dispenser_values.reject_bills2)
        disp_cnt2 = 0;
    else
        disp_cnt2 -= (WORD)memory_dispenser_values.reject_bills2;

    // if $100s low
   	if (bill_cnt1 > disp_cnt1)
   	{
   		bill_cnt2 += (bill_cnt1 - disp_cnt1) * 5;
   		bill_cnt1 = disp_cnt1;
		printHundredsLow();
   	}
   	else
   	{
   		// if $20s low
   		if (bill_cnt2 > disp_cnt2)
   		{
   			// for $100 or less
   			bill_cnt1++;
            // for more than $100
   			if (bill_cnt2 > 5)
   				bill_cnt1++;
   			bill_cnt2 = 0;
			printTwentiesLow();
   		}
   	}

   	// 2nd check for amount > $8000, added $120 for either $100 or $20 dispense breakage
  	if ((bill_cnt1 * 100) + (bill_cnt2 * 20) > (theApp.memory_dispenser_config.dispenser_payout_limit + 120))
   	{
   		theApp.PrinterHeader(0);
   		theApp.PrinterData("\nBILL DISPENSER ERROR\nPLEASE CALL FOR SERVICE\n");
   		prt_buf.Format("$100 BILL COUNT: %u\n$ 20 BILL COUNT: %u\n\n", bill_cnt1, bill_cnt2);
   		theApp.PrinterData(prt_buf.GetBuffer(0));
   		return false;
   	}

    memory_dispenser_values.tickets_amount += amount;
    memory_dispenser_values.number_tickets++;
    memory_dispenser_values.dispense_total1 += bill_cnt1 * 10000;
    memory_dispenser_values.dispense_total2 += bill_cnt2 * 2000;
    memory_dispenser_values.dispense_grand_total = memory_dispenser_values.dispense_total1 + memory_dispenser_values.dispense_total2;
    WriteDispenserFile();

   	prt_buf.Format("DISPENSE $100: %9.2f\n", (double)(bill_cnt1 * 100));
   	theApp.PrinterData(prt_buf.GetBuffer(0));
   	prt_buf.Format("DISPENSE $ 20: %9.2f\n", (double)(bill_cnt2 * 20));
   	theApp.PrinterData(prt_buf.GetBuffer(0));

    pulloon_bill_cnt1 = bill_cnt1;
    pulloon_bill_cnt2 = bill_cnt2;

    current_bill_cnt1 = bill_cnt1;
    current_bill_cnt2 = bill_cnt2;

    SendPuloonDispenseMessage();

 return true;
}

void CDispenserControl::SendPuloonDispenseMessage(void)
{

    if (pulloon_bill_cnt1)
    {
        // only send $1000 or 50 bills at a time
        if (pulloon_bill_cnt1 > 50)
        {
            //MessageWindow(false, false, "PULOON DISPENSE", "** DISPENSING BILLS **\n** PLEASE WAIT **");
            if (pMessageBox)
            {
                pMessageBox->DestroyWindow();
                delete pMessageBox;
                pMessageBox = 0;
            }

            pMessageBox = new CMessageBox(false, false, "PULOON DISPENSE", "** DISPENSING BILLS **\n** PLEASE WAIT **");
            pMessageBox->Create(IDD_MESSAGE_BOX_DIALOG);
            pMessageBox->ShowMalfunctionOverrideButton();
     
            SendPuloonUpperDispenseMessage(50);
            pulloon_bill_cnt1 -= 50;
        }
        else
        {
            if (pMessageBox)
            {
                pMessageBox->DestroyWindow();
                delete pMessageBox;
                pMessageBox = 0;
            }

            pMessageBox = new CMessageBox(false, false, "PULOON DISPENSE", "** DISPENSING BILLS **\n** PLEASE WAIT **");
            pMessageBox->Create(IDD_MESSAGE_BOX_DIALOG);
            pMessageBox->ShowMalfunctionOverrideButton();

            if (pulloon_bill_cnt2 && (pulloon_bill_cnt2 <= 50))
            {
                SendPuloonUpperAndLowerDispenseMessage(pulloon_bill_cnt1, pulloon_bill_cnt2);
                pulloon_bill_cnt1 = 0;
                pulloon_bill_cnt2 = 0;
            }
            else
            {
                SendPuloonUpperDispenseMessage(pulloon_bill_cnt1);
                pulloon_bill_cnt1 = 0;
            }

        }
    }
    else if (pulloon_bill_cnt2)
    {
        if (pulloon_bill_cnt1 == 0)
        {
            // 50 bills at a time
            if (pulloon_bill_cnt2 > 50)
            {
                //MessageWindow(false, false, "PULOON DISPENSE", "** DISPENSING BILLS **\n** PLEASE WAIT **");
                if (pMessageBox)
                {
                    pMessageBox->DestroyWindow();
                    delete pMessageBox;
                    pMessageBox = 0;
                }

                pMessageBox = new CMessageBox(false, false, "PULOON DISPENSE", "** DISPENSING BILLS **\n** PLEASE WAIT **");
                pMessageBox->Create(IDD_MESSAGE_BOX_DIALOG);
                pMessageBox->ShowMalfunctionOverrideButton();
         
                SendPuloonLowerDispenseMessage(50);
                pulloon_bill_cnt2 -= 50;
            }
            else
            {
                if (pMessageBox)
                {
                    pMessageBox->DestroyWindow();
                    delete pMessageBox;
                    pMessageBox = 0;
                }

                pMessageBox = new CMessageBox(false, false, "PULOON DISPENSE", "** DISPENSING BILLS **\n** PLEASE WAIT **");
                pMessageBox->Create(IDD_MESSAGE_BOX_DIALOG);
                pMessageBox->ShowMalfunctionOverrideButton();

                SendPuloonLowerDispenseMessage(pulloon_bill_cnt2);
                pulloon_bill_cnt2 = 0;
//                SendPuloonStatusMessage();
            }
        }
    }
}

void CDispenserControl::SendPuloonPurgeMessage(void)
{
    DispenserCommand message;

    memset(&message, 0, sizeof(message));
    message.length = PULOON_PURGE_MESSAGE_SIZE; // length of the dispense message
    message.data[0] = EOT;
    if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_LCDM4000_20_100)
        message.data[1] = PULOON_COMMUNICATION_ID_4000;
    else
        message.data[1] = PULOON_COMMUNICATION_ID;
    message.data[2] = STX;
    if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_LCDM4000_20_100)
        message.data[3] = PULOON_PURGE_4000;
    else
        message.data[3] = PULOON_PURGE;
    message.data[4] = ETX;
    message.data[5] = getPuloonBCC(message.data, 5);

    theApp.dispenser_command_queue.push(message);

	puloon_settings.sub.response_received = true;
}

void CDispenserControl::SendPuloonStatusMessage(void)
{
    DispenserCommand message;

    memset(&message, 0, sizeof(message));
    message.length = PULOON_STATUS_MESSAGE_SIZE; // length of the dispense message
    message.data[0] = EOT;
    if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_LCDM4000_20_100)
        message.data[1] = PULOON_COMMUNICATION_ID_4000;
    else
        message.data[1] = PULOON_COMMUNICATION_ID;
    message.data[2] = STX;
    message.data[3] = PULOON_STATUS;
    if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_LCDM4000_20_100)
        message.data[3] = PULOON_STATUS_4000;
    else
        message.data[3] = PULOON_STATUS;
    message.data[4] = ETX;
    message.data[5] = getPuloonBCC(message.data, 5);

    theApp.dispenser_command_queue.push(message);
}

void CDispenserControl::SendPuloonUpperDispenseMessage(WORD bill_count)
{

    if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_LCDM4000_20_100)
        SendPuloon4000UpperAndLowerDispenseMessage(bill_count, 0);
    else
    {
        DispenserCommand message;
        BYTE tens = 0;
        BYTE ones = 0;
        char tens_char = '0';
        char ones_char = '0';

        dispenser_payout_complete = FALSE;

        memset(&message, 0, sizeof(message));
        message.length = PULOON_UPPER_DISPENSE_MESSAGE_SIZE; // length of the dispense message
        message.data[0] = EOT;
        message.data[1] = PULOON_COMMUNICATION_ID;
        message.data[2] = STX;
        message.data[3] = PULOON_UPPER_DISPENSE;

        if (bill_count >= 10)
        {
            tens = bill_count / 10;
            ones = bill_count % 10;
            tens_char = '0' + tens;
            ones_char = '0' + ones;
        }
        else
        {
            ones = bill_count;
            ones_char = '0' + ones;
        }

        message.data[4] = tens_char;
        message.data[5] = ones_char;

        message.data[6] = ETX;

        message.data[7] = getPuloonBCC(message.data, 7);

        theApp.dispenser_command_queue.push(message);
    }
}

void CDispenserControl::SendPuloonLowerDispenseMessage(WORD bill_count)
{
    if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_LCDM4000_20_100)
        SendPuloon4000UpperAndLowerDispenseMessage(0, bill_count);
    else
    {
        DispenserCommand message;
        BYTE tens = 0;
        BYTE ones = 0;
        char tens_char = '0';
        char ones_char = '0';


        dispenser_payout_complete = FALSE;

        memset(&message, 0, sizeof(message));
        message.length = PULOON_LOWER_DISPENSE_MESSAGE_SIZE; // length of the dispense message
        message.data[0] = EOT;
        message.data[1] = PULOON_COMMUNICATION_ID;
        message.data[2] = STX;
        message.data[3] = PULOON_LOWER_DISPENSE;

        if (bill_count >= 10)
        {
            tens = bill_count / 10;
            ones = bill_count % 10;
            tens_char = '0' + tens;
            ones_char = '0' + ones;
        }
        else
        {
            ones = bill_count;
            ones_char = '0' + ones;
        }

        message.data[4] = tens_char;
        message.data[5] = ones_char;

        message.data[6] = ETX;

        message.data[7] = getPuloonBCC(message.data, 7);

        theApp.dispenser_command_queue.push(message);
    }
}

void CDispenserControl::SendPuloonUpperAndLowerDispenseMessage(WORD bill_count_upper, WORD bill_count_lower)
{
    if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_LCDM4000_20_100)
        SendPuloon4000UpperAndLowerDispenseMessage(bill_count_upper, bill_count_lower);
    else
    {
        DispenserCommand message;
        BYTE tens = 0;
        BYTE ones = 0;
        char tens_char = '0';
        char ones_char = '0';


        dispenser_payout_complete = FALSE;

        memset(&message, 0, sizeof(message));
        message.length = PULOON_UPPER_AND_LOWER_DISPENSE_MESSAGE_SIZE; // length of the dispense message
        message.data[0] = EOT;
        message.data[1] = PULOON_COMMUNICATION_ID;
        message.data[2] = STX;
        message.data[3] = PULOON_UPPER_AND_LOWER_DISPENSE;

        if (bill_count_upper >= 10)
        {
            tens = bill_count_upper / 10;
            ones = bill_count_upper % 10;
            tens_char = '0' + tens;
            ones_char = '0' + ones;
        }
        else
        {
            ones = bill_count_upper;
            ones_char = '0' + ones;
        }

        message.data[4] = tens_char;
        message.data[5] = ones_char;

        tens = 0;
        ones = 0;
        tens_char = '0';
        ones_char = '0';

        if (bill_count_lower >= 10)
        {
            tens = bill_count_lower / 10;
            ones = bill_count_lower % 10;
            tens_char = '0' + tens;
            ones_char = '0' + ones;
        }
        else
        {
            ones = bill_count_lower;
            ones_char = '0' + ones;
        }

        message.data[6] = tens_char;
        message.data[7] = ones_char;

        message.data[8] = ETX;

        message.data[9] = getPuloonBCC(message.data, 9);

        theApp.dispenser_command_queue.push(message);
    }
}

void CDispenserControl::SendPuloon4000UpperAndLowerDispenseMessage(WORD bill_count_upper, WORD bill_count_lower)
{
    DispenserCommand message;
    BYTE tens = 0;
    BYTE ones = 0;
    char tens_char = '0';
    char ones_char = '0';

    dispenser_payout_complete = FALSE;

    memset(&message, 0, sizeof(message));
    message.length = PULOON_4000_DISPENSE_MESSAGE_SIZE; // length of the dispense message
    message.data[0] = EOT;
    message.data[1] = PULOON_COMMUNICATION_ID_4000;
    message.data[2] = STX;
    message.data[3] = PULOON_DISPENSE_4000;

    message.data[4] = bill_count_upper + 0x20; // number of bills dispensed from casette 1
    message.data[5] = bill_count_lower + 0x20; // number of bills dispensed from casette 2
    message.data[6] = 0x20;  // number of bills dispensed from casette 3
    message.data[7] = 0x20;  // number of bills dispensed from casette 4

    message.data[8] = 0x20;  // timeout value 1 - use defaults
    message.data[9] = 0x20;  // timeout value 2 - use defaults
    // Reserved 9 bytes
    memset(&message.data[10], 0x20, 9);

    message.data[19] = ETX;

    message.data[20] = getPuloonBCC(message.data, 20);

    theApp.dispenser_command_queue.push(message);
}

bool CDispenserControl::dispenserPayoutComplete()
{
    int i;
    int sum = 0;

    // make sure we don't get trapped with the flag set to payout not complete
    switch (theApp.memory_dispenser_config.dispenser_type)
    {
        // poll the jcm hbp-10 dispenser
        case DISPENSER_TYPE_JCM_20:
        case DISPENSER_TYPE_JCM_100:
            for (i = 0; i < MAXIMUM_DISPENSER_RECEIVE; i++)
                sum = dispenser_receive.data[i];
            // if there is no data in the dispenser recieve buffer
            if (!sum || (dispenser_receive.data[4] == 0x40))
                dispenser_payout_complete = TRUE;
        break;
    }


    return dispenser_payout_complete;
}

void CDispenserControl::printHundredsLow (void)
{
	if (!theApp.memory_settings_ini.stratus_ip_address && !theApp.memory_settings_ini.idc_ip_address)
	{
   		theApp.PrinterHeader(0);
		theApp.PrinterData("\n BILL DISPENSER LOW: $100s\n\n");
	}
}

void CDispenserControl::printTwentiesLow (void)
{
	if (!theApp.memory_settings_ini.stratus_ip_address && !theApp.memory_settings_ini.idc_ip_address)
	{
   		theApp.PrinterHeader(0);
		theApp.PrinterData("\n BILL DISPENSER LOW: $20s\n\n");
	}
}

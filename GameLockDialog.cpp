/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   07/29/13    Added code to prevent cashiers from bypassing the Game Tender automated w2g when paying game locks.
Robert Fuller	12/03/13	Check the socket connection to the IDC before launching the Game Tender application.
Robert Fuller	12/03/13	Added IDC to XP Controller message to relay w2g social security and drivers license information after the drivers license information is scanned after of a taxable jackpot win.
Robert Fuller   07/02/13	Fixed the hour field overwrite problem with the Stratus jackpot 2 credit message.
Robert Fuller   07/02/13	Save important Stratus TX messages to a file on disk and do not purge unless Stratus returns an ack.
Robert Fuller   05/02/13	Make sure the game lock being paid is not a jackpot to credit event before checking for a dispenser low condition.
Robert Fuller   05/02/13	Fill the bill amount and reject amount values before sending the idc a dispense info message on a non-dispense..
Robert Fuller   03/08/13    Cleared the printer flag so that unlock game message can print.
Robert Fuller   03/08/13    Removed obsolete Location Collection code.
Robert Fuller   03/08/13    Added code to support the configurable dispenser payout limit
Robert Fuller   03/08/13    Added code to send bill denom info to IDC when a bill is accepted.
Robert Fuller	09/17/12	Added code to fill the ucmc id field in the dispense message we send to the IDC for accounting purposes.
Robert Fuller   09/17/12    Start up Game Tender after a game lock is paid.
Robert Fuller	06/20/12	Added new transaction id on the MarkerTrax Payment reciept
Robert Fuller   05/31/12    Added code to communicate with the Interactive Display Controller.
Robert Fuller   12/30/11    Adjusted the dispenser payout screens strings to include SAS direct connect games sas id.
Robert Fuller   12/30/11    Added a Marker Credit Pending message to notify the Stratus that a marker credit payment needs to be paid.
Robert Fuller   10/03/11    Print the sas id address as opposed to the irrelevant mux id address for SAS direct connect games.
Robert Fuller   08/03/11    Added code to support Location Collection features.
Robert Fuller   04/26/11    Added the 'do not print dispense info' flag and functionality.
Robert Fuller   10/27/10    Use the upper and lower Puloon dispense command to avoid unnecessary cashier dispenser interaction.
Robert Fuller   05/18/10    When adding up all the drop tickets, make sure the customer id matches the customer id of the current PAID OUT LOG report being run.
Robert Fuller   05/05/10    Added the signature lines for the player and cashier for MarkerTrax related transactions.
Robert Fuller   03/11/10    Fixed problem with Puloon dispenses over 69 bills.
Robert Fuller   01/19/10    Implemented a file redundancy strategy for files which store the paid game locks, dispenses, and drop tickets.
Robert Fuller   07/15/09    Increased the number of shifts to print on the shift report from 4 to 21.
Robert Fuller   06/08/09    Do not allow the slave to pay game locks when the master has a pending game lock pay.
Robert Fuller   04/08/09    Added code to only allow one shift at a time per login.
Robert Fuller   01/28/09    Print the game lock ticket id when displaying a ticket error.
Robert Fuller   10/14/08    Avoid displaying the ticket pay pending tilt if the controller paying the ticket is the same as the
                            controller that logged the pending ticket.
Robert Fuller   08/25/08    Stop the gamelock noise and light flash when any game lock is paid.
Robert Fuller   08/19/08    Made sure we are not in multi-controller mode before checking for pending tickets when paying cashouts.
Robert Fuller   03/20/08    Fixed problems with paying taxable events.
Robert Fuller   11/05/07    Only remove pending tickets if successfully paid. Afterwich reset active home flag.
Robert Fuller   07/15/07    If an invalid SS or drivers license is entered on the slave, DO NOT remove pending ticket.
Dave Elquist    07/15/05    Changed game lock list transfer to class.
Dave Elquist    06/09/05    Initial Revision.
*/


// GameLockDialog.cpp : implementation file
//

#define _CRT_RAND_S

#include "stdafx.h"
#include "xp_controller.h"
#include ".\gamelockdialog.h"
#include "TimerWindow.h"
#include "TaxInformation.h"

#include <stdlib.h>

// CGameLockDialog dialog

IMPLEMENT_DYNAMIC(CGameLockDialog, CDialog)
CGameLockDialog::CGameLockDialog(CWnd* pParent /*=NULL*/) : CDialog(CGameLockDialog::IDD, pParent)
{
}

CGameLockDialog::~CGameLockDialog()
{
}

BOOL CGameLockDialog::OnInitDialog()
{
 int iii;

	CDialog::OnInitDialog();

    game_list_index = 0;
    for (iii=0; iii < MAXIMUM_GAME_LOCKS_LIST; iii++)
    {
        if (game_lock_list[iii].ucmc_id)
            game_list_index++;
        else
            break;
    }

    if (game_list_index <= 4)
        m_gamelock_more_button.DestroyWindow();

    game_list_index = 0;
    UpdateGameLockButtons();

    SetDlgItemText(IDC_GAME_LOCK_TITLE_STATIC, "GAME LOCKS (MUX/SAS ID,UCMC ID,PORT)");

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CGameLockDialog::OnPaint() 
{
	CRect rect;
	GetClientRect(&rect);
	CPaintDC dc(this); // device context for painting

	CDialog::OnPaint();

    dc.FillSolidRect(&rect, 0x00000000);
}

void CGameLockDialog::UpdateGameLockButtons()
{
 int iii;
 CString window_text, payment_text;

    m_game_lock_button1.ShowWindow(SW_HIDE);
    m_game_lock_button2.ShowWindow(SW_HIDE);
    m_game_lock_button3.ShowWindow(SW_HIDE);
    m_game_lock_button4.ShowWindow(SW_HIDE);

    if (!game_list_index && !game_lock_list[0].ucmc_id)
    {
        m_game_lock_button1.SetWindowText("NO GAME LOCKS");
        m_game_lock_button1.ShowWindow(SW_SHOW);
    }
    else
    {
        for (iii=0; iii < 4; iii++)
        {
            if (game_list_index < MAXIMUM_GAME_LOCKS_LIST && game_lock_list[game_list_index + iii].ucmc_id)
            {
				if (game_lock_list[game_list_index + iii].mux_id)
					window_text.Format("%u,%u,%u: ", game_lock_list[game_list_index + iii].mux_id,
						game_lock_list[game_list_index + iii].ucmc_id, game_lock_list[game_list_index + iii].game_port);
				else
					window_text.Format("%u,%u,%u: ", game_lock_list[game_list_index + iii].sas_id,
                    game_lock_list[game_list_index + iii].ucmc_id, game_lock_list[game_list_index + iii].game_port);

                if (game_lock_list[game_list_index + iii].jackpot_to_credit_flag)
                    payment_text.Format("CREDIT PAY $%6.2f", (double)game_lock_list[game_list_index + iii].ticket_amount / 100);
                else if (game_lock_list[game_list_index + iii].marker_balance)
                    payment_text.Format("AMOUNT $%6.2f, MARKER $%6.2f", (double)game_lock_list[game_list_index + iii].ticket_amount / 100,
                        (double)game_lock_list[game_list_index + iii].marker_balance / 100);
                else
                    payment_text.Format("$%6.2f", (double)game_lock_list[game_list_index + iii].ticket_amount / 100);

                window_text += payment_text;

                switch (iii)
                {
                    case 0:
                        m_game_lock_button1.SetWindowText(window_text);
                        m_game_lock_button1.ShowWindow(SW_SHOW);
                        break;
                    case 1:
                        m_game_lock_button2.SetWindowText(window_text);
                        m_game_lock_button2.ShowWindow(SW_SHOW);
                        break;

                    case 2:
                        m_game_lock_button3.SetWindowText(window_text);
                        m_game_lock_button3.ShowWindow(SW_SHOW);
                        break;

                    case 3:
                        m_game_lock_button4.SetWindowText(window_text);
                        m_game_lock_button4.ShowWindow(SW_SHOW);
                        break;
                }
            }
        }
    }
}

void CGameLockDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GAMELOCK_BUTTON1, m_game_lock_button1);
	DDX_Control(pDX, IDC_GAMELOCK_BUTTON2, m_game_lock_button2);
	DDX_Control(pDX, IDC_GAMELOCK_BUTTON3, m_game_lock_button3);
	DDX_Control(pDX, IDC_GAMELOCK_BUTTON4, m_game_lock_button4);
	DDX_Control(pDX, IDC_GAMELOCK_MORE_BUTTON, m_gamelock_more_button);
}


BEGIN_MESSAGE_MAP(CGameLockDialog, CDialog)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_GAMELOCK_BUTTON1, OnBnClickedGamelockButton1)
	ON_BN_CLICKED(IDC_GAMELOCK_BUTTON2, OnBnClickedGamelockButton2)
	ON_BN_CLICKED(IDC_GAMELOCK_BUTTON3, OnBnClickedGamelockButton3)
	ON_BN_CLICKED(IDC_GAMELOCK_BUTTON4, OnBnClickedGamelockButton4)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_GAMELOCK_EXIT_BUTTON, OnBnClickedGamelockExitButton)
	ON_BN_CLICKED(IDC_GAMELOCK_MORE_BUTTON, OnBnClickedGamelockMoreButton)
END_MESSAGE_MAP()


void CGameLockDialog::SendPayToCreditMessageToStratus(MemoryPaidTicket *pMemoryPaidTicket)
{
    int iii;
    CString convert_string;
    StratusMessageFormat stratus_msg;
    Jackpot2Credit jackpot_to_credit;


    memset(&jackpot_to_credit, 0, sizeof(jackpot_to_credit));
    jackpot_to_credit.ticket_id = pMemoryPaidTicket->memory_game_lock.ticket_id;
    jackpot_to_credit.ticket_amount = pMemoryPaidTicket->memory_game_lock.ticket_amount;
    jackpot_to_credit.player_account = pMemoryPaidTicket->memory_game_lock.player_account;
    jackpot_to_credit.cashier_id = pMemoryPaidTicket->cashier_id;
    jackpot_to_credit.ss_number = pMemoryPaidTicket->memory_game_lock.ss_number;

    endian4(jackpot_to_credit.ticket_id);
    endian4(jackpot_to_credit.ticket_amount);
    endian4(jackpot_to_credit.player_account);
    endian4(jackpot_to_credit.cashier_id);
    endian4(jackpot_to_credit.ss_number);

    convert_string.Format("%2.2X", pMemoryPaidTicket->memory_game_lock.time_stamp[0]);
    memcpy(jackpot_to_credit.time_stamp.hour, convert_string.GetBuffer(0), 2);
    convert_string.Format("%2.2X", pMemoryPaidTicket->memory_game_lock.time_stamp[1]);
    memcpy(jackpot_to_credit.time_stamp.minute, convert_string.GetBuffer(0), 2);
    convert_string.Format("%2.2X", pMemoryPaidTicket->memory_game_lock.time_stamp[2]);
    memcpy(jackpot_to_credit.time_stamp.second, convert_string.GetBuffer(0), 2);
    convert_string.Format("%2.2X", pMemoryPaidTicket->memory_game_lock.time_stamp[3]);
    memcpy(jackpot_to_credit.time_stamp.day, convert_string.GetBuffer(0), 2);
    convert_string.Format("%2.2X", pMemoryPaidTicket->memory_game_lock.time_stamp[4]);
    memcpy(jackpot_to_credit.time_stamp.month, convert_string.GetBuffer(0), 2);
    convert_string.Format("%2.2X", pMemoryPaidTicket->memory_game_lock.time_stamp[5]);
    memcpy(&jackpot_to_credit.time_stamp.year[2], convert_string.GetBuffer(0), 2);
    jackpot_to_credit.time_stamp.year[0] = '2';
    jackpot_to_credit.time_stamp.year[1] = '0';

    memset(jackpot_to_credit.drivers_license, 0, sizeof(jackpot_to_credit.drivers_license));
    sprintf_s(jackpot_to_credit.drivers_license, sizeof(jackpot_to_credit.drivers_license), "%.0f", pMemoryPaidTicket->memory_game_lock.drivers_license);
    // Stratus needs spaces in unused fields for driver's license
	int drivers_license_string_length = strlen(jackpot_to_credit.drivers_license);
	if (drivers_license_string_length && (drivers_license_string_length < sizeof(jackpot_to_credit.drivers_license)))
		for (iii=drivers_license_string_length; iii < sizeof(jackpot_to_credit.drivers_license); iii++)
			jackpot_to_credit.drivers_license[iii] = ' ';

    memset(&stratus_msg, 0, sizeof(stratus_msg));

	CGameDevice *pGameDevice = theApp.GetGamePointer(pMemoryPaidTicket->memory_game_lock.ucmc_id);
	if (pGameDevice && pGameDevice->memory_game_config.club_code)
		stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
	else
		stratus_msg.club_code = theApp.memory_location_config.club_code;

	stratus_msg.send_code = TKT_JACKPOT_CREDIT;
	stratus_msg.task_code = TKT_TASK_TICKET;
	stratus_msg.mux_id = pMemoryPaidTicket->memory_game_lock.mux_id;
	stratus_msg.customer_id = pMemoryPaidTicket->memory_game_lock.location_id;
	stratus_msg.ucmc_id = pMemoryPaidTicket->memory_game_lock.ucmc_id;
    stratus_msg.property_id = theApp.memory_location_config.property_id;
	stratus_msg.message_length = sizeof(jackpot_to_credit) + STRATUS_HEADER_SIZE;
    stratus_msg.data_to_event_log = true;
	stratus_msg.serial_port = pMemoryPaidTicket->memory_game_lock.game_port;
	memcpy(stratus_msg.data, &jackpot_to_credit, sizeof(jackpot_to_credit));
    theApp.QueueStratusTxMessage(&stratus_msg, TRUE);
}

void CGameLockDialog::PrintTicketErrorMessage(DWORD ticket_id, DWORD ticket_amount, DWORD ucmc_id, DWORD state)
{
 CString prt_buf;

    theApp.printer_busy = true;
    theApp.PrinterData("\n");
    theApp.PrinterHeader(0);
    theApp.PrinterId();

	switch (state)
	{
        case TICKET_PAY_PENDING:
			theApp.PrinterData("TICKET PAY PENDING\n");
            MessageWindow(false, "TICKET MESSAGE", "**** TICKET PAY PENDING ****");
            break;

		case TICKET_NOT_FOUND:
			theApp.PrinterData("TICKET NOT FOUND\n");
            MessageWindow(false, "TICKET MESSAGE", "**** TICKET NOT FOUND ****");
			break;

		case SYSTEM_FILE_ERROR:
			theApp.PrinterData("SYSTEM FILE ERROR\n");
            MessageWindow(false, "TICKET MESSAGE", "**** SYSTEM FILE ERROR ****");
			break;

		case TICKET_ALREADY_PAID:
			theApp.PrinterData("TICKET ALREADY PAID\n");
            MessageWindow(false, "TICKET MESSAGE", "**** TICKET ALREADY PAID ****");
			break;

        case INVALID_USER_LOGIN_ID:
			theApp.PrinterData("INVALID USER LOGIN ID DURING PAY\n");
            MessageWindow(false, "TICKET MESSAGE", "**** INVALID USER LOGIN ID DURING PAY ****");
            break;

		case INVALID_TICKET_STATUS:
        default:
			theApp.PrinterData("INVALID TICKET STATUS\n");
			MessageWindow(false, "TICKET MESSAGE", "**** INVALID TICKET STATUS ****");
			break;
	}

	prt_buf.Format("TICKET NUMBER:   %u\n", ticket_id);
	theApp.PrinterData(prt_buf.GetBuffer(0));
	prt_buf.Format("TICKET AMOUNT: %8.2f\n", (double)ticket_amount / 100);
	theApp.PrinterData(prt_buf.GetBuffer(0));
	prt_buf.Format("UCMC ID: %u\n\n", ucmc_id);
	theApp.PrinterData(prt_buf.GetBuffer(0));
    theApp.printer_busy = false;
}

// return true is successful pay gamelock, otherwise false
bool CGameLockDialog::PayGameLock(FileGameLock *pFileGameLock, char* computer_name, DWORD *pMarkerBalance /*=NULL*/)
{
 bool got_tax_information;
 int record_index, iii;
 time_t wait_timer;
 DWORD ticket_id, file_index, dispense_amount, new_balance_due, original_marker_amount, credit_amount;
 CString prt_buf, message_display;
 FilePaidTicket file_paid_ticket;
 FilePendingTickets pending_ticket;
 CTaxInformation *pTaxInformation;
 StratusMessageFormat stratus_msg;
 GameUnlockQueue game_unlock_message;
 StratusMarkerCredit stratus_marker_credit;
 FileDropTicket drop_ticket;
 FileGameRedeemRecord redeem_record;
 FileCashierShift file_cashier_shift;
 CGameDevice *pGameDevice;
 BYTE id, number_of_game_locks;
 unsigned int marker_credit_transaction_id;

 
    if (theApp.dispenserControl.dispenserPayoutComplete())
    {
        ticket_id = 0;
        file_index = 0;

        while (!ticket_id && fileRead(PAID_TICKETS, file_index, &file_paid_ticket, sizeof(file_paid_ticket)))
        {
            if (pFileGameLock->game_lock.ticket_id == file_paid_ticket.memory.memory_game_lock.ticket_id &&
                pFileGameLock->game_lock.ucmc_id == file_paid_ticket.memory.memory_game_lock.ucmc_id &&
                pFileGameLock->game_lock.ticket_amount == file_paid_ticket.memory.memory_game_lock.ticket_amount)
            {
                PrintTicketErrorMessage(pFileGameLock->game_lock.ticket_id, pFileGameLock->game_lock.ticket_amount,
                    pFileGameLock->game_lock.ucmc_id, TICKET_ALREADY_PAID);
                ticket_id = pFileGameLock->game_lock.ticket_id;
            }
            else
                file_index++;
        }

        file_index = 0;
        while (!ticket_id && fileRead(DATED_PAID_TICKETS, file_index, &file_paid_ticket, sizeof(file_paid_ticket)))
        {
            if (pFileGameLock->game_lock.ticket_id == file_paid_ticket.memory.memory_game_lock.ticket_id &&
                pFileGameLock->game_lock.ucmc_id == file_paid_ticket.memory.memory_game_lock.ucmc_id &&
                pFileGameLock->game_lock.ticket_amount == file_paid_ticket.memory.memory_game_lock.ticket_amount)
            {
                PrintTicketErrorMessage(pFileGameLock->game_lock.ticket_id, pFileGameLock->game_lock.ticket_amount,
                    pFileGameLock->game_lock.ucmc_id, TICKET_ALREADY_PAID);
                ticket_id = pFileGameLock->game_lock.ticket_id;
            }
            else
                file_index++;
        }

        if (!ticket_id)
        {
            if (GetFileAttributes(MASTER_CONFIG) != 0xFFFFFFFF)
            {
                if (fileGetRecord(PENDING_TICKETS, &pending_ticket, sizeof(pending_ticket), pFileGameLock->game_lock.ticket_id) >= 0)
                {
                    // don't pay the ticket if the ticket is pending on another slave
                    if (strcmp(pending_ticket.computer_name, computer_name))
                    {
                        PrintTicketErrorMessage(pFileGameLock->game_lock.ticket_id, pFileGameLock->game_lock.ticket_amount,
                            pFileGameLock->game_lock.ucmc_id, TICKET_PAY_PENDING);
                        return false;
                    }
                }
                else
                {
                    if (!pFileGameLock->game_lock.taxable_flag)
                    {
                        memset(&pending_ticket, 0, sizeof(pending_ticket));
                        pending_ticket.ticket_id = pFileGameLock->game_lock.ticket_id;
                        strcpy_s(pending_ticket.computer_name, sizeof(pending_ticket.computer_name), computer_name);
                        fileWrite(PENDING_TICKETS, -1, &pending_ticket, sizeof(pending_ticket));
                    }
                }
            }

            if (!pFileGameLock->game_lock.jackpot_to_credit_flag &&
                theApp.DispenserMoneyLowCheck(pFileGameLock->game_lock.ticket_amount))
                    ticket_id = pFileGameLock->game_lock.ticket_id;
        }

	    if (!pFileGameLock->game_lock.jackpot_to_credit_flag && theApp.DispenserMoneyLowCheck(pFileGameLock->game_lock.ticket_amount))
		{
	        while (theApp.message_window_queue.empty())
		        theApp.OnIdle(0);
			return 0;
		}

        if (pFileGameLock->game_lock.jackpot_to_credit_flag)
            dispense_amount = 0;
        else
        {

// MOVING TO HERE
            // request a marker pay back from the stratus
            if (pFileGameLock->game_lock.marker_balance)
            {
                memset(&stratus_marker_credit, 0, sizeof(stratus_marker_credit));

                stratus_marker_credit.ticket_id = pFileGameLock->game_lock.ticket_id;
                stratus_marker_credit.player_account = pFileGameLock->game_lock.player_account;
                // generate a transaction number
//                rand_s(&stratus_marker_credit.transaction_id);

                if (pFileGameLock->game_lock.marker_balance > pFileGameLock->game_lock.ticket_amount)
                    stratus_marker_credit.credit_amount = pFileGameLock->game_lock.ticket_amount;
                else
                    stratus_marker_credit.credit_amount = pFileGameLock->game_lock.marker_balance;

                credit_amount = stratus_marker_credit.credit_amount;

				rand_s(&marker_credit_transaction_id);
				stratus_marker_credit.transaction_id = (DWORD)marker_credit_transaction_id;

                endian4(stratus_marker_credit.ticket_id);
                endian4(stratus_marker_credit.player_account);
                endian4(stratus_marker_credit.credit_amount);
				endian4(stratus_marker_credit.transaction_id);

                memset(&stratus_msg, 0, sizeof(stratus_msg));

			    pGameDevice = theApp.GetGamePointer(pFileGameLock->game_lock.ucmc_id);
			    if (pGameDevice && pGameDevice->memory_game_config.club_code)
				    stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
			    else
				    stratus_msg.club_code = theApp.memory_location_config.club_code;

                stratus_msg.send_code = MPI_MARKER_CREDIT;
                stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
                stratus_msg.mux_id = pFileGameLock->game_lock.mux_id;
                stratus_msg.ucmc_id = pFileGameLock->game_lock.ucmc_id;
                stratus_msg.message_length = sizeof(stratus_marker_credit) + STRATUS_HEADER_SIZE;
                stratus_msg.property_id = theApp.memory_location_config.property_id;
                stratus_msg.data_to_event_log = true;
                stratus_msg.customer_id = pFileGameLock->game_lock.location_id;
                memcpy(stratus_msg.data, &stratus_marker_credit, sizeof(stratus_marker_credit));
                theApp.marker_credit_reply = false;
                theApp.marker_message_recieved = true;
                theApp.QueueMarkerStratusTxMessage(&stratus_msg);

                wait_timer = time(NULL) + 5;	// give 5 seconds for the Stratus to reply

                while (!theApp.marker_credit_reply)
                {
	                if (wait_timer < time(NULL))
		                break;
	                else
	                {
		                theApp.OnMouseMove();
		                theApp.OnIdle(0);
	                }
                }

                if (!theApp.marker_credit_reply)
                {
                    pFileGameLock->game_lock.marker_balance = 0; // pay the player in cash if the stratus fails to respond


                    memset(&stratus_marker_credit, 0, sizeof(stratus_marker_credit));

                    stratus_marker_credit.ticket_id = pFileGameLock->game_lock.ticket_id;
                    stratus_marker_credit.player_account = pFileGameLock->game_lock.player_account;

                    if (pFileGameLock->game_lock.marker_balance > pFileGameLock->game_lock.ticket_amount)
                        stratus_marker_credit.credit_amount = pFileGameLock->game_lock.ticket_amount;
                    else
                        stratus_marker_credit.credit_amount = pFileGameLock->game_lock.marker_balance;

					stratus_marker_credit.transaction_id = (DWORD)marker_credit_transaction_id;

                    endian4(stratus_marker_credit.ticket_id);
                    endian4(stratus_marker_credit.player_account);
                    endian4(stratus_marker_credit.credit_amount);
					endian4(stratus_marker_credit.transaction_id);

                    memset(&stratus_msg, 0, sizeof(stratus_msg));

			        pGameDevice = theApp.GetGamePointer(pFileGameLock->game_lock.ucmc_id);
			        if (pGameDevice && pGameDevice->memory_game_config.club_code)
				        stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
			        else
				        stratus_msg.club_code = theApp.memory_location_config.club_code;

                    stratus_msg.send_code = MPI_ABORT_MARKER_CREDIT;
                    stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
                    stratus_msg.mux_id = pFileGameLock->game_lock.mux_id;
                    stratus_msg.ucmc_id = pFileGameLock->game_lock.ucmc_id;
                    stratus_msg.message_length = sizeof(stratus_marker_credit) + STRATUS_HEADER_SIZE;
                    stratus_msg.property_id = theApp.memory_location_config.property_id;
                    stratus_msg.data_to_event_log = true;
                    stratus_msg.customer_id = pFileGameLock->game_lock.location_id;
                    memcpy(stratus_msg.data, &stratus_marker_credit, sizeof(stratus_marker_credit));
                    theApp.marker_credit_reply = false;
                    theApp.QueueMarkerStratusTxMessage(&stratus_msg);

                    prt_buf.Format("\n\nMARKER PAYMENT FAILED\n");
                    theApp.MarkerPrinterData(prt_buf.GetBuffer(0));
                    prt_buf.Format("\n\nSYSTEM NOT RESPONDING\n");
                    theApp.MarkerPrinterData(prt_buf.GetBuffer(0));

                }
            }

            if (pFileGameLock->game_lock.marker_balance > pFileGameLock->game_lock.ticket_amount)
                dispense_amount = 0;
            else
                dispense_amount = pFileGameLock->game_lock.ticket_amount - pFileGameLock->game_lock.marker_balance;
        }

        if (dispense_amount > (theApp.memory_dispenser_config.dispenser_payout_limit * 100))
        {
            theApp.printer_busy = true;
            theApp.PrinterHeader(0);
            theApp.PrinterData("\n CALL FOR PAYMENT\n");
            theApp.PrinterId();

            prt_buf.Format("  TICKET:    %u\n", pFileGameLock->game_lock.ticket_id);
            theApp.PrinterData(prt_buf.GetBuffer(0));
            prt_buf.Format("  GAME #:     %u\n", pFileGameLock->game_lock.ucmc_id);
            theApp.PrinterData(prt_buf.GetBuffer(0));
            prt_buf.Format("  DISPENSE AMOUNT:    %9.2f\n\n", (double)dispense_amount / 100);
            theApp.PrinterData(prt_buf.GetBuffer(0));
            theApp.printer_busy = false;

            MessageWindow(false, "LARGE JACKPOT", "**** CALL FOR ****\n**** PAYMENT ****");
            ticket_id = pFileGameLock->game_lock.ticket_id;
        }

        if (!ticket_id)
        {
            memset(&file_paid_ticket, 0, sizeof(file_paid_ticket));
            memcpy(&file_paid_ticket.memory.memory_game_lock, &pFileGameLock->game_lock, sizeof(file_paid_ticket.memory.memory_game_lock));
            file_paid_ticket.memory.cashier_id = theApp.current_user.user.id;
            file_paid_ticket.memory.timestamp = time(NULL);

            // if taxable jackpot, get ss number and drivers license
            if (file_paid_ticket.memory.memory_game_lock.taxable_flag)
            {
                if (theApp.memory_dispenser_config.autopay_allowed || file_paid_ticket.memory.memory_game_lock.jackpot_to_credit_flag)
                {
                    if (!pFileGameLock->game_lock.ss_number || !pFileGameLock->game_lock.drivers_license)
                    {
                        got_tax_information = false;
                        pTaxInformation = new CTaxInformation(&got_tax_information,
							&file_paid_ticket.memory.memory_game_lock.ss_number, &file_paid_ticket.memory.memory_game_lock.drivers_license);
                        pTaxInformation->Create(IDD_CASHIER_LICENSE_SSN_DIALOG);

				        wait_timer = time(NULL) + 300;	// give 5 minutes to enter tax information

                        while (!got_tax_information)
				        {
					        if (wait_timer < time(NULL))
						        break;
					        else
					        {
						        theApp.OnMouseMove();
						        theApp.OnIdle(0);
					        }
				        }

                        pTaxInformation->DestroyWindow();
                        delete pTaxInformation;

						if (!file_paid_ticket.memory.memory_game_lock.ss_number || !file_paid_ticket.memory.memory_game_lock.drivers_license)
                        {
                            prt_buf.Format("INVALID SOCIAL SECURITY NUMBER: %u\nOR DRIVERS LICENSE: %u",
								file_paid_ticket.memory.memory_game_lock.ss_number, file_paid_ticket.memory.memory_game_lock.drivers_license);
                            MessageWindow(false, "TAX INFORMATION", prt_buf);
        //                    ticket_id = file_paid_ticket.memory.memory_game_lock.ticket_id;
            	            theApp.RemovePendingTicketFromFile(pFileGameLock->game_lock.ticket_id);
					        return false;
                        }
                        else
                        {
                            memset(&pending_ticket, 0, sizeof(pending_ticket));
                            pending_ticket.ticket_id = pFileGameLock->game_lock.ticket_id;
                            strcpy_s(pending_ticket.computer_name, sizeof(pending_ticket.computer_name), computer_name);
                            fileWrite(PENDING_TICKETS, -1, &pending_ticket, sizeof(pending_ticket));
                        }
                    }
                    else
                    {
                        memset(&pending_ticket, 0, sizeof(pending_ticket));
                        pending_ticket.ticket_id = pFileGameLock->game_lock.ticket_id;
                        strcpy_s(pending_ticket.computer_name, sizeof(pending_ticket.computer_name), computer_name);
                        fileWrite(PENDING_TICKETS, -1, &pending_ticket, sizeof(pending_ticket));
                    }
                }
                else
                {
                    theApp.PrinterData("\nCannot Pay The\nTaxable Jackpot.\nCall For Payment.\n\n");
                    MessageWindow(false, "AUTOPAY OFF", "CANNOT PAY JACKPOT\nCALL FOR PAYMENT");
                    ticket_id = file_paid_ticket.memory.memory_game_lock.ticket_id;
                }
            }
        }

        if (!ticket_id)
        {
            if (!theApp.memory_settings_ini.do_not_print_dispense_info)
            {
                theApp.printer_busy = true;
                theApp.PrinterHeader(0);
                theApp.PrinterData(" PAY GAME LOCK\n");
                theApp.PrinterId();

                if (file_paid_ticket.memory.memory_game_lock.taxable_flag)
                    prt_buf.Format("TICKET: %u JACKPOT\n", file_paid_ticket.memory.memory_game_lock.ticket_id);
                else
                    prt_buf.Format("TICKET: %u\n", file_paid_ticket.memory.memory_game_lock.ticket_id);
                theApp.PrinterData(prt_buf.GetBuffer(0));

                if (pFileGameLock->game_lock.jackpot_to_credit_flag)
                {
                    prt_buf.Format("PAY-TO-CREDITS: %7.2f\n", (double)file_paid_ticket.memory.memory_game_lock.ticket_amount / 100);
                    theApp.PrinterData(prt_buf.GetBuffer(0));
                }
                theApp.printer_busy = false;
            }


		    if (dispense_amount)
            {
                if (!theApp.memory_settings_ini.do_not_print_dispense_info)
                {
                    prt_buf.Format("CASH PAYOUT: %7.2f\n", (double)dispense_amount / 100);
                    theApp.PrinterData(prt_buf.GetBuffer(0));
                }

                if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_CASH_DRAWER)
                {
                    theApp.dispenserControl.memory_dispenser_values.tickets_amount += dispense_amount;
                    theApp.dispenserControl.memory_dispenser_values.number_tickets++;
                    theApp.dispenserControl.WriteDispenserFile();

                    theApp.OpenCashDrawer();
                    Sleep(1000);
                }
                else
                {
                    if (dispense_amount <= theApp.dispenserControl.memory_dispenser_values.dispense_grand_total -
                        theApp.dispenserControl.memory_dispenser_values.tickets_amount)
                    {
                        if (!theApp.memory_settings_ini.do_not_print_dispense_info)
                        {
                            prt_buf.Format("BILL DISPENSE NOT REQUIRED\nTICKET: %u\n", file_paid_ticket.memory.memory_game_lock.ticket_id);
                            theApp.PrinterData(prt_buf.GetBuffer(0));
                        }

                        theApp.dispenserControl.memory_dispenser_values.tickets_amount += dispense_amount;
                        theApp.dispenserControl.memory_dispenser_values.number_tickets++;
                        theApp.dispenserControl.WriteDispenserFile();
						BillMessage bill_message;
						memset(&bill_message, 0, sizeof(BillMessage));
                        bill_message.message_type = 'N';

                        switch (theApp.memory_dispenser_config.dispenser_type)
                        {
                            case DISPENSER_TYPE_JCM_20:
                            case DISPENSER_TYPE_LCDM1000_20:
                                bill_message.bill_amount_20 = theApp.dispenserControl.memory_dispenser_values.dispense_grand_total / 100;
                                bill_message.reject_amount_20 = theApp.dispenserControl.memory_dispenser_values.reject_bills1 * 20;
                                break;

                            case DISPENSER_TYPE_JCM_100:
                                bill_message.bill_amount_100 = theApp.dispenserControl.memory_dispenser_values.dispense_grand_total / 100;
                                bill_message.reject_amount_100 = theApp.dispenserControl.memory_dispenser_values.reject_bills1 * 100;
                                break;

                            case DISPENSER_TYPE_ECASH:
                            case DISPENSER_TYPE_CD2000:
                            case DISPENSER_TYPE_DIEBOLD:
                            case DISPENSER_TYPE_LCDM2000_20_100:
                            case DISPENSER_TYPE_LCDM4000_20_100:
                                bill_message.bill_amount_100 = theApp.dispenserControl.memory_dispenser_values.dispense_total1 / 100;
                                bill_message.bill_amount_20 = theApp.dispenserControl.memory_dispenser_values.dispense_total2 / 100;
                                bill_message.reject_amount_100 = theApp.dispenserControl.memory_dispenser_values.reject_bills1 * 100;
                                bill_message.reject_amount_20 = theApp.dispenserControl.memory_dispenser_values.reject_bills2 * 20;
                                break;
                        }

						theApp.SendIdcDispenseInfoMessage (&bill_message, pFileGameLock->game_lock.ucmc_id);

                    }
                    else
                    {
                        if (!theApp.memory_settings_ini.do_not_print_dispense_info)
                            theApp.PrinterData("DISPENSE BILLS\n");
                    
                        if (pFileGameLock->game_lock.mux_id)
                            id = pFileGameLock->game_lock.mux_id;
                        else
                            id = pFileGameLock->game_lock.sas_id;


                        if (theApp.dispenserControl.DispenseBills(file_paid_ticket.memory.memory_game_lock.ticket_id, dispense_amount,
                                                                  pFileGameLock->game_lock.ucmc_id, id))
                            theApp.SendDispenserBillCount(false, pFileGameLock->game_lock.ucmc_id);
                        else
                        {
                            if (!theApp.memory_settings_ini.do_not_print_dispense_info)
                                theApp.PrinterData("PROBLEM DURING DISPENSE.\n");
                            ticket_id = file_paid_ticket.memory.memory_game_lock.ticket_id;
                        }
                    }
                }
            }
        }

        if (!ticket_id)
        {
            // send game unlock message (also clears game lock file)
            memset(&game_unlock_message, 0, sizeof(game_unlock_message));
            game_unlock_message.ticket_id = file_paid_ticket.memory.memory_game_lock.ticket_id;
            game_unlock_message.ucmc_id = file_paid_ticket.memory.memory_game_lock.ucmc_id;
            game_unlock_message.ticket_amount = file_paid_ticket.memory.memory_game_lock.ticket_amount;
            game_unlock_message.marker_amount = file_paid_ticket.memory.memory_game_lock.marker_balance;
            game_unlock_message.jackpot_to_credit_flag = file_paid_ticket.memory.memory_game_lock.jackpot_to_credit_flag;

            if (file_paid_ticket.memory.memory_game_lock.taxable_flag)
            {
                fileWrite(W2G_RECORDS, -1, &file_paid_ticket, sizeof(file_paid_ticket));
                fileWrite(LOCATION_W2G_RECORDS, -1, &file_paid_ticket, sizeof(file_paid_ticket));
            }

            if (!theApp.memory_settings_ini.masters_ip_address)
                theApp.user_interface_game_unlock_queue.push(game_unlock_message);
            else
            {
                memset(&stratus_msg, 0, sizeof(stratus_msg));

			    pGameDevice = theApp.GetGamePointer(file_paid_ticket.memory.memory_game_lock.ucmc_id);
			    if (pGameDevice && pGameDevice->memory_game_config.club_code)
				    stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
			    else
				    stratus_msg.club_code = theApp.memory_location_config.club_code;

                stratus_msg.message_length = sizeof(game_unlock_message) + STRATUS_HEADER_SIZE;
                memcpy(stratus_msg.data, &game_unlock_message, sizeof(game_unlock_message));
                stratus_msg.send_code = SLAVE_TKT_GAME_UNLOCK;
                stratus_msg.task_code = SLAVE_TKT_TASK_TICKET;
                stratus_msg.mux_id = CONTROLLER_MUX_ADDRESS;
                stratus_msg.ucmc_id = file_paid_ticket.memory.memory_game_lock.ucmc_id;
                stratus_msg.property_id = theApp.memory_location_config.property_id;
                stratus_msg.customer_id = file_paid_ticket.memory.memory_game_lock.location_id;
                stratus_msg.data_to_event_log = true;
                theApp.QueueStratusTxMessage(&stratus_msg, FALSE);
            }

// MOVING FROM HERE

            // send Marker message to Stratus
            if (file_paid_ticket.memory.memory_game_lock.jackpot_to_credit_flag)
                SendPayToCreditMessageToStratus(&file_paid_ticket.memory);
            else if (file_paid_ticket.memory.memory_game_lock.marker_balance)
            {

                prt_buf.Format("MARKER BALANCE: %7.2f\n", (double)file_paid_ticket.memory.memory_game_lock.marker_balance / 100);
                theApp.PrinterData(prt_buf.GetBuffer(0));
                prt_buf.Format("MARKER PAID: %7.2f\n", (double)credit_amount / 100);
                theApp.PrinterData(prt_buf.GetBuffer(0));

                if (file_paid_ticket.memory.memory_game_lock.marker_balance > file_paid_ticket.memory.memory_game_lock.ticket_amount)
                    new_balance_due = file_paid_ticket.memory.memory_game_lock.marker_balance - file_paid_ticket.memory.memory_game_lock.ticket_amount;
                else
                    new_balance_due = 0;

                prt_buf.Format("\n\nPAYMENT RECEIPT\n");
                theApp.MarkerPrinterData(prt_buf.GetBuffer(0));
                theApp.PrinterHeader(2);
                prt_buf.Format("\nLOCATION ID: %u", file_paid_ticket.memory.memory_game_lock.location_id);
                theApp.MarkerPrinterData(prt_buf.GetBuffer(0));
                prt_buf.Format("\nMACHINE ID: %u", file_paid_ticket.memory.memory_game_lock.ucmc_id);
                theApp.MarkerPrinterData(prt_buf.GetBuffer(0));
                prt_buf.Format("\nTICKET ID: %u\n", file_paid_ticket.memory.memory_game_lock.ticket_id);
                theApp.MarkerPrinterData(prt_buf.GetBuffer(0));
                if (marker_credit_transaction_id)
                {
                    prt_buf.Format("\nTRANSACTION ID: %u%u\n", file_paid_ticket.memory.memory_game_lock.ucmc_id, file_paid_ticket.memory.memory_game_lock.ticket_id);
                    theApp.MarkerPrinterData(prt_buf.GetBuffer(0));

                }
	            prt_buf.Format("\n\nNAME:    %s", file_paid_ticket.memory.memory_game_lock.player_name);
	            theApp.MarkerPrinterData(prt_buf.GetBuffer(0));
                prt_buf.Format("\nACCOUNT: %u\n", file_paid_ticket.memory.memory_game_lock.player_account);
                theApp.MarkerPrinterData(prt_buf.GetBuffer(0));

                prt_buf.Format("\n\nMARKER PAY AMOUNT:           $%9.2f", (double)file_paid_ticket.memory.memory_game_lock.ticket_amount / 100);
                theApp.MarkerPrinterData(prt_buf.GetBuffer(0));
                prt_buf.Format("\n\nPREVIOUS BALANCE DUE:      $%9.2f", (double)file_paid_ticket.memory.memory_game_lock.marker_balance / 100);
                theApp.MarkerPrinterData(prt_buf.GetBuffer(0));

                if (pFileGameLock->game_lock.marker_balance > pFileGameLock->game_lock.ticket_amount)
                    prt_buf.Format("\nPAYMENT AMOUNT:            $%9.2f", (double)pFileGameLock->game_lock.ticket_amount / 100);
                else
                    prt_buf.Format("\nPAYMENT AMOUNT:            $%9.2f", (double)pFileGameLock->game_lock.marker_balance / 100);
                theApp.MarkerPrinterData(prt_buf.GetBuffer(0));

                prt_buf.Format("\nNEW BALANCE DUE:           $%9.2f", (double)new_balance_due / 100);
                theApp.MarkerPrinterData(prt_buf.GetBuffer(0));

                if (dispense_amount)
                {
                    prt_buf.Format("\n\nCASH PAID TO CUSTOMER:    $%9.2f", (double)dispense_amount / 100);
                    theApp.MarkerPrinterData(prt_buf.GetBuffer(0));
                }

                prt_buf.Format("\n\nORIGINAL MARKER AMOUNT:    $%9.2f", (double)theApp.stratus_marker_credit_response.credit_amount / 100);
                theApp.MarkerPrinterData(prt_buf.GetBuffer(0));
                theApp.MarkerPrinterData("\nAVAILABLE WAGERING");
                prt_buf.Format("\n     ACCOUNT BALANCE:      $%9.2f", (double)(theApp.stratus_marker_credit_response.credit_amount - new_balance_due) / 100);
                theApp.MarkerPrinterData(prt_buf.GetBuffer(0));

	            prt_buf.Format("\n\n\nPlayer Signature: \n\n");
                theApp.MarkerPrinterData(prt_buf.GetBuffer(0));
	            prt_buf.Format("\n_______________________________\n\n");
                theApp.MarkerPrinterData(prt_buf.GetBuffer(0));

	            prt_buf.Format("\n\n\nCashier Signature: \n\n");
                theApp.MarkerPrinterData(prt_buf.GetBuffer(0));
	            prt_buf.Format("\n_______________________________\n\n");
                theApp.MarkerPrinterData(prt_buf.GetBuffer(0));

                theApp.MarkerPrinterData("\n\n\n\n\n\n\n\n\n\n\n");

            }

            if (!theApp.memory_settings_ini.do_not_print_dispense_info)
            {
                prt_buf.Format("GAME #: %u\n", file_paid_ticket.memory.memory_game_lock.ucmc_id);
                theApp.PrinterData(prt_buf.GetBuffer(0));
            }

            writeRedundantFile((CString)PAID_TICKETS, -1, (BYTE*)&file_paid_ticket, sizeof(file_paid_ticket));
            fileWrite(PAID_TICKETS_TX, -1, &file_paid_ticket, sizeof(file_paid_ticket));
            fileWriteSizeLimited(DATED_PAID_TICKETS, &file_paid_ticket, sizeof(file_paid_ticket));


            if (dispense_amount)
            {
                if (!theApp.memory_settings_ini.masters_ip_address)
                {
                    memset(&drop_ticket, 0, sizeof(drop_ticket));
                    drop_ticket.ticket_id = file_paid_ticket.memory.memory_game_lock.ticket_id;
                    drop_ticket.ucmc_id = file_paid_ticket.memory.memory_game_lock.ucmc_id;
                    drop_ticket.amount = dispense_amount;

                    drop_ticket.customer_id = file_paid_ticket.memory.memory_game_lock.location_id;

                    writeRedundantFile((CString)DROP_TICKETS, -1, (BYTE*)&drop_ticket, sizeof(drop_ticket));

                    record_index = fileGetRecord(GAME_REDEEM_RECORDS, &redeem_record, sizeof(redeem_record), file_paid_ticket.memory.memory_game_lock.ucmc_id);
                    if (record_index >= 0)
                    {
                        redeem_record.amount += dispense_amount;
                        writeRedundantFile((CString)GAME_REDEEM_RECORDS, record_index, (BYTE*)&redeem_record, sizeof(redeem_record));
                    }
                    else
                    {
                        memset(&redeem_record, 0, sizeof(redeem_record));
                        redeem_record.ucmc_id = file_paid_ticket.memory.memory_game_lock.ucmc_id;
                        redeem_record.amount = dispense_amount;
                        writeRedundantFile((CString)GAME_REDEEM_RECORDS, -1, (BYTE*)&redeem_record, sizeof(redeem_record));
                    }
                }

                if (theApp.memory_settings_ini.force_cashier_shifts)
                {
                    prt_buf.Format("%s", SHIFT_TICKETS_1);

                    fileWrite(prt_buf.GetBuffer(0), -1, &file_paid_ticket.memory.memory_game_lock.ticket_id, 4);
                    fileWrite(prt_buf.GetBuffer(0), -1, &dispense_amount, 4);

                    // check file size, shut off if not being used
                    if (fileRecordTotal(prt_buf.GetBuffer(0), 8) > 1000)
                    {
                        theApp.memory_settings_ini.force_cashier_shifts = 0;
                        fileWrite(MAIN_SETTINGS_INI, 0, &theApp.memory_settings_ini, sizeof(theApp.memory_settings_ini));
                        DeleteFile(prt_buf.GetBuffer(0));
                    }

                    prt_buf = CASHIER_SHIFT_1;

                    if (!fileRead(prt_buf.GetBuffer(0), 0, &file_cashier_shift, sizeof(file_cashier_shift)))
                        memset(&file_cashier_shift, 0, sizeof(file_cashier_shift));

                    file_cashier_shift.redeem_amount += dispense_amount;
                    file_cashier_shift.number_tickets++;
                    fileWrite(prt_buf.GetBuffer(0), 0, &file_cashier_shift, sizeof(file_cashier_shift));

                    if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_CASH_DRAWER)
                    {
                        prt_buf.Format(" CASH DRAWER: $%7.2f\n", ((double)file_cashier_shift.beginning_balance -
                            (double)file_cashier_shift.redeem_amount + (double)file_cashier_shift.active_shift_fills +
                            (double)file_cashier_shift.mgr_adjust_plus - (double)file_cashier_shift.mgr_adjust_minus) / 100);
                        theApp.PrinterData(prt_buf.GetBuffer(0));
                    }
                }

                if (!theApp.memory_settings_ini.do_not_print_dispense_info && (theApp.memory_dispenser_config.dispenser_type != DISPENSER_TYPE_CASH_DRAWER))
                {
                    prt_buf.Format("BREAKAGE: %9.2f\n", (double)(theApp.dispenserControl.memory_dispenser_values.dispense_grand_total -
					    theApp.dispenserControl.memory_dispenser_values.tickets_amount) / 100);
                    theApp.PrinterData(prt_buf.GetBuffer(0));
                }
            }

            if (file_paid_ticket.memory.memory_game_lock.mux_id)
                message_display.Format("TICKET ID: %u\nMACHINE ID (PORT-MUX/SAS): %u (%u-%u)\n",
                    file_paid_ticket.memory.memory_game_lock.ticket_id, file_paid_ticket.memory.memory_game_lock.ucmc_id,
                    file_paid_ticket.memory.memory_game_lock.game_port, file_paid_ticket.memory.memory_game_lock.mux_id);
            else
                message_display.Format("TICKET ID: %u\nMACHINE ID (PORT-MUX/SAS): %u (%u-%u)\n",
                    file_paid_ticket.memory.memory_game_lock.ticket_id, file_paid_ticket.memory.memory_game_lock.ucmc_id,
                    file_paid_ticket.memory.memory_game_lock.game_port, file_paid_ticket.memory.memory_game_lock.sas_id);

            if (file_paid_ticket.memory.memory_game_lock.jackpot_to_credit_flag)
            {
                prt_buf.Format("PAY TO CREDIT: $%7.2f\n", (double)file_paid_ticket.memory.memory_game_lock.ticket_amount / 100);
                message_display += prt_buf;
            }
            else
            {
                prt_buf.Format("TICKET AMOUNT: $%7.2f\n", (double)file_paid_ticket.memory.memory_game_lock.ticket_amount / 100);
                message_display += prt_buf;

                if (file_paid_ticket.memory.memory_game_lock.marker_balance)
                {
                    prt_buf.Format("MARKER AMOUNT: $%7.2f\n", (double)file_paid_ticket.memory.memory_game_lock.marker_balance / 100);
                    message_display += prt_buf;

                    if (file_paid_ticket.memory.memory_game_lock.marker_balance > file_paid_ticket.memory.memory_game_lock.ticket_amount)
                        prt_buf.Format("MARKER PAYMENT: $%7.2f\n", (double)file_paid_ticket.memory.memory_game_lock.ticket_amount / 100);
                    else
                        prt_buf.Format("MARKER PAYMENT: $%7.2f\n", (double)file_paid_ticket.memory.memory_game_lock.marker_balance / 100);
                    message_display += prt_buf;
                }

                prt_buf.Format("CASH PAYMENT: $%7.2f\n", (double)dispense_amount / 100);
                message_display += prt_buf;
            }

            theApp.SendIdcGameLockCompleteMessage (&file_paid_ticket);

            MessageWindow(false, "PROCESS GAME LOCK", message_display);

        	if (theApp.memory_settings_ini.idc_ip_address && theApp.memory_settings_ini.idc_tcp_port && !(GetFileAttributes(GAME_TENDER_FILE) == 0xFFFFFFFF))
            {
                number_of_game_locks = 0;
                for (iii=0; iii < MAXIMUM_GAME_LOCKS_LIST; iii++)
                {
                    if (game_lock_list[iii].ucmc_id)
                        number_of_game_locks++;
                    else
                        break;
                }

            	if (number_of_game_locks == 1)
                {
                    while (theApp.message_window_queue.empty())
                        theApp.OnIdle(0);

                    if (!theApp.IdcSocket.socket_state || (theApp.IdcSocket.socket_state == WSAEISCONN))
    		    	    ShellExecute(NULL, "open", GAME_TENDER_FILE, NULL, NULL, SW_SHOWMAXIMIZED);
                }
            }
        }

	    if (ticket_id)
	    {
	        theApp.RemovePendingTicketFromFile(ticket_id);
		    return false;
	    }
	    else
	    {
            if (!theApp.memory_settings_ini.masters_ip_address)
    	        theApp.RemovePendingTicketFromFile(file_paid_ticket.memory.memory_game_lock.ticket_id);
		        return true;
        }

        if(!theApp.memory_settings_ini.do_not_print_dispense_info)
        {
            theApp.PrinterData("Transaction Completed.\n\n\n");	
            theApp.printer_busy = false;
        }
    }
    else
    {
        logMessage("CGameLockDialog::PayGameLock -  dispenser payout NOT complete");
    }
}

void CGameLockDialog::ProcessButtonClick(BYTE button_press)
{
 char filename[MAX_PATH];
 BYTE game_lock_index = game_list_index + button_press;
 time_t offline_timer, second_display;
 CTimerWindow *pTimerWindow;
 StratusMessageFormat stratus_msg;
 CString prt_buf;
 FileGameLock file_game_lock;
 bool successful_pay = false;

    if (game_lock_index < MAXIMUM_GAME_LOCKS_LIST && game_lock_list[game_lock_index].ucmc_id)
    {
        DestroyWindow();

        memset(&file_game_lock, 0, sizeof(file_game_lock));

        if (!theApp.memory_settings_ini.masters_ip_address)
        {
            if (theApp.GetGameLockFilename(game_lock_list[game_lock_index].ucmc_id, game_lock_list[game_lock_index].ticket_amount,
                filename) && fileRead(filename, 0, &file_game_lock, sizeof(file_game_lock)))
			{
//				DeleteFile(PENDING_GAMELOCK);
//				MoveFile(filename, PENDING_GAMELOCK);

                if (theApp.memory_settings_ini.idc_ip_address && theApp.memory_settings_ini.idc_tcp_port
                    && (!theApp.IdcSocket.socket_state || (theApp.IdcSocket.socket_state == WSAEISCONN)))
                {
                    if (file_game_lock.game_lock.taxable_flag && !file_game_lock.game_lock.ss_number && !theApp.memory_settings_ini.bypass_game_tender_automated_w2g)
                    {
                        MessageWindow(false, "GAME TENDER AUTOMATED W2G REQUIRED", "PLEASE ENTER TAX INFO FOR THIS PAYOUT");
                    }
                    else
                    {
                        successful_pay = PayGameLock(&file_game_lock, theApp.computer_name);
				        if (successful_pay)
					        DeleteFile(PENDING_GAMELOCK);
				        else
					        MoveFile(PENDING_GAMELOCK, filename);
                    }
                }
                else
                {
                    successful_pay = PayGameLock(&file_game_lock, theApp.computer_name);
				    if (successful_pay)
					    DeleteFile(PENDING_GAMELOCK);
				    else
					    MoveFile(PENDING_GAMELOCK, filename);
                }
			}
            else
            {
                memset(&file_game_lock, 0, sizeof(file_game_lock));
                PrintTicketErrorMessage(game_lock_list[game_lock_index].ticket_id, game_lock_list[game_lock_index].ticket_amount,
                    game_lock_list[game_lock_index].ucmc_id, TICKET_ALREADY_PAID);
            }
        }
        else
        {
            while (!theApp.stratus_dialog_queue.empty())
                theApp.stratus_dialog_queue.pop();

            if (!theApp.stratusSocket.socket_state)
            {
                memset(&stratus_msg, 0, sizeof(stratus_msg));

				CGameDevice *pGameDevice = theApp.GetGamePointer(theApp.memory_dispenser_config.ucmc_id);
				if (pGameDevice && pGameDevice->memory_game_config.club_code)
					stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
				else
					stratus_msg.club_code = theApp.memory_location_config.club_code;
                stratus_msg.message_length = 23 + STRATUS_HEADER_SIZE;
                memcpy(stratus_msg.data, &game_lock_list[game_lock_index].ucmc_id, 4);
                memcpy(&stratus_msg.data[4], &game_lock_list[game_lock_index].ticket_amount, 4);
                memcpy(&stratus_msg.data[8], theApp.computer_name, 10);
                memcpy(&stratus_msg.data[18], &theApp.current_user, sizeof(theApp.current_user));


                stratus_msg.send_code = SLAVE_TKT_TICKET_QUERY;
                stratus_msg.task_code = SLAVE_TKT_TASK_TICKET;
                stratus_msg.mux_id = CONTROLLER_MUX_ADDRESS;
                stratus_msg.ucmc_id = theApp.memory_dispenser_config.ucmc_id;
                stratus_msg.property_id = theApp.memory_location_config.property_id;
                stratus_msg.customer_id = theApp.memory_location_config.customer_id;
                stratus_msg.data_to_event_log = true;

                theApp.QueueStratusTxMessage(&stratus_msg, FALSE);

                pTimerWindow = new CTimerWindow;
                pTimerWindow->Create(IDD_TIMER_BAR_DIALOG);
                pTimerWindow->m_timer_window_bar.SetRange(0, OFFLINE_TIMER_LIMIT);
                pTimerWindow->SetDlgItemText(IDC_TIMER_WINDOW_TITLE_STATIC, "Getting Game Lock Data, Please Wait...");
                offline_timer = time(NULL) + OFFLINE_TIMER_LIMIT;
                second_display = 0;
                EnableWindow(false);

                while (offline_timer > time(NULL) && theApp.stratus_dialog_queue.empty())
                {
                    if (second_display != time(NULL))
                    {
                        pTimerWindow->m_timer_window_bar.SetPos((int)(time(NULL) + OFFLINE_TIMER_LIMIT - offline_timer));
                        second_display = time(NULL);
                    }
                    else
                        theApp.OnIdle(0);
                }

                pTimerWindow->DestroyWindow();
                delete pTimerWindow;
                EnableWindow();
            }

            if (theApp.stratus_dialog_queue.empty())
            {
                theApp.printer_busy = true;
                theApp.PrinterHeader(0);
                theApp.PrinterData(" Call Service Now.\n");
                theApp.PrinterId();
                prt_buf.Format(" UCMC ID:  %u\nUnable To Pay.\nThe Remote System is Offline.\n\n", game_lock_list[game_lock_index].ucmc_id);
                theApp.PrinterData(prt_buf.GetBuffer(0));
                theApp.printer_busy = false;
                MessageWindow(false, "SYSTEM OFFLINE", "Unable To Pay\nCall For Service");
            }
            else
            {
                stratus_msg = theApp.stratus_dialog_queue.front();
                theApp.stratus_dialog_queue.pop();

                if (stratus_msg.task_code == SLAVE_TKT_TASK_TICKET && stratus_msg.send_code == SLAVE_TKT_TICKET_QUERY)
                {
                    memcpy(&file_game_lock.game_lock, stratus_msg.data, sizeof(file_game_lock.game_lock));

                    switch (file_game_lock.game_lock.status)
                    {
                        case TICKET_PAYABLE_ASCII:
                            if (game_lock_list[game_lock_index].ucmc_id == file_game_lock.game_lock.ucmc_id &&
                                game_lock_list[game_lock_index].ticket_amount == file_game_lock.game_lock.ticket_amount &&
                                game_lock_list[game_lock_index].marker_balance == file_game_lock.game_lock.marker_balance &&
                                game_lock_list[game_lock_index].game_port == file_game_lock.game_lock.game_port &&
                                game_lock_list[game_lock_index].mux_id == file_game_lock.game_lock.mux_id &&
                                game_lock_list[game_lock_index].sas_id == file_game_lock.game_lock.sas_id &&
                                game_lock_list[game_lock_index].jackpot_to_credit_flag == file_game_lock.game_lock.jackpot_to_credit_flag)
                                    successful_pay = PayGameLock(&file_game_lock, theApp.computer_name, (DWORD*)&stratus_msg.data[sizeof(file_game_lock.game_lock)]);
                            else
                            {
                                prt_buf.Format("* INVALID GAME LOCK *\nTicket ID: %u\n(display value, file value)\nUCMC ID: %u, %u\n"
                                    "Cash Amount: %9.2f, %9.2f\nMarker Amount: %9.2f, %9.2f\nGame port: %u, %u\nMux ID: %u,%u\nJackpot To Credit: %u, %u",
                                    file_game_lock.game_lock.ticket_id, game_lock_list[game_lock_index].ucmc_id, file_game_lock.game_lock.ucmc_id,
                                    (double)game_lock_list[game_lock_index].ticket_amount / 100, (double)file_game_lock.game_lock.ticket_amount / 100,
                                    (double)game_lock_list[game_lock_index].marker_balance / 100, (double)file_game_lock.game_lock.marker_balance / 100,
                                    game_lock_list[game_lock_index].game_port, file_game_lock.game_lock.game_port,
                                    game_lock_list[game_lock_index].mux_id, file_game_lock.game_lock.mux_id,
                                    game_lock_list[game_lock_index].jackpot_to_credit_flag, file_game_lock.game_lock.jackpot_to_credit_flag);
                                MessageWindow(false, "GAME LOCK", prt_buf);
                            }
                            break;

                        case TICKET_PAY_PENDING_ASCII:
                            MessageWindow(false, "TICKET PAY PENDING", "THE PAY IS PENDING ON THE MASTER");
                            break;

                        case TICKET_NOT_FOUND_ASCII:
                            MessageWindow(false, "PAY TICKET", "* GAME NOT LOCKED *");
                            break;

                        case STRATUS_FILE_ERROR_ASCII_2:
                            PrintTicketErrorMessage(file_game_lock.game_lock.ticket_id, game_lock_list[game_lock_index].ticket_amount,
                                game_lock_list[game_lock_index].ucmc_id, SYSTEM_FILE_ERROR);
                            break;

                        case TICKET_ALREADY_PAID_ASCII:
                            PrintTicketErrorMessage(file_game_lock.game_lock.ticket_id, game_lock_list[game_lock_index].ticket_amount,
                                game_lock_list[game_lock_index].ucmc_id, TICKET_ALREADY_PAID);
                            break;

                        case INVALID_TICKET_STATUS_ASCII:
                            PrintTicketErrorMessage(file_game_lock.game_lock.ticket_id, game_lock_list[game_lock_index].ticket_amount,
                                game_lock_list[game_lock_index].ucmc_id, TICKET_PAY_PENDING);
                            DestroyWindow();
                            return;

                        case INVALID_USER_LOGIN_ID_ASCII:
                            PrintTicketErrorMessage(file_game_lock.game_lock.ticket_id, game_lock_list[game_lock_index].ticket_amount,
                                game_lock_list[game_lock_index].ucmc_id, INVALID_USER_LOGIN_ID);
                            DestroyWindow();
                            break;

                        default:
                            PrintTicketErrorMessage(file_game_lock.game_lock.ticket_id, game_lock_list[game_lock_index].ticket_amount,
                                game_lock_list[game_lock_index].ucmc_id, INVALID_TICKET_STATUS);
                            break;
                    }
                }
                else
                    PrintTicketErrorMessage(file_game_lock.game_lock.ticket_id, game_lock_list[game_lock_index].ticket_amount,
                        game_lock_list[game_lock_index].ucmc_id, INVALID_TICKET_STATUS);
            }
        }

        if (successful_pay)
        {
            theApp.RemovePendingTicketFromFile(file_game_lock.game_lock.ticket_id);
        }
        // once the cashier is in the payout screen, shut off the noise and the light flashing
        theApp.flash_active_home_light = FALSE;
    }
}

void CGameLockDialog::OnBnClickedGamelockButton1()
{
    ProcessButtonClick(0);
}

void CGameLockDialog::OnBnClickedGamelockButton2()
{
    ProcessButtonClick(1);
}

void CGameLockDialog::OnBnClickedGamelockButton3()
{
    ProcessButtonClick(2);
}

void CGameLockDialog::OnBnClickedGamelockButton4()
{
    ProcessButtonClick(3);
}

void CGameLockDialog::OnMouseMove(UINT nFlags, CPoint point)
{
	theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

void CGameLockDialog::OnBnClickedGamelockExitButton()
{
	DestroyWindow();
}

void CGameLockDialog::OnBnClickedGamelockMoreButton()
{
 BYTE total_game_locks = 0;
 int iii;

    for (iii=0; iii < MAXIMUM_GAME_LOCKS_LIST; iii++)
    {
        if (game_lock_list[iii].ucmc_id)
            total_game_locks++;
        else
            break;
    }

    game_list_index += 4;

    if (game_list_index > total_game_locks)
        game_list_index = 0;

    UpdateGameLockButtons();
}

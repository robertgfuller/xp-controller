#pragma once
#include "afxwin.h"


// CRasConnectionStatus dialog

class CRasConnectionStatus : public CDialog
{
	DECLARE_DYNAMIC(CRasConnectionStatus)

public:
	CRasConnectionStatus(CWnd* pParent = NULL);   // standard constructor
	virtual ~CRasConnectionStatus();
	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_RAS_STATUS_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

// public variables
public:
	CListBox m_ras_status_listbox;

	bool display_window_active;

// public functions
public:
	afx_msg void OnBnClickedRasConnectionStatusOkButton();
};

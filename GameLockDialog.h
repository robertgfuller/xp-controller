/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   10/14/08    Avoid displaying the ticket pay pending tilt if the controller paying the ticket is the same as the
                            controller that logged the pending ticket.
Dave Elquist    07/15/05    Changed game lock list transfer to class.
Dave Elquist    06/09/05    Initial Revision.
*/


#pragma once
#include "afxwin.h"


// CGameLockDialog dialog

class CGameLockDialog : public CDialog
{
	DECLARE_DYNAMIC(CGameLockDialog)

public:
	CGameLockDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CGameLockDialog();
	virtual BOOL OnInitDialog();

	afx_msg void OnBnClickedGamelockButton1();
	afx_msg void OnBnClickedGamelockButton2();
	afx_msg void OnBnClickedGamelockButton3();
	afx_msg void OnBnClickedGamelockButton4();
	afx_msg void OnBnClickedGamelockExitButton();
	afx_msg void OnBnClickedGamelockMoreButton();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);

// Dialog Data
	enum { IDD = IDD_GAMELOCK_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	afx_msg void OnPaint();

	DECLARE_MESSAGE_MAP()

// public variables
public:
    DisplayGameLock game_lock_list[MAXIMUM_GAME_LOCKS_LIST];

// private functions
private:
    void UpdateGameLockButtons();
    void ProcessButtonClick(BYTE button_press);
    void SendPayToCreditMessageToStratus(MemoryPaidTicket *pMemoryPaidTicket);
    void PrintTicketErrorMessage(DWORD ticket_id, DWORD ticket_amount, DWORD ucmc_id, DWORD state);
    bool PayGameLock(FileGameLock *pFileGameLock, char* computer_name, DWORD *pMarkerBalance = NULL);

// private variables
private:
    BYTE game_list_index;

	CButton m_game_lock_button1;
	CButton m_game_lock_button2;
	CButton m_game_lock_button3;
	CButton m_game_lock_button4;
	CButton m_gamelock_more_button;
};

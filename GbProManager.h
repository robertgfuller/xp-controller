/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   03/08/12    Call function to process GBA Configurator rx/tx messages from on idle instead of printer processing com thread.
Robert Fuller	02/05/13	Only log iGBA win transactions, not GBA win transactions.
Robert Fuller   06/08/11    Added mods to support GB with the XP/SAS direct connect feature.
Robert Fuller   07/09/09    Support new GB Advantage protocol features (scalable award, init categories after disconnect, custom drawing ticket text)
Robert Fuller   05/01/09    Removed code associated with now defunct Million Dollar Ticket lottery system.
Robert Fuller   04/07/09    Adjusted the card data info message processor to accommodate the fixed block size sent.
Robert Fuller   01/29/09    Added code to support the new GB Advantage technician screen.
Robert Fuller   01/22/09    Added code to process the new card info block info message.
Robert Fuller   11/13/08    Compare current hand data with previous hand data message to filter out duplicates.
Robert Fuller   10/15/08    Make sure that we have the right game device when evaluating hand data for GB Advantage wins.
Robert Fuller   09/17/08    Added code to utilize the buzzer feature on the ticket printer.
Robert Fuller   08/19/08    Added code to support the GB Advantage feature.
Dave Elquist    06/15/05    Initial Revision.
*/

#pragma once

#include "afxwin.h"


#pragma pack(1)
struct MessageReceivedAck
{
	BYTE	header;
	BYTE 	message_id;
	BYTE	length;
	BYTE	command;
	DWORD	customer_id;
	DWORD	error_code;
	BYTE	crc;
};
#pragma pack()

#pragma pack(1)
struct SetDrawingTicketTextResponse
{
	BYTE	header;
	BYTE 	message_id;
	BYTE	length;
	BYTE	command;
	DWORD	customer_id;
	DWORD	error_code;
	BYTE	crc;
};
#pragma pack()

#pragma pack(1)
struct RequestInitialization
{
	BYTE	header;
	BYTE 	message_id;
	BYTE	length;
	BYTE	command;
	DWORD	customer_id;
	BYTE	crc;
};
#pragma pack()

typedef enum PROMOTION_CODE
{
	ANY_FOUR_OF_A_KIND,
	FOUR_OF_A_KIND_TWOS,
	FOUR_OF_A_KIND_THREES,
	FOUR_OF_A_KIND_FOURS,
	FOUR_OF_A_KIND_FIVES,
	FOUR_OF_A_KIND_SIXES,
	FOUR_OF_A_KIND_SEVENS,
	FOUR_OF_A_KIND_EIGHTS,
	FOUR_OF_A_KIND_NINES,
	FOUR_OF_A_KIND_TENS,
	FOUR_OF_A_KIND_JACKS,
	FOUR_OF_A_KIND_QUEENS,
	FOUR_OF_A_KIND_KINGS,
	FOUR_OF_A_KIND_ACES,
	STRAIGHT_FLUSH_CLUBS,
	STRAIGHT_FLUSH_DIAMONDS,
	STRAIGHT_FLUSH_HEARTS,
	STRAIGHT_FLUSH_SPADES,
	ROYAL_FLUSH_CLUBS,
	ROYAL_FLUSH_DIAMONDS,
	ROYAL_FLUSH_HEARTS,
	ROYAL_FLUSH_SPADES,
	FULL_HOUSE_TWOS,
	FULL_HOUSE_THREES,
	FULL_HOUSE_FOURS,
	FULL_HOUSE_FIVES,
	FULL_HOUSE_SIXES,
	FULL_HOUSE_SEVENS,
	FULL_HOUSE_EIGHTS,
	FULL_HOUSE_NINES,
	FULL_HOUSE_TENS,
	FULL_HOUSE_JACKS,
	FULL_HOUSE_QUEENS,
	FULL_HOUSE_KINGS,
	FULL_HOUSE_ACES,
	ANY_STRAIGHT_FLUSH,
	ANY_ROYAL_FLUSH,
	ANY_FULL_HOUSE,
};


// CGbProManager command target

class CGbProManager
{
// public functions
public:
	CGbProManager();
	virtual ~CGbProManager();

    void ManagerRequestedTicket();
    void ProcessGbProRequests();
	void CheckGbProPorts();
	void UpdateGbProDisplay (void);
	void sendGbProMediaBoxPlayAnimationMessage(ULONG points, ULONG cash_voucher_amount, UINT scalable);
	void shutdownMediaBoxApp (void);
	void ProcessSetPromotionWinCategory (BYTE *buffer, BYTE message_id, bool send_to_idc);
    void ProcessCvPrinterReset();

    void ResetCvPrinter();

	void turnOnOffCvPrinterBuzzer(bool buzzer_on);

    // GB Pro Media Box server functions
	void CheckGbProMediaBoxServerTransmit();

	bool winCategoryActive (int win_category_index, DWORD account_number, NewBlock* block);
	bool winCategoryActive (int win_category_index);
	GbProWinCategory getWinCategory (UINT category);

	bool isGbaHandPromotion (WORD promotion_id);

	char* GetGbProPointsTicketsCashPromoString(WORD promotion_id);

    // GB Pro Configurator server functions
	void CheckGbProConfiguratorServerReceive();
	void CheckGbProConfiguratorServerTransmit();
	void OpenGbProConfiguratorServerSocket();

// public variables
public:
    MemoryGbProManagerSettings  memory_gb_pro_settings;

    BYTE cv_printer_idle_msg_status_bits[5];
	bool configurator_connected;

    // queues
    queue <CashVoucherTickets, deque<CashVoucherTickets> > cv_pending_printer_tickets_queue;
    queue <PrinterMessage, deque<PrinterMessage> > cv_printer_queue;

// private functions
private:

	void ProcessGbProConfiguratorServerReceive(BYTE *buffer, WORD length);
	
	void ProcessSetDrawingTicketText (BYTE *buffer, BYTE message_id);

	int GetPromotionCodeIndex (BYTE promotion_code);
	void initialGbProWinCatagories (void);
	void queueMessageReceivedAck (BYTE message_id, BYTE	command, DWORD error_code);

    // GB Pro Media Box server functions
	void OpenGbProMediaBoxServerSocket();

	// Cash Voucher printer functions
    void ProcessCvPrinterReceive(PrinterMessage *rx_data);
    void CheckCvPrinterReceive();
    void CheckCvPrinterTransmit();
    void OpenCvPrinterTcpIpPort();
    void OpenCvPrinterSerialPort();
    bool ClearToSendCvPrinterMessage();
	void SendCvToPrinter(CashVoucherTickets *pCashVoucherTickets);
    void CvTxPrinterData(char *data, int length);

	char* GetAsciiAmountString(DWORD amount);

	BYTE GetNextDisplayCategoryIndex (void);

// private variables
private:
	// Million Dollar Ticket server variables
    TcpIpRx				server_receive;

	// GB Pro server variables
    TcpIpRx				gb_pro_server_receive;
	BYTE				display_category_index;

	// Cash Voucher variables
    PrinterMessage		cv_rx_printer_message;
    BYTE				cv_printer_poll_wait;
    CMyAsyncSocket    	*pCvPrinterSocket;
    HANDLE          	hCvPrinterPort;
	time_t				cv_printer_time_check;
	PrinterReset		cv_printer_reset;
	GbProWinCategory 	gbpro_win_categories[NUMBER_OF_WIN_CATEGORIES];
};



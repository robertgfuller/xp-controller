#pragma once
#include "afxwin.h"


// CNetworkSetup dialog

class CNetworkSetup : public CDialog
{
	DECLARE_DYNAMIC(CNetworkSetup)

public:
	CNetworkSetup(CWnd* pParent = NULL);   // standard constructor
	virtual ~CNetworkSetup();

// Dialog Data
	enum { IDD = IDD_NETWORK_SETUP_DIALOG };

	virtual BOOL OnInitDialog();

	afx_msg void OnBnClickedNetworkSetupExitButton();
	afx_msg void OnBnClickedNetworkSetupSaveButton();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:
	CComboBox m_network_setup_type_combobox;
};

/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   03/08/13    Added Manual Dispense mechanism in the Technician menu.
Robert Fuller   02/07/12    Make sure GB Advantage is configured on before accessing the class pointer potentially uninitialized class pointer
Robert Fuller   08/09/10    Added mods to support the XP/SAS direct connect feature.
Robert Fuller   04/16/09    Removed code associated with PRAC interface.
Robert Fuller   01/29/09    Added code to support the new GB Advantage technician screen.
Robert Fuller   08/19/08    Added code to support the GB Advantage feature.
Dave Elquist    07/15/05    Added GB Pro support. Added master/slave support.
Dave Elquist    05/16/05    Initial Revision.
*/


// MenuTechnician.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include ".\menutechnician.h"


// CMenuTechnician dialog

IMPLEMENT_DYNAMIC(CMenuTechnician, CDialog)
CMenuTechnician::CMenuTechnician(CWnd* pParent /*=NULL*/)
	: CDialog(CMenuTechnician::IDD, pParent)
{
    pTechnicianSystemUsers              = NULL;
    pTechnicianDispense                 = NULL;
    pTechnicianDisplayGameInfoDialog    = NULL;
    pTechnicianGbAdvantageDialog        = NULL;
    pTechnicianMasterSlaveDialog        = NULL;
    pEmailSettings                      = NULL;
	pNetworkSetup						= NULL;
	pCashVoucherDialog					= NULL;
	pGbProAsciiMsg						= NULL;
}

CMenuTechnician::~CMenuTechnician()
{
    if (pTechnicianSystemUsers)
    {
        pTechnicianSystemUsers->DestroyWindow();
        delete pTechnicianSystemUsers;
    }

    if (pTechnicianDisplayGameInfoDialog)
    {
        pTechnicianDisplayGameInfoDialog->DestroyWindow();
        delete pTechnicianDisplayGameInfoDialog;
    }

    if (pTechnicianGbAdvantageDialog)
    {
        pTechnicianGbAdvantageDialog->DestroyWindow();
        delete pTechnicianGbAdvantageDialog;
    }

    if (pTechnicianMasterSlaveDialog)
    {
        pTechnicianMasterSlaveDialog->DestroyWindow();
        delete pTechnicianMasterSlaveDialog;
    }

    if (pEmailSettings)
    {
        pEmailSettings->DestroyWindow();
        delete pEmailSettings;
    }

	if (pNetworkSetup)
    {
        pNetworkSetup->DestroyWindow();
        delete pNetworkSetup;
    }
	if (pCashVoucherDialog)
	{
		pCashVoucherDialog->DestroyWindow();
		delete pCashVoucherDialog;
	}

	if (pGbProAsciiMsg)
	{
		pGbProAsciiMsg->DestroyWindow();
		delete pGbProAsciiMsg;
	}

	if (theApp.pRasConnectionStatus)
	{
		delete theApp.pRasConnectionStatus;
		theApp.pRasConnectionStatus = NULL;
	}
}

void CMenuTechnician::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TECHNICIAN_MENU_GB_ADVANTAGE_BUTTON, m_technician_menu_gb_advantage_button);
	DDX_Control(pDX, IDC_TECHNICIAN_MENU_MASTER_SLAVE_BUTTON, m_master_slave_button);
	DDX_Control(pDX, IDC_TECHNICIAN_MENU_EMAIL_BUTTON, m_email_settings_button);
	DDX_Control(pDX, IDC_TECHNICIAN_MENU_CASH_VOUCHER_BUTTON, m_technician_menu_cash_voucher_button);
	DDX_Control(pDX, IDC_GB_PRO_MACHINE_ASCII_MSGS_BUTTON, m_technician_menu_ascii_msgs_button);
	DDX_Control(pDX, IDC_TECHNICIAN_MENU_RAS_STATUS_BUTTON, m_technician_ras_status_button);
}


BEGIN_MESSAGE_MAP(CMenuTechnician, CDialog)
	ON_BN_CLICKED(IDC_EXIT_TECHNICIAN_BUTTON, OnBnClickedExitTechnicianButton)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_TECHNICIAN_MENU_USERS_BUTTON, OnBnClickedTechnicianMenuUsersButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_MENU_GAME_DATA_BUTTON, OnBnClickedTechnicianMenuGameDataButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_DISPLAY_GAME_INFO_BUTTON, OnBnClickedTechnicianDisplayGameInfoButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_MENU_GB_ADVANTAGE_BUTTON, OnBnClickedTechnicianMenuGbAdvantageButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_MENU_MASTER_SLAVE_BUTTON, OnBnClickedTechnicianMenuMasterSlaveButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_MENU_EMAIL_BUTTON, OnBnClickedTechnicianMenuEmailButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_MENU_TOUCH_SCREEN_BUTTON, OnBnClickedTechnicianMenuTouchScreenButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_RAS_CREATE_BUTTON, OnBnClickedTechnicianRasCreateButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_MENU_CASH_VOUCHER_BUTTON, &CMenuTechnician::OnBnClickedTechnicianMenuCashVoucherButton)
	ON_BN_CLICKED(IDC_GB_PRO_MACHINE_ASCII_MSGS_BUTTON, &CMenuTechnician::OnBnClickedGbProMachineAsciiMsgsButton)
	ON_BN_CLICKED(IDC_TECH_RESTART_APP_BUTTON, &CMenuTechnician::OnBnClickedTechRestartAppButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_MENU_RAS_STATUS_BUTTON, &CMenuTechnician::OnBnClickedTechnicianMenuRasStatusButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_MENU_DISPENSE_BUTTON, &CMenuTechnician::OnBnClickedTechnicianMenuDispenseButton)
END_MESSAGE_MAP()


void CMenuTechnician::OnBnClickedExitTechnicianButton()
{
    theApp.current_dialog_return = IDD_MAIN_MENU_DIALOG;
    DestroyWindow();
}

void CMenuTechnician::OnMouseMove(UINT nFlags, CPoint point)
{
    theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

void CMenuTechnician::OnBnClickedTechnicianMenuUsersButton()
{
    if (pTechnicianSystemUsers)
    {
        pTechnicianSystemUsers->DestroyWindow();
        delete pTechnicianSystemUsers;
    }

    pTechnicianSystemUsers = new CTechnicianSystemUsers;
    pTechnicianSystemUsers->Create(IDD_TECHNICIAN_SYSTEM_USERS_DIALOG);
}

void CMenuTechnician::OnBnClickedTechnicianMenuGameDataButton()
{
    if (!theApp.pTechnicianDisplay)
    {
        theApp.pTechnicianDisplay = new CTechnicianDisplay();
        theApp.pTechnicianDisplay->Create(IDD_TECHNICIAN_DISPLAY_DIALOG);
    }
}

void CMenuTechnician::OnBnClickedTechnicianDisplayGameInfoButton()
{
    if (pTechnicianDisplayGameInfoDialog)
    {
        pTechnicianDisplayGameInfoDialog->DestroyWindow();
        delete pTechnicianDisplayGameInfoDialog;
    }

    pTechnicianDisplayGameInfoDialog = new CTechnicianDisplayGameInfoDialog;
    pTechnicianDisplayGameInfoDialog->Create(IDD_TECHNICIAN_DISPLAY_GAME_INFO_DIALOG);
}

void CMenuTechnician::OnBnClickedTechnicianMenuGbAdvantageButton()
{
    if (pTechnicianGbAdvantageDialog)
    {
        pTechnicianGbAdvantageDialog->DestroyWindow();
        delete pTechnicianGbAdvantageDialog;
    }

    pTechnicianGbAdvantageDialog = new CTechnicianGbAdvantageInfo;
    pTechnicianGbAdvantageDialog->Create(IDD_TECHNICIAN_GB_ADVANTAGE_DIALOG);
}

BOOL CMenuTechnician::OnInitDialog()
{
	CDialog::OnInitDialog();

    if (theApp.memory_settings_ini.masters_ip_address)
    {
        CWnd* item;

        item = 0;
        item = GetDlgItem(IDC_TECHNICIAN_MENU_GAME_DATA_BUTTON);
        if (item)
            item->ShowWindow(SW_HIDE);
        item = 0;
        item = GetDlgItem(IDC_TECHNICIAN_DISPLAY_GAME_INFO_BUTTON);
        if (item)
            item->ShowWindow(SW_HIDE);
        item = 0;
        item = GetDlgItem(IDC_GB_PRO_MACHINE_ASCII_MSGS_BUTTON);
        if (item)
            item->ShowWindow(SW_HIDE);
        item = 0;
        item = GetDlgItem(IDC_TECHNICIAN_MENU_EMAIL_BUTTON);
        if (item)
            item->ShowWindow(SW_HIDE);
    }

	if (!theApp.pGbProManager)
	{
		m_technician_menu_gb_advantage_button.DestroyWindow();
		m_technician_menu_cash_voucher_button.DestroyWindow();
		m_technician_menu_ascii_msgs_button.DestroyWindow();
	}

	if (!theApp.pNetworkSettingsRecord || !theApp.pNetworkSettingsRecord->network_type)
		m_technician_ras_status_button.DestroyWindow();

    if (theApp.current_user.user.id / 10000 < SUPERVISOR)
    {
        m_master_slave_button.DestroyWindow();
        m_email_settings_button.DestroyWindow();
    }

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CMenuTechnician::OnBnClickedTechnicianMenuMasterSlaveButton()
{
    if (pTechnicianMasterSlaveDialog)
    {
        pTechnicianMasterSlaveDialog->DestroyWindow();
        delete pTechnicianMasterSlaveDialog;
    }

    pTechnicianMasterSlaveDialog = new CTechnicianMasterSlaveDialog;
    pTechnicianMasterSlaveDialog->Create(IDD_TECHNICIAN_MASTER_SLAVE_DIALOG);

	MessageWindow(false, "REBOOT SYSTEM", "REBOOT AFTER ANY CHANGES");
}

void CMenuTechnician::OnBnClickedTechnicianMenuEmailButton()
{
    if (pEmailSettings)
    {
        pEmailSettings->DestroyWindow();
        delete pEmailSettings;
    }

    pEmailSettings = new CEmailSettings;
    pEmailSettings->Create(IDD_TECHNICIAN_EMAIL_ALERTS_DIALOG);
}

void CMenuTechnician::OnBnClickedTechnicianMenuTouchScreenButton()
{
    if (GetFileAttributes(TOUCH_CALIBRATION_FILE) != 0xFFFFFFFF)
        ShellExecute(NULL, "open", TOUCH_CALIBRATION_FILE, NULL, NULL, SW_SHOWMAXIMIZED);
	else if (GetFileAttributes(MINTRONIX_TOUCH_CAL_FILE) != 0xFFFFFFFF)
		ShellExecute(NULL, "open", "control.exe", MINTRONIX_TOUCH_CAL_FILE, NULL, SW_SHOWNORMAL);
    else
        MessageWindow(false, "TOUCH SCREEN", "TOUCH SCREEN CONFIGURATION UTILITY NOT FOUND");
}

void CMenuTechnician::OnBnClickedTechnicianRasCreateButton()
{
	if (pNetworkSetup)
	{
		pNetworkSetup->DestroyWindow();
		delete pNetworkSetup;
	}

	pNetworkSetup = new CNetworkSetup;
	pNetworkSetup->Create(IDD_NETWORK_SETUP_DIALOG);
}
void CMenuTechnician::OnBnClickedTechnicianMenuCashVoucherButton()
{
    if (theApp.pGbProManager)
    {
	    if (pCashVoucherDialog)
	    {
		    pCashVoucherDialog->DestroyWindow();
		    delete pCashVoucherDialog;
	    }

	    pCashVoucherDialog = new CCashVoucherDialog;
	    pCashVoucherDialog->Create(IDD_CASH_VOUCHER_DIALOG);
    }
}

void CMenuTechnician::OnBnClickedGbProMachineAsciiMsgsButton()
{
	if (pGbProAsciiMsg)
	{
		pGbProAsciiMsg->DestroyWindow();
		delete pGbProAsciiMsg;
	}

	pGbProAsciiMsg = new CGbProAsciiMsg;
	pGbProAsciiMsg->Create(IDD_GB_PRO_MACHINE_ASCII_MSGS_DIALOG);
}

void CMenuTechnician::OnBnClickedTechRestartAppButton()
{
	theApp.ProcessExitProgramRequest(false);
//	ShellExecute(NULL, "open", "controller.bat", NULL, NULL, SW_SHOWMAXIMIZED);
}

void CMenuTechnician::OnBnClickedTechnicianMenuRasStatusButton()
{
	if (theApp.pRasConnectionStatus)
		delete theApp.pRasConnectionStatus;

	theApp.pRasConnectionStatus = new CRasConnectionStatus;
    theApp.pRasConnectionStatus->Create(IDD_RAS_STATUS_DIALOG);
}

void CMenuTechnician::OnBnClickedTechnicianMenuDispenseButton()
{
//	if (theApp.pRasConnectionStatus)
//		delete theApp.pRasConnectionStatus;

//	theApp.pRasConnectionStatus = new CRasConnectionStatus;
//    theApp.pRasConnectionStatus->Create(IDD_RAS_STATUS_DIALOG);
    if (pTechnicianDispense)
    {
        pTechnicianDispense->DestroyWindow();
        delete pTechnicianDispense;
    }

    pTechnicianDispense = new CTechnicianDispense;
    pTechnicianDispense->Create(IDD_TECHNICIAN_DISPENSE_DIALOG);

}


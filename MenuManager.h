/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller	07/03/13	Added code to launch the old GB Trax application if there is no IDC and the GameTenderClient.exe file is not present on the controller hardware.
Robert Fuller   03/08/13    Removed obsolete Location Collection code.
Robert Fuller   06/28/11    Allow manager to access the dispenser functions in the Collection screen.
Robert Fuller   06/09/09    Added mechanism to accomidate manager awarded promotion points.
Robert Fuller   10/02/08    Allow the Manager to clear out the ticket pay pending file when a pending ticket can not be paid.
Dave Elquist    07/15/05    Added GB Pro support.
Dave Elquist    05/16/05    Initial Revision.
*/


#pragma once
#include "afxwin.h"
#include "ManagerReports.h"
#include "ManagerSetup.h"
#include "ManagerVersionInfo.h"
#include "GbProManagerDialog.h"
#include "ManagerAwardPoints.h"
#include "FixedButton.h"
#include "MenuCollector.h"


// CMenuManager dialog

class CMenuManager : public CDialog
{
	DECLARE_DYNAMIC(CMenuManager)

public:
	CMenuManager(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMenuManager();
	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_MANAGER_MENU_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	afx_msg void OnPaint();

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedExitManagerButton();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedManagerUnlockGameButton();
	afx_msg void OnBnClickedManagerReportsButton();
	afx_msg void OnBnClickedManagerSetupButton();
	afx_msg void OnBnClickedManagerVersionInfoButton();
	afx_msg void OnBnClickedManagerMenuButton1();
	afx_msg void OnBnClickedManagerMenuButton2();
	afx_msg void OnBnClickedManagerMenuButton3();
	afx_msg void OnBnClickedManagerMenuButton4();
	afx_msg void OnBnClickedManagerMenuButton5();
	afx_msg void OnBnClickedManagerMenuButton6();
	afx_msg void OnBnClickedManagerMenuButton7();
	afx_msg void OnBnClickedManagerMenuButton8();
	afx_msg void OnBnClickedManagerMenuButton9();
	afx_msg void OnBnClickedManagerMenuButton0();
	afx_msg void OnBnClickedManagerMenuClearButton();
	afx_msg void OnBnClickedManagerGbProButton();
	afx_msg void OnEnSetfocusManagerUnlockUcmcIdEdit();
	afx_msg void OnEnSetfocusManagerTillAmountEdit();
	afx_msg void OnBnClickedManagerClearPendingTicketsButton();
	afx_msg void OnBnClickedManagerMaintenanceButton();

// private functions
private:
    void ProcessButtonClick(BYTE button_press);

// private variables
private:
    CManagerReports     *pManagerReports;
    CManagerSetup       *pManagerSetup;
    CManagerVersionInfo *pManagerVersionInfo;
    CGbProManagerDialog *pGbProManagerDialog;
    CManagerAwardPoints *pManagerAwardPointsDialog;
	CMenuCollector		*pManagerCollection;

    DWORD current_edit_focus, manager_ucmc_id;
	CStatic m_unlock_game_groupbox;
	CStatic m_unlock_game_text;
	CEdit m_unlock_game_edit;
	CButton m_gb_pro_button;

	CFixedButton keypad0;
	CFixedButton keypad1;
	CFixedButton keypad2;
	CFixedButton keypad3;
	CFixedButton keypad4;
	CFixedButton keypad5;
	CFixedButton keypad6;
	CFixedButton keypad7;
	CFixedButton keypad8;
	CFixedButton keypad9;
	CFixedButton keypadC;

	CFixedButton reports;
	CFixedButton setup;
	CFixedButton version;
	CFixedButton exit;

	CFixedButton m_unlock_game_button;
	CFixedButton m_manager_vlora_button;
	CFixedButton clear_pending_tickets_button;
	CFixedButton maintenance_button;

};

/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    07/15/05    Initial Revision.
*/


// EmailSettings.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include ".\emailsettings.h"


// CEmailSettings dialog

IMPLEMENT_DYNAMIC(CEmailSettings, CDialog)
CEmailSettings::CEmailSettings(CWnd* pParent /*=NULL*/)
	: CDialog(CEmailSettings::IDD, pParent)
{
    pEmailFlags = NULL;

}

CEmailSettings::~CEmailSettings()
{
    if (pEmailFlags)
    {
        pEmailFlags->DestroyWindow();
        delete pEmailFlags;
    }
}

void CEmailSettings::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EMAIL_SETTINGS_ADDRESS_COMBO, m_email_list_combobox);
}


BEGIN_MESSAGE_MAP(CEmailSettings, CDialog)
	ON_BN_CLICKED(IDC_EMAIL_BUTTON1, OnBnClickedEmailButton1)
	ON_BN_CLICKED(IDC_EMAIL_BUTTON2, OnBnClickedEmailButton2)
	ON_BN_CLICKED(IDC_EMAIL_BUTTON3, OnBnClickedEmailButton3)
	ON_BN_CLICKED(IDC_EMAIL_BUTTON4, OnBnClickedEmailButton4)
	ON_BN_CLICKED(IDC_EMAIL_BUTTON5, OnBnClickedEmailButton5)
	ON_BN_CLICKED(IDC_EMAIL_BUTTON6, OnBnClickedEmailButton6)
	ON_BN_CLICKED(IDC_EMAIL_BUTTON7, OnBnClickedEmailButton7)
	ON_BN_CLICKED(IDC_EMAIL_BUTTON8, OnBnClickedEmailButton8)
	ON_BN_CLICKED(IDC_EMAIL_BUTTON9, OnBnClickedEmailButton9)
	ON_BN_CLICKED(IDC_EMAIL_BUTTON0, OnBnClickedEmailButton0)
	ON_BN_CLICKED(IDC_EMAIL_ADD_BUTTON, OnBnClickedEmailAddButton)
	ON_BN_CLICKED(IDC_EMAIL_REMOVE_BUTTON, OnBnClickedEmailRemoveButton)
	ON_BN_CLICKED(IDC_EMAIL_CLEAR_BUTTON, OnBnClickedEmailClearButton)
	ON_BN_CLICKED(IDC_EMAIL_ALERTS_BUTTON, OnBnClickedEmailAlertsButton)
	ON_BN_CLICKED(IDC_EMAIL_EXIT_BUTTON, OnBnClickedEmailExitButton)
	ON_CBN_SELCHANGE(IDC_EMAIL_SETTINGS_ADDRESS_COMBO, OnCbnSelchangeEmailSettingsAddressCombo)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


void CEmailSettings::ProcessButtonClick(char *button_press)
{
 char char_compare, char_replace;
 static DWORD last_button_press_timer;
 CString email_address;

    GetDlgItemText(IDC_EMAIL_SETTINGS_EDIT, email_address);

    if (email_address.GetLength() >= MAXIMUM_EMAIL_ADDRESS_LENGTH - 1)
        MessageWindow(false, "EMAIL ADDRESS", "Maximum Address Length");
    else
    {
        if (!email_address.GetLength())
            email_address = button_press;
        else
        {
            if (last_button_press_timer < GetTickCount())
                email_address += button_press;
            else
            {
                char_compare = email_address.GetAt(email_address.GetLength() - 1);

                switch (button_press[0])
                {
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                        char_replace = (button_press[0] - '1') * 3 + 'a';
                        if (char_compare == button_press[0])
                            email_address.SetAt(email_address.GetLength() - 1, char_replace);
                        else if (char_compare == char_replace)
                            email_address.SetAt(email_address.GetLength() - 1, char_replace + 1);
                        else if (char_compare == char_replace + 1)
                            email_address.SetAt(email_address.GetLength() - 1, char_replace + 2);
                        else if (char_compare == char_replace + 2)
                            email_address.SetAt(email_address.GetLength() - 1, button_press[0]);
                        else
                            email_address += button_press;
                        break;

                    case '9':
                        if (char_compare == '9')
                            email_address.SetAt(email_address.GetLength() - 1, 'y');
                        else if (char_compare == 'y')
                            email_address.SetAt(email_address.GetLength() - 1, 'z');
                        else if (char_compare == 'z')
                            email_address.SetAt(email_address.GetLength() - 1, '9');
                        else
                            email_address += button_press;
                        break;

                    case '0':
                        if (char_compare == '0')
                            email_address.SetAt(email_address.GetLength() - 1, '.');
                        else if (char_compare == '.')
                            email_address.SetAt(email_address.GetLength() - 1, '_');
                        else if (char_compare == '_')
                            email_address.SetAt(email_address.GetLength() - 1, '@');
                        else if (char_compare == '@')
                            email_address.SetAt(email_address.GetLength() - 1, '0');
                        else
                            email_address += button_press;
                        break;
                }
            }
        }
    }

    last_button_press_timer = GetTickCount() + LAST_BUTTON_PRESS_TIMEOUT;
    SetDlgItemText(IDC_EMAIL_SETTINGS_EDIT, email_address);
}

void CEmailSettings::OnBnClickedEmailButton1()
{
    ProcessButtonClick("1");
}

void CEmailSettings::OnBnClickedEmailButton2()
{
    ProcessButtonClick("2");
}

void CEmailSettings::OnBnClickedEmailButton3()
{
    ProcessButtonClick("3");
}

void CEmailSettings::OnBnClickedEmailButton4()
{
    ProcessButtonClick("4");
}

void CEmailSettings::OnBnClickedEmailButton5()
{
    ProcessButtonClick("5");
}

void CEmailSettings::OnBnClickedEmailButton6()
{
    ProcessButtonClick("6");
}

void CEmailSettings::OnBnClickedEmailButton7()
{
    ProcessButtonClick("7");
}

void CEmailSettings::OnBnClickedEmailButton8()
{
    ProcessButtonClick("8");
}

void CEmailSettings::OnBnClickedEmailButton9()
{
    ProcessButtonClick("9");
}

void CEmailSettings::OnBnClickedEmailButton0()
{
    ProcessButtonClick("0");
}

void CEmailSettings::OnBnClickedEmailAddButton()
{
 DWORD file_index;
 FileEmailRecord file_email_record, file_email_compare_record;
 CString email_address, window_display;

    GetDlgItemText(IDC_EMAIL_SETTINGS_EDIT, email_address);

    if (email_address.GetLength() >= 5 && email_address.GetLength() <= MAXIMUM_EMAIL_ADDRESS_LENGTH - 1)
    {
        memset(&file_email_record, 0, sizeof(file_email_record));
        memcpy(file_email_record.email_address, email_address.GetBuffer(0), email_address.GetLength());

        // check for duplicate email address
        file_index = 0;
        while (fileRead(EMAIL_ADDRESS_LIST, file_index, &file_email_compare_record, sizeof(file_email_compare_record)))
        {
            if (!memcmp(file_email_record.email_address, file_email_compare_record.email_address, MAXIMUM_EMAIL_ADDRESS_LENGTH))
            {
                MessageWindow(false, "EMAIL ADDRESS", "Address Already Added");
                return;
            }
            else
                file_index++;
        }

        if (fileWrite(EMAIL_ADDRESS_LIST, -1, &file_email_record, sizeof(file_email_record)))
        {
            window_display.Format("Address Added: %s", file_email_record.email_address);
            MessageWindow(false, "EMAIL ADDRESS", window_display);
            m_email_list_combobox.AddString(file_email_record.email_address);
        }
        else
            MessageWindow(false, "EMAIL ADDRESS", "Error Writing File");
    }
    else
        MessageWindow(false, "EMAIL ADDRESS", "Invalid Address Length");
}

void CEmailSettings::OnBnClickedEmailRemoveButton()
{
 char compare_email_address[MAXIMUM_EMAIL_ADDRESS_LENGTH];
 DWORD file_index;
 FileEmailRecord file_email_record;
 CString email_address, window_display;

    GetDlgItemText(IDC_EMAIL_SETTINGS_EDIT, email_address);

    if (email_address.GetLength() >= 5 && email_address.GetLength() <= MAXIMUM_EMAIL_ADDRESS_LENGTH - 1)
    {
        memset(&compare_email_address, 0, sizeof(compare_email_address));
        memcpy(compare_email_address, email_address.GetBuffer(0), email_address.GetLength());
		file_index = 0;

        while (fileRead(EMAIL_ADDRESS_LIST, file_index, &file_email_record, sizeof(file_email_record)))
        {
            if (!memcmp(file_email_record.email_address, compare_email_address, MAXIMUM_EMAIL_ADDRESS_LENGTH))
            {
                if (fileDeleteRecord(EMAIL_ADDRESS_LIST, sizeof(file_email_record), file_index))
                {
                    window_display.Format("Address Deleted: %s", compare_email_address);
                    MessageWindow(false, "EMAIL ADDRESS", window_display);
                    int string_index = m_email_list_combobox.FindString(-1, compare_email_address);
                    if (string_index >= 0)
                        m_email_list_combobox.DeleteString(string_index);
                }
                else
                    MessageWindow(false, "EMAIL ADDRESS", "Error Deleting Address");

                return;
            }
            else
                file_index++;
        }

        MessageWindow(false, "EMAIL ADDRESS", "Address Not Found");
    }
    else
        MessageWindow(false, "EMAIL ADDRESS", "Invalid Address Length");
}

void CEmailSettings::OnBnClickedEmailClearButton()
{
    SetDlgItemText(IDC_EMAIL_SETTINGS_EDIT, "");
}

void CEmailSettings::OnBnClickedEmailAlertsButton()
{
 int item_selected = m_email_list_combobox.GetCurSel();
 CString edit_string;

    if (item_selected != CB_ERR)
    {
        m_email_list_combobox.GetLBText(item_selected, edit_string);

        if (edit_string.GetLength() >= 5)
        {
            if (pEmailFlags)
            {
                pEmailFlags->DestroyWindow();
                delete pEmailFlags;
            }

            pEmailFlags = new CEmailFlags(edit_string);
            pEmailFlags->Create(IDD_TECHNICIAN_EMAIL_FLAGS_DIALOG);
        }
    }
}

void CEmailSettings::OnBnClickedEmailExitButton()
{
	DestroyWindow();
}

void CEmailSettings::OnCbnSelchangeEmailSettingsAddressCombo()
{
 int item_selected = m_email_list_combobox.GetCurSel();
 CString edit_string;

    if (item_selected != CB_ERR)
    {
        m_email_list_combobox.GetLBText(item_selected, edit_string);
        SetDlgItemText(IDC_EMAIL_SETTINGS_EDIT, edit_string);
    }
    else
        SetDlgItemText(IDC_EMAIL_SETTINGS_EDIT, "");
}

void CEmailSettings::OnMouseMove(UINT nFlags, CPoint point)
{
	theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

BOOL CEmailSettings::OnInitDialog()
{
 DWORD file_index;
 FileEmailRecord file_email_record;

	CDialog::OnInitDialog();

    file_index = 0;
    while (fileRead(EMAIL_ADDRESS_LIST, file_index, &file_email_record, sizeof(file_email_record)))
    {
        m_email_list_combobox.AddString(file_email_record.email_address);
        file_index++;
    }

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   11/04/14    Added the redemption complete message to relay the redemption status after a redemption request operation.
Robert Fuller   10/14/14    Make sure there is a UCMC id before processing a bonus award message.
Robert Fuller   10/14/14    Purge bill accepted messages from pending idc tx message file and prioritize pending messages if they exist in the file.
Robert Fuller   07/29/14    Added diagnostic code to log meter wobble events in the controller error log for diagnostic purposes.
Robert Fuller   07/29/14    Make sure an ip address and port are configured before queuing up event messages to be sent to the customer control server.
Robert Fuller   07/29/14    Save and send bill accepted IDC tx messages if com is down between IDC and XPC.
Robert Fuller   07/09/14    Sanity check the file records being read from the pending idc tx messages file to make sure the there is a valid message
                            header, command code, and the data length does not surpass the allotted space.
Robert Fuller   07/02/14    On startup, queue up idc messages in reverse order to correct dispense report issue on IDC side.
Robert Fuller	12/11/13	Added an additional w2g printer queue which is serviced in the once second timer routine to slow down the data flow to the printer.
Robert Fuller	12/11/13	Made sure an IDC is configured before queueing up idc tx messages and writing pending tx messages file.
Robert Fuller	12/05/13	Added code to send IDC the 4ok bingo winning card message. 
Robert Fuller	12/05/13	Added code to send IDC the bill denomination when a bill is accepted in the gaming machine. 
Robert Fuller	12/05/13	Added code to send IDC the value of the credit meter when it is fetched in real time via the SAS current credits message. 
Robert Fuller	12/05/13	Added IDC to XPC message to print w2gs to automate the tax reporting process.
Robert Fuller	12/05/13	If a Stratus TX message is sent more than 10 times without getting a valid ack, remove the message from the queue so that other TX messages can be sent.
Robert Fuller	12/05/13	Run reports and clear out the paid ticket buffer when an external fill is complete.
Robert Fuller	12/05/13	Added IDC to XP Controller message to relay w2g social security and drivers license information after the drivers license information is scanned after of a taxable jackpot win.
Robert Fuller	12/05/13	When processing an add game request, instantiate the game control object before instantiating the game device object,
Robert Fuller   11/25/13    Added a transaction id to the winning hand message so we can identify duplicate winning hand messages.
Robert Fuller   11/25/13    Added a transaction id to the clear card message so we can identify duplicate clear card messages.
Robert Fuller   11/20/13    Added a transaction id in the redemption complete message to allow gaming devices to identify duplicate messages.
Robert Fuller	08/13/13	Added logic to prevent Stratus TX messages from being logged more than one time
Robert Fuller	07/04/13	Fixed the XPC subversion string problem in the SendIdcXpcVersionConfigInfo() function
Robert Fuller	07/04/13	Added ability for the IDC to send bonus win text messages to the gaming devices.
Robert Fuller	07/04/13	Send social security number and drivers license information to the IDC when the player hits a taxable jackpot for accounting purposes.
Robert Fuller	07/04/13	Save important Stratus TX messages to a file on disk and do not purge unless Stratus returns an ack.
Robert Fuller	07/04/13	Added a communication interface to the Station's Casino Casino Management System (CMS)
Robert Fuller	07/04/13	Added ability to award cashable and non-cashable credits for GBA based wins.
Robert Fuller	05/14/13	When configured to run ECash and CD2000, create another process to communicate with dispenser, instead of calling from the OnIdle task.
Robert Fuller	05/10/13	Made sure GBA is active when processing IDC GBA promo config messages.
Robert Fuller	05/10/13	Added transaction id an time/date stamp fields to the dispenser info message.
Robert Fuller	05/10/13	Added code to save pending IDC TX messages to disk until they are acknowledged by the IDC. 
Robert Fuller	05/02/13	After receiving 4 nack messages from the IDC, dequeue the message and log the error to the controller error log.
Robert Fuller	05/02/13	Added ability for the IDC to send Manual Dispense commands to the XPC.
Robert Fuller	05/02/13	If there is not enough bills in the $100s cassette, check to see if there is enough money in the $20s cassette to complete the cashout pay.
Robert Fuller	05/02/13	Allow the Puloon dispenser to dispense all bills in a cassette without having a "5 bill margin for error".
Robert Fuller	05/02/13	Added support for the 4 cassette Puloon dispenser
Robert Fuller   05/02/13    Added code for future support for the GBA interface to Stations Casino Management System.
Robert Fuller   03/08/13    Send XPC version and configuration info to IDC.
Robert Fuller   03/08/13    Added ability to configure GBA promos on the IDC.
Robert Fuller   03/08/13    Set the real time clocks on direct connect games.
Robert Fuller   03/08/13    Removed obsolete Location Collection code.
Robert Fuller   03/08/13    Added code to support the configurable dispenser payout limit..
Robert Fuller   03/08/12    Make sure the IDC ip address is configured before checking socket.
Robert Fuller   03/08/12    Call function to process GBA Configurator rx/tx messages from on idle instead of printer processing com thread.
Robert Fuller   03/08/13    Added manual dispense feature to the technician screen.
Robert Fuller   02/04/13    Only increment the message id for the XPC tx messages after recieving an ack from the IDC.
Robert Fuller   02/04/13    Check the IDC tx and rx queues in the OnIdle task, so we can process message faster than once per second.
Robert Fuller   02/04/13    Only send IDC tx messages after recieving an ack from the previous message or waiting 2 seconds.
Robert Fuller	09/17/12	Added code to fill the ucmc id field in the dispense message we send to the IDC for accounting purposes.
Robert Fuller	09/17/12	Added iGBA transaction id to differentiate iGBA wins amongst multiple games.
Robert Fuller	09/17/12	Added a code to support external awarded points for slot/keno win bonuses and manager awarded points.
Robert Fuller	09/17/12	Added code to send dispense info to the IDC.
Robert Fuller	09/17/12	Added code to relay machine information to the IDC.
Robert Fuller	09/17/12	Make sure we are ignoring GB Interface devices when traversing through the game device linked list.
Robert Fuller   06/04/12    Added code to support the new Quick Enrollment feature.
Robert Fuller   06/04/12    Added code to communicate with the Interactive Display Controller.
Robert Fuller   06/04/12    Send Stratus message when starting or ending a cashier shift or logging in.
Robert Fuller   03/05/12    Added new GB message to validate a member id selected during the enrollment process.
Robert Fuller   02/09/11    Do NOT autologout the collector when we are in the middle of a fill.
Robert Fuller   01/27/12    Added code to send the GB Initialization data to the GB Interface after a 4ok winning hand event.
Robert Fuller   12/30/11    Added code to print MarkerTrax printer output to an electronic file for troubleshooting and backup purposes.
Robert Fuller   12/30/11    Added an exclusive stratus message transmit queue for Marker Trax messages. Made sure that these messages are only dequeued
                            after receiving the Stratus response.
Robert Fuller   12/30/11    Make sure we are getting the game pointer associated with the GB Interface, not the SAS direct connect game pointer when
                            sending the initialization and update messages.
Robert Fuller   10/03/11    Print the sas id address as opposed to the irrelevant mux id address for SAS direct connect games.
Robert Fuller   10/03/11    Do not process meters from the Device Reset message if the message is coming from a GB Interface device.
Robert Fuller   08/03/11    Added code to support Location Collection features.
Robert Fuller   06/28/11    Set the stop comm thread flag when removing game control objects so the serial processing thread is stopped.
Robert Fuller   06/23/11    Send message to game to clear unpaid tickets from buffer when configured for stand alone mode.
Robert Fuller   06/08/11    Added mods to support GB with the XP/SAS direct connect feature.
Robert Fuller   04/27/11    Fixed the meter rollover compensation code for stand alone netwin calcs.
Robert Fuller   01/26/11    Made sure controller is not configured for standalone when queueing up stratus messages.
Robert Fuller   11/10/10    Adjusted the resolution of the Windows clock tick timer down to 1ms.
Robert Fuller   11/10/10    Adjusted the logic that manages the CommControl object linked list.
Robert Fuller   10/29/10    Added code to set the windows timer resolution to 1ms per system tick to fix SAS polling problem.
Robert Fuller   08/09/10    Added mods to support the XP/SAS direct connect feature.
Robert Fuller   07/22/10    Copy the customer id into the drop ticket structure to fix Slave paid tickets from not showing on the TKT PAID OUT report.
Robert Fuller   07/01/10    Added code to send the bill count message to the system on LCDM dispenser dispenses. 
Robert Fuller   06/10/10    Added code to support the new $5 $20 LCDM-2000 configuration. 
Robert Fuller   06/02/10    Added code to support the new Puloon LCDM-2000 dual denomination dispenser. 
Robert Fuller   05/20/10    Print a "DM" on the ticket fill and drop reports when a dispenser malfunction is detected during a payout.
Robert Fuller   05/18/10    When adding up all the drop tickets, make sure the customer id matches the customer id of the current PAID OUT LOG report being run.
Robert Fuller   04/26/10    Added additional customer ids to the configuration settings. Get netwin data for all customer ids when calculating shift report data.
Robert Fuller	04/07/10	Write drop report info to a file at drop time.
Robert Fuller   04/01/10    Added code necessary to abort a marker advance if the game never receives the advance messages from the system.
Robert Fuller   02/08/10    Added code to display error counters information on dispenser status screen for the new Puloon dispenser.
Robert Fuller   02/08/10    Added code to detect meter roll over and make the needed adjustments to the netwin report.
Robert Fuller   02/08/10    Made sure that we do not try to connect to the time/date server on the Stratus more than once per hour if a connection request fails.
Robert Fuller   02/08/10    Clarified Netwin reports by adding fields TOTAL OUT and NET CASH to breakout WAT related transactions from cash transactions.
Robert Fuller   01/19/10    Implemented a file redundancy strategy for files which store the paid game locks, dispenses, and drop tickets.
Robert Fuller   01/19/10    Added code to support the new Puloon dispenser.
Robert Fuller   01/19/10    Slowed the dispenser poll cycle down to once per second
Robert Fuller   01/05/10    Create the stored meters subdirectory on startup if it does not exist.
Robert Fuller   12/29/09    If the total drop tickets amount is different from redeem records, print out a descrepancy message.
Robert Fuller   12/08/09    Once a day we purge the "dated paid ticket" file by removing all tickets in the file that over 14 days old.
Robert Fuller   11/18/09    Made sure that user can not log on, and slave messages are not processed when a netwin is in process.
Robert Fuller   10/12/09    Startup the next version of the GB Advantage 3a.
Robert Fuller   09/03/09    Added code to relay game configuration data for enabled games to the Stratus.
Robert Fuller   09/03/09    Added software mods to support running the XP Controller in stand alone mode which allows locations to operate with basic features without connecting to the main frame.
Robert Fuller   07/15/09    Increased the number of shifts to print on the shift report from 4 to 21.
Robert Fuller   07/07/09    Delete the GB Advantage manager when the user configures the controller as a slave.
Robert Fuller   06/09/09    Added mechanism to accomidate manager awarded promotion points.
Robert Fuller   05/15/09    Print drink comp info in the cashier shift data when printing shift data.
Robert Fuller   04/17/09    Connect to the new Stratus time/date server that send each individual field of the time/date,
                            as opposed to sending the number of seconds since Jan 1, 1970.
Robert Fuller   04/16/09    Removed code associated with PRAC interface.
Robert Fuller   04/08/09    Double check the Netwin calculations on the Stratus by keeping track of game meters locally.
Robert Fuller   04/08/09    Change shift paradigm to only allow a single shift per login id to be active at a time.
Robert Fuller   01/28/09    Filled in the customer id and port number in the stratus event log entries.
Robert Fuller   10/15/08    When the month in the date is two digits, cash adjustment report text overflows to the next line when printed.
Robert Fuller   10/15/08    Added code to start up the GB Advantage display application if the feature is activated.
Robert Fuller   10/14/08    Avoid displaying the ticket pay pending tilt if the controller paying the ticket is the same as the
                            controller that logged the pending ticket.
Robert Fuller   10/14/08    Increased the frequency of the game lock ding and light flash.
Robert Fuller   09/25/08    Ordered the games in the redeem log report in order of mux id instead of ucmc id.
Robert Fuller   09/10/08    Added the mux ids to the Redeem Report section.
Robert Fuller   09/08/08    Added code to print the handle on the shift report.
Robert Fuller   08/26/08    Properly initialize the GB Pro settings before overwriting after a CC update.
Robert Fuller   08/19/08    Added code to support the GB Advantage feature.
Robert Fuller   08/19/08    Increased the number of characters we can print in the PrintTitle routine.
Robert Fuller	07/08/08	Made adjustment to the drop meters to compensate for the wat in being included in the drop value.
Robert Fuller   05/26/08    Fixed memory overite problem associated with initializing the bag_and_tag_info array. 
Robert Fuller   05/02/08    Added code to support CD2000 dispenser.
Robert Fuller   03/26/08    Set TZ environment variable before calling localtime_s so the clock will be right when setting the time.
Robert Fuller	03/25/08	Added the bag and tag info array to contain the ucmc_ids and game_drops arrays.
Robert Fuller   03/14/08    Fixed dispenser bill count routine. Only remove marker credit messages from tx queue if Stratus responds.
Robert Fuller   02/01/08    Send correct impress amounts to the Stratus.
Robert Fuller   01/31/08    Added total netwin drop variable to the class.
Robert Fuller	01/11/08	Added modification for bag and tag feature.
Robert Fuller   12/26/07    Fixed formatting problem with Cashier Shift Tickets.
Robert Fuller   11/05/07    Added support for the X10 Active Home mechanism to flash lamps and play chimes for game locks.
Robert Fuller   09/05/07    Fill slave message data for pending pays when processing slave ticket queries.
Robert Fuller   09/05/07    Do NOT delete the game lock file when we process slave ticket queries.
Robert Fuller   09/05/07    Do NOT change endian on the ticket id in RemovePendingTicketFromFile().
Dave Elquist    07/15/05    Added Marker support. Added GB Pro support.
Dave Elquist    04/14/05    Initial Revision.
*/

// xp_controller.cpp : Defines the class behaviors for the application.
//

#define _CRT_RAND_S

#include "stdafx.h"
#include "Mmsystem.h"
#include <atlsmtpconnection.h>
#include ".\xp_controller.h"
#include "xp_controllerDlg.h"
#include "TimerWindow.h"
#include "aes.h"
#include "aesopt.h"
#include "aestab.h"
#include "DrawerStartShift.h"
#include "WinEvaluation.h"

#ifndef SAS_COMM_H
#include "sas_comm.h"
#endif

#pragma comment (lib, "winmm.lib")
#pragma comment (lib, "Rasapi32.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#pragma pack(1)
struct
{
    BYTE hundreths;
    BYTE seconds;
    BYTE minutes;
    BYTE hours;
    BYTE day_of_week;
    BYTE day;
    BYTE month;
    BYTE year;
} mux_time_stamp;
#pragma pack()

// One Arm Bandit Host / Controller commands
const BYTE GAME_CONFIGURATION_MESSAGE = 'C';


// seconds to wait before checking
//#define DATE_TIME_SOCKET_CHECK					3600    // date and time update from Stratus
#define DATE_TIME_SOCKET_CHECK					900    // date and time update from Stratus
#define WIDE_AREA_AWARD_REQUEST_TIME            10

const UINT NICKEL =	5;

// socket ports
//#define STRATUS_DATE_TIME_PORT              37
#define STRATUS_DATE_TIME_PORT              39

#define MAX_CASHIER_SHIFTS  21

CString shift_cashier_strings[] = {CASHIER_SHIFT_1, CASHIER_SHIFT_2, CASHIER_SHIFT_3, CASHIER_SHIFT_4, CASHIER_SHIFT_5, CASHIER_SHIFT_6, CASHIER_SHIFT_7, 
                           CASHIER_SHIFT_8, CASHIER_SHIFT_9, CASHIER_SHIFT_10, CASHIER_SHIFT_11, CASHIER_SHIFT_12, CASHIER_SHIFT_13, CASHIER_SHIFT_14, 
                           CASHIER_SHIFT_15, CASHIER_SHIFT_16, CASHIER_SHIFT_17, CASHIER_SHIFT_18, CASHIER_SHIFT_19, CASHIER_SHIFT_20, CASHIER_SHIFT_21 };

CString shift_ticket_strings[] =
                          {SHIFT_TICKETS_1, SHIFT_TICKETS_2, SHIFT_TICKETS_3, SHIFT_TICKETS_4, SHIFT_TICKETS_5, SHIFT_TICKETS_6, SHIFT_TICKETS_7, 
                           SHIFT_TICKETS_8, SHIFT_TICKETS_9, SHIFT_TICKETS_10, SHIFT_TICKETS_11, SHIFT_TICKETS_12, SHIFT_TICKETS_13, SHIFT_TICKETS_14, 
                           SHIFT_TICKETS_15, SHIFT_TICKETS_16, SHIFT_TICKETS_17, SHIFT_TICKETS_18, SHIFT_TICKETS_19, SHIFT_TICKETS_20, SHIFT_TICKETS_21 };

int compareGameRedeemRecordsByMuxId (const void * grr1, const void * grr2);
int compareGameRedeemRecordsBySasId (const void * grr1, const void * grr2);

static int gb_pro_display_update_counter = 0;
static int gb_advantage_server_update_counter = 0;

static unsigned long one_arm_bandit_messages_sent = 0;

static unsigned long BIMONTHLY_SECS = 60 * 60 * 24 * 14;

static bool offline_test_flag = FALSE;

static BYTE idc_message_id;
static BYTE last_idc_tx_message_id;
static BYTE cms_message_id;

static const ULONG DEFAULT_MANUAL_DISPENSE_TRANSACTION_LIMIT = 1199;
static const ULONG DEFAULT_MANUAL_DISPENSE_DAILY_LIMIT = 2000;


UINT GbProPrinterCommThreadFunction(LPVOID pTheApp)
{
 time_t last_check_time = time(NULL);

 CControllerApp *pControllerApp = (CControllerApp*)pTheApp;

	while (pControllerApp && pControllerApp->pGbProManager && !pControllerApp->exit_application)
	{
		if (last_check_time != time(NULL))
		{
			pControllerApp->pGbProManager->ProcessGbProRequests();
			last_check_time = time(NULL);
		}

		Sleep(1);
	}

	return 0;
}

UINT EcashCd2000CommThreadFunction(LPVOID pTheApp)
{
 time_t last_check_time = time(NULL);

 CControllerApp *pControllerApp = (CControllerApp*)pTheApp;

    
	while (pControllerApp &&
	       pControllerApp->pGbProManager &&
	       !pControllerApp->exit_application &&
	       (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_ECASH) ||
            (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_CD2000))
	{
		if (last_check_time != time(NULL))
		{
            theApp.dispenserControl.CheckDispenser();
			last_check_time = time(NULL);
		}

		Sleep(1);
	}

	return 0;
}

// CControllerApp
BEGIN_MESSAGE_MAP(CControllerApp, CWinApp)
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()


// The one and only CControllerApp object
CControllerApp theApp;

// CControllerApp construction
CControllerApp::CControllerApp()
{
 CString string_message;
 EventMessage event_message;
 DWORD computer_name_length = MAX_COMPUTERNAME_LENGTH;
 int i;

    CreateDirectoryStructure();

	StratusEncryptionKey();
	StratusDecryptionKey();
	MachineEncryptionKey();
	MachineDecryptionKey();

    memset(&event_message, 0, sizeof(event_message));
    event_message.head.time_stamp = time(NULL);
    event_message.head.event_type = CONTROLLER_STARTED;
    event_message.head.data_length = (DWORD)strlen(SUBVERSION) + 5;
    DWORD controller_version = VERSION;
    memcpy(event_message.data, &controller_version, 4);
    event_message.data[4] = (BYTE)strlen(SUBVERSION);
    memcpy(&event_message.data[5], SUBVERSION, strlen(SUBVERSION));
    event_message_queue.push(event_message);

    wide_area_award_request_time    = 0;
    game_date_and_time_update       = 0;
    game_mux_message_update         = 0;
    paid_tickets_transmit           = 0;
	last_tx_event_message_time		= 0;
    last_marker_stratus_tx_time     = 0;

    one_arm_bandit_host_message_id  = 0;

    pMessageBox             	= NULL;
    pHeadGameCommControl    	= NULL;
    pCurrentDialog          	= NULL;
    pGameLockPrinterSocket  	= NULL;
    pMarkerPrinterSocket    	= NULL;
    pMultiControllerServer  	= NULL;
    pSelectCustomerIdDlg    	= NULL;
    pTechnicianDisplay      	= NULL;
    pGbProManager           	= NULL;
	pNetworkSettingsRecord		= NULL;
	pRasDialParams				= NULL;
	pHandleRasConnection		= NULL;
	pDispenserDialog			= NULL;
	pGameCollectionState		= NULL;
	pRasConnectionStatus		= NULL;
    pOneArmBanditHostSocket     = NULL;

    current_dialog_return = IDD_LOGON_DIALOG;

    hPrinterSerialPort          = INVALID_HANDLE_VALUE;
    hMarkerPrinterSerialPort    = INVALID_HANDLE_VALUE;

	exit_application			= false;
	printer_busy				= false;
	network_connection_active	= false;
    idc_tx_message_ack_pending = FALSE;

    memset(&memory_settings_ini, 0, sizeof(memory_settings_ini));
    memset(&memory_dispenser_config, 0, sizeof(memory_dispenser_config));
    memset(&memory_location_config, 0, sizeof(memory_location_config));
    memset(&current_user, 0, sizeof(current_user));
    memset(&data_display, 0, sizeof(data_display));
    memset(&stratus_tcp_ip_rx, 0, sizeof(stratus_tcp_ip_rx));
    memset(&customer_control_server_rx, 0, sizeof(customer_control_server_rx));
    memset(&last_idc_tx_message, 0, sizeof(last_idc_tx_message));
    memset(&last_cms_tx_message, 0, sizeof(last_cms_tx_message));

	customer_control_listening_time_check		= time(NULL);
	game_lock_printer_time_check				= time(NULL);
	marker_printer_time_check					= time(NULL);
	customer_control_udp_time_check				= time(NULL);
	multi_controller_server_time_check			= time(NULL);
	date_time_socket_check						= time(NULL);
	date_time_server_send_check				    = time(NULL);
	date_time_server_receive_check		        = time(NULL);
    last_idc_tx_message_sent_time		        = time(NULL) + 10;
    last_stratus_tx_message_sent_time		    = time(NULL);
    total_netwin_games = 0;
    total_netwin_drop = 0;
    total_cash_drop = 0;
    total_netwin_handle = 0;
    idc_tx_no_response_counter = 0;
    idc_tx_nack_counter = 0;
    cms_tx_no_response_counter = 0;
    cms_tx_nack_counter = 0;

    fetching_netwin_in_progress = FALSE;

    cms_players_logged_in = FALSE;
    cms_com_disconnected = TRUE;

    erroneous_ack_count = 0;

    string_message.Format("PROGRAM STARTED: v%u.%s", VERSION, SUBVERSION);
    logMessage(string_message);

    // set wave volume to max
    waveOutSetVolume(NULL, 0xF000F000);

//    initActiveHome ();
    pActiveHome = 0;
//    hr = CoCreateInstance( __uuidof(ActiveHomeScriptLib::ActiveHome), NULL, CLSCTX_INPROC, __uuidof(ActiveHomeScriptLib::IActiveHome), (LPVOID *) &pActiveHome );
    flash_active_home_light = FALSE;

//    turnOnActiveHomeLight (FALSE);
//    turnOnActiveHomeChime (FALSE);

    memset(computer_name, 0, sizeof(computer_name));
    GetComputerName(computer_name, &computer_name_length);
    // make sure the hostname is less than 20 bytes
    if (strlen(computer_name) > 10)//    memset(&computer_name[20], 0, MAX_COMPUTERNAME_LENGTH - 20);
        for (i=10; i < MAX_COMPUTERNAME_LENGTH; i++)
            computer_name[i] = 0;
        
    DeleteFile(VERSION_STRING_FILE);
    string_message.Format("PROGRAM VERSION: v%u.%s", VERSION, SUBVERSION);
    fileWrite(VERSION_STRING_FILE, 0, string_message.GetBuffer(0), string_message.GetLength());

    srand((unsigned)time(NULL));

}

CControllerApp::~CControllerApp()
{
 CString string_message;
 EventMessage event_message;

	CloseRasConnection();

    if (pMessageBox)
    {
        pMessageBox->DestroyWindow();
        delete pMessageBox;
    }

    if (pSelectCustomerIdDlg)
    {
        pSelectCustomerIdDlg->DestroyWindow();
		delete pSelectCustomerIdDlg;
    }
/*
	if (pCurrentDialog)
    {
        pCurrentDialog->DestroyWindow();
		delete pCurrentDialog;
    }
*/
    if (pTechnicianDisplay)
    {
        pTechnicianDisplay->DestroyWindow();
        delete pTechnicianDisplay;
    }

	if (pDispenserDialog)
	{
		pDispenserDialog->DestroyWindow();
		delete pDispenserDialog;
	}

	if (pGameCollectionState)
	{
		pGameCollectionState->DestroyWindow();
		delete pGameCollectionState;
	}

	if (pNetworkSettingsRecord)
		delete pNetworkSettingsRecord;

	if (pRasDialParams)
		delete pRasDialParams;

    memset(&event_message, 0, sizeof(event_message));
    event_message.head.time_stamp = time(NULL);
    event_message.head.event_type = CONTROLLER_STOPPED;
    event_message.head.data_length = (DWORD)strlen(SUBVERSION) + 5;
    DWORD controller_version = VERSION;
    memcpy(event_message.data, &controller_version, 4);
    event_message.data[4] = (BYTE)strlen(SUBVERSION);
    memcpy(&event_message.data[5], SUBVERSION, strlen(SUBVERSION));
    event_message_queue.push(event_message);

   	CheckEventMessages(true);

	if (pRasConnectionStatus)
		delete pRasConnectionStatus;
/*
    if (pActiveHome)
    {
        turnOnActiveHomeLight (FALSE);
        turnOnActiveHomeChime (FALSE);
        pActiveHome->Release();
    }
*/
    string_message.Format("PROGRAM EXIT: v%u.%s", VERSION, SUBVERSION);
    logMessage(string_message);
}

void CControllerApp::initActiveHome (void)
{
    pActiveHome = 0;
    hr = CoCreateInstance( __uuidof(ActiveHomeScriptLib::ActiveHome), NULL, CLSCTX_INPROC, __uuidof(ActiveHomeScriptLib::IActiveHome), (LPVOID *) &pActiveHome );
}

BOOL CControllerApp::OnIdle(LONG lCount)
{
 static time_t five_second_timer = 0;
 static time_t two_second_timer = 0;
 static time_t one_second_timer = 0;
 MSG peek_message;
 time_t last_check_time;
 time_t current_time;

   	CheckEventMessages(false);

    if (exit_application)
        return false;

    if (!lCount)
    {
		while (::PeekMessage(&peek_message, NULL, 0, 0, PM_NOREMOVE))
			if (!PumpMessage())
				break;
    }

    current_time = time(NULL);


	if (one_second_timer <= current_time || one_second_timer > current_time + 2)
	{
		OnIdleOneSecondTimer();
		one_second_timer = current_time + 1;
	}

	if (memory_settings_ini.idc_ip_address && memory_settings_ini.idc_tcp_port && (!IdcSocket.socket_state || (IdcSocket.socket_state == WSAEISCONN)))
    {
		CheckIdcRxMessages();
        CheckIdcTxMessages();
	}

	if (two_second_timer <= current_time || two_second_timer > current_time + 2)
	{
		OnIdleTwoSecondTimer();
		two_second_timer = current_time + 2;
	}

	if (five_second_timer <= current_time || five_second_timer > current_time + 5)
	{
		OnIdleFiveSecondTimer();
		five_second_timer = current_time + 5;
	}

    CGameCommControl *pGameCommControl = pHeadGameCommControl;

    // process all the gaming device rx tx messages on the RS 485 loop
    while (pGameCommControl != NULL)
    {
        if (pGameCommControl && !pGameCommControl->sas_com)
            pGameCommControl->ProcessRxTxGameMessages();
        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }

    if (pTechnicianDisplay && pTechnicianDisplay->destroy_dialog)
    {
        pTechnicianDisplay->DestroyWindow();
        delete pTechnicianDisplay;
        pTechnicianDisplay = NULL;
    }

	if (pDispenserDialog && pDispenserDialog->destroy_dialog)
	{
		pDispenserDialog->DestroyWindow();
		delete pDispenserDialog;
		pDispenserDialog = NULL;
	}

	if (pGameCollectionState && pGameCollectionState->destroy_dialog)
	{
		pGameCollectionState->DestroyWindow();
		delete pGameCollectionState;
		pGameCollectionState = NULL;
	}

    CheckDialogBoxes();
    CheckDataDisplayTransmit();
	customerControlListen.CheckCustomerControlMessageQueue();
    CheckForGameUnlocks();
    CheckPrinterJobs();

	if (!stratusSocket.socket_state)
    {
		// frenzy awards have been discontinued
//        if (wide_area_award_request_time < current_time && memory_location_config.club_code == GAMBLERS_BONUS_CLUB)
//            RequestGamblersBonusWideAreaAward();

        if (game_date_and_time_update < time(NULL))
            GameDateAndTimeUpdate();

        if (game_mux_message_update < time(NULL))
            GameMuxMessageUpdate();

        if (paid_tickets_transmit < time(NULL))
            PaidTicketsTransmit();

        CheckStratusRxSocket();
        CheckStratusTxQueue();
        CheckMarkerStratusTxQueue();
	}

	if (memory_settings_ini.cms_ip_address && memory_settings_ini.cms_tcp_port && (!CmsSocket.socket_state || (CmsSocket.socket_state == WSAEISCONN)))
		CheckCmsRxSocket();

	if (!customer_control_listening_time_check)
		CheckCustomerControlClientReceive();

    if (memory_settings_ini.customer_control_server_ip_address && memory_settings_ini.customer_control_server_tcp_port && !customerControlServerSocket.socket_state)
    {
        CheckCustomerControlServerRx();
		CheckCustomerControlServerEventsTx();
		CheckCustomerControlServerTxQueue();
//?????????work		SendFilesToCustomerControlServer();
	}
    // sound for game lock
//    if (sound_game_lock == 1 && PlaySound(GAME_LOCK_SOUND, NULL, SND_FILENAME | SND_ASYNC | SND_LOOP))
//        sound_game_lock = 2;


	if (::PeekMessage(&peek_message, NULL, 0, 0, PM_NOREMOVE))
		PumpMessage();

	Sleep(1);

    CWinApp::OnIdle(lCount);

 return true;
}

void CControllerApp::CheckGameControlConnections()
{
    CGameCommControl *pGameCommControl = pHeadGameCommControl;

    while (pGameCommControl)
    {
		pGameCommControl->CheckConnections();
        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }
}

void CControllerApp::OnIdleOneSecondTimer()
{
    if (!((theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_ECASH) ||
        (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_CD2000)))
        dispenserControl.CheckDispenser();

    dispenserControl.PollDispenser();

    CheckW2gPrinterJobs();

	if (!CmsSocket.socket_state || (CmsSocket.socket_state == WSAEISCONN))
    {
		CheckCmsTxQueue();
	}
}

void CControllerApp::OnIdleTwoSecondTimer()
{
    static bool light_on_flag;

    if (flash_active_home_light)
    {
        if (light_on_flag)
        {
            light_on_flag = FALSE;
        }
        else
        {
            light_on_flag = TRUE;
            PlaySound(GAME_LOCK_SOUND, NULL, SND_FILENAME | SND_ASYNC);
        }
    }
}

void CControllerApp::OnIdleFiveSecondTimer()
{
 int iii;
 time_t current_time = time(NULL);
 static ULONG xp_version_config_counter;

	CheckRemoteNetworkConnection();
	CheckGameControlConnections();

    if (pMultiControllerServer)
    {
        if (multi_controller_server_time_check)
            CheckMultiControllerServerSocket();
        else
            CheckMultiControllerClientReceive();

        for (iii=0; iii < MAXIMUM_MULTI_CONTROLLER_CONNECTIONS; iii++)
        {
            if (pMultiControllerServer->pClientSocket[iii] && pMultiControllerServer->pClientSocket[iii]->delete_socket)
            {
                delete pMultiControllerServer->pClientSocket[iii];
                pMultiControllerServer->pClientSocket[iii] = NULL;
            }
        }
    }

	// check customer control client sockets
	for (iii=0; iii < MAXIMUM_CUSTOMER_CONTROL_CLIENTS; iii++)
		if (customerControlListen.pClient[iii] && customerControlListen.pClient[iii]->delete_socket)
		{
			delete customerControlListen.pClient[iii];
			customerControlListen.pClient[iii] = NULL;
		}

    if (!email_queue.empty())
        CheckSmtpSocket();

	if (pGameLockPrinterSocket && game_lock_printer_time_check)
        CheckGameLockPrinterSocket();

	if (pMarkerPrinterSocket && marker_printer_time_check)
        CheckMarkerPrinterSocket();

    if (customer_control_udp_time_check)
        CheckCustomerControlUdpSocket();

	if (customer_control_listening_time_check)
		CheckCustomerControlListeningSocket();

	if (pGbProManager)
		pGbProManager->CheckGbProPorts();

	if (memory_settings_ini.idc_ip_address && memory_settings_ini.idc_tcp_port)
    {
        if (IdcSocket.socket_state && (IdcSocket.socket_state != WSAEISCONN))
	        CheckIdcSocket();
    }

	if (memory_settings_ini.cms_ip_address && memory_settings_ini.cms_tcp_port)
    {
        if (CmsSocket.socket_state && (CmsSocket.socket_state != WSAEISCONN))
	        CheckCmsSocket();
		else if (cms_tx_queue.empty())
        {
            // send the xp version config every minute to make sure the connection is alive
            if (xp_version_config_counter > 12)
            {
    			SendCmsXpcVersionConfigInfo();
                xp_version_config_counter = 1;
            }
            else
            {
                xp_version_config_counter++;
            }
        }
    }

	if (memory_settings_ini.stratus_ip_address && memory_settings_ini.stratus_tcp_port)
    {
	    if (network_connection_active)
	    {
		    if (stratusSocket.socket_state)
			    CheckStratusSocket();
		    else
		    {
			    // send keep alive packet
			    StratusMessageFormat stratus_msg;
			    memset(&stratus_msg, 0, sizeof(stratus_msg));
			    stratus_msg.message_length = STRATUS_HEADER_SIZE * 4;
			    stratus_msg.task_code = SLAVE_TKT_TASK_TICKET;
			    stratus_msg.send_code = SLAVE_TKT_KEEP_ALIVE;
			    QueueStratusTxMessage(&stratus_msg, FALSE);
		    }

            if (memory_settings_ini.customer_control_server_ip_address && memory_settings_ini.customer_control_server_tcp_port && customerControlServerSocket.socket_state)
			    CheckCustomerControlServer();

		    if (date_time_socket_check < time(NULL))
                CheckStratusDateTimeSocket();

	    }
	    else
	    {
		    if (!stratusSocket.socket_state)
		    {
			    logMessage("CControllerApp::OnIdleFiveSecondTimer - stratusSocket disconnect. RAS connection down.");
			    stratusSocket.Close();
			    stratusSocket.socket_state = WSAENOTCONN;
		    }

		    if (!customerControlServerSocket.socket_state)
		    {
			    logMessage("CControllerApp::OnIdleFiveSecondTimer - customerControlServerSocket disconnect. RAS connection down.");
			    customerControlServerSocket.Close();
			    customerControlServerSocket.socket_state = WSAENOTCONN;
		    }
	    }
    }
}

// CControllerApp initialization
BOOL CControllerApp::InitInstance()
{
 int iii, jjj, record_index;
 DispenserCommand dispenser_message;
 CInitialInfoDlg initialInfoDlg;
 FileSettingsIni file_settings;
 FileGbProManagerSettings gb_file_settings;
 TIMECAPS tc;
 UINT wTimerRes;
 UINT TARGET_RESOLUTION = 1;  // 1-millisecond target resolution

    m_pMainWnd = &mainWindow;

	// InitCommonControls() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	InitCommonControls();

	CWinApp::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

    if (GetFileAttributes(MAIN_SETTINGS_INI) == 0xFFFFFFFF)
    {
        memset(&file_settings, 0, sizeof(file_settings));
        fileWrite(MAIN_SETTINGS_INI, 0, &file_settings, sizeof(file_settings));

        memset(&gb_file_settings, 0, sizeof(gb_file_settings));
        fileWrite(GB_PRO_SETTINGS_INI, 0, &gb_file_settings, sizeof(gb_file_settings));
    }

    if (!LoadFilesToMemory())
        return false;

    if (memory_settings_ini.printer_comm_port)
        OpenPrinterCommPort();

    if (memory_settings_ini.marker_printer_port)
        OpenMarkerPrinterPort();

    if (memory_settings_ini.game_lock_printer_ip_address)
    {
        pGameLockPrinterSocket = new CAsyncSocket;
        CheckGameLockPrinterSocket();
    }

    if (GetFileAttributes(MASTER_CONFIG) != 0xFFFFFFFF)
    {
        pMultiControllerServer = new CMultiControllerServer;
        CheckMultiControllerServerSocket();
    }

    // open sockets
    CheckCustomerControlListeningSocket();
    CheckCustomerControlUdpSocket();

	ResetDispenser();

    printer_busy = true;
    PrinterData(PRINTER_BIG_PRINT);
    PrinterData("SYSTEM RESET\n");
    PrinterData(PRINTER_NORMAL_PRINT);
    PrinterData(convertTimeStamp(time(NULL)));
    PrinterData(PRINTER_FORM_FEED);
    printer_busy = false;

    // load dispenser queue
    if (GetFileAttributes(DISPENSER_COMMANDS) != 0xFFFFFFFF)
    {
        record_index = 0;
        while (fileRead(DISPENSER_COMMANDS, record_index, &dispenser_message, sizeof(dispenser_message)))
        {
            dispenser_command_queue.push(dispenser_message);
            record_index++;
        }

        DeleteFile(DISPENSER_COMMANDS);
    }

    // initialize crc32 table
    for (iii=0; iii <= 0xFF; iii++)
    {
        crc32_table[iii] = ReflectCrc32(iii, 8) << 24;

        for (jjj=0; jjj < 8; jjj++)
            crc32_table[iii] = (crc32_table[iii] << 1) ^ (crc32_table[iii] & (1 << 31) ? 0x04c11db7 : 0);

        crc32_table[iii] = ReflectCrc32(crc32_table[iii], 32);
    }

    if (timeGetDevCaps(&tc, sizeof(TIMECAPS)) != TIMERR_NOERROR) 
    {
        // Error; application can't continue.
    }

    wTimerRes = min(max(tc.wPeriodMin, TARGET_RESOLUTION), tc.wPeriodMax);
    timeBeginPeriod(wTimerRes); 

	if (pGbProManager)
		AfxBeginThread(GbProPrinterCommThreadFunction, this);

    if ((theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_ECASH) ||
        (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_CD2000))
		AfxBeginThread(EcashCd2000CommThreadFunction, this);

    return true;
}

bool CControllerApp::LoadFilesToMemory()
{
 int file_index;
 FileGameConfig file_game_config;
 FileLocationConfig file_location_config;
 FileDispenserConfig file_dispenser_config;
 bool write_dispenser_info = FALSE;

    if (!fileRead(MAIN_SETTINGS_INI, 0, &memory_settings_ini, sizeof(memory_settings_ini)))
    {
        logMessage("CControllerApp::InitInstance - error reading 'MAIN_SETTINGS_INI' file.");
        return false;
    }

    if (GetFileAttributes(DISPENSER_INFO) != 0xFFFFFFFF &&
        fileRead(DISPENSER_INFO, 0, &file_dispenser_config, sizeof(file_dispenser_config)))
    {
        memcpy(&memory_dispenser_config, &file_dispenser_config.memory, sizeof(memory_dispenser_config));
        if (!memory_dispenser_config.dispenser_payout_limit)
        {
             memory_dispenser_config.dispenser_payout_limit = DEFAULT_PAYOUT_LIMIT;
             write_dispenser_info = TRUE;
        }
        if (!memory_dispenser_config.manual_dispense_per_transaction_limit)
        {
             memory_dispenser_config.manual_dispense_per_transaction_limit = DEFAULT_MANUAL_DISPENSE_TRANSACTION_LIMIT;
             write_dispenser_info = TRUE;
        }

        if (!memory_dispenser_config.manual_dispense_daily_limit)
        {
             memory_dispenser_config.manual_dispense_daily_limit = DEFAULT_MANUAL_DISPENSE_DAILY_LIMIT;
             write_dispenser_info = TRUE;
        }
        if (write_dispenser_info)
        {
            memcpy(&file_dispenser_config.memory, &memory_dispenser_config, sizeof(memory_dispenser_config));
            fileWrite(DISPENSER_INFO, 0, &file_dispenser_config, sizeof(file_dispenser_config));
        }
    }

    if (GetFileAttributes(LOCATION_INFO) != 0xFFFFFFFF &&
        fileRead(LOCATION_INFO, 0, &file_location_config, sizeof(file_location_config)))
            memcpy(&memory_location_config, &file_location_config.memory, sizeof(memory_location_config));

    if (GetFileAttributes(MANUAL_DISPENSE_DAILY_LIMIT) != 0xFFFFFFFF)
        fileRead(MANUAL_DISPENSE_DAILY_LIMIT, 0, &manual_dispense_data, sizeof(manual_dispense_data));

    if (GetFileAttributes(PENDING_CMS_TX_MESSAGES) != 0xFFFFFFFF)
        QueuePendingCmsTxMessages ();

    if (GetFileAttributes(PENDING_STRATUS_TX_MESSAGES) != 0xFFFFFFFF)
        QueuePendingStratusTxMessages ();

	if (theApp.memory_settings_ini.activate_gb_advantage_support)
    {
        pGbProManager = new CGbProManager;
    }

    if (GetFileAttributes(GAME_INFO) != 0xFFFFFFFF)
    {
        file_index = 0;

        while (fileRead(GAME_INFO, file_index, &file_game_config, sizeof(file_game_config)))
        {
            file_index++;
            AddGameDevice(&file_game_config.memory);
        }
    }

 return true;
}

BYTE CControllerApp::AddGameDevice(MemoryGameConfig *pMemoryGameConfig)
{
    CString error_message;
    BYTE return_value = 0;
    BYTE  sas_id = pMemoryGameConfig->sas_id;
    BYTE  mux_id = pMemoryGameConfig->mux_id;


    if (!pMemoryGameConfig->ucmc_id)
    {
        error_message.Format("CControllerApp::AddSasGameDevice - invalid data (sas_id, ucmc id): %u,%u",
            pMemoryGameConfig->sas_id, pMemoryGameConfig->ucmc_id);
        logMessage(error_message);
        return 1;
    }

    if (pMemoryGameConfig->mux_id && pMemoryGameConfig->sas_id && (pMemoryGameConfig->mux_id != pMemoryGameConfig->sas_id))
    {
        error_message = "CControllerApp::AddSasGameDevice - invalid configuration sas id != mux id";
        logMessage(error_message);
        return 1;
    }

    if (pMemoryGameConfig->mux_id && !pMemoryGameConfig->port_number)
    {
        error_message.Format("CControllerApp::AddMuxGameDevice - invalid data (port,ucmc id): %u,%u,.",
            pMemoryGameConfig->port_number, pMemoryGameConfig->ucmc_id);
        logMessage(error_message);
        return 1;
    }

    if (pMemoryGameConfig->sas_id)
    {
        pMemoryGameConfig->mux_id = 0;
        return_value = AddSasGameDevice(pMemoryGameConfig);
        pMemoryGameConfig->mux_id = mux_id;

    }

    if (!return_value && pMemoryGameConfig->mux_id)
    {
        pMemoryGameConfig->sas_id = 0;
        return_value = AddMuxGameDevice(pMemoryGameConfig);
        pMemoryGameConfig->sas_id = sas_id;
    }

    return return_value;
}


BYTE CControllerApp::AddSasGameDevice(MemoryGameConfig *pMemoryGameConfig)
{
    CString error_message;
    GameCommPortInfo game_comm_port_info;
    CGameDevice *pGameDevice = NULL;
    CGameCommControl *pGameCommControl, *pAddGameCommControl = NULL;


    pGameCommControl = pHeadGameCommControl;

    // search for duplicate ucmc id and mux id
    while (pGameCommControl)
    {
        pGameDevice = pGameCommControl->pHeadGameDevice;

        if (!pAddGameCommControl)
            pAddGameCommControl = pGameCommControl;

        // check for duplicate ucmc id and sas id for only the GameCommControl objects that correspond to SAS direct connects.
        if (pGameCommControl->sas_com)
        {

            if (pGameDevice && (pGameDevice->memory_game_config.ucmc_id == pMemoryGameConfig->ucmc_id))
            {
                error_message.Format("CControllerApp::AddSasGameDevice - duplicate ucmc id: %u.", pMemoryGameConfig->ucmc_id);
                logMessage(error_message);
                return 2;
            }

            if (pGameCommControl->sas_com->sas_id == pMemoryGameConfig->sas_id)
            {
                error_message.Format("CControllerApp::AddSasGameDevice - duplicate sas_id %u",
                    pMemoryGameConfig->sas_id);
                logMessage(error_message);
                return 5;
            }
        }

        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }

    if (!pMemoryGameConfig->customer_id)
        pMemoryGameConfig->customer_id = memory_location_config.customer_id;

    memset(&game_comm_port_info, 0, sizeof(game_comm_port_info));

    pAddGameCommControl = new CGameCommControl(&game_comm_port_info, pMemoryGameConfig);

    if (!pHeadGameCommControl)
        pHeadGameCommControl = pAddGameCommControl;
    else
    {
        pGameCommControl = pHeadGameCommControl;

        while (pGameCommControl->pNextGameCommControl)
            pGameCommControl = pGameCommControl->pNextGameCommControl;

        pGameCommControl->pNextGameCommControl = pAddGameCommControl;
    }

	SendMachineSpecificData(pAddGameCommControl->pHeadGameDevice);

    pAddGameCommControl->GbAccountLoginInfoRequest();


 return 0;
}


BYTE CControllerApp::AddMuxGameDevice(MemoryGameConfig *pMemoryGameConfig)
{
    CString error_message;
    GameCommPortInfo game_comm_port_info;
    CGameDevice *pGameDevice,*pNewGameDevice, *pPreviousGameDevice;
    CGameCommControl *pGameCommControl, *pAddGameCommControl = NULL;


    pGameCommControl = pHeadGameCommControl;

    // search for duplicate ucmc id and mux id
    while (pGameCommControl)
    {
        // only check GameCommControl objects that are NOT SAS direct connects
        if (!pGameCommControl->sas_com)
        {
            pGameDevice = pGameCommControl->pHeadGameDevice;

            while (pGameDevice)
            {
                if (pGameCommControl->port_info.port_number)
                {
                    if (!pAddGameCommControl && pGameCommControl->port_info.port_number == pMemoryGameConfig->port_number)
                        pAddGameCommControl = pGameCommControl;
                }

                // check for duplicate ucmc id
                if (pGameDevice->memory_game_config.ucmc_id == pMemoryGameConfig->ucmc_id)
                {
                    error_message.Format("CControllerApp::AddMuxGameDevice - duplicate ucmc id: %u.", pMemoryGameConfig->ucmc_id);
                    logMessage(error_message);
                    return 2;
                }

    //            else if ((pGameCommControl->port_info.port_number == pMemoryGameConfig->port_number) &&
                if (pMemoryGameConfig->mux_id && (pGameCommControl->port_info.port_number == pMemoryGameConfig->port_number) &&
                         (pGameDevice->memory_game_config.mux_id == pMemoryGameConfig->mux_id))
                {
                    error_message.Format("CControllerApp::AddMuxGameDevice - duplicate mux id %u, port %u, ucmc id %u.",
                        pMemoryGameConfig->mux_id, pMemoryGameConfig->port_number, pMemoryGameConfig->ucmc_id);
                    logMessage(error_message);
                    return 3;
                }

                pGameDevice = pGameDevice->pNextGame;
            }
        }

        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }

    if (!pMemoryGameConfig->customer_id)
        pMemoryGameConfig->customer_id = memory_location_config.customer_id;

    // check ip address
    if (pAddGameCommControl)
    {
        if (memcmp(&pMemoryGameConfig->ip_address, &pAddGameCommControl->port_info.ip_address, 4))
        {
            BYTE *port_ip_address = (BYTE*)&pGameCommControl->port_info.ip_address;
            BYTE *game_ip_address = (BYTE*)&pMemoryGameConfig->ip_address;
            error_message.Format("CControllerApp::AddMuxGameDevice - invalid ip address match, port %u, ucmc id %u, port ip %u.%u.%u.%u, game ip %u.%u.%u.%u.",
                pMemoryGameConfig->port_number, pMemoryGameConfig->ucmc_id, port_ip_address[0], port_ip_address[1],
                port_ip_address[2], port_ip_address[3], game_ip_address[0], game_ip_address[1], game_ip_address[2], game_ip_address[3]);
            logMessage(error_message);
            return 4;
        }
    }
    else
    {
        game_comm_port_info.port_number = pMemoryGameConfig->port_number;
        game_comm_port_info.baud_rate = memory_location_config.baud_rate;
        memcpy(&game_comm_port_info.ip_address, &pMemoryGameConfig->ip_address, 4);
        pAddGameCommControl = new CGameCommControl(&game_comm_port_info, pMemoryGameConfig);

        if (!pHeadGameCommControl)
            pHeadGameCommControl = pAddGameCommControl;
        else
        {
            pGameCommControl = pHeadGameCommControl;

            while (pGameCommControl)
            {
                if (!pGameCommControl->pNextGameCommControl)
                {
                    pGameCommControl->pNextGameCommControl = pAddGameCommControl;
                    pGameCommControl = NULL;
                }
                else
                    pGameCommControl = pGameCommControl->pNextGameCommControl;
            }
        }
    }

    pNewGameDevice = new CGameDevice(pMemoryGameConfig);

    if (!pAddGameCommControl->pHeadGameDevice)
        pAddGameCommControl->pHeadGameDevice = pNewGameDevice;
    else
    {
        pGameDevice = pAddGameCommControl->pHeadGameDevice;
        pPreviousGameDevice = NULL;

        while (pGameDevice)
        {
            if (pNewGameDevice->memory_game_config.mux_id < pGameDevice->memory_game_config.mux_id)
            {
                pNewGameDevice->pNextGame = pGameDevice;

                if (!pPreviousGameDevice)
                    pAddGameCommControl->pHeadGameDevice = pNewGameDevice;
                else
                    pPreviousGameDevice->pNextGame = pNewGameDevice;

                pGameDevice = NULL;
            }
            else
            {
                if (!pGameDevice->pNextGame)
                {
                    pGameDevice->pNextGame = pNewGameDevice;
                    pGameDevice = NULL;
                }
                else
                {
                    pPreviousGameDevice = pGameDevice;
                    pGameDevice = pGameDevice->pNextGame;
                }
            }
        }
    }

	SendMachineSpecificData(pNewGameDevice);

 return 0;
}

void CControllerApp::BroadcastDispenserPayoutLimit (void)
{
 CGameDevice *pGameDevice;
 CGameCommControl *pGameCommControl = pHeadGameCommControl;

    while (pGameCommControl)
    {
        // do not send message to direct connect games
        if (!pGameCommControl->sas_com)
        {
            pGameDevice = pGameCommControl->pHeadGameDevice;

            while (pGameDevice)
            {
                pGameDevice->SetDispenserPayoutLimit();                
                pGameDevice = pGameDevice->pNextGame;
            }

        }
        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }
}

CGameDevice* CControllerApp::GetGamePointer(DWORD ucmc_id)
{
 CGameDevice *pGameDevice;
 CGameCommControl *pGameCommControl = pHeadGameCommControl;

    while (pGameCommControl)
    {
        pGameDevice = pGameCommControl->pHeadGameDevice;

        while (pGameDevice)
        {
            if (pGameDevice->memory_game_config.ucmc_id == ucmc_id)
                return pGameDevice;

            pGameDevice = pGameDevice->pNextGame;
        }

        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }

 return NULL;

}

CGameDevice* CControllerApp::GetSasGamePointer(DWORD ucmc_id)
{
 CGameDevice *pGameDevice = NULL;
 CGameCommControl *pGameCommControl = pHeadGameCommControl;

    while (pGameCommControl)
    {
        if (pGameCommControl->sas_com)
        {
            pGameDevice = pGameCommControl->pHeadGameDevice;

            while (pGameDevice)
            {
                if (pGameDevice->memory_game_config.ucmc_id == ucmc_id)
                    return pGameDevice;

                pGameDevice = pGameDevice->pNextGame;
            }
        }

        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }

 return NULL;
}

CGameDevice* CControllerApp::GetSasGamePointer(char* machine_id)
{
 CGameDevice *pGameDevice = NULL;
 CGameCommControl *pGameCommControl = pHeadGameCommControl;

    while (pGameCommControl)
    {
        if (pGameCommControl->sas_com)
        {
            pGameDevice = pGameCommControl->pHeadGameDevice;

            while (pGameDevice)
            {
                if (!strcmp(pGameDevice->memory_game_config.machine_id, machine_id))
                    return pGameDevice;

                pGameDevice = pGameDevice->pNextGame;
            }
        }

        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }

 return NULL;
}

CGameDevice* CControllerApp::GetGbInterfaceGamePointer(DWORD ucmc_id)
{
 CGameDevice *pGameDevice = 0;
 CGameCommControl *pGameCommControl = pHeadGameCommControl;

	while (pGameCommControl)
	{
		if (!pGameCommControl->sas_com)
		{
			pGameDevice = pGameCommControl->pHeadGameDevice;

			while (pGameDevice)
			{
                // make sure there is a corresponding sas game pointer, or this is not a GB Interface
				if ((pGameDevice->memory_game_config.ucmc_id == ucmc_id) && GetSasGamePointer(ucmc_id))
					return pGameDevice;

				pGameDevice = pGameDevice->pNextGame;
			}
		}

		pGameCommControl = pGameCommControl->pNextGameCommControl;
	}

 return NULL;
}

CGameDevice* CControllerApp::GetGamePointer(BYTE mux_id)
{
 CGameDevice *pGameDevice;
 CGameCommControl *pGameCommControl = pHeadGameCommControl;

    while (pGameCommControl)
    {
        pGameDevice = pGameCommControl->pHeadGameDevice;

        while (pGameDevice)
        {
            if (pGameDevice->memory_game_config.mux_id == mux_id)
                return pGameDevice;

            pGameDevice = pGameDevice->pNextGame;
        }

        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }

 return NULL;
}

CGameDevice* CControllerApp::GetGamePointer(BYTE mux_id, WORD game_port)
{
 CGameDevice *pGameDevice;
 CGameCommControl *pGameCommControl = pHeadGameCommControl;

    while (pGameCommControl)
    {
        pGameDevice = pGameCommControl->pHeadGameDevice;

        while (pGameDevice)
        {
			if ((pGameDevice->memory_game_config.mux_id == mux_id) && (pGameDevice->memory_game_config.port_number == game_port))
                return pGameDevice;

            pGameDevice = pGameDevice->pNextGame;
        }

        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }

 return NULL;
}

CGameCommControl* CControllerApp::GetGameCommControlPointer(DWORD ucmc_id)
{
    CGameCommControl *pGameCommControl = NULL;

    pGameCommControl = GetGbInterfaceGameCommControlPointer(ucmc_id);

    if (pGameCommControl == NULL)
       pGameCommControl = GetSasGameCommControlPointer(ucmc_id);

 return pGameCommControl;
}

CGameCommControl* CControllerApp::GetGbInterfaceGameCommControlPointer(DWORD ucmc_id)
{
 CGameDevice *pGameDevice;
 CGameCommControl *pGameCommControl = pHeadGameCommControl;

    while (pGameCommControl)
    {
        if (!pGameCommControl->sas_com)
        {
            pGameDevice = pGameCommControl->pHeadGameDevice;

            while (pGameDevice)
            {
                if (pGameDevice->memory_game_config.ucmc_id == ucmc_id)
                    return pGameCommControl;

                pGameDevice = pGameDevice->pNextGame;
            }
        }

        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }

 return NULL;
}

// return next game comm control object, or returns NULL if there is not another game comm control object to be found
CGameCommControl* CControllerApp::GetNextGameCommControlPointer(CGameCommControl *pGameCommControl)
{
    CGameCommControl *pNextGameCommControl = NULL;

    if (pGameCommControl)
    {
        if (pGameCommControl->pNextGameCommControl)
            pNextGameCommControl = pGameCommControl->pNextGameCommControl;
        else
        {
            if (pGameCommControl != pHeadGameCommControl)
                pNextGameCommControl = pHeadGameCommControl;
        }
    }

    return pNextGameCommControl;
}

CGameCommControl* CControllerApp::GetSasGameCommControlPointer(DWORD ucmc_id)
{
 CGameDevice *pGameDevice;
 CGameCommControl *pGameCommControl = pHeadGameCommControl;

    while (pGameCommControl)
    {
        if (pGameCommControl->sas_com)
        {
            pGameDevice = pGameCommControl->pHeadGameDevice;

            while (pGameDevice)
            {
                if (pGameDevice->memory_game_config.ucmc_id == ucmc_id)
                    return pGameCommControl;

                pGameDevice = pGameDevice->pNextGame;
            }
        }

        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }

 return NULL;
}

/*
CGameCommControl* CControllerApp::GetNextSasGameCommControlPointer(CGameCommControl *pGameCommControl)
{
    CGameCommControl *pNextGameCommControl = NULL;
    CGameCommControl *pNextSasGameCommControl = NULL;

    pNextGameCommControl = GetNextGameCommControlPointer(pGameCommControl);

    if (pNextGameCommControl->sas_com)
    {
        pNextSasGameCommControl = pNextGameCommControl;
    }
    else
    {
        while (!pNextSasGameCommControl && (pNextGameCommControl != pGameCommControl))
        {
            pNextGameCommControl = GetNextGameCommControlPointer(pGameCommControl);

            if (pNextGameCommControl->sas_com)
            {
                pNextSasGameCommControl = pNextGameCommControl;
            }
        }
    }

    return pNextSasGameCommControl;
}

CGameCommControl* CControllerApp::GetFirstSasGameCommControlPointer(void)
{
    CGameCommControl *pNextGameCommControl = pHeadGameCommControl;
    CGameCommControl *pFirstSasGameCommControl = NULL;

    if (pNextGameCommControl->sas_com)    
        pFirstSasGameCommControl = pNextGameCommControl;
    else
    {
        pNextGameCommControl = GetNextSasGameCommControlPointer(pNextGameCommControl);        

        while (!pFirstSasGameCommControl && pNextGameCommControl && (pNextGameCommControl != pHeadGameCommControl))
        {
            if (pNextGameCommControl->sas_com)    
                pFirstSasGameCommControl = pNextGameCommControl;
            else
                pNextGameCommControl = GetNextSasGameCommControlPointer(pNextGameCommControl);        
        }   
    }

    return pFirstSasGameCommControl;
}

WORD CControllerApp::GetGameCommControlCount (void)
{
    CGameCommControl *pGameCommControl = pHeadGameCommControl;
    WORD game_comm_control_count = 0;

    while (pGameCommControl)
    {
        game_comm_control_count++;
        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }

    return game_comm_control_count;
}
*/

void CControllerApp::LogoutAllCmsPlayers (void)
{
    CmsProtocolHeader header;

    if (theApp.memory_settings_ini.cms_ip_address && cms_players_logged_in)
    {
        cms_players_logged_in = FALSE;
        CGameCommControl *pGameCommControl = pHeadGameCommControl;
        CGameDevice *pGameDevice = 0;

        while (pGameCommControl)
        {
            if (pGameCommControl && pGameCommControl->sas_com)
            {
                pGameDevice = pGameCommControl->pHeadGameDevice;
                if (pGameDevice && pGameDevice->memory_game_config.machine_id && pGameDevice->account_login_data.account_number)
                {
                    memset (&header, 0, sizeof(CmsProtocolHeader));
                    memcpy(header.machine_id, pGameDevice->memory_game_config.machine_id, sizeof(header.machine_id));
                    SendIdcCmsLogoutMessage (&header, pGameDevice->account_login_data.account_number);
                    memset (&pGameDevice->account_login_data, 0, sizeof(pGameDevice->account_login_data));
                }
            }
			pGameCommControl = pGameCommControl->pNextGameCommControl;
        }
    }

}


void CControllerApp::updateGbInterfaceData(UpdateGbInterfaceData* update_gb_interface_data, DWORD ucmc_id)
{
    CGameCommControl *pGameCommControl = GetGbInterfaceGameCommControlPointer(ucmc_id);
    CGameDevice* pGameDevice = NULL;

    if (pGameCommControl)
    {
        pGameDevice = GetGbInterfaceGamePointer(ucmc_id);
        if (pGameDevice)
        {
            pGameDevice->updateGbInterfaceData(update_gb_interface_data);
        }
    }
}

void CControllerApp::initializeGbInterfaceData(GbInterfaceInitialize* initialize_gb_interface, DWORD ucmc_id)
{
    CGameCommControl *pGameCommControl = GetGbInterfaceGameCommControlPointer(ucmc_id);
    CGameDevice* pGameDevice = NULL;

    if (pGameCommControl)
    {
        pGameDevice = GetGbInterfaceGamePointer(ucmc_id);
        if (pGameDevice)
        {
            pGameDevice->initializeGbInterfaceData(initialize_gb_interface);
        }
    }
}

void CControllerApp::updateSasCommunicationStatus(bool communicating, DWORD ucmc_id)
{
    CGameCommControl *pGameCommControl = GetGbInterfaceGameCommControlPointer(ucmc_id);
    CGameDevice* pGameDevice = NULL;

    if (pGameCommControl)
    {
        pGameDevice = GetGbInterfaceGamePointer(ucmc_id);
        if (pGameDevice)
            pGameDevice->updateSasCommunicationStatus(communicating);
    }
}

void CControllerApp::updateGbInterfaceWithBingoWinningHandInfo (BingoWinningHand* bingo_winning_hand, DWORD ucmc_id)
{
    CGameCommControl *pGameCommControl = GetGbInterfaceGameCommControlPointer(ucmc_id);
    CGameDevice* pGameDevice = NULL;

    if (pGameCommControl)
    {
        pGameDevice = GetGbInterfaceGamePointer(ucmc_id);
        if (pGameDevice)
        {
            pGameDevice->updateGbInterfaceWithBingoWinningHandInfo(bingo_winning_hand);
        }
    }
}

void CControllerApp::updateGbInterfaceWithBingoWinningCardInfo (UpdateGbInterfaceWinningCardData* winning_card_data ,DWORD ucmc_id)
{
    CGameCommControl *pGameCommControl = GetGbInterfaceGameCommControlPointer(ucmc_id);
    CGameDevice* pGameDevice = NULL;

    if (pGameCommControl)
    {
        pGameDevice = GetGbInterfaceGamePointer(ucmc_id);
        if (pGameDevice)
        {
            pGameDevice->updateGbInterfaceWithBingoWinningCardInfo(winning_card_data);
        }
    }
}

// send dispenser bill count to Stratus
void CControllerApp::SendDispenserBillCount(bool initial_fill, DWORD ucmc_id)
{
 StratusMessageFormat stratus_msg;
 BillMessage bill_message;

    memset(&stratus_msg, 0, sizeof(stratus_msg));
    memset(&bill_message, 0, sizeof(bill_message));

    if (initial_fill)
        bill_message.message_type = 'I';
    else
        bill_message.message_type = 'S';

    stratus_msg.task_code = TKT_TASK_TICKET;
    stratus_msg.send_code = TKT_SEND_BILL_COUNT;
    stratus_msg.mux_id = CONTROLLER_MUX_ADDRESS;
    stratus_msg.ucmc_id = memory_dispenser_config.ucmc_id;
    stratus_msg.message_length = sizeof(bill_message) + STRATUS_HEADER_SIZE;
    stratus_msg.property_id = memory_location_config.property_id;
    stratus_msg.club_code = memory_location_config.club_code;
    stratus_msg.customer_id = memory_location_config.customer_id;
    stratus_msg.data_to_event_log = true;

    switch (memory_dispenser_config.dispenser_type)
    {
        case DISPENSER_TYPE_CASH_DRAWER:
            if (!initial_fill)
                return;
            break;

        case DISPENSER_TYPE_JCM_20:
        case DISPENSER_TYPE_LCDM1000_20:
            if (initial_fill)
                bill_message.bill_amount_20 = dispenserControl.memory_dispenser_values.impress_amount1 / 100;
            else
            {
                bill_message.bill_amount_20 = dispenserControl.memory_dispenser_values.dispense_grand_total / 100;
                bill_message.reject_amount_20 = dispenserControl.memory_dispenser_values.reject_bills1 * 20;
            }
            break;

        case DISPENSER_TYPE_JCM_100:
            if (initial_fill)
                bill_message.bill_amount_100 = dispenserControl.memory_dispenser_values.impress_amount1 / 100;
            else
            {
                bill_message.bill_amount_100 = dispenserControl.memory_dispenser_values.dispense_grand_total / 100;
                bill_message.reject_amount_100 = dispenserControl.memory_dispenser_values.reject_bills1 * 100;
            }
            break;

        case DISPENSER_TYPE_ECASH:
        case DISPENSER_TYPE_CD2000:
        case DISPENSER_TYPE_DIEBOLD:
        case DISPENSER_TYPE_LCDM2000_20_100:
        case DISPENSER_TYPE_LCDM4000_20_100:
            if (initial_fill)
            {
                bill_message.bill_amount_100 = dispenserControl.memory_dispenser_values.impress_amount1 / 100;
                bill_message.bill_amount_20 = dispenserControl.memory_dispenser_values.impress_amount2 / 100;
            }
            else
            {
                bill_message.bill_amount_100 = dispenserControl.memory_dispenser_values.dispense_total1 / 100;
                bill_message.bill_amount_20 = dispenserControl.memory_dispenser_values.dispense_total2 / 100;
                bill_message.reject_amount_100 = dispenserControl.memory_dispenser_values.reject_bills1 * 100;
                bill_message.reject_amount_20 = dispenserControl.memory_dispenser_values.reject_bills2 * 20;
            }
            break;


        default:
            return;
    }


    SendIdcDispenseInfoMessage (&bill_message, ucmc_id);

    endian4(bill_message.bill_amount_100);
    endian4(bill_message.bill_amount_20);
    endian4(bill_message.reject_amount_100);
    endian4(bill_message.reject_amount_20);

    memcpy(stratus_msg.data, &bill_message, sizeof(bill_message));
    QueueStratusTxMessage(&stratus_msg, TRUE);
}

void CControllerApp::ResetDispenser()
{
 DispenserCommand dispenser_message;

    memset(&dispenser_message, 0, sizeof(dispenser_message));
    switch (memory_dispenser_config.dispenser_type)
    {
        // reset jcm dispenser
        case DISPENSER_TYPE_JCM_20:
        case DISPENSER_TYPE_JCM_100:
            dispenser_message.length = 1;
            dispenser_message.data[0] = JCM_DISPENSER_RESET;
            dispenser_command_queue.push(dispenser_message);
            dispenser_message.data[0] = JCM_DISPENSER_INITIALIZE;
            dispenser_command_queue.push(dispenser_message);
            dispenserControl.jcm_soft_reset_sent = TRUE;
            break;

        // reset ecash dispenser
        case DISPENSER_TYPE_ECASH:
            // set the nation and number of installed cassettes
            dispenser_message.length = 4;
            dispenser_message.data[0] = ECASH_NATION_AND_NUMBER_OF_INSTALLED_CASSETTES;
            dispenser_message.data[1] = 'U';  // USA notes
            dispenser_message.data[2] = 2;    // number of installed cassettes
            dispenser_command_queue.push(dispenser_message);
            break;

        case DISPENSER_TYPE_CD2000:
            dispenser_message.length = 2;
            dispenser_message.data[0] = CD2000_INITIALIZATION;
            dispenser_message.data[1] = 3;
            dispenser_command_queue.push(dispenser_message);
            break;

        case DISPENSER_TYPE_LCDM1000_20:
        case DISPENSER_TYPE_LCDM2000_20_100:
        case DISPENSER_TYPE_LCDM4000_20_100:
            dispenserControl.SendPuloonPurgeMessage();
            break;




    }
}

void CControllerApp::PrintDispenserInfo()
{
 DWORD breakage;
 CString prt_buf;
 FileCashierShift cashier_shift;
 DrinkCompTotal drink_comps;

    printer_busy = true;

    if (memory_dispenser_config.dispenser_type == DISPENSER_TYPE_CASH_DRAWER)
        PrinterTitle("DRAWER INFO");
    else
        PrinterTitle("DISPENSER INFO");

    prt_buf.Format("\nTickets Paid  : $%9.2f\n", (double)dispenserControl.memory_dispenser_values.tickets_amount / 100);
    PrinterData(prt_buf.GetBuffer(0));

    if (memory_dispenser_config.dispenser_type == DISPENSER_TYPE_CASH_DRAWER)
    {
        prt_buf.Format("Impress Left  : $%9.2f\n", ((double)dispenserControl.memory_dispenser_values.impress_amount1 -
            (double)dispenserControl.memory_dispenser_values.tickets_amount) / 100);
        PrinterData(prt_buf.GetBuffer(0));

        if (!fileRead(CASHIER_SHIFT_1, 0, &cashier_shift, sizeof(cashier_shift)))
            memset(&cashier_shift, 0, sizeof(cashier_shift));

        if (cashier_shift.cashier_id)
            breakage = cashier_shift.beginning_balance - cashier_shift.redeem_amount + cashier_shift.active_shift_fills +
                cashier_shift.mgr_adjust_plus - cashier_shift.mgr_adjust_minus;
        else
        {
            breakage = cashier_shift.pre_shift_fills + cashier_shift.mgr_adjust_plus - cashier_shift.mgr_adjust_minus;

            if (fileRead(CASHIER_SHIFT_2, 0, &cashier_shift, sizeof(cashier_shift)))
                breakage += cashier_shift.ending_balance;
        }

        prt_buf.Format("Cashier Calculated\nCash Drawer   : $%9.2f\n\n", (double)breakage / 100);
        PrinterData(prt_buf.GetBuffer(0));
    }
    else
    {
        prt_buf.Format("Dispense Total: $%9.2f\n", (double)dispenserControl.memory_dispenser_values.dispense_grand_total / 100);
        PrinterData(prt_buf.GetBuffer(0));
        breakage = dispenserControl.memory_dispenser_values.dispense_grand_total - dispenserControl.memory_dispenser_values.tickets_amount;
        prt_buf.Format("Breakage      : $%9.2f\n\n", (double)breakage / 100);
        PrinterData(prt_buf.GetBuffer(0));
    }

    prt_buf.Format("Ticket Count  :  %9d\n", dispenserControl.memory_dispenser_values.number_tickets);
    PrinterData(prt_buf.GetBuffer(0));

    if (!fileRead(DRINK_COMPS_TOTAL, 0, &drink_comps, sizeof(drink_comps)))
        memset(&drink_comps, 0, sizeof(drink_comps));

    if (memory_settings_ini.stratus_ip_address && memory_settings_ini.stratus_tcp_port && memory_settings_ini.masters_ip_address)
	{
		prt_buf.Format("Comp Count    :  %9d\n", drink_comps.number);
		PrinterData(prt_buf.GetBuffer(0));
		prt_buf.Format("Comp Amount   : $%9.2f\n", (double)drink_comps.total_amount);
		PrinterData(prt_buf.GetBuffer(0));
	}

    PrinterData(PRINTER_FORM_FEED);
    printer_busy = false;
}

void CControllerApp::OpenCashDrawer()
{
 DispenserCommand message;

    memset(&message, 0, sizeof(message));
    message.length = 1;
    message.data[0] = OPEN_CASH_DRAWER;
    dispenser_command_queue.push(message);
    MessageWindow(false, "OPENING DRAWER......", "PLEASE CLOSE CASH\nDRAWER WHEN FINISHED");
}

// Check Dispenser and cash drawer for enough money to pay ticket
bool CControllerApp::DispenserMoneyLowCheck(DWORD amount)
{
 BYTE flag = 0;
 int hold1, hold2;
 int reject_error_margin = 0;
 DWORD bill_cnt1, bill_cnt2, disp_cnt2;


    printer_busy = true;

    switch (memory_dispenser_config.dispenser_type)
    {
        case DISPENSER_TYPE_ECASH:
        case DISPENSER_TYPE_CD2000:
        case DISPENSER_TYPE_DIEBOLD:
            // add 5 bill error margin
            reject_error_margin = 5;    
        case DISPENSER_TYPE_LCDM2000_20_100:
        case DISPENSER_TYPE_LCDM4000_20_100:
            hold1 = dispenserControl.memory_dispenser_values.impress_amount1 - dispenserControl.memory_dispenser_values.dispense_total1 -
                ((dispenserControl.memory_dispenser_values.reject_bills1 + reject_error_margin) * 10000);
            hold2 = dispenserControl.memory_dispenser_values.impress_amount2 - dispenserControl.memory_dispenser_values.dispense_total2 -
                ((dispenserControl.memory_dispenser_values.reject_bills2 + reject_error_margin) * 2000);

            // don't pay if both denoms are low
            if (hold1 <= 0 && hold2 <= 0)
            {
                hold2 = 0;
                flag  = 1;
            }
            else if (hold1 <= 0)
            {
                // don't pay if amount exceeds hold1 and hold2
                if ((int)amount > hold2)
                    flag = 1;
            }
            else if (hold2 <= 0)
            {
                // don't pay if amount exceeds hold1 and hold2
                if ((int)amount > hold1)
                    flag = 1;
            }
            else
            {
                // don't pay if amount exceeds hold1 and hold2
                if ((int)amount > (hold1 + hold2))
                    flag = 1;
            }

            // check for enough $20s to pay ticket 
            if (!memory_dispenser_config.dispense_100s_when_20s_low)
            {
                bill_cnt1 = amount / 10000;
                bill_cnt2 = amount / 2000;
                bill_cnt2 -= bill_cnt1 * 5;
                disp_cnt2 = hold2 / 2000;

                // add 1 in case of breakage
                if (amount >= dispenserControl.memory_dispenser_values.dispense_grand_total - dispenserControl.memory_dispenser_values.tickets_amount)
                    bill_cnt2++;

                if (bill_cnt2 > disp_cnt2)
                    flag = 1;
            }
            break;

        case DISPENSER_TYPE_JCM_20:
        case DISPENSER_TYPE_LCDM1000_20:
            // add 5 bill error margin
            hold1 = dispenserControl.memory_dispenser_values.impress_amount1 - dispenserControl.memory_dispenser_values.dispense_grand_total - 
                ((dispenserControl.memory_dispenser_values.reject_bills1 + 5) * 2000);

            // don't pay if dispenser low or amount exceeds hold1
            if (hold1 <= 0 || (int)amount > hold1)
                flag  = 1;
            break;

        case DISPENSER_TYPE_JCM_100:
            // add 5 bill error margin
            hold1 = dispenserControl.memory_dispenser_values.impress_amount1 - dispenserControl.memory_dispenser_values.dispense_grand_total - 
                ((dispenserControl.memory_dispenser_values.reject_bills1 + 5) * 100);

            // don't pay if dispenser low or amount exceeds hold1
            if (hold1 <= 0 || (int)amount > hold1)
                flag  = 1;
            break;

        default:
            flag = 3;
            break;
    }

    switch (flag)
    {
        case 1:
            PrinterData("\n");
            PrinterHeader(0);
            PrinterData("\nBill Dispenser Low.\nCannot Pay.\nPlease Re-fill.\n\n");
            MessageWindow(false, "BILL DISPENSER LOW", "PLEASE RE-FILL");
            break;

        case 2:
            PrinterData("\n");
            PrinterHeader(0);
            PrinterData("\nCash Drawer Low.\n");
            PrinterData("Cannot Pay.\n");
            PrinterData("Please Re-fill.\n\n");
            MessageWindow(false, "CASH DRAWER LOW", "PLEASE RE-FILL");
            break;

        case 3:
            PrinterData("\n");
            PrinterHeader(0);
            PrinterData("\nUnknown Dispenser Type.\n");
            PrinterData("Cannot Pay.\n\n");
            MessageWindow(false, "UNKNOWN DISPENSER TYPE", "UNKNOWN DISPENSER\nCANNOT PAY");
            break;
    }

    printer_busy = false;

    if (flag)
        return true;
    else
        return false;
}

void CControllerApp::SendDispenserDataForDisplay(char *string_data)
{
 DispenserUdpMessageFormat udp_message;

    if (data_display.dispenser_data_text)
    {
        udp_message.command_header = DISPENSER_TEXT_MESSAGE;
        udp_message.data = string_data;
        dispenser_display_data_queue.push(udp_message);
    }
}

void CControllerApp::SendDispenserDataForDisplay(BYTE *data, WORD length, bool tx_data)
{
 WORD iii;
 CString convert_buffer;
 DispenserUdpMessageFormat udp_message;

    if (data_display.dispenser_data_raw && length)
    {
        udp_message.command_header = DISPENSER_RAW_DATA;

        if (tx_data)
            udp_message.data = "RAW TX DATA: ";
        else
            udp_message.data = "RAW RX DATA: ";

        for (iii=0; iii < length; iii++)
        {
            convert_buffer.Format("%2.2X ", data[iii]);
            udp_message.data += convert_buffer;
        }

        dispenser_display_data_queue.push(udp_message);
    }
}

// ADJUSTMENT LOG REPORT
void CControllerApp::CashAdjustmentReport()
{
 char buffer[10];
 DWORD file_index = 0;
 CString prt_buf;
 FileCashAdjustment cash_adjustment;

    PrinterTitle("ADJUSTMENT LOG");
    PrinterId();
    PrinterData("\n TYPE    DATE / TIME            AMOUNT\n -------------------------------------\n");

    while (fileRead(ADJUSTMENT_RECORDS, file_index, &cash_adjustment, sizeof(cash_adjustment)))
    {
        switch (cash_adjustment.type)
        {
            case 1:
                strcpy_s(buffer, sizeof(buffer), "FILL");
                break;

            case 2:
                strcpy_s(buffer, sizeof(buffer), "CREDIT");
                break;

            case 3:
                strcpy_s(buffer, sizeof(buffer), "MGR FILL");
                break;

            case 4:
                strcpy_s(buffer, sizeof(buffer), "MGR TAKE");
                break;

            default:
                strcpy_s(buffer, sizeof(buffer), "UNKNOWN");
                break;
        }

        prt_buf.Format("%-9s%s %8.2f\n", buffer, convertTimeStamp(cash_adjustment.time_stamp), (double)cash_adjustment.amount / 100);
        PrinterData(prt_buf.GetBuffer(0));

        file_index++;
    }

    if (file_index)
    {
        prt_buf.Format("\n TOTAL ADJUSTMENTS   %6d\n", file_index);
        PrinterData(prt_buf.GetBuffer(0));
    }
    else
        PrinterData(" ** NO ADJUSTMENTS  **\n");

    PrinterData(PRINTER_FORM_FEED);
}

DWORD CControllerApp::GetNetwinReport(BOOL print_data, DWORD customer_id, BOOL initialize_totals)
{
    DWORD return_value = 0;


    if (customer_id)
    {
        fetching_netwin_in_progress = TRUE;
    
        if (!memory_settings_ini.stratus_ip_address && !memory_settings_ini.stratus_tcp_port)
            return_value = GetNetwinReportFromXpController(print_data, customer_id);
        else
            return_value = GetNetwinReportFromStratus(print_data, customer_id, initialize_totals);

        fetching_netwin_in_progress = FALSE;
    }

    return return_value;

}


DWORD CControllerApp::GetNetwinReportFromStratus (BOOL print_data, DWORD customer_id, BOOL initialize_totals)
{
 WORD netwin_length;
 DWORD total_netwin_payout = 0, total_netwin_wat_in = 0, total_netwin_wat_out = 0;
 double total_net_cash = 0, total_total_in = 0;
 double net_cash, total_in, total_out, net_win;
 double total_total_out = 0, total_net_win = 0;
 time_t offline_timer, second_display;
 CString prt_buf, log_message;
 NetwinGame netwin_request;
 StratusMessageFormat netwin_data;
 CTimerWindow *pTimerWindow = NULL;
 int i;
 CGameDevice *pGameDevice;
 CGameCommControl *pGameCommControl = pHeadGameCommControl;
 double cash_drop = 0;
 DWORD coin_in;
 DWORD coin_drop;
 DWORD hand_pay;
 #define NETWIN_REPORT_TIMER    20  // timeout in seconds

    if (initialize_totals)
    {
        total_netwin_drop = 0;
        total_cash_drop = 0;
        total_netwin_handle = 0;
    }

    if (print_data)
    {
        printer_busy = true;
        PrinterTitle("NET WIN");
        prt_buf.Format("CUSTOMER ID: %u\n", customer_id);
        PrinterData(prt_buf.GetBuffer(0));
        PrinterId();
        PrinterData("\n");
    }

    if (!stratusSocket.socket_state)
    {
        while (!stratus_dialog_queue.empty())
            stratus_dialog_queue.pop();

        memset(&netwin_request, 0, sizeof(netwin_request));
        RequestNetwinFromStratus(netwin_request, customer_id);

        pTimerWindow = new CTimerWindow;
        pTimerWindow->Create(IDD_TIMER_BAR_DIALOG);
        pTimerWindow->m_timer_window_bar.SetRange(0, NETWIN_REPORT_TIMER);
        offline_timer = time(NULL) + NETWIN_REPORT_TIMER;
        second_display = 0;

        // zero out the game drop holders
        for (i=0; i < 512; i++)
        {
            memset(&bag_and_tag_info[i], 0, sizeof(BagAndTagInfo));
        }

        total_netwin_games = 0;

        while (offline_timer >= time(NULL))
        {
            if (second_display != time(NULL))
            {
				pTimerWindow->m_timer_window_bar.SetPos((int)(time(NULL) + NETWIN_REPORT_TIMER - offline_timer));
                second_display = time(NULL);
            }
            else
                OnIdle(0);

            if (!stratus_dialog_queue.empty())
            {
                netwin_data = stratus_dialog_queue.front();
                stratus_dialog_queue.pop();

                if (netwin_data.task_code != TKT_TASK_TICKET || netwin_data.send_code != TKT_NET_WIN_REPORT)
                {
                    if (pTimerWindow)
                    {
                        pTimerWindow->DestroyWindow();
                        delete pTimerWindow;
                    }

                    logMessage("UserInterface::GetNetwinReport - Invalid netwin message type.");
                    return 0;
                }
                else
                {
                    netwin_length = netwin_data.message_length - STRATUS_HEADER_SIZE;
                    if (netwin_length > sizeof(netwin_request))
                        netwin_length = sizeof(netwin_request);

                    memset(&netwin_request, 0, sizeof(netwin_request));
                    memcpy(&netwin_request, netwin_data.data, netwin_length);
                    endian2(netwin_request.report_sequence);
                    endian2(netwin_request.machine_count);
                    endian4(netwin_request.ucmc_id);
                    endian4(netwin_request.drop);
                    endian4(netwin_request.payout);
                    endian4(netwin_request.handle);
                    endian4(netwin_request.wat_in);
                    endian4(netwin_request.wat_out);

                    pGameDevice = theApp.GetGamePointer((DWORD)netwin_request.ucmc_id);

                    // compare the drop, payout, and handle to stratus netwin values
                    if (pGameDevice)
                    {
                        if (pGameDevice->stored_game_meters.current_game_meters.coin_drop >= pGameDevice->stored_game_meters.last_drop_game_meters.coin_drop)
                            coin_drop = (pGameDevice->stored_game_meters.current_game_meters.coin_drop
                                                   - pGameDevice->stored_game_meters.last_drop_game_meters.coin_drop) * 5;
                        else
                        {
                            coin_drop = (((DWORD)0xFFFFFFFF - pGameDevice->stored_game_meters.last_drop_game_meters.coin_drop)
                                                    + pGameDevice->stored_game_meters.last_drop_game_meters.coin_drop) * 5;
                        }

                        if ((netwin_request.drop != coin_drop) && coin_drop)
                        {
                            log_message.Format("CControllerApp::GetNetwinReport - Drop discrepancy. System(%ld) Controller(%ld)", netwin_request.drop, coin_drop);
                            logMessage(log_message);
                        }

                        if (pGameDevice->stored_game_meters.current_game_meters.hand_pay >= pGameDevice->stored_game_meters.last_drop_game_meters.hand_pay)
                            hand_pay = (pGameDevice->stored_game_meters.current_game_meters.hand_pay
                                                     - pGameDevice->stored_game_meters.last_drop_game_meters.hand_pay) * 5;
                        else
                        {
                            hand_pay = (((DWORD)0xFFFFFFFF - pGameDevice->stored_game_meters.last_drop_game_meters.hand_pay)
                                                    + pGameDevice->stored_game_meters.last_drop_game_meters.hand_pay) * 5;
                        }

                        if ((netwin_request.payout != hand_pay) && hand_pay)
                        {
                            log_message.Format("CControllerApp::GetNetwinReport - Payout discrepancy. System(%ld) Controller(%ld)",
                                               netwin_request.payout, hand_pay);
                            logMessage(log_message);
                        }

                        if (pGameDevice->stored_game_meters.current_game_meters.coin_in >= pGameDevice->stored_game_meters.last_drop_game_meters.coin_in)
                            coin_in = (pGameDevice->stored_game_meters.current_game_meters.coin_in
                                                     - pGameDevice->stored_game_meters.last_drop_game_meters.coin_in) * 5;
                        else
                        {
                            coin_in = (((DWORD)0xFFFFFFFF - pGameDevice->stored_game_meters.last_drop_game_meters.coin_in)
                                                    + pGameDevice->stored_game_meters.last_drop_game_meters.coin_in) * 5;
                        }

                        if ((netwin_request.handle != coin_in) && coin_in)
                        {
                            log_message.Format("CControllerApp::GetNetwinReport - Handle discrepancy. System(%ld) Controller(%ld)",
                                               netwin_request.handle, coin_in);
                            logMessage(log_message);
                        }

                    }

                    bag_and_tag_info[total_netwin_games].game_drop = netwin_request.drop;
                    bag_and_tag_info[total_netwin_games].ucmc_id = netwin_request.ucmc_id;
                    bag_and_tag_info[total_netwin_games].wat_in = netwin_request.wat_in;
                    bag_and_tag_info[total_netwin_games].wat_out = netwin_request.wat_out;
                }

                // exclude all GB Interface devices from the netwin calculations
//                if (pGameDevice && !(pGameDevice->memory_game_config.mux_id && GetSasGamePointer(pGameDevice->memory_game_config.ucmc_id)))
                {
                    if (pGameDevice)
				    {
					        total_netwin_drop += netwin_request.drop;
					        total_netwin_payout += netwin_request.payout;
					        total_netwin_handle += netwin_request.handle;
					        total_netwin_wat_in += netwin_request.wat_in;
					        total_netwin_wat_out += netwin_request.wat_out;
					        total_netwin_games++;
				    }

                    if (pGameDevice && print_data)
                    {
                        prt_buf.Format("  GAME #:    %ld\n", netwin_request.ucmc_id);
                        PrinterData(prt_buf.GetBuffer(0));
					    if (pGameDevice->memory_game_config.mux_id)
					    {
						    prt_buf.Format("  MUX #:    %ld\n", pGameDevice->memory_game_config.mux_id);
						    PrinterData(prt_buf.GetBuffer(0));
					    }
					    else
					    {
						    prt_buf.Format("  SAS #:    %ld\n", pGameDevice->memory_game_config.sas_id);
						    PrinterData(prt_buf.GetBuffer(0));
					    }
                        prt_buf.Format("  HANDLE: %9.2f\n", (double)netwin_request.handle / 100);
                        PrinterData(prt_buf.GetBuffer(0));
                        prt_buf.Format("    DROP: %9.2f\n", (double)netwin_request.drop / 100);
                        PrinterData(prt_buf.GetBuffer(0));

                        prt_buf.Format("  WAT IN: %9.2f\n", (double)netwin_request.wat_in / 100);
                        PrinterData(prt_buf.GetBuffer(0));

                        total_in = ((double)netwin_request.drop + (double)netwin_request.wat_in) / 100;
                        total_total_in += total_in;
                        prt_buf.Format("  TOTAL IN: %9.2f\n", total_in);
                        PrinterData(prt_buf.GetBuffer(0));

    //                    prt_buf.Format("  PAYOUT: %9.2f\n", ((double)netwin_request.payout - (double)netwin_request.wat_out) / 100);
                        prt_buf.Format("  TKT OUT: %9.2f\n", ((double)netwin_request.payout - (double)netwin_request.wat_out) / 100);
                        PrinterData(prt_buf.GetBuffer(0));

                        prt_buf.Format("  WAT OUT: %9.2f\n", (double)netwin_request.wat_out / 100);
                        PrinterData(prt_buf.GetBuffer(0));

                        total_out = ((double)netwin_request.payout) / 100;
                        total_total_out += total_out;
                        prt_buf.Format("  TOTAL OUT: %9.2f\n", total_out);
                        PrinterData(prt_buf.GetBuffer(0));

                        net_win = total_in - total_out;
                        total_net_win += net_win;
                        prt_buf.Format("  NET WIN: %9.2f\n", net_win);
                        PrinterData(prt_buf.GetBuffer(0));

                        net_cash = 0;
                        if (netwin_request.wat_out >= netwin_request.wat_in)
                            net_cash = net_win + (((double)netwin_request.wat_out - (double)netwin_request.wat_in)) / 100;
                        else
                            net_cash = net_win - (((double)netwin_request.wat_in - (double)netwin_request.wat_out)) / 100;
                        total_net_cash += net_cash;

    //                    prt_buf.Format("  NET CASH: %9.2f\n\n", net_cash);
                        prt_buf.Format("  CASH WIN: %9.2f\n\n", net_cash);
                        PrinterData(prt_buf.GetBuffer(0));
                    }
                }








                if (netwin_request.report_sequence < netwin_request.machine_count)
                {
                    prt_buf.Format("NETWIN GAME %d of %d", netwin_request.report_sequence, netwin_request.machine_count);
                    pTimerWindow->SetDlgItemText(IDC_TIMER_WINDOW_TITLE_STATIC, prt_buf);

                    offline_timer = time(NULL) + NETWIN_REPORT_TIMER;
                    second_display = 0;
                    RequestNetwinFromStratus(netwin_request, customer_id);
                }
                else
                {
                    if (print_data)
                    {
                        prt_buf.Format(" TOTAL GAMES:  %d\n", netwin_request.machine_count);
                        PrinterData(prt_buf.GetBuffer(0));
                        prt_buf.Format(" TOTAL HANDLE: %9.2f\n", (double)total_netwin_handle / 100);
                        PrinterData(prt_buf.GetBuffer(0));
                        prt_buf.Format(" TOTAL DROP: %9.2f\n", (double)total_netwin_drop / 100);
                        PrinterData(prt_buf.GetBuffer(0));
                        prt_buf.Format(" TOTAL WAT IN: %9.2f\n", (double)total_netwin_wat_in / 100);
                        PrinterData(prt_buf.GetBuffer(0));
                        prt_buf.Format(" GRAND TOTAL IN: %9.2f\n", (double)total_total_in);
                        PrinterData(prt_buf.GetBuffer(0));
//                        prt_buf.Format(" TOTAL PAYOUT: %9.2f\n", ((double)total_netwin_payout - (double)total_netwin_wat_out) / 100);
                        prt_buf.Format(" TOTAL TKT OUT: %9.2f\n", ((double)total_netwin_payout - (double)total_netwin_wat_out) / 100);
                        PrinterData(prt_buf.GetBuffer(0));
                        prt_buf.Format(" TOTAL WAT OUT: %9.2f\n", (double)total_netwin_wat_out / 100);
                        PrinterData(prt_buf.GetBuffer(0));
                        prt_buf.Format(" GRAND TOTAL OUT: %9.2f\n", total_total_out);
                        PrinterData(prt_buf.GetBuffer(0));
                        prt_buf.Format(" TOTAL NET WIN: %9.2f\n", total_net_win);
                        PrinterData(prt_buf.GetBuffer(0));
//                        prt_buf.Format(" TOTAL NET CASH: %9.2f\n\n", total_net_cash);
                        prt_buf.Format(" TOTAL CASH WIN: %9.2f\n\n", total_net_cash);
                        PrinterData(prt_buf.GetBuffer(0));


                        printer_busy = false;
                    }

                    if (pTimerWindow)
                    {
                        pTimerWindow->DestroyWindow();
                        delete pTimerWindow;
                    }

                    return total_netwin_drop;
                }
            }
        }
    }

    if (pTimerWindow)
    {
        pTimerWindow->DestroyWindow();
        delete pTimerWindow;
    }

    MessageWindow(false, "NETWIN REPORT", "REQUEST TIMEOUT");

    if (print_data)
    {
        PrinterData("** REQUEST TIMEOUT **\n\n");
        printer_busy = false;
    }

 return 0;

}


DWORD CControllerApp::GetNetwinReportFromXpController(BOOL print_data, DWORD customer_id)
{
 DWORD total_netwin_payout = 0, total_netwin_wat_in = 0, total_netwin_wat_out = 0;
 CString prt_buf;
 NetwinGame netwin_request;
 CTimerWindow *pTimerWindow = NULL;
 int i;
 CGameDevice *pGameDevice;
 CGameCommControl *pGameCommControl = pHeadGameCommControl;
 double cash_drop = 0;
 
 #define NETWIN_REPORT_TIMER    20  // timeout in seconds

    total_netwin_drop = 0;
    total_cash_drop = 0;
    total_netwin_handle = 0;

    if (print_data)
    {
        printer_busy = true;
        PrinterTitle("NET WIN");
        prt_buf.Format("CUSTOMER ID: %u\n", customer_id);
        PrinterData(prt_buf.GetBuffer(0));
        PrinterId();
        PrinterData("\n");
    }


//    if ((!memory_settings_ini.stratus_ip_address && !memory_settings_ini.stratus_tcp_port) || stratusSocket.socket_state)
    {
        if (!memory_settings_ini.stratus_ip_address && !memory_settings_ini.stratus_tcp_port)
            PrinterData(" SYSTEM IS OFFLINE\n");

        // zero out the game drop holders
        for (i=0; i < 512; i++)
        {
            memset(&bag_and_tag_info[i], 0, sizeof(BagAndTagInfo));
        }

        total_netwin_games = 0;

        while (pGameCommControl)
        {
            pGameDevice = pGameCommControl->pHeadGameDevice;

            while (pGameDevice)
            {
                // exclude all GB Interface devices from the netwin calculations
                if (!(pGameDevice->memory_game_config.mux_id && GetSasGamePointer(pGameDevice->memory_game_config.ucmc_id)))
                {
                    memset(&netwin_request, 0, sizeof(netwin_request));

                    netwin_request.ucmc_id = pGameDevice->memory_game_config.ucmc_id;

                    if (pGameDevice->stored_game_meters.current_game_meters.coin_drop >= pGameDevice->stored_game_meters.last_drop_game_meters.coin_drop)
                        netwin_request.drop = (pGameDevice->stored_game_meters.current_game_meters.coin_drop
                                               - pGameDevice->stored_game_meters.last_drop_game_meters.coin_drop) * pGameDevice->memory_game_config.denom_type;
                    else
                    {
                        netwin_request.drop = (((DWORD)99999999 - pGameDevice->stored_game_meters.last_drop_game_meters.coin_drop)
                                                + pGameDevice->stored_game_meters.current_game_meters.coin_drop) * pGameDevice->memory_game_config.denom_type;
                    }

                    if (pGameDevice->stored_game_meters.current_game_meters.hand_pay >= pGameDevice->stored_game_meters.last_drop_game_meters.hand_pay)
                        netwin_request.payout = (pGameDevice->stored_game_meters.current_game_meters.hand_pay
                                                 - pGameDevice->stored_game_meters.last_drop_game_meters.hand_pay) * pGameDevice->memory_game_config.denom_type;
                    else
                    {
                        netwin_request.payout = (((DWORD)99999999 - pGameDevice->stored_game_meters.last_drop_game_meters.hand_pay)
                                                + pGameDevice->stored_game_meters.current_game_meters.hand_pay) * pGameDevice->memory_game_config.denom_type;
                    }

                    if (pGameDevice->stored_game_meters.current_game_meters.coin_in >= pGameDevice->stored_game_meters.last_drop_game_meters.coin_in)
                        netwin_request.handle = (pGameDevice->stored_game_meters.current_game_meters.coin_in
                                                 - pGameDevice->stored_game_meters.last_drop_game_meters.coin_in) * pGameDevice->memory_game_config.denom_type;
                    else
                    {
                        netwin_request.handle = (((DWORD)99999999 - pGameDevice->stored_game_meters.last_drop_game_meters.coin_in)
                                                + pGameDevice->stored_game_meters.current_game_meters.coin_in) * pGameDevice->memory_game_config.denom_type;
                    }

                    // adjust GB SAS Interface netwin values until we can bypass the Stratus netwin kludge.
                    if (pGameDevice->memory_game_config.version == GB_SAS_INTERFACE_VERSION)
                    {
                        if (pGameDevice->memory_game_config.denom_type)
                        {
                            netwin_request.drop = (netwin_request.drop * NICKEL) / pGameDevice->memory_game_config.denom_type;
                            netwin_request.payout = (netwin_request.payout * NICKEL) / pGameDevice->memory_game_config.denom_type;
                            netwin_request.handle = (netwin_request.handle * NICKEL) / pGameDevice->memory_game_config.denom_type;
                        }
                        else
                        {
                            netwin_request.drop = netwin_request.drop * NICKEL;
                            netwin_request.payout = netwin_request.payout * NICKEL;
                            netwin_request.handle = netwin_request.handle * NICKEL;
                        }
                    }

                    bag_and_tag_info[total_netwin_games].game_drop = netwin_request.drop;
                    bag_and_tag_info[total_netwin_games].ucmc_id = netwin_request.ucmc_id;

                    total_netwin_drop += netwin_request.drop;
                    total_netwin_payout += netwin_request.payout;
                    total_netwin_handle += netwin_request.handle;
                    total_netwin_games++;

                    if (print_data)
                    {
                        prt_buf.Format("  GAME #:    %ld\n", pGameDevice->memory_game_config.ucmc_id);
                        PrinterData(prt_buf.GetBuffer(0));

                        if (pGameDevice->memory_game_config.mux_id)
                        {
                            prt_buf.Format("  MUX #:    %ld\n", pGameDevice->memory_game_config.mux_id);
                            PrinterData(prt_buf.GetBuffer(0));
                        }
                        else
                        {
                            prt_buf.Format("  SAS #:    %ld\n", pGameDevice->memory_game_config.sas_id);
                            PrinterData(prt_buf.GetBuffer(0));
                        }

                        prt_buf.Format("  HANDLE: %9.2f\n", (double)netwin_request.handle / 100);
                        PrinterData(prt_buf.GetBuffer(0));
                        prt_buf.Format("    DROP: %9.2f\n", (double)netwin_request.drop / 100);
                        PrinterData(prt_buf.GetBuffer(0));
                        // cash drop
                        if (netwin_request.drop >= netwin_request.wat_in)
                            cash_drop = (double)netwin_request.drop - (double)netwin_request.wat_in;
                        else
                            cash_drop = 0;
                        prt_buf.Format(" CASH DROP: %9.2f\n", cash_drop / 100);
                        PrinterData(prt_buf.GetBuffer(0));
                        total_cash_drop += cash_drop;

                        prt_buf.Format("  PAYOUT: %9.2f\n", (double)netwin_request.payout / 100);
                        PrinterData(prt_buf.GetBuffer(0));
                        prt_buf.Format(" NET WIN: %9.2f\n\n", ((double)netwin_request.drop - (double)netwin_request.payout) / 100);
                        PrinterData(prt_buf.GetBuffer(0));
                    }
                }

                pGameDevice = pGameDevice->pNextGame;
            }

            pGameCommControl = pGameCommControl->pNextGameCommControl;
        }

        if (print_data)
        {
            prt_buf.Format("  TOTAL GAMES:  %d\n", total_netwin_games);
            PrinterData(prt_buf.GetBuffer(0));
            prt_buf.Format("  HANDLE: %9.2f\n", (double)total_netwin_handle / 100);
            PrinterData(prt_buf.GetBuffer(0));
            prt_buf.Format("    DROP: %9.2f\n", (double)total_netwin_drop / 100);
            PrinterData(prt_buf.GetBuffer(0));
            prt_buf.Format(" TOTAL CASH DROP: %9.2f\n", (double)total_cash_drop / 100);
            PrinterData(prt_buf.GetBuffer(0));
            prt_buf.Format(" PAYOUTS: %9.2f\n", (double)total_netwin_payout / 100);
            PrinterData(prt_buf.GetBuffer(0));
            prt_buf.Format(" NET WIN: %9.2f\n\n", ((double)total_netwin_drop - (double)total_netwin_payout) / 100);
            PrinterData(prt_buf.GetBuffer(0));
            printer_busy = false;
        }

        return total_netwin_drop;

    }

    return 0;

}


DWORD CControllerApp::getTotalNetwinHandle (void)
{
    return total_netwin_handle;
}

DWORD CControllerApp::getTotalNetwinDrop (void)
{
    return total_netwin_drop;
}

// Print W2G file data
void CControllerApp::PrintW2gTickets(DWORD customer_id)
{
 bool record_found;
// DWORD total_records, total_tickets, iii, jjj, total_redeem_amount, total_marker_amount, total_pay_to_credit_amount;
 DWORD total_records, iii, jjj;
 CString prt_buf, w2g_records;
 FilePaidTicket *w2g_ticket_array;
 CGameDevice *pGameDevice;
 TicketsByUcmcId *tickets_by_ucmc_id;

    PrinterData(PRINTER_FORM_FEED);
    PrinterTitle("W2G JACKPOT LOG");

    if (customer_id)
    {
        prt_buf.Format("CUSTOMER ID: %u\n", customer_id);
        PrinterData(prt_buf.GetBuffer(0));
    }

    PrinterId();

    w2g_records = W2G_RECORDS;

    total_records = fileRecordTotal(w2g_records.GetBuffer(0), sizeof(FilePaidTicket));

    if (!total_records)
        PrinterData("\n NO JACKPOT TICKETS\n");
    else
    {
        w2g_ticket_array = new FilePaidTicket[total_records];
        tickets_by_ucmc_id = new TicketsByUcmcId[total_records];

        memset(tickets_by_ucmc_id, 0, sizeof(TicketsByUcmcId) * total_records);

        if (!fileRead(w2g_records.GetBuffer(0), 0, w2g_ticket_array, sizeof(FilePaidTicket) * total_records))
            memset(w2g_ticket_array, 0, sizeof(FilePaidTicket) * total_records);
        else
        {
            for (iii=0; iii < total_records; iii++)
            {
                for (jjj=0; jjj < total_records; jjj++)
                {
                    if (w2g_ticket_array[iii].memory.memory_game_lock.ucmc_id == tickets_by_ucmc_id[jjj].ucmc_id)
                    {
                        tickets_by_ucmc_id[jjj].number_of_tickets++;

                        if (w2g_ticket_array[iii].memory.memory_game_lock.jackpot_to_credit_flag)
                            tickets_by_ucmc_id[jjj].pay_to_credit_amount += w2g_ticket_array[iii].memory.memory_game_lock.ticket_amount;
                        else
                        {
                            if (w2g_ticket_array[iii].memory.memory_game_lock.ticket_amount >
                                w2g_ticket_array[iii].memory.memory_game_lock.marker_balance)
                            {
                                tickets_by_ucmc_id[jjj].redeem_amount += w2g_ticket_array[iii].memory.memory_game_lock.ticket_amount -
                                    w2g_ticket_array[iii].memory.memory_game_lock.marker_balance;
                                tickets_by_ucmc_id[jjj].marker_amount += w2g_ticket_array[iii].memory.memory_game_lock.marker_balance;
                            }
                            else
                                tickets_by_ucmc_id[jjj].marker_amount += w2g_ticket_array[iii].memory.memory_game_lock.ticket_amount;
                        }
                        break;
                    }
                    else
                    {
                        if (!tickets_by_ucmc_id[jjj].ucmc_id)
                        {
                            tickets_by_ucmc_id[jjj].ucmc_id = w2g_ticket_array[iii].memory.memory_game_lock.ucmc_id;
                            tickets_by_ucmc_id[jjj].number_of_tickets = 1;

                            if (w2g_ticket_array[iii].memory.memory_game_lock.jackpot_to_credit_flag)
                                tickets_by_ucmc_id[jjj].pay_to_credit_amount = w2g_ticket_array[iii].memory.memory_game_lock.ticket_amount;
                            else
                            {
                                if (w2g_ticket_array[iii].memory.memory_game_lock.ticket_amount >
                                    w2g_ticket_array[iii].memory.memory_game_lock.marker_balance)
                                {
                                    tickets_by_ucmc_id[jjj].redeem_amount = w2g_ticket_array[iii].memory.memory_game_lock.ticket_amount -
                                        w2g_ticket_array[iii].memory.memory_game_lock.marker_balance;
                                    tickets_by_ucmc_id[jjj].marker_amount = w2g_ticket_array[iii].memory.memory_game_lock.marker_balance;
                                }
                                else
                                    tickets_by_ucmc_id[jjj].marker_amount = w2g_ticket_array[iii].memory.memory_game_lock.ticket_amount;
                            }
                            break;
                        }
                    }
                }
            }
        }

        // check list for drop using collection customer id, delete record if found
        if (customer_id)
        {
            for (iii=0; iii < total_records; iii++)
            {
                if (tickets_by_ucmc_id[iii].ucmc_id)
                {
                    pGameDevice = GetGamePointer(tickets_by_ucmc_id[iii].ucmc_id);

                    if (pGameDevice && pGameDevice->memory_game_config.customer_id != customer_id)
                        memset(&tickets_by_ucmc_id[iii], 0, sizeof(TicketsByUcmcId));
                }
            }

            for (iii=0; iii < total_records; iii++)
            {
                record_found = false;

                for (jjj=0; jjj < total_records; jjj++)
                {
                    if (w2g_ticket_array[iii].memory.memory_game_lock.ucmc_id == tickets_by_ucmc_id[jjj].ucmc_id)
                    {
                        record_found = true;
                        break;
                    }
                }

                if (!record_found)
                    memset(&w2g_ticket_array[iii], 0, sizeof(FilePaidTicket));
                else
                {
                    jjj = fileGetRecord(w2g_records.GetBuffer(0), &w2g_ticket_array[iii], sizeof(FilePaidTicket),
                        w2g_ticket_array[iii].memory.memory_game_lock.ticket_id);

                    if (jjj >= 0)
                        fileDeleteRecord(w2g_records.GetBuffer(0), sizeof(FilePaidTicket), jjj);
                }
            }
        }

        // print each individual ticket
        for (iii=0; iii < total_records; iii++)
        {
            if (w2g_ticket_array[iii].memory.memory_game_lock.ticket_id)
            {
                prt_buf.Format("\n %s\n", convertTimeStamp(w2g_ticket_array[iii].memory.timestamp));
                PrinterData(prt_buf.GetBuffer(0));
                prt_buf.Format(" GAME #: %u   CASHIER: %u\n",
                    w2g_ticket_array[iii].memory.memory_game_lock.ucmc_id, w2g_ticket_array[iii].memory.cashier_id);
                PrinterData(prt_buf.GetBuffer(0));
                prt_buf.Format(" TICKET: %u   SSN: %u\n",
                    w2g_ticket_array[iii].memory.memory_game_lock.ticket_id, w2g_ticket_array[iii].memory.memory_game_lock.ss_number);
                PrinterData(prt_buf.GetBuffer(0));

                if (w2g_ticket_array[iii].memory.memory_game_lock.jackpot_to_credit_flag)
                    prt_buf.Format(" PAY-TO-CREDIT: $%7.2f\n", (double)w2g_ticket_array[iii].memory.memory_game_lock.ticket_amount / 100);
                else
                {
                    if (w2g_ticket_array[iii].memory.memory_game_lock.marker_balance)
                        prt_buf.Format(" TOTAL TICKET AMOUNT: $%7.2f\n MARKER TICKET AMOUNT: $%7.2f\n",
                            (double)w2g_ticket_array[iii].memory.memory_game_lock.ticket_amount / 100,
                            (double)w2g_ticket_array[iii].memory.memory_game_lock.marker_balance / 100);
                    else
                        prt_buf.Format(" TICKET AMOUNT: $%7.2f\n", (double)w2g_ticket_array[iii].memory.memory_game_lock.ticket_amount / 100);
                }
                PrinterData(prt_buf.GetBuffer(0));
            }
        }

        delete[] w2g_ticket_array;
        delete[] tickets_by_ucmc_id;
    }

    PrinterData("\n END OF REPORT\n\n");
}

// Print ticket drop data
void CControllerApp::DropTicketsPrint(DWORD customer_id)
{
 DWORD total_tickets = 0, file_index = 0, total_amount = 0;
 CString prt_buf, previous_drop_time;
 FileDropTicket drop_ticket;
 CGameDevice *pGameDevice;
 time_t time_stamp;
 CString drop_tickets_string;
 CString drop_tickets_redundant_string;

    PrinterTitle("DROP TICKETS LOG");
    DropReportData("\n\nDROP TICKETS LOG");
    PrinterId();

    if (customer_id)
    {

        previous_drop_time = PREVIOUS_DROP_TIME;

        theApp.PrinterData("\nPrevious Drop Time\n");
        DropReportData("\nPrevious Drop Time\n");

        if (GetFileAttributes(previous_drop_time.GetBuffer(0)) == 0xFFFFFFFF)
        {
            theApp.PrinterData("First Drop\n");
            DropReportData("First Drop\n");
        }
        else
        {
            fileRead(previous_drop_time.GetBuffer(0), 0, &time_stamp, sizeof(time_stamp));
            prt_buf.Format("%s\n", convertTimeStamp(time_stamp));
            theApp.PrinterData(prt_buf.GetBuffer(0));
            DropReportData(prt_buf.GetBuffer(0));
        }

        time_stamp = time(NULL);
        fileWrite(previous_drop_time.GetBuffer(0), 0, &time_stamp, sizeof(time_stamp));
    }

    PrinterData("\n GAME  TICKET ID   PAID\n ----------------------\n");
    DropReportData("\n GAME  TICKET ID   PAID\n ----------------------\n");

    drop_tickets_string = DROP_TICKETS;
    drop_tickets_redundant_string = DROP_TICKETS_REDUNDANT;

//    while (fileRead(DROP_TICKETS, file_index, &drop_ticket, sizeof(drop_ticket)))
    while (fileRead(drop_tickets_string.GetBuffer(0), file_index, &drop_ticket, sizeof(drop_ticket)))
    {
        if (!drop_ticket.dispenser_ucmc_id)
        {
            if (drop_ticket.ticket_filled)
            {
                if (drop_ticket.dispenser_malfunction)
//                    prt_buf.Format("DM %ld %ld %9.2f *FILLED*\n", drop_ticket.ucmc_id, drop_ticket.ticket_id, (double)drop_ticket.amount / 100);
                    prt_buf.Format("DM %ld %u %9.2f *FILLED*\n", drop_ticket.ucmc_id, drop_ticket.ticket_id, (double)drop_ticket.amount / 100);
                else
                    prt_buf.Format(" %ld %u %9.2f *FILLED*\n", drop_ticket.ucmc_id, drop_ticket.ticket_id, (double)drop_ticket.amount / 100);
            }
            else
            {
                if (drop_ticket.dispenser_malfunction)
                    prt_buf.Format("DM %ld %u %9.2f\n", drop_ticket.ucmc_id, drop_ticket.ticket_id, (double)drop_ticket.amount / 100);
                else
                    prt_buf.Format(" %ld %u %9.2f\n", drop_ticket.ucmc_id, drop_ticket.ticket_id, (double)drop_ticket.amount / 100);
            }
        }
        else
        {
            if (drop_ticket.dispenser_malfunction)
                prt_buf.Format("DM %ld %u %9.2f *SLAVE %d*\n", drop_ticket.ucmc_id, drop_ticket.ticket_id,
                    (double)drop_ticket.amount / 100, drop_ticket.dispenser_ucmc_id);
            else
                prt_buf.Format(" %ld %u %9.2f *SLAVE %d*\n", drop_ticket.ucmc_id, drop_ticket.ticket_id,
                    (double)drop_ticket.amount / 100, drop_ticket.dispenser_ucmc_id);
        }
        if (customer_id)
        {
            pGameDevice = GetGamePointer(drop_ticket.ucmc_id);

            if (pGameDevice && pGameDevice->memory_game_config.customer_id != customer_id)
                memset(&drop_ticket, 0, sizeof(drop_ticket));


            if (drop_ticket.ucmc_id)
            {
                PrinterData(prt_buf.GetBuffer(0));
                DropReportData(prt_buf.GetBuffer(0));
                total_amount += drop_ticket.amount;
                total_tickets++;

                if (!fileDeleteRecord(drop_tickets_string.GetBuffer(0), sizeof(drop_ticket), file_index))
                    file_index++;
                else
                    fileDeleteRecord(drop_tickets_redundant_string.GetBuffer(0), sizeof(drop_ticket), file_index);
            }
            else
                file_index++;
        }
        else
        {
            PrinterData(prt_buf.GetBuffer(0));
            DropReportData(prt_buf.GetBuffer(0));
            total_amount += drop_ticket.amount;
            total_tickets++;
            file_index++;
        }
    }

    prt_buf.Format("\n TOTAL TICKETS: %d\n", total_tickets);
    PrinterData(prt_buf.GetBuffer(0));
    DropReportData(prt_buf.GetBuffer(0));
    prt_buf.Format(" TOTAL AMOUNT: $%9.2f\n\n", (double)total_amount / 100);
    PrinterData(prt_buf.GetBuffer(0));
    DropReportData(prt_buf.GetBuffer(0));
}

// get ticket drop data
DWORD CControllerApp::getTotalDropTicketAmount(DWORD customer_id)
{
 DWORD file_index = 0, total_amount = 0;
 FileDropTicket drop_ticket;
 CString drop_tickets_string;

    drop_tickets_string = DROP_TICKETS;
  
    while (fileRead(drop_tickets_string.GetBuffer(0), file_index, &drop_ticket, sizeof(drop_ticket)))
    {
        if (drop_ticket.customer_id == customer_id)
            total_amount += drop_ticket.amount;
        file_index++;
    }

    return total_amount;
}

// get ticket drop data
DWORD CControllerApp::getTotalDropTicketAmount(DWORD customer_id, DWORD ucmc_id)
{
 DWORD file_index = 0, total_amount = 0;
 FileDropTicket drop_ticket;
 CString drop_tickets_string;

    drop_tickets_string = DROP_TICKETS;
  
    while (fileRead(drop_tickets_string.GetBuffer(0), file_index, &drop_ticket, sizeof(drop_ticket)))
    {
        if ((drop_ticket.ucmc_id == ucmc_id) && (drop_ticket.customer_id == customer_id))
            total_amount += drop_ticket.amount;
        file_index++;
    }

    return total_amount;
}

void CControllerApp::PrintingCashierShift(FileCashierShift *shift_data, WORD shift_number, bool restrict_data, bool end_shift)
{
 DWORD iii, ticket_id, ticket_amount, total_ticket_amount;
 CString prt_buf, filename;
 DrinkCompTotal drink_comps;
 FILE *file_handle;
 StratusMessageFormat stratus_msg;
// FillValues fill_values;
 ShiftEndStratusData shift_end_stratus_data;
 
    memset(&shift_end_stratus_data, 0, sizeof(shift_end_stratus_data));

    prt_buf = "      \n\n";
    PrinterData(prt_buf.GetBuffer(0));

    PrinterData(PRINTER_BIG_PRINT);
    prt_buf.Format(" SHIFT %u\n", shift_number);
    PrinterData(prt_buf.GetBuffer(0));
    PrinterData(PRINTER_NORMAL_PRINT);
    prt_buf.Format(" CASHIER ID: %d\n", shift_data->cashier_id);
    PrinterData(prt_buf.GetBuffer(0));
    shift_end_stratus_data.cashier_id = shift_data->cashier_id;
    prt_buf.Format(" START: %s\n", convertTimeStamp(shift_data->start_time));
    PrinterData(prt_buf.GetBuffer(0));
    shift_end_stratus_data.start_time = shift_data->start_time;

    if (!shift_data->end_time)
        PrinterData(" END: current\n\n");
    else
    {
        prt_buf.Format("   END: %s\n\n", convertTimeStamp(shift_data->end_time));
        PrinterData(prt_buf.GetBuffer(0));
    }
    shift_end_stratus_data.end_time = shift_data->end_time;

    if (memory_dispenser_config.dispenser_type == DISPENSER_TYPE_CASH_DRAWER)
    {
        prt_buf.Format("   Beginning Balance: %9.2f\n", (double)shift_data->beginning_balance / 100);
        PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format("      Ending Balance: %9.2f\n", (double)shift_data->ending_balance / 100);
        PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format(" Calc Ending Balance: %9.2f\n", ((double)shift_data->beginning_balance -
            (double)shift_data->redeem_amount + (double)shift_data->active_shift_fills + (double)shift_data->mgr_adjust_plus -
            (double)shift_data->mgr_adjust_minus) / 100);
        PrinterData(prt_buf.GetBuffer(0));

        prt_buf.Format("     Pre-shift Fills: %9.2f\n", (double)shift_data->pre_shift_fills / 100);
        PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format("  Active Shift Fills: %9.2f\n", (double)shift_data->active_shift_fills / 100);
        PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format("  Mgr Adjustments(+): %9.2f\n", (double)shift_data->mgr_adjust_plus / 100);
        PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format("  Mgr Adjustments(-): %9.2f\n", (double)shift_data->mgr_adjust_minus / 100);
        PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format("  Number of No Sales: %9u\n", shift_data->number_nosale);
        PrinterData(prt_buf.GetBuffer(0));
    }
    else
    {
//        if (!shift_data->end_time)
        if (end_shift || !shift_data->end_time)
        {
            prt_buf.Format("     Master Breakage: %9.2f\n", (double)(dispenserControl.memory_dispenser_values.dispense_grand_total -
				dispenserControl.memory_dispenser_values.tickets_amount) / 100);
            shift_end_stratus_data.breakage = (dispenserControl.memory_dispenser_values.dispense_grand_total -
				dispenserControl.memory_dispenser_values.tickets_amount) / 100;
        }
        else
        {
            prt_buf.Format("*    Master Breakage: %9.2f\n", (double)(shift_data->ending_balance / 100));
            shift_end_stratus_data.breakage = shift_data->ending_balance / 100;
        }

        PrinterData(prt_buf.GetBuffer(0));
    }

    if (!restrict_data)
    {
        if (!shift_data->end_time)
        {
            PrinterData("CASHIER SHIFT TICKETS:\n");
            filename.Format("%s", SHIFT_TICKETS_1);
            fopen_s(&file_handle, filename.GetBuffer(0), "rb");

            if (file_handle && !fseek(file_handle, 0, SEEK_SET))

            {
                PrinterData(" TICKET ID     AMOUNT\n");
                PrinterData(" --------------------\n");
                total_ticket_amount = 0;
                iii = 0;

                while (fread(&ticket_id, 4, 1, file_handle) && fread(&ticket_amount, 4, 1, file_handle))
                {
                    iii++;
                    total_ticket_amount += ticket_amount;
                    prt_buf.Format(" %u         %9.2f\n", ticket_id, (double)ticket_amount / 100);
                    PrinterData(prt_buf.GetBuffer(0));
                }

                fclose(file_handle);

                prt_buf.Format("TOTAL TICKETS: %u\n", iii);
                PrinterData(prt_buf.GetBuffer(0));
                prt_buf.Format("TOTAL AMOUNT: %9.2f\n", (double)total_ticket_amount / 100);
                PrinterData(prt_buf.GetBuffer(0));
                prt_buf = "      \n\n";
                PrinterData(prt_buf.GetBuffer(0));
            }
            else
                PrinterData("NO TICKETS PAID\n");
        }

        PrinterData("\n");

//        if (!memory_settings_ini.masters_ip_address && GetFileAttributes(MASTER_CONFIG) == 0xFFFFFFFF)
        {
            if (!shift_data->end_time || shift_data->game_drop == -1)
                prt_buf.Format("     Shift Game Drop:       N/A\n");
            else
            {
                prt_buf.Format("     Shift Game Drop: %9.2f\n", (double)shift_data->game_drop / 100);
                shift_end_stratus_data.game_drop = shift_data->game_drop;
            }

            PrinterData(prt_buf.GetBuffer(0));
        }

        if (!shift_data->end_time || shift_data->game_handle == -1)
            prt_buf.Format("     Shift Game Handle:       N/A\n");
        else
        {
            prt_buf.Format("     Shift Game Handle: %9.2f\n", (double)shift_data->game_handle / 100);
            shift_end_stratus_data.game_handle = shift_data->game_handle;
        }
        PrinterData(prt_buf.GetBuffer(0));

        prt_buf.Format("     Paid Out:        %9.2f\n", (double)shift_data->redeem_amount / 100);
        PrinterData(prt_buf.GetBuffer(0));
        shift_end_stratus_data.paid_out = shift_data->redeem_amount;
        prt_buf.Format("   Number of Tickets: %9u\n", shift_data->number_tickets);
        PrinterData(prt_buf.GetBuffer(0));
        shift_end_stratus_data.number_of_tickets = shift_data->number_tickets;

        if (!fileRead(DRINK_COMPS_SHIFT, 0, &drink_comps, sizeof(drink_comps)))
            memset(&drink_comps, 0, sizeof(drink_comps));

		if (!memory_settings_ini.masters_ip_address && (theApp.memory_settings_ini.stratus_ip_address && theApp.memory_settings_ini.stratus_tcp_port))
		{
			prt_buf.Format("         Comp Amount: %9.2f\n", (double)shift_data->drink_comps.total_amount);
			PrinterData(prt_buf.GetBuffer(0));
			prt_buf.Format("     Number of Comps: %9u\n", shift_data->drink_comps.number);
			PrinterData(prt_buf.GetBuffer(0));
		}

        if (end_shift)
        {
			//change endian for Stratus
			endian4(shift_end_stratus_data.breakage);
			endian4(shift_end_stratus_data.cashier_id);
			endian4(shift_end_stratus_data.end_time);
			endian4(shift_end_stratus_data.game_drop);
			endian4(shift_end_stratus_data.game_handle);
			endian4(shift_end_stratus_data.number_of_tickets);
			endian4(shift_end_stratus_data.paid_out);
			endian4(shift_end_stratus_data.start_time);

            memset(&stratus_msg, 0, sizeof(stratus_msg));
            stratus_msg.send_code = TKT_SHIFT_END_INFO;
            stratus_msg.task_code = TKT_TASK_TICKET;
            stratus_msg.message_length = sizeof(shift_end_stratus_data) + STRATUS_HEADER_SIZE;
            stratus_msg.property_id = theApp.memory_location_config.property_id;
            stratus_msg.club_code = theApp.memory_location_config.club_code;
            stratus_msg.serial_port = 1;
            stratus_msg.customer_id = theApp.memory_location_config.customer_id;
            stratus_msg.data_to_event_log = true;
            memcpy(stratus_msg.data, &shift_end_stratus_data, sizeof(shift_end_stratus_data));
            theApp.QueueStratusTxMessage(&stratus_msg, FALSE);
        }
    }

    PrinterData("\n");
}

//void CControllerApp::PrintCashierShift(DWORD cashier_id, bool all_shifts, bool restrict_data)
void CControllerApp::PrintCashierShift(DWORD cashier_id, BYTE number_of_shifts, bool restrict_data, bool end_shift)
{
    CString filename, prt_buf;
    FileCashierShift cashier_shift;
    BYTE i;
    FILE *file_handle;
    DWORD iii, ticket_id, ticket_amount, total_ticket_amount;


    printer_busy = true;
    PrinterHeader(0);

    PrinterTitle("SHIFT REPORT");
    prt_buf.Format("CUSTOMER ID: %u\n", theApp.memory_location_config.customer_id);
    PrinterData(prt_buf.GetBuffer(0));
    PrinterId();
    PrinterData("\n");


    if ((number_of_shifts >= 1) && (number_of_shifts <= MAX_CASHIER_SHIFTS))
    {

        for (i=0; i < number_of_shifts; i++)
        {
            filename.Format("%s", shift_cashier_strings[i]);
            if (fileRead(filename.GetBuffer(0), 0, &cashier_shift, sizeof(cashier_shift)))
            {
                PrintingCashierShift(&cashier_shift, (i + 1), restrict_data, end_shift);

    	        if (memory_settings_ini.stratus_ip_address && memory_settings_ini.stratus_tcp_port && cashier_shift.print_offline_disclaimer)
                {
                    PrinterData("\n\n");
                    PrinterData(" SHIFT STARTED/ENDED OFFLINE\n");
                    PrinterData(" SHIFT DATA POTENTIALLY\n");
                    PrinterData(" INACCURATE!\n");
                    PrinterData("\n\n");
                }

                if (!restrict_data && memory_settings_ini.print_full_shift_report)
                {
                    PrinterData("CASHIER SHIFT TICKETS:\n");
                    filename.Format("%s", shift_ticket_strings[i]);
                    fopen_s(&file_handle, filename.GetBuffer(0), "rb");

                    if (file_handle && !fseek(file_handle, 0, SEEK_SET))

                    {
                        PrinterData(" TICKET ID     AMOUNT\n");
                        PrinterData(" --------------------\n");
                        total_ticket_amount = 0;
                        iii = 0;

                        while (fread(&ticket_id, 4, 1, file_handle) && fread(&ticket_amount, 4, 1, file_handle))
                        {
                            iii++;
                            total_ticket_amount += ticket_amount;
                            prt_buf.Format(" %u         %9.2f\n", ticket_id, (double)ticket_amount / 100);
                            PrinterData(prt_buf.GetBuffer(0));
                        }

                        fclose(file_handle);

                        prt_buf.Format("TOTAL TICKETS: %u\n", iii);
                        PrinterData(prt_buf.GetBuffer(0));
                        prt_buf.Format("TOTAL AMOUNT: %9.2f\n", (double)total_ticket_amount / 100);
                        PrinterData(prt_buf.GetBuffer(0));
                    }
                    else
                        PrinterData("NO TICKETS PAID\n");
                }    
            }
        }
    }

    PrinterData("END OF SHIFT REPORT\n\n");
    printer_busy = false;
}

void CControllerApp::SortGameRedeemRecords(void)
{
    DWORD file_index = 0;
    int i, redeem_record_index = 0;
    FileGameRedeemRecord redeem_records[512];

    // initialize the redeem record holder
    for (i=0; i < 512; i++)
    {
        memset(&redeem_records[i], 0, sizeof(FileGameRedeemRecord));
    }
    // load up the game redeem records in memory
    while (fileRead(GAME_REDEEM_RECORDS, file_index, &redeem_records[redeem_record_index], sizeof(FileGameRedeemRecord)))
    {
        file_index++;
        redeem_record_index++;
    }

	qsort( (void *)redeem_records, file_index, sizeof(FileGameRedeemRecord), compareGameRedeemRecordsByMuxId);
	qsort( (void *)redeem_records, file_index, sizeof(FileGameRedeemRecord), compareGameRedeemRecordsBySasId);
    // remove the unsorted file on disk
        DeleteFile(GAME_REDEEM_RECORDS);
        DeleteFile(GAME_REDEEM_RECORDS_REDUNDANT);
        // create the new file
        for (i=0; i < file_index; i++)
        {
            writeRedundantFile((CString)GAME_REDEEM_RECORDS, i, (BYTE*)&redeem_records[i], sizeof(FileGameRedeemRecord));
        }
    }

int compareGameRedeemRecordsByMuxId (const void * grr1, const void * grr2)
{

    FileGameRedeemRecord* redeem_record1 = (FileGameRedeemRecord*) grr1;
    FileGameRedeemRecord* redeem_record2 = (FileGameRedeemRecord*) grr2;
    CGameDevice* game_device1;
    CGameDevice* game_device2;
    BYTE mux_id1, mux_id2;

    game_device1 = theApp.GetGamePointer(redeem_record1->ucmc_id);
    if (game_device1 != NULL)
        mux_id1 = game_device1->memory_game_config.mux_id;
    else
        mux_id1 = 0;

    game_device2 = theApp.GetGamePointer(redeem_record2->ucmc_id);
    if (game_device2 != NULL)
        mux_id2 = game_device2->memory_game_config.mux_id;
    else
        mux_id2 = 0;

	if (mux_id1 < mux_id2)
		return -1;
	if (mux_id1 > mux_id2)
		return 1;

	return 0;

}

int compareGameRedeemRecordsBySasId (const void * grr1, const void * grr2)
{

    FileGameRedeemRecord* redeem_record1 = (FileGameRedeemRecord*) grr1;
    FileGameRedeemRecord* redeem_record2 = (FileGameRedeemRecord*) grr2;
    CGameDevice* game_device1;
    CGameDevice* game_device2;
    BYTE sas_id1, sas_id2;

    game_device1 = theApp.GetGamePointer(redeem_record1->ucmc_id);
    if (game_device1 != NULL)
        sas_id1 = game_device1->memory_game_config.sas_id;
    else
        sas_id1 = 0;

    game_device2 = theApp.GetGamePointer(redeem_record2->ucmc_id);
    if (game_device2 != NULL)
        sas_id2 = game_device2->memory_game_config.sas_id;
    else
        sas_id2 = 0;

    // only sort two sas ids, do not swap if one of the elements has a non zero mux id
    if (game_device1 && game_device2 && game_device1->memory_game_config.sas_id && game_device2->memory_game_config.sas_id)
    {
	    if (sas_id1 < sas_id2)
		    return -1;
	    if (sas_id1 > sas_id2)
		    return 1;
    }

	return 0;
}

// Function Description:  MACHINE REDEEM LOG
void CControllerApp::GameRedeemReport(DWORD customer_id)
{
 DWORD file_index = 0, total_redeemed = 0, redeemed_amount = 0;
 CString prt_buf, game_redeem_records_string;
 FileGameRedeemRecord redeem_record;
 CGameDevice* game_device;
 BYTE mux_id, sas_id;
 DWORD total_drop_ticket_amount = getTotalDropTicketAmount(customer_id);

    SortGameRedeemRecords();

//    PrinterTitle("PAID OUT LOG");
    PrinterTitle("TKT PAID LOG");
    prt_buf.Format("CUSTOMER ID: %u\n", customer_id);
    PrinterData(prt_buf.GetBuffer(0));
    PrinterId();
//    PrinterData("\n UCMC ID      PAID OUT      MUX ID\n ----------------------------------\n");
    PrinterData("\n UCMC ID      TKT PAID      MUX/SAS\n ----------------------------------\n");


    game_redeem_records_string = GAME_REDEEM_RECORDS;

    while (fileRead(game_redeem_records_string.GetBuffer(0), file_index, &redeem_record, sizeof(redeem_record)))
    {
        if (!redeem_record.customer_id || redeem_record.customer_id == customer_id)
        {
            game_device = theApp.GetGamePointer(redeem_record.ucmc_id);
            if (game_device != NULL)
			{
                mux_id = game_device->memory_game_config.mux_id;
                sas_id = game_device->memory_game_config.sas_id;
			}
            else
			{
                mux_id = 0;
                sas_id = 0;
			}

            redeemed_amount = getTotalDropTicketAmount(customer_id, redeem_record.ucmc_id);
//            total_redeemed += redeem_record.amount;
            total_redeemed += redeemed_amount;
//            prt_buf.Format("%6ld     %10.2f          %d\n", redeem_record.ucmc_id, (double)redeem_record.amount / 100, mux_id);
            if (mux_id)
			{
//                prt_buf.Format("%6ld     %10.2f       MUX %d\n", redeem_record.ucmc_id, (double)redeemed_amount / 100, mux_id);
                prt_buf.Format("%6ld     %10.2f           %d\n", redeem_record.ucmc_id, (double)redeemed_amount / 100, mux_id);
				PrinterData(prt_buf.GetBuffer(0));
			}
            else if (sas_id)
			{
                prt_buf.Format("%6ld     %10.2f           %d\n", redeem_record.ucmc_id, (double)redeemed_amount / 100, game_device->memory_game_config.sas_id);
				PrinterData(prt_buf.GetBuffer(0));
			}
        }

        file_index++;
    }

    prt_buf.Format("             ==========\n TOTAL -     %10.2f\n\n", (double)total_redeemed / 100);
    PrinterData(prt_buf.GetBuffer(0));

    if (total_redeemed != total_drop_ticket_amount)
    {
        prt_buf.Format("*** TKT PAID LOG / DROP TICKETS LOG\n*** DESCREPANCY DETECTED!!!\n*** SEE DROP TICKETS LOG!\n\n");
        PrinterData(prt_buf.GetBuffer(0));
    }

    PrinterData(PRINTER_FORM_FEED);
}

void CControllerApp::GetIpAddressAndPort(CString text_string, DWORD *ip_address, WORD *ip_port /*=NULL*/)
{
 char ip_convert_string[10], *string_pointer;
 BYTE *byte_ip_address = (BYTE*)ip_address;
 int iii, string_index, string_length;

    memset(ip_address, 0, 4);

    if (ip_port)
        *ip_port = 0;

    string_length = text_string.GetLength();

    if (string_length >= 7)
    {
        string_index = 0;
        string_pointer = text_string.GetBuffer(0);

        // 1st octet
        iii = 0;
        memset(&ip_convert_string, 0, sizeof(ip_convert_string));
        while (iii < 4 && string_index < string_length)
        {
            if (string_pointer[string_index] == '.')
                iii = 4;
            else
                ip_convert_string[iii] = string_pointer[string_index];

            iii++;
            string_index++;
        }
        byte_ip_address[0] = atoi(ip_convert_string);

        // 2nd octet
        iii = 0;
        memset(&ip_convert_string, 0, sizeof(ip_convert_string));
        while (iii < 4 && string_index < string_length)
        {
            if (string_pointer[string_index] == '.')
                iii = 4;
            else
                ip_convert_string[iii] = string_pointer[string_index];

            iii++;
            string_index++;
        }
        byte_ip_address[1] = atoi(ip_convert_string);

        // 3rd octet
        iii = 0;
        memset(&ip_convert_string, 0, sizeof(ip_convert_string));
        while (iii < 4 && string_index < string_length)
        {
            if (string_pointer[string_index] == '.')
                iii = 4;
            else
                ip_convert_string[iii] = string_pointer[string_index];

            iii++;
            string_index++;
        }
        byte_ip_address[2] = atoi(ip_convert_string);

        // 4th octet
        iii = 0;
        memset(&ip_convert_string, 0, sizeof(ip_convert_string));
        while (iii < 4 && string_index < string_length)
        {
            if (string_pointer[string_index] == ':')
                iii = 4;
            else
                ip_convert_string[iii] = string_pointer[string_index];

            iii++;
            string_index++;
        }
        byte_ip_address[3] = atoi(ip_convert_string);

        // port
        if (ip_port)
        {
            iii = 0;
            memset(&ip_convert_string, 0, sizeof(ip_convert_string));
            while (iii < 6 && string_index < string_length)
            {
                if (string_pointer[string_index] >= '0' && string_pointer[string_index] <= '9')
                {
                    ip_convert_string[iii] = string_pointer[string_index];
                    iii++;
                }
                string_index++;
            }

            *ip_port = atoi(ip_convert_string);
        }
    }
}

bool CControllerApp::CheckLoginPassword(ControllerUser *user)
{
 bool valid_id = false;
 char string_buffer[6];
 BYTE *encrypt_string = (BYTE*)ENCRYPT_STRING;
 int iii, jjj;
 EncryptedUserFileRecord user_record;
 char *record_pointer = (char*)&user_record;
 CashierAndManagerUserFile cashier_manager_user;

	if (GetFileAttributes(LOCATION_IDS) == 0xFFFFFFFF && GetFileAttributes(ENGR_IDS_PASSWORDS) == 0xFFFFFFFF &&
		GetFileAttributes(USER_IDS_PASSWORDS) == 0xFFFFFFFF && user->user.id / 10000 >= CASHIER &&
		user->user.id / 10000 <= ENGINEERING)
	{
		valid_id = true;
		sprintf_s(user->user.first_name, sizeof(user->user.first_name), "TESTING");
		sprintf_s(user->user.last_name, sizeof(user->user.last_name), "USER");
	}
	else
	{
	    switch (user->user.id / 10000)
	    {
	        case CASHIER:
	        case MANAGER:
	            if (fileGetRecord(LOCATION_IDS, &cashier_manager_user, sizeof(cashier_manager_user), user->user.id) >= 0 &&
	                user->user.password == cashier_manager_user.user.password)
	            {
	                valid_id = true;
	                memcpy(user->user.first_name, cashier_manager_user.user.first_name, USER_FILE_RECORD_STRING_LENGTH);
	                memcpy(user->user.last_name, cashier_manager_user.user.last_name, USER_FILE_RECORD_STRING_LENGTH);
	            }
	            break;

	        case COLLECTOR:
	        case TECHNICIAN:
	        case SUPERVISOR:
	        case ENGINEERING:
	            iii = 0;
	            while (!valid_id &&
	                ((user->user.id / 10000 >= SUPERVISOR && fileRead(ENGR_IDS_PASSWORDS, iii, &user_record, sizeof(user_record))) ||
	                (user->user.id / 10000 < SUPERVISOR && fileRead(USER_IDS_PASSWORDS, iii, &user_record, sizeof(user_record)))))
	            {
	                for (jjj=0; jjj < sizeof(user_record); jjj++)
	                    record_pointer[jjj] ^= encrypt_string[jjj];

	                memset(string_buffer, 0, sizeof(string_buffer));
	                memcpy(string_buffer, user_record.user_id, sizeof(user_record.user_id));

	                if (user->user.id == atoi(string_buffer))
	                {
	                    memset(string_buffer, 0, sizeof(string_buffer));
	                    memcpy(string_buffer, user_record.password, sizeof(user_record.password));

	                    if (user->user.password && user->user.password == atoi(string_buffer))
	                    {
	                        // reverse first name
	                        memcpy(user->user.first_name, user_record.first_name, USER_FILE_RECORD_STRING_LENGTH);
	                        for (jjj=0; jjj < USER_FILE_RECORD_STRING_LENGTH; jjj++)
	                            user_record.first_name[jjj] = user->user.first_name[USER_FILE_RECORD_STRING_LENGTH - 1 - jjj];

	                        // make sure first and last names are null terminated
	                        user_record.first_name[USER_FILE_RECORD_STRING_LENGTH - 1] = 0;
	                        user_record.last_name[USER_FILE_RECORD_STRING_LENGTH - 1] = 0;

	                        memcpy(user->user.first_name, user_record.first_name, USER_FILE_RECORD_STRING_LENGTH);
	                        memcpy(user->user.last_name, user_record.last_name, USER_FILE_RECORD_STRING_LENGTH);

	                        valid_id = true;
	                    }
	                }

	                iii++;
	            }
	            break;
	    }
	}

 return valid_id;
}

bool CControllerApp::CheckSlaveLoginPassword(ControllerUser *user)
{
 bool valid_id = false;
 time_t offline_timer, second_display;
 StratusMessageFormat stratus_msg;
 SystemUser user_return;
 CString window_message;
 CTimerWindow *pTimerWindow = NULL;

    // new request, clear queue
    while (!stratus_dialog_queue.empty())
        stratus_dialog_queue.pop();

    if (stratusSocket.socket_state)
        offline_timer = 0;
    else
    {
        memset(&stratus_msg, 0, sizeof(stratus_msg));
        stratus_msg.send_code = SLAVE_TKT_SYSTEM_LOGIN;
        stratus_msg.task_code = SLAVE_TKT_TASK_TICKET;
        stratus_msg.message_length = sizeof(user->user) + STRATUS_HEADER_SIZE;
        stratus_msg.data_to_event_log = true;
        memcpy(stratus_msg.data, &user->user, sizeof(user->user));
        QueueStratusTxMessage(&stratus_msg, FALSE);

        pTimerWindow = new CTimerWindow;
        pTimerWindow->Create(IDD_TIMER_BAR_DIALOG);
        pTimerWindow->m_timer_window_bar.SetRange(0, OFFLINE_TIMER_LIMIT);
        pTimerWindow->SetDlgItemText(IDC_TIMER_WINDOW_TITLE_STATIC, "Checking Password, Please Wait...");
        offline_timer = time(NULL) + OFFLINE_TIMER_LIMIT;
        second_display = 0;
    }

    while (offline_timer > time(NULL) && stratus_dialog_queue.empty())
    {
        if (second_display != time(NULL))
        {
            pTimerWindow->m_timer_window_bar.SetPos((int)(time(NULL) + OFFLINE_TIMER_LIMIT - offline_timer));
            second_display = time(NULL);
        }
        else
            OnIdle(0);
    }

    if (pTimerWindow)
    {
        pTimerWindow->DestroyWindow();
        delete pTimerWindow;
    }

    if (stratus_dialog_queue.empty())
    {
        printer_busy = true;
        PrinterHeader(0);
        PrinterId();
        PrinterData("\nThe Remote System Is Offline.\n");
        PrinterData("Unable To Login.\n\n");
        printer_busy = false;
        MessageWindow(false, "SYSTEM OFFLINE", "The Remote System is Offline.\nUnable To Login.");
        return valid_id;
    }

    stratus_msg = stratus_dialog_queue.front();
    stratus_dialog_queue.pop();

    if (stratus_msg.task_code == SLAVE_TKT_TASK_TICKET && stratus_msg.send_code == SLAVE_TKT_SYSTEM_LOGIN)
    {
        memcpy(&user_return, stratus_msg.data, sizeof(user_return));

        if (user_return.id == user->user.id && user_return.password == user->user.password)
        {
            // copy first and last name to controller user, other data a duplicate copy
            memcpy(&user->user, &user_return, sizeof(user_return));
            memcpy(&theApp.current_shift_user, &user_return, sizeof(user_return));
            valid_id = true;
        }
    }

 return valid_id;
}

void CControllerApp::RemovePendingTicketFromFile(DWORD ticket_id)
{
 int record_index;
 FilePendingTickets pending_ticket;
 StratusMessageFormat stratus_msg;

    if (ticket_id)
    {
        if (memory_settings_ini.masters_ip_address)
        {
            memset(&stratus_msg, 0, sizeof(stratus_msg));
            stratus_msg.message_length = sizeof(ticket_id) + STRATUS_HEADER_SIZE;
            memcpy(stratus_msg.data, &ticket_id, sizeof(ticket_id));
            stratus_msg.send_code = SLAVE_TKT_TICKET_REMOVE;
            stratus_msg.task_code = SLAVE_TKT_TASK_TICKET;
            stratus_msg.mux_id = CONTROLLER_MUX_ADDRESS;
            stratus_msg.ucmc_id = memory_dispenser_config.ucmc_id;
            stratus_msg.property_id = memory_location_config.property_id;
            stratus_msg.customer_id = memory_location_config.customer_id;
            stratus_msg.data_to_event_log = true;
            QueueStratusTxMessage(&stratus_msg, FALSE);
        }
        else if (GetFileAttributes(MASTER_CONFIG) != 0xFFFFFFFF)
        {
            record_index = fileGetRecord(PENDING_TICKETS, &pending_ticket, sizeof(pending_ticket), ticket_id);

            if (record_index >= 0)
                fileDeleteRecord(PENDING_TICKETS, sizeof(pending_ticket), record_index);
        }
    }
}

void CControllerApp::GetCustomerIdSelection(BYTE display_type)
{
 int iii;
 DWORD customer_id_list[MAXIMUM_CUSTOMER_IDS];
 CGameDevice *pGameDevice;
 CGameCommControl *pGameCommControl = pHeadGameCommControl;

    if (pSelectCustomerIdDlg)
    {
        delete pSelectCustomerIdDlg;
        pSelectCustomerIdDlg = NULL;
    }

    get_customer_id_selection = 0;

    memset(&customer_id_list, 0, sizeof(customer_id_list));

    while (pGameCommControl)
    {
        pGameDevice = pGameCommControl->pHeadGameDevice;

        while (pGameDevice)
        {
            for (iii=0; iii < MAXIMUM_CUSTOMER_IDS; iii++)
            {
                if (!customer_id_list[iii])
                {
                    customer_id_list[iii] = pGameDevice->memory_game_config.customer_id;
                    iii = MAXIMUM_CUSTOMER_IDS;
                }
                else
                {
                    if (customer_id_list[iii] == pGameDevice->memory_game_config.customer_id)
                        iii = MAXIMUM_CUSTOMER_IDS;
                }
            }

            pGameDevice = pGameDevice->pNextGame;
        }

        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }

    if (!customer_id_list[1])
    {
        if (customer_id_list[0])
            get_customer_id_selection = customer_id_list[0];
        else
            get_customer_id_selection = 1;  // set to dummy customer id to prevent infinte loops
    }
    else
    {
        pSelectCustomerIdDlg = new CSelectCustomerIdDlg(display_type, customer_id_list);
        pSelectCustomerIdDlg->Create(IDD_SELECT_CUSTOMER_ID_DIALOG);
    }

    while (!get_customer_id_selection)
        OnIdle(0);
}

void CControllerApp::CheckEventMessages(bool program_exit)
{
 bool dump_event_file;
 DWORD bytes_written;
 SYSTEMTIME time_struct;
 CString filename;
 HANDLE file_handle, tx_file_handle;
 EventMessage event_message;

	if (!event_message_queue.empty())
	{
		dump_event_file = false;
	    GetLocalTime(&time_struct);

		if (event_message_queue.size() > 200)
			dump_event_file = true;
		else
		{
		    filename.Format("%s\\%u-%u-%u.evt", EVENTS_PATH, time_struct.wYear, time_struct.wMonth, time_struct.wDay);
		    file_handle = CreateFile(filename, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
        	if (file_handle && SetFilePointer(file_handle, 0, 0, FILE_END) == 0xFFFFFFFF)
			{
		        CloseHandle(file_handle);
				file_handle = NULL;
			}

			tx_file_handle = CreateFile(TRANSMIT_EVENTS_FILE, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
        	if (tx_file_handle && SetFilePointer(tx_file_handle, 0, 0, FILE_END) == 0xFFFFFFFF)
			{
		        CloseHandle(tx_file_handle);
				tx_file_handle = NULL;
				logMessage("CControllerApp::CheckEventMessage - error opening tx event file.");
			}

			while (!event_message_queue.empty())
			{
				if (file_handle)
				{
					event_message = event_message_queue.front();
				    event_message.head.header = LONG_HEADER_IDENTIFIER;

			        if (WriteFile(file_handle, &event_message, sizeof(event_message.head) + event_message.head.data_length, &bytes_written, NULL) &&
						bytes_written == sizeof(event_message.head) + event_message.head.data_length)
					{
						event_message_queue.pop();

						if (tx_file_handle)
				        	if (!WriteFile(tx_file_handle, &event_message, sizeof(event_message.head) + event_message.head.data_length, &bytes_written, NULL) ||
								bytes_written != sizeof(event_message.head) + event_message.head.data_length)
									logMessage("CControllerApp::CheckEventMessage - error writing tx event file.");

						if (!program_exit)
						{
							if (tx_file_handle)
						        CloseHandle(tx_file_handle);

					        CloseHandle(file_handle);
							return;
						}
					}
					else
					{
						if (program_exit)
						{
							logMessage("CControllerApp::CheckEventMessages - problem writing to file.");
							dump_event_file = true;
							break;
						}
						else
						{
							if (tx_file_handle)
						        CloseHandle(tx_file_handle);

					        CloseHandle(file_handle);
							return;
						}
					}
				}
				else
				{
					if (program_exit)
					{
						dump_event_file = true;
						break;
					}
					else
					{
						if (tx_file_handle)
							CloseHandle(tx_file_handle);
						return;
					}
				}
			}

			if (file_handle)
		        CloseHandle(file_handle);

			if (tx_file_handle)
		        CloseHandle(tx_file_handle);
		}

		if (dump_event_file)
		{
			filename.Format("%s\\%u-%u-%u_dump.evt", EVENTS_PATH, time_struct.wYear, time_struct.wMonth, time_struct.wDay);
		    file_handle = CreateFile(filename, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
			if (file_handle)
				SetFilePointer(file_handle, 0, 0, FILE_END);

			tx_file_handle = CreateFile(TRANSMIT_EVENTS_FILE, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
        	if (tx_file_handle)
        		SetFilePointer(tx_file_handle, 0, 0, FILE_END);

			while (!event_message_queue.empty())
			{
				event_message = event_message_queue.front();
				event_message_queue.pop();
			    event_message.head.header = LONG_HEADER_IDENTIFIER;

				if (!tx_file_handle ||
					!WriteFile(tx_file_handle, &event_message, sizeof(event_message.head) + event_message.head.data_length, &bytes_written, NULL) ||
					bytes_written != sizeof(event_message.head) + event_message.head.data_length)
				        logMessage("CControllerApp::CheckEventMessages - problem writing to tx dump file.");

				if (!file_handle ||
					!WriteFile(file_handle, &event_message, sizeof(event_message.head) + event_message.head.data_length, &bytes_written, NULL) ||
					bytes_written != sizeof(event_message.head) + event_message.head.data_length)
				        logMessage("CControllerApp::CheckEventMessages - problem writing to dump file.");
			}

			if (tx_file_handle)
				CloseHandle(tx_file_handle);

			if (file_handle)
		        CloseHandle(file_handle);
		}
	}
}

void CControllerApp::PrinterData(char *printer_data)
{
 CString filename;
 SYSTEMTIME time_struct;

    if (current_user.autologout_timeout)
        current_user.time_setting = time(NULL);

    // do not write print commands to the printer log.
    if (strcmp(printer_data, PRINTER_FORM_FEED) && strcmp(printer_data, PRINTER_BIG_PRINT) &&
		strcmp(printer_data, PRINTER_NORMAL_PRINT))
    {
        // save printer data to file
        GetLocalTime(&time_struct);
        filename.Format("%s\\%d_%d_%d_printer.rtf", PRINTER_TEXT_PATH, time_struct.wYear, time_struct.wMonth, time_struct.wDay);
        fileWrite(filename.GetBuffer(0), -1, printer_data, (DWORD)strlen(printer_data));
    }

    general_printer_queue.push(printer_data);
}

void CControllerApp::W2gPrinterData(char *printer_data)
{
 CString filename;
 SYSTEMTIME time_struct;

    if (current_user.autologout_timeout)
        current_user.time_setting = time(NULL);

    // do not write print commands to the printer log.
    if (strcmp(printer_data, PRINTER_FORM_FEED) && strcmp(printer_data, PRINTER_BIG_PRINT) &&
		strcmp(printer_data, PRINTER_NORMAL_PRINT))
    {
        // save printer data to file
        GetLocalTime(&time_struct);
        filename.Format("%s\\%d_%d_%d_printer.rtf", PRINTER_TEXT_PATH, time_struct.wYear, time_struct.wMonth, time_struct.wDay);
        fileWrite(filename.GetBuffer(0), -1, printer_data, (DWORD)strlen(printer_data));
    }

    w2g_printer_queue.push(printer_data);
}

void CControllerApp::MarkerPrinterData(char *printer_data)
{
 CString filename;
 SYSTEMTIME time_struct;

    if (current_user.autologout_timeout)
        current_user.time_setting = time(NULL);

    // do not write print commands to the printer log.
    if (strcmp(printer_data, PRINTER_FORM_FEED) && strcmp(printer_data, PRINTER_BIG_PRINT) &&
		strcmp(printer_data, PRINTER_NORMAL_PRINT))
    {
        // save printer data to file
        GetLocalTime(&time_struct);
        filename.Format("%s\\%d_%d_%d_marker.rtf", PRINTER_TEXT_PATH, time_struct.wYear, time_struct.wMonth, time_struct.wDay);
        fileWrite(filename.GetBuffer(0), -1, printer_data, (DWORD)strlen(printer_data));
    }

    marker_printer_queue.push(printer_data);
}

void CControllerApp::DropReportData(char *printer_data)
{
 CString filename;
 SYSTEMTIME time_struct;

    if (current_user.autologout_timeout)
        current_user.time_setting = time(NULL);

    // save printer data to file
    GetLocalTime(&time_struct);
    filename.Format("%s\\%d_%d_%d_drop.rtf", DROP_REPORTS_PATH, time_struct.wYear, time_struct.wMonth, time_struct.wDay);
    fileWrite(filename.GetBuffer(0), -1, printer_data, (DWORD)strlen(printer_data));
}

void CControllerApp::PrinterTitle(char* printer_data)
{
 size_t count;
 char spaces[20], prt_buf[50];

    memset(&spaces[0], ' ', sizeof(spaces));
    PrinterData(PRINTER_FORM_FEED);

    count = strlen(memory_location_config.name);
//    count = (20 - count) / 2;
    if (count >= 40)
        count = (40 - count) / 2;
    else
        count = 0;

    if (count > 1)
        sprintf_s(prt_buf, sizeof(prt_buf), "%*.*s%s\n", count, count, spaces, memory_location_config.name);
    else
        sprintf_s(prt_buf, sizeof(prt_buf), "%s\n", memory_location_config.name);

    PrinterData(PRINTER_BIG_PRINT);
    PrinterData(prt_buf);
    PrinterData(PRINTER_NORMAL_PRINT);

    count = strlen(memory_location_config.address);
    if (count >= 40)
        count = (40 - count) / 2;
    else
        count = 0;

    if (count > 1)
        sprintf_s(prt_buf, sizeof(prt_buf), "%*.*s%s\n\n", count, count, spaces, memory_location_config.address);
    else
        sprintf_s(prt_buf, sizeof(prt_buf), "%s\n\n", memory_location_config.address);

    PrinterData(prt_buf);
    PrinterHeader(0);
    sprintf_s(prt_buf, sizeof(prt_buf), " %s\n", printer_data);
    PrinterData(prt_buf);
}

void CControllerApp::PrinterHeader(BYTE printer_destination)
{
 CString prt_buf, multi_controller_string;

    if (memory_settings_ini.masters_ip_address)
        multi_controller_string.Format("SLAVE %d", memory_dispenser_config.ucmc_id);
    else
    {
        if (GetFileAttributes(MASTER_CONFIG) != 0xFFFFFFFF)
            multi_controller_string = "MASTER";
        else
            multi_controller_string = " ";
    }

    switch (memory_dispenser_config.dispenser_type)
    {
        case DISPENSER_TYPE_NOT_SET:
            prt_buf.Format("\nV%d.%s NOT SET %s\n", VERSION, SUBVERSION, multi_controller_string);
            break;

        case DISPENSER_TYPE_CASH_DRAWER:
            prt_buf.Format("\nV%d.%s CASH DRAWER %s\n", VERSION, SUBVERSION, multi_controller_string);
            break;

        case DISPENSER_TYPE_DIEBOLD:
            prt_buf.Format("\nV%d.%s DIEBOLD %s\n", VERSION, SUBVERSION, multi_controller_string);
            break;

        case DISPENSER_TYPE_ECASH:
            prt_buf.Format("\nV%d.%s ECASH %s\n", VERSION, SUBVERSION, multi_controller_string);
            break;

        case DISPENSER_TYPE_CD2000:
            prt_buf.Format("\nV%d.%s CD2000 %s\n", VERSION, SUBVERSION, multi_controller_string);
            break;

        case DISPENSER_TYPE_JCM_100:
            prt_buf.Format("\nV%d.%s JCM $100 %s\n", VERSION, SUBVERSION, multi_controller_string);
            break;

        case DISPENSER_TYPE_JCM_20:
            prt_buf.Format("\nV%d.%s JCM $20 %s\n", VERSION, SUBVERSION, multi_controller_string);
            break;

        case DISPENSER_TYPE_LCDM1000_20:
            prt_buf.Format("\nV%d.%s UCMC1000DS %s\n", VERSION, SUBVERSION, multi_controller_string);
            break;

        case DISPENSER_TYPE_LCDM2000_20_100:
            prt_buf.Format("\nV%d.%s UCMC2000DS %s\n", VERSION, SUBVERSION, multi_controller_string);
            break;

        case DISPENSER_TYPE_LCDM4000_20_100:
            prt_buf.Format("\nV%d.%s UCMC4000DS %s\n", VERSION, SUBVERSION, multi_controller_string);
            break;
        default:
            prt_buf.Format("\nV%d.%s UNKNOWN DISPENSER %s\n", VERSION, SUBVERSION, multi_controller_string);
            break;
    }

    switch (printer_destination)
    {
        case 1:
            game_lock_printer_queue.push(prt_buf);
            game_lock_printer_queue.push(convertTimeStamp(time(NULL)));
            break;

        case 2:
            MarkerPrinterData(prt_buf.GetBuffer(0));
            MarkerPrinterData(convertTimeStamp(time(NULL)));
            break;

        default:
            PrinterData(prt_buf.GetBuffer(0));
            PrinterData(convertTimeStamp(time(NULL)));
            break;
    }
}

void CControllerApp::PrinterId()
{
 char prt_buf[50];

    switch (current_user.user.id / 10000)
    {
        case CASHIER:
            sprintf_s(prt_buf, sizeof(prt_buf), " CASHIER ID: %ld\n", current_user.user.id);
            break;

        case MANAGER:
            sprintf_s(prt_buf, sizeof(prt_buf), " MGR. ID: %ld\n", current_user.user.id);
            break;

        case COLLECTOR:
            sprintf_s(prt_buf, sizeof(prt_buf), " DROP ID: %ld\n", current_user.user.id);
            break;

        case TECHNICIAN:
            sprintf_s(prt_buf, sizeof(prt_buf), " TECH ID: %ld\n", current_user.user.id);
            break;

        case SUPERVISOR:
            sprintf_s(prt_buf, sizeof(prt_buf), " SUPER: %ld\n", current_user.user.id);
            break;

        case ENGINEERING:
            sprintf_s(prt_buf, sizeof(prt_buf), " ENGR: %ld\n", current_user.user.id);
            break;

        default:
            return;
    }

    PrinterData(prt_buf);
}

BYTE CControllerApp::VerifyCcClientUserIdAndPassword(CCustomerControlClient *socket, BYTE *user_id_and_password)
{
 char string_buffer[6];
 BYTE *record_pointer, *encrypt_string = (BYTE*)ENCRYPT_STRING, return_value = 0;
 int iii, jjj, compare_user_id, compare_password;
 DWORD rx_user_id, rx_password;
 SocketMessage client_message;
 CString error_message, client_address;
 DWORD user_level;
 EncryptedUserFileRecord user_record;

    memcpy(&rx_user_id, user_id_and_password, 4);
    memcpy(&rx_password, &user_id_and_password[4], 4);

    iii = 0;
    user_level = rx_user_id / 10000;
    record_pointer = (BYTE*)&user_record;


	if (GetFileAttributes(ENGR_IDS_PASSWORDS) == 0xFFFFFFFF && GetFileAttributes(USER_IDS_PASSWORDS) == 0xFFFFFFFF)
		return_value = (BYTE)user_level;
	else
	{
	    while ((user_level >= SUPERVISOR && fileRead(ENGR_IDS_PASSWORDS, iii, &user_record, sizeof(user_record))) ||
	        (user_level < SUPERVISOR && fileRead(USER_IDS_PASSWORDS, iii, &user_record, sizeof(user_record))))
	    {
	        for (jjj=0; jjj < sizeof(user_record); jjj++)
	            record_pointer[jjj] ^= encrypt_string[jjj];

	        memset(string_buffer, 0, sizeof(string_buffer));
	        memcpy(string_buffer, user_record.user_id, sizeof(user_record.user_id));
	        compare_user_id = atoi(string_buffer);
	        memset(string_buffer, 0, sizeof(string_buffer));
	        memcpy(string_buffer, user_record.password, sizeof(user_record.password));
	        compare_password = atoi(string_buffer);

	        if (compare_user_id && compare_password && compare_user_id == rx_user_id && compare_password == rx_password)
	        {
	            return_value = compare_user_id / 10000;
	            break;
	        }
	        else
	            iii++;
	    }
	}

    if (return_value)
        error_message = "User id and password authenticated.";
    else
        error_message = "Invalid user id and/or password, connection terminated.";

    memset(&client_message, 0, sizeof(client_message));
    client_message.head.header = LONG_HEADER_IDENTIFIER;
    client_message.head.command = CC_CLIENT_ASCII_STRING;
    client_message.head.data_length = error_message.GetLength();
    memcpy(client_message.data, error_message.GetBuffer(0), client_message.head.data_length);
    socket->Send(&client_message, client_message.head.data_length + sizeof(client_message.head));

    if (!return_value)
    {
        if (socket->m_hSocket != INVALID_SOCKET)
            socket->Close();

        socket->delete_socket = true;
    }

 return return_value;
}

char* CControllerApp::SendDispenserInfo(CCustomerControlClient *socket)
{
 char buffer[20];
 int iii;
 SocketMessage client_message;
 CString format_string;
 static CString ascii_string;

    ascii_string = "Dispenser Type: ";

    switch (memory_dispenser_config.dispenser_type)
    {
        case DISPENSER_TYPE_NO_DISPENSER:
            ascii_string += "None.\r\n";
            break;

        case DISPENSER_TYPE_CASH_DRAWER:
            ascii_string += "Cash Drawer.\r\n";
            break;

        case DISPENSER_TYPE_JCM_20:
            ascii_string += "JCM $20.\r\n";
            break;

        case DISPENSER_TYPE_JCM_100:
            ascii_string += "JCM $100.\r\n";
            break;

        case DISPENSER_TYPE_ECASH:
            ascii_string += "Ecash.\r\n";
            break;

        case DISPENSER_TYPE_CD2000:
            ascii_string += "CD2000.\r\n";
            break;

        case DISPENSER_TYPE_DIEBOLD:
            ascii_string += "Diebold.\r\n";
            break;

        case DISPENSER_TYPE_NOT_SET:
            ascii_string += "Dispenser Type Not Set.\r\n";
			break;

        case DISPENSER_TYPE_LCDM1000_20:
            ascii_string += "UCMC1000DS.\r\n";
            break;

        case DISPENSER_TYPE_LCDM2000_20_100:
            ascii_string += "UCMC2000DS.\r\n";
            break;

        case DISPENSER_TYPE_LCDM4000_20_100:
            ascii_string += "UCMC4000DS.\r\n";
            break;

        default:
            ascii_string += "Unknown Dispenser Type.\r\n";
			break;
    }

    if (memory_dispenser_config.dispenser_type == DISPENSER_TYPE_JCM_20 || memory_dispenser_config.dispenser_type == DISPENSER_TYPE_JCM_100)
    {
        format_string.Format("\r\nDispenser Counters:\r\n    Resets: %u\r\n    Initializations: %u",
            dispenserControl.jcm_dispenser_errors.resets, dispenserControl.jcm_dispenser_errors.initializations);
        ascii_string += format_string;

        format_string.Format("\r\nFailed Commands:\r\n    Resets: %u\r\n    Initializations: %u\r\n    Poll Response: %u"
            "\r\n    ROM ID: %u\r\n    Selecting: %u\r\n    Reject: %u\r\n    Counting and Dispensing: %u"
            "\r\n    NAKs Received: %u\r\n    Unknown Commands: %u", dispenserControl.jcm_dispenser_errors.failed_resets,
            dispenserControl.jcm_dispenser_errors.failed_initializations, dispenserControl.jcm_dispenser_errors.failed_dispenser_response_to_poll,
            dispenserControl.jcm_dispenser_errors.failed_dispenser_id, dispenserControl.jcm_dispenser_errors.failed_selecting_error,
            dispenserControl.jcm_dispenser_errors.failed_reject, dispenserControl.jcm_dispenser_errors.failed_counting_and_dispensing,
            dispenserControl.jcm_dispenser_errors.nak_received, dispenserControl.jcm_dispenser_errors.failed_command_unknown);
        ascii_string += format_string;

        format_string.Format("\r\nFailed Receive Messages:\r\n    EOT: %u\r\n    ACK: %u\r\n    NAK: %u\r\n    CRC: %u"
            "\r\n    Station Address: %u\r\n    Length: %u\r\n    Unknown Type: %u", dispenserControl.jcm_dispenser_errors.failed_eot_message,
            dispenserControl.jcm_dispenser_errors.failed_ack_message, dispenserControl.jcm_dispenser_errors.failed_nak_message,
            dispenserControl.jcm_dispenser_errors.failed_crc, dispenserControl.jcm_dispenser_errors.failed_device_address,
            dispenserControl.jcm_dispenser_errors.failed_message_length, dispenserControl.jcm_dispenser_errors.unknown_message_type);
        ascii_string += format_string;
    }


    if ((memory_dispenser_config.dispenser_type == DISPENSER_TYPE_LCDM1000_20) ||
        (memory_dispenser_config.dispenser_type == DISPENSER_TYPE_LCDM2000_20_100) ||
        (memory_dispenser_config.dispenser_type == DISPENSER_TYPE_LCDM4000_20_100))
    {
        format_string.Format("\r\nDispenser Error Counters:"
                             "\r\n    unknown_message_type: %u"
                             "\r\n    bcc_mismatch: %u"
                             "\r\n    message_length_error : %u"
                             "\r\n    nak_received : %u"
                             "\r\n    pickup_error: %u"
                             "\r\n    jam_at_chk: %u"
                             "\r\n    overflow_bill: %u"
                             "\r\n    jam_at_exit: %u"
                             "\r\n    jam_at_div: %u"
                             "\r\n    undefined_command: %u"
                             "\r\n    bill_end: %u"
                             "\r\n    note_request_error: %u"
                             "\r\n    counting_error1: %u"
                             "\r\n    counting_error2: %u"
                             "\r\n    reject_tray: %u"
                             "\r\n    motor_stop: %u"
                             "\r\n    jam_div: %u"
                             "\r\n    timeout: %u"
                             "\r\n    over_reject: %u"
                             "\r\n    cassette_not_recognized: %u"
                             "\r\n    dispensing_timeout: %u"
                             "\r\n    diverter_error: %u"
                             "\r\n    sol_sensor_error: %u"
                             "\r\n    purge_error: %u",
                            dispenserControl.puloon_dispenser_errors.unknown_message_type,
                            dispenserControl.puloon_dispenser_errors.bcc_mismatch,
                            dispenserControl.puloon_dispenser_errors.message_length_error,
                            dispenserControl.puloon_dispenser_errors.nak_received,
                            dispenserControl.puloon_dispenser_errors.pickup_error,
                            dispenserControl.puloon_dispenser_errors.jam_at_chk,
                            dispenserControl.puloon_dispenser_errors.overflow_bill,
                            dispenserControl.puloon_dispenser_errors.jam_at_exit,
                            dispenserControl.puloon_dispenser_errors.jam_at_div,
                            dispenserControl.puloon_dispenser_errors.undefined_command,
                            dispenserControl.puloon_dispenser_errors.bill_end,
                            dispenserControl.puloon_dispenser_errors.note_request_error,
                            dispenserControl.puloon_dispenser_errors.counting_error1,
                            dispenserControl.puloon_dispenser_errors.counting_error2,
                            dispenserControl.puloon_dispenser_errors.reject_tray,
                            dispenserControl.puloon_dispenser_errors.motor_stop,
                            dispenserControl.puloon_dispenser_errors.jam_div,
                            dispenserControl.puloon_dispenser_errors.timeout,
                            dispenserControl.puloon_dispenser_errors.over_reject,
                            dispenserControl.puloon_dispenser_errors.cassette_not_recognized,
                            dispenserControl.puloon_dispenser_errors.dispensing_timeout,
                            dispenserControl.puloon_dispenser_errors.diverter_error,
                            dispenserControl.puloon_dispenser_errors.sol_sensor_error,
                            dispenserControl.puloon_dispenser_errors.purge_error);

        ascii_string += format_string;

    }


    if (memory_dispenser_config.dispenser_type == DISPENSER_TYPE_ECASH)
    {
        format_string.Format("\r\nFailed Commands:\r\n    Initializations: %u\r\n    Sensor Poll Response: %u\r\n    Dispensing: %u"
            "\r\n    Test Dispensing: %u\r\n    CDU Version Read: %u\r\n    Note Index Settings: %u\r\n    Nation And # Of Cassettes: %u"
            "\r\n    Unknown Commands: %u", dispenserControl.ecash_dispenser_errors.failed_initializations,
            dispenserControl.ecash_dispenser_errors.failed_sensor_polling, dispenserControl.ecash_dispenser_errors.failed_dispensing,
            dispenserControl.ecash_dispenser_errors.failed_test_dispensing, dispenserControl.ecash_dispenser_errors.failed_cdu_version_read,
            dispenserControl.ecash_dispenser_errors.failed_note_index_settings,
            dispenserControl.ecash_dispenser_errors.failed_nation_and_number_of_installed_cassettes,
            dispenserControl.ecash_dispenser_errors.failed_command_unknown);
        ascii_string += format_string;

        format_string.Format("\r\nFailed Receive Messages:\r\n    Message Header: %u\r\n    NAK: %u\r\n    CRC: %u",
            dispenserControl.ecash_dispenser_errors.failed_message_header, dispenserControl.ecash_dispenser_errors.nak_received,
            dispenserControl.ecash_dispenser_errors.failed_crc);
        ascii_string += format_string;

        ascii_string += "\r\n\r\nStatus Bytes:";

        switch (dispenserControl.ecash_settings.country_code)
        {
            case 0:
                break;

            case 'H':
                ascii_string += "\r\n    Country Code: AUSTRALIA";
                break;

            case 'U':
                ascii_string += "\r\n    Country Code: UNITED STATES";
                break;

            default:
                ascii_string += "\r\n    Country Code: UNKNOWN";
                break;
        }

        memset(buffer, 0, sizeof(buffer));
        memcpy(buffer, dispenserControl.ecash_settings.dispenser_mode, 3);
        if (buffer[0])
        {
            ascii_string += "\r\n    Dispenser Mode: ";
            ascii_string += buffer;
        }

        memset(buffer, 0, sizeof(buffer));
        memcpy(buffer, dispenserControl.ecash_settings.version, 6);
        if (buffer[0])
        {
            ascii_string += "\r\n    Version: ";
            ascii_string += buffer;
        }


        format_string.Format("\r\n    Number Of Cassettes: %u", dispenserControl.ecash_settings.number_of_cassettes);
        ascii_string += format_string;

        for (iii=0; iii < dispenserControl.ecash_settings.number_of_cassettes; iii++)
        {
            format_string.Format("\r\n    Cassette %d Note Index: 0x%2.2X", iii + 1, dispenserControl.ecash_settings.notes_index[iii]);
            ascii_string += format_string;
        }

        switch (dispenserControl.ecash_settings.message_status)
        {
            case 0:
                break;

            case 'O':
                ascii_string += "\r\n    Message Status: NORMAL";
                break;

            case 'F':
                ascii_string += "\r\n    Message Status: FAIL";
                break;

            default:
                ascii_string += "\r\n    Message Status: UNKNOWN STATE";
                break;
        }

        memset(buffer, 0, sizeof(buffer));
        memcpy(buffer, dispenserControl.ecash_settings.error_code, 5);
        if (buffer[0])
        {
            ascii_string += "\r\n    Error Code: ";
            ascii_string += buffer;
        }

        ascii_string += "\r\n\r\nSensor Status:\r\n    Raw Bytes:";

        for (iii=0; iii < 13; iii++)
        {
            format_string.Format(" 0x%2.2X", dispenserControl.ecash_settings.sensor_status[iii]);
            ascii_string += format_string;
        }

        if (dispenserControl.ecash_settings.sensor_status[4] & 0x02)
            ascii_string += "\r\n    NS7_DARK: ON";
        else
            ascii_string += "\r\n    NS7_DARK: OFF";

        if (dispenserControl.ecash_settings.sensor_status[4] & 0x04)
            ascii_string += "\r\n    NS3A_NO_NOTES: ON";
        else
            ascii_string += "\r\n    NS3A_NO_NOTES: OFF";

        if (dispenserControl.ecash_settings.sensor_status[4] & 0x08)
            ascii_string += "\r\n    NS3B_NO_NOTES: ON";
        else
            ascii_string += "\r\n    NS3B_NO_NOTES: OFF";

        if (dispenserControl.ecash_settings.sensor_status[4] & 0x10)
            ascii_string += "\r\n    NS1A_NO_NOTES: ON";
        else
            ascii_string += "\r\n    NS1A_NO_NOTES: OFF";

        if (dispenserControl.ecash_settings.sensor_status[4] & 0x20)
            ascii_string += "\r\n    NS1B_NO_NOTES: ON";
        else
            ascii_string += "\r\n    NS1B_NO_NOTES: OFF";

        if (dispenserControl.ecash_settings.sensor_status[5] & 0x04)
            ascii_string += "\r\n    NS12_CST_2_LOW_NOTES: ON";
        else
            ascii_string += "\r\n    NS12_CST_2_LOW_NOTES: OFF";

        if (dispenserControl.ecash_settings.sensor_status[5] & 0x10)
            ascii_string += "\r\n    NS11_CST_1_SET: ON";
        else
            ascii_string += "\r\n    NS11_CST_1_SET: OFF";

        if (dispenserControl.ecash_settings.sensor_status[5] & 0x20)
            ascii_string += "\r\n    NS4_NO_NOTES: ON";
        else
            ascii_string += "\r\n    NS4_NO_NOTES: OFF";

        if (dispenserControl.ecash_settings.sensor_status[5] & 0x40)
            ascii_string += "\r\n    NS10_CST_1_LOW_NOTES: ON";
        else
            ascii_string += "\r\n    NS10_CST_1_LOW_NOTES: OFF";

        if (dispenserControl.ecash_settings.sensor_status[5] & 0x80)
            ascii_string += "\r\n    NS11_CST_2_SET: ON";
        else
            ascii_string += "\r\n    NS11_CST_2_SET: OFF";

        if (dispenserControl.ecash_settings.sensor_status[6] & 0x08)
            ascii_string += "\r\n    SPS4_PAPER_EXIST: ON";
        else
            ascii_string += "\r\n    SPS4_PAPER_EXIST: OFF";

        if (dispenserControl.ecash_settings.sensor_status[6] & 0x10)
            ascii_string += "\r\n    NS5_NOTE_EXIST: ON";
        else
            ascii_string += "\r\n    NS5_NOTE_EXIST: OFF";

        if (dispenserControl.ecash_settings.sensor_status[6] & 0x20)
            ascii_string += "\r\n    NS6_NOTE_EXIST: ON";
        else
            ascii_string += "\r\n    NS6_NOTE_EXIST: OFF";

        if (dispenserControl.ecash_settings.sensor_status[6] & 0x40)
            ascii_string += "\r\n    NS13_SHU_OPEN: ON";
        else
            ascii_string += "\r\n    NS13_SHU_OPEN: OFF";

        if (dispenserControl.ecash_settings.sensor_status[6] & 0x80)
            ascii_string += "\r\n    NS14_SHU_CLOSE: ON";
        else
            ascii_string += "\r\n    NS14_SHU_CLOSE: OFF";

        if (dispenserControl.ecash_settings.sensor_status[8] & 0x02)
            ascii_string += "\r\n    SPS3_LOW_PAPER: ON";
        else
            ascii_string += "\r\n    SPS3_LOW_PAPER: OFF";

        if (dispenserControl.ecash_settings.sensor_status[8] & 0x04)
            ascii_string += "\r\n    SPS2_END_DETECTED_PAPER_EXIST: ON";
        else
            ascii_string += "\r\n    SPS2_END_DETECTED_PAPER_EXIST: OFF";

        if (dispenserControl.ecash_settings.sensor_status[8] & 0x08)
            ascii_string += "\r\n    SPS1_NO_PAPER: ON";
        else
            ascii_string += "\r\n    SPS1_NO_PAPER: OFF";

        if (dispenserControl.ecash_settings.sensor_status[8] & 0x10)
            ascii_string += "\r\n    SPSW1_LEVER_OPEN: ON";
        else
            ascii_string += "\r\n    SPSW1_LEVER_OPEN: OFF";

        if (dispenserControl.ecash_settings.sensor_status[8] & 0x40)
            ascii_string += "\r\n    TPH_PAPER_EXIST: ON";
        else
            ascii_string += "\r\n    TPH_PAPER_EXIST: OFF";
    }

    if (memory_dispenser_config.ucmc_id)
    {
        if (memory_dispenser_config.autopay_allowed)
            ascii_string += "\r\nAutoPay: Yes.";
        else
            ascii_string += "\r\nAutoPay: No.";

        if (memory_dispenser_config.autopay_manager_approval)
            ascii_string += "\r\nAutoPay Manager Approval: Yes.";
        else
            ascii_string += "\r\nAutoPay Manager Approval: No.";
    }

    switch (memory_dispenser_config.dispenser_type)
    {
        case DISPENSER_TYPE_ECASH:
        case DISPENSER_TYPE_DIEBOLD:
        case DISPENSER_TYPE_CD2000:
        case DISPENSER_TYPE_LCDM2000_20_100:
        case DISPENSER_TYPE_LCDM4000_20_100:
            if (memory_dispenser_config.dispense_100s_when_20s_low)
                ascii_string += "\r\nDispense 100s when 20s are low: Yes.";
            else
                ascii_string += "\r\nDispense 100s when 20s are low: No.";

            format_string.Format("\r\n$100 Impress: %9.2f\r\n$20 Impress: %9.2f\r\n$100 Rejected Bill Count: %u"
                "\r\n$20 Rejected Bill Count: %u\r\n$100 Dispense Total: %9.2f\r\n$20 Dispense Total: %9.2f",
                (double)dispenserControl.memory_dispenser_values.impress_amount1 / 100,
                (double)dispenserControl.memory_dispenser_values.impress_amount2 / 100,
			    dispenserControl.memory_dispenser_values.reject_bills1,
		        dispenserControl.memory_dispenser_values.reject_bills2,
                (double)dispenserControl.memory_dispenser_values.dispense_total1 / 100,
                (double)dispenserControl.memory_dispenser_values.dispense_total2 / 100);

            ascii_string += format_string;
            break;
        case DISPENSER_TYPE_CASH_DRAWER:
        case DISPENSER_TYPE_JCM_20:
        case DISPENSER_TYPE_JCM_100:
        case DISPENSER_TYPE_LCDM1000_20:
            format_string.Format("\r\nImpress: %9.2f", (double)(dispenserControl.memory_dispenser_values.impress_amount1
															  + dispenserControl.memory_dispenser_values.impress_amount2) / 100);
            ascii_string += format_string;
            format_string.Format("\r\nRejected Bill Count: %u", dispenserControl.memory_dispenser_values.reject_bills1);
            ascii_string += format_string;
            format_string.Format("\r\nDispense Total: %9.2f\r\nTickets Amount: %9.2f\r\nNumber Of Tickets: %u",
                (double)dispenserControl.memory_dispenser_values.dispense_grand_total / 100,
                (double)dispenserControl.memory_dispenser_values.tickets_amount / 100,
                dispenserControl.memory_dispenser_values.number_tickets);
            ascii_string += format_string;
            break;
    }

	if (socket)
	{
		memset(&client_message, 0, sizeof(client_message));
		client_message.head.command = LVIEW_DISPENSER_INFO_GENERAL;

		if (ascii_string.GetLength() >= TCPIP_MESSAGE_BUFFER_SIZE)
		{
			logMessage("CControllerApp::SendDispenserInfo - maximum message size exceeded.");
			client_message.head.data_length = TCPIP_MESSAGE_BUFFER_SIZE - 1;
		}
		else
			client_message.head.data_length = ascii_string.GetLength();

		memcpy(client_message.data, ascii_string.GetBuffer(0), client_message.head.data_length);
		socket->client_message_queue.push(client_message);
	}

 return ascii_string.GetBuffer(0);
}

void CControllerApp::fillDispenser (void)
{
 time_t time_stamp;
 DWORD file_index, breakage, impress_amount1, impress_amount2, total_redeem, total_tickets;
 CString prt_buf;
 FileDropTicket file_drop_ticket;
 FilePaidTicket file_paid_ticket;
 FileCashierShift file_cashier_shift;
 FillValues fill_values;
 StratusMessageFormat stratus_msg;
 int i;


    theApp.fill_in_progress = TRUE;

	theApp.printer_busy = true;
	theApp.PrintDispenserInfo();
	theApp.printer_busy = false;

    if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_CASH_DRAWER)
        theApp.OpenCashDrawer();

    theApp.printer_busy = true;

    theApp.PrinterData("Previous Fill Time\n");

    if (GetFileAttributes(PREVIOUS_FILL) == 0xFFFFFFFF)
        theApp.PrinterData("First Fill\n");
    else
    {
        fileRead(PREVIOUS_FILL, 0, &time_stamp, sizeof(time_stamp));
        prt_buf.Format("%s\n", convertTimeStamp(time_stamp));
        theApp.PrinterData(prt_buf.GetBuffer(0));
    }

    time_stamp = time(NULL);
    fileWrite(PREVIOUS_FILL, 0, &time_stamp, sizeof(time_stamp));

    file_index = 0;
    while (fileRead(DROP_TICKETS, file_index, &file_drop_ticket, sizeof(file_drop_ticket)))
    {
        if (!file_drop_ticket.ticket_filled && !file_drop_ticket.dispenser_ucmc_id)
        {
            file_drop_ticket.ticket_filled = 1;
            writeRedundantFile((CString)DROP_TICKETS, file_index, (BYTE*)&file_drop_ticket, sizeof(file_drop_ticket));
        }

        file_index++;
    }

    clearPaidTicketsTotal();

    theApp.PrinterTitle("TKT FILL LOG");
    theApp.PrinterId();

    prt_buf.Format("CUSTOMER ID: %u\n", theApp.memory_location_config.customer_id);
    theApp.PrinterData(prt_buf.GetBuffer(0));

//        theApp.PrinterData("\n GAME  TICKET ID  REDEEM AMOUNT\n");
    theApp.PrinterData("\n GAME  TICKET ID  PAID OUT\n");
    theApp.PrinterData(" ------------------------------\n");

    file_index = 0;
    total_tickets = 0;
    total_redeem = 0;

    while (fileRead(PAID_TICKETS, file_index, &file_paid_ticket, sizeof(file_paid_ticket)))
    {
        if (!file_paid_ticket.memory.ticket_filled && !file_paid_ticket.memory.dispenser_ucmc_id &&
            !file_paid_ticket.memory.memory_game_lock.jackpot_to_credit_flag &&
            file_paid_ticket.memory.memory_game_lock.ticket_amount > file_paid_ticket.memory.memory_game_lock.marker_balance)
        {
            total_tickets++;
            file_paid_ticket.memory.ticket_filled = 1;
            writeRedundantFile((CString)PAID_TICKETS, file_index, (BYTE*)&file_paid_ticket, sizeof(file_paid_ticket));

            file_paid_ticket.memory.memory_game_lock.ticket_amount -= file_paid_ticket.memory.memory_game_lock.marker_balance;
            total_redeem += file_paid_ticket.memory.memory_game_lock.ticket_amount;

            addPaidTicketsTotal (file_paid_ticket.memory.memory_game_lock.ucmc_id, file_paid_ticket.memory.memory_game_lock.ticket_amount);

            if (file_paid_ticket.dispenser_malfunction)
                prt_buf.Format("DM %u  %u    %6.2f *FILLED*\n", file_paid_ticket.memory.memory_game_lock.ucmc_id,
                    file_paid_ticket.memory.memory_game_lock.ticket_id, (double)file_paid_ticket.memory.memory_game_lock.ticket_amount / 100);
            else
                prt_buf.Format(" %u  %u    %6.2f *FILLED*\n", file_paid_ticket.memory.memory_game_lock.ucmc_id,
                    file_paid_ticket.memory.memory_game_lock.ticket_id, (double)file_paid_ticket.memory.memory_game_lock.ticket_amount / 100);
            theApp.PrinterData(prt_buf.GetBuffer(0));
    	}

        file_index++;
    }

    if (!total_tickets)
    {
        theApp.PrinterData("NO TICKETS FOR FILL\n");
        theApp.PrinterData(PRINTER_FORM_FEED);
        MessageWindow(false, "FILL", "NO TICKETS FOR FILL");
    }
    else
    {
        prt_buf.Format("\n TOTAL FILL REDEEM: $%9.2f\n", (double)total_redeem / 100);
        theApp.PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format(" TOTAL TICKETS: %u\n", total_tickets);
        theApp.PrinterData(prt_buf.GetBuffer(0));

        theApp.PrinterData("\n TOTAL PER GAME\n");
        i = 0;
        while (paid_per_game[i].ucmc_id)
        {
            prt_buf.Format(" %u  %6.2f\n", paid_per_game[i].ucmc_id, (double)paid_per_game[i].ticket_amount / 100);
            theApp.PrinterData(prt_buf.GetBuffer(0));
            i++;
        }

        BalanceBreakage(1, total_redeem);

        if (total_redeem != theApp.dispenserControl.memory_dispenser_values.tickets_amount)
        {
            theApp.PrinterData("****************************************");

            // Part to get breakage right by a new method
            if (theApp.dispenserControl.memory_dispenser_values.dispense_grand_total > total_redeem)
                breakage = theApp.dispenserControl.memory_dispenser_values.dispense_grand_total - total_redeem;
            else
                breakage = 0;
        }
        else
            breakage = theApp.dispenserControl.memory_dispenser_values.dispense_grand_total -
                theApp.dispenserControl.memory_dispenser_values.tickets_amount;

        if (theApp.memory_dispenser_config.dispenser_type != DISPENSER_TYPE_CASH_DRAWER)
            BalanceBreakage(2, breakage);

        // clear dispenser values, save impress
        impress_amount1 = theApp.dispenserControl.memory_dispenser_values.impress_amount1;
        impress_amount2 = theApp.dispenserControl.memory_dispenser_values.impress_amount2;
        memset(&theApp.dispenserControl.memory_dispenser_values, 0, sizeof(theApp.dispenserControl.memory_dispenser_values));
        theApp.dispenserControl.memory_dispenser_values.impress_amount1 = impress_amount1;
        theApp.dispenserControl.memory_dispenser_values.impress_amount2 = impress_amount2;
        theApp.dispenserControl.WriteDispenserFile();

        theApp.SendDispenserBillCount(false, 0);

        DeleteFile(DRINK_COMPS_TOTAL);
        DeleteFile(PAID_TICKETS);
        DeleteFile(PAID_TICKETS_REDUNDANT);

        memset(&fill_values, 0, sizeof(fill_values));
        fill_values.fill_amount = total_redeem;
        fill_values.type        = 'F';
        fill_values.impress_bill_amount_100 = impress_amount1;
        fill_values.impress_bill_amount_20 = impress_amount2;

        // generate a transaction_id
        rand_s((unsigned int*)&fill_values.transaction_id);

        theApp.SendIdcFillValuesMessage(&fill_values);

        endian4(fill_values.fill_amount);
        endian4(fill_values.impress_bill_amount_100);
        endian4(fill_values.impress_bill_amount_20);
        endian4(fill_values.transaction_id);
        fill_values.message_revision = 'A';

        memset(&stratus_msg, 0, sizeof(stratus_msg));
        stratus_msg.send_code = TKT_FILL_MESSAGE;
        stratus_msg.task_code = TKT_TASK_TICKET;
        stratus_msg.mux_id = CONTROLLER_MUX_ADDRESS;
        stratus_msg.ucmc_id = theApp.memory_dispenser_config.ucmc_id;
        stratus_msg.message_length = sizeof(fill_values) + STRATUS_HEADER_SIZE;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.club_code = theApp.memory_location_config.club_code;
        stratus_msg.serial_port = 1;
        stratus_msg.customer_id = theApp.memory_location_config.customer_id;
        stratus_msg.data_to_event_log = true;
        memcpy(stratus_msg.data, &fill_values, sizeof(fill_values));
        theApp.QueueStratusTxMessage(&stratus_msg, TRUE);

        if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_CASH_DRAWER)
        {
            if (GetFileAttributes(CASHIER_SHIFT_1) == 0xFFFFFFFF)
                memset(&file_cashier_shift, 0, sizeof(file_cashier_shift));
            else
                fileRead(CASHIER_SHIFT_1, 0, &file_cashier_shift, sizeof(file_cashier_shift));

            if (file_cashier_shift.cashier_id)
                file_cashier_shift.active_shift_fills += total_redeem;
            else
                file_cashier_shift.pre_shift_fills += total_redeem;

            fileWrite(CASHIER_SHIFT_1, 0, &file_cashier_shift, sizeof(file_cashier_shift));
        }

        theApp.PrinterData("End of Fill Report\n\n");
    }

    theApp.printer_busy = false;

    theApp.fill_in_progress = FALSE;

}

// TYPE 1 = dispenser fill
// TYPE 2 = breakage
void CControllerApp::BalanceBreakage(BYTE type, DWORD amount)
{
 CString prt_buf;
 FileCashAdjustment file_cash_adjustment;

    memset(&file_cash_adjustment, 0, sizeof(file_cash_adjustment));
    file_cash_adjustment.type = type;
    file_cash_adjustment.amount = amount;
    file_cash_adjustment.time_stamp = time(NULL);
    fileWrite(ADJUSTMENT_RECORDS, -1, &file_cash_adjustment, sizeof(file_cash_adjustment));

    theApp.PrinterData("\n");
    theApp.PrinterData(PRINTER_BIG_PRINT);

    switch (type)
    {
        case 1:
            if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_CASH_DRAWER)
                theApp.PrinterData(" CASH DRAWER FILL\n");
            else
                theApp.PrinterData(" DISPENSER FILL\n");
            break;

        case 2:
            theApp.PrinterData(" BREAKAGE\n");
            break;
    }

    prt_buf.Format(" AMOUNT: %9.2f\n\n", (double)amount / 100);
    theApp.PrinterData(prt_buf.GetBuffer(0));

    theApp.PrinterData(PRINTER_NORMAL_PRINT);
    theApp.PrinterData("\n\nx_______________________________________\n");
    theApp.PrinterData("   Cashier Signature\n\n\n");
    theApp.PrinterData("x_______________________________________\n");
    theApp.PrinterData("   Supervisor Signature\n");
    theApp.PrinterData(PRINTER_FORM_FEED);
}

void CControllerApp::addPaidTicketsTotal (DWORD ucmc_id, DWORD ticket_amount)
{
    int i = 0;
    bool game_found = FALSE;

    while (!game_found)
    {
        if (paid_per_game[i].ucmc_id)
        {
            if (paid_per_game[i].ucmc_id == ucmc_id)
            {
                paid_per_game[i].ticket_amount = paid_per_game[i].ticket_amount + ticket_amount;
                game_found = TRUE;
            }
        }
        else
        {
            paid_per_game[i].ucmc_id = ucmc_id;
            paid_per_game[i].ticket_amount = ticket_amount;
            game_found = TRUE;
        }

        i++;
    }
}

void CControllerApp::clearPaidTicketsTotal (void)
{
    memset(paid_per_game, 0, sizeof(TotalPaidPerGame) * MAX_GAMES);
}

void CControllerApp::SendMuxMessagesFile(CCustomerControlClient *socket)
{
 int file_index = 0;
 SocketMessage client_message;

    memset(&client_message, 0, sizeof(client_message));
    client_message.head.command = LVIEW_GET_MUX_MESSAGES_FILE;
    client_message.head.data_length = sizeof(MuxFileMessages);

    while (fileRead(MUX_MESSAGES, file_index, client_message.data, client_message.head.data_length))
    {
        socket->client_message_queue.push(client_message);
        file_index++;
    }
}

void CControllerApp::LviewSendGameInfo(CCustomerControlClient *socket, bool location_setup)
{
 bool first_game;
 SocketMessage client_message;
 CGameDevice *pGameDevice;
 CGameCommControl *pGameCommControl = pHeadGameCommControl;

    if (!location_setup)
    {
        memset(&client_message, 0, sizeof(client_message));
        client_message.head.command = LVIEW_CONTROLLER_GAME_INFO_START_LIST;
        socket->client_message_queue.push(client_message);
    }
    else
        first_game = true;

    while (pGameCommControl)
    {
        pGameDevice = pGameCommControl->pHeadGameDevice;

        while (pGameDevice)
        {
            memset(&client_message, 0, sizeof(client_message));

            if (!location_setup)
            {
                client_message.head.command = LVIEW_GAME_INFO_GENERAL;
                client_message.head.data_length = sizeof(pGameDevice->memory_game_config);
                memcpy(client_message.data, &pGameDevice->memory_game_config, sizeof(pGameDevice->memory_game_config));
            }
            else
            {
                if (!first_game)
                    client_message.data[0] = 0;
                else
                {
                    first_game = false;
                    client_message.data[0] = 1;
                }

                client_message.head.command = LVIEW_GET_GAME_FILE_DATA;
                client_message.head.data_length = sizeof(pGameDevice->memory_game_config) + 1;
                memcpy(&client_message.data[1], &pGameDevice->memory_game_config, sizeof(pGameDevice->memory_game_config));
            }

            socket->client_message_queue.push(client_message);
            pGameDevice = pGameDevice->pNextGame;
        }

        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }

    if (!location_setup)
    {
        memset(&client_message, 0, sizeof(client_message));
        client_message.head.command = LVIEW_CONTROLLER_GAME_INFO_END_LIST;
        socket->client_message_queue.push(client_message);
    }
}

void CControllerApp::SendLviewNetworkStats(CCustomerControlClient *socket)  //??????????work
{
 char hostname[MAX_COMPUTERNAME_LENGTH + 1];
 int iii;
 UINT client_port;
 DWORD hostname_length = MAX_COMPUTERNAME_LENGTH;
 SocketMessage send_message;
 CString client_address;

    // clear memo box on lview
    memset(&send_message, 0, sizeof(send_message));
    send_message.head.header = LONG_HEADER_IDENTIFIER;
    send_message.head.command = LVIEW_NETWORK_STATS;
    socket->Send(&send_message, sizeof(send_message.head));

    // local system hostname
    GetComputerName(hostname, &hostname_length);
    memset(&send_message, 0, sizeof(send_message));
    sprintf_s(send_message.data, sizeof(send_message.data), "HOSTNAME: %s", hostname);
    send_message.head.header = LONG_HEADER_IDENTIFIER;
    send_message.head.command = LVIEW_NETWORK_STATS;
    send_message.head.data_length = (DWORD)strlen(send_message.data);
    socket->Send(&send_message, sizeof(send_message.head) + send_message.head.data_length);

    if (pMultiControllerServer)
    {
        for (iii=0; iii < MAXIMUM_MULTI_CONTROLLER_CONNECTIONS; iii++)
        {
            if (pMultiControllerServer->pClientSocket[iii])
            {
                if (!pMultiControllerServer->pClientSocket[iii]->GetPeerName(client_address, client_port))
                {
                    client_address = "0.0.0.0";
                    client_port = 0;
                }

                memset(&send_message, 0, sizeof(send_message));
                sprintf_s(send_message.data, sizeof(send_message.data), "SLAVE CONTROLLER: %s, port %u", client_address.GetBuffer(0), client_port);
                send_message.head.header = LONG_HEADER_IDENTIFIER;
                send_message.head.command = LVIEW_NETWORK_STATS;
                send_message.head.data_length = (DWORD)strlen(send_message.data);
                socket->Send(&send_message, sizeof(send_message.head) + send_message.head.data_length);
            }
        }
    }

    for (iii=0; iii < MAXIMUM_CUSTOMER_CONTROL_CLIENTS; iii++)
    {
        if (customerControlListen.pClient[iii])
        {
            memset(&send_message, 0, sizeof(send_message));
            sprintf_s(send_message.data, sizeof(send_message.data), "CUSTOMER CONTROL CLIENT: %u", customerControlListen.pClient[iii]->client_address);
            send_message.head.header = LONG_HEADER_IDENTIFIER;
            send_message.head.command = LVIEW_NETWORK_STATS;
            send_message.head.data_length = (DWORD)strlen(send_message.data);
            socket->Send(&send_message, sizeof(send_message.head) + send_message.head.data_length);
        }
    }
}

void CControllerApp::ProcessCcClientMemorySettingsRequest(CCustomerControlClient *socket, SocketMessage *message)
{
 CString socket_message_string;
 SocketMessage socket_message;
 FileSettingsIni file_settings_ini, received_settings_ini;
 FileGbProManagerSettings gb_file_settings;

    if (!message->head.data_length)
    {
        if (!fileRead(MAIN_SETTINGS_INI, 0, &file_settings_ini, sizeof(file_settings_ini)))
            memset(&file_settings_ini, 0, sizeof(file_settings_ini));

        memset(&socket_message, 0, sizeof(socket_message));
        socket_message.head.command = CC_CLIENT_MEMORY_SETTINGS;
        socket_message.head.data_length = sizeof(file_settings_ini);
        // copy of the gb pro configurator ip address and port over to be transmitted to Customer Control
        if (!fileRead(GB_PRO_SETTINGS_INI, 0, &gb_file_settings, sizeof(gb_file_settings)))
            memset(&gb_file_settings, 0, sizeof(gb_file_settings));
        if (!memory_settings_ini.masters_ip_address)
        {
            file_settings_ini.memory.gb_pro_configurator_ip_address = gb_file_settings.memory.gb_pro_configurator_ip_address;
            file_settings_ini.memory.gb_pro_configurator_tcp_port = gb_file_settings.memory.gb_pro_configurator_tcp_port;
        }
        else
        {
            file_settings_ini.memory.gb_pro_configurator_ip_address = 0;
            file_settings_ini.memory.gb_pro_configurator_tcp_port = 0;
        }

        memcpy(socket_message.data, &file_settings_ini, socket_message.head.data_length);
        socket->client_message_queue.push(socket_message);
    }
    else if (message->head.data_length == sizeof(file_settings_ini))
    {
        if (!fileRead(MAIN_SETTINGS_INI, 0, &file_settings_ini, sizeof(file_settings_ini)))
            memset(&file_settings_ini, 0, sizeof(file_settings_ini));

        if (!memcmp(message->data, &file_settings_ini, sizeof(file_settings_ini)))
            socket_message_string = "Duplicate settings values, settings not changed.";
        else
        {
            memcpy(&received_settings_ini, message->data, sizeof(received_settings_ini));

            if (!fileWrite(MAIN_SETTINGS_INI, 0, &received_settings_ini, sizeof(received_settings_ini)))
                socket_message_string.Format("Error writing %s.", MAIN_SETTINGS_INI);
            else
            {
                if (!fileRead(GB_PRO_SETTINGS_INI, 0, &gb_file_settings, sizeof(gb_file_settings)))
                    memset(&gb_file_settings, 0, sizeof(gb_file_settings));

                gb_file_settings.memory.gb_pro_configurator_ip_address = received_settings_ini.memory.gb_pro_configurator_ip_address;
                gb_file_settings.memory.gb_pro_configurator_tcp_port = received_settings_ini.memory.gb_pro_configurator_tcp_port;

                if (!fileWrite(GB_PRO_SETTINGS_INI, 0, &gb_file_settings, sizeof(gb_file_settings)))
                    socket_message_string.Format("Error writing %s.", GB_PRO_SETTINGS_INI);

                memcpy(&memory_settings_ini, &received_settings_ini.memory, sizeof(memory_settings_ini));
                socket_message_string = "Successful settings change.";

                // don't allow GB Advantage to be turned on if this is a slave controller
                if (memory_settings_ini.masters_ip_address)
                    file_settings_ini.memory.activate_gb_advantage_support = 0;

                if (file_settings_ini.memory.force_cashier_shifts != received_settings_ini.memory.force_cashier_shifts ||
                    file_settings_ini.memory.printer_comm_port != received_settings_ini.memory.printer_comm_port ||
                    file_settings_ini.memory.stratus_ip_address != received_settings_ini.memory.stratus_ip_address ||
                    file_settings_ini.memory.stratus_tcp_port != received_settings_ini.memory.stratus_tcp_port ||
                    file_settings_ini.memory.cms_ip_address != received_settings_ini.memory.cms_ip_address ||
                    file_settings_ini.memory.cms_tcp_port != received_settings_ini.memory.cms_tcp_port ||
                    file_settings_ini.memory.idc_ip_address != received_settings_ini.memory.idc_ip_address ||
                    file_settings_ini.memory.idc_tcp_port != received_settings_ini.memory.idc_tcp_port ||
                    file_settings_ini.memory.customer_control_server_ip_address != received_settings_ini.memory.customer_control_server_ip_address ||
                    file_settings_ini.memory.customer_control_server_tcp_port != received_settings_ini.memory.customer_control_server_tcp_port ||
                    file_settings_ini.memory.customer_control_client_port != received_settings_ini.memory.customer_control_client_port ||
                    file_settings_ini.memory.masters_ip_address != received_settings_ini.memory.masters_ip_address ||
                    file_settings_ini.memory.game_lock_printer_ip_address != received_settings_ini.memory.game_lock_printer_ip_address ||
                    file_settings_ini.memory.activate_gb_advantage_support != received_settings_ini.memory.activate_gb_advantage_support ||
                    file_settings_ini.memory.marker_printer_ip_address != received_settings_ini.memory.marker_printer_ip_address ||
                    file_settings_ini.memory.marker_printer_port != received_settings_ini.memory.marker_printer_port ||
                    file_settings_ini.memory.gb_pro_configurator_ip_address != received_settings_ini.memory.gb_pro_configurator_ip_address ||
                    file_settings_ini.memory.gb_pro_configurator_tcp_port != received_settings_ini.memory.gb_pro_configurator_tcp_port)
                        socket_message_string += " System reboot needed.";
            }
        }
    }
    else
        socket_message_string = "Invalid settings message size.";

    if (socket_message_string.GetLength())
    {
        memset(&socket_message, 0, sizeof(socket_message));
        socket_message.head.command = CC_CLIENT_ASCII_STRING;
        socket_message.head.data_length = socket_message_string.GetLength();
        memcpy(socket_message.data, socket_message_string.GetBuffer(0), socket_message_string.GetLength());
        socket->client_message_queue.push(socket_message);
    }
}

void CControllerApp::ProcessCcClientLocationSettingsRequest(CCustomerControlClient *socket, SocketMessage *message)
{
 CString socket_message_string;
 SocketMessage socket_message;
 FileLocationConfig file_location_config, received_location_config;

    if (!message->head.data_length)
    {
        if (!fileRead(LOCATION_INFO, 0, &file_location_config, sizeof(file_location_config)))
            memset(&file_location_config, 0, sizeof(file_location_config));

        memset(&socket_message, 0, sizeof(socket_message));
        socket_message.head.command = CC_CLIENT_LOCATION_SETTINGS;
        socket_message.head.data_length = sizeof(file_location_config);
        memcpy(socket_message.data, &file_location_config, socket_message.head.data_length);
        socket->client_message_queue.push(socket_message);
    }
    else if (message->head.data_length == sizeof(file_location_config))
    {
        if (!fileRead(LOCATION_INFO, 0, &file_location_config, sizeof(file_location_config)))
            memset(&file_location_config, 0, sizeof(file_location_config));

        if (!memcmp(message->data, &file_location_config, sizeof(file_location_config)))
            socket_message_string = "Duplicate location setting values, location settings not changed.";
        else
        {
            memcpy(&received_location_config, message->data, sizeof(received_location_config));
            // make sure strings are null terminated
            received_location_config.memory.name[sizeof(file_location_config.memory.name) - 1] = 0;
            received_location_config.memory.address[sizeof(file_location_config.memory.address) - 1] = 0;

            if (!fileWrite(LOCATION_INFO, 0, &received_location_config, sizeof(received_location_config)))
                socket_message_string.Format("Error writing %s.", LOCATION_INFO);
            else
            {
                memcpy(&memory_location_config, &received_location_config.memory, sizeof(memory_location_config));
                socket_message_string = "Successful location settings change.";

                if (file_location_config.memory.baud_rate != received_location_config.memory.baud_rate)
					socket_message_string += " System reboot needed.";
            }
        }
    }
    else
        socket_message_string = "Invalid location settings message size.";

    if (socket_message_string.GetLength())
    {
        memset(&socket_message, 0, sizeof(socket_message));
        socket_message.head.command = CC_CLIENT_ASCII_STRING;
        socket_message.head.data_length = socket_message_string.GetLength();
        memcpy(socket_message.data, socket_message_string.GetBuffer(0), socket_message_string.GetLength());
        socket->client_message_queue.push(socket_message);
    }
}

void CControllerApp::ProcessCcClientDispenserSettingsRequest(CCustomerControlClient *socket, SocketMessage *message)
{
 CString socket_message_string;
 SocketMessage socket_message;
 FileDispenserConfig file_dispenser_config, received_dispenser_config;

    if (!message->head.data_length)
    {
        if (!fileRead(DISPENSER_INFO, 0, &file_dispenser_config, sizeof(file_dispenser_config)))
            memset(&file_dispenser_config, 0, sizeof(file_dispenser_config));

        memset(&socket_message, 0, sizeof(socket_message));
        socket_message.head.command = CC_CLIENT_DISPENSER_SETTINGS;
        socket_message.head.data_length = sizeof(file_dispenser_config);
        memcpy(socket_message.data, &file_dispenser_config, socket_message.head.data_length);
        socket->client_message_queue.push(socket_message);
    }
    else if (message->head.data_length == sizeof(file_dispenser_config))
    {
        if (!fileRead(DISPENSER_INFO, 0, &file_dispenser_config, sizeof(file_dispenser_config)))
            memset(&file_dispenser_config, 0, sizeof(file_dispenser_config));

        if (!memcmp(message->data, &file_dispenser_config, sizeof(file_dispenser_config)))
            socket_message_string = "Duplicate dispenser setting values, settings not changed.";
        else
        {
            memcpy(&received_dispenser_config, message->data, sizeof(received_dispenser_config));
            if (!fileWrite(DISPENSER_INFO, 0, &received_dispenser_config, sizeof(received_dispenser_config)))
                socket_message_string.Format("Error writing %s.", DISPENSER_INFO);
            else
            {
                memcpy(&memory_dispenser_config, &received_dispenser_config.memory, sizeof(memory_dispenser_config));
                socket_message_string = "Successful dispenser settings change.";

                if (file_dispenser_config.memory.comm_port != received_dispenser_config.memory.comm_port ||
                    file_dispenser_config.memory.dispenser_type != received_dispenser_config.memory.dispenser_type)
                        socket_message_string += " System reboot needed.";
            }
        }
    }
    else
        socket_message_string = "Invalid dispenser message size.";

    if (socket_message_string.GetLength())
    {
        memset(&socket_message, 0, sizeof(socket_message));
        socket_message.head.command = CC_CLIENT_ASCII_STRING;
        socket_message.head.data_length = socket_message_string.GetLength();
        memcpy(socket_message.data, socket_message_string.GetBuffer(0), socket_message_string.GetLength());
        socket->client_message_queue.push(socket_message);
    }
}

void CControllerApp::ProcessCcClientMachineRxTxListRequest(CCustomerControlClient *socket)
{
 SocketMessage socket_message;
 CGameCommControl *pGameCommControl = pHeadGameCommControl;

    memset(&socket_message, 0, sizeof(socket_message));
    socket_message.head.command = CC_CLIENT_MACHINE_RX_TX_LIST;

    while (pGameCommControl)
    {
        memcpy(&socket_message.data[socket_message.head.data_length], &pGameCommControl->port_info, sizeof(pGameCommControl->port_info));
        socket_message.head.data_length += sizeof(pGameCommControl->port_info);
        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }

    socket->client_message_queue.push(socket_message);
}

void CControllerApp::ProcessCcClientGameRecordRequest(CCustomerControlClient *socket, SocketMessage *message)
{
 int file_index;
 DWORD ucmc_id;
 CString socket_message_string;
 SocketMessage socket_message;
 FileGameConfig file_game_config, received_game_config;
 CGameDevice *pGameDevice;

    if (!message->head.data_length)
    {
        memset(&socket_message, 0, sizeof(socket_message));
        socket_message.head.command = CC_CLIENT_CLEAR_GAME_RECORDS;
        socket->client_message_queue.push(socket_message);

        file_index = 0;
        while (fileRead(GAME_INFO, file_index, &file_game_config, sizeof(file_game_config)))
        {
            file_index++;
            memset(&socket_message, 0, sizeof(socket_message));
            socket_message.head.command = CC_CLIENT_GAME_RECORD;
            socket_message.head.data_length = sizeof(file_game_config);
            memcpy(socket_message.data, &file_game_config, socket_message.head.data_length);
            socket->client_message_queue.push(socket_message);
        }

        memset(&socket_message, 0, sizeof(socket_message));
        socket_message.head.command = CC_CLIENT_TOTAL_GAME_RECORDS;
        socket_message.head.data_length = 4;
        memcpy(socket_message.data, &file_index, 4);
        socket->client_message_queue.push(socket_message);
    }
    else if (message->head.data_length == 4)
    {
        memcpy(&ucmc_id, message->data, 4);

        if (!RemoveGame(&ucmc_id))
            socket_message_string = "Error removing game.";
        else
            socket_message_string = "Successful game remove.";
    }
    else if (message->head.data_length == sizeof(file_game_config))
    {
        memcpy(&received_game_config, message->data, sizeof(received_game_config));
        file_index = fileGetRecord(GAME_INFO, &file_game_config, sizeof(file_game_config), received_game_config.memory.ucmc_id);

        if (file_index < 0)
        {
            received_game_config.memory.mux_dip_switches = 0;
            received_game_config.memory.version = 0;
            received_game_config.memory.sub_version = 0;
            received_game_config.memory.collection_state = 0;

            switch (AddGameDevice(&received_game_config.memory))
            {
                case 0:
                    if (!fileWrite(GAME_INFO, -1, &received_game_config, sizeof(received_game_config)))
                        socket_message_string.Format("Error writing %s.", GAME_INFO);
                    else
                        socket_message_string = "Successful game add.";
                    break;


                case 1:
                    socket_message_string = "Game add invalid game port, ucmc id or mux id.";
                    break;

                case 2:
                    socket_message_string = "Game add duplicate ucmc id.";
                    break;

                case 3:
                    socket_message_string = "Game add duplicate mux id on port.";
                    break;

                case 4:
                    socket_message_string = "Game add invalid ip address match for port.";
                    break;

                case 5:
                    socket_message_string = "Game add duplicate USB RS232 PORT ID.";
                    break;

                default:
                    socket_message_string = "Unknown error adding game.";
                    break;
            }
        }
        else
        {
            if (!memcmp(message->data, &file_game_config, sizeof(received_game_config)))
                socket_message_string = "Duplicate setting values, settings not changed.";
            else if (file_game_config.memory.ucmc_id != received_game_config.memory.ucmc_id)
                socket_message_string = "Game modify mismatch ucmc ids.";
            else if (file_game_config.memory.ip_address != received_game_config.memory.ip_address)
                socket_message_string = "Game modify mismatch ip address.";
            else if (file_game_config.memory.port_number != received_game_config.memory.port_number)
                socket_message_string = "Game modify mismatch port number.";
            else if (file_game_config.memory.mux_id != received_game_config.memory.mux_id)
                socket_message_string = "Game modify mismatch mux id.";
            else
            {
                pGameDevice = GetGamePointer(received_game_config.memory.ucmc_id);
                if (!pGameDevice)
                    socket_message_string = "Error getting game pointer.";
                else if (!fileWrite(GAME_INFO, file_index, &received_game_config, sizeof(received_game_config)))
                    socket_message_string = "Error writing modified game to file.";
                else
                {
                    socket_message_string = "Successful game modify.";
                    memcpy(&pGameDevice->memory_game_config, &received_game_config.memory, sizeof(pGameDevice->memory_game_config));

                    if (file_game_config.memory.collection_state != received_game_config.memory.collection_state)
                        pGameDevice->SetGameCollectionState(received_game_config.memory.collection_state);
				}
            }
        }
    }
    else
        socket_message_string = "Invalid message size.";

    if (socket_message_string.GetLength())
    {
        memset(&socket_message, 0, sizeof(socket_message));
        socket_message.head.command = CC_CLIENT_ASCII_STRING;
        socket_message.head.data_length = socket_message_string.GetLength();
        memcpy(socket_message.data, socket_message_string.GetBuffer(0), socket_message_string.GetLength());
        socket->client_message_queue.push(socket_message);
    }
}

void CControllerApp::ProcessCustomerControlClientReceive(CCustomerControlClient *socket, SocketMessage *message)
{
 CString message_log;

    if (message->head.command == CC_CLIENT_PASSWORD_PROMPT)
        socket->validated_level = VerifyCcClientUserIdAndPassword(socket, (BYTE*)message->data);
    else if (socket->validated_level)
    {
        switch (message->head.command)
        {
            case CC_CLIENT_REBOOT_CONTROLLER:
                ProcessExitProgramRequest(true);
                break;

            case CC_CLIENT_MEMORY_SETTINGS:
                ProcessCcClientMemorySettingsRequest(socket, message);
                break;

            case CC_CLIENT_LOCATION_SETTINGS:
                ProcessCcClientLocationSettingsRequest(socket, message);
                break;

            case CC_CLIENT_DISPENSER_SETTINGS:
                ProcessCcClientDispenserSettingsRequest(socket, message);
                BroadcastDispenserPayoutLimit();
                break;

            case CC_CLIENT_GAME_RECORD:
                ProcessCcClientGameRecordRequest(socket, message);
                break;

            case CC_CLIENT_MACHINE_RX_TX_LIST:
                ProcessCcClientMachineRxTxListRequest(socket);
                break;

            case CC_CLIENT_MACHINE_PORT_DISPLAY:
                if (message->head.data_length == 2)
                    memcpy(&socket->client_data_display.port_number, message->data, 2);
                else
                    socket->client_data_display.port_number = 0;
                break;
#if 0
???????????????????????????????work
            case LVIEW_NETWORK_STATS:
                SendLviewNetworkStats(socket);
                break;

            case LVIEW_SERIAL_DISPLAY:
                if (socket->validated_level)
                    memcpy(&socket->client_data_display.port_number, message->data, 2);
                break;

            case LVIEW_DISPENSER_INFO_DISPLAY_MESSAGES:
                if (socket->validated_level)
                {
                    if (message->data[0])
                        socket->client_data_display.dispenser_data_text = false;
                    else
                        socket->client_data_display.dispenser_data_text = true;
                }
                break;

            case LVIEW_DISPENSER_INFO_DISPLAY_RAW_MESSAGES:
                if (socket->validated_level)
                {
                    if (message->data[0])
                        socket->client_data_display.dispenser_data_raw = false;
                    else
                        socket->client_data_display.dispenser_data_raw = true;
                }
                break;

            case LVIEW_GET_LOCATION_FILE_DATA:
                memset(&send_message, 0, sizeof(send_message));
                send_message.head.header = LONG_HEADER_IDENTIFIER;
                send_message.head.command = LVIEW_GET_LOCATION_FILE_DATA;
                send_message.head.data_length = sizeof(memory_location_config);
                memcpy(send_message.data, &memory_location_config, sizeof(memory_location_config));
                socket->Send(&send_message, sizeof(send_message.head) + send_message.head.data_length);
                break;

            case LVIEW_GET_DISPENSER_FILE_DATA:
                memset(&send_message, 0, sizeof(send_message));
                send_message.head.header = LONG_HEADER_IDENTIFIER;
                send_message.head.command = LVIEW_GET_DISPENSER_FILE_DATA;
                send_message.head.data_length = sizeof(memory_dispenser_config);
                memcpy(send_message.data, &memory_dispenser_config, sizeof(memory_dispenser_config));
                socket->Send(&send_message, sizeof(send_message.head) + send_message.head.data_length);
                break;

            case LVIEW_GET_GAME_FILE_DATA:
                LviewSendGameInfo(socket, true);
                break;

            case LVIEW_GAME_INFO_GENERAL:
                if (socket->validated_level >= SUPERVISOR)
                    LviewSendGameInfo(socket, false);
                break;

            case LVIEW_CLEAR_MUX_MESSAGES_FILE:
                if (socket->validated_level == ENGINEERING && GetFileAttributes(MUX_MESSAGES) != 0xFFFFFFFF)
                {
                    DeleteFile(MUX_MESSAGES_BAK);
                    MoveFile(MUX_MESSAGES, MUX_MESSAGES_BAK);
                }
                break;

            case LVIEW_GET_MUX_MESSAGES_FILE:
                if (socket->validated_level == ENGINEERING)
                    SendMuxMessagesFile(socket);
                break;

            case LVIEW_ADD_TO_MUX_MESSAGES_FILE:
                if (socket->validated_level == ENGINEERING && message->head.data_length == sizeof(MuxFileMessages))
                {
                    fileWrite(MUX_MESSAGES, -1, message->data, sizeof(MuxFileMessages));
                    GameMuxMessageUpdate();
                }
                break;

            case LVIEW_DISPENSER_INFO_GENERAL:
                if (socket->validated_level)
                    SendDispenserInfo(socket);
                break;

            case LVIEW_DISPENSER_INFO_CLEAR_ERRORS:
                if (socket->validated_level >= SUPERVISOR)
                {
                    memset(&dispenserControl.jcm_dispenser_errors, 0, sizeof(dispenserControl.jcm_dispenser_errors));
                    memset(&dispenserControl.ecash_dispenser_errors, 0, sizeof(dispenserControl.ecash_dispenser_errors));
                    logMessage("CControllerApp::ProcessCustomerControlClientReceive - dispenser errors cleared.");
                }
                break;

            case LVIEW_GAME_INFO_SET_COLLECTION_STATE:
                if (socket->validated_level >= SUPERVISOR)
                {
                    // set collection state
                    if (message->data[0] >= CLEAR_COLLECTION_STATE && message->data[0] <= CANCEL_COLLECTION_STATE)
                    {
                        pGameDevice = GetGamePointer(*(DWORD*)&message->data[1]);

                        if (pGameDevice)
                        {
                            pGameDevice->SetGameCollectionState(message->data[0]);
                            logMessage("CControllerApp::ProcessCustomerControlClientReceive - changing collection state.");
                        }
                    }
                }
                break;

            case LVIEW_GAME_INFO_START_OR_STOP_POLLING:
                if (socket->validated_level >= SUPERVISOR)
                {
                    // start/stop polling
                    if (!message->data[0] || message->data[0] == 1)
                    {
                        pGameDevice = GetGamePointer(*(DWORD*)&message->data[1]);

                        if (pGameDevice)
                        {
                            pGameDevice->memory_game_config.stop_polling = message->data[0];
                            pGameDevice->WriteModifiedGameConfig();
                            logMessage("CControllerApp::ProcessCustomerControlClientReceive - changing game polling flag.");
                        }
                    }
                }
                break;

            case LVIEW_GAME_INFO_RESET_GAME:
                if (socket->validated_level >= SUPERVISOR)
                {
                    pGameDevice = GetGamePointer(*(DWORD*)message->data);
                    if (pGameDevice)
                    {
                        pGameDevice->ResetGame();
                        logMessage("CControllerApp::ProcessCustomerControlClientReceive - reset game.");
                    }
                }
                break;

            case LVIEW_GAME_INFO_SERIAL_ERRORS:
                if (socket->validated_level >= SUPERVISOR)
                {
                    pGameCommControl = pHeadGameCommControl;

                    while (pGameCommControl)
                    {
                        if (*(WORD*)&message->data == pGameCommControl->port_info.port_number)
                        {
                            // get or clear serial errors
                            if (message->data[2] == 1)
                            {
                                memset(&pGameCommControl->message_errors, 0, sizeof(pGameCommControl->message_errors));
                                logMessage("CControllerApp::ProcessCustomerControlClientReceive - clearing port message errors.");
                            }
                            else if (!message->data[2])
                            {
                                memset(&send_message, 0, sizeof(send_message));
                                send_message.head.header = LONG_HEADER_IDENTIFIER;
                                send_message.head.command = LVIEW_GAME_INFO_SERIAL_ERRORS;
                                send_message.head.data_length = sizeof(pGameCommControl->message_errors);
                                memcpy(send_message.data, &pGameCommControl->message_errors, send_message.head.data_length);
                                socket->Send(&send_message, sizeof(send_message.head) + send_message.head.data_length);
                            }

                            pGameCommControl = NULL;
                        }
                        else
                            pGameCommControl = pGameCommControl->pNextGameCommControl;
                    }
                }
                break;

            default:
                logMessage("CControllerApp::ProcessCustomerControlClientReceive - invalid command.");
                break;
#endif


            default:
                message_log.Format("CControllerApp::ProcessCustomerControlClientReceive - invalid command: %u.", message->head.command);
                break;
        }
    }
}

void CControllerApp::ProcessSlaveTicketPaid(MemoryPaidTicket *memory_paid_ticket)
{
 int record_position;
 FilePaidTicket file_paid_ticket;
 FileGameRedeemRecord redeem_record;
 FileDropTicket drop_ticket;
 FilePendingTickets pending_ticket;
 FileCashierShift file_cashier_shift;
 CString prt_buf;

    memset(&file_paid_ticket, 0, sizeof(file_paid_ticket));
    memcpy(&file_paid_ticket.memory, memory_paid_ticket, sizeof(file_paid_ticket.memory));

    prt_buf = CASHIER_SHIFT_1;

    if (!fileRead(prt_buf.GetBuffer(0), 0, &file_cashier_shift, sizeof(file_cashier_shift)))
        memset(&file_cashier_shift, 0, sizeof(file_cashier_shift));

    if (!memory_paid_ticket->memory_game_lock.jackpot_to_credit_flag)
        file_cashier_shift.redeem_amount += memory_paid_ticket->memory_game_lock.ticket_amount;
    file_cashier_shift.number_tickets++;
    if (!fileWrite(prt_buf.GetBuffer(0), 0, &file_cashier_shift, sizeof(file_cashier_shift)))
        logMessage("CControllerApp::ProcessSlaveTicketPaid - error writing CASHIER_SHIFT_1 file.");

    fileWrite(PAID_TICKETS_TX, -1, &file_paid_ticket, sizeof(file_paid_ticket));
    writeRedundantFile((CString)PAID_TICKETS, -1, (BYTE*)&file_paid_ticket, sizeof(file_paid_ticket));
    fileWriteSizeLimited(DATED_PAID_TICKETS, &file_paid_ticket, sizeof(file_paid_ticket));

    if (!file_paid_ticket.memory.memory_game_lock.jackpot_to_credit_flag &&
        file_paid_ticket.memory.memory_game_lock.ticket_amount > file_paid_ticket.memory.memory_game_lock.marker_balance)
    {
        record_position = fileGetRecord(GAME_REDEEM_RECORDS, &redeem_record, sizeof(redeem_record), file_paid_ticket.memory.memory_game_lock.ucmc_id);
        if (record_position >= 0)
        {
            redeem_record.amount += file_paid_ticket.memory.memory_game_lock.ticket_amount - file_paid_ticket.memory.memory_game_lock.marker_balance;
            writeRedundantFile((CString)GAME_REDEEM_RECORDS, record_position, (BYTE*)&redeem_record, sizeof(redeem_record));
        }
        else
        {
            memset(&redeem_record, 0, sizeof(redeem_record));
            redeem_record.ucmc_id = file_paid_ticket.memory.memory_game_lock.ucmc_id;
            redeem_record.amount = file_paid_ticket.memory.memory_game_lock.ticket_amount - file_paid_ticket.memory.memory_game_lock.marker_balance;
            writeRedundantFile((CString)GAME_REDEEM_RECORDS, -1, (BYTE*)&redeem_record, sizeof(redeem_record));
        }

        memset(&drop_ticket, 0, sizeof(drop_ticket));
        drop_ticket.ticket_id = file_paid_ticket.memory.memory_game_lock.ticket_id;
        drop_ticket.ucmc_id = file_paid_ticket.memory.memory_game_lock.ucmc_id;
        drop_ticket.amount = file_paid_ticket.memory.memory_game_lock.ticket_amount - file_paid_ticket.memory.memory_game_lock.marker_balance;
        drop_ticket.dispenser_ucmc_id = file_paid_ticket.memory.dispenser_ucmc_id;

        drop_ticket.customer_id = file_paid_ticket.memory.memory_game_lock.location_id;

        writeRedundantFile((CString)DROP_TICKETS, -1, (BYTE*)&drop_ticket, sizeof(drop_ticket));
    }

    record_position = fileGetRecord(PENDING_TICKETS, &pending_ticket, sizeof(pending_ticket), file_paid_ticket.memory.memory_game_lock.ticket_id);
    if (record_position < 0)
        logMessage("CControllerApp::ProcessSlaveTicketPaid - error finding ticket in PENDING_TICKETS.");
    else
    {
        fileDeleteRecord(PENDING_TICKETS, sizeof(pending_ticket), record_position);
        flash_active_home_light = FALSE;
    }
}

void CControllerApp::ProcessSlaveGameLockQuery(CMultiControllerClient *socket)
{
 BYTE slave_tx_buffer[TCPIP_MESSAGE_BUFFER_SIZE], number_of_game_locks = 0;
 WORD packet_crc, message_length;
 StratusMessageFormat slave_msg;
 StratusProtocolHeader *tx_header;
 EventMessage event_message;
 FileGameLock file_game_lock;
 DisplayGameLock display_game_lock[MAXIMUM_GAME_LOCKS_LIST];
 HANDLE hSearch;
 CString search_string, filename;
 WIN32_FIND_DATA find_return;

    memset(&slave_msg, 0, sizeof(slave_msg));
    memset(&display_game_lock, 0, sizeof(display_game_lock));

    search_string.Format("%s\\*", GAME_LOCKS_PATH);
    hSearch = FindFirstFile(search_string, &find_return);

    if (hSearch != INVALID_HANDLE_VALUE)
    {
        filename.Format("%s\\%s", GAME_LOCKS_PATH, find_return.cFileName);

        if (fileRead(filename.GetBuffer(0), 0, &file_game_lock, sizeof(file_game_lock)))
        {
            display_game_lock[number_of_game_locks].ucmc_id = file_game_lock.game_lock.ucmc_id;
            display_game_lock[number_of_game_locks].ticket_amount = file_game_lock.game_lock.ticket_amount;
            display_game_lock[number_of_game_locks].marker_balance = file_game_lock.game_lock.marker_balance;
            display_game_lock[number_of_game_locks].game_port = file_game_lock.game_lock.game_port;
            display_game_lock[number_of_game_locks].mux_id = file_game_lock.game_lock.mux_id;
            display_game_lock[number_of_game_locks].jackpot_to_credit_flag = file_game_lock.game_lock.jackpot_to_credit_flag;
            display_game_lock[number_of_game_locks].sas_id = file_game_lock.game_lock.sas_id;
            number_of_game_locks++;
        }

		// Stratus message can only hold 0xFF bytes
        while (number_of_game_locks < 15 && FindNextFile(hSearch, &find_return))
        {
            filename.Format("%s\\%s", GAME_LOCKS_PATH, find_return.cFileName);

            if (fileRead(filename.GetBuffer(0), 0, &file_game_lock, sizeof(file_game_lock)))
            {
                display_game_lock[number_of_game_locks].ucmc_id = file_game_lock.game_lock.ucmc_id;
                display_game_lock[number_of_game_locks].ticket_amount = file_game_lock.game_lock.ticket_amount;
                display_game_lock[number_of_game_locks].marker_balance = file_game_lock.game_lock.marker_balance;
                display_game_lock[number_of_game_locks].game_port = file_game_lock.game_lock.game_port;
                display_game_lock[number_of_game_locks].mux_id = file_game_lock.game_lock.mux_id;
                display_game_lock[number_of_game_locks].jackpot_to_credit_flag = file_game_lock.game_lock.jackpot_to_credit_flag;
                display_game_lock[number_of_game_locks].sas_id = file_game_lock.game_lock.sas_id;
                number_of_game_locks++;
            }
        }

        FindClose(hSearch);
    }

    message_length = sizeof(display_game_lock[0]) * number_of_game_locks + STRATUS_HEADER_SIZE;

    slave_msg.send_code = SLAVE_TKT_GAME_LOCK_QUERY;
    slave_msg.task_code = SLAVE_TKT_TASK_TICKET;
    slave_msg.property_id = memory_location_config.property_id;
    slave_msg.message_length = message_length;
    slave_msg.data[0] = number_of_game_locks;
	memcpy(&slave_msg.data[1], &display_game_lock, message_length);

    endian2(slave_msg.send_code);
    endian2(slave_msg.task_code);
    endian2(slave_msg.property_id);
    endian2(slave_msg.message_length);

    memset(slave_tx_buffer, 0, sizeof(slave_tx_buffer));
    memcpy(&slave_tx_buffer[sizeof(StratusProtocolHeader)], &slave_msg, message_length);
    message_length += sizeof(StratusProtocolHeader);
    tx_header = (StratusProtocolHeader*)slave_tx_buffer;
    tx_header->header = PACKET_IDENTIFIER;
    tx_header->length = message_length;
    endian2(tx_header->length);

    packet_crc = CalculateStratusCrc(slave_tx_buffer, message_length);
    endian2(packet_crc);
    memcpy(&slave_tx_buffer[message_length], &packet_crc, 2);
    message_length += 2;

    if (socket->Send(slave_tx_buffer, message_length) != message_length)
    {
        logMessage("CControllerApp::ProcessSlaveGameLockQuery - problem sending data, disconnecting.");
        if (socket->m_hSocket != INVALID_SOCKET)
            socket->Close();
        socket->delete_socket = true;
    }
    else
    {
        memset(&event_message, 0, sizeof(event_message));
        event_message.head.time_stamp = time(NULL);
        event_message.head.event_type = SLAVE_TRANSMIT_MESSAGES;
        event_message.head.data_length = message_length;
        memcpy(event_message.data, slave_tx_buffer, message_length);
        event_message_queue.push(event_message);
    }
}

void CControllerApp::ProcessSlaveTicketQuery(CMultiControllerClient *socket, char *data)
{
 char filename[MAX_PATH];
 BYTE slave_tx_buffer[TCPIP_MESSAGE_BUFFER_SIZE];
 WORD packet_crc, message_length;
 int file_index;
 DWORD ucmc_id, ticket_amount, slave_marker_amount, user_login_id;
 FileGameLock file_game_lock;
 StratusMessageFormat slave_msg;
 FilePaidTicket file_paid_ticket;
 FilePendingTickets pending_ticket;
 StratusProtocolHeader *tx_header;
 EventMessage event_message;

    memset(&file_game_lock, 0, sizeof(file_game_lock));
    memcpy(&ucmc_id, data, 4);
    memcpy(&ticket_amount, &data[4], 4);
    memcpy(&user_login_id, &data[18], 4);

    if (user_login_id != current_shift_user.user.id)
        file_game_lock.game_lock.status = INVALID_USER_LOGIN_ID_ASCII;
    else
    {
        if (!GetGameLockFilename(ucmc_id, ticket_amount, filename))
            file_game_lock.game_lock.status = TICKET_ALREADY_PAID_ASCII;
        else
        {
            if (!fileRead(filename, 0, &file_game_lock, sizeof(file_game_lock)))
                file_game_lock.game_lock.status = STRATUS_FILE_ERROR_ASCII_2;
            else
            {
                if (ucmc_id != file_game_lock.game_lock.ucmc_id || ticket_amount != file_game_lock.game_lock.ticket_amount)
                    file_game_lock.game_lock.status = INVALID_TICKET_STATUS_ASCII;
                else
                {
                    DWORD attributes = GetFileAttributes(filename);

                    if (attributes == INVALID_FILE_ATTRIBUTES)
                        file_game_lock.game_lock.status = STRATUS_FILE_ERROR_ASCII_2;
                    else
                    {

                        file_index = 0;
                        while (fileRead(PAID_TICKETS, file_index, &file_paid_ticket, sizeof(file_paid_ticket)))
                        {
                            if (file_game_lock.game_lock.ucmc_id == file_paid_ticket.memory.memory_game_lock.ucmc_id &&
                                file_game_lock.game_lock.ticket_amount == file_paid_ticket.memory.memory_game_lock.ticket_amount &&
                                file_game_lock.game_lock.ticket_id == file_paid_ticket.memory.memory_game_lock.ticket_id)
                            {
                                file_game_lock.game_lock.status = TICKET_ALREADY_PAID_ASCII;
                                break;
                            }
                            else
                                file_index++;
                        }

                        if (!file_game_lock.game_lock.status)
                        {
                            file_index = 0;
                            while (fileRead(DATED_PAID_TICKETS, file_index, &file_paid_ticket, sizeof(file_paid_ticket)))
                            {
                                if (file_game_lock.game_lock.ucmc_id == file_paid_ticket.memory.memory_game_lock.ucmc_id &&
                                    file_game_lock.game_lock.ticket_amount == file_paid_ticket.memory.memory_game_lock.ticket_amount &&
                                    file_game_lock.game_lock.ticket_id == file_paid_ticket.memory.memory_game_lock.ticket_id)
                                {
                                    file_game_lock.game_lock.status = TICKET_ALREADY_PAID_ASCII;
                                    break;
                                }
                                else
                                    file_index++;
                            }
                        }

                        if (!file_game_lock.game_lock.status)
                        {
                            if (fileGetRecord(PENDING_TICKETS, &pending_ticket, sizeof(pending_ticket),
                                file_game_lock.game_lock.ticket_id) >= 0)
                                    file_game_lock.game_lock.status = TICKET_PAY_PENDING_ASCII;
                            else
                            {
                                memset(&pending_ticket, 0, sizeof(pending_ticket));
                                pending_ticket.ticket_id = file_game_lock.game_lock.ticket_id;
                                strcpy_s(pending_ticket.computer_name, sizeof(pending_ticket.computer_name), &data[8]);
                                fileWrite(PENDING_TICKETS, -1, &pending_ticket, sizeof(pending_ticket));

                                file_game_lock.game_lock.status = TICKET_PAYABLE_ASCII;
                            }
                        }
                    }
                }
            }
        }
    }


    message_length = sizeof(file_game_lock.game_lock) + sizeof(slave_marker_amount) + STRATUS_HEADER_SIZE;

    memset(&slave_msg, 0, sizeof(slave_msg));
    slave_msg.send_code = SLAVE_TKT_TICKET_QUERY;
    slave_msg.task_code = SLAVE_TKT_TASK_TICKET;
    slave_msg.message_length = message_length;

    if ((file_game_lock.game_lock.status == TICKET_PAYABLE_ASCII) || (file_game_lock.game_lock.status == TICKET_PAY_PENDING_ASCII) ||
        (file_game_lock.game_lock.status == INVALID_USER_LOGIN_ID_ASCII))
    {
        slave_msg.ucmc_id = file_game_lock.game_lock.ucmc_id;
        slave_msg.mux_id = file_game_lock.game_lock.mux_id;
//        slave_msg.sas_id = file_game_lock.game_lock.sas_id;
        slave_msg.customer_id = file_game_lock.game_lock.location_id;
        slave_msg.serial_port = file_game_lock.game_lock.game_port;
        slave_msg.property_id = memory_location_config.property_id;
        memcpy(slave_msg.data, &file_game_lock.game_lock, sizeof(file_game_lock.game_lock));

		CGameDevice *pGameDevice = GetGamePointer(slave_msg.ucmc_id);

		if (pGameDevice && pGameDevice->account_login_data.account_number == file_game_lock.game_lock.player_account)
			slave_marker_amount = pGameDevice->account_login_data.marker_limit;
		else
			slave_marker_amount = 0;

		memcpy(&slave_msg.data[sizeof(file_game_lock.game_lock)], &slave_marker_amount, sizeof(slave_marker_amount));
    }

    endian2(slave_msg.send_code);
    endian2(slave_msg.task_code);
    endian2(slave_msg.mux_id);
    endian2(slave_msg.property_id);
    endian2(slave_msg.serial_port);
    endian2(slave_msg.message_length);
    endian4(slave_msg.ucmc_id);
    endian4(slave_msg.customer_id);

    memset(slave_tx_buffer, 0, sizeof(slave_tx_buffer));
    tx_header = (StratusProtocolHeader*)slave_tx_buffer;
    tx_header->header = PACKET_IDENTIFIER;
    memcpy(&slave_tx_buffer[sizeof(StratusProtocolHeader)], &slave_msg, message_length);
    message_length += sizeof(StratusProtocolHeader);
    tx_header->length = message_length;
    endian2(tx_header->length);

    packet_crc = CalculateStratusCrc(slave_tx_buffer, message_length);
    endian2(packet_crc);
    memcpy(&slave_tx_buffer[message_length], &packet_crc, 2);
    message_length += 2;

    if (socket->Send(slave_tx_buffer, message_length) != message_length)
    {
        logMessage("CControllerApp::ProcessSlaveTicketQuery - problem sending data, disconnecting.");
        if (socket->m_hSocket != INVALID_SOCKET)
            socket->Close();
        socket->delete_socket = true;
    }
    else
    {
        memset(&event_message, 0, sizeof(event_message));
        event_message.head.time_stamp = time(NULL);
        event_message.head.event_type = SLAVE_TRANSMIT_MESSAGES;
        event_message.head.data_length = sizeof(StratusProtocolHeader) + sizeof(file_game_lock.game_lock) + STRATUS_HEADER_SIZE + 2;
        memcpy(event_message.data, slave_tx_buffer, event_message.head.data_length);
        event_message_queue.push(event_message);
    }
}

void CControllerApp::ProcessSlaveSystemLogin(CMultiControllerClient *socket, SystemUser *user)
{
 BYTE slave_tx_buffer[TCPIP_MESSAGE_BUFFER_SIZE];
 WORD packet_crc;
 StratusProtocolHeader *tx_header = (StratusProtocolHeader*)slave_tx_buffer;
 StratusMessageFormat slave_msg;
 CashierAndManagerUserFile file_user_record;
 EventMessage event_message;

    memset(&slave_msg, 0, sizeof(slave_msg));
    slave_msg.task_code = SLAVE_TKT_TASK_TICKET;
    slave_msg.send_code = SLAVE_TKT_SYSTEM_LOGIN;
    slave_msg.message_length = sizeof(file_user_record.user) + STRATUS_HEADER_SIZE;
    endian2(slave_msg.task_code);
    endian2(slave_msg.send_code);
    endian2(slave_msg.message_length);

    if (fileGetRecord(LOCATION_IDS, &file_user_record, sizeof(file_user_record), user->id) >= 0 &&
        user->password == file_user_record.user.password)
    {
        if (((user->id / 10000 == CASHIER) && theApp.memory_settings_ini.force_cashier_shifts) && !current_shift_user.user.id)
        {
            // if we don't have an active shift, let this cashier user start the new shift
            memcpy(&theApp.current_shift_user, &file_user_record.user, sizeof(theApp.current_shift_user));
            CashierShiftStart(user->id);
        }
        // make sure that the cashier on the slave is using the current shift user id to log on.
//        if (current_shift_user.user.id == file_user_record.user.id)
        if ((user->id / 10000 > CASHIER) || (current_shift_user.user.id == file_user_record.user.id))
            memcpy(slave_msg.data, &file_user_record.user, sizeof(file_user_record.user));

    }

    memset(slave_tx_buffer, 0, sizeof(slave_tx_buffer));
    tx_header->header = PACKET_IDENTIFIER;
    memcpy(&slave_tx_buffer[sizeof(StratusProtocolHeader)], &slave_msg, sizeof(file_user_record.user) + STRATUS_HEADER_SIZE);
    tx_header->length = sizeof(file_user_record.user) + STRATUS_HEADER_SIZE + sizeof(StratusProtocolHeader);
    endian2(tx_header->length);

    // calculate crc
    packet_crc = CalculateStratusCrc(slave_tx_buffer,
        sizeof(file_user_record.user) + STRATUS_HEADER_SIZE + sizeof(StratusProtocolHeader));
    endian2(packet_crc);
    memcpy(&slave_tx_buffer[sizeof(file_user_record.user) + STRATUS_HEADER_SIZE + sizeof(StratusProtocolHeader)],
        &packet_crc, 2);


    if (socket->Send(slave_tx_buffer, sizeof(file_user_record.user) + STRATUS_HEADER_SIZE + sizeof(StratusProtocolHeader) + 2) !=
        sizeof(file_user_record.user) + STRATUS_HEADER_SIZE + sizeof(StratusProtocolHeader) + 2)
    {
        logMessage("CControllerApp::ProcessSlaveSystemLogin - SEND error, closing socket.");

        if (socket->m_hSocket != INVALID_SOCKET)
            socket->Close();

        socket->delete_socket = true;
    }

    memset(&event_message, 0, sizeof(event_message));
    event_message.head.time_stamp = time(NULL);
    event_message.head.event_type = SLAVE_TRANSMIT_MESSAGES;
    event_message.head.data_length = sizeof(file_user_record.user) + STRATUS_HEADER_SIZE + sizeof(StratusProtocolHeader) + 2;
    memcpy(event_message.data, slave_tx_buffer, sizeof(file_user_record.user) + STRATUS_HEADER_SIZE + sizeof(StratusProtocolHeader) + 2);
    event_message_queue.push(event_message);
}

void CControllerApp::ProcessSlaveControllerReceiveMessage(CMultiControllerClient *socket, StratusMessageFormat *receive_message, WORD message_length)
{
 int record_position;
 DWORD ticket_id;
 CString error_message;
 FilePendingTickets pending_ticket;
 EventMessage event_message;
 StratusMessageFormat device_message;

    if (message_length > sizeof(device_message))
    {
        logMessage("CControllerApp::ProcessSlaveControllerReceiveMessage - message length exceeds maximum size.");
        memcpy(&device_message, receive_message, sizeof(device_message));
    }
    else
    {
        memset(&device_message, 0, sizeof(device_message));
        memcpy(&device_message, receive_message, message_length);
    }

    endian2(device_message.message_length);
    endian2(device_message.task_code);
    endian2(device_message.property_id);
    endian2(device_message.club_code);
    endian2(device_message.serial_port);
    endian2(device_message.mux_id);
    endian2(device_message.send_code);
    endian4(device_message.customer_id);
    endian4(device_message.ucmc_id);

    switch (device_message.task_code)
    {
        case SLAVE_TKT_TASK_TICKET:
            switch (device_message.send_code)
            {
				case SLAVE_TKT_KEEP_ALIVE:
					return;

                case SLAVE_TKT_SYSTEM_LOGIN:
                    ProcessSlaveSystemLogin(socket, (SystemUser*)device_message.data);
                    break;

                case SLAVE_TKT_TICKET_REMOVE:
                    memcpy(&ticket_id, device_message.data, sizeof(ticket_id));
                    record_position = fileGetRecord(PENDING_TICKETS, &pending_ticket, sizeof(pending_ticket), ticket_id);
                    if (record_position < 0)
                    {
                        error_message.Format("CControllerApp::ProcessSlaveControllerReceiveMessage - SLAVE_TKT_TICKET_REMOVE not found: %u.",
                            ticket_id);
                        logMessage(error_message);
                    }
                    else
                    {
                        fileDeleteRecord(PENDING_TICKETS, sizeof(pending_ticket), record_position);
                    }
                    break;

                case SLAVE_TKT_GAME_UNLOCK:
                    GameUnlockQueue game_unlock_message;
                    memcpy(&game_unlock_message, device_message.data, sizeof(game_unlock_message));
                    user_interface_game_unlock_queue.push(game_unlock_message); 
					flash_active_home_light = FALSE;
                    break;

                case SLAVE_TKT_TICKET_QUERY:
                    ProcessSlaveTicketQuery(socket, device_message.data);
                    break;

                case SLAVE_TKT_GAME_LOCK_QUERY:
                    ProcessSlaveGameLockQuery(socket);
                    break;

                case SLAVE_TKT_TICKET_PAID:
                    ProcessSlaveTicketPaid((MemoryPaidTicket*)device_message.data);
					flash_active_home_light = FALSE;
                    break;

                default:
                    error_message.Format("CControllerApp::ProcessSlaveControllerReceiveMessage - invalid SLAVE TICKET send code: %u.",
                        device_message.send_code);
                    logMessage(error_message);
                    return;
            }
            break;

        case MPI_TASK_PLAYER_ACCOUNTING:
            if (device_message.send_code == MPI_MARKER_CREDIT)
                QueueStratusTxMessage(&device_message, FALSE);
            else
            {
                error_message.Format("CControllerApp::ProcessSlaveControllerReceiveMessage - invalid SLAVE MPI send code: %u.",
                    device_message.send_code);
                logMessage(error_message);
                return;
            }
            break;

        case TKT_TASK_TICKET:
            switch (device_message.send_code)
            {
                case TKT_JACKPOT_CREDIT:
                case TKT_TICKET_PAID:
                case TKT_FILL_MESSAGE:
                case TKT_SEND_BILL_COUNT:
                    QueueStratusTxMessage(&device_message, FALSE);
                    break;

                default:
                    error_message.Format("CControllerApp::ProcessSlaveControllerReceiveMessage - invalid TICKET send code: %u.",
                        device_message.send_code);
                    logMessage(error_message);
                    return;
            }
            break;

        default:
            error_message.Format("CControllerApp::ProcessSlaveControllerReceiveMessage - invalid task code: %u.", device_message.task_code);
            logMessage(error_message);
            return;
    }

    // log Slave messages
    memset(&event_message, 0, sizeof(event_message));
    event_message.head.time_stamp = time(NULL);
    event_message.head.event_type = SLAVE_RECEIVE_MESSAGES;
    event_message.head.data_length = message_length;

    if (message_length > sizeof(StratusMessageFormat))
        memcpy(event_message.data, &device_message, sizeof(StratusMessageFormat));
    else
        memcpy(event_message.data, &device_message, message_length);

    event_message_queue.push(event_message);
}

void CControllerApp::CheckMultiControllerClientReceive()
{
 WORD calculated_crc, packet_crc;
 int iii, bytes_received, packet_size;
 StratusProtocolHeader header;

    for (iii=0; iii < MAXIMUM_MULTI_CONTROLLER_CONNECTIONS; iii++)
    {
        if (pMultiControllerServer->pClientSocket[iii] && !pMultiControllerServer->pClientSocket[iii]->delete_socket)
        {
            bytes_received = pMultiControllerServer->pClientSocket[iii]->GetBuffer(
                &pMultiControllerServer->pClientSocket[iii]->rx_buffer.data[pMultiControllerServer->pClientSocket[iii]->rx_buffer.index],
                TCPIP_MESSAGE_BUFFER_SIZE * 4 - pMultiControllerServer->pClientSocket[iii]->rx_buffer.index);

            if (bytes_received)
            {
                pMultiControllerServer->pClientSocket[iii]->rx_buffer.index += bytes_received;

                while (pMultiControllerServer->pClientSocket[iii]->rx_buffer.index >= sizeof(header))
                {
                    memcpy(&header, pMultiControllerServer->pClientSocket[iii]->rx_buffer.data, sizeof(header));

                    if (header.header != PACKET_IDENTIFIER)
                    {
                        memset(&pMultiControllerServer->pClientSocket[iii]->rx_buffer, 0,
                            sizeof(pMultiControllerServer->pClientSocket[iii]->rx_buffer));
                        logMessage("CControllerApp::CheckMultiControllerClientReceive - incorrect packet header.");
                    }
                    else
                    {
                        endian2(header.length);
                        endian2(header.route_info);
                        packet_size = header.length + 2;    // add crc bytes

                        if (pMultiControllerServer->pClientSocket[iii]->rx_buffer.index < packet_size)
                            return; // full message not received yet
                        else
                        {
                            // check crc
                            memcpy(&packet_crc, &pMultiControllerServer->pClientSocket[iii]->rx_buffer.data[header.length], 2);
                            endian2(packet_crc);
                            calculated_crc = CalculateStratusCrc(pMultiControllerServer->pClientSocket[iii]->rx_buffer.data, header.length);

                            if (calculated_crc != packet_crc)
                                logMessage("CControllerApp::CheckMultiControllerClientReceive - CRC mismatch.");
                            else
                            {
                                if (header.route_info)
                                    logMessage("CControllerApp::CheckMultiControllerClientReceive - invalid route info specifier.");
                                else
                                    if (!fetching_netwin_in_progress)
                                    {
                                        ProcessSlaveControllerReceiveMessage(pMultiControllerServer->pClientSocket[iii],
										    (StratusMessageFormat*)&pMultiControllerServer->pClientSocket[iii]->rx_buffer.data[sizeof(header)],
                                            header.length - sizeof(header));
                                    }
                            }

                            if (pMultiControllerServer->pClientSocket[iii]->rx_buffer.index == packet_size)
                                memset(&pMultiControllerServer->pClientSocket[iii]->rx_buffer, 0,
                                    sizeof(pMultiControllerServer->pClientSocket[iii]->rx_buffer));
                            else
                            {
                                memset(pMultiControllerServer->pClientSocket[iii]->rx_buffer.data, 0, packet_size);
                                if (pMultiControllerServer->pClientSocket[iii]->rx_buffer.index >= packet_size)
                                {
                                    pMultiControllerServer->pClientSocket[iii]->rx_buffer.index -= packet_size;
                                    memmove(pMultiControllerServer->pClientSocket[iii]->rx_buffer.data,
                                            &pMultiControllerServer->pClientSocket[iii]->rx_buffer.data[packet_size],
                                            pMultiControllerServer->pClientSocket[iii]->rx_buffer.index);
                                }
                                else
                                    pMultiControllerServer->pClientSocket[iii]->rx_buffer.index = 0;
                                memset(&pMultiControllerServer->pClientSocket[iii]->rx_buffer.data[pMultiControllerServer->pClientSocket[iii]->rx_buffer.index],
                                    0, packet_size);
                            }
                        }
                    }
                }
            }
        }
    }
}

void CControllerApp::CheckCustomerControlClientReceive()
{
 int iii, bytes_received;
 SocketMessage *pSocketMessage;
 EventMessage event_message;

    for (iii=0; iii < MAXIMUM_CUSTOMER_CONTROL_CLIENTS; iii++)
    {
        if (customerControlListen.pClient[iii] && !customerControlListen.pClient[iii]->delete_socket)
        {
            bytes_received = customerControlListen.pClient[iii]->GetBuffer(
                &customerControlListen.pClient[iii]->rx_buffer.data[customerControlListen.pClient[iii]->rx_buffer.index],
                TCPIP_MESSAGE_BUFFER_SIZE * 4 - customerControlListen.pClient[iii]->rx_buffer.index);

            if (bytes_received)
            {
                customerControlListen.pClient[iii]->rx_buffer.index += bytes_received;
                pSocketMessage = (SocketMessage*)customerControlListen.pClient[iii]->rx_buffer.data;

                while (customerControlListen.pClient[iii]->rx_buffer.index >= sizeof(pSocketMessage->head))
                {
                    bytes_received = pSocketMessage->head.data_length + sizeof(pSocketMessage->head);

                    if (pSocketMessage->head.header != LONG_HEADER_IDENTIFIER)
                        memset(&customerControlListen.pClient[iii]->rx_buffer, 0, sizeof(customerControlListen.pClient[iii]->rx_buffer));
                    else
                    {
                        if (customerControlListen.pClient[iii]->rx_buffer.index < bytes_received)
                            return;
                        else
                        {
                            ProcessCustomerControlClientReceive(customerControlListen.pClient[iii], pSocketMessage);

                            memset(&event_message, 0, sizeof(event_message));
                            event_message.head.time_stamp = time(NULL);
                            event_message.head.event_type = CC_CLIENT_RECEIVE_MESSAGES;
                            event_message.head.data_length = bytes_received + 4;
                            memcpy(event_message.data, &customerControlListen.pClient[iii]->client_address, 4);

                            if (event_message.head.data_length <= MAXIMUM_EVENT_LOG_DATA_SIZE - 4)
                                memcpy(&event_message.data[4], pSocketMessage, event_message.head.data_length);
                            else
                                memcpy(&event_message.data[4], pSocketMessage, MAXIMUM_EVENT_LOG_DATA_SIZE - 4);

                            event_message_queue.push(event_message);

                            if (customerControlListen.pClient[iii]->rx_buffer.index == bytes_received)
                                memset(&customerControlListen.pClient[iii]->rx_buffer, 0,
                                    sizeof(customerControlListen.pClient[iii]->rx_buffer));
                            else
                            {
                                memset(customerControlListen.pClient[iii]->rx_buffer.data, 0, bytes_received);
                                customerControlListen.pClient[iii]->rx_buffer.index -= bytes_received;
                                memmove(customerControlListen.pClient[iii]->rx_buffer.data,
                                    &customerControlListen.pClient[iii]->rx_buffer.data[bytes_received],
                                    customerControlListen.pClient[iii]->rx_buffer.index);
                                memset(&customerControlListen.pClient[iii]->rx_buffer.data[customerControlListen.pClient[iii]->rx_buffer.index],
                                    0, bytes_received);
                            }
                        }
                    }
                }
            }
        }
    }
}

void CControllerApp::CheckDataDisplayTransmit()
{
 BYTE *pClientAddress;
 int iii, bytes_sent;
 WORD dispenser_data_raw = 0, dispenser_data_text = 0;
 DWORD port_numbers = 0;
 GameDataDisplayStruct game_message;
 CString client_address;
 DispenserUdpMessageFormat udp_message;

    for (iii=0; iii < MAXIMUM_CUSTOMER_CONTROL_CLIENTS; iii++)
    {
        if (customerControlListen.pClient[iii])
        {
            dispenser_data_raw += customerControlListen.pClient[iii]->client_data_display.dispenser_data_raw;
            dispenser_data_text += customerControlListen.pClient[iii]->client_data_display.dispenser_data_text;
            port_numbers += customerControlListen.pClient[iii]->client_data_display.port_number;
        }
    }

    if (dispenser_data_raw || (pDispenserDialog && pDispenserDialog->m_display_dispenser_raw_data.GetCheck() == BST_CHECKED))
    {
        if (!data_display.dispenser_data_raw)
            data_display.dispenser_data_raw = true;
    }
    else
    {
        if (data_display.dispenser_data_raw)
            data_display.dispenser_data_raw = false;
    }

    if (dispenser_data_text || (pDispenserDialog && pDispenserDialog->m_display_dispenser_verbose_data.GetCheck() == BST_CHECKED))
    {
        if (!data_display.dispenser_data_text)
            data_display.dispenser_data_text = true;
    }
    else
    {
        if (data_display.dispenser_data_text)
            data_display.dispenser_data_text = false;
    }

    if (port_numbers || (pTechnicianDisplay && pTechnicianDisplay->display_game_port_data))
    {
        if (!data_display.port_number)
            data_display.port_number = 1;
    }
    else
    {
        if (data_display.port_number)
            data_display.port_number = 0;
    }

    if (!dispenser_display_data_queue.empty())
    {
        udp_message = dispenser_display_data_queue.front();
        dispenser_display_data_queue.pop();

        #pragma pack(1)
        struct
        {
            DWORD command_header;
            char  *data_pointer;
        } dispenser_tx;
        #pragma pack()

//???????? change dispenser commands to use length

        if (udp_message.command_header == DISPENSER_RAW_DATA)
        {
            if (pDispenserDialog && pDispenserDialog->m_display_dispenser_raw_data.GetCheck() == BST_CHECKED)
                pDispenserDialog->m_dispenser_display_listbox.InsertString(0, udp_message.data);

            for (iii=0; iii < MAXIMUM_CUSTOMER_CONTROL_CLIENTS; iii++)
            {
                if (customerControlListen.pClient[iii] &&
                    customerControlListen.pClient[iii]->client_data_display.dispenser_data_raw &&
                    customerControlListen.pClient[iii]->client_address &&
                    customerControlListen.pClient[iii]->validated_level >= SUPERVISOR)
                {
                    dispenser_tx.command_header = DISPENSER_RAW_DATA;
                    dispenser_tx.data_pointer = udp_message.data.GetBuffer(0);

                    pClientAddress = (BYTE*)&customerControlListen.pClient[iii]->client_address;
                    client_address.Format("%u.%u.%u.%u", pClientAddress[0], pClientAddress[1],
                        pClientAddress[2], pClientAddress[3]);

                    bytes_sent = customerControlUdpSocket.SendTo(&dispenser_tx, udp_message.data.GetLength() + 4,
                        CUSTOMER_CONTROL_CLIENT_UDP_PORT, client_address);

                    if (bytes_sent == SOCKET_ERROR)
                        customer_control_udp_time_check = time(NULL);
                }
            }
        }
        else if (udp_message.command_header == DISPENSER_TEXT_MESSAGE)
        {
            if (pDispenserDialog && pDispenserDialog->m_display_dispenser_verbose_data.GetCheck() == BST_CHECKED)
                pDispenserDialog->m_dispenser_display_listbox.InsertString(0, udp_message.data);

            for (iii=0; iii < MAXIMUM_CUSTOMER_CONTROL_CLIENTS; iii++)
            {
                if (customerControlListen.pClient[iii] &&
                    customerControlListen.pClient[iii]->client_data_display.dispenser_data_text &&
                    customerControlListen.pClient[iii]->client_address &&
                    customerControlListen.pClient[iii]->validated_level >= SUPERVISOR)
                {
                    dispenser_tx.command_header = DISPENSER_TEXT_MESSAGE;
                    dispenser_tx.data_pointer = udp_message.data.GetBuffer(0);

                    pClientAddress = (BYTE*)&customerControlListen.pClient[iii]->client_address;
                    client_address.Format("%u.%u.%u.%u", pClientAddress[0], pClientAddress[1],
                        pClientAddress[2], pClientAddress[3]);

                    bytes_sent = customerControlUdpSocket.SendTo(&dispenser_tx, udp_message.data.GetLength() + 4,
                        CUSTOMER_CONTROL_CLIENT_UDP_PORT, client_address);

                    if (bytes_sent == SOCKET_ERROR)
                        customer_control_udp_time_check = time(NULL);
                }
            }
        }
    }

    while (!game_data_display_queue.empty())
    {
        game_message = game_data_display_queue.front();
        game_data_display_queue.pop();

        if (pTechnicianDisplay && pTechnicianDisplay->display_game_port_data)
            pTechnicianDisplay->DisplayGameData(&game_message);

        for (iii=0; iii < MAXIMUM_CUSTOMER_CONTROL_CLIENTS; iii++)
        {
            if (customerControlListen.pClient[iii] &&
                customerControlListen.pClient[iii]->client_data_display.port_number == game_message.port_number &&
                customerControlListen.pClient[iii]->client_address)
            {
                pClientAddress = (BYTE*)&customerControlListen.pClient[iii]->client_address;
                client_address.Format("%u.%u.%u.%u", pClientAddress[0], pClientAddress[1],
                    pClientAddress[2], pClientAddress[3]);

				// change not added to customer control client
				if (game_message.data_type > MACHINE_INVALID_RECEIVE)
					game_message.data_type = MACHINE_INVALID_RECEIVE;

                bytes_sent = customerControlUdpSocket.SendTo(&game_message, game_message.length + 8,
                    CUSTOMER_CONTROL_CLIENT_UDP_PORT, client_address);

                if (bytes_sent == SOCKET_ERROR)
					customer_control_udp_time_check = time(NULL);
            }
        }
    }
}

void CControllerApp::CheckMultiControllerServerSocket()
{
 int last_error;

	if (pMultiControllerServer->m_hSocket == INVALID_SOCKET)
		pMultiControllerServer->Create(MASTER_CONTROLLER_PORT, SOCK_STREAM, FD_READ | FD_CLOSE | FD_ACCEPT);

	multi_controller_server_time_check = time(NULL);

	if (pMultiControllerServer->m_hSocket != INVALID_SOCKET)
	{
		if (pMultiControllerServer->Listen())
			multi_controller_server_time_check = 0;
		else
		{
			last_error = GetLastError();

			if (last_error == WSAEINPROGRESS || last_error == WSAEISCONN)
				multi_controller_server_time_check = 0;
			else
			{
				pMultiControllerServer->Close();
				multi_controller_server_time_check = time(NULL);
			}
		}
	}
}

void CControllerApp::CheckCustomerControlListeningSocket()
{
 int last_error;
 CString error_message;

	if (customerControlListen.m_hSocket == INVALID_SOCKET &&
		!customerControlListen.Create(3200, SOCK_STREAM, FD_READ | FD_CLOSE | FD_ACCEPT))
	{
		error_message.Format("CControllerApp::CheckCustomerControlListeningSocket - Create error %d.", GetLastError());
		logMessage(error_message);
	}

	customer_control_listening_time_check = time(NULL);

	if (customerControlListen.m_hSocket != INVALID_SOCKET)
	{
		if (customerControlListen.Listen())
			customer_control_listening_time_check = 0;
		else
		{
			last_error = GetLastError();

			if (last_error == WSAEINPROGRESS || last_error == WSAEISCONN)
				customer_control_listening_time_check = 0;
			else
			{
				error_message.Format("CControllerApp::CheckCustomerControlListeningSocket - Listen error %d.", last_error);
                logMessage(error_message);
				customerControlListen.Close();
				customer_control_listening_time_check = time(NULL);
			}
		}
	}
}

char* CControllerApp::GetRasAsciiString(DWORD error_code)
{
 static char return_string[200];
 CString display_message;

	switch (error_code)
	{
		case RASCS_OpenPort:
			display_message = "The communication port is about to be opened.";
			break;

		case RASCS_PortOpened:
			display_message = "The communication port has been opened successfully.";
			break;

		case RASCS_ConnectDevice:
			display_message = "The device is about to be connected.";
			break;

		case RASCS_DeviceConnected:
			display_message = "A device has connected successfully.";
			break;

		case RASCS_AllDevicesConnected:
			display_message = "All devices in the device chain have successfully connected. "
				"The physical link is established.";
			break;

		case RASCS_Authenticate:
			display_message = "The authentication process is starting.";
			break;

		case RASCS_AuthNotify:
			display_message = "An authentication event has occurred.";
			break;

		case RASCS_AuthRetry:
			display_message = "The client has requested another validation attempt "
				"with a new user name/password/domain.";
			break;

		case RASCS_AuthCallback:
			display_message = "The remote access server has requested a callback number.";
			break;

		case RASCS_AuthChangePassword:
			display_message = "The client has requested to change the password on the account.";
			break;

		case RASCS_AuthProject:
			display_message = "The projection phase is starting.";
			break;

		case RASCS_AuthLinkSpeed:
			display_message = "The link-speed calculation phase is starting.";
			break;

		case RASCS_AuthAck:
			display_message = "An authentication request is being acknowledged.";
			break;

		case RASCS_ReAuthenticate:
			display_message = "Reauthentication (after callback) is starting.";
			break;

		case RASCS_Authenticated:
			display_message = "The client has successfully completed authentication.";
			break;

		case RASCS_PrepareForCallback:
			display_message = "The line is about to disconnect in preparation for callback.";
			break;

		case RASCS_WaitForModemReset:
			display_message = "The client is delaying in order to give the modem time "
				"to reset itself in preparation for callback.";
			break;

		case RASCS_WaitForCallback:
			display_message = "The client is waiting for an incoming call from the remote access server.";
			break;

		case RASCS_Projected: 
			display_message = "The projection result information is available.";
			break;

		case RASCS_SubEntryConnected:
			display_message = "Subentry connected during the dialing process.";
			break;

		case RASCS_SubEntryDisconnected:
			display_message = "Subentry disconnected during the dialing process.";
			break;

		case RASCS_Interactive:
			display_message = "Terminal state paused.";
			break;

		case RASCS_RetryAuthentication:
			display_message = "Retry authentication.";
			break;

		case RASCS_CallbackSetByCaller:
			display_message = "Callback state.";
			break;

		case RASCS_PasswordExpired:
			display_message = "Password has expired.";
			break;

		case RASCS_InvokeEapUI:
			display_message = "Dialer paused.";
			break;

		case RASCS_Connected:
			display_message = "RAS connected.";
			break;

		case RASCS_Disconnected:
			display_message = "RAS disconnected.";
			break;

		case PENDING:
			display_message = "An operation is pending.";
			break;

		case ERROR_INVALID_PORT_HANDLE:
			display_message = "An invalid port handle was detected.";
			break;

		case ERROR_PORT_ALREADY_OPEN:
			display_message = "The specified port is already open.";
			break;

		case ERROR_BUFFER_TOO_SMALL:
			display_message = "The caller's buffer is too small.";
			break;

		case ERROR_WRONG_INFO_SPECIFIED:
			display_message = "Incorrect information was specified.";
			break;

		case ERROR_CANNOT_SET_PORT_INFO:
			display_message = "The port information cannot be set.";
			break;

		case ERROR_PORT_NOT_CONNECTED:
			display_message = "The specified port is not connected.";
			break;

		case ERROR_EVENT_INVALID:
			display_message = "An invalid event was detected.";
			break;

		case ERROR_DEVICE_DOES_NOT_EXIST:
			display_message = "A device was specified that does not exist.";
			break;

		case ERROR_DEVICETYPE_DOES_NOT_EXIST:
			display_message = "A device type was specified that does not exist.";
			break;

		case ERROR_BUFFER_INVALID:
			display_message = "An invalid buffer was specified.";
			break;

		case ERROR_ROUTE_NOT_AVAILABLE:
			display_message = "A route was specified that is not available.";
			break;

		case ERROR_ROUTE_NOT_ALLOCATED:
			display_message = "A route was specified that is not allocated.";
			break;

		case ERROR_INVALID_COMPRESSION_SPECIFIED:
			display_message = "An invalid compression was specified.";
			break;

		case ERROR_OUT_OF_BUFFERS:
			display_message = "There were insufficient buffers available.";
			break;

		case ERROR_PORT_NOT_FOUND:
			display_message = "The specified port was not found.";
			break;

		case ERROR_ASYNC_REQUEST_PENDING:
			display_message = "An asynchronous request is pending.";
			break;

		case ERROR_ALREADY_DISCONNECTING:
			display_message = "The modem (or other connecting device) is already disconnecting.";
			break;

		case ERROR_PORT_NOT_OPEN:
			display_message = "The specified port is not open.";
			break;

		case ERROR_PORT_DISCONNECTED:
			display_message = "A connection to the remote computer could not be established, "
				"so the port used for this connection was closed.";
			break;

		case ERROR_NO_ENDPOINTS:
			display_message = "No endpoints could be determined.";
			break;

		case ERROR_CANNOT_OPEN_PHONEBOOK:
			display_message = "The system could not open the phone book file.";
			break;

		case ERROR_CANNOT_LOAD_PHONEBOOK:
			display_message = "The system could not load the phone book file.";
			break;

		case ERROR_CANNOT_FIND_PHONEBOOK_ENTRY:
			display_message = "The system could not find the phone book entry for this connection.";
			break;

		case ERROR_CANNOT_WRITE_PHONEBOOK:
			display_message = "The system could not update the phone book file.";
			break;

		case ERROR_CORRUPT_PHONEBOOK:
			display_message = "The system found invalid information in the phone book file.";
			break;

		case ERROR_CANNOT_LOAD_STRING:
			display_message = "A string could not be loaded.";
			break;

		case ERROR_KEY_NOT_FOUND:
			display_message = "A key could not be found.";
			break;

		case ERROR_DISCONNECTION:
			display_message = "The connection was terminated by the remote computer before "
				"it could be completed.";
			break;

		case ERROR_REMOTE_DISCONNECTION:
			display_message = "The connection was closed by the remote computer.";
			break;

		case ERROR_HARDWARE_FAILURE:
			display_message = "The modem (or other connecting device) was disconnected due "
				"to hardware failure.";
			break;

		case ERROR_USER_DISCONNECTION:
			display_message = "The user disconnected the modem (or other connecting device).";
			break;

		case ERROR_INVALID_SIZE:
			display_message = "An incorrect structure size was detected.";
			break;

		case ERROR_PORT_NOT_AVAILABLE:
			display_message = "The modem (or other connecting device) is already in use or is not "
				"configured properly."; 
			break;

		case ERROR_CANNOT_PROJECT_CLIENT:
			display_message = "Your computer could not be registered on the remote network.";
			break;

		case ERROR_UNKNOWN:
			display_message = "There was an unknown error.";
			break;

		case ERROR_WRONG_DEVICE_ATTACHED:
			display_message = "The device attached to the port is not the one expected.";
			break;

		case ERROR_BAD_STRING:
			display_message = "A string was detected that could not be converted.";
			break;

		case ERROR_REQUEST_TIMEOUT:
			display_message = "The remote server is not responding in a timely fashion. ";
			break;

		case ERROR_CANNOT_GET_LANA:
			display_message = "No asynchronous net is available.";
			break;

		case ERROR_NETBIOS_ERROR:
			display_message = "An error has occurred involving NetBIOS.";
			break;

		case ERROR_SERVER_OUT_OF_RESOURCES:
			display_message = "The server cannot allocate NetBIOS resources needed to support the client.";
			break;

		case ERROR_NAME_EXISTS_ON_NET:
			display_message = "One of your computer's NetBIOS names is already registered on the remote network.";
			break;

		case ERROR_SERVER_GENERAL_NET_FAILURE:
			display_message = "A network adapter at the server failed.";
			break;

		case WARNING_MSG_ALIAS_NOT_ADDED:
			display_message = "You will not receive network message popups.";
			break;

		case ERROR_AUTH_INTERNAL:
			display_message = "There was an internal authentication error.";
			break;

		case ERROR_RESTRICTED_LOGON_HOURS:
			display_message = "The account is not permitted to log on at this time of day.";
			break;

		case ERROR_ACCT_DISABLED:
			display_message = "The account is disabled.";
			break;

		case ERROR_PASSWD_EXPIRED:
			display_message = "The password for this account has expired.";
			break;

		case ERROR_NO_DIALIN_PERMISSION:
			display_message = "The account does not have permission to dial in.";
			break;

		case ERROR_SERVER_NOT_RESPONDING:
			display_message = "The remote access server is not responding.";
			break;

		case ERROR_FROM_DEVICE:
			display_message = "The modem (or other connecting device) has reported an error.";
			break;

		case ERROR_UNRECOGNIZED_RESPONSE:
			display_message = "There was an unrecognized response from the modem "
				"(or other connecting device).";
			break;

		case ERROR_MACRO_NOT_FOUND:
			display_message = "A macro required by the modem (or other connecting device) was "
				"not found in the device.INF file.";
			break;

		case ERROR_MACRO_NOT_DEFINED:
			display_message = "A command or response in the device.INF file section refers to "
				"an undefined macro.";
			break;

		case ERROR_MESSAGE_MACRO_NOT_FOUND:
			display_message = "The <message> macro was not found in the device.INF file section.";
			break;

		case ERROR_DEFAULTOFF_MACRO_NOT_FOUND:
			display_message = "The <defaultoff> macro in the device.INF file section contains "
				"an undefined macro.";
			break;

		case ERROR_FILE_COULD_NOT_BE_OPENED:
			display_message = "The device.INF file could not be opened.";
			break;

		case ERROR_DEVICENAME_TOO_LONG:
			display_message = "The device name in the device.INF or media.INI file is too long.";
			break;

		case ERROR_DEVICENAME_NOT_FOUND:
			display_message = "The media.INI file refers to an unknown device name.";
			break;

		case ERROR_NO_RESPONSES:
			display_message = "The device.INF file contains no responses for the command.";
			break;

		case ERROR_NO_COMMAND_FOUND:
			display_message = "The device.INF file is missing a command.";
			break;

		case ERROR_WRONG_KEY_SPECIFIED:
			display_message = "There was an attempt to set a macro not listed in device.INF file section.";
			break;

		case ERROR_UNKNOWN_DEVICE_TYPE:
			display_message = "The media.INI file refers to an unknown device type.";
			break;

		case ERROR_ALLOCATING_MEMORY:
			display_message = "The system has run out of memory.";
			break;

		case ERROR_PORT_NOT_CONFIGURED:
			display_message = "The modem (or other connecting device) is not properly configured.";
			break;

		case ERROR_DEVICE_NOT_READY:
			display_message = "The modem (or other connecting device) is not functioning.";
			break;

		case ERROR_READING_INI_FILE:
			display_message = "The system was unable to read the media.INI file.";
			break;

		case ERROR_NO_CONNECTION:
			display_message = "The connection was terminated.";
			break;

		case ERROR_BAD_USAGE_IN_INI_FILE:
			display_message = "The usage parameter in the media.INI file is invalid.";
 			break;

		case ERROR_READING_SECTIONNAME:
			display_message = "The system was unable to read the section name from the media.INI file.";
 			break;

		case ERROR_READING_DEVICETYPE:
			display_message = "The system was unable to read the device type from the media.INI file.";
 			break;

		case ERROR_READING_DEVICENAME:
			display_message = "The system was unable to read the device name from the media.INI file.";
 			break;

		case ERROR_READING_USAGE:
			display_message = "The system was unable to read the usage from the media.INI file.";
 			break;

		case ERROR_READING_MAXCONNECTBPS:
			display_message = "The system was unable to read the maximum connection BPS rate from "
				"the media.INI file.";
 			break;

		case ERROR_READING_MAXCARRIERBPS:
			display_message = "The system was unable to read the maximum carrier connection speed "
				"from the media.INI file.";
 			break;

		case ERROR_LINE_BUSY:
			display_message = "The phone line is busy.";
 			break;

		case ERROR_VOICE_ANSWER:
			display_message = "A person answered instead of a modem (or other connecting device).";
 			break;

		case ERROR_NO_ANSWER:
			display_message = "The remote computer did not respond.";
 			break;

		case ERROR_NO_CARRIER:
			display_message = "The system could not detect the carrier.";
 			break;

		case ERROR_NO_DIALTONE:
			display_message = "There was no dial tone.";
 			break;

		case ERROR_IN_COMMAND:
			display_message = "The modem (or other connecting device) reported a general error.";
 			break;

		case ERROR_WRITING_SECTIONNAME:
			display_message = "There was an error in writing the section name.";
 			break;

		case ERROR_WRITING_DEVICETYPE:
			display_message = "There was an error in writing the device type.";
 			break;

		case ERROR_WRITING_DEVICENAME:
			display_message = "There was an error in writing the device name.";
 			break;

		case ERROR_WRITING_MAXCONNECTBPS:
			display_message = "There was an error in writing the maximum connection speed.";
 			break;

		case ERROR_WRITING_MAXCARRIERBPS:
			display_message = "There was an error in writing the maximum carrier speed.";
 			break;

		case ERROR_WRITING_USAGE:
			display_message = "There was an error in writing the usage.";
 			break;

		case ERROR_WRITING_DEFAULTOFF:
			display_message = " There was an error in writing the default-off.";
 			break;

		case ERROR_READING_DEFAULTOFF:
			display_message = " There was an error in reading the default-off.";
 			break;

		case ERROR_EMPTY_INI_FILE:
			display_message = "ERROR_EMPTY_INI_FILE";
 			break;

		case ERROR_AUTHENTICATION_FAILURE:
			display_message = "Access was denied because the username and/or password was invalid "
				"on the domain.";
 			break;

		case ERROR_PORT_OR_DEVICE:
			display_message = "There was a hardware failure in the modem (or other connecting device).";
 			break;

		case ERROR_NOT_BINARY_MACRO:
			display_message = "ERROR_NOT_BINARY_MACRO";
 			break;

		case ERROR_DCB_NOT_FOUND:
			display_message = "ERROR_DCB_NOT_FOUND";
 			break;

		case ERROR_STATE_MACHINES_NOT_STARTED:
			display_message = "The state machines are not started.";
 			break;

		case ERROR_STATE_MACHINES_ALREADY_STARTED:
			display_message = "The state machines are already started.";
 			break;

		case ERROR_PARTIAL_RESPONSE_LOOPING:
			display_message = "The response looping did not complete.";
 			break;

		case ERROR_UNKNOWN_RESPONSE_KEY:
			display_message = "A response keyname in the device.INF file is not in the expected format.";
 			break;

		case ERROR_RECV_BUF_FULL:
			display_message = "The modem (or other connecting device) response caused a buffer overflow.";
 			break;

		case ERROR_CMD_TOO_LONG:
			display_message = "The expanded command in the device.INF file is too long.";
 			break;

		case ERROR_UNSUPPORTED_BPS:
			display_message = "The modem moved to a connection speed not supported by the COM driver.";
 			break;

		case ERROR_UNEXPECTED_RESPONSE:
			display_message = "Device response received when none expected.";
 			break;

		case ERROR_INTERACTIVE_MODE:
			display_message = "The connection needs information from you, but the application "
				"does not allow user interaction.";
 			break;

		case ERROR_BAD_CALLBACK_NUMBER:
			display_message = "The callback number is invalid.";
 			break;

		case ERROR_INVALID_AUTH_STATE:
			display_message = "The authorization state is invalid.";
 			break;

		case ERROR_WRITING_INITBPS:
			display_message = "ERROR_WRITING_INITBPS";
 			break;

		case ERROR_X25_DIAGNOSTIC:
			display_message = "There was an error related to the X.25 protocol.";
 			break;

		case ERROR_ACCT_EXPIRED:
			display_message = "The account has expired.";
 			break;

		case ERROR_CHANGING_PASSWORD:
			display_message = "There was an error changing the password on the domain.  The password might "
				"have been too short or might have matched a previously used password.";
 			break;

		case ERROR_OVERRUN:
			display_message = "Serial overrun errors were detected while communicating with the modem.";
 			break;

		case ERROR_RASMAN_CANNOT_INITIALIZE:
			display_message = "A configuration error on this computer is preventing this connection.";
 			break;

		case ERROR_BIPLEX_PORT_NOT_AVAILABLE:
			display_message = "The two-way port is initializing.  Wait a few seconds and redial.";
 			break;

		case ERROR_NO_ACTIVE_ISDN_LINES:
			display_message = "No active ISDN lines are available.";
 			break;

		case ERROR_NO_ISDN_CHANNELS_AVAILABLE:
			display_message = "No ISDN channels are available to make the call.";
 			break;

		case ERROR_TOO_MANY_LINE_ERRORS:
			display_message = "Too many errors occurred because of poor phone line quality.";
 			break;

		case ERROR_IP_CONFIGURATION:
			display_message = "The Remote Access Service IP configuration is unusable.";
 			break;

		case ERROR_NO_IP_ADDRESSES:
			display_message = "No IP addresses are available in the static pool of Remote Access Service IP addresses.";
 			break;

		case ERROR_PPP_TIMEOUT:
			display_message = "The connection was terminated because the remote computer did not "
				"respond in a timely manner.";
 			break;

		case ERROR_PPP_REMOTE_TERMINATED:
			display_message = "The connection was terminated by the remote computer.";
 			break;

		case ERROR_PPP_NO_PROTOCOLS_CONFIGURED:
			display_message = "A connection to the remote computer could not be established. "
				"You might need to change the network settings for this connection.";
 			break;

		case ERROR_PPP_NO_RESPONSE:
			display_message = "The remote computer did not respond.";
 			break;

		case ERROR_PPP_INVALID_PACKET:
			display_message = "Invalid data was received from the remote computer. This data was ignored.";
 			break;

		case ERROR_PHONE_NUMBER_TOO_LONG:
			display_message = "The phone number, including prefix and suffix, is too long.";
 			break;

		case ERROR_IPXCP_NO_DIALOUT_CONFIGURED:
			display_message = "The IPX protocol cannot dial out on the modem (or other connecting device) "
				"because this computer is not configured for dialing out (it is an IPX router).";
 			break;

		case ERROR_IPXCP_NO_DIALIN_CONFIGURED:
			display_message = "The IPX protocol cannot dial in on the modem (or other connecting device) "
				"because this computer is not configured for dialing in (the IPX router is not installed).";
 			break;

		case ERROR_IPXCP_DIALOUT_ALREADY_ACTIVE:
			display_message = "The IPX protocol cannot be used for dialing out on more than one modem "
				"(or other connecting device) at a time.";
 			break;

		case ERROR_ACCESSING_TCPCFGDLL:
			display_message = "Cannot access TCPCFG.DLL.";
 			break;

		case ERROR_NO_IP_RAS_ADAPTER:
			display_message = "The system cannot find an IP adapter.";
 			break;

		case ERROR_SLIP_REQUIRES_IP:
			display_message = "SLIP cannot be used unless the IP protocol is installed.";
 			break;

		case ERROR_PROJECTION_NOT_COMPLETE:
			display_message = "Computer registration is not complete.";
 			break;

		case ERROR_PROTOCOL_NOT_CONFIGURED:
			display_message = "The protocol is not configured.";
 			break;

		case ERROR_PPP_NOT_CONVERGING:
			display_message = "Your computer and the remote computer could not agree on PPP control protocols.";
 			break;

		case ERROR_PPP_CP_REJECTED:
			display_message = "A connection to the remote computer could not be completed. "
				"You might need to adjust the protocols on this computer.";
 			break;

		case ERROR_PPP_LCP_TERMINATED:
			display_message = "The PPP link control protocol was terminated.";
 			break;

		case ERROR_PPP_REQUIRED_ADDRESS_REJECTED:
			display_message = "The requested address was rejected by the server.";
 			break;

		case ERROR_PPP_NCP_TERMINATED:
			display_message = "The remote computer terminated the control protocol.";
 			break;

		case ERROR_PPP_LOOPBACK_DETECTED:
			display_message = "Loopback was detected.";
 			break;

		case ERROR_PPP_NO_ADDRESS_ASSIGNED:
			display_message = "The server did not assign an address.";
 			break;

		case ERROR_CANNOT_USE_LOGON_CREDENTIALS:
			display_message = "The authentication protocol required by the remote server cannot use the stored password. "
				"Redial, entering the password explicitly.";
 			break;

		case ERROR_TAPI_CONFIGURATION:
			display_message = "An invalid dialing rule was detected.";
 			break;

		case ERROR_NO_LOCAL_ENCRYPTION:
			display_message = "The local computer does not support the required data encryption type.";
 			break;

		case ERROR_NO_REMOTE_ENCRYPTION:
			display_message = "The remote computer does not support the required data encryption type.";
 			break;

		case ERROR_REMOTE_REQUIRES_ENCRYPTION:
			display_message = "The remote computer requires data encryption.";
 			break;

		case ERROR_IPXCP_NET_NUMBER_CONFLICT:
			display_message = "The system cannot use the IPX network number assigned by the remote computer.";
 			break;

		case ERROR_INVALID_SMM:
			display_message = "ERROR_INVALID_SMM";
 			break;

		case ERROR_SMM_UNINITIALIZED:
			display_message = "ERROR_SMM_UNINITIALIZED";
 			break;

		case ERROR_NO_MAC_FOR_PORT:
			display_message = "ERROR_NO_MAC_FOR_PORT";
 			break;

		case ERROR_SMM_TIMEOUT:
			display_message = "ERROR_SMM_TIMEOUT";
 			break;

		case ERROR_BAD_PHONE_NUMBER:
			display_message = "ERROR_BAD_PHONE_NUMBER";
 			break;

		case ERROR_WRONG_MODULE:
			display_message = "ERROR_WRONG_MODULE";
 			break;

		case ERROR_INVALID_CALLBACK_NUMBER:
			display_message = "The callback number contains an invalid character. "
				"Only the following 18 characters are allowed: "
				"0 to 9, T, P, W, (, ), -, @, and space.";
 			break;

		case ERROR_SCRIPT_SYNTAX:
			display_message = "A syntax error was encountered while processing a script.";
 			break;

		case ERROR_HANGUP_FAILED:
			display_message = "The connection could not be disconnected because it was "
				"created by the multi-protocol router.";
 			break;

		case ERROR_BUNDLE_NOT_FOUND:
			display_message = "The system could not find the multi-link bundle.";
 			break;

		case ERROR_CANNOT_DO_CUSTOMDIAL:
			display_message = "The system cannot perform automated dial because this connection "
				"has a custom dialer specified.";
 			break; 

		case ERROR_DIAL_ALREADY_IN_PROGRESS:
			display_message = "This connection is already being dialed.";
 			break;

		case ERROR_RASAUTO_CANNOT_INITIALIZE:
			display_message = "Remote Access Services could not be started automatically.";
 			break;

		case ERROR_CONNECTION_ALREADY_SHARED:
			display_message = "Internet Connection Sharing is already enabled on the connection.";
 			break;

		case ERROR_SHARING_CHANGE_FAILED:
			display_message = "An error occurred while the existing Internet Connection Sharing "
				"settings were being changed.";
 			break;

		case ERROR_SHARING_ROUTER_INSTALL:
			display_message = "An error occurred while routing capabilities were being enabled.";
 			break;

		case ERROR_SHARE_CONNECTION_FAILED:
			display_message = "An error occurred while Internet Connection Sharing was being "
				"enabled for the connection.";
 			break;

		case ERROR_SHARING_PRIVATE_INSTALL:
			display_message = "An error occurred while the local network was being configured for sharing.";
 			break;

		case ERROR_CANNOT_SHARE_CONNECTION:
			display_message = "Internet Connection Sharing cannot be enabled.  There is more than one LAN "
				"connection other than the connection to be shared.";
 			break;

		case ERROR_NO_SMART_CARD_READER:
			display_message = "No smart card reader is installed.";
 			break;

		case ERROR_SHARING_ADDRESS_EXISTS:
			display_message = "Internet Connection Sharing cannot be enabled.  A LAN connection is already "
				"configured with the IP address that is required for automatic IP addressing.";
 			break;

		case ERROR_NO_CERTIFICATE:
			display_message = "A certificate could not be found. Connections that use the L2TP protocol "
				"over IPSec require the installation of a machine certificate, also known as a computer certificate.";
 			break;

		case ERROR_SHARING_MULTIPLE_ADDRESSES:
			display_message = "Internet Connection Sharing cannot be enabled. The LAN connection "
				"selected as the private network has more than one IP address configured. "
				"Please reconfigure the LAN connection with a single IP address before enabling "
				"Internet Connection Sharing.";
 			break;

		case ERROR_FAILED_TO_ENCRYPT:
			display_message = "The connection attempt failed because of failure to encrypt data.";
 			break;

		case ERROR_BAD_ADDRESS_SPECIFIED:
			display_message = "The specified destination is not reachable.";
 			break;

		case ERROR_CONNECTION_REJECT:
			display_message = "The remote computer rejected the connection attempt.";
 			break;

		case ERROR_CONGESTION:
			display_message = "The connection attempt failed because the network is busy.";
 			break;

		case ERROR_INCOMPATIBLE:
			display_message = "The remote computer's network hardware is incompatible with the type of call requested.";
 			break;

		case ERROR_NUMBERCHANGED:
			display_message = "The connection attempt failed because the destination number has changed.";
 			break;

		case ERROR_TEMPFAILURE:
			display_message = "The connection attempt failed because of a temporary failure.  Try connecting again.";
 			break; 

		case ERROR_BLOCKED:
			display_message = "The call was blocked by the remote computer.";
 			break;

		case ERROR_DONOTDISTURB:
			display_message = "The call could not be connected because the remote computer has invoked "
				"the Do Not Disturb feature.";
 			break;

		case ERROR_OUTOFORDER:
			display_message = "The connection attempt failed because the modem (or other connecting device) "
				"on the remote computer is out of order.";
 			break;

		case ERROR_UNABLE_TO_AUTHENTICATE_SERVER:
			display_message = "It was not possible to verify the identity of the server.";
 			break;

		case ERROR_SMART_CARD_REQUIRED:
			display_message = "To dial out using this connection you must use a smart card.";
 			break;

		case ERROR_INVALID_FUNCTION_FOR_ENTRY:
			display_message = "An attempted function is not valid for this connection.";
 			break;

		case ERROR_CERT_FOR_ENCRYPTION_NOT_FOUND:
			display_message = "The connection requires a certificate, and no valid certificate was found.";
 			break;

		case ERROR_SHARING_RRAS_CONFLICT:
			display_message = "Internet Connection Sharing (ICS) and Internet Connection Firewall (ICF) "
				"cannot be enabled because Routing and Remote Access has been enabled on this computer. "
				"To enable ICS or ICF, first disable Routing and Remote Access.";
 			break;

		case ERROR_SHARING_NO_PRIVATE_LAN:
			display_message = "Internet Connection Sharing cannot be enabled. The LAN connection selected "
				"as the private network is either not present, or is disconnected from the network. "
				"Please ensure that the LAN adapter is connected before enabling Internet Connection Sharing.";
 			break;

		case ERROR_NO_DIFF_USER_AT_LOGON:
			display_message = "You cannot dial using this connection at logon time, because it is configured "
				"to use a user name different than the one on the smart card. If you want to use it at "
				"logon time, you must configure it to use the user name on the smart card.";
 			break;

		case ERROR_NO_REG_CERT_AT_LOGON:
			display_message = "You cannot dial using this connection at logon time, because it is not configured "
				"to use a smart card. If you want to use it at logon time, you must edit the properties of this "
				"connection so that it uses a smart card.";
 			break;

		case ERROR_OAKLEY_NO_CERT:
			display_message = "The L2TP connection attempt failed because there is no valid machine "
				"certificate on your computer for security authentication.";
 			break;

		case ERROR_OAKLEY_AUTH_FAIL:
			display_message = "The L2TP connection attempt failed because the security layer "
				"could not authenticate the remote computer.";
 			break;

		case ERROR_OAKLEY_ATTRIB_FAIL:
			display_message = "The L2TP connection attempt failed because the security layer "
				"could not negotiate compatible parameters with the remote computer.";
 			break;

		case ERROR_OAKLEY_GENERAL_PROCESSING:
			display_message = "The L2TP connection attempt failed because the security layer "
				"encountered a processing error during initial negotiations with the remote computer.";
 			break;

		case ERROR_OAKLEY_NO_PEER_CERT:
			display_message = "The L2TP connection attempt failed because certificate validation "
				"on the remote computer failed.";
 			break;

		case ERROR_OAKLEY_NO_POLICY:
			display_message = "The L2TP connection attempt failed because security policy for the "
				"connection was not found.";
 			break;

		case ERROR_OAKLEY_TIMED_OUT:
			display_message = "The L2TP connection attempt failed because security negotiation timed out.";
 			break;

		case ERROR_OAKLEY_ERROR:
			display_message = "The L2TP connection attempt failed because an error occurred "
				"while negotiating security.";
 			break;

		case ERROR_UNKNOWN_FRAMED_PROTOCOL:
			display_message = "The Framed Protocol RADIUS attribute for this user is not PPP.";
 			break;

		case ERROR_WRONG_TUNNEL_TYPE:
			display_message = "The Tunnel Type RADIUS attribute for this user is not correct.";
 			break;

		case ERROR_UNKNOWN_SERVICE_TYPE:
			display_message = "The Service Type RADIUS attribute for this user is neither "
				"Framed nor Callback Framed.";
 			break;

		case ERROR_CONNECTING_DEVICE_NOT_FOUND:
			display_message = "A connection to the remote computer could not be established "
				"because the modem was not found or was busy.";
 			break;

		case ERROR_NO_EAPTLS_CERTIFICATE:
			display_message = "A certificate could not be found that can be used with this "
				"Extensible Authentication Protocol.";
 			break;

		case ERROR_SHARING_HOST_ADDRESS_CONFLICT:
			display_message = "Internet Connection Sharing (ICS) cannot be enabled due to an "
				"IP address conflict on the network. ICS requires the host be configured to "
				"use 192.168.0.1. Please ensure that no other client on the network is "
				"configured to use 192.168.0.1.";
 			break;

		case ERROR_AUTOMATIC_VPN_FAILED:
			display_message = "Unable to establish the VPN connection. The VPN server may be "
				"unreachable, or security parameters may not be configured properly for this connection.";
 			break;

		case ERROR_VALIDATING_SERVER_CERT:
			display_message = "This connection is configured to validate the identity of the "
				"access server, but Windows cannot verify the digital certificate sent by the server.";
			break;

		case ERROR_READING_SCARD:
			display_message = "The card supplied was not recognized. Please check that the card "
				"is inserted correctly, and fits tightly.";
			break;

		case ERROR_INVALID_PEAP_COOKIE_CONFIG:
			display_message = "The PEAP configuration stored in the session cookie does not match "
				"the current session configuration.";
			break;

		case ERROR_INVALID_PEAP_COOKIE_USER:
			display_message = "The PEAP identity stored in the session cookie does not match the current identity.";
			break;

		case ERROR_INVALID_MSCHAPV2_CONFIG:
			display_message = "You cannot dial using this connection at logon time, because it is "
				"configured to use logged on user's credentials.";
			break;

		default:
			display_message.Format("Unknown Error Code: %u.", error_code);
			break;
	}

	if (display_message.GetLength() >= sizeof(return_string))
	{
		memcpy(return_string, display_message.GetBuffer(0), 199);
		return_string[199] = 0;
	}
	else
	{
		memcpy(return_string, display_message.GetBuffer(0), display_message.GetLength());
		return_string[display_message.GetLength()] = 0;
	}

 return return_string;
}

void CControllerApp::CloseRasConnection()
{
 RASCONNSTATUS connection_status;

	if (pRasConnectionStatus && pRasConnectionStatus->display_window_active)
		pRasConnectionStatus->m_ras_status_listbox.AddString("Closing RAS Connection");

	if (pNetworkSettingsRecord && pNetworkSettingsRecord->network_type)
		network_connection_active = false;

	if (pHandleRasConnection)
	{
		RasHangUp(*pHandleRasConnection);

		memset(&connection_status, 0, sizeof(connection_status));
		connection_status.dwSize = sizeof(connection_status);

		while (RasGetConnectStatus(*pHandleRasConnection, &connection_status) != ERROR_INVALID_HANDLE)
			Sleep(0);

		delete pHandleRasConnection;
		pHandleRasConnection = NULL;
	}

	if (pRasConnectionStatus && pRasConnectionStatus->display_window_active)
		pRasConnectionStatus->m_ras_status_listbox.AddString("RAS Connection Closed");
}

void CALLBACK RasDialFunc(UINT unMsg, RASCONNSTATE rasconnstate, DWORD dwError)
{
	if (dwError)
		theApp.CloseRasConnection();
	else
		dwError = rasconnstate;

	if (theApp.pRasConnectionStatus && theApp.pRasConnectionStatus->display_window_active)
		theApp.pRasConnectionStatus->m_ras_status_listbox.AddString(theApp.GetRasAsciiString(dwError));
}

void CControllerApp::CheckModemLineConnection()
{
 BOOL password_returned;
 DWORD function_return;
 RASCONNSTATUS connection_status;
 static time_t last_sockets_check;

	if (pRasDialParams == NULL)
	{
		if (network_connection_active)
			network_connection_active = false;

		if (pRasConnectionStatus && pRasConnectionStatus->display_window_active)
			pRasConnectionStatus->m_ras_status_listbox.AddString("RAS Connection Down, Creating RAS Parameters");

		pRasDialParams = new RASDIALPARAMS;
		memset(pRasDialParams, 0, sizeof(RASDIALPARAMS));
		pRasDialParams->dwSize = sizeof(RASDIALPARAMS);
		sprintf_s(pRasDialParams->szEntryName, sizeof(pRasDialParams->szEntryName), "%s", "United Coin");
		function_return = RasGetEntryDialParams(NULL, pRasDialParams, &password_returned);

		if (function_return)
		{
			if (pRasConnectionStatus && pRasConnectionStatus->display_window_active)
				pRasConnectionStatus->m_ras_status_listbox.AddString(GetRasAsciiString(function_return));

			delete pRasDialParams;
			pRasDialParams = NULL;
		}
	}
	else if (pHandleRasConnection == NULL)
	{
		if (network_connection_active)
			network_connection_active = false;

		if (pRasConnectionStatus && pRasConnectionStatus->display_window_active)
			pRasConnectionStatus->m_ras_status_listbox.AddString("Creating RAS Handle");

		pHandleRasConnection = new HRASCONN;
		memset(pHandleRasConnection, 0, sizeof(HRASCONN));
		function_return = RasDial(NULL, NULL, pRasDialParams, 0, RasDialFunc, pHandleRasConnection);

		if (function_return)
		{
			if (pRasConnectionStatus && pRasConnectionStatus->display_window_active)
				pRasConnectionStatus->m_ras_status_listbox.AddString(GetRasAsciiString(function_return));

			CloseRasConnection();
		}
	}
	else
	{
		memset(&connection_status, 0, sizeof(connection_status));
		connection_status.dwSize = sizeof(connection_status);
		function_return = RasGetConnectStatus(*pHandleRasConnection, &connection_status);

		if (pRasConnectionStatus && pRasConnectionStatus->display_window_active)
			pRasConnectionStatus->m_ras_status_listbox.AddString("Getting RAS Connection Status");

		if (function_return)
		{
			if (pRasConnectionStatus && pRasConnectionStatus->display_window_active)
				pRasConnectionStatus->m_ras_status_listbox.AddString(GetRasAsciiString(function_return));

			CloseRasConnection();
		}
		else
		{
			if (connection_status.rasconnstate == RASCS_Disconnected)
			{
				if (pRasConnectionStatus && pRasConnectionStatus->display_window_active)
					pRasConnectionStatus->m_ras_status_listbox.AddString(GetRasAsciiString(connection_status.dwError));

				CloseRasConnection();
			}
			else if (connection_status.rasconnstate == RASCS_Connected)
			{
				if (!network_connection_active)
				{
					network_connection_active = true;
					last_sockets_check = time(NULL) + 90;	// give 90 seconds for sockets to connect

					if (pRasConnectionStatus && pRasConnectionStatus->display_window_active)
						pRasConnectionStatus->m_ras_status_listbox.AddString("RAS Connected");
				}
				else
				{
					if (stratusSocket.socket_state && customerControlServerSocket.socket_state)
					{
						// give 90 seconds for either socket to connect
						if (last_sockets_check < time(NULL))
						{
							if (pRasConnectionStatus && pRasConnectionStatus->display_window_active)
								pRasConnectionStatus->m_ras_status_listbox.AddString("Stratus and Customer Control Server "
									"are disconnect. Restarting RAS connection.");

							CloseRasConnection();
						}
					}
					else
						last_sockets_check = time(NULL) + 10;	// after initial connection for both sockets, give 10 seconds to connect
				}
			}
		}
	}
}

void CControllerApp::CheckRemoteNetworkConnection()
{
 FileNetworkSettingsRecord file_settings;

	if (pNetworkSettingsRecord == NULL)
	{
		pNetworkSettingsRecord = new NetworkSettingsRecord;
		memset(pNetworkSettingsRecord, 0, sizeof(NetworkSettingsRecord));

		if (GetFileAttributes(NETWORK_SETTINGS) == 0xFFFFFFFF)
		{
			memset(&file_settings, 0, sizeof(file_settings));
			fileWrite(NETWORK_SETTINGS, 0, &file_settings, sizeof(file_settings));
		}

		if (fileRead(NETWORK_SETTINGS, 0, &file_settings, sizeof(file_settings)))
			memcpy(pNetworkSettingsRecord, &file_settings.settings, sizeof(NetworkSettingsRecord));
	}

	if (!pNetworkSettingsRecord->network_type)
	{
		if (!network_connection_active && !offline_test_flag)
			network_connection_active = true;
	}
	else
	{
		if (pNetworkSettingsRecord->network_type == REMOTE_NETWORK_LEASE_LINE ||
			pNetworkSettingsRecord->network_type == REMOTE_NETWORK_DIAL_UP)
				CheckModemLineConnection();
		else if (pRasConnectionStatus && pRasConnectionStatus->display_window_active)
			pRasConnectionStatus->m_ras_status_listbox.AddString("CControllerApp::CheckRemoteNetworkConnection - invalid network type.");
	}
}

void CControllerApp::CheckCustomerControlServer()
{
 int last_error;
 BYTE *ip_address;
 CString string_data;

	if (customerControlServerSocket.m_hSocket == INVALID_SOCKET && !customerControlServerSocket.Create(0, SOCK_STREAM, FD_READ | FD_CLOSE, 0))
	{
		string_data.Format("CControllerApp::CheckCustomerControlServer - Create error %d.", GetLastError());
		logMessage(string_data);
	}

	customerControlServerSocket.socket_state = WSAENOTCONN;

	if (customerControlServerSocket.m_hSocket != INVALID_SOCKET)
	{
		ip_address = (BYTE*)&memory_settings_ini.customer_control_server_ip_address;
		string_data.Format("%u.%u.%u.%u", ip_address[0], ip_address[1], ip_address[2], ip_address[3]);

		if (customerControlServerSocket.Connect(string_data, memory_settings_ini.customer_control_server_tcp_port))
			customerControlServerSocket.socket_state = 0;
		else
		{
			last_error = GetLastError();

			if (last_error == WSAEISCONN)
				customerControlServerSocket.socket_state = 0;
			else if (last_error == WSAEINPROGRESS || last_error == WSAEWOULDBLOCK)
				customerControlServerSocket.socket_state = last_error;
			else
			{
				string_data.Format("CControllerApp::CheckCustomerControlServer - Connect error %d.", last_error);
				logMessage(string_data);
				customerControlServerSocket.Close();
				customerControlServerSocket.socket_state = WSAENOTCONN;
			}
		}
	}
}

void CControllerApp::CheckStratusSocket()
{
 int last_error;
 BYTE *ip_address;
 WORD port_number;
 CString string_data;

	if (memory_settings_ini.stratus_ip_address && memory_settings_ini.stratus_tcp_port)
	{
		if (stratusSocket.m_hSocket == INVALID_SOCKET && !stratusSocket.Create(0, SOCK_STREAM, FD_READ | FD_CLOSE, 0))
		{
			string_data.Format("CControllerApp::CheckStratusSocket - Create error %d.", GetLastError());
			logMessage(string_data);
		}

		stratusSocket.socket_state = WSAENOTCONN;

		if (stratusSocket.m_hSocket != INVALID_SOCKET)
		{
			if (memory_settings_ini.masters_ip_address)
			{
				port_number = MASTER_CONTROLLER_PORT;
				ip_address = (BYTE*)&memory_settings_ini.masters_ip_address;
				string_data.Format("%u.%u.%u.%u", ip_address[0], ip_address[1], ip_address[2], ip_address[3]);
			}
			else
			{
				port_number = memory_settings_ini.stratus_tcp_port;
				ip_address = (BYTE*)&memory_settings_ini.stratus_ip_address;
				string_data.Format("%u.%u.%u.%u", ip_address[0], ip_address[1], ip_address[2], ip_address[3]);
			}

			if (stratusSocket.Connect(string_data, port_number))
				stratusSocket.socket_state = 0;
			else
			{
				last_error = GetLastError();

				if (last_error == WSAEISCONN)
					stratusSocket.socket_state = 0;
				else if (last_error == WSAEINPROGRESS || last_error == WSAEWOULDBLOCK)
					stratusSocket.socket_state = last_error;
				else
				{
					string_data.Format("CControllerApp::CheckStratusSocket - Connect error %d.", last_error);
					logMessage(string_data);
					stratusSocket.Close();
					stratusSocket.socket_state = WSAENOTCONN;
				}
			}
		}
	}

}

BYTE* CControllerApp::EncryptMachineData(BYTE *data, int *length)
{
 int iii;
 div_t length_segments;
 static BYTE encrypted_buffer[MAXIMUM_SERIAL_MESSAGE_SIZE];

	memset(encrypted_buffer, 0, sizeof(encrypted_buffer));

	if (*length > MAXIMUM_SERIAL_MESSAGE_SIZE - 16)
		*length = MAXIMUM_SERIAL_MESSAGE_SIZE - 16;

	memcpy(encrypted_buffer, data, *length);

	length_segments = div(*length, 16);
	if (length_segments.rem)
	{
		*length += 16 - length_segments.rem;
		length_segments.quot++;
	}

	for (iii=0; iii < length_segments.quot; iii++)
		MachineEncryptData(&encrypted_buffer[iii * 16], &encrypted_buffer[iii * 16]);

 return encrypted_buffer;
}

BYTE* CControllerApp::DecryptMachineData(BYTE *data, int length)
{
 int iii;
 div_t length_segments;
 static BYTE decrypted_buffer[MAXIMUM_SERIAL_MESSAGE_SIZE];

	memset(decrypted_buffer, 0, sizeof(decrypted_buffer));

	length_segments = div(length, 16);

	for (iii=0; iii < length_segments.quot; iii++)
		if (iii * 16 < MAXIMUM_SERIAL_MESSAGE_SIZE - 16)
			MachineDecryptData(&data[iii * 16], &decrypted_buffer[iii * 16]);

 return decrypted_buffer;
}

BYTE* CControllerApp::DecryptStratusData(BYTE *data, int length)
{
 int iii;
 div_t length_segments;
 static BYTE decrypted_buffer[TCPIP_MESSAGE_BUFFER_SIZE];

	memset(decrypted_buffer, 0, sizeof(decrypted_buffer));

	length_segments = div(length, 16);

	for (iii=0; iii < length_segments.quot; iii++)
		if (iii * 16 < TCPIP_MESSAGE_BUFFER_SIZE - 16)
			StratusDecryptData(&data[iii * 16], &decrypted_buffer[iii * 16]);

 return decrypted_buffer;
}

BYTE* CControllerApp::EncryptStratusData(BYTE *data, int *length)
{
 int iii;
 div_t length_segments;
 static BYTE encrypted_buffer[TCPIP_MESSAGE_BUFFER_SIZE];

	memset(encrypted_buffer, 0, sizeof(encrypted_buffer));

	if (*length > TCPIP_MESSAGE_BUFFER_SIZE - 16)
		*length = TCPIP_MESSAGE_BUFFER_SIZE - 16;

	memcpy(encrypted_buffer, data, *length);

	length_segments = div(*length, 16);
	if (length_segments.rem)
	{
		*length += 16 - length_segments.rem;
		length_segments.quot++;
	}

	for (iii=0; iii < length_segments.quot; iii++)
		StratusEncryptData(&encrypted_buffer[iii * 16], &encrypted_buffer[iii * 16]);

 return encrypted_buffer;
}

bool CControllerApp::SendStratusMessage(BYTE *data, int length, bool controller_data, bool event_log_data)
{
 int bytes_sent, last_error;
 bool successful_send = false;
 WORD packet_length, packet_crc;
 BYTE *encrypted_data, stratus_tx_buffer[TCPIP_MESSAGE_BUFFER_SIZE];
 StratusProtocolHeader *tx_header = (StratusProtocolHeader*)stratus_tx_buffer;
 EventMessage event_message;
 CString error_message;
 static EventMessage last_event_message_logged;

    if (!stratusSocket.socket_state)
    {
        memset(stratus_tx_buffer, 0, sizeof(stratus_tx_buffer));

		if (!memory_settings_ini.masters_ip_address &&
			(memory_settings_ini.stratus_tcp_port == STRATUS_ENCRYPTED_PRODUCTION_PORT ||
			memory_settings_ini.stratus_tcp_port == STRATUS_ENCRYPTED_TEST_PORT ||
			memory_settings_ini.stratus_tcp_port == STRATUS_ENCRYPTED_DEVELOPMENT_PORT))
		{
			encrypted_data = EncryptStratusData(data, &length);
			memcpy(&stratus_tx_buffer[sizeof(StratusProtocolHeader)], encrypted_data, length);
		}
		else
		{
			encrypted_data = NULL;
			memcpy(&stratus_tx_buffer[sizeof(StratusProtocolHeader)], data, length);
		}

        packet_length = (WORD)(length + sizeof(StratusProtocolHeader));

        tx_header->header = PACKET_IDENTIFIER;
        tx_header->length = packet_length;
        tx_header->route_info = (WORD)controller_data;

        endian2(tx_header->length);
        endian2(tx_header->route_info);

        // calculate crc
        packet_crc = CalculateStratusCrc(stratus_tx_buffer, packet_length);
        endian2(packet_crc);

        // put the crc at the end of the packet data
        memcpy(&stratus_tx_buffer[packet_length], &packet_crc, 2);
        packet_length += 2;

		bytes_sent = stratusSocket.Send(stratus_tx_buffer, packet_length);

		if (bytes_sent == SOCKET_ERROR)
		{
			last_error = GetLastError();

			if (last_error != WSAEINPROGRESS && last_error != WSAEWOULDBLOCK)
			{
				error_message.Format("CControllerApp::SendStratusMessage - closing socket, error, %d.", last_error);
				logMessage(error_message);
				stratusSocket.Close();
				stratusSocket.socket_state = WSAENOTCONN;
			}
		}
		else
			successful_send = true;

        // log Stratus messages
        if (successful_send && event_log_data)
        {
            memset(&event_message, 0, sizeof(event_message));
            event_message.head.time_stamp = time(NULL);
            event_message.head.data_length = packet_length;

			if (encrypted_data)
				event_message.head.event_type = STRATUS_ENCRYPTED_TRANSMIT_MESSAGES;
			else
				event_message.head.event_type = STRATUS_TRANSMIT_MESSAGES;

            if (packet_length <= MAXIMUM_EVENT_LOG_DATA_SIZE)
                memcpy(event_message.data, stratus_tx_buffer, packet_length);
            else
            {
                memcpy(event_message.data, stratus_tx_buffer, MAXIMUM_EVENT_LOG_DATA_SIZE);
                logMessage("CControllerApp::SendStratusMessage - event log message exceeds maximum size.");
            }
            // make sure we only write an event once to the event log
            if ((event_message.head.event_type != last_event_message_logged.head.event_type) ||
                (event_message.head.time_stamp != last_event_message_logged.head.time_stamp) ||
                memcmp(&event_message.data, &last_event_message_logged.data, sizeof(EventMessage)))
            {
                event_message_queue.push(event_message);
                memcpy(&last_event_message_logged, &event_message, sizeof(EventMessage));
            }

        }
    }

 return successful_send;
}

// Broadcast date and time to all devices
void CControllerApp::GameDateAndTimeUpdate()
{
 BYTE *time_stamp = (BYTE*)SetTimeStamp();
 TxSerialMessage tx_message;
 CGameCommControl *pGameCommControl = pHeadGameCommControl;
 BYTE twenty_four_hour_hours;

    game_date_and_time_update = time(NULL) + 3600;  // add 1 hour

    mux_time_stamp.hundreths    = 0;
    mux_time_stamp.seconds      = ((time_stamp[12] - 48) << 4) + time_stamp[13] - 48;   // seconds
    mux_time_stamp.minutes      = ((time_stamp[10] - 48) << 4) + time_stamp[11] - 48;   // minutes
    mux_time_stamp.hours        = ((time_stamp[8]  - 48) << 4) + time_stamp[9]  - 48;   // hours
    twenty_four_hour_hours = mux_time_stamp.hours;
    mux_time_stamp.day_of_week  =  (time_stamp[14] - 48) | 0x10;                        // day of the week
    mux_time_stamp.day          = ((time_stamp[6]  - 48) << 4) + time_stamp[7]  - 48;   // day
    mux_time_stamp.month        = ((time_stamp[4]  - 48) << 4) + time_stamp[5]  - 48;   // month
    mux_time_stamp.year         = ((time_stamp[2]  - 48) << 4) + time_stamp[3]  - 48;   // year

    if (mux_time_stamp.hours > 0x12)
    {
        mux_time_stamp.hours = ((mux_time_stamp.hours >> 4) * 10) + (mux_time_stamp.hours & 0x0F);  // bcd_to_binary
        mux_time_stamp.hours -= 12;
        mux_time_stamp.hours = ((mux_time_stamp.hours / 10) << 4) + (mux_time_stamp.hours % 10);    // binary_to_bcd
        mux_time_stamp.hours |= 0xa0;
    }
    else
        mux_time_stamp.hours |= 0x80;

    memset(&tx_message, 0, sizeof(tx_message)); 
    memcpy(tx_message.msg.data, &mux_time_stamp, sizeof(mux_time_stamp));
    tx_message.msg.length = 62;
    tx_message.msg.command = DATE_TIME_NAME_ADDRESS;
    memcpy(&tx_message.msg.data[8], memory_location_config.name, 17);
    memcpy(&tx_message.msg.data[26], memory_location_config.address, 29);

    while (pGameCommControl)
    {

        if (pGameCommControl->sas_com)
            pGameCommControl->sas_com->sendDateAndTimeMessage (mux_time_stamp.month,
                                                               mux_time_stamp.day,
                                                               mux_time_stamp.year,
                                                               twenty_four_hour_hours,
                                                               mux_time_stamp.minutes,
                                                               mux_time_stamp.seconds);
        else
            pGameCommControl->broadcast_queue.push(tx_message);

        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }
}

// ticket paid information
// tx: ticket id(long), social security number(long), pay flag(char), drivers license(char[16])
void CControllerApp::PaidTicketsTransmit()
{
 char drivers_license[50];
 DWORD iii, file_index;
 FilePaidTicket file_paid_ticket;
 CGameDevice *pGameDevice;
 TxSerialMessage game_msg;
 StratusMessageFormat stratus_msg;

    paid_tickets_transmit = time(NULL) + 30;

    file_index = 0;
    while (fileRead(PAID_TICKETS_TX, file_index, &file_paid_ticket, sizeof(file_paid_ticket)))
    {
        if (memory_settings_ini.masters_ip_address)
        {
            memset(&stratus_msg, 0, sizeof(stratus_msg));
            stratus_msg.send_code = SLAVE_TKT_TICKET_PAID;
            stratus_msg.task_code = SLAVE_TKT_TASK_TICKET;
            stratus_msg.ucmc_id = file_paid_ticket.memory.memory_game_lock.ucmc_id;
            stratus_msg.message_length = sizeof(file_paid_ticket.memory) + STRATUS_HEADER_SIZE;
            stratus_msg.property_id = memory_location_config.property_id;
            stratus_msg.serial_port = 1;
            stratus_msg.data_to_event_log = true;
            file_paid_ticket.memory.dispenser_ucmc_id = (BYTE)memory_dispenser_config.ucmc_id;
            memcpy(stratus_msg.data, &file_paid_ticket.memory, sizeof(file_paid_ticket.memory));
            QueueStratusTxMessage(&stratus_msg, FALSE);
        }
        else
        {
            pGameDevice = GetGamePointer(file_paid_ticket.memory.memory_game_lock.ucmc_id);

            if (!pGameDevice)
                logMessage("CControllerApp::PaidTicketsTransmit - NULL game pointer.");
            else
            {
                memset(&game_msg, 0, sizeof(game_msg));
                game_msg.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                game_msg.msg.length = 10;
                game_msg.msg.command = CLEAR_TICKET_FROM_GAME;
                memcpy(game_msg.msg.data, &file_paid_ticket.memory.memory_game_lock.ticket_id, 4);
                pGameDevice->transmit_queue.push(game_msg);
            }

			if (pGameDevice && pGameDevice->memory_game_config.club_code)
				stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
			else
				stratus_msg.club_code = memory_location_config.club_code;

            memset(&stratus_msg, 0, sizeof(stratus_msg));
            stratus_msg.send_code = TKT_TICKET_PAID;
            stratus_msg.task_code = TKT_TASK_TICKET;
            stratus_msg.ucmc_id = file_paid_ticket.memory.memory_game_lock.ucmc_id;
            stratus_msg.message_length = 25 + STRATUS_HEADER_SIZE;
            stratus_msg.property_id = memory_location_config.property_id;
            stratus_msg.serial_port = 1;
            stratus_msg.data_to_event_log = true;
            stratus_msg.customer_id = file_paid_ticket.memory.memory_game_lock.location_id;

            // ticket id
            endian4(file_paid_ticket.memory.memory_game_lock.ticket_id);
            memcpy(stratus_msg.data, &file_paid_ticket.memory.memory_game_lock.ticket_id, 4);

            // social security number
            endian4(file_paid_ticket.memory.memory_game_lock.ss_number);
            memcpy(&stratus_msg.data[4], &file_paid_ticket.memory.memory_game_lock.ss_number, 4);

            stratus_msg.data[8] = '1';  // pay flag

            // drivers license
            memset(drivers_license, 0, sizeof(drivers_license));
			_itoa_s(file_paid_ticket.memory.memory_game_lock.drivers_license, drivers_license, sizeof(drivers_license), 10);

            for (iii=0; iii < 16; iii++)
            {
                // Stratus needs spaces in unused fields for driver's license
                if (drivers_license[iii])
                    stratus_msg.data[iii + 9] = drivers_license[iii];
                else
                    stratus_msg.data[iii + 9] = ' ';
            }
            stratus_msg.data[24] = 0;   // null terminate the string

            QueueStratusTxMessage(&stratus_msg, TRUE);
        }

        file_index++;
    }

    if (file_index)
        DeleteFile(PAID_TICKETS_TX);
}

void CControllerApp::GameMuxMessageUpdate()
{
 int file_index, segments, iii;
 div_t figure_segments;
 TxSerialMessage tx_message;
 MuxFileMessages file_message;
 CGameCommControl *pGameCommControl;

    game_mux_message_update = time(NULL) + 900; // 15 minutes

    if (GetFileAttributes(MUX_MESSAGES) != 0xFFFFFFFF)
    {
        file_index = 0;

        while (fileRead(MUX_MESSAGES, file_index, &file_message, sizeof(file_message)))
        {
            file_index++;

            if (memory_location_config.club_code == LONGHORN_BIGHORN_CLUB &&
                file_message.index && strlen(file_message.string))
            {
                figure_segments = div((int)strlen(file_message.string), 64);    // determine message segments
                segments = figure_segments.quot;

                if (figure_segments.rem)
                    segments++;

                if (segments > 4)
                    segments = 4;
                else
                    if (segments <= 0)
                        segments = 1;

                for (iii=0; iii < segments; iii++)
                {
                    memset(&tx_message, 0, sizeof(tx_message));
                    tx_message.msg.length = 75;
                    tx_message.msg.command = COMMAND_HEADER;
                    tx_message.msg.data[0] = DISPLAYED_MESSAGES;
                    tx_message.msg.data[1] = (BYTE)(file_message.index - 1);	// message index from file
                    tx_message.msg.data[2] = iii;   // message segment piece
                    memcpy(&tx_message.msg.data[5], &file_message.string[iii * 64], 64);    // message data

                    // last message segment flag
                    if (segments == iii + 1)
                    {
                        tx_message.msg.data[3] = 1;
                        tx_message.msg.data[4] = CalculateMuxCrc((BYTE*)file_message.string, (BYTE)strlen(file_message.string));
                    }

                    pGameCommControl = pHeadGameCommControl;
                    while (pGameCommControl)
                    {
                        pGameCommControl->broadcast_queue.push(tx_message);
                        pGameCommControl = pGameCommControl->pNextGameCommControl;
                    }
                }
            }
        }
    }
}

bool CControllerApp::RemoveGame(DWORD *pUcmcId)
{
 int file_index;
 FileGameConfig file_game_config;
 CGameCommControl *pGameCommControl = pHeadGameCommControl;
 CGameCommControl *pPreviousGameCommControl = pHeadGameCommControl;
 CGameDevice *pGameDevice = NULL;
 CGameDevice *pPreviousGameDevice = NULL;
 bool game_removed = FALSE;
 bool successfully_removed = FALSE;

    while (pGameCommControl)
    {
        pGameDevice = pGameCommControl->pHeadGameDevice;
        pPreviousGameDevice = NULL;
        game_removed = FALSE;

        while (pGameDevice)
        {
            if (pGameDevice->memory_game_config.ucmc_id == *pUcmcId)
            {
                // make sure we are not polling the game while we are removing
                pGameCommControl->pGameDeviceCurrentPollingGame = 0;
                // remove game from memory
                if (pPreviousGameDevice)
                    pPreviousGameDevice->pNextGame = pGameDevice->pNextGame;
                else
                    pGameCommControl->pHeadGameDevice = pGameDevice->pNextGame;

                delete pGameDevice;
                pGameDevice = NULL;

                DeleteFile(GAME_INFO_TEMP);
                file_index = 0;

                while (fileRead(GAME_INFO, file_index, &file_game_config, sizeof(file_game_config)))
                {
                    if (file_game_config.memory.ucmc_id != *pUcmcId)
                        fileWrite(GAME_INFO_TEMP, -1, &file_game_config, sizeof(file_game_config));

                    file_index++;
                }

                DeleteFile(GAME_INFO);
                MoveFile(GAME_INFO_TEMP, GAME_INFO);

                game_removed = TRUE;
                successfully_removed = TRUE;

                break;

            }

            pPreviousGameDevice = pGameDevice;
            if (pGameDevice)
                pGameDevice = pGameDevice->pNextGame;
        }
        // if there is no games in the list, remove the GameCommControl object from the linked list
        if (game_removed && (pGameCommControl->pHeadGameDevice == NULL))
        {
            if (pPreviousGameCommControl && (pPreviousGameCommControl->pNextGameCommControl == pGameCommControl))
            {
                pPreviousGameCommControl->pNextGameCommControl = pGameCommControl->pNextGameCommControl;
                RemoveGameCommControl(pGameCommControl);
            }

            pGameCommControl = pPreviousGameCommControl->pNextGameCommControl;
        }
        else
        {
            pPreviousGameCommControl = pGameCommControl;
            pGameCommControl = pGameCommControl->pNextGameCommControl;
        }

    }

    if (successfully_removed)
        SendAllMachineSpecificData();

 return successfully_removed;
}

void CControllerApp::SendFilesToCustomerControlServer()
{
 #define NUMBER_OF_FILES_TO_BACKUP  14
 char backup_filenames[NUMBER_OF_FILES_TO_BACKUP][MAXIMUM_FILE_PATH_NAME + 1];
 BYTE *file_data;
 DWORD file_data_size = 0, file_size, bytes_to_send, crc;
 int iii;
 CString error_message;
 SocketMessage socket_message;
 FileBackup file_backup;
// FileSettingsIni file_settings;
// FileLocationConfig file_location_config;
#if 0
 #pragma pack(1)
 struct
 {
    DWORD customer_id;
    WORD  property_id;
    char  name[32];
    char  address[32];
    WORD  customer_control_client_port;
 } location_info;
 #pragma pack()
#endif

    memset(&backup_filenames, 0, sizeof(backup_filenames));
    memcpy(backup_filenames[0], GAME_INFO, strlen(GAME_INFO));
    memcpy(backup_filenames[1], MAIN_SETTINGS_INI, strlen(MAIN_SETTINGS_INI));
    memcpy(backup_filenames[2], LOCATION_INFO, strlen(LOCATION_INFO));
    memcpy(backup_filenames[3], DISPENSER_INFO, strlen(DISPENSER_INFO));
    memcpy(backup_filenames[4], DISPENSER_VALUES, strlen(DISPENSER_VALUES));
    memcpy(backup_filenames[5], MASTER_CONFIG, strlen(MASTER_CONFIG));
    memcpy(backup_filenames[6], LOCATION_IDS, strlen(LOCATION_IDS));
    memcpy(backup_filenames[7], USER_IDS_PASSWORDS, strlen(USER_IDS_PASSWORDS));
    memcpy(backup_filenames[8], ENGR_IDS_PASSWORDS, strlen(ENGR_IDS_PASSWORDS));
    memcpy(backup_filenames[9], GAME_LOCK_SOUND, strlen(GAME_LOCK_SOUND));
    memcpy(backup_filenames[10], MUX_MESSAGES, strlen(MUX_MESSAGES));
    memcpy(backup_filenames[11], EMAIL_ADDRESS_LIST, strlen(EMAIL_ADDRESS_LIST));
    memcpy(backup_filenames[12], GB_PRO_SETTINGS_INI, strlen(GB_PRO_SETTINGS_INI));
    memcpy(backup_filenames[13], GB_PRO_PROMOTION_CODES_FILE, strlen(GB_PRO_PROMOTION_CODES_FILE));

    for (iii=0; iii < NUMBER_OF_FILES_TO_BACKUP; iii++)
    {
        file_size = FileByteTotal(backup_filenames[iii]);
        if (file_size)
        {
            if (file_size > file_data_size)
            {
                if (file_data_size)
                    delete[] file_data;

                file_data_size = file_size;
                file_data = new BYTE[file_data_size];
            }

            if (fileRead(backup_filenames[iii], 0, file_data, file_size))
            {
                bytes_to_send = file_size;
                memset(&file_backup, 0, sizeof(file_backup));
                file_backup.head.file_size = file_size;
                file_backup.head.filename_size = strlen(backup_filenames[iii]);
                memcpy(file_backup.filename, backup_filenames[iii], file_backup.head.filename_size);

                crc = GetCrc32(&file_data[0], file_size);

                while (bytes_to_send)
                {
                    if (bytes_to_send > MAXIMUM_FILE_BACKUP_DATA_SIZE)
                        file_backup.head.file_data_size = MAXIMUM_FILE_BACKUP_DATA_SIZE;
                    else
                        file_backup.head.file_data_size = (WORD)bytes_to_send;

                    memcpy(file_backup.file_data, &file_data, file_backup.head.file_data_size);
                    bytes_to_send -= file_backup.head.file_data_size;

                    if (bytes_to_send)
                        memmove(&file_data, &file_data[file_backup.head.file_data_size], bytes_to_send);
                    else
                        file_backup.head.file_crc = crc;

                    memset(&socket_message, 0, sizeof(socket_message));
                    socket_message.head.command = CC_SERVER_FILE_BACKUP;
                    socket_message.head.data_length = (DWORD)(sizeof(file_backup.head) + file_backup.head.filename_size +
                        file_backup.head.file_data_size);
                    memcpy(socket_message.data, file_backup.filename, file_backup.head.filename_size);
                    memcpy(&socket_message.data[file_backup.head.filename_size], file_backup.file_data, file_backup.head.file_data_size);
//?????work                    customer_control_server_tx_queue.push(socket_message);
                }
            }
        }
    }

    if (file_data_size)
        delete[] file_data;
}

void CControllerApp::SendControllerInfoToCcServer()
{
 FileSettingsIni file_settings;
 FileLocationConfig file_location_config;
 SocketMessage socket_message;

    if (!fileRead(MAIN_SETTINGS_INI, 0, &file_settings, sizeof(file_settings)))
        memset(&file_settings, 0, sizeof(file_settings));

    if (!fileRead(LOCATION_INFO, 0, &file_location_config, sizeof(file_location_config)))
        memset(&file_location_config, 0, sizeof(file_location_config));

    memset(&socket_message, 0, sizeof(socket_message));
    socket_message.head.command = CC_SERVER_COMPUTER_INFORMATION;
    socket_message.head.data_length = sizeof(file_settings) + sizeof(file_location_config);
    memcpy(socket_message.data, &file_settings, sizeof(file_settings));
    memcpy(&socket_message.data[sizeof(file_settings)], &file_location_config, sizeof(file_location_config));
    customer_control_server_tx_queue.push(socket_message);
}

void CControllerApp::ProcessDeviceMessage(StratusMessageFormat *device_message)
{
 DWORD time_played;
 CString log_message;
 CGameCommControl *pGameCommControl;
 EventMessage event_message;
 StratusMessageFormat stratus_msg;
 static BingoFrenzy bingo_frenzy;
 static CashFrenzy cash_frenzy;
 StratusMarkerCreditResponse* stratus_marker_credit_response_ptr;
 WORD send_code;

    endian2(device_message->message_length);
    endian2(device_message->task_code);
    endian2(device_message->property_id);
    endian2(device_message->club_code);
    endian2(device_message->serial_port);
    endian2(device_message->mux_id);
    endian2(device_message->send_code);
    endian4(device_message->customer_id);
    endian4(device_message->ucmc_id);

    switch (device_message->task_code)
    {
        case MPI_TASK_PLAYER_ACCOUNTING:
            switch (device_message->send_code)
            {
                case MPI_ACCOUNT_LOGIN:
                case MPI_NEW_ACCOUNT_LOGIN:
                    if (memory_settings_ini.stratus_tcp_port == STRATUS_TEST_PORT && memory_location_config.allow_player_autologout_for_test == 1)
                    {
                        memset(&stratus_msg, 0, sizeof(stratus_msg));

                        // set non-zero value for time played to log message on Stratus
                        time_played = 1;
                        endian4(time_played);
                        memcpy(&stratus_msg.data[26], &time_played, 4);

                        memcpy(stratus_msg.data, device_message->data, 4);              // account
                        memcpy(&stratus_msg.data[30], &device_message->data[40], 4);    // primary game card handle
                        memcpy(&stratus_msg.data[34], &device_message->data[44], 4);    // primary game card games
                        memcpy(&stratus_msg.data[38], &device_message->data[48], 4);    // primary game card marks
                        memcpy(&stratus_msg.data[46], &device_message->data[52], 4);    // secondary game card handle
                        memcpy(&stratus_msg.data[50], &device_message->data[56], 4);    // secondary game card games
                        memcpy(&stratus_msg.data[54], &device_message->data[60], 4);    // secondary game card marks
                        memcpy(&stratus_msg.data[58], SetTimeStamp(), 14);              // time stamp
                        stratus_msg.data[44] = '9';                                     // card type
                        stratus_msg.data[45] = ' ';                                     // game card type

                        stratus_msg.send_code = MPI_ACCOUNT_LOGOUT;
                        stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
                        stratus_msg.message_length = 72 + STRATUS_HEADER_SIZE;
                        stratus_msg.customer_id = device_message->customer_id;
                        stratus_msg.property_id = device_message->property_id;
                        stratus_msg.club_code = device_message->club_code;
                        stratus_msg.serial_port = device_message->serial_port;
                        stratus_msg.ucmc_id = device_message->ucmc_id;
                        stratus_msg.data_to_event_log = true;
                        QueueStratusTxMessage(&stratus_msg, TRUE);
                    }
                    pGameCommControl = GetGameCommControlPointer(device_message->ucmc_id);

                    if (pGameCommControl)
                        pGameCommControl->stratus_mpi_rx_queue.push(*device_message);
                    else
                    {
                        log_message.Format("CControllerApp::ProcessDeviceMessage - MPI, ucmc id, %u, NULL port pointer.",
                            device_message->ucmc_id);
                        logMessage(log_message);
                    }
                    break;
                // MarkerTrax messages just between the controller and the Stratus
                case MPI_MARKER_CREDIT_PENDING:
                case MPI_MARKER_AFT_COMPLETE:
                    if (!marker_stratus_tx_queue.empty())
                    {
                        stratus_msg = marker_stratus_tx_queue.front();
                        endian2(stratus_msg.send_code);
                        endian4(stratus_msg.ucmc_id);

                        if ((stratus_msg.send_code == device_message->send_code) && (stratus_msg.ucmc_id == device_message->ucmc_id))
                            marker_stratus_tx_queue.pop();
                    }
                    marker_message_recieved = TRUE;
                    break;







                // MarkerTrax messages going back to the game

                case MPI_MARKER_CREDIT:
                    marker_credit_reply = true;

                    stratus_marker_credit_response_ptr = (StratusMarkerCreditResponse*)device_message->data;
                    memcpy(&stratus_marker_credit_response, stratus_marker_credit_response_ptr, sizeof(stratus_marker_credit_response));
                    endian4(stratus_marker_credit_response.marker_balance);
                    endian4(stratus_marker_credit_response.credit_amount);

                case MPI_ABORT_MARKER_CREDIT:
                case MPI_MARKER_REQUEST:
                case MPI_ABORT_MARKER_ADVANCE_REQUEST:
                    if (!marker_stratus_tx_queue.empty())
                    {
                        stratus_msg = marker_stratus_tx_queue.front();
                        endian2(stratus_msg.send_code);
                        endian4(stratus_msg.ucmc_id);

                        if ((stratus_msg.send_code == device_message->send_code) && (stratus_msg.ucmc_id == device_message->ucmc_id))
                            marker_stratus_tx_queue.pop();
                    }
                    marker_message_recieved = TRUE;


                    pGameCommControl = GetGameCommControlPointer(device_message->ucmc_id);

                    if (pGameCommControl)
//                        pGameCommControl->stratus_mpi_rx_queue.push(*device_message);
                        pGameCommControl->stratus_mpi_rx_marker_queue.push(*device_message);
                    else
                    {
                        log_message.Format("CControllerApp::ProcessDeviceMessage - MPI, ucmc id, %u, NULL port pointer.",
                            device_message->ucmc_id);
                        logMessage(log_message);
                    }
                    break;

				case MPI_VALIDATE_MEMBER_ID:                    
                case MPI_REDEEM_INQUIRY:
                case MPI_REDEEM_REQUEST:
                case MPI_REDEEM_COMPLETE:
                case MPI_NEW_REDEEM_REQUEST:
                case MPI_WINNING_BINGO_CARD:
                    pGameCommControl = GetGameCommControlPointer(device_message->ucmc_id);

                    if (pGameCommControl)
                        pGameCommControl->stratus_mpi_rx_queue.push(*device_message);
                    else
                    {
                        log_message.Format("CControllerApp::ProcessDeviceMessage - MPI, ucmc id, %u, NULL port pointer.",
                            device_message->ucmc_id);
                        logMessage(log_message);
                    }
                    break;
                case MPI_ACCOUNT_LOGOUT:
                case MPI_WINNING_BINGO_HAND:
                case MPI_CLEAR_BINGO_CARD:
                case MPI_JACKPOT:
                case MPI_GB_PRO_POINTS_NEW_MSG:
                case MPI_NEW_WINNING_BINGO_HAND:
				case MPI_NEW_CLEAR_BINGO_CARD:
                    if (!stratus_tx_queue.empty())
                    {
                        stratus_msg = stratus_tx_queue.front();
                        endian2(stratus_msg.send_code);
                        endian4(stratus_msg.ucmc_id);

                        if ((stratus_msg.send_code == device_message->send_code) && (stratus_msg.ucmc_id == device_message->ucmc_id))
//                        if ((stratus_msg.send_code == device_message->send_code))
                        {
                            stratus_tx_queue.pop();
					        PurgePendingStratusTxMessage(&stratus_msg);
							stratus_tx_message_ack_pending = FALSE;
                            erroneous_ack_count = 0;
                        }
                        else
                        {
                            erroneous_ack_count++;
                            if (erroneous_ack_count > 10)
                            {
                                stratus_tx_queue.pop();
					            PurgePendingStratusTxMessage(&stratus_msg);
							    stratus_tx_message_ack_pending = FALSE;
                                erroneous_ack_count = 0;
                                log_message.Format("CControllerApp::ProcessDeviceMessage - erroneous_ack_count > 10 command code for send code: %u.",
                                    device_message->send_code);
                                logMessage(log_message);
                            }
                        }
                    }
					else
                        stratus_tx_message_ack_pending = FALSE;
					break;
                case MPI_4OK_WIN:
                    break;
                default:
                    log_message.Format("CControllerApp::ProcessDeviceMessage - Stratus RX TCP/IP invalid command code for MPI: %u.",
                        device_message->send_code);
                    logMessage(log_message);
                    return;
            }
            break;

        case SMPI_TASK_PLAYER_ACCOUNTING:
            switch (device_message->send_code)
            {
                case MPI_ACCOUNT_LOGIN_INFO:
                case MPI_GB_SESSION_UPDATE:
				case MPI_GB_SESSION_INFO_REQUEST:
                    pGameCommControl = GetGameCommControlPointer(device_message->ucmc_id);

                    if (pGameCommControl)
                        pGameCommControl->stratus_mpi_rx_queue.push(*device_message);
                    else
                    {
                        log_message.Format("CControllerApp::ProcessDeviceMessage - MPI, ucmc id, %u, NULL port pointer.",
                            device_message->ucmc_id);
                        logMessage(log_message);
                    }
                    break;

                case MPI_ACCOUNT_LOGOUT:
                case MPI_WINNING_BINGO_HAND:
                case MPI_CLEAR_BINGO_CARD:
                case MPI_GB_PRO_POINTS_NEW_MSG:
                case MPI_NEW_WINNING_BINGO_HAND:
				case MPI_NEW_CLEAR_BINGO_CARD:
//                    log_message.Format("MPI_ACCOUNT_LOGOUT reply from stratus: %u.", device_message->send_code);
//                    logMessage(log_message);
                    if (!stratus_tx_queue.empty())
                    {
                        stratus_msg = stratus_tx_queue.front();
                        endian2(stratus_msg.send_code);
                        endian4(stratus_msg.ucmc_id);

                        if ((stratus_msg.send_code == device_message->send_code) && (stratus_msg.ucmc_id == device_message->ucmc_id))
//                        if ((stratus_msg.send_code == device_message->send_code))
                        {
                            stratus_tx_queue.pop();
					        PurgePendingStratusTxMessage(&stratus_msg);
                            stratus_tx_message_ack_pending = FALSE;
                        }
                        else
                        {
                            erroneous_ack_count++;
                            if (erroneous_ack_count > 10)
                            {
                                stratus_tx_queue.pop();
					            PurgePendingStratusTxMessage(&stratus_msg);
							    stratus_tx_message_ack_pending = FALSE;
                                erroneous_ack_count = 0;
                                log_message.Format("CControllerApp::ProcessDeviceMessage - erroneous_ack_count > 10 command code for send code: %u.",
                                    device_message->send_code);
                                logMessage(log_message);
                            }
                        }
                    }
					else
                        stratus_tx_message_ack_pending = FALSE;
                    break;

                case MPI_WINNING_BINGO_CARD:
                    pGameCommControl = GetGameCommControlPointer(device_message->ucmc_id);

                    if (pGameCommControl)
                        pGameCommControl->stratus_mpi_rx_queue.push(*device_message);
                    else
                    {
                        log_message.Format("CControllerApp::ProcessDeviceMessage - MPI, ucmc id, %u, NULL port pointer.",
                            device_message->ucmc_id);
                        logMessage(log_message);
                    }
                    break;

                default:
                    log_message.Format("CControllerApp::ProcessDeviceMessage - Stratus RX TCP/IP invalid command code for MPI: %u.",
                        device_message->send_code);
                    logMessage(log_message);
                    return;
            }
            break;


		// frenzy awards have been discontinued
        case WAA_TASK_WIDE_AREA_AWARD:
			return;
#if 0
            switch (device_message->send_code)
            {
 				case WAA_CASH_FRENZY_JACKPOT:
                case WAA_JACKPOT:
                    pGameCommControl = GetGameCommControlPointer(device_message->ucmc_id);

                    if (pGameCommControl)
                        pGameCommControl->stratus_waa_rx_queue.push(*device_message);
                    else
                    {
						if (device_message->send_code == WAA_JACKPOT)
	                        log_message.Format("CControllerApp::ProcessDeviceMessage - WAA_JACKPOT, ucmc id, %u, NULL port pointer.",
								device_message->ucmc_id);
						else
    	                    log_message.Format("CControllerApp::ProcessDeviceMessage - WAA_CASH_FRENZY_JACKPOT, ucmc id, %u, NULL port pointer.",
	                            device_message->ucmc_id);

                        logMessage(log_message);
                    }
                    break;

				case WAA_CASH_FRENZY_QUERY_AFTER_WIN:
				case WAA_CASH_FRENZY_QUERY:
                case WAA_QUERY_AFTER_WIN:
                case WAA_QUERY:
                    pGameCommControl = pHeadGameCommControl;

                    while (pGameCommControl)
                    {
                        pGameCommControl->stratus_waa_rx_queue.push(*device_message);
                        pGameCommControl = pGameCommControl->pNextGameCommControl;
                    }

                    if ((device_message->send_code == WAA_QUERY && !memcmp(&bingo_frenzy, device_message->data, sizeof(bingo_frenzy))) ||
						(device_message->send_code == WAA_CASH_FRENZY_QUERY && !memcmp(&cash_frenzy, device_message->data, 8)))
	                        return;
                    else
					{
						if (device_message->send_code == WAA_QUERY || device_message->send_code == WAA_QUERY_AFTER_WIN)
							memcpy(&bingo_frenzy, device_message->data, sizeof(bingo_frenzy));
						else
							memcpy(&cash_frenzy, device_message->data, sizeof(cash_frenzy));
					}
                    break;

                default:
                    log_message.Format("CControllerApp::ProcessDeviceMessage - Stratus RX TCP/IP invalid command code for WAA: %u.",
                        device_message->send_code);
                    logMessage(log_message);
                    return;
            }
            break;
#endif

        case TKT_TASK_TICKET:
            switch (device_message->send_code)
            {
                case TKT_NET_WIN_REPORT:
                    stratus_dialog_queue.push(*device_message);
                    break;

                // reply from Stratus, not used
                case TKT_TICKET_ADD:
                case TKT_TICKET_PAID:
                case TKT_FILL_MESSAGE:
                case TKT_SEND_BILL_COUNT:
                case TKT_JACKPOT_CREDIT:
				if (!stratus_tx_queue.empty())
                {
                    stratus_msg = stratus_tx_queue.front();
                    endian2(stratus_msg.send_code);
                    endian4(stratus_msg.ucmc_id);

                    if ((stratus_msg.send_code == device_message->send_code) && (stratus_msg.ucmc_id == device_message->ucmc_id))
                    {
                        stratus_tx_queue.pop();
					    PurgePendingStratusTxMessage(&stratus_msg);
						stratus_tx_message_ack_pending = FALSE;
                    }
                    else
                    {
                        erroneous_ack_count++;
                        if (erroneous_ack_count > 10)
                        {
                            stratus_tx_queue.pop();
					        PurgePendingStratusTxMessage(&stratus_msg);
							stratus_tx_message_ack_pending = FALSE;
                            erroneous_ack_count = 0;
                            log_message.Format("CControllerApp::ProcessDeviceMessage - erroneous_ack_count > 10 command code for send code: %u.",
                                device_message->send_code);
                            logMessage(log_message);
                        }
                    }

                }
				else
                    stratus_tx_message_ack_pending = FALSE;
                break;

                default:
                    log_message.Format("CControllerApp::ProcessDeviceMessage - Stratus RX TCP/IP invalid command code for TKT: %u.",
                        device_message->send_code);
                    logMessage(log_message);
                    return;
            }
            break;

        case SLAVE_TKT_TASK_TICKET:
            switch (device_message->send_code)
            {
                case SLAVE_TKT_TICKET_QUERY:
                case SLAVE_TKT_GAME_LOCK_QUERY:
                case SLAVE_TKT_SYSTEM_LOGIN:
                    stratus_dialog_queue.push(*device_message);
                    break;

                default:
                    log_message.Format("CControllerApp::ProcessDeviceMessage - Slave RX TCP/IP invalid command code for SLAVE TKT: %u.",
                        device_message->send_code);
                    logMessage(log_message);
                    return;
            }
            break;

        default:
            log_message.Format("CControllerApp::ProcessDeviceMessage - Stratus RX TCP/IP invalid task code: %u.", device_message->task_code);
            logMessage(log_message);
            return;
    }

    // log Stratus messages
    memset(&event_message, 0, sizeof(event_message));
    event_message.head.header = LONG_HEADER_IDENTIFIER;
    event_message.head.customer_id = device_message->customer_id;
    event_message.head.port_number = device_message->serial_port;
    event_message.head.time_stamp = time(NULL);
    event_message.head.event_type = STRATUS_RECEIVE_MESSAGES;
    event_message.head.data_length = device_message->message_length;

    if (event_message.head.data_length <= sizeof(StratusMessageFormat))
        memcpy(event_message.data, device_message, event_message.head.data_length);
    else
        memcpy(event_message.data, device_message, sizeof(StratusMessageFormat));

    event_message_queue.push(event_message);
}

void CControllerApp::CheckCustomerControlServerRx()
{
 DWORD calculated_crc, packet_crc;
 CString message;
 SocketMessage *socket_message;
 int bytes_received = customerControlServerSocket.GetBuffer(&customer_control_server_rx.data[customer_control_server_rx.index],
    TCPIP_MESSAGE_BUFFER_SIZE - customer_control_server_rx.index);

	if (bytes_received)
    {
        customer_control_server_rx.index += bytes_received;
        socket_message = (SocketMessage*)customer_control_server_rx.data;

        while (customer_control_server_rx.index >= sizeof(socket_message->head) + socket_message->head.data_length &&
            socket_message->head.data_length >= 4)
        {
            if (socket_message->head.header != LONG_HEADER_IDENTIFIER)
            {
                memset(&customer_control_server_rx, 0, sizeof(customer_control_server_rx));
                logMessage("CControllerApp::CheckCustomerControlServerRx - invalid packet header.");
            }
            else
            {
                socket_message->head.data_length -= 4;  // remove CRC

                // calculate and compare crc
                memcpy(&packet_crc, &socket_message->data[socket_message->head.data_length], 4);
                calculated_crc = GetCrc32((BYTE*)socket_message, sizeof(socket_message->head) + socket_message->head.data_length);
                if (calculated_crc != packet_crc)
                {
                    message.Format("CControllerApp::CheckCustomerControlServerRx - CRC mismatch, calcuated: 0x%X, rx: 0x%X.", calculated_crc, packet_crc);
                    logMessage(message);
                }
                else
                    ProcessCustomerControlServerRx(socket_message);

                bytes_received = sizeof(socket_message->head) + socket_message->head.data_length + 4;
                if (customer_control_server_rx.index == bytes_received)
                    memset(&customer_control_server_rx, 0, sizeof(customer_control_server_rx));
                else
                {
                    memset(customer_control_server_rx.data, 0, bytes_received);
                    customer_control_server_rx.index -= bytes_received;
                    memmove(customer_control_server_rx.data, &customer_control_server_rx.data[bytes_received], customer_control_server_rx.index);
                    memset(&customer_control_server_rx.data[customer_control_server_rx.index], 0, bytes_received);
                }
            }
        }
    }
}

void CControllerApp::ProcessCustomerControlServerEventLoggingResponse()
{
 EventMessage event_message;
 FILE *file_handle2, *file_handle;

	fopen_s(&file_handle, TRANSMIT_EVENTS_FILE, "rb");

	if (file_handle)
	{
		if (!fseek(file_handle, 0, SEEK_SET) && fread(&event_message, sizeof(event_message.head), 1, file_handle) &&
			fread(event_message.data, event_message.head.data_length, 1, file_handle))
		{
			fopen_s(&file_handle2, TEMP_FILE, "wb");

            if (file_handle2)
			{
				while (fread(&event_message, sizeof(event_message.head), 1, file_handle) &&
					fread(event_message.data, event_message.head.data_length, 1, file_handle))
				{
					if (!fwrite(&event_message, sizeof(event_message.head) + event_message.head.data_length, 1, file_handle2))
					{
						fclose(file_handle);
						file_handle = NULL;
						break;
					}
				}

            	fclose(file_handle2);
			}
			else
			{
	         	fclose(file_handle);
				file_handle = NULL;
			}
		}
		else
		{
         	fclose(file_handle);
			file_handle = NULL;
		}
	}

	if (file_handle)
	{
		fclose(file_handle);
		DeleteFile(TRANSMIT_EVENTS_FILE);
		MoveFile(TEMP_FILE, TRANSMIT_EVENTS_FILE);
	}
	else
		logMessage("CControllerApp::ProcessCustomerControlServerEventLoggingResponse - error deleting event.");

	last_tx_event_message_time = 0;
}

void CControllerApp::ProcessCustomerControlServerRx(SocketMessage *socket_message)
{
 CString error_message;

    switch (socket_message->head.command)
    {
        case CC_SERVER_COMPUTER_INFORMATION:
            SendControllerInfoToCcServer();
            break;

		case CC_SERVER_CONTROLLER_EVENT:
			ProcessCustomerControlServerEventLoggingResponse();
			break;

        default:
            error_message.Format("CControllerApp::ProcessCustomerControlServerRx - invalid command: %u.", socket_message->head.command);
            logMessage(error_message);
            break;
    }

#if 0
????????????work
        DWORD   socket_message->head.header;
        WORD    socket_message->head.command;
        DWORD   socket_message->head.data_length;
    char socket_message->data[TCPIP_MESSAGE_BUFFER_SIZE];
??????????????

#endif
}

void CControllerApp::PurgePendingStratusTxMessage (StratusMessageFormat* stratus_message)
{
	StratusMessageFormat stratus_message_on_disk;
    DWORD total_messages = fileRecordTotal(PENDING_STRATUS_TX_MESSAGES, sizeof(StratusMessageFormat));
    DWORD i;

    for (i=0; i < total_messages; i++)
    {
    	memset(&stratus_message_on_disk, 0, sizeof(StratusMessageFormat));

		if (fileRead(PENDING_STRATUS_TX_MESSAGES, i, &stratus_message_on_disk, sizeof(StratusMessageFormat)))
		{
			endian2(stratus_message_on_disk.send_code);
			endian4(stratus_message_on_disk.ucmc_id);
			if ((stratus_message->send_code == stratus_message_on_disk.send_code))
            {
				if (stratus_message->ucmc_id == stratus_message_on_disk.ucmc_id)
					fileDeleteRecord(PENDING_STRATUS_TX_MESSAGES, sizeof(StratusMessageFormat), i);
				else
	                logMessage("CControllerApp::PurgePendingStratusTxMessage - ucmc_mismatch.");
                break;
            }
		}
    }

	total_messages = fileRecordTotal(PENDING_STRATUS_TX_MESSAGES, sizeof(StratusMessageFormat));
	if (!total_messages)
		DeleteFile(PENDING_STRATUS_TX_MESSAGES);
}

void CControllerApp::QueuePendingStratusTxMessages (void)
{
	StratusMessageFormat stratus_message_on_disk;
    DWORD total_messages = fileRecordTotal(PENDING_STRATUS_TX_MESSAGES, sizeof(StratusMessageFormat));
    DWORD i;

	if (!total_messages)
		DeleteFile(PENDING_STRATUS_TX_MESSAGES);
	else
	{
        for (i=0; i < total_messages; i++)
        {
    		memset(&stratus_message_on_disk, 0, sizeof(StratusMessageFormat));

			if (fileRead(PENDING_STRATUS_TX_MESSAGES, i, &stratus_message_on_disk, sizeof(StratusMessageFormat)))
			{
    			stratus_tx_queue.push(stratus_message_on_disk);

				endian2(stratus_message_on_disk.send_code);
				endian2(stratus_message_on_disk.mux_id);
				endian4(stratus_message_on_disk.ucmc_id);
			}
        }
	}
}

void CControllerApp::CheckStratusRxSocket()
{
 WORD calculated_crc, packet_crc;
 CString message;
 StratusProtocolHeader *header;
 StratusMessageFormat device_message;
 int bytes_received = stratusSocket.GetBuffer(&stratus_tcp_ip_rx.data[stratus_tcp_ip_rx.index],
    TCPIP_MESSAGE_BUFFER_SIZE - stratus_tcp_ip_rx.index);

	if (bytes_received)
	{
        stratus_tcp_ip_rx.index += bytes_received;

        while (stratus_tcp_ip_rx.index >= sizeof(StratusProtocolHeader))
        {
            header = (StratusProtocolHeader*)stratus_tcp_ip_rx.data;

            if (header->header != PACKET_IDENTIFIER)
            {
                memset(&stratus_tcp_ip_rx, 0, sizeof(stratus_tcp_ip_rx));
                logMessage("CControllerApp::CheckStratusRxSocket - incorrect packet header.");
            }
            else
            {
                endian2(header->length);
                bytes_received = header->length + 2;   // add crc bytes

                if (stratus_tcp_ip_rx.index < bytes_received)
                {
                    endian2(header->length);
                    return;
                }
                else
                {
                    endian2(header->route_info);

                    // calculate and compare crc
                    memcpy(&packet_crc, &stratus_tcp_ip_rx.data[header->length], 2);
                    endian2(packet_crc);
                    calculated_crc = CalculateStratusCrc(stratus_tcp_ip_rx.data, header->length);

                    if (calculated_crc != packet_crc)
                        logMessage("CControllerApp::CheckStratusRxSocket - CRC mismatch.");
                    else
                    {
                        // reduce length by size of header
                        header->length -= sizeof(StratusProtocolHeader);

                        if (!header->route_info)
                        {
                            if (header->length > sizeof(device_message))
                            {
                                header->length = sizeof(device_message);
                                logMessage("CControllerApp::CheckStratusRxSocket - device header length exceeds maximum location message size.");
                            }

                            memset(&device_message, 0, sizeof(device_message));

							if (!memory_settings_ini.masters_ip_address &&
								(memory_settings_ini.stratus_tcp_port == STRATUS_ENCRYPTED_PRODUCTION_PORT ||
								memory_settings_ini.stratus_tcp_port == STRATUS_ENCRYPTED_TEST_PORT ||
								memory_settings_ini.stratus_tcp_port == STRATUS_ENCRYPTED_DEVELOPMENT_PORT))
									memcpy(&device_message, DecryptStratusData(&stratus_tcp_ip_rx.data[sizeof(StratusProtocolHeader)],
										header->length), header->length);
							else
								memcpy(&device_message, &stratus_tcp_ip_rx.data[sizeof(StratusProtocolHeader)], header->length);

                            ProcessDeviceMessage(&device_message);
                        }
                        else if (header->route_info != 1)
                            logMessage("CControllerApp::CheckStratusRxSocket - invalid route info specifier.");
                    }

                    if (stratus_tcp_ip_rx.index == bytes_received)
                        memset(&stratus_tcp_ip_rx, 0, sizeof(stratus_tcp_ip_rx));
                    else
                    {
                        memset(stratus_tcp_ip_rx.data, 0, bytes_received);
                        stratus_tcp_ip_rx.index -= bytes_received;
                        memmove(stratus_tcp_ip_rx.data, &stratus_tcp_ip_rx.data[bytes_received], stratus_tcp_ip_rx.index);
                        memset(&stratus_tcp_ip_rx.data[stratus_tcp_ip_rx.index], 0, bytes_received);
                    }
                }
            }
        }
    }
}

void CControllerApp::CheckCustomerControlServerEventsTx()
{
 BYTE *file_header;
 EventMessage event_message;
 SocketMessage socket_message;
 FILE *file_handle, *file_handle2;

	if (last_tx_event_message_time < time(NULL))
	{
		fopen_s(&file_handle, TRANSMIT_EVENTS_FILE, "rb");

		if (file_handle && !fseek(file_handle, 0, SEEK_SET) && fread(&event_message, sizeof(event_message.head), 1, file_handle))
		{
			if (fread(event_message.data, event_message.head.data_length, 1, file_handle))
			{
				memset(&socket_message, 0, sizeof(socket_message));
				socket_message.head.command = CC_SERVER_CONTROLLER_EVENT;

				if (sizeof(event_message.head) + event_message.head.data_length > TCPIP_MESSAGE_BUFFER_SIZE)
					socket_message.head.data_length = TCPIP_MESSAGE_BUFFER_SIZE;
				else
					socket_message.head.data_length = sizeof(event_message.head) + event_message.head.data_length;

				memcpy(socket_message.data, &event_message, socket_message.head.data_length);
				customer_control_server_tx_queue.push(socket_message);
			}
			else	// error check file
			{
				if (event_message.head.header != LONG_HEADER_IDENTIFIER)
				{
					logMessage("CControllerApp::CheckCustomerControlServerEventsTx - header error, deleting bytes to correct.");
					event_message.head.header = 0;
				}
				else if (sizeof(event_message.head) + event_message.head.data_length > FileByteTotal(TRANSMIT_EVENTS_FILE))
				{
					logMessage("CControllerApp::CheckCustomerControlServerEventsTx - length error, deleting bytes to correct.");
					event_message.head.header = 0;
				}

				if (fseek(file_handle, 1, SEEK_SET))
				{
					fclose(file_handle);
					file_handle = NULL;
				}

				file_header = (BYTE*)&event_message;

				while (event_message.head.header != LONG_HEADER_IDENTIFIER && file_handle)
				{
					memmove(file_header, &file_header[1], 3);
					if (fread(&file_header[3], 1, 1, file_handle))
					{
						if (event_message.head.header == LONG_HEADER_IDENTIFIER)
						{
							fopen_s(&file_handle2, TEMP_FILE, "wb");
							if (file_handle2)
							{
								if (fread(&file_header[4], sizeof(event_message.head) - 4, 1, file_handle) &&
									fread(event_message.data, event_message.head.data_length, 1, file_handle) &&
									fwrite(&event_message, sizeof(event_message.head) + event_message.head.data_length, 1, file_handle2))
								{
									while (fread(&event_message, sizeof(event_message.head), 1, file_handle) &&
										fread(event_message.data, event_message.head.data_length, 1, file_handle))
									{
										if (!fwrite(&event_message, sizeof(event_message.head) + event_message.head.data_length, 1, file_handle2))
										{
											fclose(file_handle);
											file_handle = NULL;
											break;
										}
									}

									fclose(file_handle2);

									if (file_handle)
									{
										fclose(file_handle);
										file_handle = NULL;
										DeleteFile(TRANSMIT_EVENTS_FILE);
										MoveFile(TEMP_FILE, TRANSMIT_EVENTS_FILE);
									}
								}
								else
								{
									fclose(file_handle2);
									fclose(file_handle);
									file_handle = NULL;
								}
							}
							else
							{
								fclose(file_handle);
								file_handle = NULL;
							}
						}
					}
					else
					{
						fclose(file_handle);
						file_handle = NULL;
					}
				}
			}

			last_tx_event_message_time = time(NULL) + 30;	// give up to 30 seconds for response
		}
		else
			last_tx_event_message_time = time(NULL) + 5;	// wait 5 seconds before trying again

		if (file_handle)
			fclose(file_handle);
	}
}

void CControllerApp::CheckCustomerControlServerTxQueue()
{
 int bytes_sent, last_error;
 DWORD msg_crc;
 CString error_message;
 SocketMessage socket_message;

    if (!customer_control_server_tx_queue.empty())
    {
        socket_message = customer_control_server_tx_queue.front();
        socket_message.head.header = LONG_HEADER_IDENTIFIER;
        msg_crc = GetCrc32((BYTE*)&socket_message, sizeof(socket_message.head) + socket_message.head.data_length);
        memcpy(&socket_message.data[socket_message.head.data_length], &msg_crc, 4);
        socket_message.head.data_length += 4;

		bytes_sent = customerControlServerSocket.Send(&socket_message, sizeof(socket_message.head) + socket_message.head.data_length);

		if (bytes_sent == SOCKET_ERROR)
		{
			last_error = GetLastError();

			if (last_error != WSAEINPROGRESS && last_error != WSAEWOULDBLOCK)
			{
				error_message.Format("CControllerApp::CheckCustomerControlServerTxQueue - closing socket, error, %d.", last_error);
				logMessage(error_message);
				customerControlServerSocket.Close();
				customerControlServerSocket.socket_state = WSAENOTCONN;
			}
		}
		else if (bytes_sent == sizeof(socket_message.head) + socket_message.head.data_length)
			customer_control_server_tx_queue.pop();
	}
}

void CControllerApp::CheckStratusTxQueue()
{
 int message_length;
 StratusMessageFormat message;
 StratusMessageFormat stratus_message;

    if (((time(NULL) > last_stratus_tx_message_sent_time + 1) || !stratus_tx_message_ack_pending) && !stratus_tx_queue.empty())
    {
        message = stratus_tx_queue.front();
        message_length = (int)message.message_length;
        endian2(message.message_length);

        if (SendStratusMessage((BYTE*)&message, message_length, false, message.data_to_event_log))
        {
            last_stratus_tx_message_sent_time = time(NULL);

	        endian2(message.send_code);
            switch (message.send_code)
            {
                case MPI_ACCOUNT_LOGOUT:
                case MPI_WINNING_BINGO_HAND:
                case MPI_CLEAR_BINGO_CARD:
                case MPI_GB_PRO_POINTS_NEW_MSG:
                case MPI_JACKPOT:
                case TKT_TICKET_ADD:
                case TKT_TICKET_PAID:
                case TKT_FILL_MESSAGE:
                case TKT_JACKPOT_CREDIT:
                case TKT_SEND_BILL_COUNT:
                case MPI_NEW_WINNING_BINGO_HAND:
                case MPI_NEW_CLEAR_BINGO_CARD:
		            stratus_tx_message_ack_pending = TRUE;
					endian4(message.ucmc_id);
                    break;

                default:
                    stratus_tx_queue.pop();
		            stratus_tx_message_ack_pending = FALSE;
            }
        }
    }
}

void CControllerApp::CheckMarkerStratusTxQueue()
{
 int message_length;
 StratusMessageFormat message;
 WORD send_code;

 
    if (((time(NULL) > last_marker_stratus_tx_time + 3) || marker_message_recieved) && !marker_stratus_tx_queue.empty())
//    if (!marker_stratus_tx_queue.empty())
    {
        message = marker_stratus_tx_queue.front();
        message_length = (int)message.message_length;
        endian2(message.message_length);
        send_code = message.send_code;
        endian2(send_code);

        // avoid flooding the system with 100s of messages when comm is restored by only sending a message 3 times before waiting for a response
//        if (message.message_sends < 3)
        {
            if (SendStratusMessage((BYTE*)&message, message_length, false, message.data_to_event_log))
            {
                last_marker_stratus_tx_time = time(NULL);

                switch (send_code)
                {
                    case MPI_MARKER_CREDIT:
                    case MPI_MARKER_REQUEST:
                    case MPI_MARKER_AFT_COMPLETE:
                    case MPI_MARKER_CREDIT_PENDING:
                    case MPI_ABORT_MARKER_CREDIT:
                        break;
                    case MPI_ABORT_MARKER_ADVANCE_REQUEST:
                        break;
                    default:
                        marker_stratus_tx_queue.pop();
                }
                marker_message_recieved = FALSE;
            }
        }
    }
}

void CControllerApp::CheckIdcRxMessages()
{
 bool tx_message_found = FALSE;    
 BYTE calculated_crc, packet_crc;
 CString message, message_string;
 IdcProtocolHeader *header;
 IdcMessageFormat idc_message;
 int bytes_received = IdcSocket.GetBuffer(&idc_tcp_ip_rx.data[idc_tcp_ip_rx.index],
    TCPIP_MESSAGE_BUFFER_SIZE - idc_tcp_ip_rx.index);

	if (bytes_received)
	{
        idc_tcp_ip_rx.index += bytes_received;

        while (idc_tcp_ip_rx.index >= sizeof(IdcProtocolHeader))
        {
            header = (IdcProtocolHeader*)idc_tcp_ip_rx.data;

            if (header->header != (BYTE)PACKET_IDENTIFIER)
            {
                memset(&idc_tcp_ip_rx, 0, sizeof(idc_tcp_ip_rx));
                logMessage("CControllerApp::CheckIdcRxMessages - incorrect packet header.");
            }
            else
            {
                bytes_received = header->message_length;

                if (idc_tcp_ip_rx.index < bytes_received)
                {
                    return;
                }
                else
                {
                    // calculate and compare crc
                    packet_crc = idc_tcp_ip_rx.data[header->message_length - 1];
                    calculated_crc = CalculateMessageCrc(idc_tcp_ip_rx.data, header->message_length - 1);

                    if (calculated_crc != packet_crc)
					{
                        logMessage("CControllerApp::CheckIdcRxMessages - CRC mismatch.");
    					SendIdcAckMessage (header, FALSE);
					}
                    else
                    {
                        if (!GetPendingIdcTxMessage (&idc_message))
                        {
                            // 
						    if (!idc_tx_queue.empty())
                            {
							    idc_message = idc_tx_queue.front();
                                tx_message_found = TRUE;
                            }

                        }
                        else
                            tx_message_found = TRUE;


						if (tx_message_found)
						{
							// if this is an ack, then remove message from the TX queue
							if ((header->command_code[0] == idc_message.header.command_code[0]) &&
								(header->command_code[1] == idc_message.header.command_code[1]))
                            {
                                if (idc_tcp_ip_rx.data[sizeof(IdcProtocolHeader)] == 'A')
                                {
                                    idc_tx_nack_counter  = 0;

                                    idc_tx_message_ack_pending = FALSE;

                                    switch (header->command_code[0])
                                    {
                                        case POUND_HEADER:
			                                switch (header->command_code[1])
			                                {
                                                case C__GAME_LOCK_COMPLETE:
					                                PurgePendingIdcTxMessage(&idc_message);
					                                break;
				                                case D__DISPENSE_INFO:
					                                PurgePendingIdcTxMessage(&idc_message);
					                                break;
                                                case G__GAME_LOCK_RECEIVED:
					                                PurgePendingIdcTxMessage(&idc_message);
                                                    break;
                                                default:
                								    idc_tx_queue.pop();
                                                    break;
			                                }
                                            break;
                                        case COMMAND_HEADER:
			                                switch (header->command_code[1])
			                                {
                                                case b__BILL_ACCEPTED:
					                                PurgePendingIdcTxMessage(&idc_message);
					                                break;
                                                default:
                								    idc_tx_queue.pop();
                                                    break;
			                                }
                                            break;
                                        default:
        								    idc_tx_queue.pop();
                                            break;
                                    }
                                }
                                else if (idc_tcp_ip_rx.data[sizeof(IdcProtocolHeader)] == 'N')
                                {
                                    if (idc_tx_nack_counter > 3)
                                    {
                                        if (!idc_tx_message_ack_pending)
                                            logMessage("CControllerApp::CheckIdcRxMessages - unexpected IDC nack received ");

                                        idc_tx_message_ack_pending = FALSE;

                                        switch (header->command_code[0])
                                        {
                                            case POUND_HEADER:
			                                    switch (header->command_code[1])
			                                    {
                                                    case C__GAME_LOCK_COMPLETE:
					                                    PurgePendingIdcTxMessage(&idc_message);
					                                    break;
				                                    case D__DISPENSE_INFO:
					                                    PurgePendingIdcTxMessage(&idc_message);
					                                    break;
                                                    case G__GAME_LOCK_RECEIVED:
					                                    PurgePendingIdcTxMessage(&idc_message);
					                                    break;
                                                    default:
                								        idc_tx_queue.pop();
                                                        break;
			                                    }
                                                break;
                                            case COMMAND_HEADER:
			                                    switch (header->command_code[1])
			                                    {
                                                    case b__BILL_ACCEPTED:
					                                    PurgePendingIdcTxMessage(&idc_message);
					                                    break;
                                                    default:
                								        idc_tx_queue.pop();
                                                        break;
			                                    }
                                                break;
                                            default:
                								idc_tx_queue.pop();
                                                break;


                                        }
                                        idc_tx_nack_counter  = 0;
										CString message_string;
		                                message_string.Format("CControllerApp::CheckIdcRxMessages - 3 nacks for message %c %c message id %u ucmc id %u",
		                                                        header->command_code[0], header->command_code[1], header->message_id, header->ucmc_id);
		                                logMessage(message_string);
                                    }
                                    else
                                        idc_tx_nack_counter++;
                                }
                            }
							else
								ProcessIdcRxMessage();
						}
						else
							ProcessIdcRxMessage();
					}

                    if (idc_tcp_ip_rx.index == bytes_received)
                        memset(&idc_tcp_ip_rx, 0, sizeof(idc_tcp_ip_rx));
                    else
                    {
                        memset(idc_tcp_ip_rx.data, 0, bytes_received);
                        idc_tcp_ip_rx.index -= bytes_received;
                        memmove(idc_tcp_ip_rx.data, &idc_tcp_ip_rx.data[bytes_received], idc_tcp_ip_rx.index);
                        memset(&idc_tcp_ip_rx.data[idc_tcp_ip_rx.index], 0, bytes_received);
                    }
                }
            }
        }
    }

}

bool CControllerApp::GetPendingIdcTxMessage (IdcMessageFormat* idc_message)
{
    bool pending_message = FALSE;
    DWORD total_messages = fileRecordTotal(PENDING_IDC_TX_MESSAGES, sizeof(IdcMessageFormat));

    if (total_messages)
    {
        if(fileRead(PENDING_IDC_TX_MESSAGES, 0, idc_message, sizeof(IdcMessageFormat)))
            pending_message = TRUE;
    }
    else
        memset(idc_message, 0, sizeof(IdcMessageFormat));

    return pending_message;
}

void CControllerApp::PurgePendingIdcTxMessage (IdcMessageFormat* idc_message)
{
	IdcMessageFormat idc_message_on_disk;
    DWORD total_messages = fileRecordTotal(PENDING_IDC_TX_MESSAGES, sizeof(IdcMessageFormat));
    CString message_string;

    if (total_messages)
    {
        if (fileRead(PENDING_IDC_TX_MESSAGES, 0, &idc_message_on_disk, sizeof(IdcMessageFormat)))
//        if (fileRead(PENDING_IDC_TX_MESSAGES, total_messages - 1, &idc_message_on_disk, sizeof(IdcMessageFormat)))
        {
			if ((idc_message->header.command_code[0] == idc_message_on_disk.header.command_code[0]) &&
				(idc_message->header.command_code[1] == idc_message_on_disk.header.command_code[1]))
            {
                fileDeleteRecord(PENDING_IDC_TX_MESSAGES, sizeof(IdcMessageFormat), 0);
            }
			else
			{
				logMessage("CControllerApp::PurgePendingIdcTxMessage - Invalid message found in pending idc tx message file.");
                message_string.Format("idc response command code %c %c  disk command code %c %c",
		                          idc_message->header.command_code[0],
		                          idc_message->header.command_code[1],
		                          idc_message_on_disk.header.command_code[0],
		                          idc_message_on_disk.header.command_code[1]);
                logMessage(message_string);

				PurgePendingIdcTxMessage (0);
			}
		}
    }
}

void CControllerApp::PurgePendingIdcTxMessage (int record_index)
{
	IdcMessageFormat idc_message_on_disk;
    DWORD total_messages = fileRecordTotal(PENDING_IDC_TX_MESSAGES, sizeof(IdcMessageFormat));
    CString message_string;

    if (record_index < total_messages)
    {
        fileDeleteRecord(PENDING_IDC_TX_MESSAGES, sizeof(IdcMessageFormat), record_index);
    }
}

void CControllerApp::QueuePendingIdcTxMessages (void)
{
	IdcMessageFormat idc_message_on_disk;
    DWORD total_messages = fileRecordTotal(PENDING_IDC_TX_MESSAGES, sizeof(IdcMessageFormat));
    int i;
    DWORD messages_queued = 0;

    while (total_messages > messages_queued)
    {
        if (fileRead(PENDING_IDC_TX_MESSAGES, total_messages - messages_queued - 1, &idc_message_on_disk, sizeof(IdcMessageFormat)))
        {
    		idc_tx_queue.push(idc_message_on_disk);

			if ((idc_message_on_disk.header.header == MESSAGE_HEADER) &&
				(idc_message_on_disk.data_length < MAXIMUM_SERIAL_MESSAGE_SIZE) &&
				idc_message_on_disk.header.command_code[0])
        			idc_tx_queue.push(idc_message_on_disk);
			else
			{
				logMessage("CControllerApp::QueuePendingIdcTxMessages - Invalid message found in pending idc tx message file.");
				PurgePendingIdcTxMessage(messages_queued);
			}


        }
        messages_queued++;
    }
}

void CControllerApp::CheckIdcTxMessages()
{
    if (!idc_tx_message_ack_pending || (time(NULL) > last_idc_tx_message_sent_time + 2))
    {
        // prioritize any pending messages on disk
        if (!CheckIdcTxPendingMessages())
            CheckIdcTxQueue();
    }

}

bool CControllerApp::CheckIdcTxPendingMessages()
{
    bool pending_messages = FALSE;

	IdcMessageFormat idc_message_on_disk;
    DWORD total_messages = fileRecordTotal(PENDING_IDC_TX_MESSAGES, sizeof(IdcMessageFormat));
    CString message_string;

    if (total_messages)
    {
        if (fileRead(PENDING_IDC_TX_MESSAGES, 0, &idc_message_on_disk, sizeof(IdcMessageFormat)))
//        if (fileRead(PENDING_IDC_TX_MESSAGES, total_messages - 1, &idc_message_on_disk, sizeof(IdcMessageFormat)))
        {
            // verify that we have a valid message before sending it and delete the record if it is invalid.
            switch (idc_message_on_disk.header.command_code[0])
            {
                case POUND_HEADER:
		            switch (idc_message_on_disk.header.command_code[1])
		            {
                        case C__GAME_LOCK_COMPLETE:
                        case G__GAME_LOCK_RECEIVED:
			            case D__DISPENSE_INFO:
                            pending_messages = TRUE;
                            SendIdcMessage(&idc_message_on_disk);
				            break;
                        default:
                            // invalid message in pending message file
                            message_string.Format("CControllerApp::CheckIdcTxPendingMessages - Invalid message %c %c found in pending idc tx message file.",
		                                      idc_message_on_disk.header.command_code[0],
		                                      idc_message_on_disk.header.command_code[1]);
                            logMessage(message_string);
		                    PurgePendingIdcTxMessage (0);
                            break;

		            }
                    break;
                case COMMAND_HEADER:
		            switch (idc_message_on_disk.header.command_code[1])
		            {
                        case b__BILL_ACCEPTED:
                            pending_messages = TRUE;
                            SendIdcMessage(&idc_message_on_disk);
				            break;
                        default:
                            // invalid message in pending message file
                            message_string.Format("CControllerApp::CheckIdcTxPendingMessages - Invalid message %c %c found in pending idc tx message file.",
		                                      idc_message_on_disk.header.command_code[0],
		                                      idc_message_on_disk.header.command_code[1]);
                            logMessage(message_string);
		                    PurgePendingIdcTxMessage (0);
                            break;
		            }
                    break;
                default:
                    // invalid message in pending message file
                    message_string.Format("CControllerApp::CheckIdcTxPendingMessages - Invalid message %c %c found in pending idc tx message file.",
		                              idc_message_on_disk.header.command_code[0],
		                              idc_message_on_disk.header.command_code[1]);
                    logMessage(message_string);
		            PurgePendingIdcTxMessage (0);
                    break;
            }
		}
    }

    return pending_messages;
}

void CControllerApp::SendIdcMessage(IdcMessageFormat* idc_message)
{
     int message_length;
     CString message_string;
     bool do_not_send = FALSE;

//    if (!theApp.memory_settings_ini.masters_ip_address)
    {
//        if (!idc_tx_message_ack_pending || (time(NULL) > last_idc_tx_message_sent_time + 2))
        {

			if ((idc_message->header.header == MESSAGE_HEADER) &&
				(idc_message->data_length < MAXIMUM_SERIAL_MESSAGE_SIZE) &&
				idc_message->header.command_code[0])
			{
				if (sameIdcTxMessage(idc_message))
				{
					idc_tx_no_response_counter++;
					if (idc_tx_no_response_counter > 30)
					{
						idc_tx_no_response_counter = 0;
						IdcSocket.Close();
						IdcSocket.socket_state = WSAENOTCONN;
						logMessage("CControllerApp::SendIdcMessage - IDC tx no response, reconnecting.");
                        do_not_send = TRUE;
					}
				}
				else
				{
					idc_tx_no_response_counter = 0;
				}

                if (!do_not_send)
                {
				    idc_message->data[idc_message->data_length] = CalculateMessageCrc((BYTE*)idc_message, idc_message->header.message_length - 1);

				    SendIdcMessage((BYTE*)idc_message, idc_message->header.message_length, false, false);

				    last_idc_tx_message_id = idc_message->header.message_id;
				    // copy the current message into the last message
				    memcpy(&last_idc_tx_message, idc_message, sizeof(IdcMessageFormat));
				    last_idc_tx_message_sent_time =  time(NULL);
				    idc_tx_message_ack_pending = TRUE;
                }
		
			}
			else
			{
				logMessage("CControllerApp::SendIdcMessage - invalid current message, dumping message");
				idc_tx_queue.pop();
			}
        }
    }

}

void CControllerApp::CheckIdcTxQueue()
{
    int message_length;
    IdcMessageFormat idc_message;
    CString message_string;

    if (!idc_tx_queue.empty())
    {
		idc_message = idc_tx_queue.front();
		SendIdcMessage(&idc_message);
    }
}

//void CControllerApp::PushIdcTxMessage(IdcMessageFormat* new_idc_message)
void CControllerApp::PurgeOtherIdcTxMeterMessages(IdcMessageFormat* new_idc_message)
{
	IdcMessageFormat idc_message;
	queue <IdcMessageFormat, deque<IdcMessageFormat> > temp_idc_tx_queue;
	bool message_type_already_queued;

    // check the idc tx queue to see if there is already a game meter message for the same game in it.
    if ((new_idc_message->header.command_code[0] == COMMAND_HEADER) &&
        (new_idc_message->header.command_code[1] == GAME_METERS2))
    {
        // check the idc tx queue to see if there is already a game meter message for the same game in it.
        while (!idc_tx_queue.empty())
        {
            idc_message = idc_tx_queue.front();
            if (!idc_tx_queue.empty())
                idc_tx_queue.pop();
            message_type_already_queued = FALSE;

            if ((idc_message.header.command_code[0] == COMMAND_HEADER) &&
                (idc_message.header.command_code[1] == GAME_METERS2) &&
                (idc_message.header.ucmc_id == new_idc_message->header.ucmc_id))
                message_type_already_queued = TRUE;

            if (!message_type_already_queued)
                temp_idc_tx_queue.push(idc_message);
        }

        // put the idc tx queue elements back where we got them
        while (!temp_idc_tx_queue.empty())
        {
            idc_message = temp_idc_tx_queue.front();
            idc_tx_queue.push(idc_message);
            temp_idc_tx_queue.pop();
        }
    }
}

bool CControllerApp::sameIdcTxMessage(IdcMessageFormat* idc_message)
{
	bool same = TRUE;

	if (memcmp(&idc_message->header.command_code, &last_idc_tx_message.header.command_code, sizeof(idc_message->header.command_code)))
		same = FALSE;

	if (memcmp(&idc_message->header.message_length, &last_idc_tx_message.header.message_length, sizeof(idc_message->header.message_length)))
		same = FALSE;

	if (memcmp(&idc_message->header.message_id, &last_idc_tx_message.header.message_id, sizeof(idc_message->header.message_id)))
		same = FALSE;

	if (memcmp(&idc_message->header.mux_id, &last_idc_tx_message.header.mux_id, sizeof(idc_message->header.mux_id)))
		same = FALSE;

	if (memcmp(&idc_message->header.sas_id, &last_idc_tx_message.header.sas_id, sizeof(idc_message->header.sas_id)))
		same = FALSE;

	if (memcmp(&idc_message->header.ucmc_id, &last_idc_tx_message.header.ucmc_id, sizeof(idc_message->header.ucmc_id)))
		same = FALSE;

	if (memcmp(&idc_message->data_length, &last_idc_tx_message.data_length, sizeof(idc_message->data_length)))
		same = FALSE;

	if (memcmp(&idc_message->data, &last_idc_tx_message.data, sizeof(idc_message->data_length)))
		same = FALSE;

	return same;
}

void CControllerApp::ProcessIdcRxMessage(void)
{
 CString log_message;
	IdcProtocolHeader *header;

	header = (IdcProtocolHeader*)idc_tcp_ip_rx.data;

    switch (header->command_code[0])
    {
        case '!':
			switch (header->command_code[1])
			{
				case '?':
					ProcessIdcRequestMachineSpecificData(header);
					break;
			}
			break;
        case '@':
			switch (header->command_code[1])
			{
				case 'B':
					ProcessIdcBonusAwardMessage();
					break;
				case 'C':
					ProcessIdcGbAdvantageWinMessage();
					break;
                case 'p':
                    ProcessIdcSetPromotionWinCategory();
                    break;
                case 'M':
                    ProcessIdcGbaWinTextMessage();
                    break;
			}
			break;
        case '#':
			switch (header->command_code[1])
			{
				case 'f':
					ProcessIdcFillValues();
					break;
                case 'd':
                    ProcessIdcDispenseInfoMessage();
                    break;
                case 'T':
                    ProcessIdcPrintBlobMessage();
                    break;
                case 'W':
                    ProcessIdcW2gInfoMessage();
                    break;

			}
			break;
        default:
//            logMessage(log_message);
            return;
    }

}

void CControllerApp::ProcessIdcRequestMachineSpecificData (IdcProtocolHeader *header)
{
	IdcMessageFormat idc_message;
	MachineIdentifiers machine_identifiers;
	CGameDevice *pGameDevice;

	SendIdcAckMessage (header, TRUE);

	if (header->mux_id)
	{
		pGameDevice = GetGamePointer(header->mux_id);
		SendMachineSpecificData(pGameDevice);
	}
	else if (header->sas_id)
	{
//		pGameDevice = GetGamePointer(header->sas_id);
		pGameDevice = GetSasGamePointer(header->ucmc_id);
		SendMachineSpecificData(pGameDevice);
	}
	if (header->ucmc_id)
	{
		pGameDevice = GetGamePointer(header->ucmc_id);
		SendMachineSpecificData(pGameDevice);
	}
	else
	{
		SendAllMachineSpecificData();
	}
}


void CControllerApp::ProcessIdcSetPromotionWinCategory (void)
{
	IdcProtocolHeader *header = (IdcProtocolHeader*)idc_tcp_ip_rx.data;
//	IdcGbAdvantageWinMessage* win_category = (IdcGbAdvantageWinMessage*)&idc_tcp_ip_rx.data[sizeof(IdcProtocolHeader)];

    if (pGbProManager)
        pGbProManager->ProcessSetPromotionWinCategory (&idc_tcp_ip_rx.data[sizeof(IdcProtocolHeader)], 0, FALSE);

	SendIdcAckMessage (header, TRUE);
}

void CControllerApp::ProcessIdcBonusAwardMessage (void)
{
	IdcProtocolHeader *header = (IdcProtocolHeader*)idc_tcp_ip_rx.data;
	IdcBonusAwardMessage* bonus_award_message = (IdcBonusAwardMessage*)&idc_tcp_ip_rx.data[sizeof(IdcProtocolHeader)];
	CGameDevice *pGameDevice = NULL;
	GbProPointsTicketsCash points_tickets_cash;
    CGameCommControl* pCGameCommControl = NULL;

	SendIdcAckMessage (header, TRUE);

//    if (header->ucmc_id)
    {
	    memset(&points_tickets_cash, 0, sizeof(GbProPointsTicketsCash));
        // copy over relevant fields
        points_tickets_cash.player_account = bonus_award_message->player_account;
        points_tickets_cash.bonus_points = bonus_award_message->bonus_points;
        points_tickets_cash.print_drawing_ticket = bonus_award_message->print_drawing_ticket;
        points_tickets_cash.cash_voucher_amount = bonus_award_message->cash_voucher_amount;
        points_tickets_cash.promotion_id = bonus_award_message->promotion_id;
        points_tickets_cash.i_rewards_paid_win = bonus_award_message->i_rewards_paid_win;
        points_tickets_cash.transaction_id = bonus_award_message->transaction_id;
        points_tickets_cash.cashable_credits = bonus_award_message->cashable_credits;
        points_tickets_cash.noncashable_credits = bonus_award_message->noncashable_credits;
    
	    pGameDevice = GetSasGamePointer(header->ucmc_id);
	    if (!pGameDevice)
	    {
		    pGameDevice = GetGamePointer(header->ucmc_id);

            pCGameCommControl = GetGameCommControlPointer(header->ucmc_id);
            if (pCGameCommControl)
                pCGameCommControl->ProcessGbProPointsTicketsCash(&points_tickets_cash, pGameDevice);
            else
            {
                CGameCommControl temp_game_comm_control;
                temp_game_comm_control.ProcessGbProPointsTicketsCash(&points_tickets_cash, pGameDevice);
            }

	    }
        else
        {
            pCGameCommControl = GetSasGameCommControlPointer(header->ucmc_id);
            if (pCGameCommControl)
            {
                pCGameCommControl->sas_com->non_volatile_ram.whole_points += bonus_award_message->bonus_points;
                pCGameCommControl->updateGbInterfaceData();
                pCGameCommControl->ProcessGbProPointsTicketsCash(&points_tickets_cash, pGameDevice);
            }
        }


	    if (memory_settings_ini.cms_ip_address && memory_settings_ini.cms_tcp_port)
        {
		    if (pGameDevice)
			    SendCmsGbAdvantageWinMessage (pGameDevice->memory_game_config.ucmc_id, (IdcGbAdvantageWinMessage*)&points_tickets_cash);
		    else
			SendCmsGbAdvantageWinMessage (0, (IdcGbAdvantageWinMessage*)&points_tickets_cash);
        }
    }
}


void CControllerApp::ProcessIdcGbAdvantageWinMessage (void)
{
	IdcProtocolHeader *header = (IdcProtocolHeader*)idc_tcp_ip_rx.data;
	IdcGbAdvantageWinMessage* win_message = (IdcGbAdvantageWinMessage*)&idc_tcp_ip_rx.data[sizeof(IdcProtocolHeader)];
	CGameDevice *pGameDevice;
	GbProPointsTicketsCash points_tickets_cash;
    CGameCommControl* pCGameCommControl;

	SendIdcAckMessage (header, TRUE);

	memset(&points_tickets_cash, 0, sizeof(GbProPointsTicketsCash));
    if (header->message_length > (sizeof(IdcProtocolHeader) - 1))
    	memcpy(&points_tickets_cash, win_message, header->message_length - sizeof(IdcProtocolHeader) - 1);
    else
    	memcpy(&points_tickets_cash, win_message, sizeof(IdcGbAdvantageWinMessage));
    
	strcpy(points_tickets_cash.promo_string, pGbProManager->GetGbProPointsTicketsCashPromoString(win_message->promotion_id));

	pGameDevice = GetSasGamePointer(header->ucmc_id);
	if (!pGameDevice)
	{
		pGameDevice = GetGamePointer(header->ucmc_id);

		if (pGameDevice)
        {
            if (pGbProManager->isGbaHandPromotion (win_message->promotion_id))
            {
		        if (pGameDevice->removeGbaInteractiveBonusTransactionId(win_message->transaction_id))
                {
                    pCGameCommControl = GetGameCommControlPointer(header->ucmc_id);
                    if (pCGameCommControl)
                        pCGameCommControl->ProcessGbProPointsTicketsCash(&points_tickets_cash, pGameDevice);
                    else
                    {
                        CGameCommControl temp_game_comm_control;
                        temp_game_comm_control.ProcessGbProPointsTicketsCash(&points_tickets_cash, pGameDevice);
                    }
                }
            }
            else
            {
                pCGameCommControl = GetGameCommControlPointer(header->ucmc_id);
                if (pCGameCommControl)
                    pCGameCommControl->ProcessGbProPointsTicketsCash(&points_tickets_cash, pGameDevice);
                else
                {
                    CGameCommControl temp_game_comm_control;
                    temp_game_comm_control.ProcessGbProPointsTicketsCash(&points_tickets_cash, pGameDevice);
                }

            }
        }
	}
    else
    {
        pCGameCommControl = GetSasGameCommControlPointer(header->ucmc_id);
        if (pCGameCommControl)
        {
            pCGameCommControl->sas_com->non_volatile_ram.whole_points += win_message->bonus_points;
            pCGameCommControl->updateGbInterfaceData();
        }

		if (pGameDevice)
        {
            if (pGbProManager->isGbaHandPromotion (win_message->promotion_id))
            {
		        if (pGameDevice->removeGbaInteractiveBonusTransactionId(win_message->transaction_id))
                {
                    if (pCGameCommControl)
    			        pCGameCommControl->ProcessGbProPointsTicketsCash(&points_tickets_cash, pGameDevice);
                }
            }
            else
            {
                if (pCGameCommControl)
    			    pCGameCommControl->ProcessGbProPointsTicketsCash(&points_tickets_cash, pGameDevice);
            }

        }
    }

	if (pGameDevice && memory_settings_ini.cms_ip_address && memory_settings_ini.cms_tcp_port && win_message->i_rewards_paid_win)
		SendCmsGbAdvantageWinMessage (pGameDevice->memory_game_config.ucmc_id, win_message);
}

void CControllerApp::ProcessIdcGbaWinTextMessage (void)
{
	IdcProtocolHeader *header = (IdcProtocolHeader*)idc_tcp_ip_rx.data;
	IdcGbaWinTextMessage* gba_win_text_message = (IdcGbaWinTextMessage*)&idc_tcp_ip_rx.data[sizeof(IdcProtocolHeader)];
	CGameDevice *pGameDevice = NULL;

	SendIdcAckMessage (header, TRUE);

	pGameDevice = GetSasGamePointer(header->ucmc_id);
	if (pGameDevice)
		pGameDevice = GetGbInterfaceGamePointer(pGameDevice->memory_game_config.ucmc_id);
    else
		pGameDevice = GetGamePointer(header->ucmc_id);

	if (pGameDevice)
    {
        TxSerialMessage tx_message;

		memset(&tx_message, 0, sizeof(tx_message));
		tx_message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
		tx_message.msg.length = 67;
		tx_message.msg.command = PERCENT_HEADER;
		tx_message.msg.data[0] = GB_PRO_ASCII_GAME_MSG;

		memcpy(&tx_message.msg.data[1], gba_win_text_message, sizeof(IdcGbaWinTextMessage));

		pGameDevice->transmit_queue.push(tx_message);
    }

}

void CControllerApp::ProcessIdcFillValues (void)
{
	IdcProtocolHeader *header = (IdcProtocolHeader*)idc_tcp_ip_rx.data;
	IdcFillValues* fill_values = (IdcFillValues*)&idc_tcp_ip_rx.data[sizeof(IdcProtocolHeader)];

	SendIdcAckMessage (header, TRUE);

	if (fill_values->impress_bill_amount_100 || fill_values->impress_bill_amount_20)
        fillDispenser();

    dispenserControl.SetDispenserFillInfo(fill_values);
}

void CControllerApp::ProcessIdcDispenseInfoMessage (void)
{
	IdcProtocolHeader *header = (IdcProtocolHeader*)idc_tcp_ip_rx.data;
	DispenseInfo* dispense_info = (DispenseInfo*)&idc_tcp_ip_rx.data[sizeof(IdcProtocolHeader)];

	SendIdcAckMessage (header, TRUE);

    dispenserControl.DispenseBills(dispense_info->ticket_id,
                                   dispense_info->dispense_amount,
                                   header->ucmc_id,
                                   header->mux_id);
}

void CControllerApp::ProcessIdcPrintBlobMessage (void)
{
	IdcProtocolHeader *header = (IdcProtocolHeader*)idc_tcp_ip_rx.data;
	char* print_blob = (char*)&idc_tcp_ip_rx.data[sizeof(IdcProtocolHeader)];
    char data[TCPIP_MESSAGE_BUFFER_SIZE];

	SendIdcAckMessage (header, TRUE);

    if (header->message_length > (sizeof(IdcProtocolHeader) - 1))
    {
        memset(data, 0, TCPIP_MESSAGE_BUFFER_SIZE);
        memcpy(data, print_blob, header->message_length - sizeof(IdcProtocolHeader) - 1);
		printer_busy = true;
        W2gPrinterData(data);
		printer_busy = false;
    }
    else
		logMessage("CControllerApp::ProcessIdcPrintBlobMessage - invalid length received");
}

void CControllerApp::ProcessIdcW2gInfoMessage (void)
{
	IdcProtocolHeader *header = (IdcProtocolHeader*)idc_tcp_ip_rx.data;
	W2gInfo* w2g_info = (W2gInfo*)&idc_tcp_ip_rx.data[sizeof(IdcProtocolHeader)];
    CString filename;
    FileGameLock file_game_lock;

	SendIdcAckMessage (header, TRUE);

    // 'game lock path + ucmc id-amount-ticket id'
    filename.Format("%s\\%u-%u-%u",
                          GAME_LOCKS_PATH,
                          header->ucmc_id,
                          w2g_info->ticket_amount,
                          w2g_info->ticket_id);

    if (fileRead(filename.GetBuffer(0), 0, &file_game_lock, sizeof(file_game_lock)))
    {
        // put the ss# and drivers license info in the game lock file
        file_game_lock.game_lock.ss_number = w2g_info->ss_number;
        file_game_lock.game_lock.drivers_license = w2g_info->drivers_license;
        fileWrite(filename.GetBuffer(0), 0, &file_game_lock, sizeof(FileGameLock));  
    }
}


void CControllerApp::SendMachineSpecificData(CGameDevice *pGameDevice)
{
//	SendIdcGameMeterMessage (pGameDevice, 0);
    if (theApp.memory_settings_ini.cms_ip_address)
    {
    	if (pGameDevice->cms_account_login_data.account_number > 0)
            SendIdcCmsLoginMessage (pGameDevice, &pGameDevice->cms_account_login_data);
    }
    else
    {
    	if (pGameDevice->account_login_data.account_number > 0)
    		SendIdcLoginMessage (pGameDevice, 0);
    }
}

void CControllerApp::SendAllMachineSpecificData(void)
{
	CGameDevice *pGameDevice;
	CGameCommControl *pGameCommControl = pHeadGameCommControl;

    while (pGameCommControl)
    {
        pGameDevice = pGameCommControl->pHeadGameDevice;

        while (pGameDevice)
        {
			if (pGameDevice->memory_game_config.mux_id)
            {
                // if there is not an associated sas game pointer, then this is not a GBI
                if (!GetSasGamePointer(pGameDevice->memory_game_config.ucmc_id))
                {
					if (pGameDevice->account_login_data.account_number > 0)
					    SendIdcLoginMessage (pGameDevice, 0);
                }
            }
            else
            {
                if (theApp.memory_settings_ini.cms_ip_address)
                {
    	            if (pGameDevice->cms_account_login_data.account_number > 0)
                        SendIdcCmsLoginMessage (pGameDevice, &pGameDevice->cms_account_login_data);
                }
                else
                {
    	            if (pGameDevice->account_login_data.account_number > 0)
    		            SendIdcLoginMessage (pGameDevice, 0);
                }

            }


			// make sure this game device in NOT a GbInterface
			if (pGameDevice != GetGbInterfaceGamePointer(pGameDevice->memory_game_config.ucmc_id))
			{
			}

            pGameDevice = pGameDevice->pNextGame;
        }

        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }
}

void CControllerApp::SendIdcXpcVersionConfigInfo(void)
{
	IdcProtocolHeader idc_protocol_header;
    XpcVersionAndConfigInfo xpc_version_and_config_info;
	WORD data_length;

    xpc_version_and_config_info.version = VERSION;    
    memset(&xpc_version_and_config_info.subversion, 0, sizeof(xpc_version_and_config_info.subversion));
    if (strlen(SUBVERSION) <= sizeof(xpc_version_and_config_info.subversion))
        strcpy(xpc_version_and_config_info.subversion, SUBVERSION);

    xpc_version_and_config_info.gba_configured_location = (theApp.memory_settings_ini.activate_gb_advantage_support &&
                                                           pGbProManager->memory_gb_pro_settings.gb_pro_configurator_ip_address &&
                                                    	   pGbProManager->memory_gb_pro_settings.gb_pro_configurator_tcp_port);    

	idc_protocol_header.header = PACKET_IDENTIFIER;
	idc_protocol_header.command_code[0] = POUND_HEADER;
	idc_protocol_header.command_code[1] = V__XPC_VERSION_AND_CONFIGURATION_INFO;

	idc_protocol_header.mux_id = 0;
	idc_protocol_header.sas_id = 0;
	idc_protocol_header.ucmc_id = 0;
	data_length = sizeof(XpcVersionAndConfigInfo);
	idc_protocol_header.message_length = sizeof(IdcProtocolHeader) + data_length + 1;
	theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)&xpc_version_and_config_info, data_length, FALSE);
}

void CControllerApp::SendIdcGameMeterMessage (CGameDevice *pGameDevice, GameMeters* game_meters)
{
	IdcProtocolHeader idc_protocol_header;
	WORD data_length;
    CString message_string;
    GameMeters jackpot_wobble_game_meters;

	idc_protocol_header.header = PACKET_IDENTIFIER;
	idc_protocol_header.command_code[0] = COMMAND_HEADER;
	idc_protocol_header.command_code[1] = GAME_METERS2;

	idc_protocol_header.mux_id = pGameDevice->memory_game_config.mux_id;
	idc_protocol_header.sas_id = pGameDevice->memory_game_config.sas_id;
	idc_protocol_header.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
	data_length = sizeof(GameMeters);
	idc_protocol_header.message_length = sizeof(IdcProtocolHeader) + data_length + 1;


    if (pGameDevice->meter_wobbled)
    {
		message_string.Format("Meters after wobble coin_in %u coin out %u games %u drop %u handpay %u jackpot %u - ucmc id %u",
		                      game_meters->coin_in, game_meters->coin_out, game_meters->games, game_meters->coin_drop, game_meters->hand_pay,
                              game_meters->jackpot, pGameDevice->memory_game_config.ucmc_id);
		logMessage(message_string);
        pGameDevice->meter_wobbled = FALSE; // reset flag
    }

	if (game_meters)
    {
        if (game_meters->coin_in < pGameDevice->last_game_meters_sent_to_idc.coin_in)
        {
		    message_string.Format("CControllerApp::SendIdcGameMeterMessage - Meter wobble coin_in -  %u < %u - ucmc id %u",
		                          game_meters->coin_in, pGameDevice->last_game_meters_sent_to_idc.coin_in, pGameDevice->memory_game_config.ucmc_id);
		    logMessage(message_string);
            pGameDevice->meter_wobbled = TRUE;
        }

        if (game_meters->coin_out != pGameDevice->last_game_meters_sent_to_idc.coin_out)
			if (game_meters->coin_out < pGameDevice->last_game_meters_sent_to_idc.coin_out)
			{
				message_string.Format("CControllerApp::SendIdcGameMeterMessage - Meter wobble coin_out -  %u < %u - ucmc id %u",
									  game_meters->coin_out, pGameDevice->last_game_meters_sent_to_idc.coin_out, pGameDevice->memory_game_config.ucmc_id);
				logMessage(message_string);
				pGameDevice->meter_wobbled = TRUE;

			}

        if (game_meters->games < pGameDevice->last_game_meters_sent_to_idc.games)
        {
		    message_string.Format("CControllerApp::SendIdcGameMeterMessage - Meter wobble games -  %u < %u - ucmc id %u",
		                          game_meters->games, pGameDevice->last_game_meters_sent_to_idc.games, pGameDevice->memory_game_config.ucmc_id);
		    logMessage(message_string);
            pGameDevice->meter_wobbled = TRUE;

        }

        if (game_meters->coin_drop < pGameDevice->last_game_meters_sent_to_idc.coin_drop)
        {
		    message_string.Format("CControllerApp::SendIdcGameMeterMessage - Meter wobble coin_drop -  %u < %u - ucmc id %u",
		                          game_meters->coin_drop, pGameDevice->last_game_meters_sent_to_idc.coin_drop, pGameDevice->memory_game_config.ucmc_id);
		    logMessage(message_string);
            pGameDevice->meter_wobbled = TRUE;

        }

        if (game_meters->hand_pay < pGameDevice->last_game_meters_sent_to_idc.hand_pay)
        {
		    message_string.Format("CControllerApp::SendIdcGameMeterMessage - Meter wobble hand_pay -  %u < %u - ucmc id %u",
		                          game_meters->hand_pay, pGameDevice->last_game_meters_sent_to_idc.hand_pay, pGameDevice->memory_game_config.ucmc_id);
		    logMessage(message_string);
            pGameDevice->meter_wobbled = TRUE;

        }

        if (game_meters->jackpot < pGameDevice->last_game_meters_sent_to_idc.jackpot)
        {
		    message_string.Format("CControllerApp::SendIdcGameMeterMessage - Meter wobble jackpot -  %u < %u - ucmc id %u",
		                          game_meters->jackpot, pGameDevice->last_game_meters_sent_to_idc.jackpot, pGameDevice->memory_game_config.ucmc_id);
		    logMessage(message_string);
            pGameDevice->meter_wobbled = TRUE;

        }

		theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)game_meters, data_length, FALSE);

        memcpy(&pGameDevice->last_game_meters_sent_to_idc, game_meters, sizeof(GameMeters));
    }
}

void CControllerApp::SendIdcCurrentCreditsMessage (CGameDevice *pGameDevice, ULONG* current_credits)
{
	IdcProtocolHeader idc_protocol_header;
	WORD data_length;
    CString message_string;

	idc_protocol_header.header = PACKET_IDENTIFIER;
	idc_protocol_header.command_code[0] = COMMAND_HEADER;
	idc_protocol_header.command_code[1] = c__CURRENT_CREDITS;

	idc_protocol_header.mux_id = pGameDevice->memory_game_config.mux_id;
	idc_protocol_header.sas_id = pGameDevice->memory_game_config.sas_id;
	idc_protocol_header.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
	data_length = sizeof(ULONG);
	idc_protocol_header.message_length = sizeof(IdcProtocolHeader) + data_length + 1;
	if (current_credits)
		theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)current_credits, data_length, FALSE);
}

void CControllerApp::SendIdcBillAcceptedMessage (CGameDevice *pGameDevice, IdcBillAcceptedMessage* bill_accepted)
{
	IdcProtocolHeader idc_protocol_header;
	WORD data_length;
    CString message_string;

	idc_protocol_header.header = PACKET_IDENTIFIER;
	idc_protocol_header.command_code[0] = COMMAND_HEADER;
	idc_protocol_header.command_code[1] = b__BILL_ACCEPTED;

	idc_protocol_header.mux_id = pGameDevice->memory_game_config.mux_id;
	idc_protocol_header.sas_id = pGameDevice->memory_game_config.sas_id;
	idc_protocol_header.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
	data_length = sizeof(IdcBillAcceptedMessage);
	idc_protocol_header.message_length = sizeof(IdcProtocolHeader) + data_length + 1;
	if (bill_accepted)
		theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)bill_accepted, data_length, TRUE);

}

void CControllerApp::SendIdcAckMessage (IdcProtocolHeader *header, bool ack)
{
	IdcMessageFormat idc_message;

	memcpy(&idc_message.header, header, sizeof(IdcProtocolHeader));
    if (ack)
    	idc_message.data[0] = 'A';
    else
    	idc_message.data[0] = 'N';

	idc_message.data_length = 1;

	idc_message.header.message_length = 13;
	idc_message.header.message_id = header->message_id;

	idc_message.data[idc_message.data_length] = CalculateMessageCrc((BYTE*)&idc_message, idc_message.header.message_length - 1);
	// send the ack to the TCP/IP socket connection
    SendIdcMessage((BYTE*)&idc_message, idc_message.header.message_length, false, false);
}

void CControllerApp::SendIdcTransactionIdAckMessage (IdcProtocolHeader *header, DWORD transaction_id)
{
	IdcMessageFormat idc_message;

	memcpy(&idc_message.header, header, sizeof(IdcProtocolHeader));
	memcpy(&idc_message.data[0], &transaction_id, sizeof(transaction_id));
	idc_message.data_length = sizeof(transaction_id);

	idc_message.header.message_length = 16;
	idc_message.header.message_id = header->message_id;

	idc_message.data[idc_message.data_length] = CalculateMessageCrc((BYTE*)&idc_message, idc_message.header.message_length - 1);
	// send the ack to the TCP/IP socket connection
    SendIdcMessage((BYTE*)&idc_message, idc_message.header.message_length, false, false);
}

void CControllerApp::SendIdcLogoutMessage (CGameDevice *pGameDevice, AccountLogoutData* account_logout_data)
{
	IdcProtocolHeader idc_protocol_header;
	WORD data_length;

	idc_protocol_header.header = PACKET_IDENTIFIER;
	idc_protocol_header.command_code[0] = COMMAND_HEADER;
	idc_protocol_header.command_code[1] = ACCOUNT_LOGOUT;

	idc_protocol_header.mux_id = pGameDevice->memory_game_config.mux_id;
	idc_protocol_header.sas_id = pGameDevice->memory_game_config.sas_id;
	idc_protocol_header.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
	data_length = sizeof(AccountLogoutData) - sizeof(account_logout_data->time_stamp) - sizeof(account_logout_data->reserved);
	idc_protocol_header.message_length = sizeof(IdcProtocolHeader) + data_length + 1;
	theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)account_logout_data, data_length, FALSE);
}

void CControllerApp::SendIdcLoginMessage (CGameDevice *pGameDevice, AccountLogin* account_login)
{
	IdcProtocolHeader idc_protocol_header;
	WORD data_length;

	if ((account_login && account_login->account_number) || pGameDevice->account_login_data.account_number)
	{
		idc_protocol_header.header = PACKET_IDENTIFIER;
		idc_protocol_header.command_code[0] = COMMAND_HEADER;
		idc_protocol_header.command_code[1] = ACCOUNT_INQUIRY;

		idc_protocol_header.mux_id = pGameDevice->memory_game_config.mux_id;
		idc_protocol_header.sas_id = pGameDevice->memory_game_config.sas_id;
		idc_protocol_header.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
		data_length = sizeof(AccountLogin);
		idc_protocol_header.message_length = sizeof(IdcProtocolHeader) + data_length + 1;

		if (account_login)
			theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)account_login, data_length, FALSE);
		else
			theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)&pGameDevice->account_login_data, data_length, FALSE);
	}
}

void CControllerApp::SendIdcGameLockReceivedMessage (FileGameLock* file_game_lock)
{
	IdcProtocolHeader idc_protocol_header;
	IdcGameLockMessage idc_game_lock_message;
	WORD data_length;

	idc_protocol_header.command_code[0] =POUND_HEADER;
	idc_protocol_header.command_code[1] = G__GAME_LOCK_RECEIVED;

	SendIdcGameLockMessage (file_game_lock, &idc_protocol_header);
}
void CControllerApp::SendIdcGameLockCompleteMessage (FilePaidTicket* file_paid_ticket)
{
	IdcProtocolHeader idc_protocol_header;
	IdcGameLockCompleteMessage idc_game_lock_complete_message;
	WORD data_length;

	idc_protocol_header.command_code[0] = POUND_HEADER;
	idc_protocol_header.command_code[1] = C__GAME_LOCK_COMPLETE;

	if (file_paid_ticket)
	{
		idc_protocol_header.header = PACKET_IDENTIFIER;
		idc_protocol_header.mux_id = file_paid_ticket->memory.memory_game_lock.mux_id;
		idc_protocol_header.sas_id = file_paid_ticket->memory.memory_game_lock.sas_id;
		idc_protocol_header.ucmc_id = file_paid_ticket->memory.memory_game_lock.ucmc_id;

		idc_game_lock_complete_message.game_lock.ticket_id = file_paid_ticket->memory.memory_game_lock.ticket_id;
		idc_game_lock_complete_message.game_lock.ticket_amount = file_paid_ticket->memory.memory_game_lock.ticket_amount;
		idc_game_lock_complete_message.game_lock.taxable_flag = file_paid_ticket->memory.memory_game_lock.taxable_flag;
		idc_game_lock_complete_message.game_lock.marker_balance = file_paid_ticket->memory.memory_game_lock.marker_balance;
		idc_game_lock_complete_message.game_lock.player_account = file_paid_ticket->memory.memory_game_lock.player_account;
		memcpy(idc_game_lock_complete_message.game_lock.time_stamp, file_paid_ticket->memory.memory_game_lock.time_stamp, 6);
		idc_game_lock_complete_message.game_lock.jackpot_to_credit_flag = file_paid_ticket->memory.memory_game_lock.jackpot_to_credit_flag;
		memcpy(idc_game_lock_complete_message.game_lock.player_name, file_paid_ticket->memory.memory_game_lock.player_name, 14);

		idc_game_lock_complete_message.ss_number = file_paid_ticket->memory.memory_game_lock.ss_number;
		idc_game_lock_complete_message.drivers_license = file_paid_ticket->memory.memory_game_lock.drivers_license;

		data_length = sizeof(IdcGameLockCompleteMessage);
		idc_protocol_header.message_length = sizeof(IdcProtocolHeader) + data_length + 1;

		theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)&idc_game_lock_complete_message, data_length, TRUE);
	}

}

void CControllerApp::SendIdcGameLockMessage (FileGameLock* file_game_lock, IdcProtocolHeader* idc_protocol_header)
{
	IdcGameLockMessage idc_game_lock_message;
	WORD data_length;

	if (file_game_lock)
	{
		idc_protocol_header->header = PACKET_IDENTIFIER;
		idc_protocol_header->mux_id = file_game_lock->game_lock.mux_id;
		idc_protocol_header->sas_id = file_game_lock->game_lock.sas_id;
		idc_protocol_header->ucmc_id = file_game_lock->game_lock.ucmc_id;

		idc_game_lock_message.ticket_id = file_game_lock->game_lock.ticket_id;
		idc_game_lock_message.ticket_amount = file_game_lock->game_lock.ticket_amount;
		idc_game_lock_message.taxable_flag = file_game_lock->game_lock.taxable_flag;
		idc_game_lock_message.marker_balance = file_game_lock->game_lock.marker_balance;
		idc_game_lock_message.player_account = file_game_lock->game_lock.player_account;
		memcpy(idc_game_lock_message.time_stamp, file_game_lock->game_lock.time_stamp, 6);
		idc_game_lock_message.jackpot_to_credit_flag = file_game_lock->game_lock.jackpot_to_credit_flag;
		memcpy(idc_game_lock_message.player_name, file_game_lock->game_lock.player_name, 14);

		data_length = sizeof(IdcGameLockMessage);
		idc_protocol_header->message_length = sizeof(IdcProtocolHeader) + data_length + 1;

		theApp.QueueIdcTxMessage(idc_protocol_header, (BYTE*)&idc_game_lock_message, data_length, TRUE);
	}
}

void CControllerApp::SendIdcRedeemRequestMessage (CGameDevice *pGameDevice)
{
	IdcProtocolHeader idc_protocol_header;
	WORD data_length;

	idc_protocol_header.header = PACKET_IDENTIFIER;
	idc_protocol_header.command_code[0] = COMMAND_HEADER;
	idc_protocol_header.command_code[1] = W__REDEEM_REQUEST;

	idc_protocol_header.mux_id = pGameDevice->memory_game_config.mux_id;
	idc_protocol_header.sas_id = pGameDevice->memory_game_config.sas_id;
	idc_protocol_header.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
	data_length = sizeof(RedeemRequest);
	idc_protocol_header.message_length = sizeof(IdcProtocolHeader) + data_length + 1;
	theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)&pGameDevice->redeem_request, data_length, FALSE);
}

void CControllerApp::SendIdcGameEnteredMessage (CGameDevice *pGameDevice, char* paytable_id, char* manufacturer_id)
{
	IdcProtocolHeader idc_protocol_header;
	WORD data_length;
    IdcGameEnteredMessage idc_game_entered_message;

	if (pGameDevice)
	{
		memcpy(&idc_game_entered_message.paytable_id, paytable_id, 6);
		memcpy(&idc_game_entered_message.manufacturer_id, manufacturer_id, 2);

		idc_protocol_header.header = PACKET_IDENTIFIER;
		idc_protocol_header.command_code[0] = POUND_HEADER;
		idc_protocol_header.command_code[1] = GAME_ENTERED;

		idc_protocol_header.mux_id = pGameDevice->memory_game_config.mux_id;
		idc_protocol_header.sas_id = pGameDevice->memory_game_config.sas_id;
		idc_protocol_header.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
		data_length = sizeof(IdcGameEnteredMessage);
		idc_protocol_header.message_length = sizeof(IdcProtocolHeader) + data_length + 1;
		theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)&idc_game_entered_message, data_length, FALSE);
	}
}

void CControllerApp::SendIdcFillValuesMessage (FillValues* fill_values)
{
	IdcProtocolHeader idc_protocol_header;
	WORD data_length;

	idc_protocol_header.header = PACKET_IDENTIFIER;
	idc_protocol_header.command_code[0] = POUND_HEADER;
	idc_protocol_header.command_code[1] = F__FILL_VALUES;

	idc_protocol_header.mux_id = 0;
	idc_protocol_header.sas_id = 0;
	idc_protocol_header.ucmc_id = 0;
	data_length = sizeof(FillValues);
	idc_protocol_header.message_length = sizeof(IdcProtocolHeader) + data_length + 1;
	theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)fill_values, data_length, FALSE);
}

void CControllerApp::SendIdcDispenseInfoMessage (BillMessage* bill_message, DWORD ucmc_id)
{
	IdcProtocolHeader idc_protocol_header;
	WORD data_length;
    DispenseInfoMessage dispense_info_message;
    unsigned int transaction_id;

    // generate a ticket number
    rand_s(&transaction_id);

    dispense_info_message.transaction_id = (DWORD)transaction_id;

	CTime current_time = CTime::GetCurrentTime();
    dispense_info_message.day = current_time.GetDay();
    dispense_info_message.month = current_time.GetMonth();
    dispense_info_message.year = current_time.GetYear();
    dispense_info_message.hour = current_time.GetHour();
    dispense_info_message.minute = current_time.GetMinute();
    dispense_info_message.second = current_time.GetSecond();

    dispense_info_message.bill_amount_100 = bill_message->bill_amount_100;
    dispense_info_message.bill_amount_20 = bill_message->bill_amount_20;
    dispense_info_message.bill_amount_xx = bill_message->bill_amount_xx;
    dispense_info_message.reject_amount_100 = bill_message->reject_amount_100;
    dispense_info_message.reject_amount_20 = bill_message->reject_amount_20;
    dispense_info_message.reject_amount_xx = bill_message->reject_amount_xx;
    dispense_info_message.message_type = bill_message->message_type;

	idc_protocol_header.header = PACKET_IDENTIFIER;
	idc_protocol_header.command_code[0] = POUND_HEADER;
	idc_protocol_header.command_code[1] = D__DISPENSE_INFO;

	idc_protocol_header.mux_id = 0;
	idc_protocol_header.sas_id = 0;
	idc_protocol_header.ucmc_id = ucmc_id;
	data_length = sizeof(DispenseInfoMessage);
	idc_protocol_header.message_length = sizeof(IdcProtocolHeader) + data_length + 1;

    // these fields are used for other purposes in the XPC IDC message
    dispense_info_message.bill_amount_xx = dispenserControl.memory_dispenser_values.dispense_grand_total;
    dispense_info_message.reject_amount_xx = dispenserControl.memory_dispenser_values.tickets_amount;

	theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)&dispense_info_message, data_length, TRUE);
}


void CControllerApp::SendIdcStackerPullMessage (CGameDevice *pGameDevice, BYTE pulled)
{
	IdcProtocolHeader idc_protocol_header;
	WORD data_length;
	StackerPulledMessage stacker_pulled_message;

	stacker_pulled_message.pulled = pulled;

	idc_protocol_header.header = PACKET_IDENTIFIER;
	idc_protocol_header.command_code[0] = POUND_HEADER;
	idc_protocol_header.command_code[1] = S__STACKER_PULL;

	idc_protocol_header.mux_id = pGameDevice->memory_game_config.mux_id;
	idc_protocol_header.sas_id = pGameDevice->memory_game_config.sas_id;
	idc_protocol_header.ucmc_id = pGameDevice->memory_game_config.ucmc_id;

	data_length = sizeof(StackerPulledMessage);
	idc_protocol_header.message_length = sizeof(IdcProtocolHeader) + data_length + 1;

	theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)&stacker_pulled_message, data_length, FALSE);
}

void CControllerApp::SendIdcUserLoginLogoffMessage (DWORD user_id, BYTE login)
{
	IdcProtocolHeader idc_protocol_header;
	WORD data_length;
	UserLoginLogoff logon_logoff_message;

	logon_logoff_message.login_id = user_id;
	logon_logoff_message.log_in = login;

	idc_protocol_header.header = PACKET_IDENTIFIER;
	idc_protocol_header.command_code[0] = POUND_HEADER;
	idc_protocol_header.command_code[1] = L__USER_LOGIN_LOGOFF;

	idc_protocol_header.mux_id = 0;
	idc_protocol_header.sas_id = 0;
	idc_protocol_header.ucmc_id = 0;

	data_length = sizeof(UserLoginLogoff);
	idc_protocol_header.message_length = sizeof(IdcProtocolHeader) + data_length + 1;

	theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)&logon_logoff_message, data_length, FALSE);
}


void CControllerApp::SendIdcSetPromotionWinCategoryMessage (IdcGbAdvantageWinCategory* win_category)
{
	IdcProtocolHeader idc_protocol_header;
	WORD data_length;

	idc_protocol_header.header = PACKET_IDENTIFIER;
	idc_protocol_header.command_code[0] = GB_ADVANTAGE_HEADER;
	idc_protocol_header.command_code[1] = P__SET_PROMOTION_WIN_CATEGORY;

	idc_protocol_header.mux_id = 0;
	idc_protocol_header.sas_id = 0;
	idc_protocol_header.ucmc_id = 0;
	data_length = sizeof(IdcGbAdvantageWinCategory);
	idc_protocol_header.message_length = sizeof(IdcProtocolHeader) + data_length + 1;

	theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)win_category, data_length, FALSE);
}

void CControllerApp::SendIdcGbAdvantageWinMessage (IdcGbAdvantageWinMessage* win_message, CGameDevice *pGameDevice)
{
	IdcProtocolHeader idc_protocol_header;
	WORD data_length;

	idc_protocol_header.header = PACKET_IDENTIFIER;
	idc_protocol_header.command_code[0] = GB_ADVANTAGE_HEADER;
	idc_protocol_header.command_code[1] = W__GB_ADVANTAGE_WIN;

	idc_protocol_header.mux_id = pGameDevice->memory_game_config.mux_id;
	idc_protocol_header.sas_id = pGameDevice->memory_game_config.sas_id;
	idc_protocol_header.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
	data_length = sizeof(IdcGbAdvantageWinMessage);
	idc_protocol_header.message_length = sizeof(IdcProtocolHeader) + data_length + 1;

	theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)win_message, data_length, FALSE);
}

void CControllerApp::SendIdcManualDispenseInfoMessage (ManualDispenseMessageData* manual_dispense_message)
{
	IdcProtocolHeader idc_protocol_header;
	WORD data_length;

	idc_protocol_header.header = PACKET_IDENTIFIER;
	idc_protocol_header.command_code[0] = POUND_HEADER;
	idc_protocol_header.command_code[1] = d__MANUEL_DISPENSE_INFO;

	idc_protocol_header.mux_id = 0;
	idc_protocol_header.sas_id = 0;
	idc_protocol_header.ucmc_id = 0;
	data_length = sizeof(ManualDispenseMessageData);
	idc_protocol_header.message_length = sizeof(IdcProtocolHeader) + data_length + 1;


	theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)manual_dispense_message, data_length, FALSE);
}

void CControllerApp::SendIdcWinningBingoCardMessage (WinningCardInfo* winning_card_info, CGameDevice *pGameDevice)
{
	IdcProtocolHeader idc_protocol_header;
	WORD data_length;

	idc_protocol_header.header = PACKET_IDENTIFIER;
	idc_protocol_header.command_code[0] = COMMAND_HEADER;
	idc_protocol_header.command_code[1] = WINNING_BINGO_CARD;

	idc_protocol_header.mux_id = pGameDevice->memory_game_config.mux_id;
	idc_protocol_header.sas_id = pGameDevice->memory_game_config.sas_id;
	idc_protocol_header.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
	data_length = sizeof(WinningCardInfo);
	idc_protocol_header.message_length = sizeof(IdcProtocolHeader) + data_length + 1;


	theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)winning_card_info, data_length, FALSE);
}

void CControllerApp::QueueIdcTxMessage(IdcProtocolHeader *header, BYTE* data, WORD data_length, bool write_to_disk)
{
	IdcMessageFormat idc_message;

	if (!theApp.memory_settings_ini.masters_ip_address &&
        (memory_settings_ini.idc_ip_address && memory_settings_ini.idc_tcp_port) && (data_length <= sizeof(IdcMessageFormat)))
	{
		memcpy(&idc_message.header, header, sizeof(IdcProtocolHeader));
        // select the message id before queueing the message
		if (idc_message_id >= 0xff)
			idc_message_id = 1;
		else
			idc_message_id++ ;
		idc_message.header.message_id = idc_message_id;

		memcpy(idc_message.data, data, data_length);
		idc_message.data_length = data_length;

        if (write_to_disk)
        {
            fileWrite(PENDING_IDC_TX_MESSAGES, -1, &idc_message, sizeof(idc_message));
        }
        else
    		idc_tx_queue.push(idc_message);
	}
}

void CControllerApp::ProcessRequestMachineData(MachineIdentifiers* machine_identifiers)
{
}

void CControllerApp::CheckIdcSocket()
{
 int last_error;
 WORD port_number;
 CString error_message;

	if (((IdcSocket.socket_state == WSAENOTCONN) ||
	     (IdcSocket.socket_state == WSAECONNREFUSED)))
		OpenIdcSocket();
	else if (IdcSocket.socket_state == WSAETIMEDOUT)
	{
		IdcSocket.Close();
		IdcSocket.socket_state = WSAENOTCONN;
	}
    else
    {
    	error_message.Format("CControllerApp::CheckIdcSocket - Unexpected IdcSocket.socket_state  %d", IdcSocket.socket_state);
		logMessage(error_message);
    }
}

void CControllerApp::OpenIdcSocket()
{
    int last_error;
    CString message_string;
    WORD port_number;
    CString string_data;
    BYTE *pIpAddress;

	if (IdcSocket.m_hSocket == INVALID_SOCKET && !IdcSocket.Create(0, SOCK_STREAM, FD_READ | FD_CLOSE | FD_CONNECT, 0))
	{
		message_string.Format("CControllerApp::OpenIdcSocket - Create error %d.", GetLastError());
		logMessage(message_string);
	}

	if (IdcSocket.m_hSocket != INVALID_SOCKET)
	{
        pIpAddress = (BYTE*)&theApp.memory_settings_ini.idc_ip_address;

//		port_number = 7117;
		port_number = theApp.memory_settings_ini.idc_tcp_port;
/*
#ifdef _DEBUG
		string_data.Format((const char*)"%u.%u.%u.%u", 10, 1, 129, 210);
#endif
*/
    	string_data.Format((const char*)"%u.%u.%u.%u", pIpAddress[0], pIpAddress[1], pIpAddress[2], pIpAddress[3]);
//		if (!IdcSocket.Connect(message_string, memory_gb_pro_settings.gb_pro_media_box_server_tcp_port) != WSAEWOULDBLOCK)
		if (!IdcSocket.Connect(string_data, port_number) != WSAEWOULDBLOCK)
		{
			last_error = GetLastError();

    		message_string.Format("CControllerApp::OpenIdcSocket - Socket Connection error %d.", GetLastError());
			logMessage(message_string);

            if (last_error == WSAEWOULDBLOCK)
                IdcSocket.socket_state = WSAENOTCONN;
            else
                IdcSocket.socket_state = last_error;
		}
	}
}

bool CControllerApp::SendIdcMessage(BYTE *data, int length, bool controller_data, bool event_log_data)
{
	bool successful_send = false;
	int bytes_sent, last_error;
	CString error_message;

    if (!IdcSocket.socket_state || (IdcSocket.socket_state == WSAEISCONN))
    {
		bytes_sent = IdcSocket.Send(data, length);

		if (bytes_sent == SOCKET_ERROR)
		{
			last_error = GetLastError();

    		error_message.Format("CControllerApp::SendIdcMessage - IdcSocket.Send error %d.", GetLastError());
			logMessage(error_message);

			if (last_error != WSAEINPROGRESS && last_error != WSAEWOULDBLOCK)
			{
//				error_message.Format("CControllerApp::SendIdcMessage - closing socket, error, %d.", last_error);
//				logMessage(error_message);
//				IdcSocket.Close();
//				IdcSocket.socket_state = WSAENOTCONN;

			}
		}
		else
			successful_send = true;
    }

 return successful_send;
}

void CControllerApp::CheckCmsSocket()
{
 int last_error;
 WORD port_number;
 CString string_data;

	if (((CmsSocket.socket_state == WSAENOTCONN) ||
	     (CmsSocket.socket_state == WSAECONNREFUSED)))
		OpenCmsSocket();
	else if (CmsSocket.socket_state == WSAETIMEDOUT)
	{
		CmsSocket.Close();
		CmsSocket.socket_state = WSAENOTCONN;
	}
    LogoutAllCmsPlayers();

    if (!cms_com_disconnected)
    {
        cms_com_disconnected = TRUE;
        SendIdcCmsConnectionStatusMessage (TRUE);
    }
        
}

void CControllerApp::OpenCmsSocket()
{
    int last_error;
    CString message_string;
    WORD port_number;
    CString string_data;
    BYTE *pIpAddress;

	if (CmsSocket.m_hSocket == INVALID_SOCKET && !CmsSocket.Create(0, SOCK_STREAM, FD_READ | FD_CLOSE | FD_CONNECT, 0))
	{
		message_string.Format("CControllerApp::OpenCmsSocket - Create error %d.", GetLastError());
		logMessage(message_string);
	}

	if (CmsSocket.m_hSocket != INVALID_SOCKET)
	{
        pIpAddress = (BYTE*)&theApp.memory_settings_ini.cms_ip_address;

		port_number = theApp.memory_settings_ini.cms_tcp_port;
    	string_data.Format((const char*)"%u.%u.%u.%u", pIpAddress[0], pIpAddress[1], pIpAddress[2], pIpAddress[3]);
#ifdef _DEBUG
//		string_data.Format((const char*)"%u.%u.%u.%u", 10, 1, 129, 210);
#endif
//		if (!CmsSocket.Connect(message_string, memory_gb_pro_settings.gb_pro_media_box_server_tcp_port) != WSAEWOULDBLOCK)
		if (!CmsSocket.Connect(string_data, port_number) != WSAEWOULDBLOCK)
		{
			last_error = GetLastError();

            if (last_error == WSAEWOULDBLOCK)
                CmsSocket.socket_state = WSAENOTCONN;
            else
                CmsSocket.socket_state = last_error;
		}
	}
}

bool CControllerApp::SendCmsMessage(BYTE *data, int length, bool controller_data, bool event_log_data)
{
	bool successful_send = false;
	int bytes_sent, last_error;
	CString error_message;
    EventMessage event_message;
    CGameDevice *pGameDevice = NULL;

    if (!CmsSocket.socket_state || (CmsSocket.socket_state == WSAEISCONN))
    {
		bytes_sent = CmsSocket.Send(data, length);

		if (bytes_sent == SOCKET_ERROR)
		{
			last_error = GetLastError();

			if (last_error != WSAEINPROGRESS && last_error != WSAEWOULDBLOCK)
			{
//				error_message.Format("CControllerApp::SendCmsMessage - closing socket, error, %d.", last_error);
//				logMessage(error_message);
				CmsSocket.Close();
				CmsSocket.socket_state = WSAENOTCONN;
                LogoutAllCmsPlayers();
			}
		}
		else
			successful_send = true;
    }

    return successful_send;
}

void CControllerApp::PurgePendingCmsTxMessage (CmsMessageFormat* cms_message)
{
	CmsMessageFormat cms_message_on_disk;
    DWORD total_messages = fileRecordTotal(PENDING_CMS_TX_MESSAGES, sizeof(CmsMessageFormat));

    if (total_messages)
    {
        if (fileRead(PENDING_CMS_TX_MESSAGES, 0, &cms_message_on_disk, sizeof(CmsMessageFormat)))
        {
			if ((cms_message->header.command_code[0] == cms_message_on_disk.header.command_code[0]) &&
				(cms_message->header.command_code[1] == cms_message_on_disk.header.command_code[1]))
                fileDeleteRecord(PENDING_CMS_TX_MESSAGES, sizeof(CmsMessageFormat), 0);
        }
    }
}

void CControllerApp::QueuePendingCmsTxMessages (void)
{
	CmsMessageFormat cms_message_on_disk;
    DWORD total_messages = fileRecordTotal(PENDING_CMS_TX_MESSAGES, sizeof(CmsMessageFormat));

    while (total_messages)
    {
        if (fileRead(PENDING_CMS_TX_MESSAGES, (total_messages - 1), &cms_message_on_disk, sizeof(CmsMessageFormat)))
        {
    		cms_tx_queue.push(cms_message_on_disk);
        }
        total_messages--;
    }
}

void CControllerApp::CheckCmsRxSocket()
{
 BYTE calculated_crc, packet_crc;
 CString message;
 CmsProtocolHeader *header;
 CmsMessageFormat cms_message;
 int bytes_received = CmsSocket.GetBuffer(&cms_tcp_ip_rx.data[cms_tcp_ip_rx.index],
    TCPIP_MESSAGE_BUFFER_SIZE - cms_tcp_ip_rx.index);

	if (bytes_received)
	{
        cms_tcp_ip_rx.index += bytes_received;

        while (cms_tcp_ip_rx.index >= sizeof(CmsProtocolHeader))
        {
            header = (CmsProtocolHeader*)cms_tcp_ip_rx.data;

            if (header->header != (BYTE)PACKET_IDENTIFIER)
            {
                memset(&cms_tcp_ip_rx, 0, sizeof(cms_tcp_ip_rx));
                logMessage("CControllerApp::CheckCmsRxSocket - incorrect packet header.");
            }
            else
            {
                bytes_received = header->message_length;

                if (cms_tcp_ip_rx.index < bytes_received)
                {
                    return;
                }
                else
                {
                    // calculate and compare crc
                    packet_crc = cms_tcp_ip_rx.data[header->message_length - 1];
                    calculated_crc = CalculateMessageCrc(cms_tcp_ip_rx.data, header->message_length - 1);

                    if (calculated_crc != packet_crc)
					{
                        logMessage("CControllerApp::CheckCmsRxSocket - CRC mismatch.");
    					SendCmsAckMessage (header, FALSE);
					}
                    else
                    {
						if (!cms_tx_queue.empty())
						{
							cms_message = cms_tx_queue.front();

							// if this is an ack, then remove message from the TX queue
							if ((header->command_code[0] == cms_message.header.command_code[0]) &&
								(header->command_code[1] == cms_message.header.command_code[1]))
                            {
                                if (cms_message.critical_message)
    								ProcessCmsCriticalMessageAck();
                            }
							else
								ProcessCmsRxMessage();
						}
						else
							ProcessCmsRxMessage();
					}

                    if (cms_tcp_ip_rx.index == bytes_received)
                        memset(&cms_tcp_ip_rx, 0, sizeof(cms_tcp_ip_rx));
                    else
                    {
                        memset(cms_tcp_ip_rx.data, 0, bytes_received);
                        cms_tcp_ip_rx.index -= bytes_received;
                        memmove(cms_tcp_ip_rx.data, &cms_tcp_ip_rx.data[bytes_received], cms_tcp_ip_rx.index);
                        memset(&cms_tcp_ip_rx.data[cms_tcp_ip_rx.index], 0, bytes_received);
                    }
                }
            }
        }
    }

}

void CControllerApp::CheckCmsTxQueue()
{
 int message_length;
 CmsMessageFormat cms_message;
 EventMessage event_message;
 bool successful_send = FALSE;
 
    if (!cms_tx_queue.empty() && !theApp.memory_settings_ini.masters_ip_address)
    {
        cms_message = cms_tx_queue.front();

        if (sameCmsTxMessage(&cms_message))
        {
            cms_tx_no_response_counter++;
            if (cms_tx_no_response_counter > 30)
            {
                cms_tx_no_response_counter = 0;
		        CmsSocket.Close();
		        CmsSocket.socket_state = WSAENOTCONN;
                logMessage("CControllerApp::CheckCmsTxQueue - IDC tx no response, reconnecting.");
                LogoutAllCmsPlayers();
            }
        }
        else
        {
            cms_tx_no_response_counter = 0;
        }

		cms_message.data[cms_message.data_length] = CalculateMessageCrc((BYTE*)&cms_message, cms_message.header.message_length - 1);

        // copy the current message into the last message
        memcpy(&last_cms_tx_message, &cms_message, sizeof(CmsMessageFormat));

        successful_send = SendCmsMessage((BYTE*)&cms_message, cms_message.header.message_length, false, false);

        if ((cms_message.header.command_code[0] == GB_ADVANTAGE_HEADER) &&
	        (cms_message.header.command_code[1] == W__GB_ADVANTAGE_WIN))
        {
            memset(&event_message, 0, sizeof(event_message));
            event_message.head.time_stamp = time(NULL);
            event_message.head.event_type = CMS_GBA_WIN_EVENT;
            event_message.head.data_length = cms_message.header.message_length - 1;
            event_message.head.port_number = 0;
            memcpy(event_message.data, &cms_message, event_message.head.data_length);

            event_message.head.customer_id = theApp.memory_location_config.customer_id;

            event_message_queue.push(event_message);
        }

        if (successful_send && !cms_message.critical_message)
            cms_tx_queue.pop();
    }
}

bool CControllerApp::sameCmsTxMessage(CmsMessageFormat* cms_message)
{
	bool same = TRUE;

	if (memcmp(&cms_message->header.command_code, &last_cms_tx_message.header.command_code, sizeof(cms_message->header.command_code)))
		same = FALSE;

	if (memcmp(&cms_message->header.message_length, &last_cms_tx_message.header.message_length, sizeof(cms_message->header.message_length)))
		same = FALSE;

	if (memcmp(&cms_message->header.message_id, &last_cms_tx_message.header.message_id, sizeof(cms_message->header.message_id)))
		same = FALSE;

	if (strcmp(cms_message->header.machine_id, last_cms_tx_message.header.machine_id))
		same = FALSE;

	if (memcmp(&cms_message->data_length, &last_cms_tx_message.data_length, sizeof(cms_message->data_length)))
		same = FALSE;

	if (memcmp(&cms_message->data, &last_cms_tx_message.data, sizeof(cms_message->data_length)))
		same = FALSE;

	return same;
}

void CControllerApp::ProcessCmsRxMessage(void)
{
 CString log_message;
	CmsProtocolHeader *header;

	header = (CmsProtocolHeader*)cms_tcp_ip_rx.data;

    switch (header->command_code[0])
    {
        case '!':
			switch (header->command_code[1])
			{
				case 'C':
					ProcessCmsLoginMessage();
					SendCmsAckMessage (header, TRUE);
                    cms_players_logged_in = TRUE;
					break;
				case 'U':
                    ProcessCmsLogoutMessage();
					SendCmsAckMessage (header, TRUE);
					break;
                default:
					SendCmsAckMessage (header, FALSE);
                    break;
			}
			break;
        default:
//            logMessage(log_message);
//			SendCmsAckMessage (header, FALSE);
            return;
    }

}

void CControllerApp::SendCmsAckMessage (CmsProtocolHeader *header, bool ack)
{
	CmsMessageFormat cms_message;

	memcpy(&cms_message.header, header, sizeof(CmsProtocolHeader));
    if (ack)
    	cms_message.data[0] = 'A';
    else
    	cms_message.data[0] = 'N';
	cms_message.data_length = 1;

	cms_message.header.message_length = 15;
	cms_message.header.message_id = header->message_id;

	cms_message.data[cms_message.data_length] = CalculateMessageCrc((BYTE*)&cms_message, cms_message.header.message_length - 1);
	// send the ack to the TCP/IP socket connection
    SendCmsMessage((BYTE*)&cms_message, cms_message.header.message_length, false, false);
}

void CControllerApp::QueueCmsTxMessage(CmsProtocolHeader *header, BYTE* data, WORD data_length, bool critical_message)
{
	CmsMessageFormat cms_message;

	if (!theApp.memory_settings_ini.masters_ip_address && (data_length <= sizeof(CmsMessageFormat)))
	{
		memcpy(&cms_message.header, header, sizeof(CmsProtocolHeader));
        cms_message.critical_message = critical_message;

        // select the message id before queueing the message
		if (cms_message_id >= 0xff)
			cms_message_id = 1;
		else
			cms_message_id++ ;
		cms_message.header.message_id = cms_message_id;

        if (data && data_length)
    		memcpy(cms_message.data, data, data_length);
		cms_message.data_length = data_length;
		cms_tx_queue.push(cms_message);

        if (critical_message && (header->command_code[0] == GB_ADVANTAGE_HEADER) && (header->command_code[1] == W__GB_ADVANTAGE_WIN))
            fileWrite(PENDING_CMS_TX_MESSAGES, -1, &cms_message, sizeof(cms_message));
	}
}

void CControllerApp::ProcessCmsLoginMessage (void)
{
	CmsProtocolHeader *header = (CmsProtocolHeader*)cms_tcp_ip_rx.data;
	CmsLoginMessage* login_message = (CmsLoginMessage*)&cms_tcp_ip_rx.data[sizeof(CmsProtocolHeader)];
    EventMessage event_message;
	char machine_id[9];
	CGameDevice* pGameDevice = NULL;

	memset(machine_id, 0, 9);
	memcpy(machine_id, header->machine_id, 8);

	pGameDevice = GetSasGamePointer(machine_id);

    if (pGameDevice)
    {
        pGameDevice->account_login_data.account_number = login_message->account_number;

        memcpy(&pGameDevice->cms_account_login_data, login_message, sizeof(CmsLoginMessage));
    }

    memset(&event_message, 0, sizeof(event_message));
    event_message.head.time_stamp = time(NULL);
    event_message.head.event_type = CMS_LOGIN_EVENT;
    event_message.head.data_length = sizeof(CmsLoginMessage);
    event_message.head.port_number = 0;
    memcpy(event_message.data, (BYTE*)login_message, event_message.head.data_length);

    event_message.head.customer_id = theApp.memory_location_config.customer_id;

    event_message_queue.push(event_message);

	if (pGameDevice && memory_settings_ini.idc_ip_address && memory_settings_ini.idc_tcp_port)
        SendIdcCmsLoginMessage (pGameDevice, login_message);
}

void CControllerApp::ProcessCmsLogoutMessage (void)
{
	CmsProtocolHeader *header = (CmsProtocolHeader*)cms_tcp_ip_rx.data;
	DWORD* logout_message = (DWORD*)&cms_tcp_ip_rx.data[sizeof(CmsProtocolHeader)];
    EventMessage event_message;
	char machine_id[9];
	CGameDevice* pGameDevice = NULL;

	memset(machine_id, 0, 9);
	memcpy(machine_id, header->machine_id, 8);

	pGameDevice = GetSasGamePointer(machine_id);

    if (pGameDevice)
        pGameDevice->account_login_data.account_number = 0;

    memset(&event_message, 0, sizeof(event_message));
    event_message.head.time_stamp = time(NULL);
    event_message.head.event_type = CMS_LOGOUT_EVENT;
    event_message.head.data_length = sizeof(DWORD);
    event_message.head.port_number = 0;
    memcpy(event_message.data, (BYTE*)logout_message, event_message.head.data_length);

    event_message.head.customer_id = theApp.memory_location_config.customer_id;

    event_message_queue.push(event_message);

	if (memory_settings_ini.idc_ip_address && memory_settings_ini.idc_tcp_port)
        SendIdcCmsLogoutMessage (header, *logout_message);
}

void CControllerApp::ProcessCmsCriticalMessageAck (void)
{
//	CmsProtocolHeader *header = (CmsProtocolHeader*)cms_tcp_ip_rx.data;
	CmsMessageFormat *cms_message_format = (CmsMessageFormat*)cms_tcp_ip_rx.data;
	BYTE* ack = (BYTE*)&cms_tcp_ip_rx.data[sizeof(CmsProtocolHeader)];
    EventMessage event_message;
    CmsMessageFormat cms_message;


	if (!cms_tx_queue.empty())
    {
		cms_message = cms_tx_queue.front();

        if (cms_message_format->header.message_id == cms_message.header.message_id)
        {
            if ((cms_tcp_ip_rx.data[sizeof(CmsProtocolHeader)] == 'A'))
            {
			    cms_tx_queue.pop();
                cms_tx_message_ack_pending = FALSE;
			    cms_tx_no_response_counter = 0;

                switch (cms_message_format->header.command_code[0])
                {
                    case GB_ADVANTAGE_HEADER:
			            switch (cms_message_format->header.command_code[1])
			            {
				            case W__GB_ADVANTAGE_WIN:
					            PurgePendingCmsTxMessage(&cms_message);
					            break;
			            }
                }
            }
            else if (cms_tcp_ip_rx.data[sizeof(CmsProtocolHeader)] == 'N')
            {
                if (cms_tx_nack_counter > 3)
                {
				    cms_tx_queue.pop();
                    cms_tx_message_ack_pending = FALSE;

                    switch (cms_message_format->header.command_code[0])
                    {
                        case GB_ADVANTAGE_HEADER:
			                switch (cms_message_format->header.command_code[1])
			                {
				                case W__GB_ADVANTAGE_WIN:
					                PurgePendingCmsTxMessage(&cms_message);
					                break;
			                }
                    }
                    cms_tx_nack_counter  = 0;
				    CString message_string;
		            message_string.Format("CControllerApp::ProcessCmsCriticalMessageAck - 3 nacks for message %c %c",
		                                    cms_message_format->header.command_code[0], cms_message_format->header.command_code[1]);
		            logMessage(message_string);
                }
                else
                    cms_tx_nack_counter++;
            }
        }

        memset(&event_message, 0, sizeof(event_message));
        event_message.head.time_stamp = time(NULL);
        if ((cms_message_format->header.command_code[0] == GB_ADVANTAGE_HEADER) && (cms_message_format->header.command_code[1] == W__GB_ADVANTAGE_WIN))
            event_message.head.event_type = CMS_GBA_WIN_ACK_EVENT;
        else if ((cms_message_format->header.command_code[0] == POUND_HEADER) && (cms_message_format->header.command_code[1] == L__LOGIN_STATUS_REQUEST))
            event_message.head.event_type = CMS_LOGIN_STATUS_REQUEST_ACK_EVENT;
        event_message.head.data_length = sizeof(CmsProtocolHeader) + sizeof(BYTE);
        event_message.head.port_number = 0;
        memcpy(event_message.data, (BYTE*)cms_message_format, event_message.head.data_length);

        event_message.head.customer_id = theApp.memory_location_config.customer_id;

        event_message_queue.push(event_message);
    }
}

void CControllerApp::SendCmsGbAdvantageWinMessage (DWORD ucmc_id, IdcGbAdvantageWinMessage* win_message)
{
    CmsProtocolHeader cms_header;
    CGameDevice *pGameDevice = NULL;
    WORD data_length;

    cms_header.header = PACKET_IDENTIFIER;
    cms_header.command_code[0] = GB_ADVANTAGE_HEADER;
    cms_header.command_code[1] = W__GB_ADVANTAGE_WIN;

    memset(&cms_header.machine_id, 0, sizeof(cms_header.machine_id));

    pGameDevice = GetSasGamePointer(ucmc_id);

    if (!pGameDevice)
    	pGameDevice = GetGamePointer(ucmc_id);

    if (pGameDevice)
        strcpy(cms_header.machine_id, pGameDevice->memory_game_config.machine_id);

	data_length = sizeof(IdcGbAdvantageWinMessage);
    cms_header.message_length = sizeof(CmsProtocolHeader) + data_length + 1;

    theApp.QueueCmsTxMessage(&cms_header, (BYTE*)win_message, data_length, TRUE);
}

void CControllerApp::SendCmsXpcVersionConfigInfo(void)
{
    CmsProtocolHeader cms_header;
    XpcVersionAndConfigInfo xpc_version_and_config_info;
	WORD data_length;

    xpc_version_and_config_info.version = VERSION;    
    memset(&xpc_version_and_config_info.subversion, 0, sizeof(xpc_version_and_config_info.subversion));
    if (strlen(SUBVERSION) <= sizeof(xpc_version_and_config_info.subversion))
        strcpy(xpc_version_and_config_info.subversion, SUBVERSION);

    if (pGbProManager)
        xpc_version_and_config_info.gba_configured_location = (theApp.memory_settings_ini.activate_gb_advantage_support &&
                                                               pGbProManager->memory_gb_pro_settings.gb_pro_configurator_ip_address &&
                                                    	       pGbProManager->memory_gb_pro_settings.gb_pro_configurator_tcp_port);    
	memset(&cms_header, 0, sizeof(CmsProtocolHeader));
    cms_header.header = PACKET_IDENTIFIER;
    cms_header.command_code[0] = POUND_HEADER;
    cms_header.command_code[1] = V__XPC_VERSION_AND_CONFIGURATION_INFO;

	data_length = sizeof(XpcVersionAndConfigInfo);
	cms_header.message_length = sizeof(CmsProtocolHeader) + data_length + 1;
	theApp.QueueCmsTxMessage(&cms_header, (BYTE*)&xpc_version_and_config_info, data_length, FALSE);
}

void CControllerApp::SendCmsLoginStatusRequest(void)
{
    CmsProtocolHeader cms_header;
	WORD data_length;

	memset(&cms_header, 0, sizeof(CmsProtocolHeader));
    cms_header.header = PACKET_IDENTIFIER;
    cms_header.command_code[0] = POUND_HEADER;
    cms_header.command_code[1] = L__LOGIN_STATUS_REQUEST;

	data_length = 0;
	cms_header.message_length = sizeof(CmsProtocolHeader) + data_length + 1;
	theApp.QueueCmsTxMessage(&cms_header, NULL, data_length, TRUE);
}

void CControllerApp::SendIdcCmsLoginMessage (CGameDevice* pGameDevice, CmsLoginMessage* login_message)
{
	IdcProtocolHeader idc_protocol_header;
	WORD data_length;
	char machine_id[9];
    IdcCmsLoginMessage idc_cms_login_message;

	if (memory_settings_ini.idc_ip_address && memory_settings_ini.idc_tcp_port)
    {
        if (pGameDevice)
        {
	        idc_protocol_header.header = PACKET_IDENTIFIER;
	        idc_protocol_header.command_code[0] = CMS_COMMAND_HEADER;
	        idc_protocol_header.command_code[1] = ACCOUNT_INQUIRY;

	        idc_protocol_header.mux_id = pGameDevice->memory_game_config.mux_id;
	        idc_protocol_header.sas_id = pGameDevice->memory_game_config.sas_id;
	        idc_protocol_header.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
	        data_length = sizeof(IdcCmsLoginMessage);
	        idc_protocol_header.message_length = sizeof(IdcProtocolHeader) + data_length + 1;

            memcpy(&idc_cms_login_message, login_message, sizeof(CmsLoginMessage));

            memcpy(idc_cms_login_message.machine_id, pGameDevice->memory_game_config.machine_id, 8);
 	        theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)&idc_cms_login_message, data_length, FALSE);
        }
    }
}

void CControllerApp::SendIdcCmsLogoutMessage (CmsProtocolHeader *header, DWORD account_number)
{
	IdcProtocolHeader idc_protocol_header;
	WORD data_length;
	char machine_id[9];
	CGameDevice* pGameDevice = NULL;

	if (memory_settings_ini.idc_ip_address && memory_settings_ini.idc_tcp_port)
    {
	    memset(machine_id, 0, 9);
	    memcpy(machine_id, header->machine_id, 8);

	    pGameDevice = GetSasGamePointer(machine_id);

        if (pGameDevice)
        {
		    idc_protocol_header.header = PACKET_IDENTIFIER;
		    idc_protocol_header.command_code[0] = CMS_COMMAND_HEADER;
		    idc_protocol_header.command_code[1] = ACCOUNT_LOGOUT;

	        idc_protocol_header.mux_id = pGameDevice->memory_game_config.mux_id;
	        idc_protocol_header.sas_id = pGameDevice->memory_game_config.sas_id;
	        idc_protocol_header.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
		    data_length = sizeof(DWORD);
		    idc_protocol_header.message_length = sizeof(IdcProtocolHeader) + data_length + 1;

		    theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)&account_number, data_length, FALSE);
        }
    }
}

void CControllerApp::SendIdcCmsConnectionStatusMessage (bool disconnect)
{
	IdcProtocolHeader idc_protocol_header;
	WORD data_length;
    BYTE disconnect_status;

	if (memory_settings_ini.idc_ip_address && memory_settings_ini.idc_tcp_port)
    {
		idc_protocol_header.header = PACKET_IDENTIFIER;
		idc_protocol_header.command_code[0] = CMS_COMMAND_HEADER;
		idc_protocol_header.command_code[1] = S__XPC_CMS_CONNECTION_STATUS;

	    idc_protocol_header.mux_id = 0;
	    idc_protocol_header.sas_id = 0;
	    idc_protocol_header.ucmc_id = 0;
		data_length = sizeof(BYTE);
		idc_protocol_header.message_length = sizeof(IdcProtocolHeader) + data_length + 1;

        if (disconnect)
            disconnect_status = 1;
        else     
            disconnect_status = 0;

		theApp.QueueIdcTxMessage(&idc_protocol_header, (BYTE*)&disconnect_status, data_length, FALSE);
    }
}

// return true if game lock filename found
bool CControllerApp::GetGameLockFilename(DWORD ucmc_id, DWORD amount, char *filename)
{
 bool game_lock_found = false;
 HANDLE hSearch;
 CString search_string;
 WIN32_FIND_DATA find_return;

    search_string.Format("%s\\%u-%u-*", GAME_LOCKS_PATH, ucmc_id, amount);
    hSearch = FindFirstFile(search_string, &find_return);

    if (hSearch != INVALID_HANDLE_VALUE)
    {
        sprintf_s(filename, MAX_PATH, "%s\\%s", GAME_LOCKS_PATH, find_return.cFileName);
        game_lock_found = true;
        FindClose(hSearch);
    }

 return game_lock_found;
}

void CControllerApp::QueueStratusTxMessage(StratusMessageFormat *tx_message, bool write_to_disk)
{
    if (memory_settings_ini.stratus_ip_address && memory_settings_ini.stratus_tcp_port)
    {
	    endian2(tx_message->task_code);
	    endian2(tx_message->property_id);
	    endian2(tx_message->club_code);
	    endian2(tx_message->serial_port);
	    endian2(tx_message->mux_id);
	    endian2(tx_message->send_code);
	    endian4(tx_message->customer_id);
	    endian4(tx_message->ucmc_id);
	    stratus_tx_queue.push(*tx_message);

        if (write_to_disk)
            fileWrite(PENDING_STRATUS_TX_MESSAGES, -1, tx_message, sizeof(StratusMessageFormat));
    }
}

void CControllerApp::QueueMarkerStratusTxMessage(StratusMessageFormat *tx_message)
{
    // store the elements in the marker_stratus_tx_queue while we are looking through it
	queue <StratusMessageFormat, deque<StratusMessageFormat> > temp_marker_stratus_tx_queue;
    StratusMessageFormat stratus_message;
    bool message_type_already_queued = FALSE; 

	if (memory_settings_ini.stratus_ip_address && memory_settings_ini.stratus_tcp_port)
    {
	    endian2(tx_message->task_code);
	    endian2(tx_message->property_id);
	    endian2(tx_message->club_code);
	    endian2(tx_message->serial_port);
	    endian2(tx_message->mux_id);
	    endian2(tx_message->send_code);
	    endian4(tx_message->customer_id);
	    endian4(tx_message->ucmc_id);

        // check the marker stratus tx queue to see if there is already a message of that type in it.
        while (!marker_stratus_tx_queue.empty())
        {
            stratus_message = marker_stratus_tx_queue.front();

            if ((stratus_message.ucmc_id == tx_message->ucmc_id) && (stratus_message.send_code == tx_message->send_code))
            {
                message_type_already_queued = TRUE;
                break;
            }

            if (!message_type_already_queued)
            {
                temp_marker_stratus_tx_queue.push(stratus_message);
                if ((!marker_stratus_tx_queue.empty()))
                {
                    marker_stratus_tx_queue.pop();
                }
            }
        }

        // put the marker stratus tx queue elements back where we got them
        while (!temp_marker_stratus_tx_queue.empty())
        {
            stratus_message = temp_marker_stratus_tx_queue.front();
            marker_stratus_tx_queue.push(stratus_message);
            temp_marker_stratus_tx_queue.pop();
        }
        // put the new message on the queue if there isn't already one queued
        if (!message_type_already_queued)
    		marker_stratus_tx_queue.push(*tx_message);
    }
}

// tx: report sequence(short), customer machine count(short), current report ucmc id(long), drop(long), payout(long)
void CControllerApp::RequestNetwinFromStratus(NetwinGame netwin_message, DWORD customer_id)
{
 StratusMessageFormat stratus_msg;

    endian2(netwin_message.report_sequence);
    endian2(netwin_message.machine_count);
    endian4(netwin_message.ucmc_id);
    endian4(netwin_message.drop);
    endian4(netwin_message.payout);
    endian4(netwin_message.handle);

    memset(&stratus_msg, 0, sizeof(stratus_msg));
    stratus_msg.send_code = TKT_NET_WIN_REPORT;
    stratus_msg.task_code = TKT_TASK_TICKET;
    stratus_msg.mux_id = CONTROLLER_MUX_ADDRESS;
    stratus_msg.ucmc_id = memory_dispenser_config.ucmc_id;
    stratus_msg.message_length = sizeof(netwin_message) + STRATUS_HEADER_SIZE;
    stratus_msg.property_id = memory_location_config.property_id;
    stratus_msg.customer_id = customer_id;
    stratus_msg.data_to_event_log = true;
    memcpy(stratus_msg.data, &netwin_message, sizeof(netwin_message));

	// get club code
	if (customer_id != memory_location_config.customer_id)
	{
		CGameDevice *pGameDevice;
		CGameCommControl *pGameCommControl = pHeadGameCommControl;

		while (!stratus_msg.club_code && pGameCommControl)
		{
			pGameDevice = pGameCommControl->pHeadGameDevice;

			while (!stratus_msg.club_code && pGameDevice)
			{
				if (customer_id == pGameDevice->memory_game_config.customer_id)
					stratus_msg.club_code = pGameDevice->memory_game_config.club_code;

				pGameDevice = pGameDevice->pNextGame;
			}

			pGameCommControl = pGameCommControl->pNextGameCommControl;
		}
	}

	if (!stratus_msg.club_code)
		stratus_msg.club_code = memory_location_config.club_code;

    QueueStratusTxMessage(&stratus_msg, FALSE);
}

void CControllerApp::CheckCustomerControlUdpSocket()
{
 CString error_message;

    if (customerControlUdpSocket.m_hSocket == INVALID_SOCKET)
    {
        if (!customerControlUdpSocket.Create(CUSTOMER_CONTROL_CLIENT_UDP_PORT, SOCK_DGRAM, 0))
        {
//            error_message.Format("Error Creating Customer Control UDP Socket: %d.", GetLastError());
//            logMessage(error_message);
            customer_control_udp_time_check = time(NULL);
        }
        else
			customer_control_udp_time_check = 0;
    }
    else
    {
        customerControlUdpSocket.Close();
        customer_control_udp_time_check = time(NULL);
    }
}

void CControllerApp::OnMouseMove()
{
    if (current_user.autologout_timeout)
        current_user.time_setting = time(NULL);

    if (sound_game_lock)
    {
        PlaySound(NULL, NULL, 0);
        sound_game_lock = 0;
    }
}

void CControllerApp::CheckMarkerPrinterSocket()
{
 int last_error;
 CString string_data;

	if (pMarkerPrinterSocket->m_hSocket == INVALID_SOCKET)
		pMarkerPrinterSocket->Create(0, SOCK_STREAM, 0, 0);

	marker_printer_time_check = time(NULL);

	if (pMarkerPrinterSocket->m_hSocket != INVALID_SOCKET)
	{
		BYTE *ip_address = (BYTE*)&memory_settings_ini.marker_printer_ip_address;
		string_data.Format("%u.%u.%u.%u", ip_address[0], ip_address[1], ip_address[2], ip_address[3]);

		if (pMarkerPrinterSocket->Connect(string_data, memory_settings_ini.marker_printer_port))
			marker_printer_time_check = 0;
		else
		{
			last_error = GetLastError();

			if (last_error == WSAEISCONN || last_error == WSAEINPROGRESS || last_error == WSAEWOULDBLOCK)
				marker_printer_time_check = 0;
			else
			{
				marker_printer_time_check = time(NULL);
				pMarkerPrinterSocket->Close();
			}
		}
	}
}

void CControllerApp::CheckGameLockPrinterSocket()
{
 int last_error;
 CString string_data;

    if (pGameLockPrinterSocket->m_hSocket == INVALID_SOCKET)
		pGameLockPrinterSocket->Create(0, SOCK_STREAM, 0, 0);

	game_lock_printer_time_check = time(NULL);

    if (pGameLockPrinterSocket->m_hSocket != INVALID_SOCKET)
	{
		BYTE *ip_address = (BYTE*)&memory_settings_ini.game_lock_printer_ip_address;
		string_data.Format("%u.%u.%u.%u", ip_address[0], ip_address[1], ip_address[2], ip_address[3]);

		if (pGameLockPrinterSocket->Connect(string_data, GAME_LOCK_PRINTER_SOCKET_PORT))
			game_lock_printer_time_check = 0;
		else
		{
			last_error = GetLastError();

			if (last_error == WSAEISCONN || last_error == WSAEINPROGRESS || last_error == WSAEWOULDBLOCK)
				game_lock_printer_time_check = 0;
			else
			{
				game_lock_printer_time_check = time(NULL);
				pGameLockPrinterSocket->Close();
			}
		}
	}
}

void CControllerApp::CloseSmtpSocket()
{
#if 0
??????? move to customer control server

 CString send_data;

    if (smtpSocket.m_hSocket != INVALID_SOCKET)
    {
        Sleep(2000);

        send_data = "QUIT\r\n";
        if (smtpSocket.Send(send_data.GetBuffer(0), send_data.GetLength()) != send_data.GetLength())
        {
            send_data.Format("CControllerApp::CloseSmtpSocket - error sending QUIT: %d.", GetLastError());
            logMessage(send_data);
        }

        smtpSocket.Close();
    }

    smtp_socket_check.last_error = 0;
    smtp_socket_check.time = time(NULL) + SMTP_SOCKET_CHECK;
#endif
}

void CControllerApp::SendEmailMessage(EmailMessageFormat *email_message)
{
#if 0
??????? move to customer control server
    CAsyncSocket            smtpSocket;

 int file_index;
 static time_t last_email_sent;
 UINT client_port;
 CString error_message, send_data, client_address, sHeader;
 FileEmailRecord email_record;

    if (last_email_sent < time(NULL) || last_email_sent > time(NULL) + SMTP_SOCKET_CHECK)
    {
        last_email_sent = time(NULL) + SMTP_SOCKET_CHECK;

        // set date/time stamp
        TIME_ZONE_INFORMATION time_zone_info;
        int Bias;
        CTime current_time(CTime::GetCurrentTime());
        CString date_string(current_time.Format("%a, %d %b %Y %H:%M:%S ")), time_zone_bias;
        if (GetTimeZoneInformation(&time_zone_info) == TIME_ZONE_ID_DAYLIGHT)
            Bias = time_zone_info.Bias + time_zone_info.DaylightBias;
        else
            Bias = time_zone_info.Bias;
        time_zone_bias.Format("%+.2d%.2d", -Bias/60, Bias%60);
        date_string += time_zone_bias;

        sHeader.Format("From: xp_controller@unitedcoin.com\r\nTo: XP Controller Alert\r\nSubject: %s\r\n"
            "Date: %s\r\nX-Mailer: XP CONTROLLER v%u.%s\r\nReply-To: delquist@unitedcoin.com\r\n"
            "MIME-Version: 1.0\r\nContent-type: text/plain; charset=US-ASCII\r\n\r\n",
            email_message->subject, date_string, VERSION, SUBVERSION);

        if (!smtpSocket.GetPeerName(client_address, client_port))
        {
            smtp_socket_check.last_error = GetLastError();
            smtp_socket_check.time = time(NULL) + SMTP_SOCKET_CHECK;
            error_message.Format("CControllerApp::SendEmailMessage - error GetPeerName: %d.", smtp_socket_check.last_error);
            logMessage(error_message);
            CloseSmtpSocket();
            return;
        }
        else
        {
            send_data = "EHLO " + client_address + "\r\n";
            if (smtpSocket.Send(send_data.GetBuffer(0), send_data.GetLength()) != send_data.GetLength())
            {
                smtp_socket_check.last_error = GetLastError();
                smtp_socket_check.time = time(NULL) + SMTP_SOCKET_CHECK;
                error_message.Format("CControllerApp::SendEmailMessage - error sending EHLO: %d.", smtp_socket_check.last_error);
                logMessage(error_message);
                CloseSmtpSocket();
                return;
            }
        }

        send_data = "MAIL FROM:<xp_controller@unitedcoin.com>\r\n";
        if (smtpSocket.Send(send_data.GetBuffer(0), send_data.GetLength()) != send_data.GetLength())
        {
            smtp_socket_check.last_error = GetLastError();
            smtp_socket_check.time = time(NULL) + SMTP_SOCKET_CHECK;
            error_message.Format("CControllerApp::SendEmailMessage - error sending MAIL FROM: %d.", smtp_socket_check.last_error);
            logMessage(error_message);
            CloseSmtpSocket();
            return;
        }

        file_index = 0;
        while (fileRead(EMAIL_ADDRESS_LIST, file_index, &email_record, sizeof(email_record)))
        {
            if (email_record.email_flags & email_message->type)
            {
                send_data.Format("RCPT TO:<%s>\r\n", email_record.email_address);
                if (smtpSocket.Send(send_data.GetBuffer(0), send_data.GetLength()) != send_data.GetLength())
                {
                    smtp_socket_check.last_error = GetLastError();
                    smtp_socket_check.time = time(NULL) + SMTP_SOCKET_CHECK;
                    error_message.Format("CControllerApp::SendEmailMessage - error sending RCPT TO: %d.", smtp_socket_check.last_error);
                    logMessage(error_message);
                    CloseSmtpSocket();
                    return;
                }
            }

            file_index++;
        }

        send_data = "DATA\r\n";
        if (smtpSocket.Send(send_data.GetBuffer(0), send_data.GetLength()) != send_data.GetLength())
        {
            smtp_socket_check.last_error = GetLastError();
            smtp_socket_check.time = time(NULL) + SMTP_SOCKET_CHECK;
            error_message.Format("CControllerApp::SendEmailMessage - error sending DATA: %d.", smtp_socket_check.last_error);
            logMessage(error_message);
            CloseSmtpSocket();
            return;
        }

        if (smtpSocket.Send(sHeader.GetBuffer(0), sHeader.GetLength()) != sHeader.GetLength())
        {
            smtp_socket_check.last_error = GetLastError();
            smtp_socket_check.time = time(NULL) + SMTP_SOCKET_CHECK;
            error_message.Format("CControllerApp::SendEmailMessage - error sending header: %d.", smtp_socket_check.last_error);
            logMessage(error_message);
            CloseSmtpSocket();
            return;
        }

        send_data.Format("%s, %s\r\n", memory_location_config.name, memory_location_config.address);
        if (smtpSocket.Send(send_data.GetBuffer(0), send_data.GetLength()) != send_data.GetLength())
        {
            smtp_socket_check.last_error = GetLastError();
            smtp_socket_check.time = time(NULL) + SMTP_SOCKET_CHECK;
            error_message.Format("CControllerApp::SendEmailMessage - error sending location name and address: %d.", smtp_socket_check.last_error);
            logMessage(error_message);
            CloseSmtpSocket();
            return;
        }

        if (smtpSocket.Send(email_message->body.GetBuffer(0), email_message->body.GetLength()) != email_message->body.GetLength())
        {
            smtp_socket_check.last_error = GetLastError();
            smtp_socket_check.time = time(NULL) + SMTP_SOCKET_CHECK;
            error_message.Format("CControllerApp::SendEmailMessage - error sending body: %d.", smtp_socket_check.last_error);
            logMessage(error_message);
            CloseSmtpSocket();
            return;
        }

        send_data = "\r\n.\r\n";
        if (smtpSocket.Send(send_data.GetBuffer(0), send_data.GetLength()) != send_data.GetLength())
        {
            smtp_socket_check.last_error = GetLastError();
            smtp_socket_check.time = time(NULL) + SMTP_SOCKET_CHECK;
            error_message.Format("CControllerApp::SendEmailMessage - error sending end: %d.", smtp_socket_check.last_error);
            logMessage(error_message);
            CloseSmtpSocket();
            return;
        }

        email_queue.pop();
    }

    if (email_queue.empty())
        CloseSmtpSocket();
#endif
}

void CControllerApp::CheckSmtpSocket()
{
#if 0
??????? move to customer control server

 bool send_email_message = false;
 int file_index = 0;
 CString message;
 EmailMessageFormat email_message = email_queue.front();
 FileEmailRecord email_record;

    while (!send_email_message && fileRead(EMAIL_ADDRESS_LIST, file_index, &email_record, sizeof(email_record)))
    {
        if (email_record.email_flags & email_message.type)
            send_email_message = true;
        else
            file_index++;
    }

    if (!send_email_message)
    {
        email_queue.pop();
        if (email_queue.empty())
            CloseSmtpSocket();
    }
    else
    {
        if (!smtp_socket_check.time)
            SendEmailMessage(&email_message);
        else
        {
            if (smtp_socket_check.time < time(NULL) &&
                memory_location_config.email_smtp_server_port &&
                memory_location_config.email_smtp_server_ip_address)
            {
                if (smtpSocket.m_hSocket == INVALID_SOCKET)
                {
                    if (!smtpSocket.Create(0, SOCK_STREAM, 0, 0))
                    {
                        smtp_socket_check.last_error = GetLastError();
                        message.Format("CControllerApp::CheckSmtpSocket - Create error %d.", smtp_socket_check.last_error);
                        logMessage(message);
                    }
                    else
                        smtp_socket_check.last_error = 0;

                    smtp_socket_check.time = time(NULL) + SMTP_SOCKET_CHECK;
                }
                else
                {
                    if (smtp_socket_check.last_error == WSAEISCONN)
                        memset(&smtp_socket_check, 0, sizeof(smtp_socket_check));
                    else
                    {
                        if (smtp_socket_check.last_error == WSAECONNABORTED ||
                            smtp_socket_check.last_error == WSAECONNRESET ||
                            smtp_socket_check.last_error == WSAEINVAL)
                        {
                            smtpSocket.Close();
                            smtp_socket_check.last_error = 0;
                            smtp_socket_check.time = time(NULL) + SMTP_SOCKET_CHECK;
                        }
                        else
                        {
                            BYTE *email_smtp_server_ip_address = (BYTE*)&memory_location_config.email_smtp_server_ip_address;
                            message.Format("%u.%u.%u.%u", email_smtp_server_ip_address[0], email_smtp_server_ip_address[1],
                                email_smtp_server_ip_address[2], email_smtp_server_ip_address[3]);

                            if (!smtpSocket.Connect(message, memory_location_config.email_smtp_server_port))
                            {
                                smtp_socket_check.last_error = GetLastError();

                                if (smtp_socket_check.last_error != WSAEWOULDBLOCK &&
                                    smtp_socket_check.last_error != WSAEISCONN &&
                                    smtp_socket_check.last_error != 2)
                                {
                                    message.Format("CControllerApp::CheckSmtpSocket - Connect error %d.", smtp_socket_check.last_error);
                                    logMessage(message);
                                }

                                smtp_socket_check.time = time(NULL) + SMTP_SOCKET_CHECK;
                            }
                            else
                                memset(&smtp_socket_check, 0, sizeof(smtp_socket_check));
                        }
                    }
                }
            }
        }
    }
#endif
}

void CControllerApp::turnOnActiveHomeLight (bool turn_on)
{
    if (turn_on)
        pActiveHome->SendAction("sendplc", "a2 on", "on", 0);
    else
        pActiveHome->SendAction("sendplc", "a2 off", "off", 0);
}

void CControllerApp::turnOnActiveHomeChime (bool chime_on)
{
    if (chime_on)
        pActiveHome->SendAction("sendplc", "a1 on", "on", 0);
    else
        pActiveHome->SendAction("sendplc", "a1 off", "off", 0);

}


void CControllerApp::CheckStratusDateTimeSocket()
{
 int bytes_received, bytes_sent, last_error;
 SYSTEMTIME system_time;
 CString error_message;
 StratusTimeDateReturn stratus_time_date_return;
 BOOL time_change_successful = FALSE;

	if (!dateTimeSocket.socket_state)
	{
		bytes_received = dateTimeSocket.GetBuffer((BYTE*)&stratus_time_date_return, sizeof(stratus_time_date_return));

		if (bytes_received == sizeof(stratus_time_date_return))
		{

            endian2(stratus_time_date_return.wYear);
            endian2(stratus_time_date_return.wMonth);
            endian2(stratus_time_date_return.wDayOfWeek);
            endian2(stratus_time_date_return.wDay);
            endian2(stratus_time_date_return.wHour);
            endian2(stratus_time_date_return.wMinute);
            endian2(stratus_time_date_return.wSecond);
            endian2(stratus_time_date_return.wMilliseconds);

			memset(&system_time, 0, sizeof(system_time));

		    GetSystemTime(&system_time);

			system_time.wYear = stratus_time_date_return.wYear;
			system_time.wMonth = stratus_time_date_return.wMonth;
			system_time.wDayOfWeek = stratus_time_date_return.wDayOfWeek;
			system_time.wDay = stratus_time_date_return.wDay;
			system_time.wHour = stratus_time_date_return.wHour;
			system_time.wMinute = stratus_time_date_return.wMinute;
			system_time.wSecond = stratus_time_date_return.wSecond;
			system_time.wMilliseconds = stratus_time_date_return.wMilliseconds;

			// set system time to Stratus time
			time_change_successful = SetLocalTime(&system_time);

			if (!time_change_successful)
				last_error = GetLastError();

			dateTimeSocket.Close();
			dateTimeSocket.socket_state = WSAENOTCONN;
			date_time_socket_check = time(NULL) + DATE_TIME_SOCKET_CHECK;
		}
		else
		{
			bytes_sent = dateTimeSocket.Send("x", 1);

			if (bytes_sent == SOCKET_ERROR)
			{
				last_error = GetLastError();

				if (last_error != WSAEINPROGRESS && last_error != WSAEWOULDBLOCK)
				{
					dateTimeSocket.Close();
					dateTimeSocket.socket_state = WSAENOTCONN;
				}
			}
			else if (bytes_sent != 1)
			{
				dateTimeSocket.Close();
				dateTimeSocket.socket_state = WSAENOTCONN;
			}
		}
	}
	else
	{
		if (dateTimeSocket.m_hSocket == INVALID_SOCKET)
			dateTimeSocket.Create(0, SOCK_STREAM, FD_READ | FD_CLOSE, 0);

		dateTimeSocket.socket_state = WSAENOTCONN;

		if (dateTimeSocket.m_hSocket != INVALID_SOCKET)
		{
			if (dateTimeSocket.Connect(STRATUS_IP_ADDRESS, STRATUS_DATE_TIME_PORT))
				dateTimeSocket.socket_state = 0;
			else
			{
				last_error = GetLastError();

				if (last_error == WSAEISCONN)
					dateTimeSocket.socket_state = 0;
				else if (last_error == WSAEINPROGRESS || last_error == WSAEWOULDBLOCK)
					dateTimeSocket.socket_state = last_error;
				else
				{
					dateTimeSocket.Close();
					dateTimeSocket.socket_state = WSAENOTCONN;
				}
                // if we can't connect, try again in another hour.
			    date_time_socket_check = time(NULL) + DATE_TIME_SOCKET_CHECK;
			}
		}
	}
}

void CControllerApp::CheckPrinterJobs()
{
 int bytes_sent, last_error;
 BYTE buffer_dump[200];
 DWORD bytes_written, printer_bytes_left, bytes_written_second_attempt, print_job_length;
 CString print_job;
 COMSTAT comm_stats;
 static time_t clear_printer_rx_time;
 static time_t cts_timeout;

	if (clear_printer_rx_time < time(NULL))
	{
		if (hMarkerPrinterSerialPort != INVALID_HANDLE_VALUE)
			ReadFile(hMarkerPrinterSerialPort, &buffer_dump, 200, &printer_bytes_left, NULL);

		clear_printer_rx_time = time(NULL) + 30;
	}

    if (!game_lock_printer_queue.empty() && !game_lock_printer_time_check && pGameLockPrinterSocket)
    {
        print_job = game_lock_printer_queue.front();

		bytes_sent = pGameLockPrinterSocket->Send(print_job.GetBuffer(0), print_job.GetLength());

		if (bytes_sent == SOCKET_ERROR)
		{
			last_error = GetLastError();

			if (last_error != WSAEINPROGRESS && last_error != WSAEWOULDBLOCK)
			{
				game_lock_printer_time_check = time(NULL);
				pGameLockPrinterSocket->Close();
			}
			else
				game_lock_printer_queue.pop();
		}
	}

    if (!marker_printer_queue.empty())
    {
        if (pMarkerPrinterSocket)
        {
            if (!marker_printer_time_check)
            {
                print_job = marker_printer_queue.front();
				bytes_sent = pMarkerPrinterSocket->Send(print_job.GetBuffer(0), print_job.GetLength());

				if (bytes_sent == SOCKET_ERROR)
				{
					last_error = GetLastError();

					if (last_error != WSAEINPROGRESS && last_error != WSAEWOULDBLOCK)
					{
						marker_printer_time_check = time(NULL);
						pMarkerPrinterSocket->Close();
					}
				}
				else
					marker_printer_queue.pop();
            }
        }
        else if (hMarkerPrinterSerialPort != INVALID_HANDLE_VALUE)
		{
			print_job = marker_printer_queue.front();
			if (WriteFile(hMarkerPrinterSerialPort, print_job.GetBuffer(0), print_job.GetLength(), &bytes_written, NULL) &&
				bytes_written == print_job.GetLength())
					marker_printer_queue.pop();
		}
		else if (!memory_settings_ini.marker_printer_port)
			marker_printer_queue.pop();
	}

    if (hPrinterSerialPort == INVALID_HANDLE_VALUE)
    {
        if (!memory_settings_ini.printer_comm_port)
        {
            if (!general_printer_queue.empty())
                general_printer_queue.pop();

            if (!w2g_printer_queue.empty())
                w2g_printer_queue.pop();

            if (!printer_interrupt_queue.empty())
                printer_interrupt_queue.pop();
        }
    }
    else
    {
        // priority w2g printout blobs 
        if (w2g_printer_queue.empty())
        {
            if (!general_printer_queue.empty())
		    {
			    if (cts_timeout < time(NULL) && ClearCommError(hPrinterSerialPort, &printer_bytes_left, &comm_stats))
			    {
				    if (comm_stats.cbInQue > 100)
				    {
					    ReadFile(hPrinterSerialPort, &buffer_dump, 200, &printer_bytes_left, NULL);
					    cts_timeout = time(NULL);
				    }
				    else
				    {
					    print_job = general_printer_queue.front();
                        print_job_length = print_job.GetLength();

					    if (WriteFile(hPrinterSerialPort, print_job.GetBuffer(0), print_job_length, &bytes_written, NULL) &&
						    bytes_written == print_job_length)
							    general_printer_queue.pop();
					    else
                        {
						    cts_timeout = time(NULL) + 5;
                        }
				    }
			    }
            }
            else
            {
                if (!printer_busy)
                {
                    while (!printer_interrupt_queue.empty())
                    {
                        print_job = printer_interrupt_queue.front();
                        printer_interrupt_queue.pop();
                        PrinterHeader(0);
                        PrinterData(print_job.GetBuffer(0));
                    }
                }
            }
        }
    }
}

void CControllerApp::CheckW2gPrinterJobs()
{
 int bytes_sent, last_error;
 BYTE buffer_dump[200];
 DWORD bytes_written, printer_bytes_left, bytes_written_second_attempt, print_job_length;
 CString print_job;
 COMSTAT comm_stats;
 static time_t clear_printer_rx_time;
 static time_t cts_timeout;


    if (!w2g_printer_queue.empty())
	{
		if (cts_timeout < time(NULL) && ClearCommError(hPrinterSerialPort, &printer_bytes_left, &comm_stats))
		{
			if (comm_stats.cbInQue > 100)
			{
				ReadFile(hPrinterSerialPort, &buffer_dump, 200, &printer_bytes_left, NULL);
				cts_timeout = time(NULL);
			}
			else
			{
				print_job = w2g_printer_queue.front();
                print_job_length = print_job.GetLength();

				if (WriteFile(hPrinterSerialPort, print_job.GetBuffer(0), print_job_length, &bytes_written, NULL) &&
					bytes_written == print_job_length)
						w2g_printer_queue.pop();
				else
                {
					cts_timeout = time(NULL) + 5;
                }
			}
		}
    }
}


void CControllerApp::OpenMarkerPrinterPort()
{
 CString comm_port_name;
 COMMTIMEOUTS comm_timeouts;
 DCB comm_control_settings;

    if (memory_settings_ini.marker_printer_ip_address)
    {
        pMarkerPrinterSocket = new CAsyncSocket;
        CheckMarkerPrinterSocket();
    }
    else
    {
        comm_port_name.Format("COM%u:", memory_settings_ini.marker_printer_port);
        hMarkerPrinterSerialPort = CreateFile(comm_port_name, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);

        if (hMarkerPrinterSerialPort != INVALID_HANDLE_VALUE && GetCommState(hMarkerPrinterSerialPort, &comm_control_settings))
        {
            comm_control_settings.BaudRate = CBR_9600;
            comm_control_settings.Parity = EVENPARITY;
            comm_control_settings.ByteSize = 7;
            comm_control_settings.DCBlength = sizeof(comm_control_settings);
            comm_control_settings.fBinary = 1;
            comm_control_settings.fParity = 0;
            comm_control_settings.fOutxCtsFlow = 1;
            comm_control_settings.fOutxDsrFlow = 0;
            comm_control_settings.fDtrControl = DTR_CONTROL_DISABLE;
            comm_control_settings.fDsrSensitivity = 0;
            comm_control_settings.fTXContinueOnXoff = 1;
            comm_control_settings.fOutX = 0;
            comm_control_settings.fInX = 0;
            comm_control_settings.fErrorChar = 0;
            comm_control_settings.fNull = 0;
			comm_control_settings.fRtsControl = RTS_CONTROL_DISABLE;
            comm_control_settings.fAbortOnError = 0;
            comm_control_settings.StopBits = ONESTOPBIT;

            if (SetCommState(hMarkerPrinterSerialPort, &comm_control_settings) && GetCommTimeouts(hMarkerPrinterSerialPort, &comm_timeouts))
            {
                comm_timeouts.ReadIntervalTimeout = 10;
                comm_timeouts.ReadTotalTimeoutMultiplier = 1;
                comm_timeouts.ReadTotalTimeoutConstant = 50;
                comm_timeouts.WriteTotalTimeoutMultiplier = 1;
                comm_timeouts.WriteTotalTimeoutConstant = 50;
                SetCommTimeouts(hMarkerPrinterSerialPort, &comm_timeouts);
            }
            else
                AfxMessageBox("Error setting Marker printer port comm state!");
        }
        else
            AfxMessageBox("Error opening Marker printer port!");
    }
}

void CControllerApp::OpenPrinterCommPort()
{
 CString comm_port_name;
 COMMTIMEOUTS comm_timeouts;
 DCB comm_control_settings;

    comm_port_name.Format("COM%u:", memory_settings_ini.printer_comm_port);

    hPrinterSerialPort = CreateFile(comm_port_name, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);

    if (hPrinterSerialPort != INVALID_HANDLE_VALUE && GetCommState(hPrinterSerialPort, &comm_control_settings))
    {
        comm_control_settings.BaudRate = CBR_9600;
        comm_control_settings.Parity = EVENPARITY;
        comm_control_settings.ByteSize = 7;
        comm_control_settings.DCBlength = sizeof(comm_control_settings);
        comm_control_settings.fBinary = 1;
        comm_control_settings.fParity = 0;
        comm_control_settings.fOutxCtsFlow = 1;
        comm_control_settings.fOutxDsrFlow = 0;
        comm_control_settings.fDtrControl = DTR_CONTROL_DISABLE;
        comm_control_settings.fDsrSensitivity = 0;
        comm_control_settings.fTXContinueOnXoff = 1;
        comm_control_settings.fOutX = 0;
        comm_control_settings.fInX = 0;
        comm_control_settings.fErrorChar = 0;
        comm_control_settings.fNull = 0;
		comm_control_settings.fRtsControl = RTS_CONTROL_DISABLE;
        comm_control_settings.fAbortOnError = 0;
        comm_control_settings.StopBits = ONESTOPBIT;

        if (SetCommState(hPrinterSerialPort, &comm_control_settings) && GetCommTimeouts(hPrinterSerialPort, &comm_timeouts))
        {
            comm_timeouts.ReadIntervalTimeout = 10;
            comm_timeouts.ReadTotalTimeoutMultiplier = 1;
            comm_timeouts.ReadTotalTimeoutConstant = 50;
            comm_timeouts.WriteTotalTimeoutMultiplier = 1;
            comm_timeouts.WriteTotalTimeoutConstant = 50;
            SetCommTimeouts(hPrinterSerialPort, &comm_timeouts);
        }
        else
            AfxMessageBox("Error setting printer port comm state!");
    }
    else
        AfxMessageBox("Error opening printer port!");
}

void CControllerApp::VerifyGameRedeemFile()
{
 bool record_found;
 DWORD file_index = 0;
 FileGameRedeemRecord redeem_record;
 CGameDevice *pGameDevice;
 CGameCommControl *pGameCommControl;

    // check redeem file for removed games
    while (fileRead(GAME_REDEEM_RECORDS, file_index, &redeem_record, sizeof(redeem_record)))
    {
        record_found = false;
        pGameCommControl = pHeadGameCommControl;

        while (!record_found && pGameCommControl)
        {
            pGameDevice = pGameCommControl->pHeadGameDevice;

            while (!record_found && pGameDevice)
            {
                if (redeem_record.ucmc_id == pGameDevice->memory_game_config.ucmc_id)
                {
                    record_found = true;

                    if (!pGameDevice->memory_game_config.customer_id)
                    {
                        pGameDevice->memory_game_config.customer_id = memory_location_config.customer_id;
                        pGameDevice->WriteModifiedGameConfig();
                    }

                    if (redeem_record.customer_id != pGameDevice->memory_game_config.customer_id ||
                        redeem_record.port_number != pGameDevice->memory_game_config.port_number)
                    {
                        redeem_record.customer_id = pGameDevice->memory_game_config.customer_id;
                        redeem_record.port_number = pGameDevice->memory_game_config.port_number;
                        writeRedundantFile((CString)GAME_REDEEM_RECORDS, file_index, (BYTE*)&redeem_record, sizeof(redeem_record));
                    }
                }

                pGameDevice = pGameDevice->pNextGame;
            }

            pGameCommControl = pGameCommControl->pNextGameCommControl;
        }

        if (!record_found)
        {
            fileDeleteRecord(GAME_REDEEM_RECORDS, sizeof(redeem_record), file_index);
            fileDeleteRecord(GAME_REDEEM_RECORDS_REDUNDANT, sizeof(redeem_record), file_index);
        }
        else
            file_index++;
    }

    pGameCommControl = pHeadGameCommControl;

    // check for all games in the redeem file
    while (pGameCommControl)
    {
        pGameDevice = pGameCommControl->pHeadGameDevice;

        while (pGameDevice)
        {
            if (fileGetRecord(GAME_REDEEM_RECORDS, &redeem_record, sizeof(redeem_record), pGameDevice->memory_game_config.ucmc_id) < 0)
            {
                if (!pGameDevice->memory_game_config.customer_id)
                {
                    pGameDevice->memory_game_config.customer_id = memory_location_config.customer_id;
                    pGameDevice->WriteModifiedGameConfig();
                }

                memset(&redeem_record, 0, sizeof(redeem_record));
                redeem_record.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
                redeem_record.customer_id = pGameDevice->memory_game_config.customer_id;
                redeem_record.port_number = pGameDevice->memory_game_config.port_number;
                writeRedundantFile((CString)GAME_REDEEM_RECORDS, -1, (BYTE*)&redeem_record, sizeof(redeem_record));
            }

            pGameDevice = pGameDevice->pNextGame;
        }

        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }
}

void CControllerApp::CreateDirectoryStructure()
{
 CString error_message = "FATAL ERROR!\nUNABLE TO CREATE PATH:\n";

    if (GetFileAttributes(FILE_PATH) == 0xFFFFFFFF && !CreateDirectory(FILE_PATH, NULL))
    {
        error_message += FILE_PATH;
        AfxMessageBox(error_message);
        return;
    }

    if (GetFileAttributes(CONTROLLER_LOG_PATH) == 0xFFFFFFFF && !CreateDirectory(CONTROLLER_LOG_PATH, NULL))
    {
        error_message += CONTROLLER_LOG_PATH;
        AfxMessageBox(error_message);
        return;
    }

    if (GetFileAttributes(EVENTS_PATH) == 0xFFFFFFFF && !CreateDirectory(EVENTS_PATH, NULL))
    {
        error_message += EVENTS_PATH;
        AfxMessageBox(error_message);
        return;
    }

    if (GetFileAttributes(PRINTER_TEXT_PATH) == 0xFFFFFFFF && !CreateDirectory(PRINTER_TEXT_PATH, NULL))
    {
        error_message += PRINTER_TEXT_PATH;
        AfxMessageBox(error_message);
        return;
    }

    if (GetFileAttributes(DROP_REPORTS_PATH) == 0xFFFFFFFF && !CreateDirectory(DROP_REPORTS_PATH, NULL))
    {
        error_message += DROP_REPORTS_PATH;
        AfxMessageBox(error_message);
        return;
    }

    if (GetFileAttributes(GAME_DATA_PATH) == 0xFFFFFFFF && !CreateDirectory(GAME_DATA_PATH, NULL))
    {
        error_message += GAME_DATA_PATH;
        AfxMessageBox(error_message);
        return;
    }

    if (GetFileAttributes(GAME_LOCKS_PATH) == 0xFFFFFFFF && !CreateDirectory(GAME_LOCKS_PATH, NULL))
    {
        error_message += GAME_LOCKS_PATH;
        AfxMessageBox(error_message);
        return;
    }

    if (GetFileAttributes(CASHIER_SHIFT_PATH) == 0xFFFFFFFF && !CreateDirectory(CASHIER_SHIFT_PATH, NULL))
    {
        error_message += CASHIER_SHIFT_PATH;
        AfxMessageBox(error_message);
        return;
    }

    if (GetFileAttributes(GB_PRO_PATH) == 0xFFFFFFFF && !CreateDirectory(GB_PRO_PATH, NULL))
    {
        error_message += GB_PRO_PATH;
        AfxMessageBox(error_message);
        return;
    }

    if (GetFileAttributes(STORED_METERS_PATH) == 0xFFFFFFFF && !CreateDirectory(STORED_METERS_PATH, NULL))
    {
        error_message += STORED_METERS_PATH;
        AfxMessageBox(error_message);
        return;
    }

    if (GetFileAttributes(QUICK_ENROLL_PATH) == 0xFFFFFFFF && !CreateDirectory(QUICK_ENROLL_PATH, NULL))
    {
        error_message += QUICK_ENROLL_PATH;
        AfxMessageBox(error_message);
        return;
    }

}

void CControllerApp::CheckForGameUnlocks()
{
 DWORD cash_payment;
 CString string_buffer, search_string, filename;
 GameUnlockQueue unlock_message;
 HANDLE hSearch;
 WIN32_FIND_DATA find_return;
 CGameDevice *pGameDevice;
 CGameCommControl *pGameCommControl = 0; 
 TxSerialMessage message;

    if (!user_interface_game_unlock_queue.empty())
    {
        unlock_message = user_interface_game_unlock_queue.front();
        user_interface_game_unlock_queue.pop();

        if (!unlock_message.manager_override)
        {
            filename.Format("%s\\%u-%u-%u", GAME_LOCKS_PATH, unlock_message.ucmc_id,
                unlock_message.ticket_amount, unlock_message.ticket_id);
			DeleteFile(filename);
        }
        else
        {
            // normal game locks
            search_string.Format("%s\\%u-*", GAME_LOCKS_PATH, unlock_message.ucmc_id);
            hSearch = FindFirstFile(search_string, &find_return);

            if (hSearch != INVALID_HANDLE_VALUE)
            {
                filename.Format("%s\\%s", GAME_LOCKS_PATH, find_return.cFileName);
                if (!DeleteFile(filename))
                {
                    string_buffer.Format("CControllerApp::CheckForGameUnlocks - (manager override)unable to delete file: %s", filename);
                    logMessage(string_buffer);
                }

                while (FindNextFile(hSearch, &find_return))
                {
                    filename.Format("%s\\%s", GAME_LOCKS_PATH, find_return.cFileName);
                    if (!DeleteFile(filename))
                    {
                        string_buffer.Format("CControllerApp::CheckForGameUnlocks - (manager override)unable to delete file: %s", filename);
                        logMessage(string_buffer);
                    }
                }

                FindClose(hSearch);
            }
        }

        pGameDevice = GetGamePointer(unlock_message.ucmc_id);
        if (pGameDevice)
        {
            if (pGameDevice->memory_game_config.block_remote_game_unlocks)
            {
                if (unlock_message.manager_override)
                {
                    string_buffer.Format("\nCANNOT UNLOCK GAME,\nREMOTE RESET BLOCKED\n GAME #:   %u(%u-%u)\n\n",
                        pGameDevice->memory_game_config.ucmc_id, pGameDevice->memory_game_config.port_number,
                        pGameDevice->memory_game_config.mux_id);
                    printer_interrupt_queue.push(string_buffer);
                }
            }
            else
            {
                if (unlock_message.manager_override)
                {
                    string_buffer.Format("\nUNLOCK GAME OVER-RIDE\n GAME #:   %u(%u-%u)\n\n",
                        pGameDevice->memory_game_config.ucmc_id, pGameDevice->memory_game_config.port_number,
                        pGameDevice->memory_game_config.mux_id);
                    printer_interrupt_queue.push(string_buffer);
                }
                else
                {
                    search_string.Format(" UNLOCK GAME\nGAME #: %u(%u-%u)\n", pGameDevice->memory_game_config.ucmc_id,
                        pGameDevice->memory_game_config.port_number, pGameDevice->memory_game_config.mux_id);

                    if (unlock_message.jackpot_to_credit_flag)
                    {
                        string_buffer.Format("PAY-TO-CREDIT AMOUNT: %7.2f\n", (double)unlock_message.ticket_amount / 100);
                        search_string += string_buffer;
                    }
                    else
                    {
                        string_buffer.Format("TICKET AMOUNT: %7.2f\n", (double)unlock_message.ticket_amount / 100);
                        search_string += string_buffer;

                        if (unlock_message.marker_amount)
                        {
                            if (unlock_message.marker_amount >= unlock_message.ticket_amount)
                                cash_payment = 0;
                            else
                                cash_payment = unlock_message.ticket_amount - unlock_message.marker_amount;

                            string_buffer.Format("MARKER AMOUNT: %7.2f\n", (double)unlock_message.marker_amount / 100);
                            search_string += string_buffer;

                            if (!cash_payment)
                                string_buffer.Format("MARKER PAYMENT: %7.2f\n", (double)unlock_message.ticket_amount / 100);
                            else
                                string_buffer.Format("MARKER PAYMENT: %7.2f\n", (double)unlock_message.marker_amount / 100);

                            search_string += string_buffer;
                            string_buffer.Format("CASH PAYMENT: %7.2f\n\n", (double)cash_payment / 100);
                            search_string += string_buffer;
                        }
                    }

                    printer_interrupt_queue.push(search_string);
                }

                pGameCommControl = GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);
                if (pGameCommControl && pGameCommControl->sas_com)
                {
                    pGameCommControl->sas_com->queueHandpayReset();
                    pGameCommControl->sas_com->queueRequestMeterData();
                }
                else
                {
                    if (pGameDevice->memory_game_config.mux_id)
                    {
                        memset(&message, 0, sizeof(message));
                        message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                        message.msg.length = 6;
                        message.msg.command = UNLOCK_GAME;
                        pGameDevice->transmit_queue.push(message);

                        if (pGameDevice->memory_game_config.version != GB_INTERFACE_VERSION)
                        {
                            memset(&message, 0, sizeof(message));
                            message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                            message.msg.length = 7;
                            message.msg.command = COMMAND_HEADER;
                            message.msg.data[0] = SEND_CURRENT_METERS;
                            pGameDevice->transmit_queue.push(message);
                        }
                    }
                    // send the clear ticket message to the game if controller is operating in standalone mode
                	if (!(memory_settings_ini.stratus_ip_address && memory_settings_ini.stratus_tcp_port))
                    {
                        memset(&message, 0, sizeof(message));
                        message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                        message.msg.length = 10;
                        message.msg.command = CLEAR_TICKET_FROM_GAME;
                        memcpy(message.msg.data, &unlock_message.ticket_id, 4);
                        pGameDevice->transmit_queue.push(message);


                    }

                }



            }
        }
    }
}

void CControllerApp::CheckDialogBoxes()
{
    if (current_dialog_return != 0)
    {
        if (pCurrentDialog)
        {
            pCurrentDialog->DestroyWindow();
            delete pCurrentDialog;
            pCurrentDialog = NULL;
        }

        switch (current_dialog_return)
        {
            case IDD_LOGON_DIALOG:
                VerifyGameRedeemFile();
                pCurrentDialog = new CControllerDlg;
                break;

            case IDD_MAIN_MENU_DIALOG:
                if ((current_user.user.id / 10000) <= ENGINEERING)
                    pCurrentDialog = new CMainMenuDlg;
                break;

            case IDD_CASHIER_MENU_DIALOG:
                switch (memory_dispenser_config.dispenser_type)
                {
                    case DISPENSER_TYPE_ECASH:
                    case DISPENSER_TYPE_DIEBOLD:
                    case DISPENSER_TYPE_CD2000:
                    case DISPENSER_TYPE_LCDM2000_20_100:
                    case DISPENSER_TYPE_LCDM4000_20_100:
                        if (!dispenserControl.memory_dispenser_values.impress_amount1 &&
                            !dispenserControl.memory_dispenser_values.impress_amount2)
                        {
                            MessageWindow(false, "IMPRESS NOT SET", "COLLECTOR NEEDS TO SET\nDISPENSER IMPRESS");
                            current_dialog_return = 0;

                            while (theApp.message_window_queue.empty())
                                OnIdle(0);

                            current_dialog_return = IDD_LOGON_DIALOG;
                            return;
                        }
                        break;

                    default:
                        if (!dispenserControl.memory_dispenser_values.impress_amount1)
                        {
                            MessageWindow(false, "IMPRESS NOT SET", "COLLECTOR NEEDS TO SET\nDISPENSER IMPRESS");
                            current_dialog_return = 0;

                            while (theApp.message_window_queue.empty())
                                OnIdle(0);

                            current_dialog_return = IDD_LOGON_DIALOG;
                            return;
                        }
                        break;
                };

                pCurrentDialog = new CMenuCashier;
                break;

            case IDD_MANAGER_MENU_DIALOG:
                pCurrentDialog = new CMenuManager;
                break;

            case IDD_COLLECTOR_MENU_DIALOG:
                pCurrentDialog = new CMenuCollector;
                break;

            case IDD_TECHNICIAN_MENU_DIALOG:
                pCurrentDialog = new CMenuTechnician;
                break;

            case IDD_ENGINEER_MENU_DIALOG:
                pCurrentDialog = new CMenuEngineer;
                break;

            case 0xFFFFFFFF:
            default:
                ProcessExitProgramRequest(true);
                break;
        }

        if (pCurrentDialog && pCurrentDialog->Create(current_dialog_return))
            current_dialog_return = 0;
        else
            current_dialog_return = 0xFFFFFFFF;
    }
    else
    {
        if (current_user.autologout_timeout &&
             (current_user.time_setting + current_user.autologout_timeout < time(NULL)) &&
             !fetching_netwin_in_progress && !theApp.printer_busy && !theApp.fill_in_progress)
//             !fetching_netwin_in_progress)

        {
            if (pCurrentDialog)
            {
                pCurrentDialog->DestroyWindow();
                delete pCurrentDialog;
                pCurrentDialog = NULL;
            }

            current_dialog_return = IDD_LOGON_DIALOG;
            printer_busy = false;
        }
    }
}

void CControllerApp::RemoveGameCommControl(CGameCommControl *pGameCommControl)
{
	if (pGameCommControl)
	{
        if (pHeadGameCommControl && (pHeadGameCommControl == pGameCommControl))
            pHeadGameCommControl = pHeadGameCommControl->pNextGameCommControl;
        else if(pGameCommControl->pNextGameCommControl)
        {
            pGameCommControl->pNextGameCommControl->pPreviousGameCommControl = pGameCommControl->pPreviousGameCommControl;
            if (pGameCommControl->pPreviousGameCommControl)
                pGameCommControl->pPreviousGameCommControl->pNextGameCommControl = pGameCommControl->pNextGameCommControl;
        }
        else if (pGameCommControl->pPreviousGameCommControl)
        {
            pGameCommControl->pPreviousGameCommControl->pNextGameCommControl = NULL;
        }
            

		pGameCommControl->stop_comm_thread = true;
		Sleep(1000);

        if (pGameCommControl->sas_com)
        {
    		pGameCommControl->sas_com->stop_comm_thread = TRUE;
			while (pGameCommControl->sas_com->serial_recieve_task_active)
	    		Sleep(1000);
        }


		delete pGameCommControl;
        pGameCommControl = NULL;
	}
}

void CControllerApp::CashierShiftStart(DWORD slave_login_id)
{
 CString prt_buf, filename;
 FileCashierShift current_cashier_shift;
 CDrawerStartShift *pDrawerStartShift;
 DrinkCompTotal drink_comp_total;
 EventMessage event_message;


    theApp.GetNetwinReport(false, theApp.memory_location_config.customer_id, true);
    theApp.GetNetwinReport(false, theApp.memory_location_config.customer_id2, false);
    theApp.GetNetwinReport(false, theApp.memory_location_config.customer_id3, false);

    if (!fileRead(DRINK_COMPS_SHIFT, 0, &drink_comp_total, sizeof(drink_comp_total)))
        memset(&drink_comp_total, 0, sizeof(drink_comp_total));

    if (!fileRead(CASHIER_SHIFT_1, 0, &current_cashier_shift, sizeof(current_cashier_shift)))
        memset(&current_cashier_shift, 0, sizeof(current_cashier_shift));

    current_cashier_shift.previous_total_drop = getTotalNetwinDrop();
    current_cashier_shift.previous_game_handle = getTotalNetwinHandle();

    current_cashier_shift.previous_drink_comps.number = drink_comp_total.number;
    current_cashier_shift.previous_drink_comps.total_amount = drink_comp_total.total_amount;

    current_cashier_shift.drink_comps.number = 0;
    current_cashier_shift.drink_comps.total_amount = 0;

    if (!current_cashier_shift.cashier_id || !current_cashier_shift.start_time)
    {
        if (slave_login_id)
            current_cashier_shift.cashier_id = slave_login_id;
        else
            current_cashier_shift.cashier_id = current_user.user.id;
        current_cashier_shift.start_time = time(NULL);

        current_cashier_shift.end_time = 0;

        printer_busy = true;
        PrinterData("\n");
        PrinterHeader(0);
        PrinterData(" SHIFT STARTED\n");
        PrinterId();

        prt_buf.Format(" Breakage: $%9.2f\n\n", (double)(dispenserControl.memory_dispenser_values.dispense_grand_total -
			dispenserControl.memory_dispenser_values.tickets_amount) / 100);
        PrinterData(prt_buf.GetBuffer(0));
        printer_busy = false;

        // if we are offline when we start our shift then print the disclaimer
        if (stratusSocket.socket_state)
            current_cashier_shift.print_offline_disclaimer = TRUE;    

        fileWrite(CASHIER_SHIFT_1, 0, &current_cashier_shift, sizeof(current_cashier_shift));
        DeleteFile(DRINK_COMPS_SHIFT);
    }

    memset(&event_message, 0, sizeof(event_message));
    event_message.head.time_stamp = time(NULL);
    event_message.head.event_type = SHIFT_START;
    event_message.head.data_length = 4;
    memcpy(event_message.data, &current_cashier_shift.cashier_id, 4);
    theApp.event_message_queue.push(event_message);
}

void CControllerApp::purgeDatedPaidTicketsFile (void)
{
    int file_index;
    FilePaidTicket file_paid_ticket;
    time_t  current_timestamp = time(NULL);
    

    file_index = 0;
    while (fileRead(DATED_PAID_TICKETS, file_index, &file_paid_ticket, sizeof(file_paid_ticket)))
    {
        if ((current_timestamp - file_paid_ticket.memory.timestamp) >= BIMONTHLY_SECS) 
        {
            fileDeleteRecord(DATED_PAID_TICKETS, sizeof(file_paid_ticket), file_index);
        }
        else
            file_index++;
    }
}

// set the dispenser malfunction flags in the last paid tickets and drop tickets records
void CControllerApp::dispenserMalfunctionWithLastPaidTicket (void)
{
    DWORD total_records;
    FilePaidTicket file_paid_ticket;
    FileDropTicket file_drop_ticket;

    total_records = fileRecordTotal(PAID_TICKETS, sizeof(file_paid_ticket));

    if (total_records)
    {
        if (fileRead(PAID_TICKETS, (total_records - 1), &file_paid_ticket, sizeof(file_paid_ticket)))
        {
            file_paid_ticket.dispenser_malfunction = TRUE;
            writeRedundantFile((CString)PAID_TICKETS, (total_records - 1), (BYTE*)&file_paid_ticket, sizeof(file_paid_ticket));
        }
    }

    total_records = fileRecordTotal(DROP_TICKETS, sizeof(file_drop_ticket));

    if (total_records)
    {
        if (fileRead(DROP_TICKETS, (total_records - 1), &file_drop_ticket, sizeof(file_drop_ticket)))
        {
            file_drop_ticket.dispenser_malfunction = TRUE;
            writeRedundantFile((CString)DROP_TICKETS, (total_records - 1), (BYTE*)&file_drop_ticket, sizeof(file_drop_ticket));
        }
    }

}

BYTE CControllerApp::CalculateMessageCrc(BYTE *buffer, BYTE size)
{
 BYTE iii, crc = 0;

    for (iii=0; iii < size; iii++)
        crc = (crc < 0X80) ? (crc << 1) + buffer[iii] : (crc << 1) + buffer[iii] + 1;

 return crc;
}

void CControllerApp::ProcessExitProgramRequest(bool reboot)
{
 CGameCommControl *pGameCommControl = pHeadGameCommControl, *pNextGameCommControl;
 DispenserCommand dispenser_message;
 CString filename, printer_data;
 SYSTEMTIME time_struct;

    exit_application = true;

    Sleep(100);

    while (pGameCommControl)
    {
        pNextGameCommControl = pGameCommControl->pNextGameCommControl;
		RemoveGameCommControl(pGameCommControl);
        pGameCommControl = pNextGameCommControl;
    }

    if (hPrinterSerialPort != INVALID_HANDLE_VALUE)
    {
        if (CloseHandle(hPrinterSerialPort))
            hPrinterSerialPort = INVALID_HANDLE_VALUE;
        else
        {
            hPrinterSerialPort = INVALID_HANDLE_VALUE;
            logMessage("CControllerApp::ProcessExitProgramRequest - problem closing printer port.");
        }
    }

    if (hMarkerPrinterSerialPort != INVALID_HANDLE_VALUE)
    {
        CloseHandle(hMarkerPrinterSerialPort);
        hMarkerPrinterSerialPort = INVALID_HANDLE_VALUE;
    }

	customer_control_listening_time_check		= 0;
	multi_controller_server_time_check			= 0;
	customer_control_udp_time_check				= 0;
	marker_printer_time_check					= 0;
	game_lock_printer_time_check				= 0;
	date_time_socket_check						= 0;
	date_time_server_send_check				    = 0;
	date_time_server_receive_check		        = 0;
	dateTimeSocket.socket_state					= 0;
	stratusSocket.socket_state					= 0;
	customerControlServerSocket.socket_state	= 0;

    if (pGameLockPrinterSocket)
    {
        if (pGameLockPrinterSocket->m_hSocket != INVALID_SOCKET)
            pGameLockPrinterSocket->Close();

        delete pGameLockPrinterSocket;
    }

    if (pMarkerPrinterSocket)
    {
        if (pMarkerPrinterSocket->m_hSocket != INVALID_SOCKET)
            pMarkerPrinterSocket->Close();

        delete pMarkerPrinterSocket;
    }

    if (pMultiControllerServer)
    {
        if (pMultiControllerServer->m_hSocket != INVALID_SOCKET)
            pMultiControllerServer->Close();

        delete pMultiControllerServer;
    }

#if 0
??????? move to customer control server
    if (smtpSocket.m_hSocket != INVALID_SOCKET)
        smtpSocket.Close();
#endif

    if (dateTimeSocket.m_hSocket != INVALID_SOCKET)
        dateTimeSocket.Close();

    if (customerControlListen.m_hSocket != INVALID_SOCKET)
        customerControlListen.Close();

    if (stratusSocket.m_hSocket != INVALID_SOCKET)
        stratusSocket.Close();

    if (customerControlServerSocket.m_hSocket != INVALID_SOCKET)
        customerControlServerSocket.Close();

    if (dateTimeSocket.m_hSocket != INVALID_SOCKET)
        dateTimeSocket.Close();

    if (customerControlUdpSocket.m_hSocket != INVALID_SOCKET)
        customerControlUdpSocket.Close();

    if (pGbProManager)
    {
        delete pGbProManager;
		pGbProManager = NULL;
    }

    // save dispenser queue
    while (!dispenser_command_queue.empty())
    {
        dispenser_message = dispenser_command_queue.front();
        dispenser_command_queue.pop();
        if (!fileWrite(DISPENSER_COMMANDS, -1, &dispenser_message, sizeof(dispenser_message)))
            logMessage("CControllerApp::ProcessExitProgramRequest - error writing dispenser commands on shutdown.");
    }

    while (!dispenser_ui_queue.empty())
        dispenser_ui_queue.pop();

    while (!dispenser_display_data_queue.empty())
        dispenser_display_data_queue.pop();

    while (!game_data_display_queue.empty())
        game_data_display_queue.pop();

    while (!stratus_tx_queue.empty())
        stratus_tx_queue.pop();

    while (!stratus_dialog_queue.empty())
        stratus_dialog_queue.pop();

    while (!email_queue.empty())
        email_queue.pop();

//?????work    while (!customer_control_server_tx_queue.empty())
//?????work        customer_control_server_tx_queue.pop();

    if (!general_printer_queue.empty() || !printer_interrupt_queue.empty() || !game_lock_printer_queue.empty() ||
        !marker_printer_queue.empty())
    {
        GetLocalTime(&time_struct);
        filename.Format("%s\\%d_%d_%d_printer.rtf", PRINTER_TEXT_PATH, time_struct.wYear, time_struct.wMonth, time_struct.wDay);
        fileWrite(filename.GetBuffer(0), -1, "*****LOST PRINTER DATA START*****\n\n", (DWORD)strlen("*****LOST PRINTER DATA START*****\n\n"));

        while (!general_printer_queue.empty())
        {
            printer_data = general_printer_queue.front();
            general_printer_queue.pop();
            fileWrite(filename.GetBuffer(0), -1, printer_data.GetBuffer(0), printer_data.GetLength());
        }

        while (!printer_interrupt_queue.empty())
        {
            printer_data = printer_interrupt_queue.front();
            printer_interrupt_queue.pop();
            fileWrite(filename.GetBuffer(0), -1, printer_data.GetBuffer(0), printer_data.GetLength());
        }

        while (!game_lock_printer_queue.empty())
        {
            printer_data = game_lock_printer_queue.front();
            game_lock_printer_queue.pop();
            fileWrite(filename.GetBuffer(0), -1, printer_data.GetBuffer(0), printer_data.GetLength());
        }

        while (!marker_printer_queue.empty())
        {
            printer_data = marker_printer_queue.front();
            marker_printer_queue.pop();
            fileWrite(filename.GetBuffer(0), -1, printer_data.GetBuffer(0), printer_data.GetLength());
        }

        fileWrite(filename.GetBuffer(0), -1, "*****LOST PRINTER DATA END*****\n\n", (DWORD)strlen("*****LOST PRINTER DATA END*****\n\n"));
    }

    m_pMainWnd = NULL;

	if (reboot)
	{
		SystemShutdown();
		printer_data.Format("PROGRAM SYSTEM REBOOT: v%u.%s", VERSION, SUBVERSION);
	}
	else
    {
		printer_data.Format("PROGRAM RESTART: v%u.%s", VERSION, SUBVERSION);
    	ShellExecute(NULL, "open", "controller.bat", NULL, NULL, SW_SHOWMAXIMIZED);
    }

	logMessage(printer_data);
    exit(0);
}

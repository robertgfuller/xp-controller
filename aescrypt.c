#include "aesopt.h"
#include "aestab.h"

#if defined(__cplusplus)
extern "C"
{
#endif

#define si(y,x,k,c) (y[c] = word_in(x, c) ^ (k)[c])
#define so(y,x,c)   word_out(y, c, x[c])

#define l_copy(y, x)    y[0] = x[0]; y[1] = x[1]; \
                        y[2] = x[2]; y[3] = x[3];
#define state_in(y,x,k) si(y,x,k,0); si(y,x,k,1); si(y,x,k,2); si(y,x,k,3)
#define state_out(y,x)  so(y,x,0); so(y,x,1); so(y,x,2); so(y,x,3)
#define round(rm,y,x,k) rm(y,x,k,0); rm(y,x,k,1); rm(y,x,k,2); rm(y,x,k,3)


/* Visual C++ .Net v7.1 provides the fastest encryption code when using
   Pentium optimiation with small code but this is poor for decryption
   so we need to control this with the following VC++ pragmas
*/

#if defined(_MSC_VER)
#pragma optimize( "s", on )
#endif

/* Given the column (c) of the output state variable, the following
   macros give the input state variables which are needed in its
   computation for each row (r) of the state. All the alternative
   macros give the same end values but expand into different ways
   of calculating these values.  In particular the complex macro
   used for dynamically variable block sizes is designed to expand
   to a compile time constant whenever possible but will expand to
   conditional clauses on some branches (I am grateful to Frank
   Yellin for this construction)
*/

#define fwd_var(x,r,c)\
 ( r == 0 ? ( c == 0 ? x[0] : c == 1 ? x[1] : c == 2 ? x[2] : x[3])\
 : r == 1 ? ( c == 0 ? x[1] : c == 1 ? x[2] : c == 2 ? x[3] : x[0])\
 : r == 2 ? ( c == 0 ? x[2] : c == 1 ? x[3] : c == 2 ? x[0] : x[1])\
 :          ( c == 0 ? x[3] : c == 1 ? x[0] : c == 2 ? x[1] : x[2]))

#define fwd_rnd(y,x,k,c)    (y[c] = (k)[c] ^ four_tables(x,t_use(f,n),fwd_var,rf1,c))
#define fwd_lrnd(y,x,k,c)   (y[c] = (k)[c] ^ four_tables(x,t_use(f,l),fwd_var,rf1,c))

void MachineEncryptData(const unsigned char *in_blk, unsigned char *out_blk)
{
 aes_32t b0[4], b1[4];
 const aes_32t *kp = machine_encryption_key_schedule;

	state_in(b0, in_blk, kp);

	round(fwd_rnd,  b1, b0, kp + 4);
	round(fwd_rnd,  b0, b1, kp + 8);
	round(fwd_rnd,  b1, b0, kp + 12);
	round(fwd_rnd,  b0, b1, kp + 16);
	round(fwd_rnd,  b1, b0, kp + 20);
	round(fwd_rnd,  b0, b1, kp + 24);
	round(fwd_rnd,  b1, b0, kp + 28);
	round(fwd_rnd,  b0, b1, kp + 32);
	round(fwd_rnd,  b1, b0, kp + 36);
	round(fwd_lrnd, b0, b1, kp + 40);

	state_out(out_blk, b0);
}

void StratusEncryptData(const unsigned char *in_blk, unsigned char *out_blk)
{
 aes_32t b0[4], b1[4];
 const aes_32t *kp = stratus_encryption_key_schedule;

	state_in(b0, in_blk, kp);

	round(fwd_rnd,  b1, b0, kp + 4);
	round(fwd_rnd,  b0, b1, kp + 8);
	round(fwd_rnd,  b1, b0, kp + 12);
	round(fwd_rnd,  b0, b1, kp + 16);
	round(fwd_rnd,  b1, b0, kp + 20);
	round(fwd_rnd,  b0, b1, kp + 24);
	round(fwd_rnd,  b1, b0, kp + 28);
	round(fwd_rnd,  b0, b1, kp + 32);
	round(fwd_rnd,  b1, b0, kp + 36);
	round(fwd_lrnd, b0, b1, kp + 40);

	state_out(out_blk, b0);
}


/* Visual C++ .Net v7.1 provides the fastest encryption code when using
   Pentium optimiation with small code but this is poor for decryption
   so we need to control this with the following VC++ pragmas
*/

#if defined(_MSC_VER)
#pragma optimize( "t", on )
#endif

/* Given the column (c) of the output state variable, the following
   macros give the input state variables which are needed in its
   computation for each row (r) of the state. All the alternative
   macros give the same end values but expand into different ways
   of calculating these values.  In particular the complex macro
   used for dynamically variable block sizes is designed to expand
   to a compile time constant whenever possible but will expand to
   conditional clauses on some branches (I am grateful to Frank
   Yellin for this construction)
*/

#define inv_var(x,r,c)\
 ( r == 0 ? ( c == 0 ? x[0] : c == 1 ? x[1] : c == 2 ? x[2] : x[3])\
 : r == 1 ? ( c == 0 ? x[3] : c == 1 ? x[0] : c == 2 ? x[1] : x[2])\
 : r == 2 ? ( c == 0 ? x[2] : c == 1 ? x[3] : c == 2 ? x[0] : x[1])\
 :          ( c == 0 ? x[1] : c == 1 ? x[2] : c == 2 ? x[3] : x[0]))

#define inv_rnd(y,x,k,c)    (y[c] = (k)[c] ^ four_tables(x,t_use(i,n),inv_var,rf1,c))
#define inv_lrnd(y,x,k,c)   (y[c] = (k)[c] ^ four_tables(x,t_use(i,l),inv_var,rf1,c))

void MachineDecryptData(const unsigned char *in_blk,  unsigned char *out_blk)
{
 aes_32t b0[4], b1[4];
 const aes_32t *kp = machine_decryption_key_schedule + 40;

	state_in(b0, in_blk, kp);

	round(inv_rnd,  b1, b0, kp - 4);
	round(inv_rnd,  b0, b1, kp - 8);
	round(inv_rnd,  b1, b0, kp - 12);
	round(inv_rnd,  b0, b1, kp - 16);
	round(inv_rnd,  b1, b0, kp - 20);
	round(inv_rnd,  b0, b1, kp - 24);
	round(inv_rnd,  b1, b0, kp - 28);
	round(inv_rnd,  b0, b1, kp - 32);
	round(inv_rnd,  b1, b0, kp - 36);
	round(inv_lrnd, b0, b1, kp - 40);

	state_out(out_blk, b0);
}

void StratusDecryptData(const unsigned char *in_blk,  unsigned char *out_blk)
{
 aes_32t b0[4], b1[4];
 const aes_32t *kp = stratus_decryption_key_schedule + 40;

	state_in(b0, in_blk, kp);

	round(inv_rnd,  b1, b0, kp - 4);
	round(inv_rnd,  b0, b1, kp - 8);
	round(inv_rnd,  b1, b0, kp - 12);
	round(inv_rnd,  b0, b1, kp - 16);
	round(inv_rnd,  b1, b0, kp - 20);
	round(inv_rnd,  b0, b1, kp - 24);
	round(inv_rnd,  b1, b0, kp - 28);
	round(inv_rnd,  b0, b1, kp - 32);
	round(inv_rnd,  b1, b0, kp - 36);
	round(inv_lrnd, b0, b1, kp - 40);

	state_out(out_blk, b0);
}

#if defined(__cplusplus)
}
#endif

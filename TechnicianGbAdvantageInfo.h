/* Modification History:
Name            Date        Notes
----            ----        -----
*/


#pragma once
#include "afxwin.h"


// CTechnicianGbAdvantageInfo dialog

class CTechnicianGbAdvantageInfo : public CDialog
{
	DECLARE_DYNAMIC(CTechnicianGbAdvantageInfo)

public:
	CTechnicianGbAdvantageInfo(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTechnicianGbAdvantageInfo();
	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_TECHNICIAN_GB_ADVANTAGE_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedManagerVersionInfoButton();

private:
	CListBox m_gb_advantage_information_listbox;
};

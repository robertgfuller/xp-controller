/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   05/20/10    Print a "DM" on the ticket fill and drop reports when a dispenser malfunction is detected during a payout.
Robert Fuller   06/09/09    Added code for custom picture buttons.
Dave Elquist    06/09/05    Initial Revision.
*/


// CashierReports.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include "CashierReports.h"
#include ".\cashierreports.h"


// CCashierReports dialog

IMPLEMENT_DYNAMIC(CCashierReports, CDialog)
CCashierReports::CCashierReports(CWnd* pParent /*=NULL*/)
	: CDialog(CCashierReports::IDD, pParent)
{
	VERIFY(dispenser_button.LoadBitmaps(_T("CDISPENSERU"), _T("CDISPENSERD")));
	VERIFY(tickets_button.LoadBitmaps(_T("CTICKETSU"), _T("CTICKETSD")));
	VERIFY(exit_button.LoadBitmaps(_T("CEXITU"), _T("CEXITD")));
}

CCashierReports::~CCashierReports()
{
}

void CCashierReports::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CCashierReports, CDialog)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_CASHIER_REPORTS_DISPENSER_BUTTON, OnBnClickedCashierReportsDispenserButton)
	ON_BN_CLICKED(IDC_CASHIER_REPORTS_TICKETS_BUTTON, OnBnClickedCashierReportsTicketsButton)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_CASHIER_REPORTS_EXIT_BUTTON, OnBnClickedCashierReportsExitButton)
END_MESSAGE_MAP()

BOOL CCashierReports::OnInitDialog()
{
	CDialog::OnInitDialog();

    VERIFY(dispenser_button.SubclassDlgItem(IDC_CASHIER_REPORTS_DISPENSER_BUTTON, this));
    dispenser_button.SizeToContent();
    VERIFY(tickets_button.SubclassDlgItem(IDC_CASHIER_REPORTS_TICKETS_BUTTON, this));
    tickets_button.SizeToContent();
    VERIFY(exit_button.SubclassDlgItem(IDC_CASHIER_REPORTS_EXIT_BUTTON, this));
    exit_button.SizeToContent();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CCashierReports::OnPaint() 
{
	CRect rect;
	GetClientRect(&rect);
	CPaintDC dc(this); // device context for painting

	CDialog::OnPaint();

    dc.FillSolidRect(&rect, 0x00000000);
}

void CCashierReports::OnBnClickedCashierReportsDispenserButton()
{
    theApp.PrintDispenserInfo();
}

void CCashierReports::OnBnClickedCashierReportsTicketsButton()
{
 CString status_txt, prt_buf, amount_buffer;
 DWORD total_redeem = 0, total_marker = 0, total_pay_to_credit = 0, total_fill_redeem = 0, total_fill_marker = 0, total_fill_pay_to_credit = 0;
 DWORD record_index = 0, total_slave_redeemed = 0, total_slave_marker = 0, total_slave_pay_to_credit = 0;
 FilePaidTicket file_paid_ticket;

    theApp.printer_busy = true;
    theApp.PrinterTitle("TICKET LOG");
    theApp.PrinterId();
    theApp.PrinterData("\n GAME  TICKET ID  PAY-TO-CREDIT");
    theApp.PrinterData("\n GAME  TICKET ID  REDEEM(MARKER)\n");
    theApp.PrinterData(" --------------------------------\n");

    while (fileRead(PAID_TICKETS, record_index, &file_paid_ticket, sizeof(file_paid_ticket)))
    {
        if (file_paid_ticket.dispenser_malfunction)
            prt_buf.Format("DM %u  %u  ", file_paid_ticket.memory.memory_game_lock.ucmc_id, file_paid_ticket.memory.memory_game_lock.ticket_id);
        else
            prt_buf.Format(" %u  %u  ", file_paid_ticket.memory.memory_game_lock.ucmc_id, file_paid_ticket.memory.memory_game_lock.ticket_id);

        if (file_paid_ticket.memory.dispenser_ucmc_id)
        {
            if (file_paid_ticket.memory.memory_game_lock.jackpot_to_credit_flag)
            {
                total_slave_pay_to_credit += file_paid_ticket.memory.memory_game_lock.ticket_amount;
                amount_buffer.Format("%6.2f *SLAVE %u*\n", (double)file_paid_ticket.memory.memory_game_lock.ticket_amount / 100,
                    file_paid_ticket.memory.dispenser_ucmc_id);
            }
            else
            {
                if (file_paid_ticket.memory.memory_game_lock.ticket_amount > file_paid_ticket.memory.memory_game_lock.marker_balance)
                    file_paid_ticket.memory.memory_game_lock.ticket_amount -= file_paid_ticket.memory.memory_game_lock.marker_balance;
                else
                {
                    file_paid_ticket.memory.memory_game_lock.marker_balance = file_paid_ticket.memory.memory_game_lock.ticket_amount;
                    file_paid_ticket.memory.memory_game_lock.ticket_amount = 0;
                }

                total_slave_redeemed += file_paid_ticket.memory.memory_game_lock.ticket_amount;
                total_slave_marker += file_paid_ticket.memory.memory_game_lock.marker_balance;
                amount_buffer.Format("%6.2f(%6.2f) *SLAVE %u*\n", (double)file_paid_ticket.memory.memory_game_lock.ticket_amount / 100,
                    (double)file_paid_ticket.memory.memory_game_lock.marker_balance / 100, file_paid_ticket.memory.dispenser_ucmc_id);
            }
        }
        else
        {
            if (file_paid_ticket.memory.memory_game_lock.jackpot_to_credit_flag)
            {
                if (file_paid_ticket.memory.ticket_filled)
                {
                    total_fill_pay_to_credit += file_paid_ticket.memory.memory_game_lock.ticket_amount;
                    amount_buffer.Format("%6.2f *FILLED*\n", (double)file_paid_ticket.memory.memory_game_lock.ticket_amount / 100);
                }
                else
                {
                    total_pay_to_credit += file_paid_ticket.memory.memory_game_lock.ticket_amount;
                    amount_buffer.Format("%6.2f\n", (double)file_paid_ticket.memory.memory_game_lock.ticket_amount / 100);
                }
            }
            else
            {
                if (file_paid_ticket.memory.memory_game_lock.ticket_amount > file_paid_ticket.memory.memory_game_lock.marker_balance)
                    file_paid_ticket.memory.memory_game_lock.ticket_amount -= file_paid_ticket.memory.memory_game_lock.marker_balance;
                else
                {
                    file_paid_ticket.memory.memory_game_lock.marker_balance = file_paid_ticket.memory.memory_game_lock.ticket_amount;
                    file_paid_ticket.memory.memory_game_lock.ticket_amount = 0;
                }

                if (file_paid_ticket.memory.ticket_filled)
                {
                    total_fill_redeem += file_paid_ticket.memory.memory_game_lock.ticket_amount;
                    total_fill_marker += file_paid_ticket.memory.memory_game_lock.marker_balance;
                    amount_buffer.Format("%6.2f(%6.2f) *FILLED*\n", (double)file_paid_ticket.memory.memory_game_lock.ticket_amount / 100,
                        (double)file_paid_ticket.memory.memory_game_lock.marker_balance / 100);
                }
                else
                {
                    total_redeem += file_paid_ticket.memory.memory_game_lock.ticket_amount;
                    total_marker += file_paid_ticket.memory.memory_game_lock.marker_balance;
                    amount_buffer.Format("%6.2f(%6.2f)\n", (double)file_paid_ticket.memory.memory_game_lock.ticket_amount / 100,
                        (double)file_paid_ticket.memory.memory_game_lock.marker_balance / 100);
                }
            }
        }

        prt_buf += amount_buffer;
        theApp.PrinterData(prt_buf.GetBuffer(0));

        record_index++;
    }

    if (!record_index)
        theApp.PrinterData(" ** NO TICKETS PAID **\n");
    else
    {
        prt_buf.Format("\n TOTAL TICKETS: %6d\n", record_index);
        theApp.PrinterData(prt_buf.GetBuffer(0));

        if (total_redeem)
        {
            prt_buf.Format(" TOTAL PAID OUT: $%9.2f\n", (double)total_redeem / 100);
            theApp.PrinterData(prt_buf.GetBuffer(0));
        }

        if (total_marker)
        {
            prt_buf.Format(" TOTAL MARKER: $%9.2f\n", (double)total_marker / 100);
            theApp.PrinterData(prt_buf.GetBuffer(0));
        }

        if (total_pay_to_credit)
        {
            prt_buf.Format(" TOTAL PAY-TO-CREDIT: $%9.2f\n", (double)total_pay_to_credit / 100);
            theApp.PrinterData(prt_buf.GetBuffer(0));
        }

        if (total_fill_redeem)
        {
            prt_buf.Format(" TOTAL FILL PAID OUT: $%9.2f\n", (double)total_fill_redeem / 100);
            theApp.PrinterData(prt_buf.GetBuffer(0));
        }

        if (total_fill_marker)
        {
            prt_buf.Format(" TOTAL FILL MARKER: $%9.2f\n", (double)total_fill_marker / 100);
            theApp.PrinterData(prt_buf.GetBuffer(0));
        }

        if (total_fill_pay_to_credit)
        {
            prt_buf.Format(" TOTAL FILL PAY-TO-CREDIT: $%9.2f\n", (double)total_fill_pay_to_credit / 100);
            theApp.PrinterData(prt_buf.GetBuffer(0));
        }

        if (total_slave_redeemed)
        {
            prt_buf.Format(" TOTAL SLAVE PAID OUT: $%9.2f\n", (double)total_slave_redeemed / 100);
            theApp.PrinterData(prt_buf.GetBuffer(0));
        }

        if (total_slave_marker)
        {
            prt_buf.Format(" TOTAL SLAVE MARKER: $%9.2f\n", (double)total_slave_marker / 100);
            theApp.PrinterData(prt_buf.GetBuffer(0));
        }

        if (total_slave_pay_to_credit)
        {
            prt_buf.Format(" TOTAL SLAVE PAY-TO-CREDIT: $%9.2f\n", (double)total_slave_pay_to_credit / 100);
            theApp.PrinterData(prt_buf.GetBuffer(0));
        }
    }

    theApp.PrinterData(PRINTER_FORM_FEED);

    theApp.printer_busy = false;
}

void CCashierReports::OnMouseMove(UINT nFlags, CPoint point)
{
	theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

void CCashierReports::OnBnClickedCashierReportsExitButton()
{
	DestroyWindow();
}

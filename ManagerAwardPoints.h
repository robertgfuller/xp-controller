/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   06/09/09    Added mechanism to accomidate manager awarded promotion points.
*/


#pragma once
#include "afxwin.h"
#include "FixedButton.h"


// CManagerAwardPoints dialog

class CManagerAwardPoints : public CDialog
{
	DECLARE_DYNAMIC(CManagerAwardPoints)

public:
	CManagerAwardPoints(CWnd* pParent = NULL);   // standard constructor
	virtual ~CManagerAwardPoints();
	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_MANAGER_AWARD_POINTS_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	afx_msg void OnPaint();

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedExitManagerButton();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedManagerAward5000PointsButton();
	afx_msg void OnBnClickedManagerAward10000PointsButton();
	afx_msg void OnBnClickedManagerAward25000PointsButton();
	afx_msg void OnBnClickedManagerMenuButton1();
	afx_msg void OnBnClickedManagerMenuButton2();
	afx_msg void OnBnClickedManagerMenuButton3();
	afx_msg void OnBnClickedManagerMenuButton4();
	afx_msg void OnBnClickedManagerMenuButton5();
	afx_msg void OnBnClickedManagerMenuButton6();
	afx_msg void OnBnClickedManagerMenuButton7();
	afx_msg void OnBnClickedManagerMenuButton8();
	afx_msg void OnBnClickedManagerMenuButton9();
	afx_msg void OnBnClickedManagerMenuButton0();
	afx_msg void OnBnClickedManagerMenuClearButton();

// private functions
private:
    void ProcessButtonClick(BYTE button_press);

// private variables
private:
    CGbProManagerDialog *pGbProManagerDialog;

    DWORD current_edit_focus, member_id;
	CEdit member_id_edit;
	CStatic manager_points_award_group;
	CStatic manager_points_award_text;

	CFixedButton keypad0;
	CFixedButton keypad1;
	CFixedButton keypad2;
	CFixedButton keypad3;
	CFixedButton keypad4;
	CFixedButton keypad5;
	CFixedButton keypad6;
	CFixedButton keypad7;
	CFixedButton keypad8;
	CFixedButton keypad9;
	CFixedButton keypadC;

	CFixedButton exit;

	CFixedButton award_5000_points_button;
	CFixedButton award_10000_points_button;
	CFixedButton award_25000_points_button;

};

/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   09/30/08    Changed the variable type of the drivers license from DWORD to double to accomidate a 10 digit value.
Robert Fuller   03/20/08    Added code to support the manager approval feature.
Dave Elquist    06/09/05    Initial Revision.
*/


#pragma once
#include "afxwin.h"


// CTaxInformation dialog

class CTaxInformation : public CDialog
{
	DECLARE_DYNAMIC(CTaxInformation)

public:
	CTaxInformation(bool *got_tax_information, DWORD *ss_number, double *license, CWnd* pParent = NULL);   // standard constructor
	virtual ~CTaxInformation();
    virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_CASHIER_LICENSE_SSN_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedCashierDlSsnButton1();
	afx_msg void OnBnClickedCashierDlSsnButton2();
	afx_msg void OnBnClickedCashierDlSsnButton3();
	afx_msg void OnBnClickedCashierDlSsnButton4();
	afx_msg void OnBnClickedCashierDlSsnButton5();
	afx_msg void OnBnClickedCashierDlSsnButton6();
	afx_msg void OnBnClickedCashierDlSsnButton7();
	afx_msg void OnBnClickedCashierDlSsnButton8();
	afx_msg void OnBnClickedCashierDlSsnButton9();
	afx_msg void OnBnClickedCashierDlSsnButton0();
	afx_msg void OnBnClickedCashierDlSsnEnterButton();
	afx_msg void OnBnClickedCashierDlSsnClearButton();
	afx_msg void OnEnSetfocusCashierSsnEdit();
	afx_msg void OnEnSetfocusCashierDlEdit();
	afx_msg void OnEnSetfocusManagerUsernameEdit();
	afx_msg void OnEnSetfocusManagerPasswordEdit();

// private functions
private:
    void ProcessButtonClick(BYTE button_press);

// private variables
private:
    DWORD current_edit_focus, social_security_number, manager_user_id, manager_password;
	double drivers_license_number;
    DWORD *pSocialSecurityNumber;
	double *pDriversLicenseNumber;
    bool *pGotTaxInformation;

	CStatic m_idc_static1;
	CStatic m_idc_static2;
	CStatic m_idc_static3;
	CEdit m_idc_edit1;
	CEdit m_idc_edit2;
};

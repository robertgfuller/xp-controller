/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   12/30/13    Added start and end time information to active promos for troubleshooting purposes.
Robert Fuller	12/05/13	Display "scalable" if the GBA hand promotion is scalable.
Robert Fuller	07/04/13	Added ability to award cashable and non-cashable credits for GBA based wins.
Robert Fuller	09/17/12	Added text to GBA promotion description to designate the interactive GBA win type
Robert Fuller   04/06/10    Add a game configuration flag that turns GB Advantage on/off.
Robert Fuller   06/09/09    Added code to award GB Advantage wins to non-logged in players depending on configuration selection.
*/


// TechnicianGbAdvantageInfo.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include "TechnicianGbAdvantageInfo.h"
#include ".\techniciangbadvantageinfo.h"
#include "WinEvaluation.h"


// CTechnicianGbAdvantageInfo dialog

IMPLEMENT_DYNAMIC(CTechnicianGbAdvantageInfo, CDialog)
CTechnicianGbAdvantageInfo::CTechnicianGbAdvantageInfo(CWnd* pParent /*=NULL*/)
	: CDialog(CTechnicianGbAdvantageInfo::IDD, pParent)
{
}

CTechnicianGbAdvantageInfo::~CTechnicianGbAdvantageInfo()
{
}

void CTechnicianGbAdvantageInfo::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_VERSION_INFORMATION_LIST, m_gb_advantage_information_listbox);
}


BEGIN_MESSAGE_MAP(CTechnicianGbAdvantageInfo, CDialog)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_TECHNICIAN_GB_ADVANTAGE_INFO_BUTTON, OnBnClickedManagerVersionInfoButton)
END_MESSAGE_MAP()


// CTechnicianGbAdvantageInfo message handlers

void CTechnicianGbAdvantageInfo::OnMouseMove(UINT nFlags, CPoint point)
{
    theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

void CTechnicianGbAdvantageInfo::OnBnClickedManagerVersionInfoButton()
{
	DestroyWindow();
}

BOOL CTechnicianGbAdvantageInfo::OnInitDialog()
{
 DWORD hostname_length = MAX_COMPUTERNAME_LENGTH;
 BYTE *pIpAddress;
 CString client_address, edit_string;
 int i;
 GbProWinCategory category;

	CDialog::OnInitDialog();


    if (theApp.memory_settings_ini.activate_gb_advantage_support)
        edit_string = "GB ADVANTAGE IS ACTIVATED";
    else
        edit_string = "GB ADVANTAGE IS TURNED OFF";

	m_gb_advantage_information_listbox.AddString(edit_string);


	m_gb_advantage_information_listbox.AddString("");

    edit_string = "CURRENTLY ACTIVE PROMOTIONS";
	m_gb_advantage_information_listbox.AddString(edit_string);

    for (i=0; i < NUMBER_OF_WIN_CATEGORIES; i++)
    {
        if (theApp.pGbProManager->winCategoryActive(i))
        {
            category = theApp.pGbProManager->getWinCategory((UINT)i);
            
		    edit_string = category.media_string;
		    m_gb_advantage_information_listbox.AddString(edit_string);

		    edit_string.Format("POINTS AWARD: %d", category.points_award);
		    m_gb_advantage_information_listbox.AddString(edit_string);

		    edit_string.Format("CASH VOUCHER AWARD (in dollars): %d", category.cash_voucher_award);
		    m_gb_advantage_information_listbox.AddString(edit_string);

            if (category.scalable_award)
		        m_gb_advantage_information_listbox.AddString("SCALABLE");

            if (category.cashable_credits)
            {
		        edit_string.Format("CASHABLE CREDITS (in dollars): %d", category.cashable_credits / 100);
		        m_gb_advantage_information_listbox.AddString(edit_string);
            }

            if (category.noncashable_credits)
            {
		        edit_string.Format("NONCASHABLE CREDITS (in dollars): %d", category.noncashable_credits / 100);
		        m_gb_advantage_information_listbox.AddString(edit_string);
            }

            if (category.i_rewards_paid_win)
            {
    		    edit_string.Format("INTERACTIVE BONUS GAME");
    		    m_gb_advantage_information_listbox.AddString(edit_string);
            }

		    edit_string.Format("MINIMUM MONEY BET (in cents): %d", category.minimum_money_bet);
		    m_gb_advantage_information_listbox.AddString(edit_string);

		    edit_string.Format("MINIMUM DENOMINATION (in cents): %d", category.minimum_denomination);
		    m_gb_advantage_information_listbox.AddString(edit_string);

		    edit_string.Format("MINIMUM BET (in credits ): %d", category.minimum_bet);
		    m_gb_advantage_information_listbox.AddString(edit_string);

		    edit_string.Format("GB LOGIN NOT REQUIRED: %d (0 = NO)", category.gb_login_not_required);
		    m_gb_advantage_information_listbox.AddString(edit_string);

		    edit_string.Format("PRINT DRAWING TICKET: %d (0 = NO)", category.print_drawing_ticket);
		    m_gb_advantage_information_listbox.AddString(edit_string);

		    edit_string.Format("START TIME: %s ", convertTimeStamp(category.start_time));
		    m_gb_advantage_information_listbox.AddString(edit_string);

		    edit_string.Format("END TIME: %s ", convertTimeStamp(category.end_time));
		    m_gb_advantage_information_listbox.AddString(edit_string);

        	m_gb_advantage_information_listbox.AddString("");

        }
    }


	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

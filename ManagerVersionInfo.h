/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    07/15/05    Moved function.
Dave Elquist    06/09/05    Initial Revision.
*/


#pragma once
#include "afxwin.h"


// CManagerVersionInfo dialog

class CManagerVersionInfo : public CDialog
{
	DECLARE_DYNAMIC(CManagerVersionInfo)

public:
	CManagerVersionInfo(CWnd* pParent = NULL);   // standard constructor
	virtual ~CManagerVersionInfo();
	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_MANAGER_VERSION_INFO_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	afx_msg void OnPaint();

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedManagerVersionInfoButton();

private:
	CListBox m_version_information_listbox;
};

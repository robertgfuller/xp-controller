/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    07/15/05    Changed dialog layout.
Dave Elquist    05/16/05    Initial Revision.
*/


// MenuEngineer.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include ".\menuengineer.h"


// CMenuEngineer dialog

IMPLEMENT_DYNAMIC(CMenuEngineer, CDialog)
CMenuEngineer::CMenuEngineer(CWnd* pParent /*=NULL*/)
	: CDialog(CMenuEngineer::IDD, pParent)
{
    current_edit_focus = 0;
}

CMenuEngineer::~CMenuEngineer()
{
}

void CMenuEngineer::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CMenuEngineer, CDialog)
	ON_BN_CLICKED(IDC_EXIT_ENGINEER_BUTTON, OnBnClickedExitEngineerButton)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_ENGR_CONTROL_PANEL_BUTTON, OnBnClickedEngrControlPanelButton)
	ON_BN_CLICKED(IDC_ENGR_KEYBOARD_BUTTON, OnBnClickedEngrKeyboardButton)
	ON_BN_CLICKED(IDC_ENGR_WINDOWS_EXPLORER_BUTTON, OnBnClickedEngrWindowsExplorerButton)
	ON_BN_CLICKED(IDC_ENGR_TASK_MANAGER_BUTTON, OnBnClickedEngrTaskManagerButton)
END_MESSAGE_MAP()


void CMenuEngineer::OnBnClickedExitEngineerButton()
{
    theApp.current_dialog_return = IDD_MAIN_MENU_DIALOG;
	DestroyWindow();
}

void CMenuEngineer::OnMouseMove(UINT nFlags, CPoint point)
{
	theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

BOOL CMenuEngineer::OnInitDialog()
{
 BYTE *pIpAddress = (BYTE*)&theApp.memory_settings_ini.game_lock_printer_ip_address;
 CString edit_string;

	CDialog::OnInitDialog();

    edit_string.Format("%u.%u.%u.%u", pIpAddress[0], pIpAddress[1], pIpAddress[2], pIpAddress[3]);
    SetDlgItemText(IDC_ENGINEER_GAME_LOCK_PRINTER_ADDRESS_EDIT, edit_string);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CMenuEngineer::OnBnClickedEngrControlPanelButton()
{
	ShellExecute(NULL, "open", "control.exe", NULL, NULL, SW_SHOWMAXIMIZED);
}

void CMenuEngineer::OnBnClickedEngrKeyboardButton()
{
	ShellExecute(NULL, "open", "osk.exe", NULL, NULL, SW_SHOWMAXIMIZED);
}

void CMenuEngineer::OnBnClickedEngrWindowsExplorerButton()
{
	ShellExecute(NULL, "open", "explorer.exe", NULL, NULL, SW_SHOWMAXIMIZED);
}

void CMenuEngineer::OnBnClickedEngrTaskManagerButton()
{
	ShellExecute(NULL, "open", "taskmgr.exe", NULL, NULL, SW_SHOWMAXIMIZED);
}

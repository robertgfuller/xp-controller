#if !defined( _AESTAB_H )
#define _AESTAB_H

#define t_dec(m,n) t_##m##n
#define t_set(m,n) t_##m##n
#define t_use(m,n) t_##m##n

#if defined(DO_TABLES)
#define Extern
#else
#define Extern extern
#endif

#if defined(__cplusplus)
extern "C"
{
#endif

#if defined(DO_TABLES) && defined(FIXED_TABLES)
#define d_4(t,n,b,e,f,g,h) const t n[4][256] = { b(e), b(f), b(g), b(h) }
Extern const aes_32t t_dec(r,c)[10] = rc_data(w0);
#else
#define d_4(t,n,b,e,f,g,h) Extern const t n[4][256]
Extern const aes_32t t_dec(r,c)[10];
#endif

d_4(aes_32t, t_dec(f,n), sb_data, u0, u1, u2, u3);
d_4(aes_32t, t_dec(f,l), sb_data, w0, w1, w2, w3);
d_4(aes_32t, t_dec(i,n), isb_data, v0, v1, v2, v3);
d_4(aes_32t, t_dec(i,l), isb_data, w0, w1, w2, w3);
d_4(aes_32t, t_dec(i,m), mm_data, v0, v1, v2, v3);

#if defined(__cplusplus)
}
#endif

#endif

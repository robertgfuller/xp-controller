/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   07/06/09    Delete the GB Advantage manager when user configures the controller as a slave.
Dave Elquist    07/15/05    Initial Revision.
*/


// TechnicianMasterSlaveDialog.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include ".\technicianmasterslavedialog.h"


// CTechnicianMasterSlaveDialog dialog

IMPLEMENT_DYNAMIC(CTechnicianMasterSlaveDialog, CDialog)
CTechnicianMasterSlaveDialog::CTechnicianMasterSlaveDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CTechnicianMasterSlaveDialog::IDD, pParent)
{
}

CTechnicianMasterSlaveDialog::~CTechnicianMasterSlaveDialog()
{
}

void CTechnicianMasterSlaveDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TECHNICIAN_MASTER_SLAVE_LIST, m_controller_list_box);
}


BEGIN_MESSAGE_MAP(CTechnicianMasterSlaveDialog, CDialog)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_TECHNICIAN_MASTER_SLAVE_BUTTON1, OnBnClickedTechnicianMasterSlaveButton1)
	ON_BN_CLICKED(IDC_TECHNICIAN_MASTER_SLAVE_BUTTON2, OnBnClickedTechnicianMasterSlaveButton2)
	ON_BN_CLICKED(IDC_TECHNICIAN_MASTER_SLAVE_BUTTON3, OnBnClickedTechnicianMasterSlaveButton3)
	ON_BN_CLICKED(IDC_TECHNICIAN_MASTER_SLAVE_BUTTON4, OnBnClickedTechnicianMasterSlaveButton4)
	ON_BN_CLICKED(IDC_TECHNICIAN_MASTER_SLAVE_BUTTON5, OnBnClickedTechnicianMasterSlaveButton5)
	ON_BN_CLICKED(IDC_TECHNICIAN_MASTER_SLAVE_BUTTON6, OnBnClickedTechnicianMasterSlaveButton6)
	ON_BN_CLICKED(IDC_TECHNICIAN_MASTER_SLAVE_BUTTON7, OnBnClickedTechnicianMasterSlaveButton7)
	ON_BN_CLICKED(IDC_TECHNICIAN_MASTER_SLAVE_BUTTON8, OnBnClickedTechnicianMasterSlaveButton8)
	ON_BN_CLICKED(IDC_TECHNICIAN_MASTER_SLAVE_BUTTON9, OnBnClickedTechnicianMasterSlaveButton9)
	ON_BN_CLICKED(IDC_TECHNICIAN_MASTER_SLAVE_PERIOD_BUTTON, OnBnClickedTechnicianMasterSlavePeriodButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_MASTER_SLAVE_BUTTON0, OnBnClickedTechnicianMasterSlaveButton0)
	ON_BN_CLICKED(IDC_TECHNICIAN_MASTER_SLAVE_CLEAR_BUTTON, OnBnClickedTechnicianMasterSlaveClearButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_SLAVE_CONTROLLER_ADD_BUTTON, OnBnClickedTechnicianSlaveControllerAddButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_MASTER_CONTROLLER_ADD_BUTTON, OnBnClickedTechnicianMasterControllerAddButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_CONTROLLER_REMOVE_BUTTON, OnBnClickedTechnicianControllerRemoveButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_MASTER_SLAVE_EXIT_BUTTON, OnBnClickedTechnicianMasterSlaveExitButton)
END_MESSAGE_MAP()


// CTechnicianMasterSlaveDialog message handlers

void CTechnicianMasterSlaveDialog::OnMouseMove(UINT nFlags, CPoint point)
{
	theApp.OnMouseMove();
    CDialog::OnMouseMove(nFlags, point);
}

void CTechnicianMasterSlaveDialog::ProcessButtonClick(BYTE button_press)
{
 char string_add[2];
 CString edit_string;

    if (button_press == '.')
        string_add[0] = button_press;
    else
        string_add[0] = 0x30 + button_press;

    string_add[1] = 0;
    GetDlgItemText(IDC_TECHNICIAN_MASTER_SLAVE_EDIT, edit_string);
    edit_string += string_add;
    SetDlgItemText(IDC_TECHNICIAN_MASTER_SLAVE_EDIT, edit_string);
}

void CTechnicianMasterSlaveDialog::OnBnClickedTechnicianMasterSlaveButton1()
{
    ProcessButtonClick(1);
}

void CTechnicianMasterSlaveDialog::OnBnClickedTechnicianMasterSlaveButton2()
{
    ProcessButtonClick(2);
}

void CTechnicianMasterSlaveDialog::OnBnClickedTechnicianMasterSlaveButton3()
{
    ProcessButtonClick(3);
}

void CTechnicianMasterSlaveDialog::OnBnClickedTechnicianMasterSlaveButton4()
{
    ProcessButtonClick(4);
}

void CTechnicianMasterSlaveDialog::OnBnClickedTechnicianMasterSlaveButton5()
{
    ProcessButtonClick(5);
}

void CTechnicianMasterSlaveDialog::OnBnClickedTechnicianMasterSlaveButton6()
{
    ProcessButtonClick(6);
}

void CTechnicianMasterSlaveDialog::OnBnClickedTechnicianMasterSlaveButton7()
{
    ProcessButtonClick(7);
}

void CTechnicianMasterSlaveDialog::OnBnClickedTechnicianMasterSlaveButton8()
{
    ProcessButtonClick(8);
}

void CTechnicianMasterSlaveDialog::OnBnClickedTechnicianMasterSlaveButton9()
{
    ProcessButtonClick(9);
}

void CTechnicianMasterSlaveDialog::OnBnClickedTechnicianMasterSlaveButton0()
{
    ProcessButtonClick(0);
}

void CTechnicianMasterSlaveDialog::OnBnClickedTechnicianMasterSlavePeriodButton()
{
    ProcessButtonClick('.');
}

void CTechnicianMasterSlaveDialog::OnBnClickedTechnicianMasterSlaveClearButton()
{
    SetDlgItemText(IDC_TECHNICIAN_MASTER_SLAVE_EDIT, "");
}

void CTechnicianMasterSlaveDialog::FillMasterSlaveListBox()
{
 BYTE *pIpAddress;
 DWORD file_index;
 CString edit_string;
 MasterSlaveConfig master_slave_config;

    m_controller_list_box.ResetContent();

    if (!theApp.memory_settings_ini.masters_ip_address && GetFileAttributes(MASTER_CONFIG) == 0xFFFFFFFF)
    {
        m_controller_list_box.AddString("MULTI-CONTROLLER");
        m_controller_list_box.AddString("NOT SETUP.");
    }
    else if (theApp.memory_settings_ini.masters_ip_address && GetFileAttributes(MASTER_CONFIG) != 0xFFFFFFFF)
    {
        m_controller_list_box.AddString("CONFLICTING MASTER &");
        m_controller_list_box.AddString("SLAVE RELATIONSHIP.");
    }

    if (theApp.memory_settings_ini.masters_ip_address)
    {
        m_controller_list_box.AddString("MASTER'S IP ADDRESS:");
        pIpAddress = (BYTE*)&theApp.memory_settings_ini.masters_ip_address;
        edit_string.Format("    %u.%u.%u.%u", pIpAddress[0], pIpAddress[1], pIpAddress[2], pIpAddress[3]);
        m_controller_list_box.AddString(edit_string);
    }

    if (GetFileAttributes(MASTER_CONFIG) != 0xFFFFFFFF)
    {
        m_controller_list_box.AddString("SLAVE CONTROLLER LIST:");
        file_index = 0;
        pIpAddress = (BYTE*)&master_slave_config.ip_address;

        while (fileRead(MASTER_CONFIG, file_index, &master_slave_config, sizeof(master_slave_config)))
        {
            edit_string.Format("    %u.%u.%u.%u", pIpAddress[0], pIpAddress[1], pIpAddress[2], pIpAddress[3]);
            m_controller_list_box.AddString(edit_string);
            file_index++;
        }
    }
}

void CTechnicianMasterSlaveDialog::OnBnClickedTechnicianSlaveControllerAddButton()
{
 DWORD ip_address;
 CString edit_string;
 MasterSlaveConfig master_slave_config;

    if (theApp.memory_settings_ini.masters_ip_address)
        MessageWindow(false, "ADD SLAVE CONTROLLER", "REMOVE MASTER'S IP ADDRESS\nBEFORE ADDING ANY SLAVES");
    else
    {
        GetDlgItemText(IDC_TECHNICIAN_MASTER_SLAVE_EDIT, edit_string);
        theApp.GetIpAddressAndPort(edit_string, &ip_address);

        if (ip_address && fileGetRecord(MASTER_CONFIG, &master_slave_config, sizeof(master_slave_config), ip_address) < 0)
        {
            memset(&master_slave_config, 0, sizeof(master_slave_config));
            master_slave_config.ip_address = ip_address;
            if (fileWrite(MASTER_CONFIG, -1, &master_slave_config, sizeof(master_slave_config)))
                SetDlgItemText(IDC_TECHNICIAN_MASTER_SLAVE_EDIT, "");
        }
    }

    FillMasterSlaveListBox();
}

void CTechnicianMasterSlaveDialog::OnBnClickedTechnicianMasterControllerAddButton()
{
 DWORD ip_address;
 CString edit_string;

    if (GetFileAttributes(MASTER_CONFIG) != 0xFFFFFFFF)
        MessageWindow(false, "MASTER CONTROLLER", "REMOVE ALL SLAVE CONTROLLERS\nBEFORE ADDING MASTER'S IP ADDRESS");
    else
    {
        GetDlgItemText(IDC_TECHNICIAN_MASTER_SLAVE_EDIT, edit_string);
        theApp.GetIpAddressAndPort(edit_string, &ip_address);

        if (ip_address)
        {
            theApp.memory_settings_ini.masters_ip_address = ip_address;
            fileWrite(MAIN_SETTINGS_INI, 0, &theApp.memory_settings_ini, sizeof(theApp.memory_settings_ini));
            SetDlgItemText(IDC_TECHNICIAN_MASTER_SLAVE_EDIT, "");
        }
    }

    FillMasterSlaveListBox();
}

void CTechnicianMasterSlaveDialog::OnBnClickedTechnicianControllerRemoveButton()
{
 int record_position;
 DWORD ip_address;
 CString edit_string;
 MasterSlaveConfig file_record;

    GetDlgItemText(IDC_TECHNICIAN_MASTER_SLAVE_EDIT, edit_string);
    theApp.GetIpAddressAndPort(edit_string, &ip_address);

    if (ip_address)
    {
        if (ip_address == theApp.memory_settings_ini.masters_ip_address)
        {
            theApp.memory_settings_ini.masters_ip_address = 0;
            fileWrite(MAIN_SETTINGS_INI, 0, &theApp.memory_settings_ini, sizeof(theApp.memory_settings_ini));
            SetDlgItemText(IDC_TECHNICIAN_MASTER_SLAVE_EDIT, "");
        }
        else
        {
            record_position = fileGetRecord(MASTER_CONFIG, &file_record, sizeof(file_record), ip_address);
            if (record_position >= 0 && fileDeleteRecord(MASTER_CONFIG, sizeof(file_record), record_position) &&
                !fileRecordTotal(MASTER_CONFIG, sizeof(file_record)))
                    DeleteFile(MASTER_CONFIG);
        }
    }

    FillMasterSlaveListBox();
}

BOOL CTechnicianMasterSlaveDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

    FillMasterSlaveListBox();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CTechnicianMasterSlaveDialog::OnBnClickedTechnicianMasterSlaveExitButton()
{
	DestroyWindow();
}

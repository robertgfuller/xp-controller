/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    06/24/05    Initial Revision.
*/


#pragma once
#include "afxwin.h"


// CGbProManagerDialog dialog

class CGbProManagerDialog : public CDialog
{
	DECLARE_DYNAMIC(CGbProManagerDialog)

public:
	CGbProManagerDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CGbProManagerDialog();

// Dialog Data
	enum { IDD = IDD_MANAGER_GB_PRO_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedManagerGbProExitButton();
	afx_msg void OnBnClickedGbProGenerateMillionDollarTicketButton();
	afx_msg void OnBnClickedGbProPlayerPointsButton();
	afx_msg void OnBnClickedManagerGbProAccountButton1();
	afx_msg void OnBnClickedManagerGbProAccountButton2();
	afx_msg void OnBnClickedManagerGbProAccountButton3();
	afx_msg void OnBnClickedManagerGbProAccountButton4();
	afx_msg void OnBnClickedManagerGbProAccountButton5();
	afx_msg void OnBnClickedManagerGbProAccountButton6();
	afx_msg void OnBnClickedManagerGbProAccountButton7();
	afx_msg void OnBnClickedManagerGbProAccountButton8();
	afx_msg void OnBnClickedManagerGbProAccountButton9();
	afx_msg void OnBnClickedManagerGbProAccountButton0();
	afx_msg void OnBnClickedManagerGbProAccountClearButton();

// private functions
private:
    void ProcessButtonClick(BYTE button_press);

// private variables
private:
    DWORD player_account;
	CComboBox m_player_points;
	CButton m_million_dollar_ticket_button;
};

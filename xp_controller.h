/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   10/14/14    Purge bill accepted messages from pending idc tx message file and prioritize pending messages if they exist in the file.
Robert Fuller	12/05/13	Added an additional w2g printer queue which is serviced in the once second timer routine to slow down the data flow to the printer.
Robert Fuller	12/05/13	Added code to send IDC the 4ok bingo winning card message. 
Robert Fuller	12/05/13	Added code to send IDC the bill denomination when a bill is accepted in the gaming machine. 
Robert Fuller	12/05/13	Added code to send IDC the value of the credit meter when it is fetched in real time via the SAS current credits message. 
Robert Fuller	12/05/13	Added IDC to XPC message to print w2gs to automate the tax reporting process.
Robert Fuller	12/05/13	Added IDC to XP Controller message to relay w2g social security and drivers license information after the drivers license information is scanned after of a taxable jackpot win.
Robert Fuller	12/05/13	When processing an add game request, instantiate the game control object before instantiating the game device object,
Robert Fuller   11/25/13    Added a transaction id to the winning hand message so we can identify duplicate winning hand messages.
Robert Fuller   11/25/13    Added a transaction id to the clear card message so we can identify duplicate clear card messages.
Robert Fuller   11/20/13    Added a transaction id in the redemption complete message to allow gaming devices to identify duplicate messages.
Robert Fuller	07/04/13	Added a communication interface to the Station's Casino Casino Management System (CMS)
Robert Fuller	07/04/13	Send social security number and drivers license information to the IDC when the player hits a taxable jackpot for accounting purposes.
Robert Fuller	07/04/13	Save important Stratus TX messages to a file on disk and do not purge unless Stratus returns an ack.
Robert Fuller	05/10/13	Added transaction id an time/date stamp fields to the dispenser info message.
Robert Fuller	05/10/13	Added code to save pending IDC TX messages to disk until they are acknowledged by the IDC. 
Robert Fuller   05/02/12    Added code for future support for the GBA interface to Stations Casino Management System.
Robert Fuller   03/08/13    Removed obsolete Location Collection code.
Robert Fuller   03/08/13    Added code to support the configurable dispenser payout limit..
Robert Fuller   03/08/13    Send XPC version and configuration info to IDC.
Robert Fuller   02/04/13    Only send IDC tx messages after recieving an ack from the previous message or waiting 2 seconds.
Robert Fuller	09/17/12	Added code to fill the ucmc id field in the dispense message we send to the IDC for accounting purposes.
Robert Fuller	09/17/12	Added iGBA transaction id to differentiate iGBA wins amongst multiple games.
Robert Fuller	09/17/12	Added a code to support external awarded points for slot/keno win bonuses and manager awarded points.
Robert Fuller	09/17/12	Added code to send dispense info to the IDC.
Robert Fuller	09/17/12	Added code to relay machine information to the IDC.
Robert Fuller	09/17/12	Make sure we are ignoring GB Interface devices when traversing through the game device linked list.
Robert Fuller   06/04/12    Added code to support the new Quick Enrollment feature.
Robert Fuller   06/04/12    Added code to communicate with the Interactive Display Controller.
Robert Fuller   06/04/12    Send Stratus message when starting or ending a cashier shift or logging in.
Robert Fuller   03/05/12    Send the new primary and secondary 4ok bingo awards in the winning card message.
Robert Fuller   02/09/11    Do NOT autologout the collector when we are in the middle of a fill.
Robert Fuller   02/08/10    Added code to display error counters information on dispenser status screen for the new Puloon dispenser.
Robert Fuller   12/30/11    Added code to print MarkerTrax printer output to an electronic file for troubleshooting and backup purposes.
Robert Fuller   12/30/11    Added an exclusive stratus message transmit queue for Marker Trax messages. Made sure that these messages are only dequeued
                            after receiving the Stratus response.
Robert Fuller   12/30/11    Make sure we are getting the game pointer associated with the GB Interface, not the SAS direct connect game pointer when
                            sending the initialization and update messages.
Robert Fuller   08/03/11    Added code to support Location Collection features.
Robert Fuller   06/08/11    Added mods to support GB with the XP/SAS direct connect feature.
Robert Fuller   08/09/10    Added mods to support the XP/SAS direct connect feature.
Robert Fuller   05/20/10    Print a "DM" on the ticket fill and drop reports when a dispenser malfunction is detected during a payout.
Robert Fuller   04/26/10    Added additional customer ids to the configuration settings. Get netwin data for all customer ids when calculating shift report data.
Robert Fuller	04/07/10	Write drop report info to a file at drop time.
Robert Fuller   01/19/10    Slowed the dispenser poll cycle down to once per second
Robert Fuller   12/08/09    Once a day we purge the "dated paid ticket" file by removing all tickets in the file that over 14 days old.
Robert Fuller   09/03/09    Added code to relay game configuration data for enabled games to the Stratus.
Robert Fuller   07/15/09    Increased the number of shifts to print on the shift report from 4 to 21.
Robert Fuller   04/17/09    Connect to the new Stratus time/date server that send each individual field of the time/date,
                            as opposed to sending the number of seconds since Jan 1, 1970.
Robert Fuller   04/16/09    Removed code associated with PRAC interface.
Robert Fuller   04/08/09    Double check the Netwin calculations on the Stratus by keeping track of game meters locally.
Robert Fuller   04/08/09    Change shift paradigm to only allow a single shift per login id to be active at a time.
Robert Fuller   10/14/08    Avoid displaying the ticket pay pending tilt if the controller paying the ticket is the same as the
                            controller that logged the pending ticket.
Robert Fuller   10/14/08    Increased the frequency of the game lock ding and light flash.
Robert Fuller   09/25/08    Ordered the games in the redeem log report in order of mux id instead of ucmc id.
Robert Fuller   09/08/08    Added code to print the handle on the shift report.
Robert Fuller   08/19/08    Added code to support the GB Advantage feature.
Robert Fuller	03/25/08	Added the bag and tag info array.
Robert Fuller	01/11/08	Added modification for bag and tag feature.
Robert Fuller   11/05/07    Added support for the X10 Active Home mechanism to flash lamps and play chimes for game locks.
Dave Elquist    07/15/05    Added Marker support. Added GB Pro support.
Dave Elquist    04/14/05    Initial Revision.
*/


// xp_controller.h : main header file for the PROJECT_NAME application
//


#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#if _MSC_VER > 1200
#import "progid:X10.ActiveHome" 
#else
#import "c:\\program files\\common files\\x10\\common\\ahscript.dll"
#endif

#include "resource.h"		// main symbols
#include "utility.h"
#include "dispenser.h"
#ifndef GAMECOMM_H
#include "gamecomm.h"
#endif
#include "initialinfo.h"
#include "MainMenuDialog.h"
#include "MenuCashier.h"
#include "MenuManager.h"
#include "MenuCollector.h"
#include "MenuTechnician.h"
#include "MenuEngineer.h"
#include "Afxmt.h"
#include "MultiController.h"
#include "SelectCustomerIdDlg.h"
#include "TechnicianDisplay.h"
#include "GbProManager.h"
#include "CustomerControl.h"
#include "DispenserDialog.h"
#include "GameCollectionState.h"
#include "MyAsyncSocket.h"
#include "RasConnectionStatus.h"
#ifndef SAS_COMM_H
#include "SAS_Comm.h"
#endif

struct StratusTimeDateReturn
{
  WORD wYear;  // 2 bytes
  WORD wMonth; // 2 bytes
  WORD wDayOfWeek; // 2 bytes
  WORD wDay; // 2 bytes
  WORD wHour; // 2 bytes
  WORD wMinute; // 2 bytes
  WORD wSecond; // 2 bytes
  WORD wMilliseconds; // 2 bytes
}; 

struct TotalPaidPerGame
{
    DWORD ticket_amount;
    DWORD ucmc_id;
};

#pragma pack(1)
struct XpcVersionAndConfigInfo
{
	DWORD version;
	char subversion[4];
	BYTE gba_configured_location;
};
#pragma pack()

// CControllerApp:
// See xp_controller.cpp for the implementation of this class
//

class CControllerApp : public CWinApp
{
// public class functions
public:
	CControllerApp();
    ~CControllerApp();

	void CheckEventMessages(bool program_exit);
    void ProcessExitProgramRequest(bool reboot);
	void ProcessRequestMachineData(MachineIdentifiers* machine_identifiers);
    void OnMouseMove();
    void GetCustomerIdSelection(BYTE display_type);
    void RemovePendingTicketFromFile(DWORD ticket_id);
    bool CheckSlaveLoginPassword(ControllerUser *user);
    bool CheckLoginPassword(ControllerUser *user);
    void GetIpAddressAndPort(CString text_string, DWORD *ip_address, WORD *ip_port = NULL);
    void GameDateAndTimeUpdate();
	void RemoveGameCommControl(CGameCommControl *pGameCommControl);
	void shutdownMediaBoxApp (void);
	void purgeDatedPaidTicketsFile(void);
	void dispenserMalfunctionWithLastPaidTicket (void);
	void dequeStratusTxQueue();

    // printer functions
    void PrinterData(char *printer_data);
	void W2gPrinterData(char *printer_data);
	void MarkerPrinterData(char *printer_data);
    void PrinterTitle(char *printer_data);
    void PrinterHeader(BYTE printer_destination);
    void PrinterId();

	// drop report printing functions
	void DropReportData(char *printer_data);

    // Stratus message functions
    void QueueStratusTxMessage(StratusMessageFormat *tx_message, bool write_to_disk);
	void QueueMarkerStratusTxMessage(StratusMessageFormat *tx_message);
    void RequestNetwinFromStratus(NetwinGame netwin_message, DWORD customer_id);
	BYTE* EncryptStratusData(BYTE *data, int *length);
	BYTE* DecryptStratusData(BYTE *data, int length);

    // game functions
    bool GetGameLockFilename(DWORD ucmc_id, DWORD amount, char *filename);
    CGameDevice* GetGamePointer(DWORD ucmc_id);
	CGameDevice* GetSasGamePointer(DWORD ucmc_id);
	CGameDevice* GetSasGamePointer(char* machine_id);
	CGameDevice* GetGbInterfaceGamePointer(DWORD ucmc_id);
	CGameDevice* GetGamePointer(BYTE mux_id);
	CGameDevice* CControllerApp::GetGamePointer(BYTE mux_id, WORD game_port);
	CGameCommControl* GetGameCommControlPointer(DWORD ucmc_id);
	CGameCommControl* GetGbInterfaceGameCommControlPointer(DWORD ucmc_id);
	CGameCommControl* GetSasGameCommControlPointer(DWORD ucmc_id);
	CGameCommControl* GetNextGameCommControlPointer(CGameCommControl *pGameCommControl);
//	CGameCommControl* GetNextSasGameCommControlPointer(CGameCommControl *pGameCommControl);
//	CGameCommControl* GetFirstSasGameCommControlPointer(void);
//	WORD GetGameCommControlCount (void);

	void initializeGbInterfaceData(GbInterfaceInitialize* initialize_gb_interface, DWORD ucmc_id);
	void updateGbInterfaceData(UpdateGbInterfaceData* update_gb_interface_data, DWORD ucmc_id);
	void updateSasCommunicationStatus(bool communicating, DWORD ucmc_id);
	void updateGbInterfaceWithBingoWinningHandInfo (BingoWinningHand* bingo_winning_hand, DWORD ucmc_id);
	void updateGbInterfaceWithBingoWinningCardInfo (UpdateGbInterfaceWinningCardData* winning_card_data, DWORD ucmc_id);
	BYTE* EncryptMachineData(BYTE *data, int *length);
	BYTE* DecryptMachineData(BYTE *data, int length);

    // dispenser functions
    void OpenCashDrawer();
	void SendDispenserBillCount(bool initial_fill, DWORD ucmc_id);
    void SendDispenserDataForDisplay(char *string_data);
    void SendDispenserDataForDisplay(BYTE *data, WORD length, bool tx_data);
    bool DispenserMoneyLowCheck(DWORD amount);
    void PrintDispenserInfo();
	void ResetDispenser();
	char* SendDispenserInfo(CCustomerControlClient *socket);
	void BroadcastDispenserPayoutLimit (void);
    void BalanceBreakage(BYTE type, DWORD amount);
	void fillDispenser (void);

    // report functions
	void SortGameRedeemRecords(void);
	void GameRedeemReport(DWORD customer_id);
    void PrintW2gTickets(DWORD customer_id);
    void DropTicketsPrint(DWORD customer_id);
	DWORD getTotalDropTicketAmount(DWORD customer_id);
	DWORD getTotalDropTicketAmount(DWORD customer_id, DWORD ucmc_id);
    void CashAdjustmentReport();
    DWORD GetNetwinReport(BOOL print_data, DWORD customer_id, BOOL initialize_totals);
	DWORD GetNetwinReportFromXpController(BOOL print_data, DWORD customer_id);
	DWORD GetNetwinReportFromStratus(BOOL print_data, DWORD customer_id, BOOL initialize_totals);
	DWORD getTotalNetwinHandle (void);
	DWORD getTotalNetwinDrop (void);
	void PrintCashierShift(DWORD cashier_id, BYTE number_of_shifts, bool restrict_data, bool end_shift);
    void PrintingCashierShift(FileCashierShift *shift_data, WORD shift_number, bool restrict_data, bool end_shift);

    // email functions
    void SendEmailMessage(EmailMessageFormat *email_message);

	// x10 technology funtions
	void turnOnActiveHomeLight (bool turn_on);
	void turnOnActiveHomeChime (bool chime_on);

	// remote network setup/checking functions
	void CheckRemoteNetworkConnection();
	void CheckModemLineConnection();
	void CloseRasConnection();
	char* GetRasAsciiString(DWORD error_code);

	// GB to external parties functions
	// Globally accessed XPC / IDC functions
	void SendMachineSpecificData(CGameDevice *pGameDevice);
	void SendIdcGameMeterMessage (CGameDevice *pGameDevice, GameMeters* game_meters);
	void SendIdcCurrentCreditsMessage (CGameDevice *pGameDevice, ULONG* current_credits);
	void SendIdcBillAcceptedMessage (CGameDevice *pGameDevice, IdcBillAcceptedMessage* bill_accepted);
	void SendIdcLogoutMessage (CGameDevice *pGameDevice, AccountLogoutData* account_logout_data);
	void SendIdcLoginMessage (CGameDevice *pGameDevice, AccountLogin* account_login);
	void SendIdcGameLockMessage (FileGameLock* file_game_lock, IdcProtocolHeader* idc_protocol_header);
	void SendIdcGameLockReceivedMessage (FileGameLock* file_game_lock);
	void SendIdcGameLockCompleteMessage (FilePaidTicket* file_paid_ticket);
	void SendIdcRedeemRequestMessage (CGameDevice *pGameDevice);
	void SendIdcSetPromotionWinCategoryMessage (IdcGbAdvantageWinCategory* win_category);
	void SendIdcFillValuesMessage (FillValues* fill_values);
	void SendIdcDispenseInfoMessage (BillMessage* bill_message, DWORD ucmc_id);
	void SendIdcManualDispenseInfoMessage (ManualDispenseMessageData* manual_dispense_message);
	void SendIdcStackerPullMessage (CGameDevice *pGameDevice, byte pulled);
	void SendIdcGameEnteredMessage (CGameDevice *pGameDevice, char* paytable_id, char* manufacturer_id);
	void SendIdcUserLoginLogoffMessage (DWORD user_id, BYTE login);
	void SendIdcGbAdvantageWinMessage (IdcGbAdvantageWinMessage* win_message, CGameDevice *pGameDevice);
	void SendIdcAckMessage (IdcProtocolHeader *header, bool ack);
	void SendIdcTransactionIdAckMessage (IdcProtocolHeader *header, DWORD transaction_id);
	void SendIdcCmsLoginMessage (CGameDevice* pGameDevice, CmsLoginMessage* login_message);
	void SendIdcCmsLogoutMessage (CmsProtocolHeader *header, DWORD account_number);
	void SendIdcCmsConnectionStatusMessage (bool disconnect);
	void SendIdcWinningBingoCardMessage (WinningCardInfo* winning_card_info, CGameDevice *pGameDevice);

	// shift report functions
    void CashierShiftStart(DWORD slave_login_id);

	// GB to IDC functions
	void SendAllMachineSpecificData(void);
	void SendIdcXpcVersionConfigInfo(void);

	// XPC / CMS messages
	void SendCmsGbAdvantageWinMessage (DWORD ucmc_id, IdcGbAdvantageWinMessage* win_message);
	void SendCmsXpcVersionConfigInfo(void);
	void SendCmsLoginStatusRequest(void);

// Overrides
public:
	virtual BOOL InitInstance();
	virtual BOOL OnIdle(LONG lCount);

	DECLARE_MESSAGE_MAP()

// public class variables
public:
    CWnd mainWindow;
    CDispenserControl   dispenserControl;
    CGameCommControl    *pHeadGameCommControl;

    MemorySettingsIni       memory_settings_ini;
    MemoryDispenserConfig   memory_dispenser_config;
    MemoryLocationConfig    memory_location_config;
    ControllerUser          current_user;
    ControllerUser          current_shift_user;
	ManualDispenseData		manual_dispense_data;

	time_t customer_control_listening_time_check;
	time_t multi_controller_server_time_check;
	time_t customer_control_udp_time_check;
	time_t marker_printer_time_check;
	time_t game_lock_printer_time_check;
	time_t date_time_socket_check;
	time_t date_time_server_send_check;
	time_t date_time_server_receive_check;
	time_t last_idc_tx_message_sent_time;
	time_t last_stratus_tx_message_sent_time;

    CAsyncSocket    *pGameLockPrinterSocket;
    CAsyncSocket    *pMarkerPrinterSocket;
    CMyAsyncSocket	stratusSocket;
    CMyAsyncSocket	customerControlServerSocket;
	CMediaBoxAsyncSocket *pOneArmBanditHostSocket;
	CIDCAsyncSocket	IdcSocket;
	CCmsAsyncSocket	CmsSocket;

    CGbProManager   *pGbProManager;

    UINT current_dialog_return;
    CDialog *pCurrentDialog;
	CRasConnectionStatus *pRasConnectionStatus;

	CTechnicianDisplay		*pTechnicianDisplay;
	CDispenserDialog		*pDispenserDialog;
	CGameCollectionState	*pGameCollectionState;

    BYTE  sound_game_lock;
    bool  printer_busy;
    bool  exit_application;
    DWORD get_customer_id_selection;
	bool flash_active_home_light;
	bool marker_credit_reply;
	bool marker_message_recieved;
	bool idc_tx_message_ack_pending;
	bool cms_tx_message_ack_pending;
	bool stratus_tx_message_ack_pending;
	StratusMarkerCreditResponse stratus_marker_credit_response;

    DataDisplayStruct data_display;

	// variables for remote network connection
	NetworkSettingsRecord	*pNetworkSettingsRecord;
	bool network_connection_active;

	BagAndTagInfo bag_and_tag_info[512];

	int total_netwin_games;
	DWORD total_netwin_drop;
	DWORD total_cash_drop;
	DWORD total_netwin_handle;

	char computer_name[MAX_COMPUTERNAME_LENGTH + 1];

	bool fill_in_progress;

	bool cms_com_disconnected;

    // queues
    queue <DispenserCommand, deque<DispenserCommand> > dispenser_command_queue; // dispenser commands
    queue <DispenserCommand, deque<DispenserCommand> > dispenser_ui_queue;  // messages from dispenser to user interface
    queue <GameDataDisplayStruct, deque<GameDataDisplayStruct> > game_data_display_queue;
    queue <CString, deque<CString> > w2g_printer_queue;
    queue <CString, deque<CString> > general_printer_queue;
    queue <CString, deque<CString> > printer_interrupt_queue;
    queue <CString, deque<CString> > game_lock_printer_queue;   // pending game lock printer jobs
    queue <CString, deque<CString> > marker_printer_queue;      // pending Marker printer jobs
    queue <StratusMessageFormat, deque<StratusMessageFormat> > stratus_dialog_queue;    // queue for receiving messages from Stratus for dialog
    queue <EmailMessageFormat, deque<EmailMessageFormat> > email_queue; // hold pending emails for transmit
    queue <GameUnlockQueue, deque<GameUnlockQueue> > user_interface_game_unlock_queue;
	queue <EventMessage, deque<EventMessage> > event_message_queue;
    queue <int, deque<int> > message_window_queue;
    queue <OneArmBanditHostMessage, deque<OneArmBanditHostMessage> > one_arm_bandit_host_queue;
    queue <StratusMessageFormat, deque<StratusMessageFormat> > stratus_tx_queue;
    queue <StratusMessageFormat, deque<StratusMessageFormat> > marker_stratus_tx_queue;
	queue <IdcMessageFormat, deque<IdcMessageFormat> > idc_tx_queue;
	queue <CmsMessageFormat, deque<CmsMessageFormat> > cms_tx_queue;

// private class variables
private:
    CSelectCustomerIdDlg    *pSelectCustomerIdDlg;
    CMyAsyncSocket			dateTimeSocket;
    CAsyncSocket            customerControlUdpSocket;
    CMultiControllerServer  *pMultiControllerServer;
	CCustomerControlListen	customerControlListen;
    TcpIpRx                 stratus_tcp_ip_rx;
    TcpIpRx                 customer_control_server_rx;
    TcpIpRx					one_arm_bandit_host_receive;
    TcpIpRx                 idc_tcp_ip_rx;
    TcpIpRx                 cms_tcp_ip_rx;

	BYTE one_arm_bandit_host_message_id;
	bool one_arm_bandit_connected;

	// variables for remote network connection
	RASDIALPARAMS			*pRasDialParams;
	HRASCONN 				*pHandleRasConnection;

    time_t wide_area_award_request_time;
    time_t game_mux_message_update;
    time_t paid_tickets_transmit;
	time_t last_tx_event_message_time;
    time_t game_date_and_time_update;
	time_t last_marker_stratus_tx_time;

    HANDLE hPrinterSerialPort;
    HANDLE hMarkerPrinterSerialPort;

    // queues
    queue <DispenserUdpMessageFormat, deque<DispenserUdpMessageFormat> > dispenser_display_data_queue;
    queue <SocketMessage, deque<SocketMessage> > customer_control_server_tx_queue;

    ActiveHomeScriptLib::IActiveHome* pActiveHome;
	HRESULT hr;

	bool fetching_netwin_in_progress;

	IdcMessageFormat last_idc_tx_message;
	DWORD idc_tx_no_response_counter;
	DWORD idc_tx_nack_counter;

	CmsMessageFormat last_cms_tx_message;
	DWORD cms_tx_no_response_counter;
	DWORD cms_tx_nack_counter;

	bool cms_players_logged_in;

	WORD erroneous_ack_count;

	TotalPaidPerGame paid_per_game[MAX_GAMES];


// private class functions
private:
    void CreateDirectoryStructure();
    bool LoadFilesToMemory();
    BYTE AddGameDevice(MemoryGameConfig *pMemoryGameConfig);
	BYTE AddMuxGameDevice(MemoryGameConfig *pMemoryGameConfig);
	BYTE AddSasGameDevice(MemoryGameConfig *pMemoryGameConfig);
    void VerifyGameRedeemFile();
    void CheckDialogBoxes();
    bool RemoveGame(DWORD *pUcmcId);
    void CheckForGameUnlocks();
	void CheckGameControlConnections();
	void OnIdleOneSecondTimer();
	void OnIdleTwoSecondTimer();
	void OnIdleFiveSecondTimer();
	void initActiveHome (void);

    // multi controller functions
    void CheckMultiControllerServerSocket();
    void CheckMultiControllerClientReceive();
    void ProcessSlaveControllerReceiveMessage(CMultiControllerClient *socket, StratusMessageFormat *receive_message, WORD message_length);
    void ProcessSlaveSystemLogin(CMultiControllerClient *socket, SystemUser *user);
    void ProcessSlaveTicketQuery(CMultiControllerClient *socket, char *data);
    void ProcessSlaveGameLockQuery(CMultiControllerClient *socket);
    void ProcessSlaveTicketPaid(MemoryPaidTicket *memory_paid_ticket);

    // Stratus message functions
    void CheckStratusTxQueue();
    void CheckMarkerStratusTxQueue();
    void CheckStratusRxSocket();
    bool SendStratusMessage(BYTE *data, int length, bool controller_data, bool event_log_data);
    void ProcessDeviceMessage(StratusMessageFormat *device_message);
	void PurgePendingStratusTxMessage (StratusMessageFormat* stratus_message);
	void QueuePendingStratusTxMessages (void);
	
	// XP IDC functions
	void CheckIdcSocket();
	void CheckIdcRxMessages(void);

	void CheckIdcTxQueue(void);
	void SendIdcMessage(IdcMessageFormat* idc_message);
	void CheckIdcTxMessages();
	bool CheckIdcTxPendingMessages();
	bool sameIdcMessage(IdcMessageFormat* idc_message);
	void ProcessIdcRxMessage(void);
	bool sameIdcTxMessage(IdcMessageFormat* idc_message);
	bool SendIdcMessage(BYTE *data, int length, bool controller_data, bool event_log_data);
	void ProcessIdcRequestMachineSpecificData (IdcProtocolHeader *header);
	void ProcessIdcBonusAwardMessage (void);
	void ProcessIdcGbAdvantageWinMessage (void);
	void ProcessIdcGbaWinTextMessage (void);
//	void ProcessIdcExternalAwardedPoints (void);
	void ProcessIdcFillValues (void);
	void ProcessIdcDispenseInfoMessage (void);
	void ProcessIdcPrintBlobMessage (void);
	void ProcessIdcW2gInfoMessage (void);
	void ProcessIdcSetPromotionWinCategory (void);
	void QueueIdcTxMessage(IdcProtocolHeader *header, BYTE* data, WORD data_length, bool write_to_disk);
	void PurgeOtherIdcTxMeterMessages(IdcMessageFormat* new_idc_message);
	void OpenIdcSocket();
	BYTE CalculateMessageCrc(BYTE *buffer, BYTE size);
	void PurgePendingIdcTxMessage (IdcMessageFormat* idc_message);
	void PurgePendingIdcTxMessage (int record_index);
	bool GetPendingIdcTxMessage (IdcMessageFormat* idc_message);
	void QueuePendingIdcTxMessages (void);

	// XP CMS functions
	void CheckCmsSocket();
	void CheckCmsRxSocket(void);
	void CheckCmsTxQueue(void);
	bool sameCmsMessage(CmsMessageFormat* idc_message);
	void ProcessCmsRxMessage(void);
	void SendCmsAckMessage (CmsProtocolHeader *header, bool ack);
	bool sameCmsTxMessage(CmsMessageFormat* idc_message);
	bool SendCmsMessage(BYTE *data, int length, bool controller_data, bool event_log_data);
	void PurgePendingCmsTxMessage (CmsMessageFormat* cms_message);
	void QueuePendingCmsTxMessages (void);
	void LogoutAllCmsPlayers (void);

	void ProcessCmsLoginMessage (void);
	void ProcessCmsLogoutMessage (void);
	void ProcessCmsCriticalMessageAck (void);

	void QueueCmsTxMessage(CmsProtocolHeader *header, BYTE* data, WORD data_length, bool critical_message);
	void OpenCmsSocket();
	BYTE CalculateCmsMessageCrc(BYTE *buffer, BYTE size);

    // email functions
    void CheckSmtpSocket();
    void CloseSmtpSocket();
    void SendEmailFailedAddOrModifyLocation(bool add_flag, BYTE *buffer, int length);

    void GameMuxMessageUpdate();
    void PaidTicketsTransmit();

	void addPaidTicketsTotal (DWORD ucmc_id, DWORD ticket_amount);
	void clearPaidTicketsTotal (void);

    // Stratus connection checks
    void CheckStratusSocket();
    void CheckStratusDateTimeSocket();

    // printer functions
    void OpenPrinterCommPort();
    void CheckPrinterJobs();
	void CheckW2gPrinterJobs();
    void CheckGameLockPrinterSocket();
    void OpenMarkerPrinterPort();
    void CheckMarkerPrinterSocket();

    // Customer Control Socket Interface functions
    void CheckCustomerControlClientReceive();
    void CheckCustomerControlUdpSocket();
    void CheckCustomerControlListeningSocket();
    void ProcessCustomerControlClientReceive(CCustomerControlClient *socket, SocketMessage *message);
    void ProcessCcClientMemorySettingsRequest(CCustomerControlClient *socket, SocketMessage *message);
    void ProcessCcClientLocationSettingsRequest(CCustomerControlClient *socket, SocketMessage *message);
    void ProcessCcClientDispenserSettingsRequest(CCustomerControlClient *socket, SocketMessage *message);
    void ProcessCcClientGameRecordRequest(CCustomerControlClient *socket, SocketMessage *message);
    void ProcessCcClientMachineRxTxListRequest(CCustomerControlClient *socket);
    BYTE VerifyCcClientUserIdAndPassword(CCustomerControlClient *socket, BYTE *user_id_and_password);

    // Customer Control Server Interface functions
    void CheckCustomerControlServer();
    void CheckCustomerControlServerTxQueue();
	void CheckCustomerControlServerEventsTx();
    void CheckCustomerControlServerRx();
    void ProcessCustomerControlServerRx(SocketMessage *socket_message);
	void ProcessCustomerControlServerEventLoggingResponse();
    void SendFilesToCustomerControlServer();
    void SendControllerInfoToCcServer();

    // Lview functions
    void CheckDataDisplayTransmit();
    void SendLviewNetworkStats(CCustomerControlClient *socket);
    void LviewGetEventData(CCustomerControlClient *socket, time_t *start, time_t *stop);
    void LviewSendGameInfo(CCustomerControlClient *socket, bool location_setup);
    void SendMuxMessagesFile(CCustomerControlClient *socket);
};

extern CControllerApp theApp;

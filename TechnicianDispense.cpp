/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller	07/03/13	Save important Stratus TX messages to a file on disk and do not purge unless Stratus returns an ack.
Robert Fuller	05/02/13	After a tech manual dispense, add the amount dispensed in cents to the total tickets paid amount.
Robert Fuller	05/02/13	During manual dispense transactions, send the ucmc id of the game associated with the dispute to the IDC in the dispense info message.
Dave Elquist    07/15/05    Changed user level calculation.
Dave Elquist    06/14/05    Initial Revision.
*/


// TechnicianSystemUsers.cpp : implementation file
//
#define _CRT_RAND_S

#include "stdafx.h"
#include "xp_controller.h"
#include "TechnicianDispense.h"


// CTechnicianDispense dialog

IMPLEMENT_DYNAMIC(CTechnicianDispense, CDialog)
CTechnicianDispense::CTechnicianDispense(CWnd* pParent /*=NULL*/)
	: CDialog(CTechnicianDispense::IDD, pParent)
{
    current_edit_focus = 0;
    dispense_amount = 0;
	ucmc_id = 0;
}

CTechnicianDispense::~CTechnicianDispense()
{
}

BOOL CTechnicianDispense::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_dispense_event_type_combobox.AddString("SELECT A MANUAL DISPENSE CATEGORY!");
	m_dispense_event_type_combobox.AddString("Stolen Bill");
	m_dispense_event_type_combobox.AddString("Jackpot");
	m_dispense_event_type_combobox.AddString("Ticket not found");
	m_dispense_event_type_combobox.AddString("Game or System Failure");
	m_dispense_event_type_combobox.AddString("Ram clear with credits owed");
	m_dispense_event_type_combobox.AddString("Bingo card cleared");
	m_dispense_event_type_combobox.AddString("Stolen GB points");
	m_dispense_event_type_combobox.AddString("Ticket Already paid");
	m_dispense_event_type_combobox.AddString("Game not communicating");
	m_dispense_event_type_combobox.AddString("Customer Dispute");

/*
	if (!fileRead(NETWORK_SETTINGS, 0, &file_settings, sizeof(file_settings)))
		DestroyWindow();
	else
		m_dispense_event_type_combobox.SetCurSel(file_settings.settings.network_type);
*/

	m_dispense_event_type_combobox.SetCurSel(0);
    current_edit_focus = IDC_TECHNICIAN_DISPENSE_AMOUNT_EDIT;

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CTechnicianDispense::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TECHNICIAN_DISPENSE_DISPENSE_EVENT_TYPE_COMBO, m_dispense_event_type_combobox);
}


BEGIN_MESSAGE_MAP(CTechnicianDispense, CDialog)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USERS_EXIT_BUTTON, OnBnClickedTechnicianSystemUsersExitButton)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_TECHNICIAN_DISPENSE_BUTTON, OnBnClickedTechnicianDispenseButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_BUTTON1, OnBnClickedTechnicianSystemUserButton1)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_BUTTON2, OnBnClickedTechnicianSystemUserButton2)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_BUTTON3, OnBnClickedTechnicianSystemUserButton3)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_BUTTON4, OnBnClickedTechnicianSystemUserButton4)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_BUTTON5, OnBnClickedTechnicianSystemUserButton5)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_BUTTON6, OnBnClickedTechnicianSystemUserButton6)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_BUTTON7, OnBnClickedTechnicianSystemUserButton7)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_BUTTON8, OnBnClickedTechnicianSystemUserButton8)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_BUTTON9, OnBnClickedTechnicianSystemUserButton9)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_BUTTON0, OnBnClickedTechnicianSystemUserButton0)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_CLEAR_BUTTON, OnBnClickedTechnicianSystemUserClearButton)
	ON_EN_SETFOCUS(IDC_TECHNICIAN_DISPENSE_AMOUNT_EDIT, OnEnSetfocusTechnicianDispenseAmountEdit)
	ON_EN_SETFOCUS(IDC_TECHNICIAN_UCMC_ID_EDIT, OnEnSetfocusTechnicianUcmcIdEdit)
END_MESSAGE_MAP()


// CTechnicianDispense message handlers

void CTechnicianDispense::OnBnClickedTechnicianSystemUsersExitButton()
{
	DestroyWindow();
}

void CTechnicianDispense::OnMouseMove(UINT nFlags, CPoint point)
{
	theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

void CTechnicianDispense::OnBnClickedTechnicianDispenseButton()
{
    ManualDispenseMessageData manual_dispense_message_data;
    unsigned int ticket_number;
	int dispense_event_category;
    StratusMessageFormat stratus_msg;
    CString prt_buf;
	CString edit_string, error_string, message;
    bool over_daily_limit = FALSE;
    bool over_transaction_limit = FALSE;
    FileDropTicket drop_ticket;
    FileGameRedeemRecord redeem_record;
    FilePaidTicket file_paid_ticket;
    int record_index;
    time_t current_time2 = time(NULL);
    struct tm current_time_struct;
    FileCashierShift file_cashier_shift;
    DWORD dispense_amount_cents = dispense_amount * 100;

	dispense_event_category = m_dispense_event_type_combobox.GetCurSel();


    if (dispense_amount >= theApp.memory_dispenser_config.manual_dispense_per_transaction_limit)
        over_transaction_limit = TRUE;

	CTime current_time = CTime::GetCurrentTime();
    
    if ((theApp.manual_dispense_data.month != current_time.GetMonth()) ||
        (theApp.manual_dispense_data.day != current_time.GetDay()) ||
        (theApp.manual_dispense_data.year != current_time.GetYear()))
    {
        // initialize the manual dispense data for the new day
        theApp.manual_dispense_data.month = current_time.GetMonth();
        theApp.manual_dispense_data.day = current_time.GetDay();
        theApp.manual_dispense_data.year = current_time.GetYear();
        theApp.manual_dispense_data.dispense_amount = 0;
    }

    // check to see if dispense amount will be over the daily limit
    if ((theApp.manual_dispense_data.dispense_amount + dispense_amount) > theApp.memory_dispenser_config.manual_dispense_daily_limit)
        over_daily_limit = TRUE;

    if (!over_transaction_limit && !over_daily_limit && dispense_amount && dispense_event_category && theApp.GetGamePointer(ucmc_id))
    {
        // generate a ticket number
        rand_s(&ticket_number);

        theApp.PrinterHeader(0);
        theApp.PrinterData("\n\n MANUAL DISPENSE\n\n");
        theApp.PrinterId();

        prt_buf.Format(" UCMC ID:    %u\n", ucmc_id);
        theApp.PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format(" TICKET:    %u\n", ticket_number);
        theApp.PrinterData(prt_buf.GetBuffer(0));
        // dispense money if needed
        if (dispense_amount_cents <= theApp.dispenserControl.memory_dispenser_values.dispense_grand_total -
            theApp.dispenserControl.memory_dispenser_values.tickets_amount)
        {
            if (!theApp.memory_settings_ini.do_not_print_dispense_info)
            {
                prt_buf.Format("BILL DISPENSE NOT REQUIRED\nCASH PAYOUT $: %u.00\n", dispense_amount);
                theApp.PrinterData(prt_buf.GetBuffer(0));

            }

            theApp.dispenserControl.memory_dispenser_values.tickets_amount += dispense_amount_cents;
            theApp.dispenserControl.memory_dispenser_values.number_tickets++;
            theApp.dispenserControl.WriteDispenserFile();
        }
        else
        {
            prt_buf.Format(" DISPENSE AMOUNT:    $ %u\n", dispense_amount);
            theApp.PrinterData(prt_buf.GetBuffer(0));
            if (theApp.dispenserControl.DispenseBills(ticket_number, dispense_amount_cents, ucmc_id, 0))
                theApp.SendDispenserBillCount(false, ucmc_id);
        }

        prt_buf.Format(" LOGIN ID:     %u\n", theApp.current_user.user.id);
        theApp.PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format(" DISPENSE TYPE:     %u\n", dispense_event_category);
        theApp.PrinterData(prt_buf.GetBuffer(0));

        if (!theApp.memory_settings_ini.do_not_print_dispense_info && (theApp.memory_dispenser_config.dispenser_type != DISPENSER_TYPE_CASH_DRAWER))
        {
            prt_buf.Format("BREAKAGE: %9.2f\n", (double)(theApp.dispenserControl.memory_dispenser_values.dispense_grand_total -
				theApp.dispenserControl.memory_dispenser_values.tickets_amount) / 100);
            theApp.PrinterData(prt_buf.GetBuffer(0));
        }

	    prt_buf.Format("\n\n\nLocation Signature1: \n\n");
        theApp.PrinterData(prt_buf.GetBuffer(0));
	    prt_buf.Format("\n_______________________________\n\n");
        theApp.PrinterData(prt_buf.GetBuffer(0));

	    prt_buf.Format("\n\n\nLocation Signature2: \n\n");
        theApp.PrinterData(prt_buf.GetBuffer(0));
	    prt_buf.Format("\n_______________________________\n\n");
        theApp.PrinterData(prt_buf.GetBuffer(0));

        theApp.PrinterData("\n\n\n\n\n\n\n\n\n\n\n");


        theApp.printer_busy = false;

        // add the manual dispense to the shift tickets paid
        if (theApp.memory_settings_ini.force_cashier_shifts)
        {
            prt_buf.Format("%s", SHIFT_TICKETS_1);

            fileWrite(prt_buf.GetBuffer(0), -1, &ticket_number, 4);
            fileWrite(prt_buf.GetBuffer(0), -1, &dispense_amount_cents, 4);

            // check file size, shut off if not being used
            if (fileRecordTotal(prt_buf.GetBuffer(0), 8) > 1000)
            {
                theApp.memory_settings_ini.force_cashier_shifts = 0;
                fileWrite(MAIN_SETTINGS_INI, 0, &theApp.memory_settings_ini, sizeof(theApp.memory_settings_ini));
                DeleteFile(prt_buf.GetBuffer(0));
            }

            prt_buf = CASHIER_SHIFT_1;

            if (!fileRead(prt_buf.GetBuffer(0), 0, &file_cashier_shift, sizeof(file_cashier_shift)))
                memset(&file_cashier_shift, 0, sizeof(file_cashier_shift));

            file_cashier_shift.redeem_amount += dispense_amount_cents;
            file_cashier_shift.number_tickets++;
            fileWrite(prt_buf.GetBuffer(0), 0, &file_cashier_shift, sizeof(file_cashier_shift));

            if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_CASH_DRAWER)
            {
                prt_buf.Format(" CASH DRAWER: $%7.2f\n", ((double)file_cashier_shift.beginning_balance -
                    (double)file_cashier_shift.redeem_amount + (double)file_cashier_shift.active_shift_fills +
                    (double)file_cashier_shift.mgr_adjust_plus - (double)file_cashier_shift.mgr_adjust_minus) / 100);
                theApp.PrinterData(prt_buf.GetBuffer(0));
            }
        }

        // add the dispense event in the drop ticket log and the game redeem records
        memset(&drop_ticket, 0, sizeof(drop_ticket));
        drop_ticket.ticket_id = ticket_number;
        drop_ticket.ucmc_id = ucmc_id;
        drop_ticket.amount = dispense_amount_cents;

        drop_ticket.customer_id = theApp.memory_location_config.customer_id;

        writeRedundantFile((CString)DROP_TICKETS, -1, (BYTE*)&drop_ticket, sizeof(drop_ticket));

        record_index = fileGetRecord(GAME_REDEEM_RECORDS, &redeem_record, sizeof(redeem_record), ucmc_id);
        if (record_index >= 0)
        {
            redeem_record.amount += dispense_amount_cents;
            writeRedundantFile((CString)GAME_REDEEM_RECORDS, record_index, (BYTE*)&redeem_record, sizeof(redeem_record));
        }
        else
        {
            memset(&redeem_record, 0, sizeof(redeem_record));
            redeem_record.ucmc_id = ucmc_id;
            redeem_record.amount = dispense_amount_cents;
            writeRedundantFile((CString)GAME_REDEEM_RECORDS, -1, (BYTE*)&redeem_record, sizeof(redeem_record));
        }
        
        memset(&file_paid_ticket, 0, sizeof(file_paid_ticket));

        file_paid_ticket.memory.cashier_id = theApp.current_user.user.id;
        file_paid_ticket.memory.timestamp = current_time2;

        file_paid_ticket.memory.memory_game_lock.ticket_id = ticket_number;
        file_paid_ticket.memory.memory_game_lock.ticket_amount = dispense_amount_cents;
        // get time stamp info
        localtime_s(&current_time_struct, &current_time2);
        file_paid_ticket.memory.memory_game_lock.time_stamp[0] = current_time_struct.tm_hour;
        file_paid_ticket.memory.memory_game_lock.time_stamp[1] = current_time_struct.tm_min;
        file_paid_ticket.memory.memory_game_lock.time_stamp[2] = current_time_struct.tm_sec;
        file_paid_ticket.memory.memory_game_lock.time_stamp[3] = current_time_struct.tm_mday;
        file_paid_ticket.memory.memory_game_lock.time_stamp[4] = current_time_struct.tm_mon;
        file_paid_ticket.memory.memory_game_lock.time_stamp[5] = current_time_struct.tm_year;

        file_paid_ticket.memory.memory_game_lock.ucmc_id = ucmc_id;
        file_paid_ticket.memory.memory_game_lock.location_id = theApp.memory_location_config.property_id;

        writeRedundantFile((CString)PAID_TICKETS, -1, (BYTE*)&file_paid_ticket, sizeof(file_paid_ticket));
//        fileWrite(PAID_TICKETS_TX, -1, &file_paid_ticket, sizeof(file_paid_ticket));
        fileWriteSizeLimited(DATED_PAID_TICKETS, &file_paid_ticket, sizeof(file_paid_ticket));

        // send the manual dispense event to the stratus
        manual_dispense_message_data.ticket_id = (DWORD)ticket_number;
        manual_dispense_message_data.amount = dispense_amount_cents;
        manual_dispense_message_data.authorization_id = (DWORD)theApp.current_user.user.id;
        manual_dispense_message_data.idc_dispense_event = 0;
        manual_dispense_message_data.dispense_event_category = dispense_event_category;
        manual_dispense_message_data.todays_dispense_amount = theApp.manual_dispense_data.dispense_amount;
        manual_dispense_message_data.year = theApp.manual_dispense_data.year;
        manual_dispense_message_data.month = theApp.manual_dispense_data.month;
        manual_dispense_message_data.day = theApp.manual_dispense_data.day;
        manual_dispense_message_data.daily_limit = theApp.memory_dispenser_config.manual_dispense_daily_limit;
        manual_dispense_message_data.transaction_limit = theApp.memory_dispenser_config.manual_dispense_per_transaction_limit;



//        theApp.SendIdcManualDispenseInfoMessage (&manual_dispense_message_data);

        // send manual dispense event to the Stratus
        memset(&stratus_msg, 0, sizeof(stratus_msg));
		stratus_msg.club_code = theApp.memory_location_config.club_code;
        stratus_msg.send_code = TKT_MANUAL_DISPENSE;
        stratus_msg.task_code = TKT_TASK_TICKET;
        stratus_msg.ucmc_id = ucmc_id;
        stratus_msg.message_length = sizeof(ManualDispenseMessageData) + STRATUS_HEADER_SIZE;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.serial_port = 1;
        stratus_msg.data_to_event_log = true;
        stratus_msg.customer_id = theApp.memory_location_config.customer_id;;

        endian4(manual_dispense_message_data.ticket_id);
		endian4(manual_dispense_message_data.amount);
        endian4(manual_dispense_message_data.authorization_id);
        endian4(manual_dispense_message_data.idc_dispense_event);
        endian4(manual_dispense_message_data.dispense_event_category);
        endian4(manual_dispense_message_data.todays_dispense_amount);
        endian4(manual_dispense_message_data.year);
        endian4(manual_dispense_message_data.month);
        endian4(manual_dispense_message_data.day);
        endian4(manual_dispense_message_data.daily_limit);
        endian4(manual_dispense_message_data.transaction_limit);

        memcpy(stratus_msg.data, &manual_dispense_message_data, sizeof(ManualDispenseMessageData));

        theApp.QueueStratusTxMessage(&stratus_msg, FALSE);

        // update the manual dispense data on disk
        theApp.manual_dispense_data.dispense_amount += dispense_amount;
        if (!fileWrite(MANUAL_DISPENSE_DAILY_LIMIT, 0, &theApp.manual_dispense_data, sizeof(theApp.manual_dispense_data)))
        {
            message.Format("CTechnicianDispense::OnBnClickedTechnicianDispenseButton() - Error writing %s.", MANUAL_DISPENSE_DAILY_LIMIT);
            logMessage(message);
        }

		dispense_amount = 0;
		ucmc_id = 0;
		m_dispense_event_type_combobox.SetCurSel(0);
		PostMessage(WM_CLOSE);
    }
	else
	{
		error_string = "";
		if (!dispense_amount)
		{
			error_string = "Enter Dispense Amount.";
		}


		if (over_transaction_limit)
		{
            message.Format("\n$%u is over the manual dispense transaction limit of $%u",
                           dispense_amount, 
                           theApp.memory_dispenser_config.manual_dispense_per_transaction_limit);

			error_string += message;
		}

		if (over_daily_limit)
		{
            message.Format("\n$%u + today's dispense amount of $%u is over the daily dispense limit of $%u",
                           dispense_amount, 
                           theApp.manual_dispense_data.dispense_amount, 
                           theApp.memory_dispenser_config.manual_dispense_daily_limit);

			error_string += message;
		}

		if (!dispense_event_category)
		{
			error_string += "\nSelect Dispense Category.";
		}
		if (!theApp.GetGamePointer(ucmc_id))
		{
			error_string += "\nInvalid UCMC ID.";
		}

		EnableWindow(FALSE);
		MessageWindow(true, "ADDITIONAL INFORMATION NEEDED!", error_string);

		while (theApp.message_window_queue.empty())
			theApp.OnIdle(0);

		EnableWindow();


	}
}

void CTechnicianDispense::OnBnClickedTechnicianSystemUserClearButton()
{
    if (current_edit_focus == IDC_TECHNICIAN_DISPENSE_AMOUNT_EDIT)
    {
        dispense_amount = 0;
        SetDlgItemText(IDC_TECHNICIAN_DISPENSE_AMOUNT_EDIT, "");
    }
    if (current_edit_focus == IDC_TECHNICIAN_UCMC_ID_EDIT)
    {
        ucmc_id = 0;
        SetDlgItemText(IDC_TECHNICIAN_UCMC_ID_EDIT, "");
    }
}


void CTechnicianDispense::ProcessButtonClick(BYTE button_press)
{
 char *pNameString, *pDialogNameString, button_char;
 int iii;
 static DWORD last_button_press_timer;
 CString edit_string;

    if (current_edit_focus == IDC_TECHNICIAN_DISPENSE_AMOUNT_EDIT)
    {
//        if (dispense_amount * 10 + button_press <= theApp.memory_dispenser_config.manual_dispense_per_transaction_limit)
        {
            dispense_amount = dispense_amount * 10 + button_press;
            edit_string.Format("%u", dispense_amount);
            SetDlgItemText(IDC_TECHNICIAN_DISPENSE_AMOUNT_EDIT, edit_string);
        }
    }
    else if (current_edit_focus == IDC_TECHNICIAN_UCMC_ID_EDIT)
    {
        if (ucmc_id * 10 + button_press <= 999999)
        {
            ucmc_id = ucmc_id * 10 + button_press;
            edit_string.Format("%u", ucmc_id);
            SetDlgItemText(IDC_TECHNICIAN_UCMC_ID_EDIT, edit_string);
        }
    }

    last_button_press_timer = GetTickCount() + LAST_BUTTON_PRESS_TIMEOUT;
//    SetDlgItemText(current_edit_focus, pDialogNameString);
}

void CTechnicianDispense::OnBnClickedTechnicianSystemUserButton1()
{
    ProcessButtonClick(1);
}

void CTechnicianDispense::OnBnClickedTechnicianSystemUserButton2()
{
    ProcessButtonClick(2);
}

void CTechnicianDispense::OnBnClickedTechnicianSystemUserButton3()
{
    ProcessButtonClick(3);
}

void CTechnicianDispense::OnBnClickedTechnicianSystemUserButton4()
{
    ProcessButtonClick(4);
}

void CTechnicianDispense::OnBnClickedTechnicianSystemUserButton5()
{
    ProcessButtonClick(5);
}

void CTechnicianDispense::OnBnClickedTechnicianSystemUserButton6()
{
    ProcessButtonClick(6);
}

void CTechnicianDispense::OnBnClickedTechnicianSystemUserButton7()
{
    ProcessButtonClick(7);
}

void CTechnicianDispense::OnBnClickedTechnicianSystemUserButton8()
{
    ProcessButtonClick(8);
}

void CTechnicianDispense::OnBnClickedTechnicianSystemUserButton9()
{
    ProcessButtonClick(9);
}

void CTechnicianDispense::OnBnClickedTechnicianSystemUserButton0()
{
    ProcessButtonClick(0);
}


void CTechnicianDispense::OnEnSetfocusTechnicianDispenseAmountEdit()
{
    current_edit_focus = IDC_TECHNICIAN_DISPENSE_AMOUNT_EDIT;
}

void CTechnicianDispense::OnEnSetfocusTechnicianUcmcIdEdit()
{
    current_edit_focus = IDC_TECHNICIAN_UCMC_ID_EDIT;
}

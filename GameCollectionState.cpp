// GameCollectionState.cpp : implementation file
//
/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   03/08/13    Removed obsolete Location Collection code.
Robert Fuller   09/17/12    Make sure we are ignoring GB Interface devices when traversing through the game device linked list.
Robert Fuller   12/30/11    Make sure we are fetching SAS game pointers for a particular UCMC id when forcing, clearing, and displaying games.
Robert Fuller   08/03/11    Added code to support Location Collection features.
Robert Fuller   06/08/11    Added mods to support GB with the XP/SAS direct connect feature.
Robert Fuller   08/09/10    Added mods to support the XP/SAS direct connect feature.
Robert Fuller   08/27/09    Set all the games to the cancel collection state when the "cancel all" button is depressed.
Robert Fuller   04/08/09    Save game meter snapshot in the force processors.
Robert Fuller   11/06/08    Added mechanism to start, force, and cancel all the games simultaneously.
*/

#include "stdafx.h"
#include "xp_controller.h"
#include "GameCollectionState.h"
#include ".\gamecollectionstate.h"


// CGameCollectionState dialog

IMPLEMENT_DYNAMIC(CGameCollectionState, CDialog)
CGameCollectionState::CGameCollectionState(CWnd* pParent /*=NULL*/)
	: CDialog(CGameCollectionState::IDD, pParent)
{
	destroy_dialog = false;
}

CGameCollectionState::CGameCollectionState(bool manager_entry, CWnd* pParent /*=NULL*/)
	: CDialog(CGameCollectionState::IDD, pParent)
{
	destroy_dialog = false;
}

CGameCollectionState::~CGameCollectionState()
{
	destroy_dialog = true;
}

void CGameCollectionState::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FORCE_COLLECTION_GAME_LIST, m_game_listbox);
	DDX_Control(pDX, IDC_FORCE_COLLECTION_START_BUTTON, m_start_collection_button);
	DDX_Control(pDX, IDC_FORCE_COLLECTION_FORCE_BUTTON, m_force_collection_button);
	DDX_Control(pDX, IDC_FORCE_COLLECTION_CANCEL_BUTTON, m_cancel_collection_button);
}


BEGIN_MESSAGE_MAP(CGameCollectionState, CDialog)
	ON_BN_CLICKED(IDC_FORCE_COLLECTION_EXIT_BUTTON, OnBnClickedForceCollectionExitButton)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_FORCE_COLLECTION_PREVIOUS_GAME_BUTTON, OnBnClickedForceCollectionPreviousGameButton)
	ON_BN_CLICKED(IDC_FORCE_COLLECTION_NEXT_GAME_BUTTON, OnBnClickedForceCollectionNextGameButton)
	ON_BN_CLICKED(IDC_FORCE_COLLECTION_REFRESH_BUTTON, OnBnClickedForceCollectionRefreshButton)
	ON_BN_CLICKED(IDC_FORCE_COLLECTION_START_BUTTON, OnBnClickedForceCollectionStartButton)
	ON_BN_CLICKED(IDC_FORCE_COLLECTION_FORCE_BUTTON, OnBnClickedForceCollectionForceButton)
	ON_BN_CLICKED(IDC_FORCE_COLLECTION_CANCEL_BUTTON, OnBnClickedForceCollectionCancelButton)
	ON_BN_CLICKED(IDC_START_ALL_BUTTON, OnBnClickedStartAllButton)
	ON_BN_CLICKED(IDC_FORCE_ALL_BUTTON, OnBnClickedForceAllButton)
	ON_BN_CLICKED(IDC_CANCEL_ALL_BUTTON, OnBnClickedCancelAllButton)
END_MESSAGE_MAP()


// CGameCollectionState message handlers

void CGameCollectionState::OnBnClickedForceCollectionExitButton()
{
	destroy_dialog = true;
}

BOOL CGameCollectionState::OnInitDialog()
{
	CDialog::OnInitDialog();

    pDisplayGameDevice = NULL;
    m_game_listbox.SetHorizontalExtent(3000);
    list_game_info_initialized = FALSE;
	OnBnClickedForceCollectionRefreshButton();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CGameCollectionState::OnMouseMove(UINT nFlags, CPoint point)
{
	theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

void CGameCollectionState::OnBnClickedForceCollectionPreviousGameButton()
{
 int ucmc_id;
 int mux_id;
 int sas_id;
 CString edit_string;
 CGameDevice *pGameDevice, *pPreviousGameDevice;
 CGameCommControl *pGameCommControl;

    pDisplayGameDevice = NULL;

	GetDlgItemText(IDC_FORCE_COLLECTION_UCMC_ID_EDIT, edit_string);
	ucmc_id = atoi(edit_string);

	GetDlgItemText(IDC_FORCE_COLLECTION_MUX_ID_EDIT, edit_string);
	mux_id = atoi(edit_string);

	GetDlgItemText(IDC_FORCE_COLLECTION_SAS_ID_EDIT, edit_string);
	sas_id = atoi(edit_string);

	if (ucmc_id > 0)
	{
		pPreviousGameDevice = NULL;
		pGameCommControl = theApp.pHeadGameCommControl;

		while (pGameCommControl && !pDisplayGameDevice)
		{
			pGameDevice = pGameCommControl->pHeadGameDevice;

			while (pGameDevice && !pDisplayGameDevice)
			{
//				if (pGameDevice->memory_game_config.ucmc_id == ucmc_id)
				if ((pGameDevice->memory_game_config.ucmc_id == ucmc_id) &&
					(pGameDevice->memory_game_config.mux_id == mux_id) &&
					(pGameDevice->memory_game_config.sas_id == sas_id))
				{
					if (pPreviousGameDevice)
						pDisplayGameDevice = pPreviousGameDevice;
					else
						pDisplayGameDevice = pGameDevice;
				}
				else
				{
					pPreviousGameDevice = pGameDevice;
					pGameDevice = pGameDevice->pNextGame;
				}
			}

			pGameCommControl = pGameCommControl->pNextGameCommControl;
		}
	}

    if (pDisplayGameDevice)
    {
        ListGameInformation(pDisplayGameDevice);
        if (pDisplayGameDevice->memory_game_config.mux_id && (theApp.GetSasGamePointer(pDisplayGameDevice->memory_game_config.ucmc_id)))
            OnBnClickedForceCollectionPreviousGameButton();
    }


}

void CGameCollectionState::OnBnClickedForceCollectionNextGameButton()
{
 bool game_found;
 int ucmc_id;
 int mux_id;
 int sas_id;
 CString edit_string;
 CGameDevice *pGameDevice;
 CGameCommControl *pGameCommControl;

    pDisplayGameDevice = NULL;

	GetDlgItemText(IDC_FORCE_COLLECTION_UCMC_ID_EDIT, edit_string);
	ucmc_id = atoi(edit_string);

	GetDlgItemText(IDC_FORCE_COLLECTION_MUX_ID_EDIT, edit_string);
	mux_id = atoi(edit_string);

	GetDlgItemText(IDC_FORCE_COLLECTION_SAS_ID_EDIT, edit_string);
	sas_id = atoi(edit_string);

	pGameDevice = theApp.GetSasGamePointer((DWORD)ucmc_id);
	if (!pGameDevice)
		pGameDevice = theApp.GetGamePointer((DWORD)ucmc_id);

    if (pGameDevice)
    {

        pDisplayGameDevice = getNextNonGbInterfaceGameDevice(pGameDevice);

        if (pDisplayGameDevice)
        {
            ListGameInformation(pDisplayGameDevice);
        }
    }

}

void CGameCollectionState::ListGameInformation(CGameDevice *pDisplayGameDevice)
{
 CString display_string;
 CGameDevice *pGameDevice, *pSasGameDevice;
 CGameCommControl *pGameCommControl;
 BYTE  collection_state; // CLEAR=0, STARTED=1, ARMED=2, FORCE=3, CANCEL=4

	m_start_collection_button.ShowWindow(SW_HIDE);
	m_force_collection_button.ShowWindow(SW_HIDE);
	m_cancel_collection_button.ShowWindow(SW_HIDE);

	SetDlgItemText(IDC_FORCE_COLLECTION_MUX_ID_EDIT, "");
	SetDlgItemText(IDC_FORCE_COLLECTION_SAS_ID_EDIT, "");
	SetDlgItemText(IDC_FORCE_COLLECTION_PORT_ID_EDIT, "");
	SetDlgItemText(IDC_FORCE_COLLECTION_UCMC_ID_EDIT, "");
	SetDlgItemText(IDC_FORCE_COLLECTION_STATE_EDIT, "");

	if (!pDisplayGameDevice)
	{
		pGameCommControl = theApp.pHeadGameCommControl;

		while (pGameCommControl)
		{
			pGameDevice = pGameCommControl->pHeadGameDevice;
			if (pGameDevice)
			{
				pDisplayGameDevice = pGameDevice;
				pGameCommControl = NULL;
			}
			else
				pGameCommControl = pGameCommControl->pNextGameCommControl;
		}
	}

	if (!pDisplayGameDevice)
	{
		m_game_listbox.ResetContent();
        m_game_listbox.AddString("NO GAMES FOUND");
	}
	else
	{
        pSasGameDevice = theApp.GetSasGamePointer((DWORD)pDisplayGameDevice->memory_game_config.ucmc_id);

        if (!pSasGameDevice)
    		display_string.Format("%u", pDisplayGameDevice->memory_game_config.mux_id);
        else
    		display_string.Format("%u", 0);
		SetDlgItemText(IDC_FORCE_COLLECTION_MUX_ID_EDIT, display_string);

        if (pSasGameDevice)
    		display_string.Format("%u", pSasGameDevice->memory_game_config.sas_id);
        else
    		display_string.Format("%u", 0);
		SetDlgItemText(IDC_FORCE_COLLECTION_SAS_ID_EDIT, display_string);

		display_string.Format("%u", pDisplayGameDevice->memory_game_config.port_number);
		SetDlgItemText(IDC_FORCE_COLLECTION_PORT_ID_EDIT, display_string);

		display_string.Format("%u", pDisplayGameDevice->memory_game_config.ucmc_id);
		SetDlgItemText(IDC_FORCE_COLLECTION_UCMC_ID_EDIT, display_string);

        if (pSasGameDevice)
            collection_state = pSasGameDevice->memory_game_config.collection_state;
        else
            collection_state = pDisplayGameDevice->memory_game_config.collection_state;

		switch (collection_state)
		{
			case CLEAR_COLLECTION_STATE:
				SetDlgItemText(IDC_FORCE_COLLECTION_STATE_EDIT, "CLEARED");
				m_start_collection_button.ShowWindow(SW_SHOW);
				break;

			case START_COLLECTION_STATE:
				SetDlgItemText(IDC_FORCE_COLLECTION_STATE_EDIT, "STARTED");
				m_cancel_collection_button.ShowWindow(SW_SHOW);
				break;

			case ARMED_COLLECTION_STATE:
				SetDlgItemText(IDC_FORCE_COLLECTION_STATE_EDIT, "ARMED");
				m_force_collection_button.ShowWindow(SW_SHOW);
				m_cancel_collection_button.ShowWindow(SW_SHOW);
				break;

			case FORCE_COLLECTION_STATE:
				SetDlgItemText(IDC_FORCE_COLLECTION_STATE_EDIT, "FORCED");
				break;

			case CANCEL_COLLECTION_STATE:
				SetDlgItemText(IDC_FORCE_COLLECTION_STATE_EDIT, "CANCELED");
				break;

			default:
				SetDlgItemText(IDC_FORCE_COLLECTION_STATE_EDIT, "ERROR");
				break;
		}
	}
}

void CGameCollectionState::OnBnClickedForceCollectionRefreshButton()
{
 CString display_string;
 CGameDevice *pGameDevice;
 CGameCommControl *pGameCommControl = theApp.pHeadGameCommControl;
 BYTE sas_id;

	m_game_listbox.ResetContent();
//	ListGameInformation(NULL);

	while (pGameCommControl)
	{
		pGameDevice = pGameCommControl->pHeadGameDevice;

		while (pGameDevice)
		{
            if (!(pGameDevice->memory_game_config.mux_id && theApp.GetSasGamePointer(pGameDevice->memory_game_config.ucmc_id)))
//            if ( (pGameDevice->memory_game_config.mux_id && !theApp.GetSasGamePointer(pGameDevice->memory_game_config.ucmc_id)) ||
//                pGameDevice->memory_game_config.sas_id)
            {

			    display_string.Format("%u: %u %u (%u)\t", pGameDevice->memory_game_config.port_number,
				    pGameDevice->memory_game_config.mux_id, pGameDevice->memory_game_config.sas_id, pGameDevice->memory_game_config.ucmc_id);

                if (!list_game_info_initialized)
                {
                    list_game_info_initialized = TRUE;
                    pDisplayGameDevice = theApp.GetSasGamePointer(pGameDevice->memory_game_config.ucmc_id);
                    ListGameInformation(pDisplayGameDevice);
                }

			    switch (pGameDevice->memory_game_config.collection_state)
			    {
				    case CLEAR_COLLECTION_STATE:
					    display_string += "CLEARED";
					    break;

				    case START_COLLECTION_STATE:
					    display_string += "\tSTARTED";
					    break;

				    case ARMED_COLLECTION_STATE:
					    display_string += "\tARMED";
					    break;

				    case FORCE_COLLECTION_STATE:
					    display_string += "\tFORCED";
					    break;

				    case CANCEL_COLLECTION_STATE:
					    display_string += "\tCANCELED";
					    break;

				    default:
					    display_string += "\tERROR";
					    break;
			    }

			    m_game_listbox.AddString(display_string);
            }

			pGameDevice = pGameDevice->pNextGame;
		}

		pGameCommControl = pGameCommControl->pNextGameCommControl;
	}
}

void CGameCollectionState::OnBnClickedForceCollectionStartButton()
{
 int ucmc_id;
 CString edit_string;
 CGameDevice *pGameDevice = 0;
 CGameCommControl* pGameCommControl = 0;

	GetDlgItemText(IDC_FORCE_COLLECTION_UCMC_ID_EDIT, edit_string);
	ucmc_id = atoi(edit_string);

	pGameDevice = theApp.GetSasGamePointer((DWORD)ucmc_id);

    if (!pGameDevice)
    {
    	pGameDevice = theApp.GetGamePointer((DWORD)ucmc_id);
        pGameCommControl = theApp.GetGameCommControlPointer((DWORD)ucmc_id);
    }
    else
        pGameCommControl = theApp.GetSasGameCommControlPointer((DWORD)ucmc_id);

	if (pGameDevice)
    {
		pGameDevice->SetGameCollectionState(START_COLLECTION_STATE);
        if (pGameDevice->memory_game_config.sas_id)
            pGameDevice->SetGameCollectionState(ARMED_COLLECTION_STATE);
//        pGameCommControl = theApp.GetGameCommControlPointer((DWORD)ucmc_id);
        
        if (pGameCommControl && !pGameDevice->memory_game_config.mux_id && pGameCommControl->sas_com)
            pGameCommControl->sas_com->non_volatile_ram.game_status |= COLLECT_APPROVED;
    }
}

void CGameCollectionState::OnBnClickedForceCollectionForceButton()
{
 int ucmc_id;
 CString edit_string;
 CGameDevice *pGameDevice = 0;
 CGameCommControl* pGameCommControl = 0;

	GetDlgItemText(IDC_FORCE_COLLECTION_UCMC_ID_EDIT, edit_string);
	ucmc_id = atoi(edit_string);

	pGameDevice = theApp.GetSasGamePointer((DWORD)ucmc_id);

    if (!pGameDevice)
    {
    	pGameDevice = theApp.GetGamePointer((DWORD)ucmc_id);
        pGameCommControl = theApp.GetGameCommControlPointer((DWORD)ucmc_id);
    }
    else
        pGameCommControl = theApp.GetSasGameCommControlPointer((DWORD)ucmc_id);

	if (pGameDevice)
	{
        if (pGameDevice->memory_game_config.mux_id)
        {
		    pGameDevice->SetGameCollectionState(FORCE_COLLECTION_STATE);
            pGameDevice->SaveGameMeterDropSnapshot();
        }
        else if (pGameCommControl && pGameCommControl->sas_com)
        {
            if(pGameCommControl->GetGameCollectionState() == ARMED_COLLECTION_STATE)
            {
                pGameCommControl->SetGameCollectionState(CLEAR_COLLECTION_STATE);
                pGameCommControl->processGameMeters(MMI_GAME_COLLECTED_METERS);
                pGameDevice->SaveGameMeterDropSnapshot();
            }
        }
	}
}

void CGameCollectionState::OnBnClickedForceCollectionCancelButton()
{
 int ucmc_id;
 CString edit_string;
 CGameDevice *pGameDevice;
 CGameCommControl* pGameCommControl;

	GetDlgItemText(IDC_FORCE_COLLECTION_UCMC_ID_EDIT, edit_string);
	ucmc_id = atoi(edit_string);

	pGameDevice = theApp.GetSasGamePointer((DWORD)ucmc_id);

    if (!pGameDevice)
    {
    	pGameDevice = theApp.GetGamePointer((DWORD)ucmc_id);
        pGameCommControl = theApp.GetGameCommControlPointer((DWORD)ucmc_id);
    }
    else
        pGameCommControl = theApp.GetSasGameCommControlPointer((DWORD)ucmc_id);

	if (pGameDevice)
    {
        if (pGameDevice->memory_game_config.sas_id)
            pGameDevice->SetGameCollectionState(CLEAR_COLLECTION_STATE);
        else
            pGameDevice->SetGameCollectionState(CANCEL_COLLECTION_STATE);

//        pGameCommControl = theApp.GetGameCommControlPointer((DWORD)ucmc_id);
        if (!pGameDevice->memory_game_config.mux_id && pGameCommControl && pGameCommControl->sas_com)
        {
            pGameCommControl->sas_com->non_volatile_ram.game_status &= ~COLLECT_APPROVED;
            pGameCommControl->sas_com->non_volatile_ram.game_status &= ~COLLECT_IN_PROG;
        }
    }
}

void CGameCollectionState::OnBnClickedStartAllButton()
{
    CGameCommControl *pGameCommControl = theApp.pHeadGameCommControl;
    CGameDevice *pGameDevice = 0;

    while (pGameCommControl)
    {
        pGameDevice = pGameCommControl->pHeadGameDevice;

        if (pGameDevice && !pGameDevice->memory_game_config.mux_id && pGameCommControl->sas_com)
            pGameCommControl->sas_com->non_volatile_ram.game_status |= COLLECT_APPROVED;

        while (pGameDevice)
        {
    		pGameDevice->SetGameCollectionState(START_COLLECTION_STATE);
            if (pGameDevice->memory_game_config.sas_id)
                pGameDevice->SetGameCollectionState(ARMED_COLLECTION_STATE);
            pGameDevice = pGameDevice->pNextGame;
        }
        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }
	m_game_listbox.ResetContent();
}

void CGameCollectionState::OnBnClickedForceAllButton()
{
    CGameCommControl *pGameCommControl = theApp.pHeadGameCommControl;
    CGameDevice *pGameDevice = 0;

    while (pGameCommControl)
    {
        pGameDevice = pGameCommControl->pHeadGameDevice;

        while (pGameDevice)
        {
            if (pGameDevice->memory_game_config.mux_id)
            {
                if (!theApp.GetSasGamePointer(pGameDevice->memory_game_config.ucmc_id))
                {
	                pGameDevice->SetGameCollectionState(FORCE_COLLECTION_STATE);
                    pGameDevice->SaveGameMeterDropSnapshot();
                }
            }
            else if (pGameCommControl->sas_com)
            {
                if(pGameCommControl->GetGameCollectionState() == ARMED_COLLECTION_STATE)
                {
                    pGameCommControl->SetGameCollectionState(CLEAR_COLLECTION_STATE);
                    pGameCommControl->processGameMeters(MMI_GAME_COLLECTED_METERS);
                    pGameDevice->SaveGameMeterDropSnapshot();
                }
            }

            pGameDevice = pGameDevice->pNextGame;
        }

        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }
	m_game_listbox.ResetContent();
}

void CGameCollectionState::OnBnClickedCancelAllButton()
{
    CGameCommControl *pGameCommControl = theApp.pHeadGameCommControl;
    CGameDevice *pGameDevice = 0;

    while (pGameCommControl)
    {
        pGameDevice = pGameCommControl->pHeadGameDevice;

        if (pGameDevice && !pGameDevice->memory_game_config.mux_id && pGameCommControl->sas_com)
        {
            pGameCommControl->sas_com->non_volatile_ram.game_status &= ~COLLECT_APPROVED;
            pGameCommControl->sas_com->non_volatile_ram.game_status &= ~COLLECT_IN_PROG;
        }

        while (pGameDevice)
        {

            if (pGameDevice->memory_game_config.sas_id)
                pGameDevice->SetGameCollectionState(CLEAR_COLLECTION_STATE);
            else
                pGameDevice->SetGameCollectionState(CANCEL_COLLECTION_STATE);

            pGameDevice = pGameDevice->pNextGame;
        }
        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }
	m_game_listbox.ResetContent();
}

CGameDevice* CGameCollectionState::getNextNonGbInterfaceGameDevice(CGameDevice *pGameDevice)
{
    CGameDevice *pNextGameDevice = NULL;
    CGameCommControl *pGameCommControl = NULL;
    CGameCommControl *pInitialGameCommControl = NULL;
    DWORD ucmc_id;

	if (pGameDevice)
	{
        ucmc_id = pGameDevice->memory_game_config.ucmc_id;

        pGameCommControl = theApp.GetSasGameCommControlPointer(ucmc_id);
        if (!pGameCommControl)
            pGameCommControl = theApp.GetGameCommControlPointer(ucmc_id);

		pInitialGameCommControl = pGameCommControl;
		while (pGameCommControl && !pNextGameDevice)
		{
            if (!pGameDevice)
    			pGameDevice = pGameCommControl->pHeadGameDevice;

			while (pGameDevice)
			{
                // exclude GB Interface game devices
                if (!pNextGameDevice &&
					(pGameDevice->memory_game_config.ucmc_id != ucmc_id) &&
					(pGameDevice != theApp.GetGbInterfaceGamePointer(pGameDevice->memory_game_config.ucmc_id)))
                {
                    pNextGameDevice = pGameDevice;
                    break;
                }
                else
                    pGameDevice = pGameDevice->pNextGame;

			}

            if (!pNextGameDevice)
			{
    			pGameCommControl = theApp.GetNextGameCommControlPointer(pGameCommControl);
				if (pGameCommControl == pInitialGameCommControl)
					pGameCommControl = 0;
			}
		}
	}

    return (pNextGameDevice);
}

/*
Robert Fuller	07/29/14	Check for a 4ok win in a hard lockup handpay scenario before logging the player out of GB.
Robert Fuller	05/15/14	Increment handle and games when players are logged into GB and are playing poker games on the U1 manufactured games.
Robert Fuller	12/05/13	Added code to send IDC the value of the credit meter when it is fetched in real time via the SAS current credits message. 
Robert Fuller	12/05/13	Flag paytable ids that start with SBJ as being blackjack personality types.
Robert Fuller	12/05/13	Added code to send IDC the bill denomination when a bill is accepted in the gaming machine. 
Robert Fuller	12/05/13	DCB ports are now assigned thru customer control directly by serial number
Robert Fuller	12/05/13	Fine tuned the game delay to the version of IGT game software. Reduced the message delay from 40ms to 10ms.
Robert Fuller   05/02/13    Moved all global BCD conversion routines into the SAS communications object where other threads accessing the same functions and data cannot corrupt the end result.
Robert Fuller   03/08/13    Added code to support the configurable dispenser payout limit..
Robert Fuller   03/08/13    Do not manually adjust total in and total out meters in Game Start and End to eliminate meter wobble..
Robert Fuller   03/08/13    Fixed the real time clock update logic.
Robert Fuller   03/08/13    Implemented the configurable dispenser payout limit.
Robert Fuller	09/17/12	Send stacker pull and game entered messages to the IDC
Robert Fuller	09/17/12	Send game meters every ten seconds a couple of times after game play is idle.
Robert Fuller	06/20/12	Fetch the game meters when the stacker is pulled.
Robert Fuller   06/01/12    Call low level FTDI chip device driver function to determine the number of current serial ports
                            available, and make sure the sas id of the corresponding game is greater than or equal to the
							number of available ports before sending or receiving messages.
Robert Fuller   03/08/12    Send update message to GBI when a bill is put on the game after playing down to zero credits.
Robert Fuller   03/08/12    Send the game meter values for Direct Connect games every 5 games to get GB Trax to update properly.
Robert Fuller   03/05/12    Send the new primary and secondary 4ok bingo awards in the winning card message.
Robert Fuller   03/05/12    Send meters after credits go to zero or after a cashout, and one more time a minute later to trigger Stratus to set the
                            inactivity flag.
Robert Fuller   01/27/12    Added code to send the GB Initialization data to the GB Interface after a 4ok winning hand event.
Robert Fuller   01/27/12    Do NOT send a GB session update message before a winning card message to the stratus.
Robert Fuller   12/30/11    Give GB players who are logged in, and playing game types besides poker and blackjack double the GB points for their play.
Robert Fuller   10/03/11    Print the sas id address as opposed to the irrelevant mux id address for SAS direct connect games.
Robert Fuller   06/08/11    Added mods to support GB with the XP/SAS direct connect feature.
Robert Fuller   04/28/11    Reset the FTDI device instead of closing and opening the port to fix comm bug.
Robert Fuller   04/26/11    If we don't receive SAS data within 30 secs, close and reopen port.
Robert Fuller   04/26/11    Added code to log SAS communication events.
Robert Fuller   01/25/11    Tuned up the SAS communication by eliminated the message processing thread, and moving the processing code directly
                            into the serial receive task.
Robert Fuller   01/26/11    Request the bill in meter information when we request the general meter data.
Robert Fuller   11/10/10    Make sure that the taxable event flag only gets set for jackpot events.
Robert Fuller   11/10/10    Fill the ticket add structure with the information from the game lock file when the game lock structure is loaded from disk.

Robert Fuller   11/10/10    Request meters once every 120 seconds during idle activity, every 10 seconds during game play.
Robert Fuller   10/07/10    Added code to recover from a USB to RS232 disconnect.

Robert Fuller   Mar 22, 2004    Made sure that middle of the game bets get accounted for in the denom meters and gb points.
Robert Fuller   Mar 22, 2004    Added the games played since logon check to previous change.
Robert Fuller	Apr 08, 2004	Added total jackpot to the total atten meter.
Robert Fuller	Apr 13, 2004	Added logic to track current machine bank and to start the logout timer.
Robert Fuller	Apr 20, 2004	Start the logout timer if there are zero credits when processing a current credits message.
Robert Fuller	Jul 06, 2004	Added logic to determine a taxable event and set the w2g_pay flag.
Robert Fuller	Jul 08, 2004	No longer immediately log off players after receiving cashout events.
                                Now we start the logout timer, and log player off after 30 seconds.
Robert Fuller	Jul 09, 2004	Corrected logic in the handpay message that incorrectly set the handpay amount.
Robert Fuller	Jul 12, 2004	No longer change gui state to get logon after cashouts. The log off timer does this now.
Robert Fuller	Aug 04, 2004	Send card data info even if player is not logged on.
Robert Fuller	Sep 30, 2004	Doubled queue size. Get enabled games at init.
Robert Fuller   Nov 02, 2004    Removed card data events.
Robert Fuller   Nov 10, 2004    Made sure to divide the legacy bonus amount by accounting denomination.
Robert Fuller	Nov 24, 2004	Added support for the new locked cashout message and reset w2gs to credit meter.
Robert Fuller	Nov 26, 2004	Fixed problem with cashouts not generating game locks. Commented out test code associated with
                                the new locked cashout message. Fixed the dequeue card data routine.
Robert Fuller	Jan 03, 2005    Check for additional queued handpays when clearing game locks.
Robert Fuller   Feb 10, 2005    Make sure the system allows the jackpot to credit feature before initiating sequence.
Robert Fuller   Feb 10, 2005    Added autopay_enable flag check in the handay processor.
Robert Fuller   Feb 10, 2005    Rearranged handpay info processor logic.
Robert Fuller   Feb 23, 2005    Increased the game delay between games to insure that I get valid card data.
Robert Fuller   Mar 01, 2005    When I process a meter data message, update the NVRAM variable if it is different from the game data.
Robert Fuller   Mar 07, 2005    Make sure we are sending the SEND_ENABLED_GAME_NUMBERS message only for SAS version 6.
Robert Fuller   Mar 18, 2005    Queue up a game delay message if we are playing poker when processing a game start message.
Robert Fuller   Mar 21, 2005    Added code for the game delay check timer.

Robert Fuller   May 12, 2005    Do not send the jackpot message when the jackpot goes to the credit meter.
Robert Fuller   May 12, 2005    Non-progressive handpays come across as credits so we need to multiply by accounting denom.
Robert Fuller   May 18, 2005    Made adjustments to meters sent by game to account for accounting denom > 1 cent.
Robert Fuller   Jul 14, 2005    Added eMarker related changes.
Robert Fuller   Sep 19, 2005    Adjusted current_bank_meter to take into account machine accounting denomination.
Robert Fuller   Sep 20, 2005    Set the current_bank_meter to zero during handpay situations.
Robert Fuller   Sep 20, 2005    Reduced the IGT game delay to 750ms.
Robert Fuller   Sep 20, 2005    Reset the marker_advanced_this_session flag when player plays down to zero credits
Robert Fuller   Dec 06, 2005    Made sure to not advance number of card blocks variable past max array value.
Robert Fuller   Jan 03, 2006    Only queue up hand data when there is a winning hand.
Robert Fuller   Jan 24, 2006    Need to register AFT if SAS version is 6 or greater.
Robert Fuller   Jan 26, 2006    Added SAS 6 or greater flag.
Robert Fuller   Feb 02, 2006    Removed reference to ee_deno_type.
Robert Fuller   Feb 15, 2006    Send GB reset message when a the game powers up.
Robert Fuller   Apr 25, 2006    Save manufacture id to nonvolatile ram. Don't send AFT registration messages to Aristocrat games.
Robert Fuller   Aug 02, 2006    Send jackpot messages before the logout message to accomidate the GB PRO functionality.
Robert Fuller   Oct 03, 2006    Added flag to keep track of meter messages from game after downloads.
Robert Fuller   Sep 29, 2006    Added Cash Frenzy support.
Robert Fuller   Oct 03, 2006    Added flag to keep track of meter messages from game after downloads.
Robert Fuller   Oct 11, 2006    Copy latest frenzy broadcast strings and attributes at beginning of each game.
Robert Fuller   Oct 13, 2006    Show cash frenzy congrats screen only after receiving verification message from system.
Robert Fuller   Nov 02, 2006    When the game starts, zero out the frenzy congrats strings.
Robert Fuller   Nov 07, 2006    Made sure that the SAS version is greater than 6 before setting the current denomination in game starts.
Robert Fuller   Nov 07, 2006    Set the current denomination when I get a game configuration response message.
Robert Fuller   Nov 10, 2006    Get handpay info from game during initialization.
Robert Fuller   Nov 20, 2006    Only set the current game denom in the game start message with a valid wager type.
Robert Fuller   May 23, 2007    Added mods to support non-MarkerTrax functionality.
Robert Fuller   Jul 05, 2007    Added functions and mods to support Timbers club code 4.
Robert Fuller   Dec 14, 2007    When a game is started call new function to update the bingo card award values.
Robert Fuller   Feb 12, 2008    Only clear the marker advanced this session flag at end of game with zero credits.
Robert Fuller   Feb 19, 2008    Do not send the winning card message until after the congrats screen is displayed.
Robert Fuller   Feb 26, 2008    Moved the current bank meter variable to nonvolitile ram.
Robert Fuller   May 05, 2008    Make sure there is no marker advance pending before allowing a GB logout.
Robert Fuller   May 05, 2008    Request the current credits on game starts and game ends so we can keep an accurate account of bank meter.
Robert Fuller   Nov 26, 2008    Reduced ths sas poll delay from 200ms to 40ms.
Robert Fuller   Nov 26, 2008    Prioritized the polling of the card info message to help insure a timely response.
Robert Fuller   Nov 26, 2008    Subtracted the WAT IN meter value from the game drop meters to keep Stratus accounting consistent.
Robert Fuller	Jan 22, 2009	Increased the size of the bet element in the card info block structure. Send new message command for this.
Robert Fuller	Jan 27, 2009	Added precaution to eliminate queueing up two game locks for the same cashout, thus causing double dispenses.
Robert Fuller	Feb 16, 2009	Send the 4ok win hand code directly to the event processor.
Robert Fuller	Feb 16, 2009	Tuned up SAS communications performance (boosted comm thread priority, do not queue up messages already in the queue,
                                do not send long polls if there is RTE activity on the game, check all message response message sizes before processing)
Robert Fuller	Feb 16, 2009	Send a second type of ack to dequeue the handpay info on the game if the first ack fails (potential Bally bug).
Robert Fuller	Feb 16, 2009    Make sure we hand pay lockup on the game and do not send game locks over $8K to controller if autopay is on / j2credit off.
Robert Fuller   Feb 19, 2009    Added code to AFT GB point redemptions if the game supports SAS version 6.01 or greater.
Robert Fuller   Feb 25, 2009    Make sure to kill the zero credits timer if we determine that there are current credits on the gaming machine.
Robert Fuller   Feb 26, 2009    Start the zero credits log out timer when detecting zero credits even if the last game was a win.
Robert Fuller   Mar 06, 2009    Made sure we do not send taxable events to both the credit meter and pay back the markertrax account.
Robert Fuller   Mar 25, 2009    Send a queued long poll when encountering a game non-response event to assure game gets configured for RTE. 
Robert Fuller   Oct 20, 2009    Get the correct currently selected game number before queueing up a selected game meter data request.
Robert Fuller   Nov 20, 2009    Introduced a delay between the message ack and the next SAS poll so the U1 game does not miss messages.
Robert Fuller   Jan 06, 2010    Only send game delay commnands to the game when it is not being played.
Robert Fuller   Jan 22, 2010    Only allow players to access MarkerTrax accounts if the SAS version is greater than 6.0.
Robert Fuller   Feb 03, 2010    Put in processors for game busy responses.
Robert Fuller   Mar 19, 2010    Check to see if remote handpay reset is available before sending command to the game.
Robert Fuller   Apr 05, 2010    Only send a remote handpay reset message to the game 3 times, and then remove from the transmit queue.
Robert Fuller   Apr 14, 2010    Do not allow marker advances and GB point redemptions when the game is locked up in a handpay.

*/

#define _CRT_RAND_S

#include <stdlib.h>

#include "stdafx.h"
//#include "afxwin.h"
//#include <commctrl.h>
#include "Winuser.h"
#include "resource.h"
//#include <windows.h>
//#include "Error_Handler.h"
//#include "utility.h"




#ifndef SAS_COMM_H
#include "SAS_Comm.h"
#endif

#ifndef GAMECOMM_H
#include "gamecomm.h"
#endif

//external functions
extern void ReportCommError (LPTSTR lpszMessage);

extern bool frenzy_win_on_any_win;
extern HANDLE card_data_message_semiphore;


UINT SEND_METERS_SECOND_COUNT = 30;
UINT SECONDS_TO_SEND_METERS_GAME_PLAY = 10;

UINT SEND_METERS_MSEC_COUNT_IDLE = 120;
UINT SEND_METERS_MSEC_COUNT_GAME_PLAY = 10;

UINT SEND_SAS_COMMUNICATIONS_STATUS_SEC_COUNT = 45;

UINT ZERO_CREDIT_LOGOUT_TIME_MSECS = 30;


UINT MAX_SAS_NO_RECEIVE_SECS = 30;

// constants
#define LSB_MASK                 0x01
const UINT MAX_AFT_PENDING_SECONDS = 5;

#define BINGO_AWARD_FACTOR1		1200		// nickel to quarter denomination
#define BINGO_AWARD_FACTOR2		600		// quarter to dollar denomination

#define NICKEL_BINGO_AWARD 		6000
#define NICKEL 					   5
#define QUARTER_BINGO_AWARD 	   15000
#define QUARTER 				      25
#define FIFTY_CENT_BINGO_AWARD 	30000
#define FIFTY_CENT 				   50
#define DOLLAR_BINGO_AWARD 		60000
#define ONEDOLLAR 					100
#define MAX_BRACKET 			      8
#define MAX_BINGO_GAMES 		   60000
#define DOLLAR_BINGO_AWARD 		60000
#define MAX_HANDLE 				   500
#define WHOLEPOINT 				   1000

#define MAX_FRENZY_AWARD 		   99999
#define MAX_HANDLE					500

#define ENABLE_DISABLE_EVENT_REPORTING          0x0E
#define REQUEST_GAME_METER_DATA                 0x0f
#define DOOR_OPEN_EVENT                         0x11
#define DOOR_CLOSED_EVENT                       0x12
#define DROP_DOOR_OPEN_EVENT                    0x13
#define DROP_DOOR_CLOSED_EVENT                  0x14
#define AC_POWER_APPLIED_EVENT                  0x17
#define AC_POWER_LOST_EVENT                     0x18
#define CASHBOX_DOOR_OPEN_EVENT                 0x19
#define CASHBOX_DOOR_CLOSED_EVENT               0x1a
#define SEND_CURRENT_CREDITS                    0x1a
#define CASHBOX_REMOVED                         0x1b
#define SEND_HANDPAY_INFO                       0x1b
#define CASHBOX_INSTALLED                       0x1c
#define REQUEST_BILL_METER_DATA                 0x1e
#define REQUEST_MACHINE_INFO                    0x1f
#define SEND_TOTAL_HAND_PAID_CANCELLED_CREDITS  0x2d
#define GAME_DELAY                              0x2e
#define SEND_SELECTED_GAME_METERS               0x2f
#define BILL_ACCEPTED                           0x4f
#define HANDPAY_PENDING                         0x51
#define HANDPAY_RESET_KEYSWITCH                 0x52
#define SEND_GAME_CONFIGURATION                 0x53
#define SEND_SAS_VERSION                        0x54
#define SEND_SELECTED_GAME_NUMBER               0x55
#define CASHOUT_BUTTON_PRESSED                  0x66
#define AFT_TRANSFER_COMPLETE                   0x69
#define GAME_LOCKED                             0x6f
#define TRANSFER_FUNDS                          0x72
#define AFT_REGISTRATION                       0x73
#define AFT_STATUS                              0x74
#define SYSTEM_BONUS_PAY                        0x7c
#define GAME_START                              0x7e
#define GAME_END                                0x7f
#define UPDATE_DATE_AND_TIME                    0x7f
#define REEL_N_STOPPED                          0x88
#define CREDIT_WAGERED                          0x89
#define GAME_RECALL_ENTERED                     0x8a
#define INITIATE_LEGACY_BONUS                   0x8a
#define CARD_HELD_NOT_HELD                      0x8b
#define GAME_SELECTED                           0x8c
#define SEND_CARD_INFO                          0x8e
#define HANDPAY_RESET                           0x94
#define ENABLE_JACKPOT_HANDPAY_RESET_METHOD     0xA8

// MULTI-DENOM PREAMBLE
#define MULTI_DENOM_PREAMBLE                    0xB0

// MULTI-DENOM BASE COMMANDS
#define SEND_ENABLED_GAME_NUMBERS               0x56



#define EVENT_ID_INDEX                          1
#define EXCEPTION_CODE_INDEX                    2
#define COMMAND_CODE_INDEX                      1

#define SINGLE_METER_ACCOUNTING                 '?'
//#define MAX_NO_RESPONSE_COUNT                   5
#define MAX_NO_RESPONSE_COUNT                   10
//#define MAX_POLL_CYLE_COUNT                     70
#define MAX_POLL_CYLE_COUNT                     700
#define MINIMUM_EVENT_RESPONSE                  5
#define MINIMUM_RPOLL_RESPONSE                  4
#define MINIMUM_SPOLL_RESPONSE                  1
#define MINIMUM_DPOLL_RESPONSE                  11
#define MINIMUM_MPOLL_RESPONSE                  1

#define MIN_FRENZY_BET 			                5

#define  FOUR_ACES_WIN_CODE      0x03010000     /* 4 ACES */      /* default bingo HANDS as defined on */
#define  FOUR_DEUCES_WIN_CODE    0x03020000     /* 4 DUECES */    /* BINGO HAND ENCODING SPEC. by RJS */
#define  FOUR_THREES_WIN_CODE    0x03030000     /* 4 THREES */
#define  FOUR_FOURS_WIN_CODE     0x03040000     /* ect. */
#define  FOUR_FIVES_WIN_CODE     0x03050000
#define  FOUR_SIXES_WIN_CODE     0x03060000
#define  FOUR_SEVENS_WIN_CODE    0x03070000
#define  FOUR_EIGHTS_WIN_CODE    0x03080000
#define  FOUR_NINES_WIN_CODE     0x03090000
#define  FOUR_TENS_WIN_CODE      0x030A0000
#define  FOUR_JACKS_WIN_CODE     0x030B0000
#define  FOUR_QUEENS_WIN_CODE    0x030C0000
#define  FOUR_KINGS_WIN_CODE     0x030D0000

static const unsigned long ACE_MASK     = 0x00000001;
static const unsigned long TWO_MASK     = 0x00000002;
static const unsigned long THREE_MASK   = 0x00000004;
static const unsigned long FOUR_MASK    = 0x00000008;
static const unsigned long FIVE_MASK    = 0x00000010;
static const unsigned long SIX_MASK     = 0x00000020;
static const unsigned long SEVEN_MASK   = 0x00000040;
static const unsigned long EIGHT_MASK   = 0x00000080;
static const unsigned long NINE_MASK    = 0x00000100;
static const unsigned long TEN_MASK     = 0x00000200;
static const unsigned long JACK_MASK    = 0x00000400;
static const unsigned long QUEEN_MASK   = 0x00000800;
static const unsigned long KING_MASK    = 0x00001000;

#define 	PRIMARY_CARD	    1
#define     SECONDARY_CARD      2
#define 	WINNING_CARD		3

static const ULONG MAX_METER_REQUEST_COUNT = 200;

static const unsigned char NUMBER_OF_GAME_PLAYS_TO_CHECK = 3;

static const ULONG DEFAULT_GAME_DELAY_IN_MSECS = 200;

static const ULONG DEFAULT_MESSAGE_DELAY_IN_MSECS = 40;
static const ULONG IGT_GK_MESSAGE_DELAY_IN_MSECS = 10;

static const ULONG GAME_DELAY_CHECK_TIMER_VALUE = 60000;

static const ULONG SEND_HANDPAY_INFO_DELAY_IN_MSECS = 2000;

static const char* single_hand_bases[] = {(char*)"M00549", (char*)"M00499"};

CString comm_rx_event("CommRxEvent");

CWinThread* SAS_serial_process_task_handle = NULL;

static UINT serialReceiveTask(LPVOID pMainComm);

static const UINT SEND_GB_SESSION_UPDATE_NUMBER_OF_GAMES = 10;


// communication function prototypes and variables
//BOOL closeSASPort(void);
//static BOOL waitForCommEvent (HANDLE hFile);
//static void sendSerialMessage (unsigned char* tx_msg, UINT len);

//////////////////////////////////////////////////////////////////////////
// Communications functions

CSAS_Comm::CSAS_Comm(LPVOID pGameCommControl, BYTE sas_address)
{
    pCGameCommControl = (CGameCommControl*)pGameCommControl;
    sas_id = sas_address;
    poll_SAS = FALSE;
    check_game_meters = FALSE;
    poll_cyle_count = 0;
    poll_count = 0;
    no_response_count = 0;
    total_queued_messages = 0;
    total_queued_aft_messages = 0;
    real_time_event_reporting_enabled = FALSE;
    single_hand_pokers_in_base_chip = FALSE;
    poker_game_being_played = FALSE;
    bj_game_being_played = FALSE;
    first_meter_fetch = TRUE;
    first_game_play = FALSE;
    aft_pending = FALSE;
    transaction_number = '1';
    cumulative_drink_comp_dollars = 0;
    bonus_jackpot_award = 0;
    machine_info_recieved = FALSE;
    last_handpay_win_amount = 0;
    game_delay_check_timer_expired = TRUE;
    handpay_pending = FALSE;
    handpay_info_received = TRUE;
    handpay_reset_sends = 0;
    current_game_wager = 0;
    delay_next_sas_poll = FALSE;
    handpay_reset_sends = 0;
    pending_card_info_message = FALSE;
    serial_recieve_task_active = FALSE;
    real_time_event_activity = FALSE;
    last_meter_check_time = time(NULL);
    last_sas_communication_check_time = time(NULL);
    last_sas_poll_received_time = time(NULL);
    last_zero_credit_logout_check_time = time(NULL);
    games_played_since_last_meter_send = 0;
    check_game_delay_delta = FALSE;

    meter_check_msec_count = SEND_METERS_MSEC_COUNT_IDLE;
    idle_meter_sends = 0;
    sas_communicating = FALSE;

    game_delay_in_msecs = DEFAULT_GAME_DELAY_IN_MSECS;
    message_delay_in_msecs = DEFAULT_MESSAGE_DELAY_IN_MSECS;

    clearSasMessage (&current_tx_message);
    current_tx_message.poll_type = G_POLL;
    m_hmodule = 0;
}

CSAS_Comm::~CSAS_Comm()
{
    closeSASPort();
}

BOOL CSAS_Comm::openSASPort (BYTE usb_rs232_port, char* serial_number)
{
    FT_STATUS status;
    CString error_message;


    sas_id = usb_rs232_port;

    if (!m_hmodule)
    {
        Loadem();

        if (!strlen(serial_number))
            status = Open(sas_id - 1);
        else
            status = OpenEx(serial_number);

		if (status != FT_OK)
	    {
            error_message.Format("CSAS_Comm::openSASPort() - can't open port %d serial # %s - error %u.", usb_rs232_port, serial_number, status);
            logMessage(error_message);

		    return FALSE;
	    }

        status = SetTimeouts(50, 50);

        status = SetDataCharacteristics(m_ftHandle, FT_BITS_8, FT_STOP_BITS_1, FT_PARITY_ODD);
        status = SetBaudRate(m_ftHandle, FT_BAUD_19200);
    }

    hRxCommEvent = CreateEvent(NULL, 
                          false, // auto-reset event 
                          false, // non-signalled state 
                           (LPCSTR)&comm_rx_event);
    EventMask = FT_EVENT_RXCHAR; 

	stop_comm_thread = false;

	// now need to create the thread that will be reading the comms port
    SAS_serial_process_task_handle = AfxBeginThread(serialReceiveTask, this);
    SAS_serial_process_task_handle->SetThreadPriority(THREAD_PRIORITY_TIME_CRITICAL);
                                                
	if(SAS_serial_process_task_handle == NULL)	
	{
        logMessage("CSAS_Comm::openSASPort() - error creating thread.");
		closeSASPort();
		return FALSE;
	}
//	else
//	{
//		CloseHandle(SAS_serial_process_task_handle);
//	}

    non_volatile_ram.sas_six_or_greater = FALSE; // initialize this to zero

	return TRUE;
}

// Stops the thread that's reading the comms. port, and then closes the
// comms port.
BOOL CSAS_Comm::closeSASPort(void)
{
   CString error_message;
   UINT nExitCode;
   FT_STATUS status;
    
    if (m_ftHandle)
    {
        stop_comm_thread = TRUE;
        sas_communicating = FALSE;
        Sleep(100);
        status = Purge(FT_PURGE_RX | FT_PURGE_TX);
        if (status != FT_OK)
        {
            error_message = "CSAS_Comm::closeSASPort() - can't purge port\n";
            logMessage(error_message);
        }


        CloseHandle(hRxCommEvent);

        status = Close();
        m_ftHandle = 0;
        if (status != FT_OK)
        {
            error_message = "CSAS_Comm::closeSASPort() - can't close port\n";
            logMessage(error_message);
        }

        FreeLibrary(m_hmodule);
        m_hmodule = 0;

    }

	return TRUE;
}

// Thread function to read communications port. This thread is responsible
// for getting incoming serial data.
//static DWORD WINAPI serialReceiveTask (LPVOID lpParam)
UINT serialReceiveTask(LPVOID pMainComm)
{
    CSAS_Comm *pCSAS_Comm = (CSAS_Comm*)pMainComm;
    UINT exit_code;


    pCSAS_Comm->serial_recieve_task_active = TRUE;

    pCSAS_Comm->ftStatus = pCSAS_Comm->SetEventNotification(pCSAS_Comm->m_ftHandle, pCSAS_Comm->EventMask, pCSAS_Comm->hRxCommEvent); 

    pCSAS_Comm->non_volatile_ram.current_game_denomination = 25;

	while (pCSAS_Comm && !pCSAS_Comm->stop_comm_thread)
	{
		// clear receive buffer in preparation for next incoming message
		memset(pCSAS_Comm->receive_buffer, 0, MAX_SAS_MSG_LENGTH);
		pCSAS_Comm->receive_buffer_index = 0;
		// wait for the first batch of incoming characters
		pCSAS_Comm->ftStatus = pCSAS_Comm->SetEventNotification(pCSAS_Comm->m_ftHandle, pCSAS_Comm->EventMask, pCSAS_Comm->hRxCommEvent); 
		if (WaitForSingleObject(pCSAS_Comm->hRxCommEvent, 50))
		{
			if (!pCSAS_Comm->stop_comm_thread)
			{
				pCSAS_Comm->GetQueueStatus(&pCSAS_Comm->bytes_in_rx_queue);
				if (pCSAS_Comm->bytes_in_rx_queue)
				{
					pCSAS_Comm->receive_buffer_index += pCSAS_Comm->getSerialData (&pCSAS_Comm->receive_buffer[pCSAS_Comm->receive_buffer_index]);

					switch (pCSAS_Comm->current_tx_message.poll_type)
					{
						case GENERAL_POLL:
							pCSAS_Comm->getGeneralPollResponse();
							break;
						case R_POLL:
							pCSAS_Comm->getRPollResponse();
							break;
						case S_POLL:
							pCSAS_Comm->getSPollResponse();
							break;
						case M_POLL:
							pCSAS_Comm->getMPollResponse();
							break;
					}
				}

			}
		}

		pCSAS_Comm->poll_SAS = TRUE;
		pCSAS_Comm->processSASMessages();
    }

    pCSAS_Comm->serial_recieve_task_active = FALSE;

    AfxEndThread(exit_code);

	return 0;
}

void CSAS_Comm::getGeneralPollResponse (void)
{
    WORD discarded_bytes = 0;

    if (real_time_event_reporting_enabled)
    {
        // the first byte of the received message needs to be the address of the game = 1
		while (receive_buffer[0] != sas_id)
		{
            memset(receive_buffer, 0, MAX_SAS_MSG_LENGTH);
            ftStatus = SetEventNotification(m_ftHandle, EventMask, hRxCommEvent); 
            if (WaitForSingleObject(hRxCommEvent, 50))
                if (stop_comm_thread)
                    return;
                else
                    receive_buffer_index = getSerialData (&receive_buffer[0]);
 	    }


        // get all the bytes
        while ((receive_buffer_index < MINIMUM_EVENT_RESPONSE) &&
               (receive_buffer_index < getEventMessageSize(receive_buffer[EXCEPTION_CODE_INDEX])))
        {
//    		if (waitForCommEvent (SAS_port_handle))
            ftStatus = SetEventNotification(m_ftHandle, EventMask, hRxCommEvent); 
            if (WaitForSingleObject(hRxCommEvent, 50))
                if (stop_comm_thread)
                    return;
                else
                    receive_buffer_index += getSerialData (&receive_buffer[receive_buffer_index]);
            Sleep (1);
        }
    }
    else
        clearSasMessage (&current_tx_message);


//    if (WaitForSingleObject(rx_message_semiphore, MAX_EVENT_WAIT_TIMOUT) != WAIT_TIMEOUT)
    {
        memcpy(response_message.message, receive_buffer, receive_buffer_index);
        response_message.message_size = receive_buffer_index;
        response_message.message_ready = TRUE;
//        ReleaseSemaphore(rx_message_semiphore, 1, NULL);
    }
}

void CSAS_Comm::getRPollResponse (void)
{
	while ((receive_buffer[0] != sas_id) && receive_buffer[0])
	{
        memset(receive_buffer, 0, MAX_SAS_MSG_LENGTH);
        ftStatus = SetEventNotification(m_ftHandle, EventMask, hRxCommEvent); 
        if (WaitForSingleObject(hRxCommEvent, 50))
            if (stop_comm_thread)
                return;
            else
                receive_buffer_index = getSerialData (&receive_buffer[0]);
 	}

    // get all the bytes
    while ((receive_buffer_index <= MINIMUM_RPOLL_RESPONSE) &&
           (receive_buffer_index < getRPollMessageSize()))
    {
//		if (waitForCommEvent (SAS_port_handle))
        ftStatus = SetEventNotification(m_ftHandle, EventMask, hRxCommEvent); 
        if (WaitForSingleObject(hRxCommEvent, 50))
            if (stop_comm_thread)
                return;
            else
                receive_buffer_index += getSerialData (&receive_buffer[receive_buffer_index]);
        Sleep (1);
    }
//    WaitForSingleObject(rx_message_semiphore, MAX_EVENT_WAIT_TIMOUT);
    memcpy(response_message.message, receive_buffer, receive_buffer_index);
    response_message.message_ready = TRUE;
    response_message.message_size = receive_buffer_index;
//    ReleaseSemaphore(rx_message_semiphore, 1, NULL);
}

void CSAS_Comm::getSPollResponse (void)
{
// the first byte of the received message needs to be the address of the game = 1
while ((receive_buffer[0] != sas_id) && receive_buffer[0])
{
    memset(receive_buffer, 0, MAX_SAS_MSG_LENGTH);
    ftStatus = SetEventNotification(m_ftHandle, EventMask, hRxCommEvent); 
    if (WaitForSingleObject(hRxCommEvent, 50))
        if (stop_comm_thread)
            return;
        else
            receive_buffer_index = getSerialData (&receive_buffer[0]);
}

    // get all the bytes
    while ((receive_buffer_index <= MINIMUM_SPOLL_RESPONSE) &&
           (receive_buffer_index < getSPollMessageSize()))
    {
//		if (waitForCommEvent (SAS_port_handle))
        ftStatus = SetEventNotification(m_ftHandle, EventMask, hRxCommEvent); 
        if (WaitForSingleObject(hRxCommEvent, 50))
            if (stop_comm_thread)
                return;
            else
                receive_buffer_index += getSerialData (&receive_buffer[receive_buffer_index]);
        Sleep (1);
    }
//    WaitForSingleObject(rx_message_semiphore, MAX_EVENT_WAIT_TIMOUT);
    memcpy(response_message.message, receive_buffer, receive_buffer_index);
    response_message.message_ready = TRUE;
    response_message.message_size = receive_buffer_index;
//    ReleaseSemaphore(rx_message_semiphore, 1, NULL);
}

void CSAS_Comm::getMPollResponse (void)
{
// the first byte of the received message needs to be the address of the game = 1
while ((receive_buffer[0] != sas_id) && receive_buffer[0])
{
    memset(receive_buffer, 0, MAX_SAS_MSG_LENGTH);
    ftStatus = SetEventNotification(m_ftHandle, EventMask, hRxCommEvent); 
    if (WaitForSingleObject(hRxCommEvent, 50))
        if (stop_comm_thread)
            return;
        else
            receive_buffer_index = getSerialData (&receive_buffer[0]);
}

    // get all the bytes
    while ((receive_buffer_index <= MINIMUM_MPOLL_RESPONSE) &&
           (receive_buffer_index < getMPollMessageSize()))
    {
//		if (waitForCommEvent (SAS_port_handle))
        ftStatus = SetEventNotification(m_ftHandle, EventMask, hRxCommEvent); 
        if (WaitForSingleObject(hRxCommEvent, 50))
            if (stop_comm_thread)
                return;
            else
                receive_buffer_index += getSerialData (&receive_buffer[receive_buffer_index]);
        Sleep (1);
    }
//    WaitForSingleObject(rx_message_semiphore, MAX_EVENT_WAIT_TIMOUT);
    memcpy(response_message.message, receive_buffer, receive_buffer_index);
    response_message.message_ready = TRUE;
    response_message.message_size = receive_buffer_index;
//    ReleaseSemaphore(rx_message_semiphore, 1, NULL);
}


void CSAS_Comm::initializeSAS (void)
{
    memset (sas_message_que, 0, sizeof(SAS_MESSAGE) * SAS_MESSAGE_QUE_SIZE);

	non_volatile_ram.manufacturer_id[0] = 0;
	non_volatile_ram.manufacturer_id[1] = 0;

    clearSasMessage (&current_tx_message);

    message_queue_semiphore = CreateSemaphore(NULL,
                                              1,
                                              1,
                                              TEXT("MessageQueueSemiphore"));

    aft_message_queue_semiphore = CreateSemaphore(NULL,
                                              1,
                                              1,
                                              TEXT("AftMessageQueueSemiphore"));

    sendBroadcastPoll ();
//    queueEnableEventReporting(FALSE);
    queueRequestMachineInfo();
    queueSendSelectedGameNumber();
    queueSendSasVersion();
//    queueAftStatusRequest();
//    queueGameDelayMessage();
//    queueSendTotalHandPaidCancelledCredits();
    queueRequestBillMeterData();
    queueRequestMeterData();
    queueEnableEventReporting(TRUE);
//    queueAftRegistration(0);
    queueSendCurrentCredits();
    if(pCGameCommControl->payThroughDispenser())
        queueSendHandpayInfo();

//	non_volatile_ram.last_TOTAL_IN = non_volatile_ram.TOTAL_IN;
    non_volatile_ram.game_played_since_logon = FALSE;

    non_volatile_ram.middle_of_a_game = FALSE;

    if (pCGameCommControl)
        pCGameCommControl->updateSasCommunicationStatus (sas_communicating);
}

// place the message in the queue
bool CSAS_Comm::queueSasMessage (SAS_MESSAGE* sas_message)
{
    bool queued = FALSE;
    bool same_message_found = FALSE;
    int i;


    for (i= 0; i < total_queued_messages; i++)
    {
        if (sas_message->message[COMMAND_CODE_INDEX] == sas_message_que[i].message[COMMAND_CODE_INDEX] &&
            sas_message->message[COMMAND_CODE_INDEX+1] == sas_message_que[i].message[COMMAND_CODE_INDEX+1])
        {
            same_message_found = TRUE;
            break;
        }
    }

    if (!same_message_found)
    {
        WaitForSingleObject(message_queue_semiphore, MAX_EVENT_WAIT_TIMOUT);
        if (total_queued_messages < SAS_MESSAGE_QUE_SIZE)
        {
            sas_message->message_ready = FALSE;
            total_queued_messages++;
            memcpy(&sas_message_que[total_queued_messages - 1],
                   sas_message, MAX_SAS_MSG_LENGTH);
            queued = TRUE;
        }
        ReleaseSemaphore(message_queue_semiphore, 1, NULL);
    }


    return queued;
}

void CSAS_Comm::dequeueSasMessage (void)
{
    WaitForSingleObject(message_queue_semiphore, MAX_EVENT_WAIT_TIMOUT);
    if (total_queued_messages >= 2)
    {
        memmove(sas_message_que, &sas_message_que[1], (total_queued_messages - 1) * sizeof(SAS_MESSAGE));
        memset(&sas_message_que[total_queued_messages - 1], 0, sizeof(SAS_MESSAGE));
        total_queued_messages--;
    }
    else if (total_queued_messages)
    {
        memset(sas_message_que, 0, sizeof(SAS_MESSAGE));
        total_queued_messages--;
    }
    ReleaseSemaphore(message_queue_semiphore, 1, NULL);
}

void CSAS_Comm::sendQueuedSasMessage (void)
{
    if (total_queued_messages)
        sendSerialMessage (&sas_message_que[0]);

    // make sure we dequeue remote handpay reset messages after 3 tries
    if (sas_message_que[0].message[1] == HANDPAY_RESET)
    {
        if (handpay_reset_sends > 3)
        {
            dequeueSasMessage();
            handpay_reset_sends = 0;
        }
        else
            handpay_reset_sends++;
    }
    else
        handpay_reset_sends = 0;
}



// place the card info message in the queue
bool CSAS_Comm::queueCardInfoSasMessage (SAS_MESSAGE* sas_message)
{
    bool queued = FALSE;

//    WaitForSingleObject(message_queue_semiphore, MAX_EVENT_WAIT_TIMOUT);
    if (!pending_card_info_message)
    {
        sas_message->message_ready = FALSE;
        pending_card_info_message = TRUE;
        memcpy(&card_info_message_que[0], sas_message, MAX_SAS_MSG_LENGTH);
        queued = TRUE;
    }
//    ReleaseSemaphore(message_queue_semiphore, 1, NULL);

    return queued;
}

void CSAS_Comm::dequeueCardInfoSasMessage (void)
{
//    WaitForSingleObject(message_queue_semiphore, MAX_EVENT_WAIT_TIMOUT);
    if (pending_card_info_message)
    {
        memset(&card_info_message_que[0], 0, sizeof(SAS_MESSAGE));
        pending_card_info_message = FALSE;
    }
//    ReleaseSemaphore(message_queue_semiphore, 1, NULL);
}

void CSAS_Comm::sendQueuedCardInfoSasMessage (void)
{
    if (pending_card_info_message)
        sendSerialMessage (&card_info_message_que[0]);
}



void CSAS_Comm::sendQueuedSasAftMessage (void)
{
    if (total_queued_aft_messages)
    {
        sendSerialMessage (&sas_aft_message_que[0]);

        // need to save the last AFT request message
        if ((sas_aft_message_que[0].message[1] == TRANSFER_FUNDS) &&
            (sas_aft_message_que[0].message[3] == 0))
            savePendingSasAftMessage();
    }
}

// place the message in the queue
bool CSAS_Comm::queueSasAftMessage (SAS_MESSAGE* sas_message)
{
    bool queued = FALSE;

    WaitForSingleObject(aft_message_queue_semiphore, MAX_EVENT_WAIT_TIMOUT);
    if (total_queued_aft_messages < SAS_AFT_MESSAGE_QUE_SIZE)
    {
        sas_message->message_ready = FALSE;
        total_queued_aft_messages++;
        memcpy(&sas_aft_message_que[total_queued_aft_messages - 1],
               sas_message, MAX_SAS_MSG_LENGTH);
        queued = TRUE;
    }
    ReleaseSemaphore(aft_message_queue_semiphore, 1, NULL);

    return queued;
}

// return TRUE if there is an aft interrogation poll in sas aft message queue
bool CSAS_Comm::aftInterrogationPollQueued (void)
{
    int i;
    bool interrogation_poll_queued = FALSE;

    WaitForSingleObject(aft_message_queue_semiphore, MAX_EVENT_WAIT_TIMOUT);
    for (i=0; i < SAS_AFT_MESSAGE_QUE_SIZE; i++)
    {
        if (sas_aft_message_que[i].message[3] == 0xff)
        {
            interrogation_poll_queued = TRUE;
            break;
        }
    }
    ReleaseSemaphore(aft_message_queue_semiphore, 1, NULL);

    return interrogation_poll_queued;
}

void CSAS_Comm::savePendingSasAftMessage (void)
{
    pending_sas_aft_message = sas_aft_message_que[0];
}

void CSAS_Comm::dequeueSasAftMessage (void)
{
    WaitForSingleObject(aft_message_queue_semiphore, MAX_EVENT_WAIT_TIMOUT);
    if (total_queued_aft_messages >= 2)
    {
        memmove(sas_aft_message_que, &sas_aft_message_que[1], (SAS_AFT_MESSAGE_QUE_SIZE - 1) * sizeof(SAS_MESSAGE));
        memset(&sas_aft_message_que[SAS_AFT_MESSAGE_QUE_SIZE - 1], 0, sizeof(SAS_MESSAGE));
        total_queued_aft_messages--;
    }
    else if (total_queued_aft_messages)
    {
        memset(sas_aft_message_que, 0, sizeof(SAS_MESSAGE));
        total_queued_aft_messages--;
    }
    ReleaseSemaphore(aft_message_queue_semiphore, 1, NULL);
}

void CSAS_Comm::requeuePendingSasAftMessage (void)
{
    if (pending_sas_aft_message.poll_type &&
        pending_sas_aft_message.message_size &&
        (pending_sas_aft_message.message[2] >= 1) &&
        (pending_sas_aft_message.message[3] != 0x80))
    {
        SAS_MESSAGE sas_message;

        sas_message = pending_sas_aft_message;
        queueSasAftMessage (&sas_message);
    }
}

void CSAS_Comm::clearPendingSasAftMessage (void)
{
    memset (&pending_sas_aft_message, 0, sizeof(SAS_MESSAGE));
}

void CSAS_Comm::processSASMessages (void)
{
    time_t current_time;
    BOOL real_time_event_activity = FALSE;
    CString message;

    // process messages from the game or poll game if it is time...
    if (response_message.message_ready)
    {
        real_time_event_activity = processSASMessage();
        last_sas_poll_received_time = time(NULL);

        if (!sas_communicating)
        {
            if (pCGameCommControl)
                pCGameCommControl->updateSasCommunicationStatus (TRUE);
        }
        sas_communicating = TRUE;
    }

    if (poll_SAS)
    {
        poll_SAS = FALSE;

        current_time = time(NULL);


        if ((current_time >= last_meter_check_time + meter_check_msec_count) || check_game_meters)
        {
            check_game_meters = FALSE;
            queueRequestBillMeterData();
            queueRequestMeterData();

            idle_meter_sends++;
            if (idle_meter_sends >= 2)
            {
                meter_check_msec_count = SEND_METERS_MSEC_COUNT_IDLE;
                idle_meter_sends = 0;
            }
        }

        if (delay_next_sas_poll)
        {
            Sleep(message_delay_in_msecs);
            delay_next_sas_poll = FALSE;
        }

        // if the current tx message - message ready flag is not clear - we did not
        // process a response yet
        if (!stop_comm_thread)
        {
            if ((current_tx_message.poll_type != G_POLL) && current_tx_message.message_ready)
            {
                no_response_count++;

                if (no_response_count > MAX_NO_RESPONSE_COUNT)
                {
                    no_response_count = 0;

                    if (!stop_comm_thread && (current_time > (last_sas_poll_received_time + MAX_SAS_NO_RECEIVE_SECS)))
                    {
                        ResetDevice();
                        Sleep(1000);

                        last_sas_poll_received_time = time(NULL);

                        if ((getGbSystemState() == ON_AVAIL) || (getGbSystemState() == ON_UNAVAIL))
                        {
                            if ((!non_volatile_ram.marker_advanced_this_session || !non_volatile_ram.current_bank_meter) &&
                                !non_volatile_ram.marker_advance_pending)
                            {
                                if (pCGameCommControl)
                                {
                                    pCGameCommControl->AccountLogout((ULONG)0);

                                }
                                saveNonVolatileRam();
                            }
                        }
                    }
                    else if (!(handpay_pending && !handpay_info_received))
                    {
                        sendBroadcastPoll();
                        Sleep(message_delay_in_msecs);
                        queueEnableEventReporting(TRUE);
			            sendQueuedSasMessage();
                        aft_pending = FALSE;
                        // make sure we send a game delay when comm is reestablished
                        if (sas_communicating)
                        {
                            sas_communicating = FALSE;
                            if (pCGameCommControl)
                                pCGameCommControl->updateSasCommunicationStatus (sas_communicating);
                        }
                    }

                }
            }
            else
            {
                    no_response_count = 0;

                    if (real_time_event_activity)
                        sendGeneralPoll(sas_id);
                    else if (pending_card_info_message)  // getting card info is the highest priority
                        sendQueuedCardInfoSasMessage();
                    else if (total_queued_messages)
                        sendQueuedSasMessage();
                    else if (total_queued_aft_messages && !aft_pending)
                        sendQueuedSasAftMessage();
                    else
                        sendGeneralPoll(sas_id);
        	}
    	}
	}
}

// returns TRUE if there was real time event activity
BOOL CSAS_Comm::processSASMessage (void)
{
    BOOL real_time_event_activity = FALSE;

//    WaitForSingleObject(rx_message_semiphore, MAX_EVENT_WAIT_TIMOUT);

    // if the response is a real time event, process it regardless of which message was sent to the game
    if ((response_message.message[0] == sas_id) &&
        (response_message.message[EVENT_ID_INDEX] == 0xff))
    {
//        if (response_message.message[EXCEPTION_CODE_INDEX] != 0)
        if ((response_message.message[EXCEPTION_CODE_INDEX] != 0) && (response_message.message[EXCEPTION_CODE_INDEX] != 0x1f))
            real_time_event_activity = TRUE;

        processRealTimeEventMessage();
    }
    else
    {
        // make sure the first byte is the polling address so we don't process garbage
//            if (response_message.message[0] == sas_id)
            switch (current_tx_message.poll_type)
            {
                case R_POLL:
                    processRPollResponse();
                    break;
                case S_POLL:
                    processSPollResponse();
                    break;
                case M_POLL:
                    processMPollResponse();
                    break;
            }
    }

//    ReleaseSemaphore(rx_message_semiphore, 1, NULL);

    return real_time_event_activity;
}

void CSAS_Comm::processRPollResponse (void)
{
    switch (response_message.message[1])
    {
        case 0:
            // the gaming machine is busy, do nothing
            sendAck();
            break;
        case REQUEST_GAME_METER_DATA:
            if ((non_volatile_ram.manufacturer_id[0] == 'B') && (non_volatile_ram.manufacturer_id[1] == '7'))
                processBallyMeterDataRequestResponse();
            else
                processMeterDataRequestResponse();
            break;
        case REQUEST_BILL_METER_DATA:
            processBillMeterDataRequestResponse();
            break;
        case REQUEST_MACHINE_INFO:
            processMachineInfoRequestResponse();
            break;
        case SEND_SELECTED_GAME_NUMBER:
            processSelectedGameNumberResponse();
            break;
        case SEND_CARD_INFO:
            processCardInfoResponse();
            break;
        case SEND_HANDPAY_INFO:
            processHandpayInfoResponse();
            break;
        case SEND_SAS_VERSION:
            processSasVersionResponse();
            break;
        case SEND_CURRENT_CREDITS:
            processSendCurrentCredits();
            break;
        case MULTI_DENOM_PREAMBLE:
            processMultiDenomMessage();
            break;
        default:
//            sendAck();
//            dequeueSasMessage();
            clearSasMessage (&current_tx_message);
            break;
    }
}

void CSAS_Comm::processSPollResponse (void)
{
    if ((response_message.message[0] == sas_id) &&
        (response_message.message[1] == 0) &&
        (response_message.message_size == 2))
    {
        // machine is busy do nothing except ack recieved message
        sendAck();
        if (current_tx_message.message[1] == INITIATE_LEGACY_BONUS)
        {
                pCGameCommControl->redeemComplete((BYTE)1);
                dequeueSasMessage();
                clearSasMessage (&current_tx_message);
        }
    }
    else
    {
        switch (current_tx_message.message[1])
        {
            case ENABLE_DISABLE_EVENT_REPORTING:
                if (response_message.message[0] == sas_id)
                {
                    real_time_event_reporting_enabled = TRUE;
                    sendAck();
                    dequeueSasMessage();
                    clearSasMessage (&current_tx_message);
                }
                break;
            case TRANSFER_FUNDS:
                processTransferFundsResponse();
                break;
            case AFT_STATUS:
                processAftLockAndStatusResponse();
                break;
            case AFT_REGISTRATION:
                processAftRegistrationResponse();
                break;
            case ENABLE_JACKPOT_HANDPAY_RESET_METHOD:
                processEnableJackpotHandpayResetMethodResponse();
                break;
            case HANDPAY_RESET:
                processHandpayResetResponse();
                break;
            case INITIATE_LEGACY_BONUS:
                if (point_redemption)
                    pCGameCommControl->redeemComplete((BYTE)0);
                sendAck();
                dequeueSasMessage();
                clearSasMessage (&current_tx_message);
                break;
            default:
                sendAck();
                dequeueSasMessage();
                clearSasMessage (&current_tx_message);
                break;
        }
    }
}

void CSAS_Comm::processMPollResponse (void)
{
    switch (response_message.message[1])
    {
        case 0:
            // the gaming machine is busy, do nothing
            sendAck();
            break;
        case SEND_GAME_CONFIGURATION:
            processGameConfigurationResponse();
            break;
        case SEND_SELECTED_GAME_METERS:
            processSelectedGameMetersResponse();
            break;
        case SEND_TOTAL_HAND_PAID_CANCELLED_CREDITS:
            processTotalHandPaidCancelledCredits();
            break;
        default:
//            sendAck();
//            dequeueSasMessage();
            clearSasMessage (&current_tx_message);
            break;
    }
}

void CSAS_Comm::processRealTimeEventMessage (void)
{
    unsigned char exception_code = response_message.message[EXCEPTION_CODE_INDEX];


    if (response_message.message[EVENT_ID_INDEX] == 0xff)
    {
        switch (exception_code)
        {
            case BILL_ACCEPTED:
                processBillAcceptedEvent();
//                queueRequestBillMeterData();
                queueRequestMeterData();
                if (pCGameCommControl)
                    pCGameCommControl->logSasRealTimeEventMessage (exception_code);
                break;
            case GAME_START:
                processGameStartEvent();
                break;
            case GAME_END:
                processGameEndEvent();
                break;
            case CREDIT_WAGERED:
                sendAck();
                if (poker_game_being_played && check_game_delay_delta)
                {
					check_game_delay_delta = FALSE;

					SYSTEMTIME current_system_time;
					GetSystemTime(&current_system_time);

					if ((current_system_time.wHour == last_game_end_time.wHour) &&
						(current_system_time.wMinute == last_game_end_time.wMinute) &&
						(current_system_time.wSecond == last_game_end_time.wSecond))
					{					
						if ((current_system_time.wMilliseconds - last_game_end_time.wMilliseconds) < game_delay_in_msecs)
                        {
		            	    queueGameDelayMessage();
                        }
					}
                }
                 
                break;
            case GAME_SELECTED:
                first_game_play = TRUE;
                poker_game_being_played = FALSE; // initialize this each time a game is entered
                bj_game_being_played = FALSE;
                processGameSelectedEvent();
                break;
            case HANDPAY_PENDING:
                sendAck();
                handpay_pending = TRUE;
                handpay_info_received = FALSE;
                if(pCGameCommControl->payThroughDispenser())
                    queueSendHandpayInfo();
                if (pCGameCommControl)
                    pCGameCommControl->logSasRealTimeEventMessage (exception_code);
                break;
            case AFT_TRANSFER_COMPLETE:
                sendAck();
                queueAftInterrogation();
                aft_pending = FALSE;
                break;
            case DOOR_OPEN_EVENT:
                sendAck();
                non_volatile_ram.door_state |= MAIN_DOOR;
        		non_volatile_ram.game_status |= GAME_DOOR;
                if (pCGameCommControl)
                {
                    pCGameCommControl->logSasRealTimeEventMessage (exception_code);
                    pCGameCommControl->updateGbInterfaceData();
                }
                break;
            case DOOR_CLOSED_EVENT:
                sendAck();
                non_volatile_ram.door_state &= ~MAIN_DOOR;
        		non_volatile_ram.game_status &= ~GAME_DOOR;
                if (pCGameCommControl)
                {
                    pCGameCommControl->logSasRealTimeEventMessage (exception_code);
                    pCGameCommControl->updateGbInterfaceData();
                }
                break;
            case DROP_DOOR_OPEN_EVENT:
                sendAck();
                non_volatile_ram.door_state |= CASH_DOOR;
        		non_volatile_ram.game_status |= DROP_DOOR;
                if (pCGameCommControl)
                    pCGameCommControl->logSasRealTimeEventMessage (exception_code);
                break;
            case DROP_DOOR_CLOSED_EVENT:
                sendAck();
                non_volatile_ram.door_state &= ~CASH_DOOR;
        		non_volatile_ram.game_status &= ~DROP_DOOR;
                if (pCGameCommControl)
                    pCGameCommControl->logSasRealTimeEventMessage (exception_code);
                break;
            case CASHBOX_DOOR_OPEN_EVENT:
                sendAck();
                non_volatile_ram.door_state |= CASH_DOOR;
				if (non_volatile_ram.game_status & COLLECT_APPROVED)
				{
					non_volatile_ram.game_status &= ~COLLECT_APPROVED;
            		non_volatile_ram.game_status |= COLLECT_IN_PROG;
//                    pCGameCommControl->SetGameCollectionState(FORCE_COLLECTION_STATE);
				}
                if (pCGameCommControl)
                    pCGameCommControl->logSasRealTimeEventMessage (exception_code);
                queueRequestMeterData();
                break;
            case CASHBOX_DOOR_CLOSED_EVENT:
                sendAck();
                non_volatile_ram.door_state &= ~CASH_DOOR;
        		non_volatile_ram.game_status &= ~DROP_DOOR ;
                if (pCGameCommControl)
                    pCGameCommControl->logSasRealTimeEventMessage (exception_code);
                break;

            case CASHBOX_INSTALLED:
                sendAck();
                if (pCGameCommControl)
				{
                    pCGameCommControl->logSasRealTimeEventMessage (exception_code);
					pCGameCommControl->SendIdcStackerPullMessage (0);
				}
                break;

            case CASHBOX_REMOVED:
                if(pCGameCommControl->GetGameCollectionState() == ARMED_COLLECTION_STATE)
                {
                    pCGameCommControl->SetGameCollectionState(CLEAR_COLLECTION_STATE);
                    if (!first_meter_fetch)
                        pCGameCommControl->processGameMeters(MMI_GAME_COLLECTED_METERS);
                    else
                        queueRequestMeterData();
                }
                sendAck();
                if (pCGameCommControl)
				{
                    pCGameCommControl->logSasRealTimeEventMessage (exception_code);
					pCGameCommControl->SendIdcStackerPullMessage (1);
				}
                break;

            case CASHOUT_BUTTON_PRESSED:
                sendAck();
                if (pCGameCommControl)
                    pCGameCommControl->logSasRealTimeEventMessage (exception_code);
                break;
            case AC_POWER_APPLIED_EVENT:
                sendAck();
//                queueEnableEventReporting(FALSE);
                queueRequestMachineInfo();
                queueSendSelectedGameNumber();
                queueSendSasVersion();
                queueGameDelayMessage();
//                queueSendTotalHandPaidCancelledCredits();
                queueRequestBillMeterData();
                queueRequestMeterData();
                queueEnableEventReporting(TRUE);
                if (pCGameCommControl)
                    pCGameCommControl->logSasRealTimeEventMessage (exception_code);
                break;
            case CARD_HELD_NOT_HELD:
                processCardHeldNotHeldMessage();
                break;
            case HANDPAY_RESET_KEYSWITCH:
                sendAck();
//                non_volatile_ram.current_bank_meter = 0;
                queueSendCurrentCredits();
                handpay_pending = FALSE;
                handpay_info_received = TRUE;
                last_meter_check_time = time(NULL);
                meter_check_msec_count = 5;
                check_game_meters = TRUE;
                if (pCGameCommControl)
                    pCGameCommControl->logSasRealTimeEventMessage (exception_code);
                break;
            default:
                sendAck();
                break;
        }
    }
}

void CSAS_Comm::processBillAcceptedEvent (void)
{
    SAS_MESSAGE response_msg = response_message;
    BillAcceptedEvent* bill_accepted_event = (BillAcceptedEvent*) response_msg.message;

    if (CRC(response_msg.message, sizeof(BillAcceptedEvent) - 2, 0)
        == bill_accepted_event->crc)
    {
        clearSasMessage (&current_tx_message);
        sendAck();

        queueSendCurrentCredits();
        queueRequestBillMeterData();

        switch(bcdToUnsignedChar (bill_accepted_event->denomination_code))
        {
            case 0:
                cumulative_drink_comp_dollars++;
                pCGameCommControl->processBillAcceptedEvent (100);
                break;
            case 1:
                cumulative_drink_comp_dollars += 2;
                pCGameCommControl->processBillAcceptedEvent (200);
                break;
            case 2:
                cumulative_drink_comp_dollars += 5;
                pCGameCommControl->processBillAcceptedEvent (500);
                break;
            case 3:
                cumulative_drink_comp_dollars += 10;
                pCGameCommControl->processBillAcceptedEvent (1000);
                break;
            case 4:
                cumulative_drink_comp_dollars += 20;
                pCGameCommControl->processBillAcceptedEvent (2000);
                break;
            case 6:
                cumulative_drink_comp_dollars += 50;
                pCGameCommControl->processBillAcceptedEvent (5000);
                break;
            case 7:
                cumulative_drink_comp_dollars += 100;
                pCGameCommControl->processBillAcceptedEvent (10000);
                break;
        }

        if (cumulative_drink_comp_dollars >= 10)
        {
            pCGameCommControl->processGameDrinkComp(cumulative_drink_comp_dollars);
            cumulative_drink_comp_dollars = 0;
        }
    }
}

void CSAS_Comm::processGameStartEvent (void)
{
    SAS_MESSAGE response_msg = response_message;
    GameStartEvent* game_start_event = (GameStartEvent*) response_msg.message;
	ULONG denomination, total_in;
    int i;
//    ULONG cents_played;


    if (CRC(response_msg.message, sizeof(GameStartEvent) - 2, 0)
        == game_start_event->crc)
    {
        clearSasMessage (&current_tx_message);
        sendAck();

        game_win = 0;
        logout_after_4ok_win_check = FALSE;

        // initialize held cards flags
        for (i=0; i < 5; i++)
            non_volatile_ram.held_cards[i] = 0;

        non_volatile_ram.middle_of_a_game = TRUE;
        non_volatile_ram.GAMES_PLAYED++;

        current_game_wager = bcd2ToUnsignedLong(game_start_event->credits_wagered[0],
                                               game_start_event->credits_wagered[1]);


        // update the total coin in meter
        total_in =  bcd4ToUnsignedLong (game_start_event->total_coin_in_meter[0],
                                       game_start_event->total_coin_in_meter[1],
                                       game_start_event->total_coin_in_meter[2],
                                       game_start_event->total_coin_in_meter[3]);

// COMMENT OUT TO ELIMINATE METER WOBBLE
        // sent in units of accouting denomination - convert to cents
//        non_volatile_ram.TOTAL_IN = total_in * non_volatile_ram.machine_accounting_denomination;

        if (game_start_event->wager_type)
            denomination = translateDenomination(game_start_event->wager_type & 0x1f);
        else
            denomination = non_volatile_ram.machine_accounting_denomination;

        if (denomination != non_volatile_ram.current_game_denomination)
        {
            non_volatile_ram.current_game_denomination = denomination;
        }

//        non_volatile_ram.TOTAL_IN += current_game_wager * non_volatile_ram.current_game_denomination;

        // initialize each time a game is played
//        SEND_METERS_SECOND_COUNT = SECONDS_TO_SEND_METERS_GAME_PLAY;
        meter_check_msec_count = SEND_METERS_MSEC_COUNT_GAME_PLAY;
        idle_meter_sends = 0;

        if ((getGbSystemState() == ON_AVAIL) ||
             (getGbSystemState() == ON_UNAVAIL))
        {
            if (poker_game_being_played)
                incrementBingoHandle(current_game_wager);
            else if (!bj_game_being_played)
            {
                // U1 games do not ever get double points
                if (!((non_volatile_ram.manufacturer_id[0] == 'U') && (non_volatile_ram.manufacturer_id[1] == '1')) &&
                    !((non_volatile_ram.manufacturer_id[0] == 'S') && (non_volatile_ram.manufacturer_id[1] == 'F')))
                {
                    incrementPlayerGbPoints (current_game_wager); // give the player double GB points for non-poker games.
                }
            }

            incrementPlayerGbPoints (current_game_wager);

            saveNonVolatileRam();

            if (pCGameCommControl)
                pCGameCommControl->updateGbInterfaceData();

        }

        cumulative_drink_comp_dollars = 0; // reset every game
        non_volatile_ram.frenzy_award_hit = 0;

/*
        // make adjustment to gamblers bonus points and denomination meters if there were additional credits bet
        // last game cycle (i.e. blackjack split, double up or poker double or nothing)
        if (non_volatile_ram.last_TOTAL_IN &&
            (non_volatile_ram.TOTAL_IN > (non_volatile_ram.last_TOTAL_IN + (current_game_wager * non_volatile_ram.current_game_denomination))))
        {
            // make adjustment to gamblers bonus points and denomination meters
            cents_played = non_volatile_ram.TOTAL_IN - (non_volatile_ram.last_TOTAL_IN + (current_game_wager * non_volatile_ram.current_game_denomination));

        }

        non_volatile_ram.last_TOTAL_IN = non_volatile_ram.TOTAL_IN;
*/
		queueSendCurrentCredits();
    }
}

void CSAS_Comm::processGameEndEvent (void)
{
    SAS_MESSAGE response_msg = response_message;
    GameEndEvent* game_end_event = (GameEndEvent*) response_msg.message;

    if (CRC(response_msg.message, sizeof(GameEndEvent) - 2, 0)
        == game_end_event->crc)
    {
        clearSasMessage (&current_tx_message);
        sendAck();

		GetSystemTime(&last_game_end_time);
        check_game_delay_delta = TRUE;

        game_win = bcd4ToUnsignedLong(game_end_event->game_win[0],
                              game_end_event->game_win[1],
                              game_end_event->game_win[2],
                              game_end_event->game_win[3]);

        if ((sas_version[0] == '5') && (sas_version[1] == '0') && (sas_version[2] == '1'))
            game_win = game_win * non_volatile_ram.current_game_denomination;
        else
            game_win *= non_volatile_ram.machine_accounting_denomination;

        if (game_win)
        {
            // initialize each time a game is played
            meter_check_msec_count = SEND_METERS_MSEC_COUNT_GAME_PLAY;
            idle_meter_sends = 0;
        }

        non_volatile_ram.CENTS_WON += game_win;
// COMMENT OUT TO ELIMINATE METER WOBBLE
//        non_volatile_ram.TOTAL_OUT += game_win;

        if ((poker_game_being_played || first_game_play) && game_win)
        {
            if (!logout_after_4ok_win_check)
                queueSendCardInfo();
        }
        first_game_play = FALSE;
        non_volatile_ram.middle_of_a_game = FALSE;

		queueSendCurrentCredits();

        // send the meters every 5 games played, which is about every 10 seconds
        if (games_played_since_last_meter_send >= 5)
        {
            games_played_since_last_meter_send = 0;
            if (!first_meter_fetch)
                pCGameCommControl->processGameMeters(MMI_UPDATE_GAME_METERS);
            else
                queueRequestMeterData();
        }
        else
            games_played_since_last_meter_send++;

    }
}

void CSAS_Comm::processGameSelectedEvent (void)
{
    SAS_MESSAGE response_msg = response_message;
    GameSelectedEvent* game_selected_event = (GameSelectedEvent*) response_msg.message;

    if (CRC(response_msg.message, sizeof(GameSelectedEvent) - 2, 0)
        == game_selected_event->crc)
    {
        clearSasMessage (&current_tx_message);
        sendAck();

        currently_selected_game_number[0] = game_selected_event->game_number[0];
        currently_selected_game_number[1] = game_selected_event->game_number[1];

        if (bcd2ToUnsignedLong(currently_selected_game_number[0],
                               currently_selected_game_number[1]))
        {
            queueSendSelectedGameMeterData (currently_selected_game_number);
        }

        queueSendGameConfiguration (game_selected_event->game_number);
        queueRequestMeterData();
    }
}

void CSAS_Comm::processCardHeldNotHeldMessage (void)
{
    SAS_MESSAGE response_msg = response_message;
    CardHeldNotHeldEvent* card_held_not_held_event = (CardHeldNotHeldEvent*) response_msg.message;

    if (CRC(response_msg.message, sizeof(CardHeldNotHeldEvent) - 2, 0)
        == card_held_not_held_event->crc)
    {
        clearSasMessage (&current_tx_message);
        sendAck();

        if (card_held_not_held_event->card_status & 0x80)
            non_volatile_ram.held_cards[card_held_not_held_event->card_status & 0x7f] = 1;
    }
}

void CSAS_Comm::processMeterDataRequestResponse (void)
{
    ULONG games_played_meter;
    ULONG total_in_meter;
    ULONG total_out_meter;
    ULONG total_drop_meter;
    ULONG total_atten_meter;
	ULONG total_jackpot;

    SAS_MESSAGE response_msg = response_message;
    MeterDataRequestResponse* meter_data_request_response
            = (MeterDataRequestResponse*) response_msg.message;

    if (CRC(response_msg.message, sizeof(MeterDataRequestResponse) - 2, 0) == meter_data_request_response->crc)
    {
        sendAck();
        dequeueSasMessage();
        clearSasMessage (&current_tx_message);

        total_in_meter =  bcd4ToUnsignedLong (meter_data_request_response->total_in[0],
                                              meter_data_request_response->total_in[1],
                                              meter_data_request_response->total_in[2],
                                              meter_data_request_response->total_in[3]);
        // sent in accounting units - adjust to cents
        total_in_meter *= non_volatile_ram.machine_accounting_denomination;
        if (total_in_meter != non_volatile_ram.TOTAL_IN)
            non_volatile_ram.TOTAL_IN = total_in_meter;

        total_out_meter =  bcd4ToUnsignedLong (meter_data_request_response->total_out[0],
                                        meter_data_request_response->total_out[1],
                                        meter_data_request_response->total_out[2],
                                        meter_data_request_response->total_out[3]);
        // sent in accounting units - adjust to cents
        total_out_meter *= non_volatile_ram.machine_accounting_denomination;
        if (total_out_meter != non_volatile_ram.TOTAL_OUT)
            non_volatile_ram.TOTAL_OUT = total_out_meter;

        games_played_meter = bcd4ToUnsignedLong (meter_data_request_response->games_played[0],
                                                 meter_data_request_response->games_played[1],
                                                 meter_data_request_response->games_played[2],
                                                 meter_data_request_response->games_played[3]);
        if (games_played_meter != non_volatile_ram.GAMES_PLAYED)
            non_volatile_ram.GAMES_PLAYED = games_played_meter;

        total_drop_meter = bcd4ToUnsignedLong (meter_data_request_response->total_drop[0],
                                               meter_data_request_response->total_drop[1],
                                               meter_data_request_response->total_drop[2],
                                               meter_data_request_response->total_drop[3]);
        // sent in accounting units - adjust to cents
        total_drop_meter *= non_volatile_ram.machine_accounting_denomination;
        if (total_drop_meter != non_volatile_ram.TOTAL_DROP)
        {
//            non_volatile_ram.TOTAL_DROP = total_drop_meter;
            if (total_drop_meter >= non_volatile_ram.WAT_IN)
                non_volatile_ram.TOTAL_DROP = total_drop_meter - non_volatile_ram.WAT_IN;
            else
                non_volatile_ram.TOTAL_DROP = total_drop_meter;
        }

        total_jackpot = bcd4ToUnsignedLong (meter_data_request_response->total_jackpot[0],
                                        meter_data_request_response->total_jackpot[1],
                                        meter_data_request_response->total_jackpot[2],
                                        meter_data_request_response->total_jackpot[3]);
        // sent in accounting units - adjust to cents
        total_jackpot *= non_volatile_ram.machine_accounting_denomination;

        non_volatile_ram.TOTAL_HAND_PAID_CANCELLED_CREDITS
                          = bcd4ToUnsignedLong (meter_data_request_response->cancelled_credits[0],
                                                meter_data_request_response->cancelled_credits[1],
                                                meter_data_request_response->cancelled_credits[2],
                                                meter_data_request_response->cancelled_credits[3]);
        // sent in accounting units - adjust to cents
        non_volatile_ram.TOTAL_HAND_PAID_CANCELLED_CREDITS *= non_volatile_ram.machine_accounting_denomination;

        total_atten_meter = total_jackpot + non_volatile_ram.TOTAL_HAND_PAID_CANCELLED_CREDITS;
        if (total_atten_meter != non_volatile_ram.TOTAL_ATTEN)
            non_volatile_ram.TOTAL_ATTEN = total_atten_meter;


        non_volatile_ram.meters_received_after_download = TRUE;

        if (first_meter_fetch)
        {
            first_meter_fetch = FALSE;
        }

        if (pCGameCommControl->processGameMeters(MMI_UPDATE_GAME_METERS))
            pCGameCommControl->logSasMeterDataMessage (meter_data_request_response);

    }
}

void CSAS_Comm::processBallyMeterDataRequestResponse (void)
{
    ULONG games_played_meter;
    ULONG total_in_meter;
    ULONG total_out_meter;
    ULONG total_drop_meter;
    ULONG total_atten_meter;
	ULONG total_jackpot;

    SAS_MESSAGE response_msg = response_message;
    BallyMeterDataRequestResponse* meter_data_request_response
            = (BallyMeterDataRequestResponse*) response_msg.message;

    if (CRC(response_msg.message, sizeof(BallyMeterDataRequestResponse) - 2, 0) == meter_data_request_response->crc)
    {
        sendAck();
        dequeueSasMessage();
        clearSasMessage (&current_tx_message);

        total_in_meter =  bcd4ToUnsignedLong (meter_data_request_response->total_in[0],
                                              meter_data_request_response->total_in[1],
                                              meter_data_request_response->total_in[2],
                                              meter_data_request_response->total_in[3]);
        // sent in accounting units - adjust to cents
        total_in_meter *= non_volatile_ram.machine_accounting_denomination;
        if (total_in_meter != non_volatile_ram.TOTAL_IN)
            non_volatile_ram.TOTAL_IN = total_in_meter;

        total_out_meter =  bcd4ToUnsignedLong (meter_data_request_response->total_out[0],
                                        meter_data_request_response->total_out[1],
                                        meter_data_request_response->total_out[2],
                                        meter_data_request_response->total_out[3]);
        // sent in accounting units - adjust to cents
        total_out_meter *= non_volatile_ram.machine_accounting_denomination;
        if (total_out_meter != non_volatile_ram.TOTAL_OUT)
            non_volatile_ram.TOTAL_OUT = total_out_meter;

        games_played_meter = bcd4ToUnsignedLong (meter_data_request_response->games_played[0],
                                                 meter_data_request_response->games_played[1],
                                                 meter_data_request_response->games_played[2],
                                                 meter_data_request_response->games_played[3]);
        if (games_played_meter != non_volatile_ram.GAMES_PLAYED)
            non_volatile_ram.GAMES_PLAYED = games_played_meter;

        total_drop_meter = bcd4ToUnsignedLong (meter_data_request_response->total_drop[0],
                                               meter_data_request_response->total_drop[1],
                                               meter_data_request_response->total_drop[2],
                                               meter_data_request_response->total_drop[3]);
        // sent in accounting units - adjust to cents
        total_drop_meter *= non_volatile_ram.machine_accounting_denomination;
        if (total_drop_meter != non_volatile_ram.TOTAL_DROP)
        {
//            non_volatile_ram.TOTAL_DROP = total_drop_meter;
            if (total_drop_meter >= non_volatile_ram.WAT_IN)
                non_volatile_ram.TOTAL_DROP = total_drop_meter - non_volatile_ram.WAT_IN;
            else
                non_volatile_ram.TOTAL_DROP = total_drop_meter;
        }

        total_jackpot = bcd4ToUnsignedLong (meter_data_request_response->total_jackpot[0],
                                        meter_data_request_response->total_jackpot[1],
                                        meter_data_request_response->total_jackpot[2],
                                        meter_data_request_response->total_jackpot[3]);
        // sent in accounting units - adjust to cents
        total_jackpot *= non_volatile_ram.machine_accounting_denomination;

        non_volatile_ram.TOTAL_HAND_PAID_CANCELLED_CREDITS
                          = bcd4ToUnsignedLong (meter_data_request_response->cancelled_credits[0],
                                                meter_data_request_response->cancelled_credits[1],
                                                meter_data_request_response->cancelled_credits[2],
                                                meter_data_request_response->cancelled_credits[3]);
        // sent in accounting units - adjust to cents
        non_volatile_ram.TOTAL_HAND_PAID_CANCELLED_CREDITS *= non_volatile_ram.machine_accounting_denomination;

        total_atten_meter = total_jackpot + non_volatile_ram.TOTAL_HAND_PAID_CANCELLED_CREDITS;
        if (total_atten_meter != non_volatile_ram.TOTAL_ATTEN)
            non_volatile_ram.TOTAL_ATTEN = total_atten_meter;


        non_volatile_ram.meters_received_after_download = TRUE;

        if (first_meter_fetch)
        {
            first_meter_fetch = FALSE;
        }

        pCGameCommControl->processGameMeters(MMI_UPDATE_GAME_METERS);

    }
    else
        processMeterDataRequestResponse();
}


void CSAS_Comm::processBillMeterDataRequestResponse (void)
{
    ULONG ones;
    ULONG fives;
    ULONG tens;
    ULONG twenties;
    ULONG fifties;
	ULONG hundreds;

    SAS_MESSAGE response_msg = response_message;
    BillMeterDataRequestResponse* bill_meter_data_request_response
            = (BillMeterDataRequestResponse*) response_msg.message;

    if (CRC(response_msg.message, sizeof(BillMeterDataRequestResponse) - 2, 0) == bill_meter_data_request_response->crc)
    {
        sendAck();
        dequeueSasMessage();
        clearSasMessage (&current_tx_message);

        ones =  bcd4ToUnsignedLong (bill_meter_data_request_response->ones[0],
                                              bill_meter_data_request_response->ones[1],
                                              bill_meter_data_request_response->ones[2],
                                              bill_meter_data_request_response->ones[3]);

        fives =  bcd4ToUnsignedLong (bill_meter_data_request_response->fives[0],
                                              bill_meter_data_request_response->fives[1],
                                              bill_meter_data_request_response->fives[2],
                                              bill_meter_data_request_response->fives[3]);

        tens =  bcd4ToUnsignedLong (bill_meter_data_request_response->tens[0],
                                              bill_meter_data_request_response->tens[1],
                                              bill_meter_data_request_response->tens[2],
                                              bill_meter_data_request_response->tens[3]);

        twenties =  bcd4ToUnsignedLong (bill_meter_data_request_response->twenties[0],
                                              bill_meter_data_request_response->twenties[1],
                                              bill_meter_data_request_response->twenties[2],
                                              bill_meter_data_request_response->twenties[3]);

        fifties =  bcd4ToUnsignedLong (bill_meter_data_request_response->fifties[0],
                                              bill_meter_data_request_response->fifties[1],
                                              bill_meter_data_request_response->fifties[2],
                                              bill_meter_data_request_response->fifties[3]);

        hundreds =  bcd4ToUnsignedLong (bill_meter_data_request_response->hundreds[0],
                                              bill_meter_data_request_response->hundreds[1],
                                              bill_meter_data_request_response->hundreds[2],
                                              bill_meter_data_request_response->hundreds[3]);
        non_volatile_ram.TOTAL_BILLS = 0;
        non_volatile_ram.TOTAL_BILLS += (ones * 1);
        non_volatile_ram.TOTAL_BILLS += (fives * 5);
        non_volatile_ram.TOTAL_BILLS += (tens * 10);
        non_volatile_ram.TOTAL_BILLS += (twenties * 20);
        non_volatile_ram.TOTAL_BILLS += (fifties * 50);
        non_volatile_ram.TOTAL_BILLS += (hundreds * 100);

        pCGameCommControl->logSasBillMeterDataMessage (bill_meter_data_request_response);
    }
}




void CSAS_Comm::processSelectedGameMetersResponse (void)
{
    SAS_MESSAGE response_msg = response_message;
    SelectedGameMetersRequestResponse* selected_game_meters_request_response
            = (SelectedGameMetersRequestResponse*) response_msg.message;

    if (CRC(response_msg.message, sizeof(SelectedGameMetersRequestResponse) - 2, 0)
        == selected_game_meters_request_response->crc)
    {
        sendAck();
        dequeueSasMessage();
        clearSasMessage (&current_tx_message);

        non_volatile_ram.GAME_CENTS_PLAYED =
            bcd4ToUnsignedLong (selected_game_meters_request_response->cents_played[0],
                                selected_game_meters_request_response->cents_played[1],
                                selected_game_meters_request_response->cents_played[2],
                                selected_game_meters_request_response->cents_played[3]);
        non_volatile_ram.GAME_CENTS_PLAYED *= non_volatile_ram.machine_accounting_denomination;

        non_volatile_ram.GAME_CENTS_WON =
            bcd4ToUnsignedLong (selected_game_meters_request_response->cents_won[0],
                                selected_game_meters_request_response->cents_won[1],
                                selected_game_meters_request_response->cents_won[2],
                                selected_game_meters_request_response->cents_won[3]);
        non_volatile_ram.GAME_CENTS_WON *= non_volatile_ram.machine_accounting_denomination;

    }
}


void CSAS_Comm::processTotalHandPaidCancelledCredits (void)
{
    SAS_MESSAGE response_msg = response_message;
    TotalHandPaidCancelledCreditsRequestResponse* total_hand_paid_cancelled_credits_request_response
            = (TotalHandPaidCancelledCreditsRequestResponse*) response_msg.message;

    if (CRC(response_msg.message, sizeof(TotalHandPaidCancelledCreditsRequestResponse) - 2, 0)
        == total_hand_paid_cancelled_credits_request_response->crc)
    {
        sendAck();
        dequeueSasMessage();
        clearSasMessage (&current_tx_message);

        non_volatile_ram.TOTAL_HAND_PAID_CANCELLED_CREDITS =
            bcd4ToUnsignedLong (total_hand_paid_cancelled_credits_request_response->total_hand_paid_cancelled_credits[0],
                                total_hand_paid_cancelled_credits_request_response->total_hand_paid_cancelled_credits[1],
                                total_hand_paid_cancelled_credits_request_response->total_hand_paid_cancelled_credits[2],
                                total_hand_paid_cancelled_credits_request_response->total_hand_paid_cancelled_credits[3]);
        non_volatile_ram.TOTAL_HAND_PAID_CANCELLED_CREDITS *= non_volatile_ram.machine_accounting_denomination;
    }
}

void CSAS_Comm::processMachineInfoRequestResponse (void)
{
    SAS_MESSAGE response_msg = response_message;
    MachineInfoRequestResponse* machine_info_request_response
            = (MachineInfoRequestResponse*) response_msg.message;

    if (CRC(response_msg.message, sizeof(MachineInfoRequestResponse) - 2, 0)
        == machine_info_request_response->crc)
    {
        sendAck();
        dequeueSasMessage();
        clearSasMessage (&current_tx_message);

        non_volatile_ram.machine_accounting_denomination =
                 translateDenomination(machine_info_request_response->denomination);

        memcpy(&non_volatile_ram.manufacturer_id, machine_info_request_response->game_id, 2);

        machine_info_recieved = TRUE;

	    queueGameDelayMessage();
    }
}

void CSAS_Comm::processCardInfoResponse (void)
{
    SAS_MESSAGE response_msg = response_message;
    CardInfoResponse* card_info_response
            = (CardInfoResponse*) response_msg.message;
    unsigned char fok_rank;
//    long updateGbInterfaceWithBingoWinningHandInfo = 0;
    BYTE card_type;
    long win_code;
	unsigned long card_award;
    int j;
	unsigned char cards[5];

    if (CRC(response_msg.message, sizeof(CardInfoResponse) - 2, 0)
        == card_info_response->crc)
    {
        sendAck();
        dequeueCardInfoSasMessage();
        clearSasMessage (&current_tx_message);

/*
card_info_response->hand[0] = 0x07;
card_info_response->hand[1] = 0x17;
card_info_response->hand[2] = 0x27;
card_info_response->hand[3] = 0x32;
card_info_response->hand[4] = 0x09;
*/
        // This flag only set in the deal state - reset in the draw state.
        if (poker_game_being_played && (card_info_response->hand_type == 1))
        {
            if ((getGbSystemState() == ON_AVAIL) || (getGbSystemState() == ON_UNAVAIL))
            {
                if (pCGameCommControl)
                {
                    fok_rank = pCGameCommControl->get4okWin (card_info_response->hand);

                    if (fok_rank != NO_4OK)
                    {
                        win_code = markBingoCard(fok_rank);
                        if (win_code)
                        {
                            if (non_volatile_ram.bingo.primary.type)
                            {
                                pCGameCommControl->updateGbInterfaceWithBingoWinningHandInfo(win_code, non_volatile_ram.bingo.primary.handle_to_date,
                                                                              non_volatile_ram.bingo.primary.games_to_date,
                                                                              PRIMARY_CARD);
                                pCGameCommControl->WinningBingoHand(sas_id,
                                                                    non_volatile_ram.member_id,
                                                                    win_code,
                                                                    non_volatile_ram.bingo.primary.handle_to_date,
                                                                    non_volatile_ram.bingo.primary.games_to_date,
                                                                    PRIMARY_CARD);
                                // don't send stratus a session update when the card is filled or they will automatically clear the card
                                if (!non_volatile_ram.bingo_card_filled)
                                    pCGameCommControl->GbSessionUpdate();
                            }
                            else
                            {
                                pCGameCommControl->updateGbInterfaceWithBingoWinningHandInfo(win_code, non_volatile_ram.bingo.secondary.handle_to_date,
                                                                              non_volatile_ram.bingo.secondary.games_to_date,
                                                                              SECONDARY_CARD);
                                pCGameCommControl->WinningBingoHand(sas_id,
                                                                    non_volatile_ram.member_id,
                                                                    win_code,
                                                                    non_volatile_ram.bingo.secondary.handle_to_date,
                                                                    non_volatile_ram.bingo.secondary.games_to_date,
                                                                    SECONDARY_CARD);
                                // don't send stratus a session update when the card is filled or they will automatically clear the card
                                if (!non_volatile_ram.bingo_card_filled)
                                    pCGameCommControl->GbSessionUpdate();
                            }
                        }

                        check4okBingoWin();
                        saveNonVolatileRam();
                        if (non_volatile_ram.bingo_card_filled)
                        {
	                        card_award = calculateCardAward (non_volatile_ram.bingo.primary.handle_to_date,
								                         (unsigned short)non_volatile_ram.bingo.primary.games_to_date);
                            pCGameCommControl->WinningBingoCard(sas_id, non_volatile_ram.member_id, card_award * ONEDOLLAR);
                        }
                    }

                    if (logout_after_4ok_win_check)
                    {
                        // don't log player out if the bingo card was filled
                        if (non_volatile_ram.bingo_card_filled == FALSE)
                        {
                            pCGameCommControl->AccountLogout(non_volatile_ram.handpay_amount);
                            pCGameCommControl->updateGbInterfaceData();
                        }
                    }

                }
            }

            //// convert IGT defined card values into Bally defined card values ////
            for (j=0; j < 5; j++)
            {
                if (((card_info_response->hand[j] & 0x70) > 0x30) ||
                    ((card_info_response->hand[j] & 0x0f) > 0x0c))
                    cards[j] = 0x0f;
                else
                {
                    // set the suit
                    switch(card_info_response->hand[j] & 0x70)
                    {
                        case 0: // spades
                            cards[j] = 0x30;
                            break;
                        case 0x10: // clubs
                            cards[j] = 0x00;
                            break;
                        case 0x20: // hearts
                            cards[j] = 0x20;
                            break;
                        case 0x30: // diamonds
                            cards[j] = 0x10;
                            break;
                    };
                    // set the rank
                    cards[j] = cards[j] + ((card_info_response->hand[j] & 0x0f) + 2);
                }

                if (non_volatile_ram.held_cards[j])
                    cards[j] |= 0x80; 
            }

//            queueCardDataInfo (card_info_response->hand);
            pCGameCommControl->processHandData(cards, current_game_wager, non_volatile_ram.current_game_denomination);
        }
    }
}

void CSAS_Comm::processHandpayInfoResponse (void)
{
    SAS_MESSAGE response_msg = response_message;
    HandpayInfoResponse* handpay_info_response = (HandpayInfoResponse*) response_msg.message;
    long win_amount;
    FileGameLock file_game_lock;
    MemoryTicketAdd ticket_add;
    CString message;
    unsigned short crcval;


    crcval = CRC(response_msg.message, sizeof(HandpayInfoResponse) - 2, 0);
    
    if (crcval == handpay_info_response->crc)
    {
        sendAck();
        dequeueSasMessage();
        clearSasMessage (&current_tx_message);
        if(pCGameCommControl->payThroughDispenser())
        {
                handpay_info_received = TRUE;

                ULONG total_win_amount_in_cents;

                total_win_amount_in_cents = bcd5ToUnsignedLong(handpay_info_response->amount[0],
                                                               handpay_info_response->amount[1],
                                                               handpay_info_response->amount[2],
                                                               handpay_info_response->amount[3],
                                                               handpay_info_response->amount[4]);

                total_win_amount_in_cents -= bcd2ToUnsignedLong(handpay_info_response->partial_pay[0],
                                                               handpay_info_response->partial_pay[1]);
                switch (handpay_info_response->level)
                {
                    case 0:
                    case 0x40:
                    case 0x80:
                        win_amount = total_win_amount_in_cents * non_volatile_ram.machine_accounting_denomination;
                        break;
                    default:
                        win_amount = total_win_amount_in_cents;
                        break;
                }

                if (win_amount && last_handpay_win_amount && (win_amount == last_handpay_win_amount))
                {
                    message.Format("CSAS_Comm::processHandpayInfoResponse - win_amount same as last_handpay_win_amount = %d", win_amount);
                    logMessage(message);
                    last_handpay_win_amount = win_amount;
                    queueSendHandpayInfo();
                }
                else if (win_amount)
                {
                    non_volatile_ram.handpay_amount = win_amount;

                    if ((handpay_info_response->level != 0x80) && (win_amount >= TAXABLE_LIMIT))
                        w2g_pay = 1;
                    else
                        w2g_pay = 0;

                    if ((non_volatile_ram.handpay_amount == bonus_jackpot_award) &&
                        (non_volatile_ram.handpay_amount / non_volatile_ram.machine_accounting_denomination > 4000))
                    {
                        bonus_jackpot_award = 0;
                        pCGameCommControl->ProcessJackpotMessage(non_volatile_ram.member_id, non_volatile_ram.handpay_amount, 0);
                        if ((getGbSystemState() == ON_AVAIL) || (getGbSystemState() == ON_UNAVAIL))
                        {
                            pCGameCommControl->AccountLogout(non_volatile_ram.handpay_amount);
                            pCGameCommControl->updateGbInterfaceData();
                        }
                    }
                    else if (non_volatile_ram.autopay_enabled)
                    {
                        // if the system will allow it and handpay reset to the credit meter is available,
                        // request that this method be used
                        if (non_volatile_ram.jackpot_to_credit_enabled && (handpay_info_response->reset_id == 1))
                        {
                            queueEnableJackpotHandpayResetMethod();
                            if (pCGameCommControl)
                            {
                                memset(&file_game_lock, 0, sizeof(file_game_lock));
                                getFileGameLock(&file_game_lock, non_volatile_ram.handpay_amount, TRUE);
                                ticket_add.ticket_id = file_game_lock.game_lock.ticket_id;
                                ticket_add.ticket_amount = file_game_lock.game_lock.ticket_amount;
                                ticket_add.player_account = file_game_lock.game_lock.player_account;
                                ticket_add.taxable_flag = file_game_lock.game_lock.taxable_flag;
                                memcpy(ticket_add.time_stamp, file_game_lock.game_lock.time_stamp, 6);
                                pCGameCommControl->ProcessGameLockMessage(&file_game_lock, &ticket_add);
                            }
                        }
                        else
                        {
                            if (non_volatile_ram.handpay_amount >= pCGameCommControl->getDispenserPayoutLimit() * 100)
                            {
        /*
                                log_event (EVT_HANDPAY, 0, 0);
                                if ((main_window->getGbSystemState() == ON_AVAIL) || (main_window->getGbSystemState() == ON_UNAVAIL))
                                {
                                    if ((!non_volatile_ram.marker_advanced_this_session || !non_volatile_ram.current_bank_meter) && !non_volatile_ram.marker_advance_pending)
                                    {
                                        log_event(EVT_GB_LOGOUT, 0, 0);
                                        main_window->changeGuiState(GET_LOGON_INFO);
                                        main_window->saveNonVolatileRam();
                                    }
                                }
        */
                                pCGameCommControl->ProcessJackpotMessage(non_volatile_ram.member_id, non_volatile_ram.handpay_amount, 0);
                                if ((getGbSystemState() == ON_AVAIL) || (getGbSystemState() == ON_UNAVAIL))
                                {
                                    pCGameCommControl->updateGbInterfaceData();
                                    if (!logout_after_4ok_win_check)
                                        queueSendCardInfo();
                                    logout_after_4ok_win_check = TRUE;
                                }

                            }
                            else
                            {
                                if (pCGameCommControl)
                                {
                                    memset(&file_game_lock, 0, sizeof(file_game_lock));
                                    getFileGameLock(&file_game_lock, non_volatile_ram.handpay_amount, FALSE);
                                    ticket_add.ticket_id = file_game_lock.game_lock.ticket_id;
                                    ticket_add.ticket_amount = file_game_lock.game_lock.ticket_amount;
                                    ticket_add.player_account = file_game_lock.game_lock.player_account;
                                    ticket_add.taxable_flag = file_game_lock.game_lock.taxable_flag;
                                    memcpy(ticket_add.time_stamp, file_game_lock.game_lock.time_stamp, 6);
                                    pCGameCommControl->ProcessGameLockMessage(&file_game_lock, &ticket_add);
                                }
                            }
                        }
                    }
                    else
                    {
        //                if (non_volatile_ram.handpay_amount >= HANDPAY_LIMIT)
                        if (non_volatile_ram.handpay_amount >= pCGameCommControl->getDispenserPayoutLimit() * 100)
                        {
                            /*
                            log_event (EVT_HANDPAY, 0, 0);
                            if ((main_window->getGbSystemState() == ON_AVAIL) ||
                            (main_window->getGbSystemState() == ON_UNAVAIL))
                            {
                                if ((!non_volatile_ram.marker_advanced_this_session || !non_volatile_ram.current_bank_meter) &&
                                !non_volatile_ram.marker_advance_pending)
                                {
                                    log_event(EVT_GB_LOGOUT, 0, 0);
                                    main_window->changeGuiState(GET_LOGON_INFO);
                                    main_window->saveNonVolatileRam();
                                }
                            }
                            */
                            pCGameCommControl->ProcessJackpotMessage(non_volatile_ram.member_id, non_volatile_ram.handpay_amount, 0);
                            if ((getGbSystemState() == ON_AVAIL) || (getGbSystemState() == ON_UNAVAIL))
                            {
                                pCGameCommControl->updateGbInterfaceData();
                                if (!logout_after_4ok_win_check)
                                    queueSendCardInfo();
                                logout_after_4ok_win_check = TRUE;
                            }

                        }
                        else
                        {
                            if (pCGameCommControl)
                            {
                                memset(&file_game_lock, 0, sizeof(file_game_lock));
                                getFileGameLock(&file_game_lock, non_volatile_ram.handpay_amount, FALSE);
                                ticket_add.ticket_id = file_game_lock.game_lock.ticket_id;
                                ticket_add.ticket_amount = file_game_lock.game_lock.ticket_amount;
                                ticket_add.player_account = file_game_lock.game_lock.player_account;
                                ticket_add.taxable_flag = file_game_lock.game_lock.taxable_flag;
                                memcpy(ticket_add.time_stamp, file_game_lock.game_lock.time_stamp, 6);
                                pCGameCommControl->ProcessGameLockMessage(&file_game_lock, &ticket_add);
                            }
                        }
                    }
                    pCGameCommControl->logSasHandpayPendingInfoMessage (handpay_info_response);
                    last_handpay_win_amount = win_amount;
                    queueSendHandpayInfo();
                }
                else
                {
                    last_handpay_win_amount = 0;
                    pCGameCommControl->logSasHandpayPendingInfoMessage (handpay_info_response);
                }
            }
            else
            {
                no_response_count = 0;
                message.Format("CSAS_Comm::processHandpayInfoResponse - sas id %d - CRC mismatch %d - %d", sas_id, crcval, handpay_info_response->crc);
                logMessage(message);
                queueSendHandpayInfo();
                if (total_queued_messages)
                    sendQueuedSasMessage();

            }
        }
}

void CSAS_Comm::getFileGameLock(FileGameLock* file_game_lock, long handpay_amount, unsigned char jackpot_to_credit)
{
    time_t current_time = time(NULL);
    struct tm current_time_struct;
    unsigned int ticket_number;

    // generate a ticket number
    rand_s(&ticket_number);

    // get time stamp info
    localtime_s(&current_time_struct, &current_time);

    file_game_lock->game_lock.ticket_id = ticket_number;
    file_game_lock->game_lock.ticket_amount = handpay_amount;
    file_game_lock->game_lock.time_stamp[0] = current_time_struct.tm_hour;
    file_game_lock->game_lock.time_stamp[1] = current_time_struct.tm_min;
    file_game_lock->game_lock.time_stamp[2] = current_time_struct.tm_sec;
    file_game_lock->game_lock.time_stamp[3] = current_time_struct.tm_mday;
    file_game_lock->game_lock.time_stamp[4] = current_time_struct.tm_mon;
    file_game_lock->game_lock.time_stamp[5] = current_time_struct.tm_year;

    file_game_lock->game_lock.taxable_flag = w2g_pay;

    file_game_lock->game_lock.jackpot_to_credit_flag = jackpot_to_credit;
    file_game_lock->game_lock.mux_id = 0;
    file_game_lock->game_lock.player_account = non_volatile_ram.member_id;
    file_game_lock->game_lock.status = 0;
    file_game_lock->game_lock.sas_id = sas_id;
}

void CSAS_Comm::processSasVersionResponse (void)
{
    SAS_MESSAGE response_msg = response_message;
    SasVersionResponse* sas_version_response
            = (SasVersionResponse*) response_msg.message;
    WORD message_crc;

    message_crc = (response_msg.message[sas_version_response->length + 4] << 8) + 
                  response_msg.message[sas_version_response->length + 3];

    if (CRC(response_msg.message, sas_version_response->length + 3, 0)
        == message_crc)
    {
        sendAck();
        dequeueSasMessage();
        clearSasMessage (&current_tx_message);

        memcpy(sas_version, sas_version_response->sas_version, 3); 

		if ((non_volatile_ram.manufacturer_id[0] == 'G') && (non_volatile_ram.manufacturer_id[1] == 'K') ||
		    (non_volatile_ram.manufacturer_id[0] == 'W') && (non_volatile_ram.manufacturer_id[1] == 'C'))
		{
		    message_delay_in_msecs = IGT_GK_MESSAGE_DELAY_IN_MSECS;

			memset(string1, 0, 7);
			memset(string2, 0, 7);
			memcpy(string1, sas_version, 3);
			sprintf_s(string2, sizeof(string1), "602");

			if (!strcmp(string1, string2))
				game_delay_in_msecs = 150;

			sprintf_s(string2, sizeof(string1), "603");

			if (!strcmp(string1, string2))
				game_delay_in_msecs = 150;

			sprintf_s(string2, sizeof(string1), "501");

			if (!strcmp(string1, string2))
				game_delay_in_msecs = 100;

			sprintf_s(string2, sizeof(string1), "502");

			if (!strcmp(string1, string2))
				game_delay_in_msecs = 200;

			sprintf_s(string2, sizeof(string1), "600");

			if (!strcmp(string1, string2))
				game_delay_in_msecs = 150;

			sprintf_s(string2, sizeof(string1), "601");

			if (!strcmp(string1, string2))
//				game_delay_in_msecs = 150;
				game_delay_in_msecs = 200;

		}


//        pCGameCommControl->setGameVersion(sas_version[0], sas_version[1]);
        
        if ( (sas_version[0] >= '6') &&
             !((non_volatile_ram.manufacturer_id[0] == 'A') && (non_volatile_ram.manufacturer_id[1] == 'T') )
           )
        {
            non_volatile_ram.sas_six_or_greater = TRUE;
//            queueAftStatusRequest();
//            queueAftRegistration(0);
            if (!((non_volatile_ram.manufacturer_id[0] == 'U') && (non_volatile_ram.manufacturer_id[1] == '1')))
                queueSendEnabledGameNumbers();
        }     
    }
}

void CSAS_Comm::processSelectedGameNumberResponse (void)
{
    SAS_MESSAGE response_msg = response_message;
    SelectedGameNumberResponse* selected_game_number_response
            = (SelectedGameNumberResponse*) response_msg.message;

    if (CRC(response_msg.message, sizeof(SelectedGameNumberResponse) - 2, 0)
        == selected_game_number_response->crc)
    {
        sendAck();
        dequeueSasMessage();
        clearSasMessage (&current_tx_message);

        if (bcd2ToUnsignedLong (selected_game_number_response->game_number[0],
                               selected_game_number_response->game_number[1]))
            queueSendGameConfiguration (selected_game_number_response->game_number);
    }
}

void CSAS_Comm::processGameConfigurationResponse (void)
{
    SAS_MESSAGE response_msg = response_message;
    GameConfigurationResponse* game_configuration_response
            = (GameConfigurationResponse*) response_msg.message;

    if (CRC(response_msg.message, sizeof(GameConfigurationResponse) - 2, 0)
        == game_configuration_response->crc)
    {
        sendAck();
        dequeueSasMessage();
        clearSasMessage (&current_tx_message);

        non_volatile_ram.max_bet = game_configuration_response->max_bet;

        memset(paytable_string, 0, 6);
        memcpy(paytable_string, game_configuration_response->paytable, 6);
        memcpy(base_percentage_string, game_configuration_response->base_percentage, 4);

        char manufacturer_id[2];
        memset(manufacturer_id, 0, 2);
		if (pCGameCommControl)
			pCGameCommControl->SendIdcGameEnteredMessage ((char*)&paytable_string, (char*)&manufacturer_id);

        determineGameType();
    }
}

void CSAS_Comm::determineGameType (void)
{

    if (((non_volatile_ram.manufacturer_id[0] == 'A') && (non_volatile_ram.manufacturer_id[1] == 'V')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'B') && (non_volatile_ram.manufacturer_id[1] == 'E')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'B') && (non_volatile_ram.manufacturer_id[1] == 'J')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'G') && (non_volatile_ram.manufacturer_id[1] == 'K')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'K') && (non_volatile_ram.manufacturer_id[1] == '+')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'K') && (non_volatile_ram.manufacturer_id[1] == 'E')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'K') && (non_volatile_ram.manufacturer_id[1] == 'N')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'M') && (non_volatile_ram.manufacturer_id[1] == '+')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'M') && (non_volatile_ram.manufacturer_id[1] == 'G')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'P') && (non_volatile_ram.manufacturer_id[1] == '+')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'P') && (non_volatile_ram.manufacturer_id[1] == 'K')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'P') && (non_volatile_ram.manufacturer_id[1] == 'P')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'P') && (non_volatile_ram.manufacturer_id[1] == 'S')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'R') && (non_volatile_ram.manufacturer_id[1] == 'S')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'S') && (non_volatile_ram.manufacturer_id[1] == 'G')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'S') && (non_volatile_ram.manufacturer_id[1] == 'P')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'S') && (non_volatile_ram.manufacturer_id[1] == 'S')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'T') && (non_volatile_ram.manufacturer_id[1] == '+')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'V') && (non_volatile_ram.manufacturer_id[1] == 'M')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'W') && (non_volatile_ram.manufacturer_id[1] == 'C')))
    {
        if (paytable_string[0] == 'M')
        {
            if (isSingleHandBase(paytable_string))
                single_hand_pokers_in_base_chip = TRUE;

			if ((non_volatile_ram.manufacturer_id[0] == 'G') && (non_volatile_ram.manufacturer_id[1] == 'K') ||
			    (non_volatile_ram.manufacturer_id[0] == 'W') && (non_volatile_ram.manufacturer_id[1] == 'C'))
			{
				memset(string1, 0, 7);
				memset(string2, 0, 7);
				memcpy(string1, paytable_string, 6);
				sprintf_s(string2, sizeof(string1), "M00526");

				if (!strcmp(string1, string2))
					game_delay_in_msecs = 100;

				sprintf_s(string2, sizeof(string1), "M00837");
				if (!strcmp(string1, string2))
					game_delay_in_msecs = 300;

				sprintf_s(string2, sizeof(string1), "M00888");
				if (!strcmp(string1, string2))
					game_delay_in_msecs = 300;

				sprintf_s(string2, sizeof(string1), "M00791");
				if (!strcmp(string1, string2))
//					game_delay_in_msecs = 150;
					game_delay_in_msecs = 200;

				sprintf_s(string2, sizeof(string1), "M01001");
				if (!strcmp(string1, string2))
					game_delay_in_msecs = 150;

				sprintf_s(string2, sizeof(string1), "M00542");
				if (!strcmp(string1, string2))
					game_delay_in_msecs = 100;

			}
        }
        else
        {
            switch (paytable_string[0])
            {
                case 'P':
                    poker_game_being_played = TRUE;
                    break;
                case 'C':
                    if (paytable_string[1] == 'K')
                        poker_game_being_played = TRUE;
                    if (paytable_string[1] == 'A')
                        poker_game_being_played = TRUE;
				    break;
                case 'B':
                    if (paytable_string[1] == 'K')
                        poker_game_being_played = TRUE;
                    if (paytable_string[1] == 'J')
                        bj_game_being_played = TRUE;
                    if (paytable_string[1] == 'A')
                        poker_game_being_played = TRUE;
                    break;
                case 'K':
                    if (paytable_string[1] == 'K')
                        poker_game_being_played = TRUE;
                    break;
                case 'G':
                    if (paytable_string[1] == 'A')
                        poker_game_being_played = TRUE;
                    break;
                case 'J':
                    bj_game_being_played = TRUE;
                    break;
            };
        }
    }
    else if (((non_volatile_ram.manufacturer_id[0] == 'B') && (non_volatile_ram.manufacturer_id[1] == '7')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'B') && (non_volatile_ram.manufacturer_id[1] == '9')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'B') && (non_volatile_ram.manufacturer_id[1] == 'S')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'B') && (non_volatile_ram.manufacturer_id[1] == 'V')))
    {
        switch (paytable_string[0])
        {
            case 'P':
                poker_game_being_played = TRUE;
                break;
            case 'B':
                bj_game_being_played = TRUE;
                break;
            case 'J':
                bj_game_being_played = TRUE;
                break;
            case 'S':
                if ((paytable_string[1] == 'B') && (paytable_string[2] == 'J'))
                    bj_game_being_played = TRUE;
                break;
        };
    }
    else if (paytable_string[0] == 'P')
        poker_game_being_played = TRUE;

    if (poker_game_being_played)
        queueGameDelayMessage();
}


void CSAS_Comm::processSendCurrentCredits (void)
{
    SAS_MESSAGE response_msg = response_message;
    CurrentCreditsResponse* current_bank_meter_response
            = (CurrentCreditsResponse*) response_msg.message;
	ULONG last_bank_meter = non_volatile_ram.current_bank_meter;

    if (CRC(response_msg.message, sizeof(CurrentCreditsResponse) - 2, 0)
        == current_bank_meter_response->crc)
    {
        sendAck();
        dequeueSasMessage();
        clearSasMessage (&current_tx_message);

        // NOTE: The "current credits" sent back by the SAS protocol is not the current credits at all,
        //       but is the current monetary units in cents.
        non_volatile_ram.current_bank_meter =  bcd4ToUnsignedLong (current_bank_meter_response->current_bank_meter[0],
                                               current_bank_meter_response->current_bank_meter[1],
                                               current_bank_meter_response->current_bank_meter[2],
                                               current_bank_meter_response->current_bank_meter[3]);
        non_volatile_ram.current_bank_meter = non_volatile_ram.current_bank_meter * non_volatile_ram.machine_accounting_denomination;

        if ((non_volatile_ram.current_bank_meter == 0) && !non_volatile_ram.middle_of_a_game)
        {
            queueRequestMeterData();
            check_game_meters = TRUE;
            pCGameCommControl->updateGbInterfaceData();
        }
        else
        {
            if ((last_bank_meter == 0) && non_volatile_ram.current_bank_meter)
                pCGameCommControl->updateGbInterfaceData();
        }
        pCGameCommControl->processCurrentCreditsMeter (non_volatile_ram.current_bank_meter);
    }
}

void CSAS_Comm::processMultiDenomMessage (void)
{
    SAS_MESSAGE response_msg = response_message;
    MultiDenomMessageResponse* multi_denom_message_response
            = (MultiDenomMessageResponse*) response_msg.message;

	WORD crc;
    unsigned char number_of_currently_enabled_games;
	BCD game_number[2];

    crc = response_msg.message[3 + multi_denom_message_response->length]  + 
          (response_msg.message[3 + multi_denom_message_response->length + 1] << 8);

    if (CRC(response_msg.message, 3 + multi_denom_message_response->length, 0) == crc)
    {
        sendAck();
        dequeueSasMessage();
        clearSasMessage (&current_tx_message);

        switch (multi_denom_message_response->base_command)
        {
            case SEND_ENABLED_GAME_NUMBERS:
                number_of_currently_enabled_games = response_msg.message[6];
                if (number_of_currently_enabled_games == 1)
                {
                    game_number[0] = response_msg.message[7];
                    game_number[1] = response_msg.message[8];
                    queueSendGameConfiguration (game_number);
                }
                break;
        };
    }
}

void CSAS_Comm::processTransferFundsResponse (void)
{
    SAS_MESSAGE response_msg = response_message;
    TransferCreditsResponse* transfer_credits_response
            = (TransferCreditsResponse*) response_msg.message;


    WORD message_crc;

    message_crc = (response_msg.message[transfer_credits_response->length + 4] << 8) + 
                  response_msg.message[transfer_credits_response->length + 3];

    if (CRC(response_msg.message, transfer_credits_response->length + 3, 0)
        == message_crc)
    {
        switch (transfer_credits_response->transfer_status)
        {
            case 0:
                sendAck();
                dequeueSasAftMessage();
                clearSasMessage (&current_tx_message);
                clearPendingSasAftMessage();
                break;
            case 0x40:
                sendAck();
                aft_pending = TRUE;
                dequeueSasAftMessage();
                clearSasMessage (&current_tx_message);
                break;
            case 0x81:  // transaction id not unique
            case 0x82:  // not valid transfer function (unsupported type, amount, index, etc.)
            case 0x95:  // Transaction ID not valid
                sendAck();
                dequeueSasAftMessage();
                clearSasMessage (&current_tx_message);
//                incrementTransactionNumber();
//                queueTransferFunds (bcd5ToUnsignedLong(transfer_credits_response->cashable_amount[0],
//                                                       transfer_credits_response->cashable_amount[1],
//                                                       transfer_credits_response->cashable_amount[2],
//                                                      transfer_credits_response->cashable_amount[3],
//                                                       transfer_credits_response->cashable_amount[4]),
//                                                       BONUS_COIN_OUT);
                queueTransferFunds (bcd5ToUnsignedLong(pending_sas_aft_message.message[6],
                                                       pending_sas_aft_message.message[7],
                                                       pending_sas_aft_message.message[8],
                                                       pending_sas_aft_message.message[9],
                                                       pending_sas_aft_message.message[10]),
                                                       IN_HOUSE);
                break;
            case 0x87:  // unable to complete transfer (game tilt, door open, cashout in progress, ...)
                sendAck();
                dequeueSasAftMessage();
                clearSasMessage (&current_tx_message);
                requeuePendingSasAftMessage();
                break;
            case 0x93: // asset number zero or does not match
                sendAck();
                dequeueSasAftMessage();
                clearSasMessage (&current_tx_message);
                queueAftStatusRequest(); // refetch the asset number
//                queueTransferFunds (bcd5ToUnsignedLong(transfer_credits_response->cashable_amount[0],
//                                                       transfer_credits_response->cashable_amount[1],
//                                                       transfer_credits_response->cashable_amount[2],
//                                                       transfer_credits_response->cashable_amount[3],
//                                                       transfer_credits_response->cashable_amount[4]),
//                                                       BONUS_COIN_OUT);
                queueTransferFunds (bcd5ToUnsignedLong(pending_sas_aft_message.message[6],
                                                       pending_sas_aft_message.message[7],
                                                       pending_sas_aft_message.message[8],
                                                       pending_sas_aft_message.message[9],
                                                       pending_sas_aft_message.message[10]),
                                                       IN_HOUSE);

                break;
            case 0xC0:
                sendAck();
                dequeueSasAftMessage();
                clearSasMessage (&current_tx_message);
                queueAftInterrogation();
                requeuePendingSasAftMessage();
                break;
            default:
                sendAck();
//                dequeueSasAftMessage();
//                clearSasMessage (&current_tx_message);
                break;
        }    

    }
}

void CSAS_Comm::processAftLockAndStatusResponse (void)
{
    SAS_MESSAGE response_msg = response_message;
    AftLockAndStatusResponse* aft_lock_and_status_response
            = (AftLockAndStatusResponse*) response_msg.message;

    if (CRC(response_msg.message, sizeof(AftLockAndStatusResponse) - 2, 0)
        == aft_lock_and_status_response->crc)
    {
        memcpy (asset_number, &response_msg.message[3], 4); // store the machine asset number

        sendAck();

        dequeueSasMessage();
        clearSasMessage (&current_tx_message);
    }
}

void CSAS_Comm::processAftRegistrationResponse (void)
{
    SAS_MESSAGE response_msg = response_message;
    AftRegistrationResponse* aft_registration_response
            = (AftRegistrationResponse*) response_msg.message;

    if (CRC(response_msg.message, sizeof(AftRegistrationResponse) - 2, 0)
        == (WORD)aft_registration_response->crc)
    {
        sendAck();
        dequeueSasMessage();
        clearSasMessage (&current_tx_message);


        switch (aft_registration_response->registration_status)
        {
            case 0:
                queueAftRegistration(1);
                break;
            case 0x80:
                queueAftRegistration(0);
                break;
        }

    }
}

void CSAS_Comm::processEnableJackpotHandpayResetMethodResponse (void)
{
    SAS_MESSAGE response_msg = response_message;
    EnableJackpotHandpayResetMethodResponse* response
            = (EnableJackpotHandpayResetMethodResponse*) response_msg.message;

    if (CRC(response_msg.message, sizeof(EnableJackpotHandpayResetMethodResponse) - 2, 0)
        == (WORD)response->crc)
    {
        sendAck();
        dequeueSasMessage();
        clearSasMessage (&current_tx_message);
    }
}

void CSAS_Comm::processHandpayResetResponse (void)
{
    SAS_MESSAGE response_msg = response_message;
    HandpayResetResponse* response
            = (HandpayResetResponse*) response_msg.message;

    if (CRC(response_msg.message, sizeof(HandpayResetResponse) - 2, 0)
        == (WORD)response->crc)
    {
        sendAck();
        dequeueSasMessage();
        clearSasMessage (&current_tx_message);

        // if reset code = 0 then the handpay was successfully reset
        if (!response->reset_code)
        {
//            non_volatile_ram.current_bank_meter = 0;
            queueSendCurrentCredits();
            handpay_pending = FALSE;

            last_meter_check_time = time(NULL);
            meter_check_msec_count = 5;
            check_game_meters = TRUE;

        }
    }
}

bool CSAS_Comm::isSingleHandBase (char* base_string)
{
    bool single_hand_base = TRUE;
    int i;

    for (i=0; i < 6; i++)
    {
        if (single_hand_bases[0][i] != base_string[i])
        {
            single_hand_base = FALSE;
            break;
        }
    }

    if (!single_hand_base)
    {
        single_hand_base = TRUE;
        for (i=0; i < 6; i++)
        {
            if (single_hand_bases[1][i] != base_string[i])
            {
                single_hand_base = FALSE;
                break;
            }
        }
    }

    return single_hand_base;
}

void CSAS_Comm::clearSasMessage (SAS_MESSAGE* msg)
{
//    WaitForSingleObject(message_queue_semiphore, MAX_EVENT_WAIT_TIMOUT);
    memset (msg, 0, sizeof(SAS_MESSAGE));
//    ReleaseSemaphore(message_queue_semiphore, 1, NULL);
}
/*
static BOOL waitForCommEvent (HANDLE hFile)
{
	DWORD fdwCommMask;
	DWORD error;
    BOOL return_value;
	DWORD Mask;
    COMSTAT stat;

	return_value = WaitCommEvent (hFile, &fdwCommMask, 0);

    if (fdwCommMask & EV_BREAK)
        Mask |= EV_BREAK;

    if (fdwCommMask & EV_ERR)
    {
        if (ClearCommError(hFile, &error, &stat))
        {
            switch (error)
            {
                case CE_BREAK:
                    Mask |= EV_ERR;
                    break;
                case CE_FRAME:
                    Mask |= EV_ERR;
                    break;
                case CE_IOE:
                    Mask |= EV_ERR;
                    break;
                case CE_MODE:
                    Mask |= EV_ERR;
                    break;
                case CE_OVERRUN:
                    Mask |= EV_ERR;
                    break;
                case CE_RXOVER:
                    Mask |= EV_ERR;
                    break;
                case CE_RXPARITY:
                    Mask |= EV_ERR;
                    break;
                case CE_TXFULL:
                    Mask |= EV_ERR;
                    break;
            }
        }
    }

    if (fdwCommMask & EV_RXCHAR)
        Mask |= EV_RXCHAR;

    if (fdwCommMask & EV_TXEMPTY)
        Mask |= EV_TXEMPTY;

    if (return_value == 0)
    {
        error = GetLastError();
		ReportCommError(TEXT("waiting for comm event."));
    }

    return return_value;
}
*/
unsigned short CSAS_Comm::CRC (unsigned char *s, int len, unsigned short crcval)
{
	register unsigned short c, q;
 
	for(; len; len--)
	{
		c = *s++;
        q = (crcval ^ c) & 017;
		crcval = (crcval >> 4) ^ (q * 010201);
        q = (crcval ^ (c >> 4)) & 017;
        crcval = (crcval >> 4) ^ (q * 010201);
	}

	return(crcval);
}

void CSAS_Comm::queueEnableEventReporting (bool flag)
{
    unsigned char* crc_ptr;
	unsigned short crc;
    unsigned char* crc_short_pointer;
    SAS_MESSAGE sas_message;

    sas_message.message[0] = sas_id; // game address
    sas_message.message[1] = ENABLE_DISABLE_EVENT_REPORTING; // command
    if (flag)
        sas_message.message[2] = 0x01;    // enable/disable real time event reporting
    else
        sas_message.message[2] = 0;    // enable/disable real time event reporting

    crc_ptr = (unsigned char*)&sas_message.message[3];

    crc = CRC (sas_message.message, 3, 0);
    crc_short_pointer = (unsigned char*)&crc;

    *crc_ptr = *crc_short_pointer;
    crc_ptr++;
    crc_short_pointer++;
    *crc_ptr = *crc_short_pointer;

    sas_message.message_size = 5;
    sas_message.poll_type = S_POLL;

    queueSasMessage (&sas_message);
}

void CSAS_Comm::queueSendGameConfiguration (BCD* game_number)
{
    unsigned char* crc_ptr;
	unsigned short crc;
    unsigned char* crc_short_pointer;
    SAS_MESSAGE sas_message;

    sas_message.message[0] = sas_id; // game address
    sas_message.message[1] = SEND_GAME_CONFIGURATION; // command
    sas_message.message[2] = game_number[0];
    sas_message.message[3] = game_number[1];

    crc_ptr = (unsigned char*)&sas_message.message[4];

    crc = CRC (sas_message.message, 4, 0);
    crc_short_pointer = (unsigned char*)&crc;

    *crc_ptr = *crc_short_pointer;
    crc_ptr++;
    crc_short_pointer++;
    *crc_ptr = *crc_short_pointer;

    sas_message.message_size = 6;
    sas_message.poll_type = M_POLL;

    queueSasMessage (&sas_message);
}

void CSAS_Comm::queueSendSelectedGameNumber (void)
{
    SAS_MESSAGE sas_message;

    sas_message.message[0] = sas_id; // game address
    sas_message.message[1] = SEND_SELECTED_GAME_NUMBER; // command

    sas_message.message_size = 2;
    sas_message.poll_type = R_POLL;

    queueSasMessage (&sas_message);
}

void CSAS_Comm::queueSendEnabledGameNumbers (void)
{
    unsigned char* crc_ptr;
	unsigned short crc;
    unsigned char* crc_short_pointer;
    SAS_MESSAGE sas_message;

    sas_message.message[0] = sas_id; // game address
    sas_message.message[1] = MULTI_DENOM_PREAMBLE; // command
    sas_message.message[2] = 2; // number of bytes following, not including the CRC
    sas_message.message[3] = 0; // denomination code
    sas_message.message[4] = SEND_ENABLED_GAME_NUMBERS; // base command

    crc_ptr = (unsigned char*)&sas_message.message[5];

    crc = CRC (sas_message.message, 5, 0);
    crc_short_pointer = (unsigned char*)&crc;

    *crc_ptr = *crc_short_pointer;
    crc_ptr++;
    crc_short_pointer++;
    *crc_ptr = *crc_short_pointer;

    sas_message.message_size = 7;
    sas_message.poll_type = R_POLL;

    queueSasMessage (&sas_message);
}

void CSAS_Comm::queueRequestMeterData (void)
{
    SAS_MESSAGE sas_message;

    sas_message.message[0] = sas_id; // game address
    sas_message.message[1] = REQUEST_GAME_METER_DATA; // command


    sas_message.message_size = 2;
    sas_message.poll_type = R_POLL;

    queueSasMessage (&sas_message);

    last_meter_check_time = time(NULL);
}

void CSAS_Comm::queueRequestBillMeterData (void)
{
    SAS_MESSAGE sas_message;

    sas_message.message[0] = sas_id; // game address
    sas_message.message[1] = REQUEST_BILL_METER_DATA; // command

    sas_message.message_size = 2;
    sas_message.poll_type = R_POLL;

    queueSasMessage (&sas_message);
}

void CSAS_Comm::queueSendSelectedGameMeterData (BCD* game_number)
{
    unsigned char* crc_ptr;
	unsigned short crc;
    unsigned char* crc_short_pointer;
    SAS_MESSAGE sas_message;

    sas_message.message[0] = sas_id; // game address
    sas_message.message[1] = SEND_SELECTED_GAME_METERS; // command
    sas_message.message[2] = 6; // length
    sas_message.message[3] = game_number[0];
    sas_message.message[4] = game_number[1];

    sas_message.message[5] = 5; // games played
    sas_message.message[6] = 6; // games won
    sas_message.message[7] = 0; // cents played
    sas_message.message[8] = 1; // cents won

    crc_ptr = (unsigned char*)&sas_message.message[9];

    crc = CRC (sas_message.message, 9, 0);
    crc_short_pointer = (unsigned char*)&crc;

    *crc_ptr = *crc_short_pointer;
    crc_ptr++;
    crc_short_pointer++;
    *crc_ptr = *crc_short_pointer;

    sas_message.message_size = 11;
    sas_message.poll_type = M_POLL;

    queueSasMessage (&sas_message);
}

void CSAS_Comm::queueRequestMachineInfo (void)
{
    SAS_MESSAGE sas_message;

    sas_message.message[0] = sas_id; // game address
    sas_message.message[1] = REQUEST_MACHINE_INFO; // command


    sas_message.message_size = 2;
    sas_message.poll_type = R_POLL;

    queueSasMessage (&sas_message);
}

void CSAS_Comm::queueSendCardInfo (void)
{
    SAS_MESSAGE sas_message;

    sas_message.message[0] = sas_id; // game address
    sas_message.message[1] = SEND_CARD_INFO; // command


    sas_message.message_size = 2;
    sas_message.poll_type = R_POLL;

    if (poker_game_being_played)
	    queueCardInfoSasMessage (&sas_message);
}

void CSAS_Comm::queueSendHandpayInfo (void)
{
    SAS_MESSAGE sas_message;

    sas_message.message[0] = sas_id; // game address
    sas_message.message[1] = SEND_HANDPAY_INFO; // command


    sas_message.message_size = 2;
    sas_message.poll_type = R_POLL;

    queueSasMessage (&sas_message);
}

void CSAS_Comm::queueSendSasVersion (void)
{
    SAS_MESSAGE sas_message;

    sas_message.message[0] = sas_id; // game address
    sas_message.message[1] = SEND_SAS_VERSION; // command


    sas_message.message_size = 2;
    sas_message.poll_type = R_POLL;

    queueSasMessage (&sas_message);
}

void CSAS_Comm::queueAftRegistration (unsigned char registration_code)
{
    unsigned char* crc_ptr;
	unsigned short crc;
    unsigned char* crc_short_pointer;
    SAS_MESSAGE sas_message;

    memset (&sas_message, 0, sizeof(SAS_MESSAGE));

    sas_message.message[0] = sas_id; // game address
    sas_message.message[1] = AFT_REGISTRATION; // command
    sas_message.message[2] = 29; // length

    sas_message.message[3] = registration_code; // registration code

    memcpy (&sas_message.message[4], asset_number, 4); // copy stored machine asset number

    sas_message.message[8] = 1; // registration key - 20 binary 
    sas_message.message[27] = 0; // registration key - 20 binary 

    sas_message.message[28] = 0; // POS id - 4 binary 

    crc_ptr = (unsigned char*)&sas_message.message[32];

    crc = CRC (sas_message.message, 32, 0);
    crc_short_pointer = (unsigned char*)&crc;

    *crc_ptr = *crc_short_pointer;
    crc_ptr++;
    crc_short_pointer++;
    *crc_ptr = *crc_short_pointer;

    sas_message.message_size = 34;
    sas_message.poll_type = S_POLL;

    queueSasMessage (&sas_message);
}

void CSAS_Comm::queueHandpayReset (void)
{
    unsigned char* crc_ptr;
	unsigned short crc;
    unsigned char* crc_short_pointer;
    SAS_MESSAGE sas_message;

    sas_message.message[0] = sas_id; // game address
    sas_message.message[1] = HANDPAY_RESET; // command

    crc_ptr = (unsigned char*)&sas_message.message[2];

    crc = CRC (sas_message.message, 2, 0);
    crc_short_pointer = (unsigned char*)&crc;

    *crc_ptr = *crc_short_pointer;
    crc_ptr++;
    crc_short_pointer++;
    *crc_ptr = *crc_short_pointer;

    sas_message.message_size = 4;
    sas_message.poll_type = S_POLL;

    queueSasMessage (&sas_message);
}

void CSAS_Comm::queueEnableJackpotHandpayResetMethod (void)
{
    unsigned char* crc_ptr;
	unsigned short crc;
    unsigned char* crc_short_pointer;
    SAS_MESSAGE sas_message;

    sas_message.message[0] = sas_id; // game address
    sas_message.message[1] = ENABLE_JACKPOT_HANDPAY_RESET_METHOD; // command
    sas_message.message[2] = 1; // reset to the credit meter

    crc_ptr = (unsigned char*)&sas_message.message[3];

    crc = CRC (sas_message.message, 3, 0);
    crc_short_pointer = (unsigned char*)&crc;

    *crc_ptr = *crc_short_pointer;
    crc_ptr++;
    crc_short_pointer++;
    *crc_ptr = *crc_short_pointer;

    sas_message.message_size = 5;
    sas_message.poll_type = S_POLL;

    queueSasMessage (&sas_message);
}

void CSAS_Comm::queueAftStatusRequest (void)
{
    unsigned char* crc_ptr;
	unsigned short crc;
    unsigned char* crc_short_pointer;
    SAS_MESSAGE sas_message;

    memset (&sas_message, 0, sizeof(SAS_MESSAGE));

    sas_message.message[0] = sas_id; // game address
    sas_message.message[1] = AFT_STATUS; // command
    sas_message.message[2] = 0xff; // lock code - request current status

    crc_ptr = (unsigned char*)&sas_message.message[6];

    crc = CRC (sas_message.message, 6, 0);
    crc_short_pointer = (unsigned char*)&crc;

    *crc_ptr = *crc_short_pointer;
    crc_ptr++;
    crc_short_pointer++;
    *crc_ptr = *crc_short_pointer;

    sas_message.message_size = 8;
    sas_message.poll_type = S_POLL;

    queueSasMessage (&sas_message);
}

void CSAS_Comm::queueTransferFunds (ULONG amount, TRANSFER_TYPE transfer_type)
{
    if (amount)
    {
        unsigned char* crc_ptr;
    	unsigned short crc;
        unsigned char* crc_short_pointer;
        SAS_MESSAGE sas_message;

        memset (&sas_message, 0, sizeof(SAS_MESSAGE));

        sas_message.message[0] = sas_id; // game address
        sas_message.message[1] = TRANSFER_FUNDS; // command
        sas_message.message[2] = 52;    // length not including CRC
        sas_message.message[3] = 0;   // transfer code = full transfer only
        sas_message.message[4] = 0;    // transaction index

        sas_message.message[5] = (unsigned char)transfer_type;    // transfer type

        unsignedLongToBcd5 (amount, &sas_message.message[6]);  // cashable amount 5 BCD

//        sas_message.message[10] = ;    // restricted amount - 5 BCD

//        unsignedLongToBcd5 (amount, &sas_message.message[16]);  // nonrestricted amount 5 BCD

        sas_message.message[21] = 2;    // transfer flags

        memcpy (&sas_message.message[22], asset_number, 4); // store the machine asset number

//        sas_message.message[26] = 0;    // registration key - 20 binary
        sas_message.message[26] = 1;    // registration key - 20 binary

        sas_message.message[46] = 1;    // transaction id length

        sas_message.message[47] = incrementTransactionNumber();    // transaction id

//        sas_message.message[48] = 0;    // expiration - 4 BCD
//        sas_message.message[52] = 0;    // pool id - 2 binary
        sas_message.message[54] = 0;    // receipt data length


        crc_ptr = (unsigned char*)&sas_message.message[55];

        crc = CRC (sas_message.message, 55, 0);
        crc_short_pointer = (unsigned char*)&crc;

        *crc_ptr = *crc_short_pointer;
        crc_ptr++;
        crc_short_pointer++;
        *crc_ptr = *crc_short_pointer;

        sas_message.message_size = 57;
        sas_message.poll_type = S_POLL;

//        queueSasMessage (&sas_message);
        queueSasAftMessage (&sas_message);
        aft_pending = FALSE;
    }
}

void CSAS_Comm::queueInitiateLegacyBonus (ULONG amount, bool redemption)
{
    if (amount)
    {
        unsigned char* crc_ptr;
    	unsigned short crc;
        unsigned char* crc_short_pointer;
        SAS_MESSAGE sas_message;

        point_redemption = redemption;

        memset (&sas_message, 0, sizeof(SAS_MESSAGE));

        sas_message.message[0] = sas_id; // game address
        sas_message.message[1] = INITIATE_LEGACY_BONUS; // command

//        unsignedLongToBcd4 (amount, &sas_message.message[2]); // cashable amount 4 BCD
        if (non_volatile_ram.machine_accounting_denomination)
            unsignedLongToBcd4 ((amount / non_volatile_ram.machine_accounting_denomination), &sas_message.message[2]); // cashable amount 4 BCD
        else
            unsignedLongToBcd4 ((amount), &sas_message.message[2]); // cashable amount 4 BCD
    
        sas_message.message[6] = 0; // tax status

        crc_ptr = (unsigned char*)&sas_message.message[7];

        crc = CRC (sas_message.message, 7, 0);
        crc_short_pointer = (unsigned char*)&crc;

        *crc_ptr = *crc_short_pointer;
        crc_ptr++;
        crc_short_pointer++;
        *crc_ptr = *crc_short_pointer;

        sas_message.message_size = 9;
        sas_message.poll_type = S_POLL;

        queueSasMessage (&sas_message);

//        queueSendCurrentCredits();
        non_volatile_ram.current_bank_meter = non_volatile_ram.current_bank_meter + amount;
        pCGameCommControl->updateGbInterfaceData();
    }
}

void CSAS_Comm::putMoneyOnTheGame (ULONG amount)
{
    if (sas_version[0] >= '6')
        queueTransferFunds (amount, IN_HOUSE);
    else
        queueInitiateLegacyBonus (amount, FALSE);
}

void CSAS_Comm::queueGameDelayMessage (void)
{
    unsigned char* crc_ptr;
    unsigned short crc;
    unsigned char* crc_short_pointer;
    SAS_MESSAGE sas_message;

    sas_message.message[0] = sas_id; // game address
    sas_message.message[1] = GAME_DELAY; // command

    // do not delay game for this group of manufacturers because they don't have poker games
    if (((non_volatile_ram.manufacturer_id[0] == 'A') && (non_volatile_ram.manufacturer_id[1] == 'T')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'W') && (non_volatile_ram.manufacturer_id[1] == 'M')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'K') && (non_volatile_ram.manufacturer_id[1] == 'G')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'V') && (non_volatile_ram.manufacturer_id[1] == 'I')) ||
        ((non_volatile_ram.manufacturer_id[0] == 'S') && (non_volatile_ram.manufacturer_id[1] == 'F')))
        sas_message.message[2] = 0; // delay time in units of 100ms
    else
    	// game delay to insure valid card data
        sas_message.message[2] = (game_delay_in_msecs / 100); // delay time in units of 100ms

    sas_message.message[3] = 0; // delay time in units of 100ms

    crc_ptr = (unsigned char*)&sas_message.message[4];

    crc = CRC (sas_message.message, 4, 0);
    crc_short_pointer = (unsigned char*)&crc;

    *crc_ptr = *crc_short_pointer;
    crc_ptr++;
    crc_short_pointer++;
    *crc_ptr = *crc_short_pointer;

    sas_message.message_size = 6;
    sas_message.poll_type = S_POLL;

	queueSasMessage (&sas_message);

}

void CSAS_Comm::queueSendCurrentCredits (void)
{
    queueSingleMeterPoll (SEND_CURRENT_CREDITS);
}

void CSAS_Comm::queueAftInterrogation (void)
{
    if (!aftInterrogationPollQueued())
    {
        unsigned char* crc_ptr;
    	unsigned short crc;
        unsigned char* crc_short_pointer;
        SAS_MESSAGE sas_message;

        memset (&sas_message, 0, sizeof(SAS_MESSAGE));

        sas_message.message[0] = sas_id; // game address
        sas_message.message[1] = TRANSFER_FUNDS; // command
        sas_message.message[2] = 2;    // length not including CRC
        sas_message.message[3] = 0xff;   // transfer code = interrogation request
        sas_message.message[4] = 0;    // transaction index

        crc_ptr = (unsigned char*)&sas_message.message[5];

        crc = CRC (sas_message.message, 5, 0);
        crc_short_pointer = (unsigned char*)&crc;

        *crc_ptr = *crc_short_pointer;
        crc_ptr++;
        crc_short_pointer++;
        *crc_ptr = *crc_short_pointer;

        sas_message.message_size = 7;
        sas_message.poll_type = S_POLL;

        queueSasAftMessage (&sas_message);
    }
}

void CSAS_Comm::queueSendTotalHandPaidCancelledCredits (void)
{
    unsigned char* crc_ptr;
	unsigned short crc;
    unsigned char* crc_short_pointer;
    SAS_MESSAGE sas_message;

    memset (&sas_message, 0, sizeof(SAS_MESSAGE));

    sas_message.message[0] = sas_id; // game address
    sas_message.message[1] = SEND_TOTAL_HAND_PAID_CANCELLED_CREDITS; // command
    sas_message.message[2] = 0;
    sas_message.message[3] = 0;

    crc_ptr = (unsigned char*)&sas_message.message[4];

    crc = CRC (sas_message.message, 4, 0);
    crc_short_pointer = (unsigned char*)&crc;

    *crc_ptr = *crc_short_pointer;
    crc_ptr++;
    crc_short_pointer++;
    *crc_ptr = *crc_short_pointer;

    sas_message.message_size = 6;
    sas_message.poll_type = M_POLL;

    queueSasMessage (&sas_message);
}

//void CSAS_Comm::sendDateAndTimeMessage (void)
void CSAS_Comm::sendDateAndTimeMessage (BYTE month, BYTE day, BYTE year, BYTE hour, BYTE minute, BYTE second)
{
    unsigned char* crc_ptr;
	unsigned short crc;
    unsigned char* crc_short_pointer;
    SAS_MESSAGE sas_message;
    SYSTEMTIME system_time;

    sas_message.message[0] = sas_id; // global broadcast
    sas_message.message[1] = UPDATE_DATE_AND_TIME; // command

    sas_message.message[2] = month; // MM

    sas_message.message[3] = day; // DD

    sas_message.message[4] = 0x20; // YY

    sas_message.message[5] = year; // YY

    sas_message.message[6] = hour; // HH

    sas_message.message[7] = minute; // MM

    sas_message.message[8] = second; // SS

    crc_ptr = (unsigned char*)&sas_message.message[9];

    crc = CRC (sas_message.message, 9, 0);
    crc_short_pointer = (unsigned char*)&crc;

    *crc_ptr = *crc_short_pointer;
    crc_ptr++;
    crc_short_pointer++;
    *crc_ptr = *crc_short_pointer;

    sas_message.message_size = 11;
    sas_message.poll_type = S_POLL;

//    sendSerialMessage (&sas_message);
    queueSasMessage (&sas_message);
}

//static void sendSerialMessage (unsigned char* tx_msg, UINT len)
void CSAS_Comm::sendSerialMessage (SAS_MESSAGE* sas_message)
{

    if (sas_message->message_size <= MAX_SAS_MSG_LENGTH)
    {
//        memcpy(current_tx_message.message, sas_message->message, sas_message->message_size);
        memcpy(&current_tx_message, sas_message, sizeof(SAS_MESSAGE));
        current_tx_message.message_ready = TRUE;
        clearSasMessage (&response_message);

        sendFirstMessageByte (current_tx_message.message[0]);
        if (sas_message->message_size > 1)
        {
            sendRemainingMessageBytes (&current_tx_message.message[1], sas_message->message_size - 1);
        }

    }
}

// sets the wakup bit high and sends the byte
void CSAS_Comm::sendFirstMessageByte (unsigned char data)
{
    FT_STATUS status;
    UINT failure_counter = 0;
    DWORD bytes_written;
    bool change_parity = FALSE;
    unsigned char sum_of_bits;

    sum_of_bits = (data & LSB_MASK) +
                  ((data >> 1) & LSB_MASK) +
                  ((data >> 2) & LSB_MASK) +
                  ((data >> 3) & LSB_MASK) +
                  ((data >> 4) & LSB_MASK) +
                  ((data >> 5) & LSB_MASK) +
                  ((data >> 6) & LSB_MASK) +
                  ((data >> 7) & LSB_MASK);

    if (sum_of_bits % 2)
    {
        if (dcb.Parity != EVENPARITY)
        {
    	    dcb.Parity = EVENPARITY;
            change_parity = TRUE;
        }
    }
    else
    {
        if (dcb.Parity != ODDPARITY)
        {
    	    dcb.Parity = ODDPARITY;
            change_parity = TRUE;
        }
    }

    if (change_parity)
    {
        if (dcb.Parity == ODDPARITY)
        {
            status = SetDataCharacteristics(m_ftHandle, FT_BITS_8, FT_STOP_BITS_1, FT_PARITY_ODD); 
            while ((status != FT_OK) && !stop_comm_thread && (status != FT_IO_ERROR))
            {
                Sleep (1);
                status = SetDataCharacteristics(m_ftHandle, FT_BITS_8, FT_STOP_BITS_1, FT_PARITY_ODD); 
            }
        }
        else
        {
            status = SetDataCharacteristics(m_ftHandle, FT_BITS_8, FT_STOP_BITS_1, FT_PARITY_EVEN); 
            while ((status != FT_OK) && !stop_comm_thread && (status != FT_IO_ERROR))
            {
                Sleep (1);
                status = SetDataCharacteristics(m_ftHandle, FT_BITS_8, FT_STOP_BITS_1, FT_PARITY_EVEN); 
            }
        }


        if (status != FT_IO_ERROR)
        {
            status = Write((LPVOID)&data, 1, &bytes_written);
            if (bytes_written != 1)
    		    ReportCommError(TEXT("sendFirstMessageByte"));
            if (status != FT_OK)
                pCGameCommControl->connection_check_second_delay = 0;
            else
                pCGameCommControl->connection_check_second_delay = 60;

            Sleep (1);
        }
        else
            pCGameCommControl->connection_check_second_delay = 0;


    }
    else
    {
        status = Write((LPVOID)&data, 1, &bytes_written);

        if (bytes_written != 1)
    		ReportCommError(TEXT("sendFirstMessageByte"));
        if (status != FT_OK)
            pCGameCommControl->connection_check_second_delay = 0;
        else
            pCGameCommControl->connection_check_second_delay = 60;
        Sleep (1);
    }
}

// sets the wakup bit low and sends the len bytes
void CSAS_Comm::sendRemainingMessageBytes (unsigned char* tx_msg, UINT len)
{
    bool parity_change = FALSE;
    UINT failure_counter = 0;
    DWORD bytes_written;
    UINT i;
    unsigned char sum_of_bits;
    FT_STATUS status;


    for (i=0; i < len; i++)
    {
        sum_of_bits = (tx_msg[i] & LSB_MASK) +
                      ((tx_msg[i] >> 1) & LSB_MASK) +
                      ((tx_msg[i] >> 2) & LSB_MASK) +
                      ((tx_msg[i] >> 3) & LSB_MASK) +
                      ((tx_msg[i] >> 4) & LSB_MASK) +
                      ((tx_msg[i] >> 5) & LSB_MASK) +
                      ((tx_msg[i] >> 6) & LSB_MASK) +
                      ((tx_msg[i] >> 7) & LSB_MASK);

        if (sum_of_bits % 2)
		{
			if(dcb.Parity != ODDPARITY)
            {
            	dcb.Parity = ODDPARITY;
                parity_change = TRUE;
            }
            else
                parity_change = FALSE;
        }
        else
        {
            if (dcb.Parity != EVENPARITY)
            {
            	dcb.Parity = EVENPARITY;
                parity_change = TRUE;
            }
            else
                parity_change = FALSE;
        }

        if (parity_change)
        {

            if (dcb.Parity == ODDPARITY)
            {
                status = SetDataCharacteristics(m_ftHandle, FT_BITS_8, FT_STOP_BITS_1, FT_PARITY_ODD);
                while ((status != FT_OK) && !stop_comm_thread && (status != FT_IO_ERROR))
                {
                    Sleep (1);
                    status = SetDataCharacteristics(m_ftHandle, FT_BITS_8, FT_STOP_BITS_1, FT_PARITY_ODD);
                }
            }
            else
            {
                status = SetDataCharacteristics(m_ftHandle, FT_BITS_8, FT_STOP_BITS_1, FT_PARITY_EVEN);
                while ((status != FT_OK) && !stop_comm_thread && (status != FT_IO_ERROR))
                {
                    Sleep (1);
                    status = SetDataCharacteristics(m_ftHandle, FT_BITS_8, FT_STOP_BITS_1, FT_PARITY_EVEN);
                }
            }

            if (status != FT_IO_ERROR)
            {
                status = Write((LPVOID)&tx_msg[i], 1, &bytes_written);
                if (bytes_written != 1)
    		        ReportCommError(TEXT("sendRemainingMessageBytes"));
                if (status != FT_OK)
                    pCGameCommControl->connection_check_second_delay = 0;
                else
                    pCGameCommControl->connection_check_second_delay = 60;
                Sleep (1);
            }
            else
                pCGameCommControl->connection_check_second_delay = 0;


        }
        else
        {
            status = Write((LPVOID)&tx_msg[i], 1, &bytes_written);
            if (bytes_written != 1)
    		    ReportCommError(TEXT("sendRemainingMessageBytes"));
            if (status != FT_OK)
                pCGameCommControl->connection_check_second_delay = 0;
            else
                pCGameCommControl->connection_check_second_delay = 60;
            Sleep (1);
        }
    }
}

void CSAS_Comm::sendGeneralPoll (unsigned char game_address)
{
//    unsigned char tx_msg[1];
    SAS_MESSAGE sas_message;

    sas_message.message[0] = game_address | 0x80;      // game address

    sas_message.poll_type = GENERAL_POLL;
    sas_message.message_size = 1;
    sendSerialMessage (&sas_message);

}

void CSAS_Comm::sendBroadcastPoll (void)
{
    SAS_MESSAGE sas_message;

    sas_message.message[0] = 0 | 0x80;      // global

//    current_tx_message.poll_type = G_POLL;
    sas_message.poll_type = G_POLL;
    sas_message.message_size = 1;
    sendSerialMessage (&sas_message);
}

void CSAS_Comm::sendAck (void)
{
    sendBroadcastPoll();
    delay_next_sas_poll = TRUE;
}

void CSAS_Comm::sendAck2 (void)
{
    sendGeneralPoll(sas_id + 1);
}

void CSAS_Comm::queueSingleMeterPoll (UCHAR command)
{
    SAS_MESSAGE sas_message;

    sas_message.message[0] = sas_id;
    sas_message.message[1] = command;

    sas_message.message_size = 2;
    sas_message.poll_type = R_POLL;

    queueSasMessage (&sas_message);
}

// returns bytes read
DWORD CSAS_Comm::getSerialData (unsigned char* lpBuffer)
{
	DWORD bytes_read = 0;
    DWORD AmountInRxQueue;

    if (!stop_comm_thread)
    {
        GetQueueStatus(&AmountInRxQueue);

        if (AmountInRxQueue >= (MAX_SAS_MSG_LENGTH - receive_buffer_index))
        {
            Read((LPVOID)lpBuffer, (MAX_SAS_MSG_LENGTH - receive_buffer_index), &bytes_read);
            Purge(FT_PURGE_RX);
        }
        else
            Read((LPVOID)lpBuffer, AmountInRxQueue, &bytes_read);
    }

    return bytes_read; 
}

UINT CSAS_Comm::getEventMessageSize (UINT exception_code)
{
    UINT message_size = 0;

    switch (exception_code)
    {
        case SINGLE_METER_ACCOUNTING:
            message_size = 8;
            break;
        case ENABLE_DISABLE_EVENT_REPORTING:
            message_size = 5;
            break;
        case BILL_ACCEPTED:
            message_size = 11;
            break;
        case SYSTEM_BONUS_PAY:
            message_size = 15;
            break;
        case GAME_START:
            message_size = 13;
            break;
        case GAME_END:
            message_size = 9;
            break;
        case REEL_N_STOPPED:
            message_size = 7;
            break;
        case GAME_RECALL_ENTERED:
            message_size = 9;
            break;
        case CARD_HELD_NOT_HELD:
            message_size = 6;
            break;
        case GAME_SELECTED:
            message_size = 7;
            break;
        default:
            message_size = 5;
            break;
    }

    return message_size;
}

UINT CSAS_Comm::getRPollMessageSize (void)
{
    UINT message_size = 0;

    switch (receive_buffer[1])
    {
        case REQUEST_GAME_METER_DATA:
            message_size = sizeof(MeterDataRequestResponse);
            break;
        case REQUEST_BILL_METER_DATA:
            message_size = sizeof(BillMeterDataRequestResponse);
            break;
        case REQUEST_MACHINE_INFO:
            message_size = sizeof(MachineInfoRequestResponse);
            break;
        case SEND_SELECTED_GAME_NUMBER:
            message_size = sizeof(SelectedGameNumberResponse);
            break;
        case SEND_CARD_INFO:
            message_size = sizeof(CardInfoResponse);
            break;
        case SEND_HANDPAY_INFO:
            message_size = sizeof(HandpayInfoResponse);
            break;
        case SEND_SAS_VERSION:
            message_size = sizeof(SasVersionResponse);
            break;
        case SEND_CURRENT_CREDITS:
            message_size = sizeof(CurrentCreditsResponse);
            break;
		default:
			break;
    }

    return message_size;
}

UINT CSAS_Comm::getSPollMessageSize (void)
{
    UINT message_size = 0;

//    switch (current_tx_message.message[1])
    switch (receive_buffer[1])
    {
        case ENABLE_DISABLE_EVENT_REPORTING:
            message_size = 1;
            break;
        case TRANSFER_FUNDS:
            message_size = 1;
            break;
        case AFT_STATUS:
            message_size = sizeof(AftLockAndStatusResponse);
            break;
        case AFT_REGISTRATION:
            message_size = sizeof(AftRegistrationResponse);
            break;
        case ENABLE_JACKPOT_HANDPAY_RESET_METHOD:
            message_size = sizeof(EnableJackpotHandpayResetMethodResponse);
            break;
        case HANDPAY_RESET:
            message_size = sizeof(HandpayResetResponse);
            break;

        default:
            message_size = 1;
            break;
    }

    return message_size;
}

UINT CSAS_Comm::getMPollMessageSize (void)
{
    UINT message_size = 0;

    switch (receive_buffer[1])
    {
        case SEND_GAME_CONFIGURATION:
            message_size = sizeof(GameConfigurationResponse);
            break;
        case SEND_SELECTED_GAME_METERS:
            message_size = sizeof(SelectedGameMetersRequestResponse);
            break;
        case SEND_TOTAL_HAND_PAID_CANCELLED_CREDITS:
            message_size = sizeof(TotalHandPaidCancelledCreditsRequestResponse);
            break;
		default:
			break;
    }

    return message_size;
}

ULONG CSAS_Comm::translateDenomination (unsigned char denomination_code)
{
    ULONG denomination = 1;

    switch (denomination_code)
    {
        case 0x19:
            denomination = 15;
            break;
        case 0x18:
            denomination = 3;
            break;
        case 0x17:
            denomination = 2;
            break;
        case 0x16:
            denomination = 500000;
            break;
        case 0x15:
            denomination = 250000;
            break;
        case 0x14:
            denomination = 200000;
            break;
        case 0x13:
            denomination = 100000;
            break;
        case 0x12:
            denomination = 50000;
            break;
        case 0x11:
            denomination = 25000;
            break;
        case 0x10:
            denomination = 20000;
            break;
        case 0x0f:
            denomination = 5000;
            break;
        case 0x0e:
            denomination = 2500;
            break;
        case 0x0d:
            denomination = 250;
            break;
        case 0x0c:
            denomination = 200;
            break;
        case 0x0b:
            denomination = 20;
            break;
        case 0x0a:
            denomination = 10000;
            break;
        case 9:
            denomination = 2000;
            break;
        case 8:
            denomination = 1000;
            break;
        case 7:
            denomination = 500;
            break;
        case 6:
            denomination = 100;
            break;
        case 5:
            denomination = 50;
            break;
        case 4:
            denomination = 25;
            break;
        case 3:
            denomination = 10;
            break;
        case 2:
            denomination = 5;
            break;
        case 1:
            denomination = 1;
            break;
        default:
            break;
    }

    return denomination;
}
/*
// returns 0x0f if no 4ok hit
unsigned char CSAS_Comm::get4okWin (unsigned char* hand)
{
    unsigned char fok_rank = NO_4OK;
    int i;
    unsigned char rank_card1, rank_card2, rank_matches;
    unsigned char hand_ranks[5];

    for (i = 0; i < 5; i++)
        hand_ranks[i] = hand[i] & RANK_MASK;

    // set first hand rank to the first rank            
    rank_card1 = hand_ranks[0];    

    // set second hand rank to the next unique rank in the rank array
    for (i = 0; i < 5; i++)
        if (hand_ranks[i] != rank_card1)
        {
            rank_card2 = hand_ranks[i];
            break;
        }

    // count the number of ranks that match the first rank
    rank_matches = 0;

    for (i = 0; i < 5; i++)
        if (hand_ranks[i] == rank_card1)
            rank_matches++;

    // if we found a 4ok then skip checking the second rank matches
    if (rank_matches >= 4)
        fok_rank = rank_card1;
    else
    {
        // count the number of ranks that match the second rank
        rank_matches = 0;

        for (i = 0; i < 5; i++)
            if (hand_ranks[i] == rank_card2)
                rank_matches++;

        if (rank_matches >= 4)
            fok_rank = rank_card2;
    }

    return fok_rank;
}
*/
char CSAS_Comm::incrementTransactionNumber ()
{
    if ((transaction_number < '1') || (transaction_number >= '9'))
        transaction_number = '1';
    else
        transaction_number++;

    return transaction_number;
}

void CSAS_Comm::ReportCommError(LPTSTR lpszMessage)
{
/*
	TCHAR szBuffer[200];
	wsprintf(szBuffer, TEXT("Communications Error %d \n%s"), 
				GetLastError(),
				lpszMessage);
//	MessageBox(hWnd, szBuffer, szTitle, MB_OK | MB_ICONEXCLAMATION);
    AfxMessageBox(lpszMessage);
*/
}

void CSAS_Comm::LoadDLL (void)
{
    if (!m_hmodule)
    {
    	m_hmodule = AfxLoadLibrary((CString)"Ftd2xx.dll");	

	    if(m_hmodule == NULL)
	    {
		    logMessage((CString)"Error: Can't Load ft8u245.dll");
		    return;
	    }

	    m_pWrite = (PtrToWrite)GetProcAddress(m_hmodule, "FT_Write");
	    if (m_pWrite == NULL)
	    {
		    logMessage((CString)"Error: Can't Find FT_Write");
		    return;
	    }

	    m_pRead = (PtrToRead)GetProcAddress(m_hmodule, "FT_Read");
	    if (m_pRead == NULL)
	    {
		    logMessage((CString)"Error: Can't Find FT_Read");
		    return;
	    }

	    m_pOpen = (PtrToOpen)GetProcAddress(m_hmodule, "FT_Open");
	    if (m_pOpen == NULL)
	    {
		    logMessage((CString)"Error: Can't Find FT_Open");
		    return;
	    }

	    m_pOpenEx = (PtrToOpenEx)GetProcAddress(m_hmodule, "FT_OpenEx");
	    if (m_pOpenEx == NULL)
	    {
		    logMessage((CString)"Error: Can't Find FT_OpenEx");
		    return;
	    }

	    m_pCreateDeviceInfoList = (PtrToCreateDeviceInfoList)GetProcAddress(m_hmodule, "FT_CreateDeviceInfoList");
	    if(m_pCreateDeviceInfoList == NULL)
		    {
			    logMessage((CString)"Error: Can't Find FT_CreateDeviceInfoList");
			    return;
		    }

	    m_pListDevices = (PtrToListDevices)GetProcAddress(m_hmodule, "FT_ListDevices");
	    if(m_pListDevices == NULL)
		    {
			    logMessage((CString)"Error: Can't Find FT_ListDevices");
			    return;
		    }

	    m_pGetDeviceInfo = (PtrToGetDeviceInfo)GetProcAddress(m_hmodule, "FT_GetDeviceInfo");
	    if(m_pGetDeviceInfo == NULL)
		    {
			    logMessage((CString)"Error: Can't Find FT_GetDeviceInfo");
			    return;
		    }

	    m_pClose = (PtrToClose)GetProcAddress(m_hmodule, "FT_Close");
	    if (m_pClose == NULL)
	    {
		    logMessage((CString)"Error: Can't Find FT_Close");
		    return;
	    }

	    m_pResetDevice = (PtrToResetDevice)GetProcAddress(m_hmodule, "FT_ResetDevice");
	    if (m_pResetDevice == NULL)
	    {
		    logMessage((CString)"Error: Can't Find FT_ResetDevice");
		    return;
	    }

	    m_pPurge = (PtrToPurge)GetProcAddress(m_hmodule, "FT_Purge");
	    if (m_pPurge == NULL)
	    {
		    logMessage((CString)"Error: Can't Find FT_Purge");
		    return;
	    }

	    m_pStopInTask = (PtrToStopInTask)GetProcAddress(m_hmodule, "FT_StopInTask");
	    if (m_pStopInTask == NULL)
	    {
		    logMessage((CString)"Error: Can't Find FT_StopInTask");
		    return;
	    }

	    m_pRestartInTask = (PtrToRestartInTask)GetProcAddress(m_hmodule, "FT_RestartInTask");
	    if (m_pRestartInTask == NULL)
	    {
		    logMessage((CString)"Error: Can't Find FT_RestartInTask");
		    return;
	    }

	    m_pSetTimeouts = (PtrToSetTimeouts)GetProcAddress(m_hmodule, "FT_SetTimeouts");
	    if (m_pSetTimeouts == NULL)
	    {
		    logMessage((CString)"Error: Can't Find FT_SetTimeouts");
		    return;
	    }

	    m_pGetQueueStatus = (PtrToGetQueueStatus)GetProcAddress(m_hmodule, "FT_GetQueueStatus");
	    if (m_pGetQueueStatus == NULL)
	    {
		    logMessage((CString)"Error: Can't Find FT_GetQueueStatus");
		    return;
	    }

	    m_pSetDataCharacteristics = (PtrToSetDataCharacteristics)GetProcAddress(m_hmodule, "FT_SetDataCharacteristics");
	    if (m_pSetDataCharacteristics == NULL)
	    {
		    logMessage((CString)"Error: Can't Find FT_SetDataCharacteristics");
		    return;
	    }

	    m_pSetBaudRate = (PtrToSetBaudRate)GetProcAddress(m_hmodule, "FT_SetBaudRate");
	    if (m_pSetBaudRate == NULL)
	    {
		    logMessage((CString)"Error: Can't Find FT_SetBaudRate");
		    return;
	    }

	    m_pSetEventNotification = (PtrToSetEventNotification)GetProcAddress(m_hmodule, "FT_SetEventNotification");
	    if (m_pSetEventNotification == NULL)
	    {
		    logMessage((CString)"Error: Can't Find FT_SetEventNotification");
		    return;
	    }

	    m_pSetUSBParameters = (PtrToSetUSBParameters)GetProcAddress(m_hmodule, "FT_SetUSBParameters");
	    if (m_pSetUSBParameters == NULL)
	    {
		    logMessage((CString)"Error: Can't Find FT_SetUSBParameters");
		    return;
	    }
    }
}	

void CSAS_Comm::Loadem (void)
{
	LoadDLL();
}

bool CSAS_Comm::CheckConnections (void)
{
    FT_DEVICE ftDevice;
    FT_STATUS ftStatus;
    DWORD deviceID;
    char SerialNumber[16];
    char Description[64];
    bool port_fatal_error = FALSE;
    time_t last_port_check_time;
    time_t current_time;

    CreateDeviceInfoList(&number_of_usb_devices);

	ftStatus = GetDeviceInfo (m_ftHandle, &ftDevice, &deviceID, SerialNumber, Description, NULL);

	if (ftStatus != FT_OK)
	{
		port_fatal_error = TRUE;
		stop_comm_thread = TRUE;

		last_port_check_time = time(NULL);

		while (serial_recieve_task_active && (current_time < (last_port_check_time + 30)))
		{
			current_time = time(NULL);
			Sleep(1);
		}
	}

    return port_fatal_error;
}

//FT_STATUS CSAS_Comm::Open(PVOID pvDevice)
FT_STATUS CSAS_Comm::Open(int pvDevice)
{
    CreateDeviceInfoList(&number_of_usb_devices);

	if (!m_pOpen)
	{
		logMessage((CString)"FT_Open is not valid!"); 
		return FT_INVALID_HANDLE;
	}
	
	return (*m_pOpen)(pvDevice, &m_ftHandle );
}	

FT_STATUS CSAS_Comm::OpenEx(char* serial_number)
{
    CreateDeviceInfoList(&number_of_usb_devices);

	if (!m_pOpenEx)
	{
		logMessage((CString)"FT_Open is not valid!"); 
		return FT_INVALID_HANDLE;
	}
	
	return (*m_pOpenEx)((VOID*)serial_number, FT_OPEN_BY_SERIAL_NUMBER, &m_ftHandle );
}	

FT_STATUS CSAS_Comm::Read(LPVOID lpvBuffer, DWORD dwBuffSize, LPDWORD lpdwBytesRead)
{
	if (!m_ftHandle)
    {
		logMessage((CString)"Read m_ftHandle invalid!"); 
        return FT_INVALID_HANDLE;
    }
	if (!m_pRead)
	{
		logMessage((CString)"FT_Read is not valid!"); 
		return FT_INVALID_HANDLE;
	}

	return (*m_pRead)(m_ftHandle, lpvBuffer, dwBuffSize, lpdwBytesRead);
}	

FT_STATUS CSAS_Comm::Write(LPVOID lpvBuffer, DWORD dwBuffSize, LPDWORD lpdwBytes)
{
	if (!m_ftHandle)
    {
		logMessage((CString)"Write m_ftHandle invalid!"); 
        return FT_INVALID_HANDLE;
    }
	if (!m_pWrite)
	{
		logMessage((CString)"FT_Write is not valid!"); 
		return FT_INVALID_HANDLE;
	}
	
	return (*m_pWrite)(m_ftHandle, lpvBuffer, dwBuffSize, lpdwBytes);
}	

FT_STATUS CSAS_Comm::CreateDeviceInfoList(LPDWORD lpdwNumDevs)
{
	if (!m_ftHandle)
    {
		logMessage((CString)"CreateDeviceInfoList m_ftHandle invalid!"); 
        return FT_INVALID_HANDLE;
    }
	if (!m_pCreateDeviceInfoList)
	{
		logMessage((CString)"FT_CreateDeviceInfoList is not valid!"); 
		return FT_INVALID_HANDLE;
	}
	
	return (*m_pCreateDeviceInfoList)(lpdwNumDevs);
}	

FT_STATUS CSAS_Comm::ListDevices(PVOID pArg1, PVOID pArg2, DWORD dwFlags)
{
	if (!m_ftHandle)
    {
		logMessage((CString)"ListDevices m_ftHandle invalid!"); 
        return FT_INVALID_HANDLE;
    }
	if (!m_pListDevices)
	{
		logMessage((CString)"FT_ListDevices is not valid!"); 
		return FT_INVALID_HANDLE;
	}
	
	return (*m_pListDevices)(pArg1, pArg2, dwFlags);
}	

FT_STATUS CSAS_Comm::GetDeviceInfo (FT_HANDLE ftHandle, FT_DEVICE *pftType, LPDWORD lpdwID, PCHAR pcSerialNumber, PCHAR pcDescription, PVOID pvDummy)
{
	if (!m_ftHandle)
    {
		logMessage((CString)"GetDeviceInfo m_ftHandle invalid!"); 
        return FT_INVALID_HANDLE;
    }
	if (!m_pGetDeviceInfo)
	{
		logMessage((CString)"FT_GetDeviceInfo is not valid!"); 
		return FT_INVALID_HANDLE;
	}
	
	return (*m_pGetDeviceInfo)(ftHandle, pftType, lpdwID, pcSerialNumber, pcDescription, pvDummy);
}

FT_STATUS CSAS_Comm::Close()
{
	if (!m_pClose)
	{
		logMessage((CString)"FT_Close is not valid!"); 
		return FT_INVALID_HANDLE;
	}
	
	return (*m_pClose)(m_ftHandle);
}	

FT_STATUS CSAS_Comm::ResetDevice()
{
	if (!m_pResetDevice)
	{
		logMessage((CString)"FT_ResetDevice is not valid!"); 
		return FT_INVALID_HANDLE;
	}
	
	return (*m_pResetDevice)(m_ftHandle);
}	

FT_STATUS CSAS_Comm::StopInTask()
{
	if (!m_pStopInTask)
	{
		logMessage((CString)"FT_StopInTask is not valid!"); 
		return FT_INVALID_HANDLE;
	}
	
	return (*m_pStopInTask)(m_ftHandle);
}	

FT_STATUS CSAS_Comm::RestartInTask()
{
	if (!m_pRestartInTask)
	{
		logMessage((CString)"FT_RestartInTask is not valid!"); 
		return FT_INVALID_HANDLE;
	}
	
	return (*m_pRestartInTask)(m_ftHandle);
}	


FT_STATUS CSAS_Comm::Purge(ULONG dwMask)
{
	if (!m_pPurge)
	{
		logMessage((CString)"FT_Purge is not valid!"); 
		return FT_INVALID_HANDLE;
	}

	return (*m_pPurge)(m_ftHandle, dwMask);
}	

FT_STATUS CSAS_Comm::SetTimeouts(ULONG dwReadTimeout, ULONG dwWriteTimeout)
{
	if (!m_ftHandle)
    {
		logMessage((CString)"SetTimeouts m_ftHandle invalid!"); 
        return FT_INVALID_HANDLE;
    }
	if (!m_pSetTimeouts)
	{
		logMessage((CString)"FT_SetTimeouts is not valid!"); 
		return FT_INVALID_HANDLE;
	}

	return (*m_pSetTimeouts)(m_ftHandle, dwReadTimeout, dwWriteTimeout);
}	

FT_STATUS CSAS_Comm::GetQueueStatus(LPDWORD lpdwAmountInRxQueue)
{
	if (!m_ftHandle)
    {
		logMessage((CString)"GetQueueStatus m_ftHandle invalid!"); 
        return FT_INVALID_HANDLE;
    }
	if (!m_pGetQueueStatus)
	{
		logMessage((CString)"FT_GetQueueStatus is not valid!"); 
		return FT_INVALID_HANDLE;
	}

	return (*m_pGetQueueStatus)(m_ftHandle, lpdwAmountInRxQueue);
}	

FT_STATUS CSAS_Comm::SetDataCharacteristics(FT_HANDLE ftHandle, UCHAR WordLength, UCHAR StopBits, UCHAR Parity)
{
	if (!m_ftHandle)
    {
		logMessage((CString)"SetDataCharacteristics m_ftHandle invalid!"); 
        return FT_INVALID_HANDLE;
    }
	if (!m_pSetDataCharacteristics)
	{
		logMessage((CString)"FT_SetDataCharacteristics is not valid!"); 
		return FT_INVALID_HANDLE;
	}

	return (*m_pSetDataCharacteristics)(m_ftHandle, WordLength, StopBits, Parity);
}

FT_STATUS CSAS_Comm::SetBaudRate(FT_HANDLE ftHandle, DWORD BaudRate)
{
	if (!m_ftHandle)
    {
		logMessage((CString)"SetBaudRate m_ftHandle invalid!"); 
        return FT_INVALID_HANDLE;
    }
	if (!m_pSetBaudRate)
	{
		logMessage((CString)"FT_SetBaudRate is not valid!"); 
		return FT_INVALID_HANDLE;
	}

	return (*m_pSetBaudRate)(m_ftHandle, BaudRate);
}

FT_STATUS CSAS_Comm::SetEventNotification (FT_HANDLE ftHandle, DWORD dwEventMask, PVOID pvArg)
{
	if (!m_ftHandle)
    {
		logMessage((CString)"SetEventNotification m_ftHandle invalid!"); 
        return FT_INVALID_HANDLE;
    }
	if (!m_pSetEventNotification)
	{
		logMessage((CString)"FT_SetEventNotification is not valid!"); 
		return FT_INVALID_HANDLE;
	}

	return (*m_pSetEventNotification)(m_ftHandle, dwEventMask, pvArg);
}  

FT_STATUS CSAS_Comm::SetUSBParameters (FT_HANDLE ftHandle, ULONG ulInTransferSize, ULONG ulOutTransferSize)
{
	if (!m_ftHandle)
    {
		logMessage((CString)"SetUSBParameters m_ftHandle invalid!"); 
        return FT_INVALID_HANDLE;
    }
	if (!m_pSetUSBParameters)
	{
		logMessage((CString)"FT_SetUSBParameters is not valid!"); 
		return FT_INVALID_HANDLE;
	}

	return (*m_pSetUSBParameters)(m_ftHandle, ulInTransferSize, ulOutTransferSize);
}  

void CSAS_Comm::changeGbSystemState (GbSystemState system_state)
{

    if (system_state != non_volatile_ram.gb_system_state)
    {
        switch (system_state)
        {
            case ON_AVAIL:
//                updateFrenzyStatusInfo();
                break;
            case ON_UNAVAIL:
//                updateAccountStatusInfo();
                break;
            case OFF_AVAIL:
//                if (gui_state == GET_LOGON_INFO)
//		            PostMessage(WM_USER_UPDATE_LOGON_TILT_MESSAGE);
                break;
            case OFF_UNAVAIL:
                break;
        }
    }

    non_volatile_ram.gb_system_state = system_state;
}

GbSystemState CSAS_Comm::getGbSystemState (void)
{
    return non_volatile_ram.gb_system_state;
}

void CSAS_Comm::incrementPlayerGbPoints (ULONG current_wager)
{
	long points = 0;
	long tempwager;

	tempwager = current_wager * non_volatile_ram.current_game_denomination * 10; // wager base value 
	tempwager = tempwager * non_volatile_ram.inc_points / WHOLEPOINT; 		// bonus point multiplier
	tempwager += non_volatile_ram.fract_points;							// tack on current fractional points

	while (tempwager >= WHOLEPOINT)							// determine incremental whole points
	   {
	      tempwager -= WHOLEPOINT;
		  points++;
	   }

	non_volatile_ram.whole_points += points;
	non_volatile_ram.session_points += points;
	non_volatile_ram.fract_points = (USHORT)tempwager;	
    // Timbers club code only
	non_volatile_ram.monthly_whole_points += points;
	non_volatile_ram.monthly_fract_points = (USHORT)tempwager;

    if (non_volatile_ram.whole_points > last_session_update_points + 500)	
    {
        last_session_update_points = non_volatile_ram.whole_points;
    	pCGameCommControl->GbSessionUpdate();
    }
}

void CSAS_Comm::incrementBingoHandle (ULONG current_wager)
{
	if (non_volatile_ram.bingo.primary.card)
	{	
		/* impose the $5.00 handle limit */
		non_volatile_ram.bingo.primary.handle_to_date +=  
			 ((non_volatile_ram.current_game_denomination * current_wager) < MAX_HANDLE) ?
			 non_volatile_ram.current_game_denomination * current_wager : MAX_HANDLE;

		non_volatile_ram.bingo.primary.games_to_date++;	

		if (non_volatile_ram.bingo.secondary.card)
		{
    		non_volatile_ram.bingo.secondary.handle_to_date +=  
    			 ((non_volatile_ram.current_game_denomination * current_wager) < MAX_HANDLE) ?
    			 non_volatile_ram.current_game_denomination * current_wager : MAX_HANDLE;

  			non_volatile_ram.bingo.secondary.games_to_date++;
		}
	}
}

long CSAS_Comm::markBingoCard (unsigned char rank)
{
    long win_code = 0;

    switch (rank)
    {
        case 0:
            if (!(non_volatile_ram.bingo.primary.card & TWO_MASK))
            {
                non_volatile_ram.bingo.primary.card |= TWO_MASK;
                non_volatile_ram.bingo.primary.type = 2;
                non_volatile_ram.bingo.secondary.type = 0;
            }
            else if (!(non_volatile_ram.bingo.secondary.card & TWO_MASK))
            {
                non_volatile_ram.bingo.secondary.card |= TWO_MASK;
                non_volatile_ram.bingo.secondary.type = 2;
                non_volatile_ram.bingo.primary.type = 0;
            }
            win_code = FOUR_DEUCES_WIN_CODE;
            break;
        case 1:
            if (!(non_volatile_ram.bingo.primary.card & THREE_MASK))
            {
                non_volatile_ram.bingo.primary.card |= THREE_MASK;
                non_volatile_ram.bingo.primary.type = 3;
                non_volatile_ram.bingo.secondary.type = 0;
            }
            else if (!(non_volatile_ram.bingo.secondary.card & THREE_MASK))
            {
                non_volatile_ram.bingo.secondary.card |= THREE_MASK;
                non_volatile_ram.bingo.secondary.type = 3;
                non_volatile_ram.bingo.primary.type = 0;
            }
            win_code = FOUR_THREES_WIN_CODE;
            break;
        case 2:
            if (!(non_volatile_ram.bingo.primary.card & FOUR_MASK))
            {
                non_volatile_ram.bingo.primary.card |= FOUR_MASK;
                non_volatile_ram.bingo.primary.type = 4;
                non_volatile_ram.bingo.secondary.type = 0;
            }
            else if (!(non_volatile_ram.bingo.secondary.card & FOUR_MASK))
            {
                non_volatile_ram.bingo.secondary.card |= FOUR_MASK;
                non_volatile_ram.bingo.secondary.type = 4;
                non_volatile_ram.bingo.primary.type = 0;
            }
            win_code = FOUR_FOURS_WIN_CODE;
            break;
        case 3:
            if (!(non_volatile_ram.bingo.primary.card & FIVE_MASK))
            {
                non_volatile_ram.bingo.primary.card |= FIVE_MASK;
                non_volatile_ram.bingo.primary.type = 5;
                non_volatile_ram.bingo.secondary.type = 0;
            }
            else if (!(non_volatile_ram.bingo.secondary.card & FIVE_MASK))
            {
                non_volatile_ram.bingo.secondary.card |= FIVE_MASK;
                non_volatile_ram.bingo.secondary.type = 5;
                non_volatile_ram.bingo.primary.type = 0;
            }
            win_code = FOUR_FIVES_WIN_CODE;
            break;
        case 4:
            if (!(non_volatile_ram.bingo.primary.card & SIX_MASK))
            {
                non_volatile_ram.bingo.primary.card |= SIX_MASK;
                non_volatile_ram.bingo.primary.type = 6;
                non_volatile_ram.bingo.secondary.type = 0;
            }
            else if (!(non_volatile_ram.bingo.secondary.card & SIX_MASK))
            {
                non_volatile_ram.bingo.secondary.card |= SIX_MASK;
                non_volatile_ram.bingo.secondary.type = 6;
                non_volatile_ram.bingo.primary.type = 0;
            }
            win_code = FOUR_SIXES_WIN_CODE;
            break;
        case 5:
            if (!(non_volatile_ram.bingo.primary.card & SEVEN_MASK))
            {
                non_volatile_ram.bingo.primary.card |= SEVEN_MASK;
                non_volatile_ram.bingo.primary.type = 7;
                non_volatile_ram.bingo.secondary.type = 0;
            }
            else if (!(non_volatile_ram.bingo.secondary.card & SEVEN_MASK))
            {
                non_volatile_ram.bingo.secondary.card |= SEVEN_MASK;
                non_volatile_ram.bingo.secondary.type = 7;
                non_volatile_ram.bingo.primary.type = 0;
            }
            win_code = FOUR_SEVENS_WIN_CODE;
            break;
        case 6:
            if (!(non_volatile_ram.bingo.primary.card & EIGHT_MASK))
            {
                non_volatile_ram.bingo.primary.card |= EIGHT_MASK;
                non_volatile_ram.bingo.primary.type = 8;
                non_volatile_ram.bingo.secondary.type = 0;
            }
            else if (!(non_volatile_ram.bingo.secondary.card & EIGHT_MASK))
            {
                non_volatile_ram.bingo.secondary.card |= EIGHT_MASK;
                non_volatile_ram.bingo.secondary.type = 8;
                non_volatile_ram.bingo.primary.type = 0;
            }
            win_code = FOUR_EIGHTS_WIN_CODE;
            break;
        case 7:
            if (!(non_volatile_ram.bingo.primary.card & NINE_MASK))
            {
                non_volatile_ram.bingo.primary.card |= NINE_MASK;
                non_volatile_ram.bingo.primary.type = 9;
                non_volatile_ram.bingo.secondary.type = 0;
            }
            else if (!(non_volatile_ram.bingo.secondary.card & NINE_MASK))
            {
                non_volatile_ram.bingo.secondary.card |= NINE_MASK;
                non_volatile_ram.bingo.secondary.type = 9;
                non_volatile_ram.bingo.primary.type = 0;
            }
            win_code = FOUR_NINES_WIN_CODE;
            break;
        case 8:
            if (!(non_volatile_ram.bingo.primary.card & TEN_MASK))
            {
                non_volatile_ram.bingo.primary.card |= TEN_MASK;
                non_volatile_ram.bingo.primary.type = 10;
                non_volatile_ram.bingo.secondary.type = 0;
            }
            else if (!(non_volatile_ram.bingo.secondary.card & TEN_MASK))
            {
                non_volatile_ram.bingo.secondary.card |= TEN_MASK;
                non_volatile_ram.bingo.secondary.type = 10;
                non_volatile_ram.bingo.primary.type = 0;
            }
            win_code = FOUR_TENS_WIN_CODE;
            break;
        case 9:
            if (!(non_volatile_ram.bingo.primary.card & JACK_MASK))
            {
                non_volatile_ram.bingo.primary.card |= JACK_MASK;
                non_volatile_ram.bingo.primary.type = 11;
                non_volatile_ram.bingo.secondary.type = 0;
            }
            else if (!(non_volatile_ram.bingo.secondary.card & JACK_MASK))
            {
                non_volatile_ram.bingo.secondary.card |= JACK_MASK;
                non_volatile_ram.bingo.secondary.type = 11;
                non_volatile_ram.bingo.primary.type = 0;
            }
            win_code = FOUR_JACKS_WIN_CODE;
            break;
        case 0x0a:
            if (!(non_volatile_ram.bingo.primary.card & QUEEN_MASK))
            {
                non_volatile_ram.bingo.primary.card |= QUEEN_MASK;
                non_volatile_ram.bingo.primary.type = 12;
                non_volatile_ram.bingo.secondary.type = 0;
            }
            else if (!(non_volatile_ram.bingo.secondary.card & QUEEN_MASK))
            {
                non_volatile_ram.bingo.secondary.card |= QUEEN_MASK;
                non_volatile_ram.bingo.secondary.type = 12;
                non_volatile_ram.bingo.primary.type = 0;
            }
            win_code = FOUR_QUEENS_WIN_CODE;
            break;
        case 0x0b:
            if (!(non_volatile_ram.bingo.primary.card & KING_MASK))
            {
                non_volatile_ram.bingo.primary.card |= KING_MASK;
                non_volatile_ram.bingo.primary.type = 13;
                non_volatile_ram.bingo.secondary.type = 0;
            }
            else if (!(non_volatile_ram.bingo.secondary.card & KING_MASK))
            {
                non_volatile_ram.bingo.secondary.type = 13;
                non_volatile_ram.bingo.secondary.card |= KING_MASK;
                non_volatile_ram.bingo.primary.type = 0;
            }
            win_code = FOUR_KINGS_WIN_CODE;
            break;
        case 0x0c:
            if (!(non_volatile_ram.bingo.primary.card & ACE_MASK))
            {
                non_volatile_ram.bingo.primary.card |= ACE_MASK;
                non_volatile_ram.bingo.primary.type = 14;
                non_volatile_ram.bingo.secondary.type = 0;
            }
            else if (!(non_volatile_ram.bingo.secondary.card & ACE_MASK))
            {
                non_volatile_ram.bingo.secondary.card |= ACE_MASK;
                non_volatile_ram.bingo.secondary.type = 14;
                non_volatile_ram.bingo.primary.type = 0;
            }
            win_code = FOUR_ACES_WIN_CODE;
            break;
    }

//    if (non_volatile_ram.bingo.primary.type || non_volatile_ram.bingo.secondary.type)
//		log_event (EVT_WIN_HAND, win_code, 0);


    return win_code;
//    main_window->changeGuiState(SHOW_ACCOUNT_STATUS);
//    main_window->updateAccountStatusInfo();

}

void CSAS_Comm::check4okBingoWin (void)
{
    // see if the card is filled
	if (non_volatile_ram.bingo.primary.card == 0x1fff)
        non_volatile_ram.bingo_card_filled = TRUE;
    else
        non_volatile_ram.bingo_card_filled = FALSE;
}

void CSAS_Comm::saveNonVolatileRam (void)
{
    CString filename;

    if (pCGameCommControl && pCGameCommControl->pHeadGameDevice && pCGameCommControl->pHeadGameDevice->memory_game_config.ucmc_id)
    {
        // save non volatile ram data
        filename.Format("%s%u", GAME_DATA, pCGameCommControl->pHeadGameDevice->memory_game_config.ucmc_id);
        fileWrite(filename.GetBuffer(0), 0, &non_volatile_ram, sizeof(NonVolatileRam));
    }
}

unsigned long CSAS_Comm::calculateCardAward (unsigned long handle, unsigned short games)
{
	unsigned long award;

	if (games <= 100)
		award = getFirstBracketAward ();
	else if (games > 100 && games <= 200)
		award = getSecondBracketAward (handle, games);
	else if (games > 200 && games <= MAX_BINGO_GAMES)
		award = getThirdBracketAward (handle, games);
	else if (games > MAX_BINGO_GAMES)
		award = MAX_BRACKET * ((long) games / handle); /* */

	/* dont' exceed the maximum allowed */
	if ((DOLLAR_BINGO_AWARD) < award)
		return (DOLLAR_BINGO_AWARD);
	else
	   {
	      if (award < 1)
		     award = 1;
		  return (award);
	   }
}

unsigned long CSAS_Comm::getFirstBracketAward (void)
{
	long award;

	if (non_volatile_ram.current_game_denomination < QUARTER)
	   {
	      if (non_volatile_ram.max_bet > 5)
		     award = BINGO_AWARD_FACTOR1 * non_volatile_ram.current_game_denomination;
	      else
		     award = (BINGO_AWARD_FACTOR1 * non_volatile_ram.current_game_denomination) / 2;
	   }
	else if ((non_volatile_ram.current_game_denomination >= QUARTER) &&
	         (non_volatile_ram.current_game_denomination <= FIFTY_CENT))
	   {
	      if (non_volatile_ram.max_bet >= 10)
		     award = (BINGO_AWARD_FACTOR2 * non_volatile_ram.current_game_denomination) * 2;
		  else
			 award = BINGO_AWARD_FACTOR2 * non_volatile_ram.current_game_denomination;
	   }
	else if ((non_volatile_ram.current_game_denomination > FIFTY_CENT) &&
	         (non_volatile_ram.current_game_denomination <= ONEDOLLAR))
	   {
			 award = BINGO_AWARD_FACTOR2 * non_volatile_ram.current_game_denomination;
	   }
	else if (non_volatile_ram.current_game_denomination > ONEDOLLAR) 
        award = BINGO_AWARD_FACTOR2 * ONEDOLLAR;

	return (award);
}

unsigned long CSAS_Comm::getSecondBracketAward (unsigned long handle, unsigned short games)	
{
	unsigned long faward;
	unsigned long award;
	unsigned long tempdiv, tgames;

	faward =  getFirstBracketAward () * 100;
	 /* these temp variables were put in for the debug and left for clarity  */
	tempdiv = 120 * handle * 100 / games;
	tgames = games - 100;
	if (tempdiv > faward)
		award = faward / 100;
	else
		award = (faward - tgames * ((faward - tempdiv) / 100)) / 100;
	return (award);
}

unsigned long CSAS_Comm::getThirdBracketAward (unsigned long handle, unsigned short games)
{

	unsigned long int_slope, deltax, deltay, tempawd, tempratio, tempgam;
	unsigned long award = 0;
	const unsigned short BINGO_GAMES[] = {0, 2000, 5000, 8000, 13000, 20000, 30000, 60000};
	const unsigned short BINGO_AWARD[] = {12000, 12000, 800, 80, 8, 8, 8, 8};
	short i;
	
	for (i = 0; i < MAX_BRACKET; i++)
		{
		if ((BINGO_GAMES[i] < games) && (games <= BINGO_GAMES[i+1]))
			{
			deltax = (unsigned long) BINGO_GAMES[i+1] - (unsigned long) BINGO_GAMES[i];
			if (deltax == 0) /* divide by zero not allowed */
				int_slope = 0;
			else
				{
				/* the 10000 is to get some needed precision for some of the intervals */
				deltay = ((unsigned long) BINGO_AWARD[i] - (unsigned long) BINGO_AWARD[i+1]) * 10000;
				int_slope = deltay / deltax;
				}
			/* these temp variables were added for debug and left because it made the code easier to read */
			tempratio = handle / (long) games;
            if (tempratio > MAX_HANDLE)
			   tempratio = MAX_HANDLE;
			tempawd = (unsigned long) BINGO_AWARD[i];
			tempgam = (unsigned long) BINGO_GAMES[i];
			award = (tempawd - (((long) games - tempgam) * int_slope) / 10000) * tempratio / 100;
			break;
			}
		}

	return (award);
}

unsigned char CSAS_Comm::bcdToUnsignedChar (unsigned char bcd)
{
    unsigned char data = (bcd>>4)*10 + (bcd&0xf);

    return (data);
}

ULONG CSAS_Comm::bcd4ToUnsignedLong (unsigned char bcd0,
                                unsigned char bcd1,
                                unsigned char bcd2,
                                unsigned char bcd3)
{
    ULONG data0 = (long)(bcdToUnsignedChar(bcd3) * 1); 
    ULONG data1 = (long)(bcdToUnsignedChar(bcd2) * 100); 
    ULONG data2 = (long)(bcdToUnsignedChar(bcd1) * 10000); 
    ULONG data3 = (long)(bcdToUnsignedChar(bcd0) * 1000000); 

    return (data0 + data1 + data2 + data3);
}

ULONG CSAS_Comm::bcd5ToUnsignedLong (unsigned char bcd0,
                                unsigned char bcd1,
                                unsigned char bcd2,
                                unsigned char bcd3,
                                unsigned char bcd4)
{
    ULONG data0 = (long)(bcdToUnsignedChar(bcd4) * 1); 
    ULONG data1 = (long)(bcdToUnsignedChar(bcd3) * 100); 
    ULONG data2 = (long)(bcdToUnsignedChar(bcd2) * 10000); 
    ULONG data3 = (long)(bcdToUnsignedChar(bcd1) * 1000000); 
    ULONG data4 = (long)(bcdToUnsignedChar(bcd0) * 100000000); 

    return (data0 + data1 + data2 + data3 + data4);
}

ULONG CSAS_Comm::bcd2ToUnsignedLong (unsigned char bcd0,
                                unsigned char bcd1)
{
    ULONG data0 = (long)(bcdToUnsignedChar(bcd1) * 1); 
    ULONG data1 = (long)(bcdToUnsignedChar(bcd0) * 100); 

    return (data0 + data1);
}

void CSAS_Comm::unsignedLongToBcd4 (ULONG u_long, BCD* bcd)
{
    unsigned char dig[8];

    memset(dig, 0, 8);

    dig[0] = (unsigned char)(u_long % 10);
    dig[1] = (unsigned char)((u_long / 10) % 10);
    dig[2] = (unsigned char)((u_long / 100) % 10);
    dig[3] = (unsigned char)((u_long / 1000) % 10);
    dig[4] = (unsigned char)((u_long / 10000) % 10);
    dig[5] = (unsigned char)((u_long / 100000) % 10);
    dig[6] = (unsigned char)((u_long / 1000000) % 10);
    dig[7] = (unsigned char)((u_long / 10000000) % 10);

    bcd[3] = dig[1] * 16 + dig[0];
    bcd[2] = dig[3] * 16 + dig[2];
    bcd[1] = dig[5] * 16 + dig[4];
    bcd[0] = dig[7] * 16 + dig[6];
}

void CSAS_Comm::unsignedLongToBcd5 (ULONG u_long, BCD* bcd)
{
    unsigned char dig[10];

    memset(dig, 0, 10);

    dig[0] = (unsigned char)(u_long % 10);
    dig[1] = (unsigned char)((u_long / 10) % 10);
    dig[2] = (unsigned char)((u_long / 100) % 10);
    dig[3] = (unsigned char)((u_long / 1000) % 10);
    dig[4] = (unsigned char)((u_long / 10000) % 10);
    dig[5] = (unsigned char)((u_long / 100000) % 10);
    dig[6] = (unsigned char)((u_long / 1000000) % 10);
    dig[7] = (unsigned char)((u_long / 10000000) % 10);
    dig[8] = (unsigned char)((u_long / 100000000) % 10);
    dig[9] = (unsigned char)((u_long / 1000000000) % 10);

    bcd[4] = dig[1] * 16 + dig[0];
    bcd[3] = dig[3] * 16 + dig[2];
    bcd[2] = dig[5] * 16 + dig[4];
    bcd[1] = dig[7] * 16 + dig[6];
    bcd[0] = dig[9] * 16 + dig[8];
}

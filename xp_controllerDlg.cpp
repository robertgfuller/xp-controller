/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller	07/04/13	Save important Stratus TX messages to a file on disk and do not purge unless Stratus returns an ack.
Robert Fuller	07/03/13	Added code to launch the old GB Trax application if there is no IDC and the GameTenderClient.exe file is not present on the controller hardware.
Robert Fuller	05/02/12	Added support for the 4 cassette Puloon dispenser
Robert Fuller	09/17/12	Send XPC logon/logoff info messages to IDC.
Robert Fuller   06/04/12    Send Stratus message when starting or ending a cashier shift or logging in.
Robert Fuller   06/04/10    Added code to communicate with the Interactive Display Controller.
Robert Fuller   06/02/10    Added code to support the new Puloon LCDM-2000 dual denomination dispenser. 
Robert Fuller   03/11/10    Restart the application instead of rebooting the controller when a new version of xp controller is found.
Robert Fuller   01/19/10    Added code to support the new Puloon dispenser.
Robert Fuller   12/08/09    Once a day we purge the "dated paid ticket" file by removing all tickets in the file that over 14 days old.
Robert Fuller   06/09/09    Added code for custom picture buttons.
Robert Fuller   04/08/09    Change shift paradigm to only allow a single shift per login id to be active at a time.
Robert Fuller   09/25/08    Changed the way we keep track of the game comps for each cashiers shift.
Robert Fuller   09/22/08    Initialized the previous drop and handle shift variables by getting netwin data when a shift begins.
Robert Fuller   05/02/08    Added code to support CD2000 dispenser.
Dave Elquist    07/15/05    Changed user level calculation.
Dave Elquist    04/14/05    Initial Revision.
*/


// xp_controllerDlg.cpp : implementation file
//


#include "stdafx.h"
#include "xp_controller.h"
#include ".\xp_controllerdlg.h"
#include "TimerWindow.h"
#include "DrawerStartShift.h"
#include "utility.h"
//#include "NonVolatileRam.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// 60 seconds * 60 minutes * 24 hours * 14 days * 1000 msecs
#define DAILY_MSECS 60 * 60 * 24 * 1000

#define TIMEOUT_TO_GAME_TENDER_MSECS 10 * 1000

// CControllerDlg dialog

CControllerDlg::CControllerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CControllerDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    login_keypad_counter = 0;

	VERIFY(keypad0.LoadBitmaps(_T("KEYPAD0U"), _T("KEYPAD0D")));
	VERIFY(keypad1.LoadBitmaps(_T("KEYPAD1U"), _T("KEYPAD1D")));
	VERIFY(keypad2.LoadBitmaps(_T("KEYPAD2U"), _T("KEYPAD2D")));
	VERIFY(keypad3.LoadBitmaps(_T("KEYPAD3U"), _T("KEYPAD3D")));
	VERIFY(keypad4.LoadBitmaps(_T("KEYPAD4U"), _T("KEYPAD4D")));
	VERIFY(keypad5.LoadBitmaps(_T("KEYPAD5U"), _T("KEYPAD5D")));
	VERIFY(keypad6.LoadBitmaps(_T("KEYPAD6U"), _T("KEYPAD6D")));
	VERIFY(keypad7.LoadBitmaps(_T("KEYPAD7U"), _T("KEYPAD7D")));
	VERIFY(keypad8.LoadBitmaps(_T("KEYPAD8U"), _T("KEYPAD8D")));
	VERIFY(keypad9.LoadBitmaps(_T("KEYPAD9U"), _T("KEYPAD9D")));
	VERIFY(keypadC.LoadBitmaps(_T("KEYPADCU"), _T("KEYPADCD")));
}

CControllerDlg::~CControllerDlg()
{
}

void CControllerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CControllerDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_KEYPAD0_BUTTON, OnBnClickedZeroLogonButton)
	ON_BN_CLICKED(IDC_KEYPAD1_BUTTON, OnBnClickedOneLogonButton)
	ON_BN_CLICKED(IDC_KEYPAD2_BUTTON, OnBnClickedTwoLogonButton)
	ON_BN_CLICKED(IDC_KEYPAD3_BUTTON, OnBnClickedThreeLogonButton)
	ON_BN_CLICKED(IDC_KEYPAD4_BUTTON, OnBnClickedFourLogonButton)
	ON_BN_CLICKED(IDC_KEYPAD5_BUTTON, OnBnClickedFiveLogonButton)
	ON_BN_CLICKED(IDC_KEYPAD6_BUTTON, OnBnClickedSixLogonButton)
	ON_BN_CLICKED(IDC_KEYPAD7_BUTTON, OnBnClickedSevenLogonButton)
	ON_BN_CLICKED(IDC_KEYPAD8_BUTTON, OnBnClickedEightLogonButton)
	ON_BN_CLICKED(IDC_KEYPAD9_BUTTON, OnBnClickedNineLogonButton)
	ON_BN_CLICKED(IDC_KEYPADC_BUTTON, OnBnClickedClearLogonButton)
	ON_WM_MOUSEMOVE()
//ON_WM_ENTERIDLE()
END_MESSAGE_MAP()


// CControllerDlg message handlers

BOOL CControllerDlg::OnInitDialog()
{
 CString window_title;
 EventMessage event_message;
 CashierLogonLogoffStratusData cashier_logon_logoff_stratus_data;
 StratusMessageFormat stratus_msg;

	CDialog::OnInitDialog();

    VERIFY(keypad0.SubclassDlgItem(IDC_KEYPAD0_BUTTON, this));
    keypad0.SizeToContent();
    VERIFY(keypad1.SubclassDlgItem(IDC_KEYPAD1_BUTTON, this));
    keypad1.SizeToContent();
    VERIFY(keypad2.SubclassDlgItem(IDC_KEYPAD2_BUTTON, this));
    keypad2.SizeToContent();
    VERIFY(keypad3.SubclassDlgItem(IDC_KEYPAD3_BUTTON, this));
    keypad3.SizeToContent();
    VERIFY(keypad4.SubclassDlgItem(IDC_KEYPAD4_BUTTON, this));
    keypad4.SizeToContent();
    VERIFY(keypad5.SubclassDlgItem(IDC_KEYPAD5_BUTTON, this));
    keypad5.SizeToContent();
    VERIFY(keypad6.SubclassDlgItem(IDC_KEYPAD6_BUTTON, this));
    keypad6.SizeToContent();
    VERIFY(keypad7.SubclassDlgItem(IDC_KEYPAD7_BUTTON, this));
    keypad7.SizeToContent();
    VERIFY(keypad8.SubclassDlgItem(IDC_KEYPAD8_BUTTON, this));
    keypad8.SizeToContent();
    VERIFY(keypad9.SubclassDlgItem(IDC_KEYPAD9_BUTTON, this));
    keypad9.SizeToContent();
    VERIFY(keypadC.SubclassDlgItem(IDC_KEYPADC_BUTTON, this));
    keypadC.SizeToContent();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

    if (theApp.memory_settings_ini.masters_ip_address)
        window_title.Format("SLAVE CONTROLLER %u v%u.%s   ",
            theApp.memory_dispenser_config.ucmc_id, VERSION, SUBVERSION);
    else
        if (GetFileAttributes(MASTER_CONFIG) != 0xFFFFFFFF)
            window_title.Format("MASTER CONTROLLER v%u.%s   ", VERSION, SUBVERSION);
        else
            window_title.Format("CONTROLLER v%u.%s   ", VERSION, SUBVERSION);

    switch (theApp.memory_dispenser_config.dispenser_type)
    {
        case DISPENSER_TYPE_NOT_SET:
            window_title += "DISPENSER NOT SET";
            break;

        case DISPENSER_TYPE_NO_DISPENSER:
            window_title += "NO DISPENSER";
            break;

        case DISPENSER_TYPE_CASH_DRAWER:
            window_title += "CASH DRAWER";
            break;

        case DISPENSER_TYPE_JCM_20:
            window_title += "$20 JCM DISPENSER";
            break;

        case DISPENSER_TYPE_JCM_100:
            window_title += "$100 JCM DISPENSER";
            break;

        case DISPENSER_TYPE_DIEBOLD:
            window_title += "DIEBOLD DISPENSER";
            break;

        case DISPENSER_TYPE_ECASH:
            window_title += "ECASH DISPENSER";
            break;

        case DISPENSER_TYPE_CD2000:
            window_title += "CD2000 DISPENSER";
            break;

        case DISPENSER_TYPE_LCDM1000_20:
            window_title += "UCMC1000DS DISPENSER";
            break;

        case DISPENSER_TYPE_LCDM2000_20_100:
            window_title += "UCMC2000DS DISPENSER";
            break;

        case DISPENSER_TYPE_LCDM4000_20_100:
            window_title += "UCMC4000DS DISPENSER";
            break;

        default:
            window_title += "ERROR DISPENSER TYPE";
            break;
    }

    #ifdef _DEBUG
        window_title += "(DEBUG)";
    #endif

    SetDlgItemText(IDC_LOGON_DIALOG_TITLE_STATIC, window_title);

    if (theApp.current_user.user.id)
    {
        memset(&event_message, 0, sizeof(event_message));
        event_message.head.time_stamp = time(NULL);
        event_message.head.event_type = USER_INTERFACE_LOGOFF;
        event_message.head.data_length = 4;
        memcpy(event_message.data, &theApp.current_user.user.id, 4);
        theApp.event_message_queue.push(event_message);

        theApp.printer_busy = true;
        theApp.PrinterHeader(0);
        theApp.PrinterData(" LOG-OFF\n");
        theApp.PrinterId();
        theApp.PrinterData(PRINTER_FORM_FEED);
        theApp.printer_busy = false;

        if (theApp.current_user.user.id / 10000 == CASHIER)
        {
		    if ((theApp.memory_settings_ini.stratus_tcp_port == STRATUS_ENCRYPTED_PRODUCTION_PORT ||
			    theApp.memory_settings_ini.stratus_tcp_port == STRATUS_ENCRYPTED_TEST_PORT ||
			    theApp.memory_settings_ini.stratus_tcp_port == STRATUS_ENCRYPTED_DEVELOPMENT_PORT))
            {
			    //change endian for Stratus
                cashier_logon_logoff_stratus_data.cashier_id = theApp.current_user.user.id;
                cashier_logon_logoff_stratus_data.log_on = 0;

			    endian4(cashier_logon_logoff_stratus_data.cashier_id);
			    endian4(cashier_logon_logoff_stratus_data.log_on);

                memset(&stratus_msg, 0, sizeof(stratus_msg));
                stratus_msg.send_code = TKT_CASHIER_LOG_ON_OFF;
                stratus_msg.task_code = TKT_TASK_TICKET;
                stratus_msg.message_length = sizeof(cashier_logon_logoff_stratus_data) + STRATUS_HEADER_SIZE;
                stratus_msg.property_id = theApp.memory_location_config.property_id;
                stratus_msg.club_code = theApp.memory_location_config.club_code;
                stratus_msg.serial_port = 1;
                stratus_msg.customer_id = theApp.memory_location_config.customer_id;
                stratus_msg.data_to_event_log = true;
                memcpy(stratus_msg.data, &cashier_logon_logoff_stratus_data, sizeof(cashier_logon_logoff_stratus_data));
                theApp.QueueStratusTxMessage(&stratus_msg, FALSE);
            }
        }
        theApp.SendIdcUserLoginLogoffMessage (theApp.current_user.user.id, 0);

        memset(&theApp.current_user, 0, sizeof(theApp.current_user));
    }

    SetTimer(DAILY_TIMER, DAILY_MSECS, 0);

    if (theApp.memory_settings_ini.idc_ip_address && theApp.memory_settings_ini.idc_tcp_port && !(GetFileAttributes(GAME_TENDER_FILE) == 0xFFFFFFFF))
        SetTimer(GAME_TENDER_TIMER, TIMEOUT_TO_GAME_TENDER_MSECS, 0);
	

 return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CControllerDlg::OnPaint() 
{
	CRect rect;
	GetClientRect(&rect);
	CPaintDC dc(this); // device context for painting

	if (IsIconic())
	{

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);

	}
	else
	{
		CDialog::OnPaint();
	}
    dc.FillSolidRect(&rect, 0x00000000);
}

void CControllerDlg::OnTimer(UINT nIDEvent)
{
    switch (nIDEvent)
    {
        case DAILY_TIMER:
            theApp.purgeDatedPaidTicketsFile();
            break;
		case GAME_TENDER_TIMER:
			KillTimer(GAME_TENDER_TIMER);
        	if (theApp.memory_settings_ini.idc_ip_address && theApp.memory_settings_ini.idc_tcp_port && !(GetFileAttributes(GAME_TENDER_FILE) == 0xFFFFFFFF))
                if (!theApp.IdcSocket.socket_state || (theApp.IdcSocket.socket_state == WSAEISCONN))
        			ShellExecute(NULL, "open", GAME_TENDER_FILE, NULL, NULL, SW_SHOWMAXIMIZED);
			break;
    }

	CDialog::OnTimer(nIDEvent);
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CControllerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CControllerDlg::ProcessKeypadInput(char *number)
{
 bool invalid_userid = true;
 ControllerUser temp_user;
 CString user_id_box_string, password_box_string, message_window_string;
 EventMessage event_message;
 CashierLogonLogoffStratusData cashier_logon_logoff_stratus_data;
 StratusMessageFormat stratus_msg;

	KillTimer(GAME_TENDER_TIMER);

	if (login_keypad_counter <= 4)
    {
        GetDlgItemText(IDC_USER_ID_LOGON_EDIT, user_id_box_string);
        user_id_box_string += number;
        SetDlgItemText(IDC_USER_ID_LOGON_EDIT, user_id_box_string);
    }
    else
        if (login_keypad_counter <= 8)
        {
            GetDlgItemText(IDC_USER_PASSWORD_LOGON_EDIT, password_box_string);
            password_box_string += number;
            SetDlgItemText(IDC_USER_PASSWORD_LOGON_EDIT, password_box_string);
        }

    login_keypad_counter++;
    if (login_keypad_counter >= 9)
    {
        memset(&temp_user, 0, sizeof(temp_user));

        if (GetDlgItemText(IDC_USER_ID_LOGON_EDIT, user_id_box_string) &&
            GetDlgItemText(IDC_USER_PASSWORD_LOGON_EDIT, password_box_string))
        {
            temp_user.user.id = atoi(user_id_box_string);
            temp_user.user.password = atoi(password_box_string);
        }

		#ifdef _DEBUG
			if (!temp_user.user.id && !temp_user.user.password)
	        {
	            EnableWindow(FALSE);
	            MessageWindow(true, "EXIT?", "PROGRAM EXIT USER ID\nAND PASSWORD ENTERED!");

	            while (theApp.message_window_queue.empty())
	                theApp.OnIdle(0);

	            EnableWindow();

	            if (theApp.message_window_queue.front() == IDOK)
	            {
	                theApp.current_dialog_return = 0xFFFFFFFF;
	                DestroyWindow();
	            }
	            else
	                OnBnClickedClearLogonButton();

	            return;
	        }
		#endif	// #ifdef _DEBUG

        if (temp_user.user.id / 10000 > MANAGER || !theApp.memory_settings_ini.masters_ip_address)
        {
            if (theApp.CheckLoginPassword(&temp_user))
                invalid_userid = false;
        }
        else
        {
            EnableWindow(false);

            if (theApp.CheckSlaveLoginPassword(&temp_user))
                invalid_userid = false;

            EnableWindow();
        }

        if (!invalid_userid)
        {
            if (GetFileAttributes(NEW_CONTROLLER) != 0xFFFFFFFF)
            {
                EnableWindow(FALSE);

                MessageWindow(true, "RESTART?", "SYSTEM UPGRADE PENDING\nRESTART APPLICATION NOW?");

                while (theApp.message_window_queue.empty())
                    theApp.OnIdle(0);

                EnableWindow();

                if (theApp.message_window_queue.front() == IDOK)
                {
                    theApp.ProcessExitProgramRequest(false);
                    DestroyWindow();
                    return;

                }
            }

            if (temp_user.user.id / 10000 < ENGINEERING)
            {
                temp_user.autologout_timeout = theApp.memory_settings_ini.auto_logout_timeouts[temp_user.user.id / 10000];
                if (temp_user.autologout_timeout)
                    temp_user.time_setting = time(NULL);
            }

            memcpy(&theApp.current_user, &temp_user, sizeof(theApp.current_user));

            if (theApp.current_user.user.id / 10000 == CASHIER && theApp.memory_settings_ini.force_cashier_shifts)
            {
                if (!theApp.current_shift_user.user.id)
                {
                    // if we don't have an active shift, let this cashier user start the new shift
                    memcpy(&theApp.current_shift_user, &theApp.current_user, sizeof(theApp.current_shift_user));
                    theApp.CashierShiftStart(0);
                }
                else if (theApp.current_user.user.id != theApp.current_shift_user.user.id)
                {
                    theApp.printer_busy = true;
                    theApp.PrinterHeader(0);
                    theApp.PrinterData(" INVALID LOGON - END PREVIOUS SHIFT BEFORE LOGGING IN\n\n");
                    theApp.printer_busy = false;

                    message_window_string.Format("END %u SHIFT BEFORE LOGGING IN", theApp.current_shift_user.user.id);

                    MessageWindow(false, "INVALID SHIFT LOGIN", message_window_string);
                    OnBnClickedClearLogonButton();
                    return;
                }
            }

            theApp.current_dialog_return = IDD_MAIN_MENU_DIALOG;

            memset(&event_message, 0, sizeof(event_message));
            event_message.head.time_stamp = time(NULL);
            event_message.head.event_type = USER_INTERFACE_LOGON;
            event_message.head.data_length = 4;
            memcpy(event_message.data, &theApp.current_user.user.id, 4);
            theApp.event_message_queue.push(event_message);

            theApp.printer_busy = true;
            theApp.PrinterHeader(0);
            theApp.PrinterData(" VALID LOGON\n");
            theApp.PrinterId();
            theApp.PrinterData("\n");
            theApp.printer_busy = false;

            if (theApp.current_user.user.id / 10000 == CASHIER)
            {
		        if ((theApp.memory_settings_ini.stratus_tcp_port == STRATUS_ENCRYPTED_PRODUCTION_PORT ||
			        theApp.memory_settings_ini.stratus_tcp_port == STRATUS_ENCRYPTED_TEST_PORT ||
			        theApp.memory_settings_ini.stratus_tcp_port == STRATUS_ENCRYPTED_DEVELOPMENT_PORT))
                {
			        //change endian for Stratus
                    cashier_logon_logoff_stratus_data.cashier_id = theApp.current_user.user.id;
                    cashier_logon_logoff_stratus_data.log_on = 1;

			        endian4(cashier_logon_logoff_stratus_data.cashier_id);
			        endian4(cashier_logon_logoff_stratus_data.log_on);

                    memset(&stratus_msg, 0, sizeof(stratus_msg));
                    stratus_msg.send_code = TKT_CASHIER_LOG_ON_OFF;
                    stratus_msg.task_code = TKT_TASK_TICKET;
                    stratus_msg.message_length = sizeof(cashier_logon_logoff_stratus_data) + STRATUS_HEADER_SIZE;
                    stratus_msg.property_id = theApp.memory_location_config.property_id;
                    stratus_msg.club_code = theApp.memory_location_config.club_code;
                    stratus_msg.serial_port = 1;
                    stratus_msg.customer_id = theApp.memory_location_config.customer_id;
                    stratus_msg.data_to_event_log = true;
                    memcpy(stratus_msg.data, &cashier_logon_logoff_stratus_data, sizeof(cashier_logon_logoff_stratus_data));
                    theApp.QueueStratusTxMessage(&stratus_msg, FALSE);
                }
            }
            theApp.SendIdcUserLoginLogoffMessage (theApp.current_user.user.id, 1);

//            DestroyWindow();
        }
        else
        {
            theApp.printer_busy = true;
            theApp.PrinterHeader(0);
            theApp.PrinterData(" INVALID LOGON\n\n");
            theApp.printer_busy = false;

            if (theApp.memory_settings_ini.masters_ip_address)
                MessageWindow(false, "INVALID", "INVALID USER ID AND/OR PASSWORD\nOR USER ID IS NOT THE CURRENT SHIFT ID");
            else
                MessageWindow(false, "INVALID", "INVALID USER ID AND/OR PASSWORD");

            OnBnClickedClearLogonButton();
        }
    }
}

void CControllerDlg::OnBnClickedOneLogonButton()
{
    ProcessKeypadInput("1");
}

void CControllerDlg::OnBnClickedTwoLogonButton()
{
    ProcessKeypadInput("2");
}

void CControllerDlg::OnBnClickedThreeLogonButton()
{
    ProcessKeypadInput("3");
}

void CControllerDlg::OnBnClickedFourLogonButton()
{
    ProcessKeypadInput("4");
}

void CControllerDlg::OnBnClickedFiveLogonButton()
{
    ProcessKeypadInput("5");
}

void CControllerDlg::OnBnClickedSixLogonButton()
{
    ProcessKeypadInput("6");
}

void CControllerDlg::OnBnClickedSevenLogonButton()
{
    ProcessKeypadInput("7");
}

void CControllerDlg::OnBnClickedEightLogonButton()
{
    ProcessKeypadInput("8");
}

void CControllerDlg::OnBnClickedNineLogonButton()
{
    ProcessKeypadInput("9");
}

void CControllerDlg::OnBnClickedZeroLogonButton()
{
    ProcessKeypadInput("0");
}

void CControllerDlg::OnBnClickedClearLogonButton()
{
    login_keypad_counter = 0;
    SetDlgItemText(IDC_USER_ID_LOGON_EDIT, "");
    SetDlgItemText(IDC_USER_PASSWORD_LOGON_EDIT, "");
}

void CControllerDlg::OnMouseMove(UINT nFlags, CPoint point)
{
    theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

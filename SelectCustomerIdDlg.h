/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    07/15/05    Moved function.
Dave Elquist    05/26/05    Initial Revision.
*/


#pragma once
#include "afxwin.h"


// CSelectCustomerIdDlg dialog

class CSelectCustomerIdDlg : public CDialog
{
	DECLARE_DYNAMIC(CSelectCustomerIdDlg)

public:
	CSelectCustomerIdDlg(BYTE display_type, DWORD *pCustomerIdList, CWnd *pParent = NULL);   // standard constructor
	virtual ~CSelectCustomerIdDlg();
	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_SELECT_CUSTOMER_ID_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedSelectCustomerIdButton1();
	afx_msg void OnBnClickedSelectCustomerIdButton2();
	afx_msg void OnBnClickedSelectCustomerIdButton3();
	afx_msg void OnBnClickedSelectCustomerIdButton4();
	afx_msg void OnBnClickedSelectCustomerIdButton5();
	afx_msg void OnBnClickedSelectCustomerIdButton6();
	afx_msg void OnBnClickedSelectCustomerIdButton7();
	afx_msg void OnBnClickedSelectCustomerIdButton8();
	afx_msg void OnBnClickedSelectCustomerIdButton9();
	afx_msg void OnBnClickedSelectCustomerIdButton10();
	afx_msg void OnBnClickedSelectCustomerIdButton11();
	afx_msg void OnBnClickedSelectCustomerIdButton12();
	afx_msg void OnBnClickedSelectCustomerIdButton13();
	afx_msg void OnBnClickedSelectCustomerIdButton14();
	afx_msg void OnBnClickedSelectCustomerIdButton15();
	afx_msg void OnBnClickedSelectCustomerIdButton16();
	afx_msg void OnBnClickedSelectCustomerIdButton17();
	afx_msg void OnBnClickedSelectCustomerIdButton18();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);

private:
	CButton m_button1;
	CButton m_button2;
	CButton m_button3;
	CButton m_button4;
	CButton m_button5;
	CButton m_button6;
	CButton m_button7;
	CButton m_button8;
	CButton m_button9;
	CButton m_button10;
	CButton m_button11;
	CButton m_button12;
	CButton m_button13;
	CButton m_button14;
	CButton m_button15;
	CButton m_button16;
	CButton m_button17;
	CButton m_button18;

	CString windowTitle;

    DWORD customer_id_list[MAXIMUM_CUSTOMER_IDS];
};

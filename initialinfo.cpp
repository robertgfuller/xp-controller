/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   11/04/08    Corrected the default ip address of the GB Advantage Configurator application when bringing up the
                            controller for the first time.
Robert Fuller   08/19/08    Added code to support the GB Advantage feature.
Dave Elquist    07/15/05    Added support to changed settings from engineer's menu.
Dave Elquist    04/14/05    Initial Revision.
*/


// InitialInfo.cpp : implementation file
//


#include "stdafx.h"
#include "xp_controller.h"
#include ".\initialinfo.h"


// CInitialInfoDlg dialog

IMPLEMENT_DYNAMIC(CInitialInfoDlg, CDialog)
CInitialInfoDlg::CInitialInfoDlg(CWnd* pParent /*=NULL*/) : CDialog(CInitialInfoDlg::IDD, pParent)
{
    current_edit_focus = 0;
}

CInitialInfoDlg::~CInitialInfoDlg()
{
}

void CInitialInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CInitialInfoDlg, CDialog)
	ON_BN_CLICKED(IDC_ZERO_KEYPAD_BUTTON, OnBnClickedZeroKeypadButton)
	ON_BN_CLICKED(IDC_ONE_KEYPAD_BUTTON, OnBnClickedOneKeypadButton)
	ON_BN_CLICKED(IDC_TWO_KEYPAD_BUTTON, OnBnClickedTwoKeypadButton)
	ON_BN_CLICKED(IDC_THREE_KEYPAD_BUTTON, OnBnClickedThreeKeypadButton)
	ON_BN_CLICKED(IDC_FOUR_KEYPAD_BUTTON, OnBnClickedFourKeypadButton)
	ON_BN_CLICKED(IDC_FIVE_KEYPAD_BUTTON, OnBnClickedFiveKeypadButton)
	ON_BN_CLICKED(IDC_SIX_KEYPAD_BUTTON, OnBnClickedSixKeypadButton)
	ON_BN_CLICKED(IDC_SEVEN_KEYPAD_BUTTON, OnBnClickedSevenKeypadButton)
	ON_BN_CLICKED(IDC_EIGHT_KEYPAD_BUTTON, OnBnClickedEightKeypadButton)
	ON_BN_CLICKED(IDC_NINE_KEYPAD_BUTTON, OnBnClickedNineKeypadButton)
	ON_BN_CLICKED(IDC_COLON_KEYPAD_BUTTON, OnBnClickedColonKeypadButton)
	ON_BN_CLICKED(IDC_PERIOD_KEYPAD_BUTTON, OnBnClickedPeriodKeypadButton)
	ON_BN_CLICKED(IDC_OK_KEYPAD_BUTTON, OnBnClickedOkKeypadButton)
	ON_BN_CLICKED(IDC_CANCEL_KEYPAD_BUTTON, OnBnClickedCancelKeypadButton)
	ON_BN_CLICKED(IDC_CLEAR_KEYPAD_BUTTON, OnBnClickedClearKeypadButton)
	ON_EN_SETFOCUS(IDC_CUSTOMER_CONTROL_SERVER_IP_PORT_EDIT, OnEnSetfocusCustomerControlServerIpPortEdit)
	ON_EN_SETFOCUS(IDC_CUSTOMER_CONTROL_CLIENT_PORT_EDIT, OnEnSetfocusCustomerControlClientPortEdit)
	ON_BN_CLICKED(IDC_INITIAL_SOUTH_BUTTON, OnBnClickedInitialSouthButton)
	ON_BN_CLICKED(IDC_INITIAL_NORTH_BUTTON, OnBnClickedInitialNorthButton)
	ON_BN_CLICKED(IDC_INITIAL_EAST_BUTTON, OnBnClickedInitialEastButton)
	ON_EN_SETFOCUS(IDC_GB_PRO_CONFIGURATOR_IP_PORT_EDIT, OnEnSetfocusGbProConfiguratorIpPortEdit)

END_MESSAGE_MAP()


// CInitialInfoDlg message handlers

void CInitialInfoDlg::ProcessButtonClick(BYTE button_press)
{
 char string_add[2];
 CString edit_string;

    if (current_edit_focus == IDC_CUSTOMER_CONTROL_CLIENT_PORT_EDIT ||
        current_edit_focus == IDC_CUSTOMER_CONTROL_SERVER_IP_PORT_EDIT ||
        current_edit_focus == IDC_GB_PRO_CONFIGURATOR_IP_PORT_EDIT)
    {
        string_add[0] = 0x30 + button_press;
        string_add[1] = 0;

        GetDlgItemText(current_edit_focus, edit_string);
        edit_string += string_add;
        SetDlgItemText(current_edit_focus, edit_string);
    }
}

void CInitialInfoDlg::OnBnClickedZeroKeypadButton()
{
    ProcessButtonClick(0);
}

void CInitialInfoDlg::OnBnClickedOneKeypadButton()
{
    ProcessButtonClick(1);
}

void CInitialInfoDlg::OnBnClickedTwoKeypadButton()
{
    ProcessButtonClick(2);
}

void CInitialInfoDlg::OnBnClickedThreeKeypadButton()
{
    ProcessButtonClick(3);
}

void CInitialInfoDlg::OnBnClickedFourKeypadButton()
{
    ProcessButtonClick(4);
}

void CInitialInfoDlg::OnBnClickedFiveKeypadButton()
{
    ProcessButtonClick(5);
}

void CInitialInfoDlg::OnBnClickedSixKeypadButton()
{
    ProcessButtonClick(6);
}

void CInitialInfoDlg::OnBnClickedSevenKeypadButton()
{
    ProcessButtonClick(7);
}

void CInitialInfoDlg::OnBnClickedEightKeypadButton()
{
    ProcessButtonClick(8);
}

void CInitialInfoDlg::OnBnClickedNineKeypadButton()
{
    ProcessButtonClick(9);
}

void CInitialInfoDlg::OnBnClickedColonKeypadButton()
{
 CString edit_string;

    if (current_edit_focus == IDC_CUSTOMER_CONTROL_SERVER_IP_PORT_EDIT ||
        current_edit_focus == IDC_GB_PRO_CONFIGURATOR_IP_PORT_EDIT)
    {
        GetDlgItemText(current_edit_focus, edit_string);
        edit_string += ":";
        SetDlgItemText(current_edit_focus, edit_string);
    }
}

void CInitialInfoDlg::OnBnClickedPeriodKeypadButton()
{
 CString ip_string;

    if (current_edit_focus == IDC_CUSTOMER_CONTROL_SERVER_IP_PORT_EDIT ||
        current_edit_focus == IDC_GB_PRO_CONFIGURATOR_IP_PORT_EDIT)
    {
        GetDlgItemText(current_edit_focus, ip_string);
        ip_string += ".";
        SetDlgItemText(current_edit_focus, ip_string);
    }
}

void CInitialInfoDlg::OnBnClickedOkKeypadButton()
{
 CString edit_string;
 FileSettingsIni file_settings;
 FileGbProManagerSettings gb_file_settings;

 BYTE *ip_address, *configurator_ip_address;

    memset(&file_settings, 0, sizeof(file_settings));
    memset(&gb_file_settings, 0, sizeof(gb_file_settings));

    GetDlgItemText(IDC_CUSTOMER_CONTROL_SERVER_IP_PORT_EDIT, edit_string);
    theApp.GetIpAddressAndPort(edit_string, &file_settings.memory.customer_control_server_ip_address,
        &file_settings.memory.customer_control_server_tcp_port);

    if (!file_settings.memory.customer_control_server_tcp_port)
    {
        MessageWindow(false, "ERROR!", "INVALID CUSTOMER SERVER TCP/IP PORT");
        return;
    }

    GetDlgItemText(IDC_GB_PRO_CONFIGURATOR_IP_PORT_EDIT, edit_string);
    theApp.GetIpAddressAndPort(edit_string, &gb_file_settings.memory.gb_pro_configurator_ip_address,
        &gb_file_settings.memory.gb_pro_configurator_tcp_port);

    if (!gb_file_settings.memory.gb_pro_configurator_tcp_port)
    {
        MessageWindow(false, "ERROR!", "INVALID GB PRO CONFIGURATOR TCP/IP PORT");
        return;
    }

    GetDlgItemText(IDC_CUSTOMER_CONTROL_CLIENT_PORT_EDIT, edit_string);
    if (edit_string.GetLength())
        file_settings.memory.customer_control_client_port = atoi(edit_string.GetBuffer(0));
    if (!file_settings.memory.customer_control_client_port)
    {
        MessageWindow(false, "ERROR!", "INVALID CUSTOMER CLIENT TCP/IP PORT");
        return;
    }

    ip_address = (BYTE*)&file_settings.memory.customer_control_server_ip_address;
    configurator_ip_address = (BYTE*)&gb_file_settings.memory.gb_pro_configurator_ip_address;
     
    edit_string.Format("CUSTOMER CONTROL IP ADDRESS AND PORT: %u.%u.%u.%u:%u\nCUSTOMER CONTROL CLIENT PORT: %u\nGB ADVANTAGE CONFIGURATOR IP ADDRESS & PORT: %u.%u.%u.%u:%u\n",
        ip_address[0], ip_address[1], ip_address[2], ip_address[3], file_settings.memory.customer_control_server_tcp_port,
        file_settings.memory.customer_control_client_port,
        configurator_ip_address[0], configurator_ip_address[1], configurator_ip_address[2], configurator_ip_address[3],
        gb_file_settings.memory.gb_pro_configurator_tcp_port);

    EnableWindow(FALSE);
    MessageWindow(true, "SAVE DATA?", edit_string);

    while (theApp.message_window_queue.empty())
    {
        MSG peek_message;
        if (::PeekMessage(&peek_message, NULL, 0, 0, PM_NOREMOVE))
            theApp.PumpMessage();
        else
            Sleep(1);
    }

    EnableWindow();

    if (theApp.message_window_queue.front() == IDOK)
    {
        file_settings.memory.gb_pro_configurator_ip_address = gb_file_settings.memory.gb_pro_configurator_ip_address;
        file_settings.memory.gb_pro_configurator_tcp_port = gb_file_settings.memory.gb_pro_configurator_tcp_port;

        if (!fileWrite(MAIN_SETTINGS_INI, 0, &file_settings, sizeof(file_settings)))
        {
            edit_string.Format("INVALID FILE WRITE:\n%s", MAIN_SETTINGS_INI);
            MessageWindow(false, "ERROR!", edit_string);
        }

        if (!fileWrite(GB_PRO_SETTINGS_INI, 0, &gb_file_settings, sizeof(gb_file_settings)))
        {
            edit_string.Format("INVALID FILE WRITE:\n%s", MAIN_SETTINGS_INI);
            MessageWindow(false, "ERROR!", edit_string);
        }

        CDialog::EndDialog(0);
    }
}

void CInitialInfoDlg::OnBnClickedCancelKeypadButton()
{
    CDialog::EndDialog(0);
}

void CInitialInfoDlg::OnBnClickedClearKeypadButton()
{
    if (current_edit_focus == IDC_CUSTOMER_CONTROL_CLIENT_PORT_EDIT ||
        current_edit_focus == IDC_CUSTOMER_CONTROL_SERVER_IP_PORT_EDIT ||
        current_edit_focus == IDC_GB_PRO_CONFIGURATOR_IP_PORT_EDIT)
            SetDlgItemText(current_edit_focus, "");
}

void CInitialInfoDlg::OnEnSetfocusCustomerControlServerIpPortEdit()
{
    current_edit_focus = IDC_CUSTOMER_CONTROL_SERVER_IP_PORT_EDIT;
}

void CInitialInfoDlg::OnEnSetfocusCustomerControlClientPortEdit()
{
    current_edit_focus = IDC_CUSTOMER_CONTROL_CLIENT_PORT_EDIT;
}


void CInitialInfoDlg::OnEnSetfocusGbProConfiguratorIpPortEdit()
{
    current_edit_focus = IDC_GB_PRO_CONFIGURATOR_IP_PORT_EDIT;
}


BOOL CInitialInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

 return true;
}

LRESULT CInitialInfoDlg::DefWindowProc(UINT nMsg, WPARAM wParam, LPARAM lParam)
{
    if (nMsg == WM_EXITSIZEMOVE)
        CenterWindow(NULL);

 return CDialog::DefWindowProc(nMsg, wParam, lParam);
}

void CInitialInfoDlg::OnBnClickedInitialSouthButton()
{
    SetDlgItemText(IDC_CUSTOMER_CONTROL_SERVER_IP_PORT_EDIT, "10.1.129.82:5050");
    SetDlgItemText(IDC_CUSTOMER_CONTROL_CLIENT_PORT_EDIT, "3200");
    SetDlgItemText(IDC_GB_PRO_CONFIGURATOR_IP_PORT_EDIT, "10.1.129.63:50000");
}

void CInitialInfoDlg::OnBnClickedInitialNorthButton()
{
    SetDlgItemText(IDC_CUSTOMER_CONTROL_SERVER_IP_PORT_EDIT, "10.1.129.82:5052");
    SetDlgItemText(IDC_CUSTOMER_CONTROL_CLIENT_PORT_EDIT, "3200");
    SetDlgItemText(IDC_GB_PRO_CONFIGURATOR_IP_PORT_EDIT, "10.1.129.63:50000");
}

void CInitialInfoDlg::OnBnClickedInitialEastButton()
{
    SetDlgItemText(IDC_CUSTOMER_CONTROL_SERVER_IP_PORT_EDIT, "10.1.129.82:5054");
    SetDlgItemText(IDC_CUSTOMER_CONTROL_CLIENT_PORT_EDIT, "3200");
    SetDlgItemText(IDC_GB_PRO_CONFIGURATOR_IP_PORT_EDIT, "10.1.129.63:50000");
}

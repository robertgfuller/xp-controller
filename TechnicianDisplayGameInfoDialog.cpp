/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller	05/05/14	Display the serial number of the FTDI card for each direct connect game.
Robert Fuller	05/05/14	Refresh the Game Info every 10 seconds so techs can troubleshoot direct connect game communications easier.
Robert Fuller	12/05/13	Added a SAS communicating flag to the game info screen so techs can easily determine whether or not a gaming device is communicating with the XPC.
Robert Fuller   08/09/10    Added mods to support the XP/SAS direct connect feature.
Robert Fuller   04/06/10    Add a game configuration flag that turns GB Advantage on/off.
Dave Elquist    06/24/05    Initial Revision.
*/


// TechnicianDisplayGameInfoDialog.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include ".\techniciandisplaygameinfodialog.h"


#define GAME_INFO_TIMER_MSECS 10 * 1000


// CTechnicianDisplayGameInfoDialog dialog

IMPLEMENT_DYNAMIC(CTechnicianDisplayGameInfoDialog, CDialog)
CTechnicianDisplayGameInfoDialog::CTechnicianDisplayGameInfoDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CTechnicianDisplayGameInfoDialog::IDD, pParent)
{
}

CTechnicianDisplayGameInfoDialog::~CTechnicianDisplayGameInfoDialog()
{
}

void CTechnicianDisplayGameInfoDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TECHNICIAN_GAME_INFO_LIST, m_game_info_list_box);
}


BEGIN_MESSAGE_MAP(CTechnicianDisplayGameInfoDialog, CDialog)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_TECHNICIAN_DISPLAY_GAME_INFO_EXIT_BUTTON, OnBnClickedTechnicianDisplayGameInfoExitButton)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


// CTechnicianDisplayGameInfoDialog message handlers

void CTechnicianDisplayGameInfoDialog::OnBnClickedTechnicianDisplayGameInfoExitButton()
{
	DestroyWindow();
}

void CTechnicianDisplayGameInfoDialog::OnMouseMove(UINT nFlags, CPoint point)
{
    theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

BOOL CTechnicianDisplayGameInfoDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

    DisplayInfo();
	SetTimer(GAME_INFO_REFRESH_TIMER, GAME_INFO_TIMER_MSECS, 0);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CTechnicianDisplayGameInfoDialog::OnTimer(UINT nIDEvent)
{
    switch (nIDEvent)
    {
		case GAME_INFO_REFRESH_TIMER:
//			KillTimer(GAME_INFO_REFRESH_TIMER);
            DisplayInfo();
			break;
    }

	CDialog::OnTimer(nIDEvent);
}


void CTechnicianDisplayGameInfoDialog::DisplayInfo(void)
{
 char ascii_string[100], display_string[10];
 BYTE *ip_address;
 size_t string_length;
 DWORD file_index = 0;
 CString game_info = "";
 FileGameConfig file_game_config;
 CGameCommControl *pGameCommControl = 0;
 CGameDevice *pGameDevice;
 BYTE  collection_state; // CLEAR=0, STARTED=1, ARMED=2, FORCE=3, CANCEL=4


    m_game_info_list_box.SetHorizontalExtent(3000);
    m_game_info_list_box.ResetContent();

    while (fileRead(GAME_INFO, file_index, &file_game_config, sizeof(file_game_config)))
    {
        file_index++;

        game_info.Format("%u: %u %u (%u) %u  v%u.%2.2u   %u ", file_game_config.memory.port_number,
            file_game_config.memory.mux_id, file_game_config.memory.sas_id, file_game_config.memory.ucmc_id,
            file_game_config.memory.customer_id, file_game_config.memory.version,
            file_game_config.memory.sub_version, file_game_config.memory.denom_type);

        if (file_game_config.memory.denom_type < 100)
            game_info += "   ";

        // game gambler's bonus flag
        if (file_game_config.memory.gamblers_bonus_flag)
            game_info += "Y";
        else
            game_info += "N";

        // game bingo frenzy flag
        if (file_game_config.memory.bingo_frenzy_flag)
            game_info += "Y";
        else
            game_info += "N";

        // game multi-play poker flag
        if (file_game_config.memory.multi_play_poker_flag)
            game_info += "Y";
        else
            game_info += "N";

        // block remote game unlocks
        if (file_game_config.memory.block_remote_game_unlocks)
            game_info += "Y";
        else
            game_info += "N";

        // sound for game lock
        if (file_game_config.memory.sound_for_game_lock)
            game_info += "Y";
        else
            game_info += "N";

        // Marker flag
        if (file_game_config.memory.marker_flag)
            game_info += "Y";
        else
            game_info += "N";

        // jackpot to credit flag
        if (file_game_config.memory.jackpot_to_credit_pay)
            game_info += "Y";
        else
            game_info += "N";

        // stop game polling
        if (file_game_config.memory.stop_polling)
            game_info += "Y";
        else
            game_info += "N";

        // gb advantage on a game basis
        if (!file_game_config.memory.gb_advantage_off_flag)
            game_info += "Y   ";
        else
            game_info += "N   ";

        if (file_game_config.memory.sas_id)
        {
            pGameCommControl = theApp.GetSasGameCommControlPointer(file_game_config.memory.ucmc_id);
            if (pGameCommControl)
            {
				if (pGameCommControl->sas_com->sas_communicating)
                    game_info += "Y";
                else
                    game_info += "N";
                game_info += " ";
            }
        }
        else
                game_info += "  ";

        game_info += "   ";


        if (file_game_config.memory.sas_id)
            pGameDevice = theApp.GetSasGamePointer(file_game_config.memory.ucmc_id);
        else
            pGameDevice = theApp.GetGamePointer(file_game_config.memory.ucmc_id);

        if (pGameDevice)
            collection_state = pGameDevice->memory_game_config.collection_state;
        else
            collection_state = file_game_config.memory.collection_state;


        // game collection state
        switch (collection_state)
        {
            case CLEAR_COLLECTION_STATE:
                game_info += "CLEARED  ";
                break;

            case START_COLLECTION_STATE:
                game_info += "STARTED  ";
                break;

            case ARMED_COLLECTION_STATE:
                game_info += "ARMED    ";
                break;

            case FORCE_COLLECTION_STATE:
                game_info += "FORCED   ";
                break;

            case CANCEL_COLLECTION_STATE:
                game_info += "CANCELED ";
                break;

            default:
                game_info += "ERROR    ";
                break;
        }

        // game type, if used
        switch (file_game_config.memory.machine_type)
        {
            case 0:
                game_info += "         ";
                break;

            // BALLY_GAMETYPE
            case 1:
                game_info += "BALLY    ";
                break;

            // CEI_GAMETYPE
            case 2:
                game_info += "CEI      ";
                break;

            // IGT_GAMETYPE
            case 3:
                game_info += "IGT      ";
                break;

            // WILLIAMS_GAMETYPE
            case 4:
                game_info += "WILLIAMS ";
                break;

            // IGT_GAMETYPE_2
            case 5:
                game_info += "IGT 2    ";
                break;

            // BALLY_GAMETYPE_2
            case 6:
                game_info += "BALLY 2  ";
                break;

            default:
                game_info += "ERROR    ";
                break;
        }

        if (file_game_config.memory.ip_address)
        {
            ip_address = (BYTE*)&file_game_config.memory.ip_address;
            sprintf_s(ascii_string, sizeof(ascii_string), "%u.%u.%u.%u   ", ip_address[0], ip_address[1], ip_address[2], ip_address[3]);
            game_info += ascii_string;
        }

        if (strlen(file_game_config.memory.serial_number) != 0)
        {
            game_info += file_game_config.memory.serial_number;
        }
        m_game_info_list_box.AddString(game_info);
    }

    if (!file_index)
        m_game_info_list_box.AddString("NO GAMES FOUND");

	// EXCEPTION: OCX Property Pages should return FALSE
}

/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   12/05/13    Added code to send IDC the bill denomination when a bill is accepted in the gaming machine
Robert Fuller   12/05/13    DCB ports are now assigned thru customer control directly by serial number
Robert Fuller   12/05/13    Added IDC to XP Controller message to relay w2g social security and drivers license information after the drivers license information is scanned after of a taxable jackpot win.
Robert Fuller   11/25/13    Added a transaction id to the winning hand message so we can identify duplicate winning hand messages.
Robert Fuller   11/25/13    Added a transaction id to the clear card message so we can identify duplicate clear card messages.
Robert Fuller   11/20/13    Added a transaction id in the redemption complete message to allow gaming devices to identify duplicate messages.
Robert Fuller	07/04/13	Added ability for the IDC to send bonus win text messages to the gaming devices.
Robert Fuller	07/04/13	Send social security number and drivers license information to the IDC when the player hits a taxable jackpot for accounting purposes.
Robert Fuller	07/04/13	Save important Stratus TX messages to a file on disk and do not purge unless Stratus returns an ack.
Robert Fuller	07/04/13	Added a communication interface to the Station's Casino Casino Management System (CMS)
Robert Fuller	07/04/13	Added ability to award cashable and non-cashable credits for GBA based wins.
Robert Fuller	05/10/13	Added transaction id an time/date stamp fields to the dispenser info message.
Robert Fuller	05/10/13	Added code to save pending IDC TX messages to disk until they are acknowledged by the IDC. 
Robert Fuller	05/02/12	Added support for the 4 cassette Puloon dispenser
Robert Fuller   05/02/12    Added code for future support for the GBA interface to Stations Casino Management System.
Robert Fuller   03/08/13    Added code to allow tito pays instead of dispenser pays.
Robert Fuller   03/08/13    Added code to support the configurable dispenser payout limit..
Robert Fuller   03/08/13    Added manual dispense feature to the technician screen.
Robert Fuller   03/08/13    Removed obsolete Location Collection code.
Robert Fuller   02/04/13    Version bump.
Robert Fuller   02/04/13    Keep track of iGBA transaction ids in a linked list instead of fixed array. Only log iGBA win transactions, not GBA win transactions.
Robert Fuller	09/17/12	Added a code to support external awarded points for slot/keno win bonuses and manager awarded points.
Robert Fuller	09/17/12	Added quick enroll button to cashier menu.
Robert Fuller	09/17/12	Send stacker pull and game entered messages to the IDC.
Robert Fuller	09/17/12	Added iGBA transaction id to differentiate iGBA wins amongst multiple games.
Robert Fuller	06/20/12	Added Stratus Ticket Task codes for cashier login/logoff and end shift.
Robert Fuller   06/04/12    Added code to support the new Quick Enrollment feature.
Robert Fuller   06/04/12    Added code to communicate with the Interactive Display Controller.
Robert Fuller   06/04/12    Send Stratus message when starting or ending a cashier shift or logging in.
Robert Fuller   03/05/12    Adjusted the size of the FileGameLock data struct to compensate for the 14 characters I added for the
							player's name in the MemoryGameLock data structure.
Robert Fuller   03/05/12    Added new GB message to validate a member id selected during the enrollment process.
Robert Fuller   03/05/12    Added View Cards message to send necessary data to the GB Interface.
Robert Fuller   12/30/11    Added a Marker Credit Pending message to notify the Stratus that a marker credit payment needs to be paid.
Robert Fuller   12/30/11    Added a Marker Balance Update message.
Robert Fuller   08/03/11    Added code to support Location Collection features.
Robert Fuller   06/28/11    Allow manager to access the dispenser functions in the Collection screen.
Robert Fuller   06/08/11    Added mods to support GB with the XP/SAS direct connect feature.
Robert Fuller   01/26/11    Added fixes associated with the XP/SAS direct connect feature.
Robert Fuller   08/09/10    Added mods to support the XP/SAS direct connect feature.
Robert Fuller   06/02/10    Added code to support the new Puloon LCDM-2000 dual denomination dispenser. 
Robert Fuller   05/20/10    Print a "DM" on the ticket fill and drop reports when a dispenser malfunction is detected during a payout.
Robert Fuller   05/18/10    When adding up all the drop tickets, make sure the customer id matches the customer id of the current PAID OUT LOG report being run.
Robert Fuller   04/26/10    Added additional customer ids to the configuration settings. Get netwin data for all customer ids when calculating shift report data.
Robert Fuller   04/06/10    Add a game configuration flag that turns GB Advantage on/off.
Robert Fuller	04/07/10	Write drop report info to a file at drop time.
Robert Fuller   01/19/10    Added code to support the new Puloon dispenser.
Robert Fuller   01/19/10    Implemented a file redundancy strategy for files which store the paid game locks, dispenses, and drop tickets.
Robert Fuller   12/08/09    Once a day we purge the "dated paid ticket" file by removing all tickets in the file that over 14 days old.
Robert Fuller   09/22/09    Put 4ok win hand data in the event log.
Robert Fuller   09/03/09    Added software mods to support running the XP Controller in stand alone mode which allows locations to operate with basic features without connecting to the main frame.
Robert Fuller   07/15/09    Increased the number of shifts to print on the shift report from 4 to 21.
Robert Fuller   06/09/09    Added mechanism to accomidate manager awarded promotion points.
Robert Fuller   06/09/09    Added code to make printing bag and tag info optional.
Robert Fuller   06/09/09    Added code to support printing a drawing ticket for a GB Advantage win.
Robert Fuller   05/04/09    Removed MDT code.
Robert Fuller   04/07/09    Adjusted the card data info message processor to accommodate the fixed block size sent.
Robert Fuller   01/27/09    Fill the game lock structure with the ticket id.
Robert Fuller	01/22/09	Added structures for new card info message.
Robert Fuller   11/13/08    Compare current hand data with previous hand data message to filter out duplicates.
Robert Fuller   10/14/08    Avoid displaying the ticket pay pending tilt if the controller paying the ticket is the same as the
                            controller that logged the pending ticket.
Robert Fuller   09/30/08    Changed the variable type of the drivers license from DWORD to double to accomidate a 10 digit value.
Robert Fuller   09/29/08    Added code to print the promotion type string on the cash voucher ticket.
Robert Fuller   09/25/08    Changed the way we keep track of the game comps for each cashiers shift.
Robert Fuller   09/08/08    Added code to print the handle on the shift report.
Robert Fuller   08/19/08    Added code to support the GB Advantage feature.
Robert Fuller	06/02/08	Bumped rev to 1.3.
Robert Fuller   05/02/08    Added code to support CD2000 dispenser.
Robert Fuller	03/25/08	Added BagAndTagInfo structure and bumped rev to 1.2.
Robert Fuller	03/14/08	Changed rev and added bill message structure.
Robert Fuller	11/05/07	Added dispenser key reference. Moved TicketsByUcmcId into this file. Changed subersion.
Robert Fuller	09/05/07	Added extra byte to the DisplayGameLock structure. Changed subersion.
Dave Elquist    07/15/05    Added Marker support. Added GB Pro support.
Dave Elquist    04/13/05    Initial Revision.
*/

// Filename: Utility.h
// Misc. Functions And Types For Controller Program


#pragma once

#include "MessageBox.h"
#include <list>
#include <iostream>
#include <queue>
#include <deque>
#include <ras.h>
#include <RasError.h>

#using <System.dll>

using namespace std;
using namespace System;
using namespace System::Globalization;

typedef unsigned char BCD;

#define VERSION         3       // xp controller major version
#define SUBVERSION      "17p"   // xp controller minor version string

#define STRATUS_IP_ADDRESS  "10.1.129.1"

// network ports
#define STRATUS_PRODUCTION_PORT				3000
#define STRATUS_ENCRYPTED_PRODUCTION_PORT	3001
#define STRATUS_TEST_PORT					4000
#define STRATUS_ENCRYPTED_TEST_PORT			4001
#define STRATUS_DEVELOPMENT_PORT			5000
#define STRATUS_ENCRYPTED_DEVELOPMENT_PORT	5001

// remote network types
#define REMOTE_NETWORK_LAN			0
#define REMOTE_NETWORK_LEASE_LINE	1
#define REMOTE_NETWORK_DIAL_UP		2

// file path names
#define FILE_PATH               "files"
#define CONTROLLER_LOG_PATH     "files\\controller_logs"
#define EVENTS_PATH             "files\\events"
#define PRINTER_TEXT_PATH       "files\\printer"
#define GAME_LOCKS_PATH         "files\\game_locks"
#define CASHIER_SHIFT_PATH      "files\\shifts"
#define GB_PRO_PATH             "files\\gb_pro"
#define VLORA_PATH				"c:\\program files\\vlora"
#define STORED_METERS_PATH      "files\\stored_meters"
#define DROP_REPORTS_PATH       "files\\drop reports"
#define GAME_DATA_PATH		    "files\\game_data"
#define QUICK_ENROLL_PATH       "files\\quick_enroll"
#define QUICK_ENROLL_APPLICATION_PATH	"C:\\enroll"

// filenames
#define NEW_CONTROLLER              "new_xp_controller.exe"
#define TOUCH_CALIBRATION_FILE      "c:\\program files\\touchkit\\touchkit.exe"
#define VLORA_FILE					"c:\\program files\\vlora\\vlora.exe"
#define GAME_TENDER_FILE			"C:\\Program Files\\GamblersBonus\\GameTender Client\\GameTenderClient.exe"
#define QUICK_ENROLL_APPLICATION	"C:\\enroll\\enroll.exe"
#define MINTRONIX_TOUCH_CAL_FILE	"c:\\windows\\system32\\elotouch.cpl"
#define TEMP_FILE                   "files\\file.tmp"
#define TRANSMIT_EVENTS_FILE		"files\\transmit.evt"
#define MAIN_SETTINGS_INI           "files\\main_settings.ini"
#define LOCATION_INFO               "files\\location.info"
#define DISPENSER_INFO              "files\\dispenser.info"
#define DISPENSER_VALUES            "files\\dispenser.values"
#define DISPENSER_COMMANDS          "files\\dispenser.cmd"
#define DIEBOLD_DISPENSER_KEY				"files\\dispenser.key"
#define GAME_INFO                   "files\\game.info"
#define GAME_INFO_TEMP              "files\\game_info.tmp"
#define MASTER_CONFIG               "files\\master_controller.cfg"
#define GAME_REDEEM_RECORDS         "files\\redeem.records"
#define GAME_REDEEM_RECORDS_REDUNDANT	"files\\redeem.records~"
#define LOCATION_IDS                "files\\location.ids"
#define USER_IDS_PASSWORDS          "files\\users.pwl"
#define ENGR_IDS_PASSWORDS          "files\\engineering_users.pwl"
#define GAME_LOCK_SOUND             "files\\game_lock.wav"
#define DRINK_COMPS_TOTAL           "files\\drink_comps_total"
#define DRINK_COMPS_SHIFT           "files\\drink_comps_shift"
#define MUX_MESSAGES                "files\\messages.mux"
#define MUX_MESSAGES_BAK            "files\\messages_mux.bak"
#define PENDING_TICKETS             "files\\pending_tickets"
#define PAID_TICKETS                "files\\paid_tickets"
#define PAID_TICKETS_REDUNDANT		"files\\paid_tickets~"
#define PAID_TICKETS_TX             "files\\paid_tickets.tx"
#define DATED_PAID_TICKETS          "files\\dated_paid_tickets"
#define DROP_TICKETS                "files\\drop_tickets"
#define DROP_TICKETS_REDUNDANT      "files\\drop_tickets~"
#define EMAIL_ADDRESS_LIST          "files\\email_list"
#define W2G_RECORDS                 "files\\w2g_records"
#define LOCATION_W2G_RECORDS        "files\\location_w2g_records"
#define PREVIOUS_FILL               "files\\previous_dispenser_fill_time"
#define PREVIOUS_DROP_TIME               "files\\previous_drop_time"
#define PREVIOUS_LOCATION_DROP_TIME      "files\\previous_location_drop_time"
#define ADJUSTMENT_RECORDS          "files\\adjustment_records"
#define QUICK_ENROLL                "files\\quick_enroll"
#define MANUAL_DISPENSE_DAILY_LIMIT "files\\mddl"
#define PENDING_IDC_TX_MESSAGES		"files\\pending_idc_messages.tx"
#define REDUNDANT_PENDING_IDC_TX_MESSAGES		"files\\pending_idc_messages.tx~"
#define PENDING_CMS_TX_MESSAGES		"files\\pending_cms_messages.tx"
#define PENDING_STRATUS_TX_MESSAGES	"files\\pending_stratus_messages.tx"

#define CASHIER_SHIFT_1             "files\\shifts\\shift_1."
#define CASHIER_SHIFT_2             "files\\shifts\\shift_2."
#define CASHIER_SHIFT_3             "files\\shifts\\shift_3."
#define CASHIER_SHIFT_4             "files\\shifts\\shift_4."
#define CASHIER_SHIFT_5             "files\\shifts\\shift_5."
#define CASHIER_SHIFT_6             "files\\shifts\\shift_6."
#define CASHIER_SHIFT_7             "files\\shifts\\shift_7."
#define CASHIER_SHIFT_8             "files\\shifts\\shift_8."
#define CASHIER_SHIFT_9             "files\\shifts\\shift_9."
#define CASHIER_SHIFT_10            "files\\shifts\\shift_10."
#define CASHIER_SHIFT_11            "files\\shifts\\shift_11."
#define CASHIER_SHIFT_12            "files\\shifts\\shift_12."
#define CASHIER_SHIFT_13            "files\\shifts\\shift_13."
#define CASHIER_SHIFT_14            "files\\shifts\\shift_14."
#define CASHIER_SHIFT_15            "files\\shifts\\shift_15."
#define CASHIER_SHIFT_16            "files\\shifts\\shift_16."
#define CASHIER_SHIFT_17            "files\\shifts\\shift_17."
#define CASHIER_SHIFT_18            "files\\shifts\\shift_18."
#define CASHIER_SHIFT_19            "files\\shifts\\shift_19."
#define CASHIER_SHIFT_20            "files\\shifts\\shift_20."
#define CASHIER_SHIFT_21            "files\\shifts\\shift_21."

#define SHIFT_TICKETS_1             "files\\shifts\\shift_tickets1"
#define SHIFT_TICKETS_2             "files\\shifts\\shift_tickets2"
#define SHIFT_TICKETS_3             "files\\shifts\\shift_tickets3"
#define SHIFT_TICKETS_4             "files\\shifts\\shift_tickets4"
#define SHIFT_TICKETS_5             "files\\shifts\\shift_tickets5"
#define SHIFT_TICKETS_6             "files\\shifts\\shift_tickets6"
#define SHIFT_TICKETS_7             "files\\shifts\\shift_tickets7"
#define SHIFT_TICKETS_8             "files\\shifts\\shift_tickets8"
#define SHIFT_TICKETS_9             "files\\shifts\\shift_tickets9"
#define SHIFT_TICKETS_10            "files\\shifts\\shift_tickets10"
#define SHIFT_TICKETS_11            "files\\shifts\\shift_tickets11"
#define SHIFT_TICKETS_12            "files\\shifts\\shift_tickets12"
#define SHIFT_TICKETS_13            "files\\shifts\\shift_tickets13"
#define SHIFT_TICKETS_14            "files\\shifts\\shift_tickets14"
#define SHIFT_TICKETS_15            "files\\shifts\\shift_tickets15"
#define SHIFT_TICKETS_16            "files\\shifts\\shift_tickets16"
#define SHIFT_TICKETS_17            "files\\shifts\\shift_tickets17"
#define SHIFT_TICKETS_18            "files\\shifts\\shift_tickets18"
#define SHIFT_TICKETS_19            "files\\shifts\\shift_tickets19"
#define SHIFT_TICKETS_20            "files\\shifts\\shift_tickets20"
#define SHIFT_TICKETS_21            "files\\shifts\\shift_tickets21"

#define SHIFT_TICKETS               "files\\shifts\\tickets."
#define SHIFT_LOG                   "files\\shifts\\shift.log"
#define GB_PRO_SETTINGS_INI         "files\\gb_pro\\gb_pro_settings.ini"
#define GB_PRO_PROMOTION_CODES_FILE "files\\gb_pro\\promotioncodes.txt"
#define GB_PRO_MANAGER_POINTS_FILE  "files\\gb_pro\\gb_pro_points"
#define GB_PRO_MANAGER_TICKETS_FILE "files\\gb_pro\\gb_pro_tickets"
#define GB_PRO_POINTS_TICKETS_FILE  "files\\gb_pro\\game_points_tickets"
#define GB_PRO_PRAC_CONFIG_FILE		"files\\gb_pro\\prac_config."
#define CV_PENDING_PRINTER_FILE     "files\\gb_pro\\cv_pending_printer"
#define GB_PRO_ASCII_MESSAGES_FILE	"files\\gb_pro\\ascii_machine_messages"
#define NETWORK_SETTINGS			"files\\network.settings"
#define PENDING_GAMELOCK			"files\\game_locks\\pending_gamelock"
#define STORED_METERS		      	"files\\stored_meters\\meters_mux"
#define STORED_DROP_METERS		    "files\\stored_meters\\drop_meters_"
#define GAME_DATA				    "files\\game_data\\game_data"
#define LOGIN_DATA				    "files\\game_data\\login_data_"

#define VERSION_STRING_FILE			"files\\version_info"

#define USER_FILE_RECORD_STRING_LENGTH  21

#define ENCRYPT_STRING  "sfj34080jkvlAHGJ4236jliopqac5104635so79R0FZ9Y78254ohaQ47f9q2"  // 60 bytes long

#define MAXIMUM_CUSTOMER_IDS    18  // maximum customer ids per location

#define GB_PRO_DISPLAY_LINE_ONE_DEFAULT			"CONGRATULATIONS! YOU WON"
#define GB_PRO_DISPLAY_LINE_TWO_POINTS_DEFAULT	"$$$$ POINTS!"
#define GB_PRO_DISPLAY_LINE_TWO_CASH_DEFAULT	"$$$$ IN CREDITS!"

// control characters
#define STX 0x02    // start of text
#define ETX 0x03    // end of text
#define DLE 0x10    // data link escape

// protocol header byte
#define GB_PRO_CONFIGURATOR_HEADER_BYTE              0xFF
#define ONE_ARM_BANDIT_HOST_HEADER_BYTE              0xFF


#define ACTIVE_GB_PRO_WIN_CATEGORY_NOT_FOUND         0xFF

// timeouts in seconds
#define OFFLINE_TIMER_LIMIT 30  // maximum time to wait for response from Stratus
#define MDT_SERVER_TIMEOUT  60  // maximum time to wait for response from Million Dollar Ticket Server

#define LAST_BUTTON_PRESS_TIMEOUT  2000    // timeout in milliseconds between button presses for alpha-numeric keypad

#define TCPIP_MESSAGE_BUFFER_SIZE       4096    // maximum tcp/ip message packet size
#define MAXIMUM_EVENT_LOG_DATA_SIZE     5000    // maximum message size for event logging (data area only)
#define MAXIMUM_FILE_BACKUP_DATA_SIZE   500     // maximum data portion size to send for file backups
#define MAXIMUM_FILE_PATH_NAME          255     // maximum file name length including path

// seconds to wait before checking
#define DISPENSER_COMM_CHECK                    10      // dispenser comm connection

// printer defines
#define PRINTER_FORM_FEED       "\012\012\012"
#define PRINTER_BIG_PRINT       "\016"
#define PRINTER_NORMAL_PRINT    "\017"

#define PACKET_IDENTIFIER       0xFFFF
#define LONG_HEADER_IDENTIFIER  0xFFFFFFFF

#define MAXIMUM_CUSTOMER_CONTROL_CLIENTS            5
#define MAXIMUM_MULTI_CONTROLLER_CONNECTIONS        5

#define MAXIMUM_GAME_LOCKS_LIST 30

// socket ports
#define CUSTOMER_CONTROL_CLIENT_UDP_PORT    3110
#define MASTER_CONTROLLER_PORT              6000
#define GAME_LOCK_PRINTER_SOCKET_PORT       10001

// change endian for 2-byte integers
#define endian2(x)  x = ((short)(((x>>8)&0xff) | (x<<8)))

// change endian for 4-byte integers
#define endian4(x)  x = ((long)(((x>>24)&0xff) | ((x>>8)&0xff00) | ((x<<8)&0xff0000L) | (x<<24)))

// serial message sizes
#define MINIMUM_SERIAL_MESSAGE_SIZE				6
#define MINIMUM_ENCRYPTED_SERIAL_MESSAGE_SIZE	7
#define MAXIMUM_SERIAL_MESSAGE_SIZE				0xFF

// header size for packets between stratus and serial devices
#define STRATUS_HEADER_SIZE	22

// maximum email address length including null terminator
#define MAXIMUM_EMAIL_ADDRESS_LENGTH    81

// club codes
#define MOUNTAIN_VIEW_CLUB      1
#define GAMBLERS_BONUS_CLUB     2
#define LONGHORN_BIGHORN_CLUB   3

// task codes for tcp/ip
#define MPI_TASK_PLAYER_ACCOUNTING	1
#define WAA_TASK_WIDE_AREA_AWARD	2
#define TKT_TASK_TICKET				3
#define MMI_TASK_GAME_METERS		4
#define SMPI_TASK_PLAYER_ACCOUNTING	5	// this other task code is a Stratus code kludge

// player account constants for MPI_TASK_PLAYER_ACCOUNTING
#define MPI_ACCOUNT_LOGIN			1
#define MPI_ACCOUNT_LOGOUT			2
#define MPI_REDEEM_INQUIRY			3
#define MPI_REDEEM_REQUEST			4
#define MPI_WINNING_BINGO_HAND		5
#define MPI_WINNING_BINGO_CARD		6
#define MPI_CLEAR_BINGO_CARD		7
#define MPI_JACKPOT					8
#define MPI_GB_PRO_POINTS           9
#define MPI_NEW_ACCOUNT_LOGIN       11
#define MPI_ACCOUNT_LOGIN_INFO		12
#define MPI_MARKER_REQUEST          18
#define MPI_MARKER_CREDIT           19
#define MPI_ABORT_MARKER_ADVANCE_REQUEST    20

#define MPI_GB_SESSION_UPDATE		21
#define MPI_GB_SESSION_INFO_REQUEST	22

#define MPI_MARKER_CREDIT_PENDING   23

#define MPI_PROCESS_ESCROW_REDEEMED_POINTS	24

#define MPI_ABORT_MARKER_CREDIT           25
#define MPI_MARKER_AFT_COMPLETE           26

//#define MPI_GB_PRO_POINTS_NEW_MSG   29
#define MPI_4OK_WIN					30
#define MPI_CLEAR_ESCROWED_REDEMPTION_MONEY       32	// used on Wincontroller
#define MPI_VALIDATE_MEMBER_ID       33	// inquires whether or not member id is already selected or not
#define MPI_GB_PRO_POINTS_NEW_MSG   35

#define MPI_NEW_WINNING_BINGO_HAND		36

#define MPI_NEW_CLEAR_BINGO_CARD		37

#define MPI_NEW_REDEEM_REQUEST			38

#define MPI_REDEEM_COMPLETE				39

// wide area award constants for WAA_TASK_WIDE_AREA_AWARD
#define WAA_QUERY_AFTER_WIN				1
#define WAA_JACKPOT						2
#define WAA_QUERY						3
#define WAA_CASH_FRENZY_QUERY_AFTER_WIN	4
#define WAA_CASH_FRENZY_JACKPOT			5
#define WAA_CASH_FRENZY_QUERY			6


// ticket related constants for TKT_TASK_TICKET
#define TKT_TICKET_ADD			50
#define TKT_TICKET_QUERY		51  // not used
#define TKT_NET_WIN_REPORT		52
#define TKT_TICKET_DESTROYED	53  // not used
#define TKT_TICKET_PAID			56
#define TKT_FILL_MESSAGE		57
#define TKT_SEND_BILL_COUNT		58
#define TKT_JACKPOT_CREDIT      59
#define TKT_SHIFT_END_INFO      60
#define TKT_CASHIER_LOG_ON_OFF  61
#define TKT_MANUAL_DISPENSE		62

// dispenser types
#define DISPENSER_TYPE_NOT_SET      	0
#define DISPENSER_TYPE_NO_DISPENSER 	1
#define DISPENSER_TYPE_CASH_DRAWER  	2
#define DISPENSER_TYPE_JCM_20       	3
#define DISPENSER_TYPE_JCM_100      	4
#define DISPENSER_TYPE_DIEBOLD      	5
#define DISPENSER_TYPE_ECASH        	6
#define DISPENSER_TYPE_CD2000       	7
//#define DISPENSER_TYPE_PULOON       	8
#define DISPENSER_TYPE_LCDM1000_20    	8
#define DISPENSER_TYPE_LCDM2000_20_100	9
//#define DISPENSER_TYPE_LCDM2000_5_20	10
#define DISPENSER_TYPE_LCDM4000_20_100	10

// user levels
#define CASHIER     1
#define MANAGER     2
#define COLLECTOR   3
#define TECHNICIAN  4
#define SUPERVISOR  5
#define ENGINEERING 6

// event logging types
#define CONTROLLER_TX_ACK                   100
#define CONTROLLER_TX_NO_ACK                101
#define CONTROLLER_RX                       102
#define PRAC_CONTROLLER_RX					103

#define SAS_REAL_TIME_EVENTS				110
#define SAS_HANDPAY_PENDING_INFO_EVENT		111
#define SAS_METER_DATA_EVENT				112
#define SAS_BILL_METER_DATA_EVENT			113

#define CMS_LOGIN_EVENT						150
#define CMS_LOGOUT_EVENT					151
#define CMS_GBA_WIN_EVENT					152
#define CMS_GBA_WIN_ACK_EVENT				153
#define CMS_LOGIN_STATUS_REQUEST_ACK_EVENT	154

#define CC_CLIENT_RECEIVE_MESSAGES          200
#define CC_CLIENT_SOCKET_CLOSE              201
#define CC_CLIENT_INITIAL_CONNECTION        202
#define USER_INTERFACE_LOGON                300
#define USER_INTERFACE_LOGOFF               301
#define CONTROLLER_STARTED                  302
#define CONTROLLER_STOPPED                  303
#define SHIFT_START			                304
#define SHIFT_END							305
#define FOUR_OF_A_KIND_EVENT				306
#define MANAGER_AWARDED_POINTS				307

#define DISPENSER_SWITCH_OPEN               400
#define DISPENSER_SWITCH_CLOSED             401
#define DISPENSER_SWITCH_MALFUNCTION        402
#define JCM_DISPENSER_DISPENSE              500
#define JCM_DISPENSER_DISPENSE_REPLY        501
#define JCM_REJECT_REPLY                    502
#define ECASH_DISPENSER_DISPENSE            510
#define ECASH_DISPENSER_DISPENSE_REPLY      511
#define DIEBOLD_DISPENSER_DISPENSE          520
#define DIEBOLD_DISPENSER_DISPENSE_REPLY    521
#define CD2000_DISPENSER_DISPENSE           530
#define CD2000_DISPENSER_DISPENSE_REPLY     531
#define PULOON_DISPENSER_DISPENSE           540
#define PULOON_DISPENSER_DISPENSE_REPLY     541
#define STRATUS_RECEIVE_MESSAGES            700
#define STRATUS_TRANSMIT_MESSAGES           701
#define STRATUS_ENCRYPTED_TRANSMIT_MESSAGES	702
#define SLAVE_RECEIVE_MESSAGES              800
#define SLAVE_TRANSMIT_MESSAGES             801

// customer control server commands
#define CC_SERVER_COMPUTER_INFORMATION      1
#define CC_SERVER_FILE_BACKUP               2 //????????????work
#define CC_SERVER_CONTROLLER_EVENT			3

//????????????delete ?????????work replace commands with customer control
#define LVIEW_SERIAL_DISPLAY                        3
#define LVIEW_DISPENSER_INFO_GENERAL                4
#define LVIEW_DISPENSER_INFO_DISPLAY_MESSAGES       5
#define LVIEW_DISPENSER_INFO_DISPLAY_RAW_MESSAGES   6
#define LVIEW_DISPENSER_INFO_CLEAR_ERRORS           7
#define LVIEW_GAME_INFO_GENERAL                     8
#define LVIEW_GAME_INFO_SERIAL_ERRORS               9
#define LVIEW_CONTROLLER_GAME_INFO_START_LIST       10
#define LVIEW_CONTROLLER_GAME_INFO_END_LIST         11
#define LVIEW_GAME_INFO_SET_COLLECTION_STATE        13
#define LVIEW_GAME_INFO_START_OR_STOP_POLLING       15
#define LVIEW_GAME_INFO_RESET_GAME                  16
#define LVIEW_GET_MUX_MESSAGES_FILE                 18
#define LVIEW_CLEAR_MUX_MESSAGES_FILE               19
#define LVIEW_ADD_TO_MUX_MESSAGES_FILE              20
#define LVIEW_GET_LOCATION_FILE_DATA                21
#define LVIEW_GET_DISPENSER_FILE_DATA               22
#define LVIEW_GET_GAME_FILE_DATA                    23
#define LVIEW_NETWORK_STATS                         30

//??????????work
// customer control client commands
#define CC_CLIENT_PASSWORD_PROMPT       1
#define CC_CLIENT_REBOOT_CONTROLLER     2
#define CC_CLIENT_MEMORY_SETTINGS       3
#define CC_CLIENT_LOCATION_SETTINGS     4
#define CC_CLIENT_DISPENSER_SETTINGS    5
#define CC_CLIENT_GAME_RECORD           6
#define CC_CLIENT_CLEAR_GAME_RECORDS    7
#define CC_CLIENT_TOTAL_GAME_RECORDS    8
#define CC_CLIENT_ASCII_STRING          9
#define CC_CLIENT_MACHINE_RX_TX_LIST    10
#define CC_CLIENT_MACHINE_PORT_DISPLAY  11

// Customer Control Client UDP command flags
#define DISPENSER_RAW_DATA			1
#define DISPENSER_TEXT_MESSAGE		2
#define MACHINE_TRANSMIT			3
#define MACHINE_VALID_RECEIVE		4
#define MACHINE_INVALID_RECEIVE		5
#define INVALID_RX_OFFLINE_POLL		6
#define INVALID_RX_MUX_IDLE			7
#define INVALID_RX_EXTRA_BYTES		8
#define INVALID_RX_BUFFER_OVERFLOW	9
#define INVALID_RX_INVALID_HANDLE	10
#define INVALID_RX_INVALID_READ		11
#define INVALID_RX_MESSAGE_HEADER	12
#define INVALID_RX_FAILED_CRC		13
#define INVALID_RX_UNREQUESTED_MSG	14
#define INVALID_RX_SEQUENCE_ERROR	15
#define INVALID_RX_OFFLINE_MUX_ID	16
#define INVALID_RX_OFFLINE_COMMAND	17

// task codes for multiple controller setup
#define SLAVE_TKT_TASK_TICKET       50
#define SLAVE_TKT_SYSTEM_LOGIN      100
#define SLAVE_TKT_TICKET_PAID       101
#define SLAVE_TKT_TICKET_REMOVE     102
#define SLAVE_TKT_TICKET_QUERY      103
#define SLAVE_TKT_GAME_LOCK_QUERY   104
#define SLAVE_TKT_GAME_UNLOCK       105
#define SLAVE_TKT_KEEP_ALIVE		106

// collection states
#define CLEAR_COLLECTION_STATE  0
#define START_COLLECTION_STATE  1
#define ARMED_COLLECTION_STATE  2
#define FORCE_COLLECTION_STATE  3
#define CANCEL_COLLECTION_STATE 4

// collection types
#define UCMC_COLLECTION			0
#define LOCATION_COLLECTION		1

// bit flags for sending email messages
#define STRATUS_LINK_DOWN           0x00000001
#define STRATUS_LINK_UP             0x00000002
#define SERIAL_PORT_COMM_PROBLEM    0x00000004
#define SERIAL_PORT_NO_RESPONSE     0x00000008
#define PLAYER_CARD_STUCK_SENSOR    0x00000010
#define PLAYER_CARD_DEAD_READER     0x00000020
#define PLAYER_CARD_ABANDONED_CARD  0x00000040
#define PROGRESSIVE_LINK_DOWN       0x00000080
#define MDT_EMAIL_DRAW              0x00000100

// total number of game types
#define TOTAL_MACHINE_TYPES     6

// game type arrays for game serial, change lamp, max. service on, max. service off,
// min. service pulse, max. service pulse, game start, game doors
#define BALLY_GAMETYPE      {0x00, 0x00, 0x50, 0x50, 0x46, 0x4B, 0x04, 0x02}
#define CEI_GAMETYPE        {0x00, 0x00, 0x37, 0x37, 0x2D, 0x34, 0x04, 0x02}
#define IGT_GAMETYPE        {0x03, 0x01, 0x1E, 0x1E, 0x19, 0x23, 0x03, 0x03}
#define WILLIAMS_GAMETYPE   {0x00, 0x00, 0x34, 0x34, 0x2D, 0x37, 0x04, 0x02}
#define IGT_GAMETYPE_2      {0x03, 0x01, 0x1E, 0x1E, 0x19, 0x23, 0x05, 0x02}
#define BALLY_GAMETYPE_2    {0x00, 0x00, 0x50, 0x50, 0x46, 0x4B, 0x05, 0x02}

// ticket status flags
#define TICKET_NOT_FOUND            1
#define SYSTEM_FILE_ERROR           2
#define TICKET_ALREADY_PAID         3
#define INVALID_TICKET_STATUS       4
#define TICKET_PAY_PENDING          5
#define INVALID_USER_LOGIN_ID		6
#define STRATUS_FILE_ERROR_ASCII    'A'
#define TICKET_PAYABLE_ASCII        '0'
#define TICKET_NOT_FOUND_ASCII      '1'
#define STRATUS_FILE_ERROR_ASCII_2  '2'
#define TICKET_ALREADY_PAID_ASCII   '3'
#define INVALID_TICKET_STATUS_ASCII '4'
#define TICKET_PAY_PENDING_ASCII    '5'
#define INVALID_USER_LOGIN_ID_ASCII	'6'

#define MAXIMUM_MDT_SERVER_MSG_SIZE 2048
#define MDT_MAXDRAW                 9       // MAXDRAW

#define MAXIMUM_CLUB_NAME_FOR_MUX		18
#define MAXIMUM_CLUB_ADDRESS_FOR_MUX	30

#define PLAYER_INFO_NAME_SIZE	17

// Million Dollar Ticket Server commands
#define MDT_VEND        21  // TC_VEND
#define MDT_PAY         22  // TC_PAY
#define MDT_VOID        23  // TC_VOID
#define MDT_ERROR       24  // TC_ERROR
#define MDT_DRAW        25  // TC_DRAW
#define MDT_GETDRAW     26  // TC_GETDRAW
#define MDT_CHECKTKT    27  // TC_CHECKTKT

#define MAXIMUM_GBPRO_CONFIGURATOR_SERVER_MSG_SIZE 512

#define MAXIMUM_GBPRO_MEDIA_BOX_SERVER_MSG_SIZE 256


// GB Pro Configurator Server to Controller Message Indexes
#define HEADER_INDEX				0
#define MESSAGE_ID_INDEX			1
#define MESSAGE_LENGTH_INDEX		2
#define MESSAGE_COMMAND_TYPE_INDEX	3
#define CUSTOMER_ID_INDEX			4

#define RANK_MASK   0x0f
#define NO_4OK      0x0f


#define DAILY_TIMER					WM_TIMER + 1
#define GAME_TENDER_TIMER			DAILY_TIMER + 1
#define GAME_INFO_REFRESH_TIMER		GAME_TENDER_TIMER + 1

#define DEFAULT_PAYOUT_LIMIT		8000


extern CMessageBox *pMessageBox;
extern DWORD crc32_table[256];

const int MAX_MEDIA_STRING_LENGTH = 100;

// function prototypes
DWORD ReflectCrc32(DWORD reference, BYTE bit_count);
DWORD GetCrc32(BYTE *buffer, DWORD length);
WORD CalculateStratusCrc(BYTE *buffer, int length);
BYTE CalculateMuxCrc(BYTE *buffer, BYTE size);
void logMessage(CString message);
BOOL fileWriteSizeLimited(char *filename, void *file_struct, DWORD file_struct_size);
BOOL fileWrite(char *filename, int index, void *file_struct, DWORD file_struct_size);
BOOL fileRead(char *filename, DWORD index, void *file_struct, DWORD file_struct_size);
DWORD FileByteTotal(char *filename);
DWORD fileRecordTotal(char *filename, DWORD record_size);
int fileGetRecord(char *filename, void *return_value, DWORD record_size, DWORD search_record);
BOOL fileDeleteRecord(char* filename, DWORD record_size, DWORD record_position);
void writeRedundantFile(CString file_name, int index, BYTE* struct_ptr, int byte_count);
char* convertTimeStamp(time_t time_stamp);
char* SetTimeStamp();
void SystemShutdown();
void MessageWindow(BOOL wantCancelButton, CString windowCaption, CString windowText);



unsigned char bcdToUnsignedChar (unsigned char bcd);
ULONG bcd4ToUnsignedLong (unsigned char bcd0,
                                unsigned char bcd1,
                                unsigned char bcd2,
                                unsigned char bcd3);
ULONG bcd5ToUnsignedLong (unsigned char bcd0, unsigned char bcd1, unsigned char bcd2, unsigned char bcd3, unsigned char bcd4);
ULONG bcd2ToUnsignedLong (unsigned char bcd0, unsigned char bcd1);
void unsignedLongToBcd4 (ULONG u_long, BCD* bcd);
void unsignedLongToBcd5 (ULONG u_long, BCD* bcd);

struct DispenserUdpMessageFormat
{
    DWORD   command_header;
    CString data;
};

struct EmailMessageFormat
{
    DWORD   type;
    CString subject;
    CString body;
};

#pragma pack(1)
 struct BillMessage
 {
    DWORD bill_amount_100;
    DWORD bill_amount_20;
    DWORD bill_amount_xx;   // not used
    DWORD reject_amount_100;
    DWORD reject_amount_20;
    DWORD reject_amount_xx; // not used
    char  message_type;
 };
 #pragma pack()

#pragma pack(1)
 struct DispenseInfoMessage
 {
    DWORD transaction_id;
	DWORD day;
	DWORD month;
	DWORD year;
	DWORD hour;
	DWORD minute;
	DWORD second;
    DWORD bill_amount_100;
    DWORD bill_amount_20;
    DWORD bill_amount_xx;   // not used
    DWORD reject_amount_100;
    DWORD reject_amount_20;
    DWORD reject_amount_xx; // not used
    char  message_type;
 };
 #pragma pack()

#pragma pack(1)
 struct StackerPulledMessage
 {
    BYTE  pulled; // 1 = pulled
};
 #pragma pack()

#pragma pack(1)
struct UserLoginLogoff
{
    DWORD login_id;
    BYTE log_in;		// 1 = logon, 0 = logoff
};
#pragma pack()


#pragma pack(1)
struct FileDropTicket
{
    DWORD ticket_id;
    DWORD ucmc_id;
    DWORD amount;
    BYTE  ticket_filled;
    BYTE  dispenser_ucmc_id;
    DWORD customer_id;
	BYTE  dispenser_malfunction;
    BYTE  unused_space[95];
};
 #pragma pack()

#pragma pack(1)
struct MemoryGameLock
{
    DWORD ticket_id;
    DWORD ticket_amount;
    DWORD marker_balance;
    BYTE  time_stamp[6];    // BCD: hour, minute, second, day, month, year
    BYTE  taxable_flag;
    BYTE  jackpot_to_credit_flag;
    DWORD ucmc_id;
    BYTE  mux_id;
    DWORD location_id;
    DWORD player_account;
    WORD  game_port;
    char  status;
	BYTE  sas_id;
	char player_name[14];
    DWORD   ss_number;
    double   drivers_license;
};
 #pragma pack()

#pragma pack(1)
struct MemoryPaidTicket
{
    MemoryGameLock  memory_game_lock;
    DWORD   cashier_id;
    time_t  timestamp;
    BYTE    ticket_filled;
    BYTE    dispenser_ucmc_id;
};
 #pragma pack()

#pragma pack(1)
struct FilePaidTicket
{
    MemoryPaidTicket    memory;
	BYTE				dispenser_malfunction;
    BYTE                unused_space[184];
};
 #pragma pack()

#pragma pack(1)
struct FileGameLock
{
    MemoryGameLock game_lock;
//    BYTE           unused_space[185];
    BYTE           unused_space[173];
};
 #pragma pack()

#pragma pack(1)
struct SystemUser
{
    DWORD id;
    DWORD password;
    char  first_name[USER_FILE_RECORD_STRING_LENGTH];
    char  last_name[USER_FILE_RECORD_STRING_LENGTH];
};

struct ControllerUser
{
    SystemUser  user;
    time_t      autologout_timeout;
    time_t      time_setting;
};

struct CashierAndManagerUserFile
{
    SystemUser  user;
    BYTE        unused_space[200];
};

struct EncryptedUserFileRecord
{
    BYTE password[4];
    BYTE last_name[USER_FILE_RECORD_STRING_LENGTH];
    BYTE first_name[USER_FILE_RECORD_STRING_LENGTH]; // backwards
    BYTE user_id[5];
};

struct MemorySettingsIni
{
    BYTE    print_drink_comps;
    BYTE    force_cashier_shifts;   // flag for cashiers' shifts 0=no, 1=yes, 2=yes with restricted rights
    BYTE    print_account_tier_level;
    WORD    printer_comm_port;
    DWORD   stratus_ip_address;
    WORD    stratus_tcp_port;
    DWORD   customer_control_server_ip_address;
    WORD    customer_control_server_tcp_port;
    WORD    customer_control_client_port;
    DWORD   masters_ip_address;
    DWORD   game_lock_printer_ip_address;
    DWORD   auto_logout_timeouts[ENGINEERING];
    BYTE    activate_gb_advantage_support;
    DWORD   marker_printer_ip_address;
    WORD    marker_printer_port;
    DWORD   gb_pro_configurator_ip_address;
    WORD    gb_pro_configurator_tcp_port;
    BYTE    do_not_print_dispense_info;
    BYTE    print_full_shift_report;
    BYTE    bypass_game_tender_automated_w2g;
    BYTE    reserved[5];
    DWORD   cms_ip_address;
    WORD    cms_tcp_port;
    DWORD   idc_ip_address;
    WORD    idc_tcp_port;
};

// filename MAIN_SETTINGS_INI
struct FileSettingsIni
{
    MemorySettingsIni   memory;
//    BYTE                unused_space[186];
    BYTE                unused_space[180];
};

struct MemoryDispenserConfig
{
    WORD  comm_port;
    DWORD dispenser_type;
    DWORD ucmc_id;
    BYTE  autopay_allowed;
    BYTE  autopay_manager_approval;
    BYTE  dispense_100s_when_20s_low;
	BYTE tito;
	DWORD dispenser_payout_limit;
	DWORD manual_dispense_per_transaction_limit;
	DWORD manual_dispense_daily_limit;
};

// filename DISPENSER_INFO
struct FileDispenserConfig
{
    MemoryDispenserConfig   memory;
//    BYTE                    unused_space[195];
    BYTE                    unused_space[187];
};

struct MemoryLocationConfig
{
    char    name[32];
    char    address[32];
    DWORD   customer_id;
    WORD    club_code;
    WORD    property_id;
    DWORD   baud_rate;
    BYTE    allow_player_autologout_for_test;
    DWORD   unused_space_1;						// not used, email_smtp_server_ip_address, customer control server will send emails
    WORD    unused_space_2;						// not used, email_smtp_server_port, customer control server will send emails
    DWORD   email_notify_message_limit;
    BYTE    email_notify_percent;
    DWORD   customer_id2;
    DWORD   customer_id3;
};

// filename LOCATION_INFO
struct FileLocationConfig
{
    MemoryLocationConfig    memory;
    BYTE unused_space[192];
};

struct MemoryGameConfig
{
    DWORD ucmc_id;
    DWORD customer_id;
    DWORD ip_address;   // ip address, if used instead of serial port connection 
    WORD  port_number;  // serial or ip port number
    WORD  denom_type;   // NONE=0, $0.01=1, $0.05=5, $0.10=10, $0.25=25, $0.50=50, $1.00=100, $5.00=500
    BYTE  mux_id;
    BYTE  gamblers_bonus_flag;
    BYTE  bingo_frenzy_flag;
    BYTE  multi_play_poker_flag;
    BYTE  mux_dip_switches; // dip switch settings on the mux card
    BYTE  block_remote_game_unlocks;
    BYTE  sound_for_game_lock;
    BYTE  stop_polling;
    BYTE  machine_type;
    BYTE  version;
    BYTE  sub_version;
    BYTE  collection_state; // CLEAR=0, STARTED=1, ARMED=2, FORCE=3, CANCEL=4
    BYTE  marker_flag;
    BYTE  jackpot_to_credit_pay;
	BYTE  club_code;
	BYTE  gb_advantage_off_flag;
    BYTE  sas_id;
//    BYTE  collection_type; // UCMC COLLECTION = 0, LOCATION COLLECT = 1
    BYTE  reserved;
	char  machine_id[9];
	char  serial_number[20];
};

// filename GAME_INFO
struct FileGameConfig
{
    MemoryGameConfig    memory;
    BYTE                unused_space[165];
};

struct MemoryPracConfig
{
	DWORD ip_address;	// ip address, if used instead of serial port connection
	WORD  port_number;	// serial or ip port number
};

// filename GB_PRO_PRAC_CONFIG_FILE
struct FileMemoryPracConfig
{
	MemoryPracConfig	memory;
	BYTE				unused_space[200];
};

struct EventMessage
{
    struct
    {
        DWORD   header;
        DWORD   customer_id;
        WORD    port_number;
        time_t  time_stamp;
        DWORD   event_type;
        DWORD   data_length;
    } head;

    BYTE data[MAXIMUM_EVENT_LOG_DATA_SIZE];
};

struct FileGameRedeemRecord
{
    DWORD   ucmc_id;
    DWORD   amount;
    DWORD   customer_id;
    WORD    port_number;
    BYTE    unused_space[100];
};

struct GameCommPortInfo
{
    WORD  port_number;   // serial or ip port number
    DWORD baud_rate;
    DWORD ip_address;   // ip address, if used instead of serial port connection
};

struct SocketMessage
{
    struct
    {
        DWORD   header;
        WORD    command;
        DWORD   data_length;
    } head;

    char data[TCPIP_MESSAGE_BUFFER_SIZE];
};

struct StratusMessageFormat
{
    WORD    message_length; // total data block size
    WORD    task_code;      // 1=player accounting(MPI), 2=wide area award(WAA), 3=ticket related data(TKT), 4=game meters(MMU)
    WORD    property_id;
    WORD    club_code;
    WORD    serial_port;
    WORD    mux_id;
    WORD    send_code;
    DWORD   customer_id;
    DWORD   ucmc_id;
    char    data[MAXIMUM_SERIAL_MESSAGE_SIZE];
    bool    data_to_event_log;
};

struct TcpIpRx
{
    WORD index;
    BYTE data[TCPIP_MESSAGE_BUFFER_SIZE];
};

struct StratusProtocolHeader
{
    WORD header;
    WORD length;
    WORD route_info;
};
#pragma pack()


#pragma pack(1)
struct IdcProtocolHeader
{
    BYTE	header;
    BYTE    mux_id;
    BYTE    sas_id;
    DWORD   ucmc_id;
    BYTE    message_id;
    BYTE    message_length; // total data block size
	BYTE	command_code[2];

};
#pragma pack()

#pragma pack(1)
struct CmsProtocolHeader
{
    BYTE	header;
	char	machine_id[8];
    BYTE    message_id;
    BYTE    message_length; // total data block size
	BYTE	command_code[2];

};
#pragma pack()

#pragma pack(1)
struct IdcMessageFormat
{
	IdcProtocolHeader header;
    char data[MAXIMUM_SERIAL_MESSAGE_SIZE];
	WORD data_length;
};
#pragma pack()

#pragma pack(1)
struct CmsMessageFormat
{
	CmsProtocolHeader header;
    char data[MAXIMUM_SERIAL_MESSAGE_SIZE];
	WORD data_length;
	bool critical_message;
};
#pragma pack()

#pragma pack(1)
struct IdcGbAdvantageWinMessage
{
	DWORD	player_account;
	DWORD	bonus_points;
	BYTE	print_drawing_ticket;
	DWORD	cash_voucher_amount;
	BYTE	scalable_award;
	BYTE	machine_mux_id;
	WORD	machine_denomination;
	WORD	credit_bet;
	WORD	promotion_id;
	BYTE	i_rewards_paid_win;
	BYTE	hand_data[5];
	DWORD	transaction_id;
	DWORD	cashable_credits;
	DWORD	noncashable_credits;
};
//#pragma pack()

struct IdcBonusAwardMessage
{
	DWORD	player_account;
	DWORD	bonus_points;
	BYTE	print_drawing_ticket;
	DWORD	cash_voucher_amount;
	WORD	promotion_id;
	BYTE	i_rewards_paid_win;
	DWORD	transaction_id;
	DWORD	cashable_credits;
	DWORD	noncashable_credits;
};

struct IdcGameLockMessage
{
    DWORD ticket_id;
    DWORD ticket_amount;
    DWORD marker_balance;
    DWORD player_account;
    BYTE  time_stamp[6];    // BCD: hour, minute, second, day, month, year
    BYTE  taxable_flag;
    BYTE  jackpot_to_credit_flag;
	char player_name[14];
};

struct IdcGameLockCompleteMessage
{
	IdcGameLockMessage game_lock;	
    DWORD   ss_number;
    double   drivers_license;
};

struct IdcGbaWinTextMessage
{
	char points_text_line_one[28];
	char points_text_line_two[28];
};

struct IdcExternalAwardedPointsMessage
{
    DWORD member_id;
	DWORD awarded_points;
	DWORD transaction_id;
	BYTE  event_type;
};

struct IdcGameEnteredMessage
{
    char paytable_id[6];
    char manufacturer_id[2];
};

struct IdcBillAcceptedMessage
{
	DWORD	denomination_in_cents;
	DWORD	transaction_id;
};

struct CmsLoginMessage
{
    DWORD account_number;
	DWORD tier_level;
	char first_name[30];
	char last_name[30];
	char nick_name[30];
	char email_address[50];
	char birthdate[8];
	BYTE gender;
};

struct IdcCmsLoginMessage
{
    DWORD account_number;
	DWORD tier_level;
	char first_name[30];
	char last_name[30];
	char nick_name[30];
	char email_address[50];
	char birthdate[8];
	BYTE gender;
	char machine_id[8];
};

struct BingoFrenzy
{
    DWORD amount;
    DWORD display_frequency;
    DWORD amount_per_display;
    DWORD current_4ofkind;
};

struct CashFrenzy
{
	DWORD	bingo_award;
	DWORD	current_4ofkind;
	char	first_string[28];
	char	second_string[28];
	BYTE	flash_text;
	BYTE	special_text_color;
	BYTE	special_background_color;
};

struct GameMessageErrors
{
    DWORD invalid_byte_count;
    DWORD header;
	DWORD crc;
    DWORD no_request;
    DWORD sequence;
    DWORD mux_id;
    DWORD no_response;
};

struct GameDataDisplayStruct
{
    DWORD   data_type;
    WORD    length;
    WORD    port_number;
    BYTE    data[0xFFFF];
};

struct DataDisplayStruct
{
    WORD port_number;
    bool dispenser_data_text;
    bool dispenser_data_raw;
};

struct AccountLogin
{
    DWORD account_number;
    char  name[16];
    WORD  rating;
    DWORD whole_points;
    WORD  fractional_points;
    WORD  session_fractional_points;
    WORD  bonus;
    DWORD ltd_points;
    WORD  pin_number;
    BYTE  status_code;
    BYTE  game_card_type;
    DWORD primary_card_handle;
    DWORD primary_card_games;
    DWORD primary_card_marks;
    DWORD secondary_card_handle;
    DWORD secondary_card_games;
    DWORD secondary_card_marks;
    char  tier_level;
    // new account login message variables
    BYTE  unused_byte;
    DWORD marker_limit;
    DWORD marker_owed;
};

#pragma pack()


#pragma pack(1)
struct GbInterfaceInitialize
{
    char  name[16];
    DWORD account_number;
    DWORD whole_points;
    DWORD primary_card_marks;
    DWORD primary_card_award;
    DWORD secondary_card_marks;
    DWORD secondary_card_award;
    WORD  pin_number;
	BYTE  club_code;
    char  manufacturer_id[2];
	BYTE  reserved;
};
#pragma pack()

#pragma pack(1)
struct ViewCardsInfo
{
    DWORD primary_card_handle;
    DWORD primary_card_games;
    DWORD secondary_card_handle;
    DWORD secondary_card_games;
    DWORD current_denomination;
};
#pragma pack()

struct MemoryTicketAdd
{
    DWORD ticket_id;
    DWORD ticket_amount;
    BYTE  time_stamp[6];
    DWORD player_account;
    BYTE  taxable_flag;
};

struct StratusMarkerCredit
{
    DWORD ticket_id;
    DWORD player_account;
    DWORD credit_amount;
    DWORD transaction_id;
};

struct StratusMarkerCreditResponse
{
    DWORD marker_balance;
    DWORD credit_amount;
};

struct StratusMarkerCreditPending
{
    DWORD player_account;
    DWORD credit_amount;
};

struct Jackpot2CreditTimeStamp
{
    char hour[2];
    char minute[2];
    char second[2];
    char day[2];
    char month[2];
    char year[4];
};

#pragma pack(1)
struct Jackpot2Credit
{
	DWORD ticket_id;
	DWORD ticket_amount;
	DWORD player_account;
	DWORD cashier_id;
	DWORD ss_number;
	char  drivers_license[15];
	Jackpot2CreditTimeStamp time_stamp;
};
#pragma pack()


struct DrinkCompTotal
{
    WORD    number;
    DWORD   total_amount;
};

struct MuxFileMessages
{
    DWORD index;
    char  string[256];
};

// filename MASTER_CONFIG
struct MasterSlaveConfig
{
    DWORD ip_address;
    BYTE  unused_space[100];
};

struct FilePendingTickets
{
    DWORD ticket_id;
	char computer_name[20]; // string identifying the computer initiating the pending ticket
    BYTE  unused_space[80];
};

struct DisplayGameLock
{
    DWORD ticket_id;
    DWORD ucmc_id;
    DWORD ticket_amount;
    DWORD marker_balance;
    WORD  game_port;
    BYTE  jackpot_to_credit_flag;
    BYTE  mux_id;
    BYTE  sas_id;
};

struct MonthDayYear
{
    BYTE month;
    BYTE day;
    WORD year;
};

// filename EMAIL_ADDRESS_LIST
struct FileEmailRecord
{
    char  email_address[MAXIMUM_EMAIL_ADDRESS_LENGTH];
    DWORD email_flags;          // bit flags for sending email messages
    BYTE  unused_space[200];
};

struct NetworkSettingsRecord
{
	BYTE network_type;	// 0=LAN, 1=Lease Line, 2=Dial Up
};

// filename NETWORK_SETTINGS
struct FileNetworkSettingsRecord
{
	NetworkSettingsRecord	settings;
	BYTE					unused_space[500];
};

struct FileCashierShift
{
    DWORD           cashier_id;
    time_t          start_time;
    time_t          end_time;
    int             beginning_balance;
    int             ending_balance;
    DWORD           previous_total_drop;
    DWORD           game_drop;
    DWORD   		previous_game_handle;
    DWORD   		game_handle;
    DWORD           redeem_amount;
    DWORD           pre_shift_fills;
    DWORD           active_shift_fills;
    DWORD           mgr_adjust_plus;
    DWORD           mgr_adjust_minus;
    DWORD           number_tickets;
    DWORD           number_nosale;
    DrinkCompTotal  drink_comps;
    DrinkCompTotal  previous_drink_comps;
    BYTE            print_offline_disclaimer;
    BYTE            reserved[1];
    DWORD           manager_awarded_points;
    BYTE            unused_space[194];
};

struct FileShiftLog
{
    DWORD  cashier_id;
    time_t end_time;
    DWORD  redeem_amount;
    BYTE   unused_space[50];
};

struct FileCashAdjustment
{
    DWORD   user_id;
    BYTE    type;
    DWORD   amount;
    time_t  time_stamp;
    BYTE    unused_space[100];
};

struct MemoryGbProManagerSettings
{
    DWORD   million_dollar_ticket_server_ip_address;
    WORD    million_dollar_ticket_server_tcp_port;
    DWORD   million_dollar_ticket_printer_ip_address;
    WORD    million_dollar_ticket_printer_port;
    BYTE    stop_printing_million_dollar_tickets;
    DWORD   cash_voucher_printer_ip_address;
    WORD    cash_voucher_printer_port;
    DWORD   gb_pro_media_box_server_ip_address;
    WORD    gb_pro_media_box_server_tcp_port;
    DWORD   gb_pro_configurator_ip_address;
    WORD    gb_pro_configurator_tcp_port;
};

// filename GB_PRO_SETTINGS_INI
struct FileGbProManagerSettings
{
    MemoryGbProManagerSettings  memory;
    BYTE                        unused_space[200];
};

struct MemoryGbProManagerPoints
{
    DWORD   player_account;
    DWORD   gb_points;
    DWORD   system_user_id;
    time_t  time_stamp;
};

struct FileGbProManagerPoints
{
    MemoryGbProManagerPoints    memory;
    BYTE                        unused_space[100];
};

struct MemoryGbProManagerTickets
{
    DWORD   system_user_id;
    time_t  time_stamp;
};

struct FileGbProManagerTickets
{
    MemoryGbProManagerTickets   memory;
    BYTE                        unused_space[100];
};

struct FillValues
{
    DWORD fill_amount;
    DWORD drop_amount;
	DWORD impress_bill_amount_100;
	DWORD impress_bill_amount_20;
    BYTE  type;
    BYTE  message_revision; // starting with revision 'A'
    WORD  reserved_word;
    DWORD transaction_id;
};

struct IdcFillValues
{
	DWORD impress_bill_amount_100;
	DWORD impress_bill_amount_20;
	DWORD total_100s_added;
	DWORD total_20s_added;
};

struct DispenseInfo
{
	DWORD ticket_id;
	DWORD dispense_amount;
	DWORD login_id;
	DWORD dispense_event_category;
};

struct W2gInfo
{
	DWORD ticket_id;
	DWORD ticket_amount;
	DWORD ss_number;
	double drivers_license;
};

struct ShiftEndStratusData
{
    DWORD cashier_id;
    DWORD start_time;
    DWORD end_time;
    DWORD breakage;
    DWORD game_drop;
    DWORD game_handle;
    DWORD paid_out;
    DWORD number_of_tickets;
};

struct CashierLogonLogoffStratusData
{
    DWORD cashier_id;
    DWORD log_on;		// 1 = logon, 0 = logoff
};

struct 	ManualDispenseData
{
    DWORD dispense_amount;
	int year;
	int month;
	int day;
};

struct NetwinGame
{
    WORD  report_sequence;
    WORD  machine_count;
    DWORD ucmc_id;
    DWORD drop;
    DWORD payout;
    DWORD handle;
    DWORD wat_in;
    DWORD wat_out;
};

struct TicketDataFromStratus
{
    DWORD amount;
    DWORD ucmc_id;
    BYTE  status;
    BYTE  taxable;
};

struct GameUnlockQueue
{
    DWORD ticket_id;
    DWORD ucmc_id;
    DWORD ticket_amount;
    DWORD marker_amount;
    BYTE  jackpot_to_credit_flag;
    BYTE  manager_override;
};

struct GbProConfiguratorServerHeader
{
	BYTE	header;
	BYTE 	message_id;
	BYTE	length;
	BYTE	command;
	DWORD	property_id;
};

struct GbProConfiguratorServerMessage
{
	BYTE	header;
	BYTE 	message_id;
	BYTE	length;
	BYTE	command;
	DWORD	customer_id;
    BYTE    data[MAXIMUM_GBPRO_CONFIGURATOR_SERVER_MSG_SIZE];
};

struct OneArmBanditHostMessage
{
    BYTE    data[MAXIMUM_GBPRO_CONFIGURATOR_SERVER_MSG_SIZE];
};

typedef struct
{
	BYTE header;
	BYTE message_id;
	BYTE message_length;
	BYTE message_command;
	DWORD customer_id;
	DWORD ucmc_id;
	char game_number[2];
	char game_id[2];
	char additional_id[3];
	BYTE denomination;
	BYTE max_bet;
	BYTE progressive_group;
	WORD game_options;
	char paytable[6];
	char base_percentage[4];
	BYTE crc;
}OneArmBanditGameConfigurationMessage;

typedef struct
{
//	unsigned char address;
//	unsigned char command;
//	BCD game_number[2];
	char game_number[2];
	char game_id[2];
	char additional_id[3];
	unsigned char denomination;
	unsigned char max_bet;
	unsigned char progressive_group;
	WORD game_options;
	char paytable[6];
	char base_percentage[4];
}SasGameConfigurationMessage;


struct GbProMediaBoxServerMessage
{
    char    message[MAXIMUM_GBPRO_MEDIA_BOX_SERVER_MSG_SIZE];
};

struct CashVoucherTickets
{
    char	player_name[PLAYER_INFO_NAME_SIZE];
    DWORD	player_account;
    DWORD	cash_voucher_amount;  // in pennies
    DWORD	game_ucmc_id;
    BYTE	game_mux_id;
    WORD	game_denomination;
    WORD	credits_wagered;
    WORD	promotion_id;
    time_t	time_stamp;
    bool	suppress_printing_on_lat;
	BYTE	print_drawing_ticket;
	char	promo_string [MAX_MEDIA_STRING_LENGTH];
};

struct GbProPointsAndTickets
{
    DWORD   player_account;
    DWORD   bonus_points;
    BYTE    print_drawing_ticket;
    BYTE    game_mux_id;
    WORD    promotion_id;
};

struct GbProPointsAndTicketsFile
{
    GbProPointsAndTickets   memory;
    DWORD                   ucmc_id;
    time_t                  time_stamp;
    BYTE                    unused_space[100];
};

#pragma pack(1)
struct GbProPointsTicketsCash
{
	DWORD	player_account;
	DWORD	bonus_points;
	BYTE	print_drawing_ticket;
	DWORD	cash_voucher_amount;
	BYTE	scalable_award;
	BYTE	machine_mux_id;
	WORD	machine_denomination;
	WORD	credit_bet;
	WORD	promotion_id;
//	BYTE	i_rewards_paid_win;
	char	i_rewards_paid_win;
	BYTE	hand_data[5];
//	char	promo_string [MAX_MEDIA_STRING_LENGTH];
	DWORD	transaction_id;
	DWORD	cashable_credits;
	DWORD	noncashable_credits;
	char	promo_string [MAX_MEDIA_STRING_LENGTH];
};
#pragma pack()

#pragma pack(1)
struct StratusGbProPointsTicketsCash
{
    DWORD account_number;
    DWORD account_points;
    WORD  i_rewards_paid_win;
    BYTE  new_promotion_id[4];
    BYTE  card_hand_data[10];
    DWORD cash_voucher_amount;
    DWORD game_credits_played;
    DWORD game_denomination;

	DWORD	transaction_id;
	DWORD	cashable_credits;
	DWORD	noncashable_credits;
};
#pragma pack()

struct iGBALinkListWrapper
{
	DWORD transaction_id;
	iGBALinkListWrapper* next;
	iGBALinkListWrapper* last;
};

struct PrinterMessage
{
    int  length;
    char data[1000];
};

struct FileBackup //????????????work
{
    struct
    {
        DWORD   file_size;
        DWORD   file_crc;
        size_t  filename_size;
        WORD    file_data_size;
    } head;

    char filename[MAXIMUM_FILE_PATH_NAME];
    BYTE file_data[MAXIMUM_FILE_BACKUP_DATA_SIZE];
};

struct PrinterReset
{
	bool	flag_sent;
	time_t	time_sent;
};

struct SerialDeviceReceive
{
	WORD index;
	BYTE data[MAXIMUM_SERIAL_MESSAGE_SIZE];
};

// filename GB_PRO_ASCII_MESSAGES_FILE
struct GbProAsciiMachineMessagesFile
{
	char points_text_line_one[29];
	char points_text_line_two[29];
	char cash_text_line_one[29];
	char cash_text_line_two[29];
	char unused_space[200];
};

struct ReceiveIpBuffer
{
	WORD index;
	BYTE data[TCPIP_MESSAGE_BUFFER_SIZE * 4];
};

struct TicketsByUcmcId
{
    DWORD ucmc_id;
    WORD  number_of_tickets;
    DWORD redeem_amount;
    DWORD marker_amount;
    DWORD pay_to_credit_amount;
};

typedef struct
{
    DWORD ucmc_id;
	DWORD game_drop;
    BYTE  mux_id;
	DWORD wat_in;
	DWORD wat_out;
} BagAndTagInfo;

#pragma pack()

#define MAX_BLOCKS_PER_MESSAGE      5

#pragma pack(1)
struct NewBlock
{
	BYTE block_id;
	WORD bet;
	WORD denomination;
	BYTE card1;
	BYTE card2;
	BYTE card3;
	BYTE card4;
	BYTE card5;
    BYTE reserved;
};

struct NewCardDataMessage
{
    BYTE number_of_blocks;
    BYTE block_length;
    NewBlock blocks[MAX_BLOCKS_PER_MESSAGE];
};

struct OldBlock
{
	BYTE block_id;
	BYTE bet;
	WORD denomination;
	BYTE card1;
	BYTE card2;
	BYTE card3;
	BYTE card4;
	BYTE card5;
    BYTE reserved;
};

struct OldCardDataMessage
{
    BYTE number_of_blocks;
    BYTE block_length;
    OldBlock blocks[MAX_BLOCKS_PER_MESSAGE];
};

#pragma pack()

#pragma pack(1)
struct MachineIdentifiers
{
    BYTE  mux_id;
	BYTE  sas_id;
    DWORD ucmc_id;
};
 #pragma pack()

#pragma pack(1)
struct ManualDispenseMessageData
{
	DWORD ticket_id;
	DWORD amount;
	DWORD authorization_id;
	DWORD idc_dispense_event;
	DWORD dispense_event_category;
	DWORD todays_dispense_amount;
	DWORD year;
	DWORD month;
	DWORD day;
	DWORD daily_limit;
	DWORD transaction_limit;
};
 #pragma pack()

//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by xp_controller.rc
//
#define IDD_LOGON_DIALOG                102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDD_MESSAGE_BOX_DIALOG          104
#define IDD_MAIN_MENU_DIALOG            106
#define IDD_INITIAL_INFO_DIALOG         107
#define IDD_CASHIER_MENU_DIALOG         108
#define IDD_MANAGER_MENU_DIALOG         109
#define IDD_COLLECTOR_MENU_DIALOG       110
#define IDD_TECHNICIAN_MENU_DIALOG      111
#define IDD_ENGINEER_MENU_DIALOG        112
#define IDD_COLLECTOR_IMPRESS_DIALOG    113
#define IDD_SELECT_CUSTOMER_ID_DIALOG   114
#define IDD_TIMER_BAR_DIALOG            115
#define IDD_GAMELOCK_DIALOG             116
#define IDD_CASHIER_REPORTS_DIALOG      119
#define IDD_CASHIER_DRAWER_DENOM_DIALOG 120
#define IDD_CASHIER_LICENSE_SSN_DIALOG  121
#define IDD_MANAGER_REPORTS_DIALOG      122
#define IDD_MANAGER_SETUP_DIALOG        123
#define IDD_MANAGER_VERSION_INFO_DIALOG 124
#define IDD_TECHNICIAN_SYSTEM_USERS_DIALOG 125
#define IDD_CASHIER_DRAWER_START_SHIFT_DENOM_COUNT_DIALOG 126
#define IDD_TECHNICIAN_DISPLAY_DIALOG   127
#define IDR_MAINFRAME                   128
#define IDD_MANAGER_GB_PRO_DIALOG       128
#define IDD_CASHIER_GB_PRO_DIALOG       129
#define IDD_MANAGER_GB_PRO_REPORTS_DIALOG 130
#define IDD_TECHNICIAN_DISPLAY_GAME_INFO_DIALOG 131
#define IDD_TECHNICIAN_GB_PRO_DIALOG    132
#define IDD_TECHNICIAN_MASTER_SLAVE_DIALOG 133
#define IDD_TECHNICIAN_EMAIL_ALERTS_DIALOG 134
#define IDD_NETWORK_SETUP_DIALOG        135
#define IDD_TECHNICIAN_EMAIL_FLAGS_DIALOG 136
#define IDD_DISPENSER_INFO_DIALOG       137
#define IDD_FORCE_COLLECTION_DIALOG     138
#define IDD_PRAC_CONFIG_DIALOG          139
#define IDD_CASH_VOUCHER_DIALOG         140
#define IDD_GB_PRO_MACHINE_ASCII_MSGS_DIALOG 141
#define IDD_RAS_STATUS_DIALOG           142
#define IDD_TECHNICIAN_GB_ADVANTAGE_DIALOG 143
#define IDD_MANAGER_AWARD_POINTS_DIALOG 144
#define IDD_TECHNICIAN_DISPENSE_DIALOG  145
#define IDD_CASHOUT_TICKET_DIALOG       146
#define IDD_MESSAGE_BOX_DIALOG1         147
#define IDD_TICKET_OR_DISPENSE_MESSAGE_BOX_DIALOG 147
#define IDB_BITMAP1                     168
#define IDB_BITMAP2                     169
#define IDB_PASSWORD                    257
#define IDB_USERID                      258
#define IDB_BITMAP3                     259
#define IDB_UCMC_ID_TEXT                260
#define IDB_UNLOCK_GAME                 261
#define IDB_AWARD_POINTS                262
#define IDB_CASHIER_SHIFTS              263
#define IDB_MEMBERNUM                   264
#define IDB_BITMAP5                     285
#define IDC_ONE_KEYPAD_BUTTON           1002
#define IDC_TWO_KEYPAD_BUTTON           1003
#define IDC_THREE_KEYPAD_BUTTON         1004
#define IDC_FOUR_KEYPAD_BUTTON          1005
#define IDC_FIVE_KEYPAD_BUTTON          1006
#define IDC_SIX_KEYPAD_BUTTON           1007
#define IDC_SEVEN_KEYPAD_BUTTON         1008
#define IDC_EIGHT_KEYPAD_BUTTON         1009
#define IDC_NINE_KEYPAD_BUTTON          1010
#define IDC_ZERO_KEYPAD_BUTTON          1011
#define IDC_COLON_KEYPAD_BUTTON         1012
#define IDC_PERIOD_KEYPAD_BUTTON        1013
#define IDC_OK_KEYPAD_BUTTON            1014
#define IDC_CLEAR_KEYPAD_BUTTON         1016
#define IDC_DISPENSER_TYPE_COMBO        1017
#define IDC_DISPENSER_COMM_EDIT         1018
#define IDC_PRINTER_COMM_EDIT           1020
#define IDC_DISPENSER_COMM_STATIC       1021
#define IDC_PRINTER_COMM_STATIC         1022
#define IDC_CANCEL_KEYPAD_BUTTON        1023
#define IDC_IP_ADDRESS_KEYPAD_EDIT      1024
#define IDC_IP_ADDRESS_KEYPAD_STATIC    1025
#define IDC_KEYPAD1_BUTTON              1027
#define IDC_TWO_LOGON_BUTTON            1028
#define IDC_KEYPAD2_BUTTON              1028
#define IDC_THREE_LOGON_BUTTON          1029
#define IDC_KEYPAD3_BUTTON              1029
#define IDC_FOUR_LOGON_BUTTON           1030
#define IDC_KEYPAD4_BUTTON              1030
#define IDC_FIVE_LOGON_BUTTON           1031
#define IDC_KEYPAD5_BUTTON              1031
#define IDC_SIX_LOGON_BUTTON            1032
#define IDC_KEYPAD6_BUTTON              1032
#define IDC_SEVEN_LOGON_BUTTON          1033
#define IDC_KEYPAD7_BUTTON              1033
#define IDC_EIGHT_LOGON_BUTTON          1034
#define IDC_KEYPAD8_BUTTON              1034
#define IDC_NINE_LOGON_BUTTON           1035
#define IDC_KEYPAD9_BUTTON              1035
#define IDC_ZERO_LOGON_BUTTON           1036
#define IDC_KEYPAD0_BUTTON              1036
#define IDC_CLEAR_LOGON_BUTTON          1037
#define IDC_KEYPADC_BUTTON              1037
#define IDC_USER_ID_LOGON_EDIT          1038
#define IDC_USER_PASSWORD_LOGON_EDIT    1039
#define IDC_USER_ID_LOGON_STATIC        1040
#define IDC_USER_PASSWORD_LOGON_STATIC  1041
#define IDC_CASHIER_MENU_BUTTON         1043
#define IDC_CASHIER_BUTTON              1043
#define IDC_MANAGER_MENU_BUTTON         1044
#define IDC_COLLECTOR_MENU_BUTTON       1045
#define IDC_TECHNICIAN_MENU_BUTTON      1046
#define IDC_ENGINEER_MENU_BUTTON        1047
#define IDC_LOGOUT_MENU_BUTTON          1048
#define IDC_BUTTON1                     1052
#define IDC_EXIT_CASHIER_BUTTON         1052
#define IDC_EXIT_MANAGER_BUTTON         1052
#define IDC_EXIT_COLLECTOR_BUTTON       1052
#define IDC_EXIT_TECHNICIAN_BUTTON      1052
#define IDC_EXIT_ENGINEER_BUTTON        1052
#define IDC_MESSAGE_BOX_OK_BUTTON       1052
#define IDC_MESSAGE_DIALOG_STATIC       1053
#define IDC_MESSAGE_DIALOG_STATIC2      1057
#define IDC_MESSAGE_DIALOG_STATIC3      1058
#define IDC_MESSAGE_DIALOG_STATIC4      1059
#define IDC_COLLECTOR_IMPRESS_BUTTON    1060
#define IDC_COLLECTOR_DISPENSER_FILL_BUTTON 1061
#define IDC_COLLECTOR_GAME_DROP_BUTTON  1062
#define IDC_COLLECTOR_IMPRESS_100_EDIT  1063
#define IDC_COLLECTOR_IMPRESS_20_EDIT   1064
#define IDC_BUTTON2                     1065
#define IDC_BUTTON3                     1066
#define IDC_BUTTON4                     1067
#define IDC_BUTTON5                     1068
#define IDC_BUTTON6                     1069
#define IDC_BUTTON7                     1070
#define IDC_BUTTON8                     1071
#define IDC_BUTTON9                     1072
#define IDC_BUTTON10                    1073
#define IDC_BUTTON11                    1074
#define IDC_BUTTON12                    1075
#define IDC_COLLECTOR_100_STATIC        1076
#define IDC_COLLECTOR_20_STATIC         1078
#define IDC_BUTTON13                    1079
#define IDC_RICHEDIT21                  1080
#define IDC_MESSAGE_BOX_CANCEL_BUTTON   1081
#define IDC_SELECT_CUSTOMER_ID_BUTTON14 1082
#define IDC_SELECT_CUSTOMER_ID_BUTTON15 1083
#define IDC_SELECT_CUSTOMER_ID_BUTTON16 1084
#define IDC_SELECT_CUSTOMER_ID_BUTTON17 1085
#define IDC_SELECT_CUSTOMER_ID_BUTTON18 1086
#define IDC_SELECT_CUSTOMER_ID_BUTTON1  1087
#define IDC_SELECT_CUSTOMER_ID_BUTTON2  1088
#define IDC_SELECT_CUSTOMER_ID_BUTTON3  1089
#define IDC_SELECT_CUSTOMER_ID_BUTTON4  1090
#define IDC_SELECT_CUSTOMER_ID_BUTTON5  1091
#define IDC_SELECT_CUSTOMER_ID_BUTTON6  1092
#define IDC_SELECT_CUSTOMER_ID_BUTTON7  1093
#define IDC_SELECT_CUSTOMER_ID_BUTTON8  1094
#define IDC_SELECT_CUSTOMER_ID_BUTTON9  1095
#define IDC_SELECT_CUSTOMER_ID_BUTTON10 1096
#define IDC_SELECT_CUSTOMER_ID_BUTTON11 1097
#define IDC_SELECT_CUSTOMER_ID_BUTTON12 1098
#define IDC_SELECT_CUSTOMER_ID_BUTTON13 1099
#define IDC_PROGRESS1                   1100
#define IDC_TIMER_WINDOW_PROGRESS       1100
#define IDC_CASHIER_PAY_BUTTON          1101
#define IDC_CASHIER_REPORTS_BUTTON      1104
#define IDC_CASHIER_SHIFT_END_BUTTON    1105
#define IDC_CASHIER_OPEN_DRAWER_BUTTON  1106
#define IDC_GAMELOCK_BUTTON1            1107
#define IDC_CASHIER_QUICKENROLL_BUTTON  1107
#define IDC_GAMELOCK_BUTTON2            1108
#define IDC_GAMELOCK_BUTTON3            1109
#define IDC_GAMELOCK_BUTTON4            1110
#define IDC_CASHIER_TICKET_BUTTON1      1115
#define IDC_CASHIER_TICKET_BUTTON2      1116
#define IDC_CASHIER_TICKET_BUTTON3      1117
#define IDC_CASHIER_TICKET_BUTTON4      1118
#define IDC_CASHIER_TICKET_BUTTON5      1119
#define IDC_CASHIER_TICKET_BUTTON6      1120
#define IDC_CASHIER_TICKET_BUTTON7      1121
#define IDC_CASHIER_TICKET_BUTTON8      1122
#define IDC_CASHIER_TICKET_BUTTON9      1123
#define IDC_CASHIER_TICKET_BUTTON0      1124
#define IDC_CASHIER_TICKET_ENTER_BUTTON 1125
#define IDC_CASHIER_TICKET_CLEAR_BUTTON 1126
#define IDC_CASHIER_REDEEM_TICKET_EDIT  1127
#define IDC_CASHIER_DESTROYED_UCMC_ID_EDIT 1128
#define IDC_CASHIER_TICKET_BUTTON10     1128
#define IDC_CASHIER_DESTROYED_TICKET_AMOUNT_EDIT 1129
#define IDC_CASHIER_DESTROY_BUTTON1     1130
#define IDC_CASHIER_DESTROY_BUTTON2     1131
#define IDC_CASHIER_DESTROY_BUTTON3     1132
#define IDC_CASHIER_DESTROY_BUTTON4     1133
#define IDC_CASHIER_DESTROY_BUTTON5     1134
#define IDC_CASHIER_DESTROY_BUTTON6     1135
#define IDC_CASHIER_DESTROY_BUTTON7     1136
#define IDC_CASHIER_DESTROY_BUTTON8     1137
#define IDC_CASHIER_DESTROY_BUTTON9     1138
#define IDC_CASHIER_DESTROY_BUTTON0     1139
#define IDC_CASHIER_DESTROY_ENTER_BUTTON 1140
#define IDC_CASHIER_DESTROY_CLEAR_BUTTON 1141
#define IDC_CASHIER_REPORTS_DISPENSER_BUTTON 1142
#define IDC_CASHIER_REPORTS_TICKETS_BUTTON 1143
#define IDC_DRAWER_DENOMINATION_PENNY_EDIT 1145
#define IDC_DRAWER_DENOMINATION_NICKEL_EDIT 1146
#define IDC_DRAWER_DENOMINATION_DIME_EDIT 1147
#define IDC_DRAWER_DENOMINATION_QUARTER_EDIT 1148
#define IDC_DRAWER_DENOMINATION_HALF_EDIT 1149
#define IDC_DRAWER_DENOMINATION_DOLLAR_EDIT 1150
#define IDC_DRAWER_DENOMINATION_HARD_OTHER_EDIT 1151
#define IDC_DRAWER_DENOMINATION_ONE_EDIT 1152
#define IDC_DRAWER_DENOMINATION_FIVE_EDIT 1153
#define IDC_DRAWER_DENOMINATION_TEN_EDIT 1154
#define IDC_DRAWER_DENOMINATION_TWENTY_EDIT 1155
#define IDC_DRAWER_DENOMINATION_FIFTY_EDIT 1156
#define IDC_DRAWER_DENOMINATION_HUNDRED_EDIT 1157
#define IDC_DRAWER_DENOMINATION_SOFT_OTHER_EDIT 1158
#define IDC_DRAWER_TOTAL_AMOUNT_EDIT    1159
#define IDC_DRAWER_DENOMINATION_BUTTON0 1160
#define IDC_DRAWER_DENOMINATION_ENTER_BUTTON 1161
#define IDC_DRAWER_DENOMINATION_CANCEL_BUTTON 1162
#define IDC_DRAWER_DENOMINATION_BUTTON1 1163
#define IDC_DRAWER_DENOMINATION_BUTTON2 1164
#define IDC_DRAWER_DENOMINATION_BUTTON3 1165
#define IDC_DRAWER_DENOMINATION_BUTTON4 1166
#define IDC_DRAWER_DENOMINATION_BUTTON5 1167
#define IDC_DRAWER_DENOMINATION_BUTTON6 1168
#define IDC_DRAWER_DENOMINATION_BUTTON7 1169
#define IDC_DRAWER_DENOMINATION_BUTTON8 1170
#define IDC_DRAWER_DENOMINATION_BUTTON9 1171
#define IDC_DRAWER_DENOMINATION_CLEAR_BUTTON 1172
#define IDC_CASHIER_SSN_EDIT            1173
#define IDC_CASHIER_DL_EDIT             1174
#define IDC_CASHIER_DL_SSN_BUTTON1      1176
#define IDC_CASHIER_DL_SSN_BUTTON2      1177
#define IDC_CASHIER_DL_SSN_BUTTON3      1178
#define IDC_CASHIER_DL_SSN_BUTTON4      1179
#define IDC_CASHIER_DL_SSN_BUTTON5      1180
#define IDC_CASHIER_DL_SSN_BUTTON6      1181
#define IDC_CASHIER_DL_SSN_BUTTON7      1182
#define IDC_CASHIER_DL_SSN_BUTTON8      1183
#define IDC_CASHIER_DL_SSN_BUTTON9      1184
#define IDC_CASHIER_DL_SSN_BUTTON0      1185
#define IDC_CASHIER_DL_SSN_ENTER_BUTTON 1186
#define IDC_CASHIER_DL_SSN_CLEAR_BUTTON 1187
#define IDC_CASHIER_SSN_DL_MANAGER_USER_ID_EDIT 1188
#define IDC_CASHIER_SSN_DL_MANAGER_PASSWORD_EDIT 1189
#define IDC_CASHIER_SSN_DL_STATIC1      1190
#define IDC_CASHIER_SSN_DL_STATIC2      1191
#define IDC_CASHIER_SSN_DL_STATIC3      1192
#define IDC_MANAGER_REPORTS_BUTTON      1193
#define IDC_MANAGER_SETUP_BUTTON        1194
#define IDC_MANAGER_VERSION_INFO_BUTTON 1195
#define IDC_MANAGER_UNLOCK_UCMC_ID_EDIT 1196
#define IDC_MANAGER_UNLOCK_GAME_BUTTON  1197
#define IDC_MANAGER_TILL_AMOUNT_EDIT    1198
#define IDC_MANAGER_MEMBER_NUMBER_EDIT  1198
#define IDC_MANAGER_ADD_FUNDS_BUTTON    1199
#define IDC_MANAGER_5000_POINTS_BUTTON  1199
#define IDC_MANAGER_REMOVE_FUNDS_BUTTON 1200
#define IDC_MANAGER_10000_POINTS_BUTTON 1200
#define IDC_MANAGER_MENU_BUTTON0        1201
#define IDC_MANAGER_MENU_CLEAR_BUTTON   1202
#define IDC_MANAGER_MENU_BUTTON1        1203
#define IDC_MANAGER_MENU_BUTTON2        1204
#define IDC_MANAGER_MENU_BUTTON3        1205
#define IDC_MANAGER_MENU_BUTTON4        1206
#define IDC_MANAGER_MENU_BUTTON5        1207
#define IDC_MANAGER_MENU_BUTTON6        1208
#define IDC_MANAGER_MENU_BUTTON7        1209
#define IDC_MANAGER_MENU_BUTTON8        1210
#define IDC_MANAGER_MENU_BUTTON9        1211
#define IDC_MANAGER_TILL_ADJUST_STATIC  1212
#define IDC_MEMBER_ID_STATIC            1212
#define IDC_MANAGER_TILL_ADJUST_GROUP_STATIC 1213
#define IDC_MANAGER_POINTS_AWARD_GROUP_STATIC 1213
#define IDC_MANAGER_UNLOCK_GAME_GROUP   1214
#define IDC_MANAGER_UNLOCK_GAME_STATIC  1215
#define IDC_MANAGER_REPORT_TICKETS_BUTTON 1216
#define IDC_MANAGER_25000_POINTS_BUTTON 1216
#define IDC_MANAGER_REPORT_DISPENSER_BUTTON 1217
#define IDC_MANAGER_REPORT_NETWIN_BUTTON 1218
#define IDC_MANAGER_REPORT_ADJUSTMENTS_BUTTON 1219
#define IDC_MANAGER_REPORT_W2G_BUTTON   1220
#define IDC_MANAGER_REPORT_MACHINES_BUTTON 1221
#define IDC_MANAGER_REPORT_CASHIER_IDS_BUTTON 1222
#define IDC_MANAGER_REPORT_REDEEM_BUTTON 1223
#define IDC_MANAGER_REPORT_SHIFT_GROUPBOX 1224
#define IDC_MANAGER_REPORT_CASHIER_ID_STATIC 1226
#define IDC_MANAGER_REPORT_CASHIER_SHIFT_ALL_BUTTON 1227
#define IDC_MANAGER_REPORT_CASHIER_SHIFT_4_BUTTON 1227
#define IDC_MANAGER_REPORT_CASHIER_SHIFT_CURRENT_BUTTON 1228
#define IDC_COMBO1                      1229
#define IDC_MANAGER_SOUND_FOR_GAMELOCKS_TEST_BUTTON 1232
#define IDC_MANAGER_PLAYER_TIER_CHECK   1233
#define IDC_MANAGER_PRINT_DRINK_COMPS_CHECK 1234
#define IDC_MANAGER_FORCE_CASHIER_SHIFTS_CHECK 1235
#define IDC_MANAGER_SETUP_EXIT_BUTTON   1236
#define IDC_MANAGER_REPORTS_EXIT_BUTTON 1237
#define IDC_MANAGER_SETUP_CASHIER_SHIFT_PRINTING_CHECK 1238
#define IDC_VERSION_INFORMATION_LIST    1239
#define IDC_TECHNICIAN_MENU_USERS_BUTTON 1240
#define IDC_CASHIER_REPORTS_EXIT_BUTTON 1242
#define IDC_TECHNICIAN_SYSTEM_USERS_EXIT_BUTTON 1243
#define IDC_TECHNICIAN_SYSTEM_USER_ID_EDIT 1244
#define IDC_TECHNICIAN_SYSTEM_USER_PASSWORD_EDIT 1245
#define IDC_TECHNICIAN_SYSTEM_USER_ID_STATIC 1246
#define IDC_TECHNICIAN_SYSTEM_USER_PASSWORD_STATIC 1247
#define IDC_TECHNICIAN_SYSTEM_USER_FIRST_NAME_EDIT 1248
#define IDC_TECHNICIAN_SYSTEM_USER_LAST_NAME_EDIT 1249
#define IDC_TECHNICIAN_SYSTEM_USER_FIRST_NAME_STATIC 1250
#define IDC_TECHNICIAN_SYSTEM_USER_LAST_NAME_STATIC 1251
#define IDC_TECHNICIAN_SYSTEM_USER_ADD_BUTTON 1252
#define IDC_TECHNICIAN_DISPENSE_AMOUNT_EDIT 1252
#define IDC_TECHNICIAN_SYSTEM_USER_REMOVE_BUTTON 1253
#define IDC_TECHNICIAN_SYSTEM_USER_BUTTON1 1254
#define IDC_TECHNICIAN_SYSTEM_USER_BUTTON2 1255
#define IDC_TECHNICIAN_SYSTEM_USER_BUTTON3 1256
#define IDC_TECHNICIAN_SYSTEM_USER_BUTTON4 1257
#define IDC_TECHNICIAN_SYSTEM_USER_BUTTON5 1258
#define IDC_TECHNICIAN_SYSTEM_USER_BUTTON6 1259
#define IDC_TECHNICIAN_SYSTEM_USER_BUTTON7 1260
#define IDC_TECHNICIAN_SYSTEM_USER_BUTTON8 1261
#define IDC_TECHNICIAN_SYSTEM_USER_BUTTON9 1262
#define IDC_TECHNICIAN_SYSTEM_USER_BUTTON0 1263
#define IDC_TECHNICIAN_SYSTEM_USER_CLEAR_BUTTON 1264
#define IDC_TECHNICIAN_SYSTEM_USER_PRINT_BUTTON 1265
#define IDC_ENGINEER_AUTOLOGOUT_EDIT    1266
#define IDC_ENGINEER_AUTOLOGOUT_BUTTON  1267
#define IDC_ENGINEER_DIALOG_BUTTON0     1273
#define IDC_ENGINEER_DIALOG_BUTTON1     1275
#define IDC_ENGINEER_DIALOG_BUTTON2     1276
#define IDC_ENGINEER_DIALOG_BUTTON3     1277
#define IDC_ENGINEER_DIALOG_BUTTON4     1278
#define IDC_ENGINEER_DIALOG_BUTTON5     1279
#define IDC_ENGINEER_DIALOG_BUTTON6     1280
#define IDC_ENGINEER_DIALOG_BUTTON7     1281
#define IDC_ENGINEER_DIALOG_BUTTON8     1282
#define IDC_ENGINEER_DIALOG_BUTTON9     1283
#define IDC_ENGINEER_DIALOG_PERIOD_BUTTON 1284
#define IDC_ENGINEER_DIALOG_CLEAR_BUTTON 1286
#define IDC_ENGINEER_TIMEOUTS_COMBO     1287
#define IDC_ENGINEER_GAME_LOCK_PRINTER_ADDRESS_EDIT 1288
#define IDC_ENGINEER_GAME_LOCK_PRINTER_MODIFY_BUTTON 1289
#define IDC_CASHIER_DRAWER_BALANCE_EDIT 1290
#define IDC_CASHIER_DRAWER_BALANCE_BUTTON1 1291
#define IDC_CASHIER_DRAWER_BALANCE_BUTTON2 1292
#define IDC_CASHIER_DRAWER_BALANCE_BUTTON3 1293
#define IDC_CASHIER_DRAWER_BALANCE_BUTTON4 1294
#define IDC_CASHIER_DRAWER_BALANCE_BUTTON5 1295
#define IDC_CASHIER_DRAWER_BALANCE_BUTTON6 1296
#define IDC_CASHIER_DRAWER_BALANCE_BUTTON7 1297
#define IDC_CASHIER_DRAWER_BALANCE_BUTTON8 1298
#define IDC_CASHIER_DRAWER_BALANCE_BUTTON9 1299
#define IDC_CASHIER_DRAWER_BALANCE_BUTTON0 1300
#define IDC_CASHIER_DRAWER_BALANCE_CLEAR_BUTTON 1301
#define IDC_CASHIER_DRAWER_BALANCE_ENTER_BUTTON 1302
#define IDC_TECHNICIAN_DISPLAY_LIST     1303
#define IDC_TECHNICIAN_TX_DISPLAY_LIST  1303
#define IDC_TECHNICIAN_DISPLAY_EXIT_BUTTON 1304
#define IDC_TECHNICAN_DISPLAY_CHECK     1306
#define IDC_TECHNICIAN_MENU_GAME_DATA_BUTTON 1307
#define IDC_TECHNICIAN_DISPLAY_GAME_INFO_BUTTON 1309
#define IDC_CHECK3                      1311
#define IDC_DISPENSER_AUTOPAY_MANAGER_APPROVAL_CHECK 1311
#define IDC_MANAGER_GB_PRO_BUTTON       1313
#define IDC_MANAGER_GB_PRO_EXIT_BUTTON  1314
#define IDC_GB_PRO_GENERATE_MILLION_DOLLAR_TICKET_BUTTON 1315
#define IDC_GB_PRO_PLAYER_ACCOUNT_EDIT  1316
#define IDC_GB_PRO_PLAYER_POINTS_BUTTON 1317
#define IDC_MANAGER_GB_PRO_ACCOUNT_BUTTON1 1319
#define IDC_MANAGER_GB_PRO_ACCOUNT_BUTTON2 1320
#define IDC_MANAGER_GB_PRO_ACCOUNT_BUTTON3 1321
#define IDC_MANAGER_GB_PRO_ACCOUNT_BUTTON4 1322
#define IDC_MANAGER_GB_PRO_ACCOUNT_BUTTON5 1323
#define IDC_MANAGER_GB_PRO_ACCOUNT_BUTTON6 1324
#define IDC_MANAGER_GB_PRO_ACCOUNT_BUTTON7 1325
#define IDC_MANAGER_GB_PRO_ACCOUNT_BUTTON8 1326
#define IDC_MANAGER_GB_PRO_ACCOUNT_BUTTON9 1327
#define IDC_MANAGER_GB_PRO_ACCOUNT_BUTTON0 1328
#define IDC_MANAGER_GB_PRO_ACCOUNT_CLEAR_BUTTON 1329
#define IDC_CASHIER_GB_PRO_BUTTON       1330
#define IDC_CASHIER_MDT_DAY_EDIT        1331
#define IDC_CASHIER_MDT_NUMBER_EDIT     1332
#define IDC_CASHIER_MDT_PAY_BUTTON      1333
#define IDC_CASHIER_MDT_CHECK_BUTTON    1334
#define IDC_CASHIER_MDT_DRAW_RESULTS_BUTTON 1335
#define IDC_CASHIER_MDT_EXIT_BUTTON     1337
#define IDC_CASHIER_MDT_BUTTON0         1338
#define IDC_CASHIER_MDT_CLEAR_BUTTON    1339
#define IDC_CASHIER_MDT_BUTTON1         1340
#define IDC_CASHIER_MDT_BUTTON2         1341
#define IDC_CASHIER_MDT_BUTTON3         1342
#define IDC_CASHIER_MDT_BUTTON4         1343
#define IDC_CASHIER_MDT_BUTTON5         1344
#define IDC_CASHIER_MDT_BUTTON6         1345
#define IDC_CASHIER_MDT_BUTTON7         1346
#define IDC_CASHIER_MDT_BUTTON8         1347
#define IDC_CASHIER_MDT_BUTTON9         1348
#define IDC_MANAGER_REPORT_GB_PRO_BUTTON 1349
#define IDC_MANAGER_GB_PRO_REPORTS_LIST 1350
#define IDC_MANAGER_GB_PRO_REPORTS_EXIT_BUTTON 1351
#define IDC_MANAGER_GB_PRO_REPORTS_POINTS_BUTTON 1352
#define IDC_MANAGER_GB_PRO_REPORTS_TICKETS_BUTTON 1353
#define IDC_MANAGER_GB_PRO_REPORTS_GAME_POINTS_BUTTON 1354
#define IDC_MANAGER_GB_PRO_REPORTS_GAME_TICKETS_BUTTON 1355
#define IDC_MANAGER_GB_PRO_REPORTS_LIST_ALL_BUTTON 1356
#define IDC_MANAGER_GB_PRO_REPORTS_PRINT_BUTTON 1357
#define IDC_MESSAGE_DIALOG_TITLE_STATIC 1358
#define IDC_LOGON_DIALOG_TITLE_STATIC   1359
#define IDC_MAIN_MENU_TITLE_STATIC      1360
#define IDC_COLLECTOR_MENU_TITLE_STATIC 1361
#define IDC_CASHIER_DESTROYED_TICKET_TITLE_STATIC 1362
#define IDC_CASHIER_REDEEM_TICKET_TITLE_STATIC 1363
#define IDC_CASHIER_REPORTS_TITLE_STATIC 1364
#define IDC_TIMER_WINDOW_TITLE_STATIC   1366
#define IDC_TECHNICIAN_DISPLAY_TITLE_STATIC 1367
#define IDC_SELECT_CUSTOMER_ID_TITLE_STATIC 1368
#define IDC_TECHNICIAN_DISPLAY_GAME_INFO_EXIT_BUTTON 1369
#define IDC_TECHNICIAN_GAME_INFO_LIST   1370
#define IDC_TECHNICIAN_DISPLAY_MUX_ID_COMBO 1371
#define IDC_TECHNICIAN_DISPLAY_RESET_BUTTON 1372
#define IDC_TECHNICIAN_DISPLAY_POLLING_BUTTON 1374
#define IDC_TECHNICIAN_MENU_GB_ADVANTAGE_BUTTON 1375
#define IDC_TECHNICIAN_GB_PRO_LIST      1376
#define IDC_TECHNICIAN_MENU_DISPENSE_BUTTON 1376
#define IDC_TECHNICIAN_GB_PRO_PRINTER_STATUS_BUTTON 1377
#define IDC_TECHNICIAN_GB_PRO_PRINTER_RESET_BUTTON 1378
#define IDC_TECHNICIAN_GB_PRO_PRINTER_QUEUE_BUTTON 1379
#define IDC_TECHNICIAN_GB_PRO_SERVER_STATUS_BUTTON 1380
#define IDC_TECHNICIAN_GB_PRO_SERVER_QUEUE_BUTTON 1381
#define IDC_GAME_LOCK_TITLE_STATIC      1383
#define IDC_DISPENSER_UCMC_ID_EDIT      1384
#define IDC_DISPENSER_AUTOPAY_CHECK     1385
#define IDC_DISPENSER_100S_WHEN_20S_LOW_CHECK 1386
#define IDC_TECHNICIAN_MENU_MASTER_SLAVE_BUTTON 1387
#define IDC_TECHNICIAN_MASTER_SLAVE_LIST 1388
#define IDC_TECHNICIAN_MASTER_SLAVE_EDIT 1389
#define IDC_TECHNICIAN_SLAVE_CONTROLLER_ADD_BUTTON 1390
#define IDC_TECHNICIAN_MASTER_CONTROLLER_ADD_BUTTON 1391
#define IDC_TECHNICIAN_CONTROLLER_REMOVE_BUTTON 1392
#define IDC_TECHNICIAN_MASTER_SLAVE_PERIOD_BUTTON 1393
#define IDC_TECHNICIAN_MASTER_SLAVE_CLEAR_BUTTON 1394
#define IDC_TECHNICIAN_MASTER_SLAVE_BUTTON1 1395
#define IDC_TECHNICIAN_MASTER_SLAVE_BUTTON2 1396
#define IDC_TECHNICIAN_MASTER_SLAVE_BUTTON3 1397
#define IDC_TECHNICIAN_MASTER_SLAVE_BUTTON4 1398
#define IDC_TECHNICIAN_MASTER_SLAVE_BUTTON5 1399
#define IDC_TECHNICIAN_MASTER_SLAVE_BUTTON6 1400
#define IDC_TECHNICIAN_MASTER_SLAVE_BUTTON7 1401
#define IDC_TECHNICIAN_MASTER_SLAVE_BUTTON8 1402
#define IDC_TECHNICIAN_MASTER_SLAVE_BUTTON9 1403
#define IDC_TECHNICIAN_MASTER_SLAVE_BUTTON0 1404
#define IDC_TECHNICIAN_MASTER_SLAVE_BUTTON10 1405
#define IDC_TECHNICIAN_MENU_EMAIL_BUTTON 1405
#define IDC_TECHNICIAN_MASTER_CONTROLLER_ADD_BUTTON2 1405
#define IDC_EMAIL_ADD_BUTTON            1406
#define IDC_TECHNICIAN_CONTROLLER_REMOVE_BUTTON2 1406
#define IDC_TECHNICIAN_MENU_TOUCH_SCREEN_BUTTON 1406
#define IDC_EMAIL_REMOVE_BUTTON         1407
#define IDC_EMAIL_CLEAR_BUTTON          1408
#define IDC_EMAIL_BUTTON1               1409
#define IDC_EMAIL_BUTTON2               1410
#define IDC_EMAIL_BUTTON3               1411
#define IDC_EMAIL_BUTTON4               1412
#define IDC_EMAIL_BUTTON5               1413
#define IDC_EMAIL_BUTTON6               1414
#define IDC_EMAIL_BUTTON7               1415
#define IDC_EMAIL_BUTTON8               1416
#define IDC_EMAIL_BUTTON9               1417
#define IDC_EMAIL_BUTTON0               1418
#define IDC_EMAIL_SETTINGS_ADDRESS_COMBO 1419
#define IDC_TECHNICIAN_MASTER_SLAVE_EXIT_BUTTON 1420
#define IDC_EMAIL_EXIT_BUTTON           1421
#define IDC_EMAIL_ALERTS_BUTTON         1422
#define IDC_EMAIL_SETTINGS_EDIT         1423
#define IDC_CASHIER_MARKER_BUTTON       1424
#define IDC_MARKER_DIALOG_STATIC1       1426
#define IDC_MARKER_DIALOG_STATIC2       1427
#define IDC_MARKER_DIALOG_STATIC3       1428
#define IDC_MARKER_EXIT_BUTTON          1429
#define IDC_MARKER_PRINT_BUTTON         1430
#define IDC_MARKER_CANCEL_BUTTON        1431
#define IDC_MARKER_APPROVE_BUTTON       1432
#define IDC_MARKER_COMBO                1433
#define IDC_EMAIL_FLAGS_ADDRESS_STATIC  1434
#define IDC_EMAIL_FLAGS_EXIT_BUTTON     1435
#define IDC_EMAIL_FLAGS_UPDATE_BUTTON   1436
#define IDC_EMAIL_ALERT_STRATUS_LINK_UP_CHECK 1437
#define IDC_EMAIL_ALERT_STRATUS_LINK_DOWN_CHECK 1438
#define IDC_EMAIL_ALERT_PORT_COMM_PROBLEM_CHECK 1444
#define IDC_EMAIL_ALERT_PORT_NO_RESPONSE_CHECK 1445
#define IDC_EMAIL_ALERT_PROGRESSIVE_LINK_DOWN_CHECK 1446
#define IDC_EMAIL_ALERT_MILLION_DOLLAR_TICKET_DRAW_CHECK 1447
#define IDC_EMAIL_ALERT_PLAYER_CARD_STUCK_SENSOR_CHECK 1448
#define IDC_EMAIL_ALERT_PLAYER_CARD_DEAD_READER_CHECK 1449
#define IDC_EMAIL_ALERT_PLAYER_CARD_ABANDONED_CARD_CHECK 1450
#define IDC_ENGINEER_INITIAL_SETTINGS_BUTTON 1451
#define IDC_GAMELOCK_MORE_BUTTON        1452
#define IDC_GAMELOCK_EXIT_BUTTON        1453
#define IDC_EDIT1                       1454
#define IDC_CUSTOMER_CONTROL_SERVER_IP_PORT_EDIT 1454
#define IDC_TECH_DISPLAY_PORT_EDIT      1454
#define IDC_FORCE_COLLECTION_UCMC_ID_EDIT 1454
#define IDC_TECHNICIAN_GB_PRO_MDT_SERVER_SAVE_EDIT 1454
#define IDC_MACHINE_PORT_TO_ADD_PRAC_EDIT 1454
#define IDC_CASH_VOUCHER_PRINTER_IP_PORT_EDIT 1454
#define IDC_GB_PRO_ASCII_POINTS_LINE_ONE_EDIT 1454
#define IDC_TECHNICIAN_UCMC_ID_EDIT     1454
#define IDC_EDIT2                       1455
#define IDC_CUSTOMER_CONTROL_CLIENT_PORT_EDIT 1455
#define IDC_FORCE_COLLECTION_MUX_ID_EDIT 1455
#define IDC_TECHNICIAN_GB_PRO_MDT_PRINTER_SAVE_EDIT 1455
#define IDC_PRAC_IP_AND_PORT_EDIT       1455
#define IDC_GB_PRO_ASCII_POINTS_LINE_TWO_EDIT 1455
#define IDC_MESSAGE_DIALOG_LIST         1456
#define IDC_GB_PRO_CONFIGURATOR_IP_PORT_EDIT 1456
#define IDC_FORCE_COLLECTION_SAS_ID_EDIT 1456
#define IDC_EDIT3                       1457
#define IDC_LOCATION_VIEW_CLIENT_PORT_EDIT 1457
#define IDC_FORCE_COLLECTION_PORT_ID_EDIT 1457
#define IDC_GB_PRO_ASCII_CASH_LINE_ONE_EDIT 1457
#define IDC_CHECK1                      1458
#define IDC_INITIAL_TEST_SETUP_CHECK    1458
#define IDC_DISPENSER_RAW_DATA_CHECK    1458
#define IDC_MANAGER_PRINT_FULL_SHIFT_REPORT 1458
#define IDC_INITIAL_SOUTH_BUTTON        1459
#define IDC_INITIAL_NORTH_BUTTON        1460
#define IDC_INITIAL_EAST_BUTTON         1461
#define IDC_LIST1                       1463
#define IDC_DISPENSER_DISPLAY_LIST      1463
#define IDC_FORCE_COLLECTION_GAME_LIST  1463
#define IDC_TECHNICIAN_RX_DISPLAY_LIST  1463
#define IDC_RAS_STATUS_LIST             1463
#define IDC_PRAC_TX_LIST                1463
#define IDC_TECHNICIAN_GB_PRO_EXIT_BUTTON 1464
#define IDC_COLLECTOR_REBOOT_BUTTON     1465
#define IDC_TECHNICIAN_RAS_CREATE_BUTTON 1466
#define IDC_NETWORK_SETUP_EXIT_BUTTON   1468
#define IDC_NETWORK_SETUP_SAVE_BUTTON   1469
#define IDC_NETWORK_SETUP_TYPE_COMBO    1470
#define IDC_SPIN1                       1471
#define IDC_TECH_CHANGE_PORT_DISPLAY_BUTTON 1472
#define IDC_DISPENSER_CONTROL_BUTTON    1474
#define IDC_DISPENSER_DIALOG_EXIT_BUTTON 1475
#define IDC_DISPENSER_VERBOSE_DATA_CHECK 1476
#define IDC_DISPENSER_RESET_BUTTON      1477
#define IDC_DISPENSER_STATUS_BUTTON     1478
#define IDC_FORCE_COLLECTION_BUTTON     1479
#define IDC_FORCE_COLLECTION_EXIT_BUTTON 1480
#define IDC_FORCE_COLLECTION_STATE_EDIT 1482
#define IDC_FORCE_COLLECTION_NEXT_GAME_BUTTON 1483
#define IDC_FORCE_COLLECTION_PREVIOUS_GAME_BUTTON 1484
#define IDC_FORCE_COLLECTION_START_BUTTON 1485
#define IDC_FORCE_COLLECTION_FORCE_BUTTON 1486
#define IDC_FORCE_COLLECTION_CANCEL_BUTTON 1487
#define IDC_FORCE_COLLECTION_REFRESH_BUTTON 1488
#define IDC_ENGR_CONTROL_PANEL_BUTTON   1489
#define IDC_ENGR_KEYBOARD_BUTTON        1490
#define IDC_ENGR_WINDOWS_EXPLORER_BUTTON 1491
#define IDC_ENGR_TASK_MANAGER_BUTTON    1492
#define IDC_CASHIER_VLORA_BUTTON        1493
#define IDC_MANAGER_VLORA_BUTTON        1494
#define IDC_CASHIER_GAMETENDER_BUTTON   1494
#define IDC_TECHNICIAN_GB_PRO_TEST_MDT_PRINTER_BUTTON 1495
#define IDC_MANAGER_CLEAR_PENDING_TICKETS_BUTTON 1495
#define IDC_TECHNICIAN_GB_PRO_MDT_SERVER_SAVE_BUTTON 1496
#define IDC_TECHNICIAN_GB_PRO_MDT_PRINTER_SAVE_BUTTON 1497
#define IDC_TECHNICIAN_GB_PRO_MDT_KEYBOARD_BUTTON 1498
#define IDC_PRAC_SETUP_BUTTON           1499
#define IDC_PRAC_CONFIG_EXIT_BUTTON     1500
#define IDC_MACHINE_PORT_TO_ADD_PRAC_NEXT_BUTTON 1501
#define IDC_IP_AND_PRAC_PORT_SAVE_BUTTON 1502
#define IDC_PRAC_CONFIG_KEYBOARD_BUTTON 1503
#define IDC_TECHNICIAN_MENU_CASH_VOUCHER_BUTTON 1504
#define IDC_CASH_VOUCHER_EXIT_BUTTON    1505
#define IDC_CASH_VOUCHER_PRINTER_RESET_BUTTON 1506
#define IDC_CASH_VOUCHER_PRINTER_STATUS_BUTTON 1507
#define IDC_CASH_VOUCHER_PRINTER_QUEUE_BUTTON 1508
#define IDC_CASH_VOUCHER_TEST_PRINTER_BUTTON 1509
#define IDC_CASH_VOUCHER_PRINTER_SAVE_BUTTON 1510
#define IDC_CASH_VOUCHER_PRINTER_KEYBOARD_BUTTON 1511
#define IDC_LIST                        1512
#define IDC_CASH_VOUCHER_PRINTER_LIST   1512
#define IDC_GB_PRO_MACHINE_ASCII_MSGS_BUTTON 1513
#define IDC_GB_PRO_ASCII_EXIT_BUTTON    1514
#define IDC_GB_PRO_ASCII_KEYBOARD_BUTTON 1515
#define IDC_GB_PRO_ASCII_SAVE_BUTTON    1516
#define IDC_GB_PRO_ASCII_CASH_LINE_TWO_EDIT 1517
#define IDC_GB_PRO_ASCII_TEST_POINTS_BUTTON 1518
#define IDC_GB_PRO_ASCII_TEST_CASH_BUTTON 1519
#define IDC_GB_PRO_ASCII_TEST_POINTS_EDIT 1520
#define IDC_GB_PRO_ASCII_TEST_CASH_EDIT 1521
#define IDC_TECH_RESTART_APP_BUTTON     1522
#define IDC_TECHNICIAN_MENU_RAS_STATUS_BUTTON 1523
#define IDC_RAS_CONNECTION_STATUS_OK_BUTTON 1524
#define IDC_COLLECTOR_RESTART_APP_BUTTON 1525
#define IDC_LIST2                       1526
#define IDC_PRAC_RX_LIST                1526
#define IDC_TECHICIAN_MESSAGE_ERROR_LIST2 1526
#define IDC_START_ALL                   1527
#define IDC_START_ALL_BUTTON            1527
#define IDC_FORCE_ALL_BUTTON            1528
#define IDC_CANCEL_ALL_BUTTON           1529
#define IDC_TECHNICIAN_GB_ADVANTAGE_INFO_BUTTON 1530
#define IDC_USERID                      1533
#define IDC_PASSWORD                    1534
#define IDC_TECHICIAN_MESSAGE_ERROR_LIST 1536
#define IDC_UPDATE_ERRORS_BUTTON        1537
#define IDC_UCMC_ID_TEXT                1539
#define IDC_UNLOCK_GAME                 1540
#define IDC_MEMBERNUM                   1541
#define IDC_AWARD_POINTS                1542
#define IDC_MANAGER_REPORT_CASHIER_SHIFT_14_BUTTON 1543
#define IDC_MANAGER_REPORT_CASHIER_SHIFT_21_BUTTON 1544
#define IDC_CHECK2                      1545
#define IDC_MANAGER_MAINTENANCE_BUTTON  1546
#define IDC_AWARD_POINTS_TITLE_STATIC   1547
#define IDC_MANAGER_COLLECTION_BUTTON   1547
#define IDC_Malfunction_Overide_Button  1548
#define IDC_MALFUNCTION_OVERRIDE_BUTTON 1548
#define IDC_TECHNICIAN_DISPENSE_BUTTON  1550
#define IDC_TECHNICIAN_DISPENSE_DISPENSE_EVENT_TYPE_COMBO 1552
#define IDC_TECHNICIAN_MENU_CASHOUT_TICKET_BUTTON 1562
#define IDC_CASH_VOUCHER_DELETE_PENDING_TICKETS_BUTTON 1563

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        289
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1564
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

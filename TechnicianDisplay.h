/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   07/06/09    Added diagnostic error statistics in the technician rx/tx screen to help troublshoot game/controller communications.
Dave Elquist    07/15/05    Added game display information.
Dave Elquist    06/15/05    Initial Revision.
*/


#pragma once
#include "afxwin.h"


// CTechnicianDisplay dialog

class CTechnicianDisplay : public CDialog
{
	DECLARE_DYNAMIC(CTechnicianDisplay)

public:
	CTechnicianDisplay(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTechnicianDisplay();
	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_TECHNICIAN_DISPLAY_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

// public functions
public:
	afx_msg void OnBnClickedTechnicianDisplayExitButton();
	afx_msg void OnBnClickedTechnicanDisplayCheck();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedTechnicianDisplayPollingButton();
	afx_msg void OnBnClickedTechnicianDisplayResetButton();
	afx_msg void OnCbnSelchangeTechnicianDisplayMuxIdCombo();
	afx_msg void OnBnClickedTechChangePortDisplayButton();

    void DisplayGameData(GameDataDisplayStruct *pGameDataDisplayStruct);

// public variables
public:
    bool destroy_dialog;
    WORD display_game_port_data;

	CListBox m_technician_tx_display_list_box;
	CListBox m_technician_rx_display_list_box;
	CListBox m_technician_message_error_list_box;
	CListBox m_technician_message_error_list_box2;

// private functions
private:
    CGameDevice* GetGamePointer();

// private variables
private:
	CComboBox m_mux_id_combo_box;

	CButton m_game_data_pause;
	CButton m_reset_button;
	CButton m_polling_button;
	CButton m_tech_change_port_display_button;

	CGameCommControl *pDisplayGameCommControl;

	DWORD offline_poll_error;
	DWORD mux_idle_error;
	DWORD extra_bytes_error;
	DWORD buffer_overflow_error;
	DWORD invalid_handle_error;
	DWORD invalid_read_error;
	DWORD message_header_error;
	DWORD failed_crc_error;
	DWORD unrequested_msg_error;
	DWORD sequence_error;
	DWORD offline_mux_id_error;
	DWORD offline_command_error;
	DWORD unknown_error;

	afx_msg void OnBnClickedUpdateErrorsButton();
};

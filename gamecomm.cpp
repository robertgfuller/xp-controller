/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   11/04/14    Added the redemption complete message to relay the redemption status after a redemption request operation.
Robert Fuller   07/29/14    Fixed problem with the version and subversion for GBIs not being saved to the game info file.
Robert Fuller   07/29/14    Removed interface to the obsolete GB Pro Media Box interface. The IDC takes care of the TV interface now.
Robert Fuller   12/19/13    Added cashable credits win to the player session out dollar out meter.
Robert Fuller   12/19/13    Increment the session coin out meter by the amount of the 4ok bingo win for every case except when there is a hard attendant handpay lockup at the game and the game's jackpot meter increments.
Robert Fuller   11/25/13    Added a transaction id to the GBA win congrats message so that gaming device can determine a duplicate message.
Robert Fuller   11/25/13    Added a transaction id to the winning hand message so we can identify duplicate winning hand messages.
Robert Fuller   11/25/13    Added a transaction id to the clear card message so we can identify duplicate clear card messages.
Robert Fuller   11/20/13    Added the jackpot handpay to the session coin out meter when logging player off because of a jackpot handpay lockup.
Robert Fuller   11/20/13    Added code to send IDC the 4ok bingo winning card message. 
Robert Fuller   11/20/13    Added code to send IDC the bill denomination when a bill is accepted in the gaming machine. 
Robert Fuller   11/20/13    Added code to send IDC the value of the credit meter when it is fetched in real time via the SAS current credits message. 
Robert Fuller   11/20/13    Zeroed out the current tx message buffer when after a no response situation is detected so the next polled game's response is valid.
Robert Fuller   11/20/13    Added a transaction id in the redemption complete message to allow gaming devices to identify duplicate messages.
Robert Fuller   11/20/13    Added GB point redemption transactions to the CENTS WON meter for direct connect games, so that the session Dollar Out meter values are accurate and consistent amongst platforms
Robert Fuller   11/20/13    Reduced the number of processor clock ticks from 5000 to 3000 so that non-responsive games do not slow up polling to the other games in the RS485 loop.
Robert Fuller   11/20/13    Individually assign direct connect ports via FTDI card serial number and port when configuring the game in the XP Controller.
Robert Fuller	06/28/13	Save important Stratus TX messages to a file on disk and do not purge unless Stratus returns an ack.
Robert Fuller	06/28/13	Zero out login info if Stratus returns zero account number or there is an error code..
Robert Fuller	05/10/13	Added support for the new game entered message processing.
Robert Fuller   03/08/13    Send meter messages coming from the gaming devices to the IDC when in offline mode, but not the Stratus
Robert Fuller   03/08/13    Added code to support the configurable dispenser payout limit..
Robert Fuller   03/08/13    Bumped the max game lock amount to $100K.
Robert Fuller   03/08/13    Added new logout message with more enhanced acking message.
Robert Fuller   03/08/13    Eliminated the individual Game Control processing threads, and called new function from OnIdle process all rx/tx messages.
Robert Fuller   03/08/13    Removed obsolete Location Collection code.
Robert Fuller   02/04/13    Keep track of iGBA transaction ids in a linked list instead of fixed array. Only log iGBA win transactions, not GBA win transactions.
Robert Fuller   09/17/12    Print the correct transaction id on the marker advance receipt.
Robert Fuller   09/17/12    Send stacker pull and game entered messages to the IDC.
Robert Fuller   09/17/12    Added iGBA transaction id to differentiate iGBA wins amongst multiple games.
Robert Fuller   09/17/12    Make sure we have a valid redemption before sending data to XPC.
Robert Fuller   09/17/12    Make sure there is not a login status error before storing login info on the XPC and sending the login info to the IDC.
Robert Fuller   09/17/12    Make sure we are ignoring GB Interface devices when traversing through the game device linked list.
Robert Fuller   05/31/12    Print out the transaction ID for MarkerTrax related transactions.
Robert Fuller   05/31/12    Added code to check duplicate 4ok bingo win messages sent from the games.
Robert Fuller   05/31/12    Added code to support the new Quick Enrollment feature.
Robert Fuller   05/31/12    Added code to communicate with the Interactive Display Controller.
Robert Fuller   03/05/12    Added message so GBI could Request SAS Com Status every couple minutes if the sas communicating flag is not set.
Robert Fuller   03/05/12    Send GB Interface update messages regardless of whether or not the player is logged on.
Robert Fuller   03/05/12    Fixed the session point value in the Redeem Points Complete Message. 
Robert Fuller   03/05/12    Corrected problem with saving drop meter GB Trax data to disk.
Robert Fuller   03/05/12    Added View Cards message to send necessary data to the GB Interface.
Robert Fuller   03/05/12    Added new GB message to validate a member id selected during the enrollment process.
Robert Fuller   03/05/12    Send the new primary and secondary 4ok bingo awards in the winning card message.
Robert Fuller   03/05/12    Added a low priority queue for GB Interface update messages.
Robert Fuller   03/05/12    Dequeue all existing GB Interface Update/Initialization messages when queuing new GB Interface Update/Initialization messages.
Robert Fuller   02/03/12    Only dequeue MarkerTrax type messages with custom ack messages sent by the controller.
Robert Fuller   01/27/12    Do NOT put GB point redemption money on the game if there is an error in the redemption complete message.
Robert Fuller   01/27/12    Send an update GB Interface data message when there is a login on a GB Interface game.
Robert Fuller   01/27/12    Added code to send the GB Initialization data to the GB Interface after a 4ok winning hand event.
Robert Fuller   12/30/11    Added a Marker Credit Pending message to notify the Stratus that a marker credit payment needs to be paid.
Robert Fuller   12/30/11    Added an exclusive stratus message transmit queue for Marker Trax messages. Made sure that these messages are only dequeued
                            after receiving the Stratus response.
Robert Fuller   12/30/11    Corrected the logic that calculates the session seconds time played.
Robert Fuller   12/30/11    Fill the whole point and fractional point values with the controller values instead of the GB Interface values.
Robert Fuller   12/30/11    Check for error code in the gb points redemption response before putting money on the game.
Robert Fuller   12/30/11    Added a Marker Balance Update message.
Robert Fuller   12/30/11    Added an error code to the Marker Advance return message that signals a marker payment pending condition.
Robert Fuller   10/03/11    Print the sas id address as opposed to the irrelevant mux id address for SAS direct connect games.
Robert Fuller   10/03/11    Do not process meters from the Device Reset message if the message is coming from a GB Interface device.
Robert Fuller   08/03/11    Removed unnecessary data fields in the update GB Interface message.
Robert Fuller   08/03/11    Added code to support Location Collection features.
Robert Fuller   08/03/11    Deprioritized GB Interface update messages.
Robert Fuller   06/08/11    Added mods to support GB with the XP/SAS direct connect feature.
Robert Fuller   04/26/11    Added code to log SAS communication events.
Robert Fuller   01/25/11    Tuned up the SAS communication by eliminated the message processing thread, and moving the processing code directly
                            into the serial receive task.
Robert Fuller   01/26/11    Request the bill in meter information when we request the general meter data.
Robert Fuller   11/10/10    Only check the SAS Comm connection when we are not receiving response from a game.
Robert Fuller   11/10/10    Add the jackpot to credit and autopay flags to the SAS Comm object.
Robert Fuller   10/07/10    Added code to recover from a USB to RS232 disconnect.
Robert Fuller   08/09/10    Added mods to support the XP/SAS direct connect feature.
Robert Fuller   05/05/10    Added the signature lines for the player and cashier for MarkerTrax related transactions.
Robert Fuller   04/01/10    Added code necessary to abort a marker advance if the game never receives the advance messages from the system.
Robert Fuller   04/01/10    Added player name to marker advance transaction printed on the printer tape.
Robert Fuller   02/03/10    Removed unused code associated with the now defunct prac.
Robert Fuller   01/05/10    Update the stored game meter file on disk when incoming game meters differ from meters in memory.
Robert Fuller   09/03/09    Added a sanity check to not accept game locks over $20K from gaming machines.
Robert Fuller   09/03/09    Added code to relay game configuration data for enabled games to the Stratus.
Robert Fuller   09/03/09    Added software mods to support running the XP Controller in stand alone mode which allows locations to operate with basic features without connecting to the main frame.
Robert Fuller   07/15/09    Make sure the controller is connected to the MarkerTrax port before processing MarkerTrax related game messages.
Robert Fuller   05/15/09    When processing a drink comp message, save changes to the cashier shift data file.
Robert Fuller   05/15/09    Make sure that a ticket add does not have a corresponding paid ticket entry before queueing up a missing game lock.
Robert Fuller   05/01/09    Save the machine meter data every time we process the data. 
Robert Fuller   04/16/09    Removed code associated with PRAC interface.
Robert Fuller   04/08/09    Save game meter snapshot in the force processors.
Robert Fuller   04/07/09    Adjusted the card data info message processor to accommodate the fixed block size sent.
Robert Fuller   03/26/09    Made sure GB Advantage is activated before doing win evaluation logic in card data processor.
Robert Fuller   01/22/09    Added support for the new card data info message.
Robert Fuller   10/15/08    Make sure that we have the right game device when evaluating hand data for GB Advantage wins.
Robert Fuller   10/01/08    When a ticket add is received, see if the game lock file exists. If not create a gamelock internally.
Robert Fuller   08/19/08    Added code to process card data messages.
Robert Fuller   08/19/08    Clear out the login data after an account logout.
Robert Fuller   01/03/08    Removed PRESS ANY KEY TO SILENCE string being printed on ticket.
Robert Fuller   11/05/07    Set the flash active home flag after collect event received.
Dave Elquist    07/15/05    Added Marker support.
Dave Elquist    04/14/05    Initial Revision.
*/

// gamecomm.cpp : implementation file
//

#define _CRT_RAND_S

#include "stdafx.h"
#include "xp_controller.h"
#include "utility.h"
#include ".\gamecomm.h"

#define SEC_IN_HR       3600
#define SEC_IN_MIN      60
#define SEC_IN_DAY 		86400

#define USER_TYPE       0x39
#define WHOLEPOINT 		1000
#define SUCCESS_ENTRY	5

const UINT NICKEL =	5;
//const UINT SAS_POLL_CYCLE = 50;
const UINT SAS_POLL_CYCLE = 20;

static BYTE offline_flag = FALSE;


// CGameDevice
CGameDevice::CGameDevice(MemoryGameConfig *game_config)
{
    CString filename;

    pHeadiGBALinkListWrapper = NULL;
    collection_state_counter = 0;
    idle_msg_status_bits = 0;
    account_tier_wait_timer = 0;
    pNextGame = NULL;
    memcpy(&memory_game_config, game_config, sizeof(memory_game_config));
    memset(&account_login_data, 0, sizeof(account_login_data));
    memset(&cms_account_login_data, 0, sizeof(cms_account_login_data));
    memset(&stored_game_meters.current_game_meters, 0, sizeof(stored_game_meters.current_game_meters));
    memset(&stored_game_meters.last_drop_game_meters, 0, sizeof(stored_game_meters.last_drop_game_meters));
    memset(&last_game_meters_sent_to_idc, 0, sizeof(last_game_meters_sent_to_idc));
    memset(&last_winning_gba_block, 0, sizeof(NewBlock));

    meter_wobbled = FALSE;
    all_zero_meter_read = FALSE;

    stored_game_meters.mux_id = game_config->mux_id;

    low_priority_queue_semiphore = CreateSemaphore(NULL,
                                              1,
                                              1,
                                              TEXT("LowPriorityQueueSemiphore"));

    filename.Format("%s%u", STORED_METERS, game_config->mux_id);
    if (GetFileAttributes(filename) == 0xFFFFFFFF)
    {
        filename.Format("%s%u", STORED_METERS, game_config->ucmc_id);
        if (GetFileAttributes(filename) == 0xFFFFFFFF)
            memset(&stored_game_meters, 0, sizeof(stored_game_meters));
        else
            fileRead(filename.GetBuffer(0), 0, &stored_game_meters, sizeof(stored_game_meters));
    }
    else
        fileRead(filename.GetBuffer(0), 0, &stored_game_meters, sizeof(stored_game_meters));

	if (memory_game_config.mux_id)
		ResetGame();
}

CGameDevice::~CGameDevice()
{
    while (!transmit_queue.empty())
        transmit_queue.pop();
}

void CGameDevice::WriteModifiedGameConfig()
{
 int file_index = 0;
 FileGameConfig file_game_config;
 FILE *file_handle;
 
 	fopen_s(&file_handle, GAME_INFO, "r+b");

    if (file_handle && !fseek(file_handle, 0, SEEK_SET))
    {
        while (fread(&file_game_config, sizeof(file_game_config), 1, file_handle))
        {
            if (memory_game_config.ucmc_id == file_game_config.memory.ucmc_id)
            {
                if ((memory_game_config.mux_id == file_game_config.memory.mux_id) &&
                    (memory_game_config.sas_id == file_game_config.memory.sas_id))
                {
                    memcpy(&file_game_config.memory, &memory_game_config, sizeof(memory_game_config));

                    if (!fseek(file_handle, file_index * sizeof(file_game_config), SEEK_SET))
                        fwrite(&file_game_config, sizeof(file_game_config), 1, file_handle);
                }
                else
                {
                    if (!file_game_config.memory.version)
                    {
                        file_game_config.memory.version = memory_game_config.version;
                        file_game_config.memory.sub_version = memory_game_config.sub_version;

                        if (!fseek(file_handle, file_index * sizeof(file_game_config), SEEK_SET))
                            fwrite(&file_game_config, sizeof(file_game_config), 1, file_handle);
                    }
                }
                break;
            }
            else
                file_index++;
        }

        fclose(file_handle);
    }
}

void CGameDevice::ResetGame()
{
 TxSerialMessage message;

    memset(&message, 0, sizeof(message));
    message.msg.mux_id = memory_game_config.mux_id;
    message.msg.length = 8;
    message.msg.command = COMMAND_HEADER;
    message.msg.data[0] = RESET_MUX;
    transmit_queue.push(message);
}

void CGameDevice::SetDispenserPayoutLimit()
{
    DWORD dispenser_payout_limit = theApp.memory_dispenser_config.dispenser_payout_limit;
	TxSerialMessage message;

    memset(&message, 0, sizeof(message));
    message.msg.mux_id = memory_game_config.mux_id;
    message.msg.length = 10;
    message.msg.command = S__SET_DISPENSER_PAYOUT_LIMIT;
    memcpy (&message.msg.data[0], (BYTE*)&dispenser_payout_limit, sizeof(dispenser_payout_limit));

    switch (memory_game_config.version)
    {
        case 12:    // Power Vision
            if (memory_game_config.sub_version >= 2)
                transmit_queue.push(message);
            break;
        case 5:    // Bally V7000
            if (memory_game_config.sub_version >= 25)
                transmit_queue.push(message);
            break;
        case 10:    // GB SAS Interface
            if (memory_game_config.sub_version >= 58)
                transmit_queue.push(message);
            break;
    }
}

void CGameDevice::updateGbInterfaceData(UpdateGbInterfaceData* update_gb_interface_data)
{
    TxSerialMessage message, low_priority_message, temp_message;
    queue <TxSerialMessage, deque<TxSerialMessage> > temp_queue;

    memset(&message, 0, sizeof(message));
    message.msg.mux_id = memory_game_config.mux_id;
    message.msg.length = 27;
    message.msg.command = CARET_HEADER;
    message.msg.data[0] = UPDATE_GB_INTERFACE;

    memcpy (&message.msg.data[1], (BYTE*)update_gb_interface_data, sizeof(UpdateGbInterfaceData));

    memset(&low_priority_message, 0, sizeof(low_priority_message));

    WaitForSingleObject(low_priority_queue_semiphore, MAX_EVENT_WAIT_TIMOUT);
    // put all messages in a temp queue, except all existing initialization messages
    while(!low_priority_transmit_queue.empty())
    {
        low_priority_message = low_priority_transmit_queue.front();
        if (!((low_priority_message.msg.command == CARET_HEADER) && (low_priority_message.msg.data[0] == UPDATE_GB_INTERFACE)))
        {
            temp_queue.push(low_priority_message);        
        }
        low_priority_transmit_queue.pop();
    }
    // put all the temp queue elements back into the low priority queue
    while(!temp_queue.empty())
    {
        temp_message = temp_queue.front();
        low_priority_transmit_queue.push(temp_message);        
        temp_queue.pop();
    }

    low_priority_transmit_queue.push(message);

    ReleaseSemaphore(low_priority_queue_semiphore, 1, NULL);



}

void CGameDevice::initializeGbInterfaceData(GbInterfaceInitialize* gb_interface_initialize)
{
    TxSerialMessage message, low_priority_message, temp_message;
    queue <TxSerialMessage, deque<TxSerialMessage> > temp_queue;

    // send game configuration data(turn on/off features, timing values, etc...)
    memset(&message, 0, sizeof(message));
    message.msg.length = 53;
    message.msg.mux_id = memory_game_config.mux_id;
    message.msg.command = CARET_HEADER;
    message.msg.data[0] = INITIALIZE_GB_INTERFACE;

    memcpy(&message.msg.data[1], (BYTE*)gb_interface_initialize, sizeof(GbInterfaceInitialize));

    WaitForSingleObject(low_priority_queue_semiphore, MAX_EVENT_WAIT_TIMOUT);
    // put all messages in a temp queue, except all existing initialization messages
    while(!low_priority_transmit_queue.empty())
    {
        low_priority_message = low_priority_transmit_queue.front();
        if (!((low_priority_message.msg.command == CARET_HEADER) && (low_priority_message.msg.data[0] == INITIALIZE_GB_INTERFACE)))
        {
            temp_queue.push(low_priority_message);        
        }
        low_priority_transmit_queue.pop();
    }
    // put all the temp queue elements back into the low priority queue
    while(!temp_queue.empty())
    {
        temp_message = temp_queue.front();
        low_priority_transmit_queue.push(temp_message);        
        temp_queue.pop();
    }

    low_priority_transmit_queue.push(message);

    ReleaseSemaphore(low_priority_queue_semiphore, 1, NULL);
}

void CGameDevice::updateSasCommunicationStatus(bool communicating)
{
    TxSerialMessage message;

    memset(&message, 0, sizeof(message));
    message.msg.mux_id = memory_game_config.mux_id;
    message.msg.length = 8;
    message.msg.command = CARET_HEADER;
    message.msg.data[0] = SAS_COMMUNICATION_STATUS;

	if (communicating)
		message.msg.data[1] = 1;
	else
		message.msg.data[1] = 0;

    transmit_queue.push(message);
}

void CGameDevice::updateGbInterfaceWithBingoWinningHandInfo(BingoWinningHand* bingo_winning_hand)
{
    TxSerialMessage message;

    memset(&message, 0, sizeof(message));
    message.msg.mux_id = memory_game_config.mux_id;
    message.msg.length = 20;
    message.msg.command = CARET_HEADER;
    message.msg.data[0] = BINGO_WINNING_HAND;

    memcpy (&message.msg.data[1], (BYTE*)bingo_winning_hand, sizeof(BingoWinningHand));

    transmit_queue.push(message);
}

void CGameDevice::updateGbInterfaceWithBingoWinningCardInfo(UpdateGbInterfaceWinningCardData* winning_card_data)
{
    TxSerialMessage message;

    memset(&message, 0, sizeof(message));
    message.msg.mux_id = memory_game_config.mux_id;
    message.msg.length = 15;
    message.msg.command = CARET_HEADER;
    message.msg.data[0] = BINGO_WINNING_CARD;

    memcpy (&message.msg.data[1], (BYTE*)winning_card_data, sizeof(UpdateGbInterfaceWinningCardData));

    transmit_queue.push(message);
}

void CGameDevice::addGbaInteractiveBonusTransactionId (DWORD transaction_id)
{
    iGBALinkListWrapper* pNewiGBALinkListWrapper = new iGBALinkListWrapper;
    iGBALinkListWrapper* piGBALinkList = pHeadiGBALinkListWrapper;

    pNewiGBALinkListWrapper->transaction_id = transaction_id;
    pNewiGBALinkListWrapper->next = 0;
    pNewiGBALinkListWrapper->last = 0;

    if (pHeadiGBALinkListWrapper)
    {
        iGBALinkListWrapper* pLastiGBALinkListWrapper = pHeadiGBALinkListWrapper;

        while (pLastiGBALinkListWrapper->next)
        {
            pLastiGBALinkListWrapper = pLastiGBALinkListWrapper->next;
        }
        pNewiGBALinkListWrapper->last = pLastiGBALinkListWrapper;
        pLastiGBALinkListWrapper->next = pNewiGBALinkListWrapper;
    }
    else
    {
        pHeadiGBALinkListWrapper = pNewiGBALinkListWrapper;
    }
}

bool CGameDevice::removeGbaInteractiveBonusTransactionId (DWORD transaction_id)
{
	bool removed = FALSE;

    if (pHeadiGBALinkListWrapper)
    {
		if (pHeadiGBALinkListWrapper->transaction_id == transaction_id)
        {
            removeGbaInteractiveBonusTransactionId (pHeadiGBALinkListWrapper);
            removed = TRUE;
        }
        else
        {
            iGBALinkListWrapper* pLastiGBALinkListWrapper = pHeadiGBALinkListWrapper->next;

            while (pLastiGBALinkListWrapper)
            {
                if (pLastiGBALinkListWrapper->transaction_id == transaction_id)
                {
                    removeGbaInteractiveBonusTransactionId (pLastiGBALinkListWrapper);
                    removed = TRUE;
                    break;
                }
                else
                    pLastiGBALinkListWrapper = pLastiGBALinkListWrapper->next;
            }
        }
    }

	return removed;
}

void CGameDevice::removeGbaInteractiveBonusTransactionId (iGBALinkListWrapper* piGBALinkListWrapper)
{

    if (pHeadiGBALinkListWrapper && piGBALinkListWrapper)
    {

        if (pHeadiGBALinkListWrapper == piGBALinkListWrapper)
        {
            if (pHeadiGBALinkListWrapper->next)
            {
                pHeadiGBALinkListWrapper->next->last = 0;
                pHeadiGBALinkListWrapper = pHeadiGBALinkListWrapper->next;
            }
            else
                pHeadiGBALinkListWrapper = 0;

            delete piGBALinkListWrapper;
        }
        else
        {
            iGBALinkListWrapper* pNextiGBALinkListWrapper = pHeadiGBALinkListWrapper;

            if (pNextiGBALinkListWrapper->next == piGBALinkListWrapper)
            {
                pNextiGBALinkListWrapper->next = piGBALinkListWrapper->next;
                if (piGBALinkListWrapper->next)
                    piGBALinkListWrapper->next->last = pNextiGBALinkListWrapper;
                delete piGBALinkListWrapper;
            }
            else
            {
                while (pNextiGBALinkListWrapper->next && (pNextiGBALinkListWrapper->next != piGBALinkListWrapper))
                {
                    pNextiGBALinkListWrapper = pNextiGBALinkListWrapper->next;

                    if (pNextiGBALinkListWrapper->next == piGBALinkListWrapper)
                    {
                        pNextiGBALinkListWrapper->next = piGBALinkListWrapper->next;
                        if (piGBALinkListWrapper->next)
                            piGBALinkListWrapper->next->last = pNextiGBALinkListWrapper;
                        delete piGBALinkListWrapper;
                    }
                }
            }
        }
    }
}

// set game collection state
void CGameDevice::SetGameCollectionState(BYTE state)
{
    if (memory_game_config.collection_state != state)
    {
        switch (state)
        {
            case CLEAR_COLLECTION_STATE:
                memory_game_config.collection_state = CLEAR_COLLECTION_STATE;
                break;

            case START_COLLECTION_STATE:
                if (memory_game_config.collection_state == CLEAR_COLLECTION_STATE)
                    memory_game_config.collection_state = START_COLLECTION_STATE;
                break;

            case ARMED_COLLECTION_STATE:
                if (memory_game_config.collection_state == START_COLLECTION_STATE)
                    memory_game_config.collection_state = ARMED_COLLECTION_STATE;
                break;

            case FORCE_COLLECTION_STATE:
                if (!memory_game_config.sas_id && memory_game_config.collection_state == ARMED_COLLECTION_STATE)
                    memory_game_config.collection_state = FORCE_COLLECTION_STATE;
                break;

            case CANCEL_COLLECTION_STATE:
                if (memory_game_config.collection_state)
                    memory_game_config.collection_state = CANCEL_COLLECTION_STATE;
                break;

            default:
                logMessage("CGameDevice::SetGameCollectionState - invalid collection state.");
                return;
        }

        collection_state_counter = 0;
//        WriteModifiedGameConfig();
    }

	if (theApp.pGameCollectionState)
	{
		int ucmc_id;
		CString edit_string;

		theApp.pGameCollectionState->GetDlgItemText(IDC_FORCE_COLLECTION_UCMC_ID_EDIT, edit_string);
		ucmc_id = atoi(edit_string);

		if (memory_game_config.ucmc_id == ucmc_id)
			theApp.pGameCollectionState->ListGameInformation(this);
	}
}

void CGameDevice::SaveGameMeterDropSnapshot (void)
{
        memcpy(&stored_game_meters.last_drop_game_meters, &stored_game_meters.current_game_meters, sizeof(GameMeters));
}

CGameCommControl::CGameCommControl()
{
    pHeadGameDevice = NULL;
    pNextGameCommControl = NULL;
    pPreviousGameCommControl = NULL;
    pGameDeviceCurrentPollingGame = NULL;
    hGameSerialPort = INVALID_HANDLE_VALUE;
    pGameSocket = NULL;
	sas_com = 0;
}

// CGameCommControl
CGameCommControl::CGameCommControl(GameCommPortInfo *pGameCommPortInfo, MemoryGameConfig *pMemoryGameConfig)
{
    CString filename, message_string;
    CWinThread* SAS_comm_thread_handle = NULL;
    CGameDevice *pNewGameDevice = NULL;

    memcpy(&port_info, pGameCommPortInfo, sizeof(port_info));

    pHeadGameDevice = NULL;
    pNextGameCommControl = NULL;
    pPreviousGameCommControl = NULL;
    pGameDeviceCurrentPollingGame = NULL;

    hGameSerialPort = INVALID_HANDLE_VALUE;

    memset(&current_tx_message, 0, sizeof(current_tx_message));
    memset(&message_errors, 0, sizeof(message_errors));
    memset(&game_receive, 0, sizeof(game_receive));

    // setup network connnection, else serial connection
    if (port_info.ip_address)
	{
        pGameSocket = new CMyAsyncSocket;
		OpenGameTcpIpPort();
	}
    else
    {
        pGameSocket = NULL;
        game_port_check = 1;
    }
    last_port_check_time = 0;
	stop_comm_thread = false;
    connection_check_second_delay = 60;

    if (pMemoryGameConfig->sas_id)
    {

        pNewGameDevice = new CGameDevice(pMemoryGameConfig);

        if (!pHeadGameDevice)
        {
            pHeadGameDevice = pNewGameDevice;
        }


        sas_com = new CSAS_Comm(this, pMemoryGameConfig->sas_id);
        if (sas_com)
        {
            // load non_volatile_ram for this SAS Comm object
            filename.Format("%s%u", GAME_DATA, pMemoryGameConfig->ucmc_id);
            if (GetFileAttributes(filename) == 0xFFFFFFFF)
                memset(&sas_com->non_volatile_ram, 0, sizeof(NonVolatileRam));
            else
                fileRead(filename.GetBuffer(0), 0, &sas_com->non_volatile_ram, sizeof(NonVolatileRam));

            if (sas_com->openSASPort(pMemoryGameConfig->sas_id, pMemoryGameConfig->serial_number))
            {
                sas_com->non_volatile_ram.jackpot_to_credit_enabled = pMemoryGameConfig->jackpot_to_credit_pay;
                sas_com->non_volatile_ram.autopay_enabled = theApp.memory_dispenser_config.autopay_allowed;
                sas_com->initializeSAS();
            }
        }
        else
        {
            delete sas_com;
            sas_com = 0;
        }
    }
    else
        sas_com = 0;
}

CGameCommControl::~CGameCommControl()
{
 CGameDevice *pGameDevice, *pNextGameDevice;

    while (!stratus_mpi_rx_queue.empty())
        stratus_mpi_rx_queue.pop();

    while (!stratus_mpi_rx_marker_queue.empty())
        stratus_mpi_rx_marker_queue.pop();

    while (!stratus_waa_rx_queue.empty())
        stratus_waa_rx_queue.pop();

    while (!broadcast_queue.empty())
        broadcast_queue.pop();

    pGameDevice = pHeadGameDevice;

    while (pGameDevice)
    {
        pNextGameDevice = pGameDevice->pNextGame;
        delete pGameDevice;
        pGameDevice = pNextGameDevice;
    }

    if (pGameSocket)
    {
        if (pGameSocket->m_hSocket != INVALID_SOCKET)
            pGameSocket->Close();
        Sleep(1000);    // give the socket time to close
        delete pGameSocket;
		pGameSocket = NULL;
    }

    if (hGameSerialPort != INVALID_HANDLE_VALUE)
        CloseHandle(hGameSerialPort);

    if (sas_com)
    {
        delete sas_com;
        sas_com = 0;
    }
}

void CGameCommControl::ProcessRxTxGameMessages (void)
{

    if (!sas_com)
	{
	    // check game comm connection
		if (pGameSocket)
			last_port_check = pGameSocket->socket_state;
		else
			last_port_check = game_port_check;

		if (!last_port_check)
	    {
	        if (!current_tx_message.response_pending)
	            TransmitGameMessage();
			else
			{
				CheckGameResponse();

	            milli_second_time = GetTickCount();

		        // if message pending, check response time
	    	    // added 2nd condition when GetTickCount() rolls over after 49.71 days
				if (current_tx_message.response_pending && (milli_second_time > current_tx_message.response_time_timer ||
	            	current_tx_message.response_time_timer > milli_second_time + 3000))
                    ProcessNoGameResponse();
			}

			if (!current_tx_message.response_pending)
	            TransmitGameMessage();
			else
				CheckGameResponse();
		}
	}
}

// frenzy awards have been discontinued
void CGameCommControl::ProcessWaaMessage()
{
    stratus_waa_rx_queue.pop();
}

 #pragma pack(1)
 struct
 {
	DWORD credit_limit;
	DWORD current_balance;
	DWORD amount_advanced;
    char name_string [13];
	byte error_code;
 } marker_request;
 struct
 {
	DWORD transaction_id;
	DWORD current_balance;
	DWORD credit_limit;
 } abort_marker_request;
 struct
 {
	DWORD current_balance;
 } update_marker_balance;
 #pragma pack()

void CGameCommControl::ProcessMpiMessage()
{
 BYTE *encrypted_data;
 int iii;
 StratusMessageFormat stratus_message = stratus_mpi_rx_queue.front();
 CGameDevice *pGameDevice = GetGamePointer(stratus_message.ucmc_id);
 WORD length = stratus_message.message_length - STRATUS_HEADER_SIZE;
 CString log_message, prt_buf;
 AccountLogin account_login;
 GbSessionInfoData gb_session_info_data;
 CGameCommControl *pGameCommControl;
 CString filename;
 SYSTEMTIME system_time;
 unsigned char secs, mins, hours;

    stratus_mpi_rx_queue.pop();

    if (pGameDevice && stratus_message.task_code == MPI_TASK_PLAYER_ACCOUNTING)
    {
        switch (stratus_message.send_code)
        {
            case MPI_MARKER_CREDIT:
            case MPI_ABORT_MARKER_CREDIT:
                memcpy(&update_marker_balance, &stratus_message.data[0], sizeof(update_marker_balance));
                endian4(update_marker_balance.current_balance);

                memcpy(&current_tx_message.msg.data[1], &update_marker_balance, sizeof(update_marker_balance));

                iii = 4;
				encrypted_data = theApp.EncryptMachineData(&current_tx_message.msg.data[1], &iii);
				memcpy(&current_tx_message.msg.data[1], encrypted_data, iii);

				current_tx_message.msg.length = iii + MINIMUM_ENCRYPTED_SERIAL_MESSAGE_SIZE;
                current_tx_message.msg.command = ASTERISK_HEADER;
                current_tx_message.msg.data[0] = U__UPDATE_MARKER_BALANCE;
                current_tx_message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                break;

            // QUEUE MUX CARD RETURN MESSAGE
            // Account information from the Stratus to the game
            // rx: account(long), name(char[16]), rating(short), whole points(ulong), fractional points(short),
            //	   session fractional points(short), bonus(short), ltd points(long), pin number(short), status code(char),
            //	   game card type(char), primary card handle(long), primary card games(long), primary card marks(long),
            //	   secondary card handle(long), secondary card games(long), secondary card marks(long), tier level(char),
            //     unused_byte(char), Marker limit(DWORD), Marker owed(DWORD)
            // tx: minus account and change endian
            // The account number is not used.
            case MPI_NEW_ACCOUNT_LOGIN:
                // error check message length
                if (length < 73)
                {
                    log_message.Format("GameCommControl::ProcessMpiMessage - MPI_NEW_ACCOUNT_LOGIN invalid message length, ucmc id %u.", stratus_message.ucmc_id);
                    logMessage(log_message);
                }
                else
                {
                    memcpy(&account_login, stratus_message.data, sizeof(account_login));

                    endian4(account_login.account_number);
                    endian4(account_login.whole_points);
                    endian4(account_login.ltd_points);
                    endian4(account_login.primary_card_handle);
                    endian4(account_login.primary_card_games);
                    endian4(account_login.primary_card_marks);
                    endian4(account_login.secondary_card_handle);
                    endian4(account_login.secondary_card_games);
                    endian4(account_login.secondary_card_marks);
                    endian4(account_login.marker_limit);
                    endian4(account_login.marker_owed);
                    endian2(account_login.fractional_points);
                    endian2(account_login.session_fractional_points);
                    endian2(account_login.bonus);
                    endian2(account_login.pin_number);

                    if (pGameDevice && !account_login.status_code)
                    {
                        pGameCommControl = theApp.GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);

                        if (pGameCommControl && pGameCommControl->sas_com)
                        {
							if (pGameCommControl->pHeadGameDevice)
	                            memcpy(&pGameCommControl->pHeadGameDevice->account_login_data, &account_login, sizeof(pGameCommControl->pHeadGameDevice->account_login_data));

                    	    memcpy(pGameCommControl->sas_com->non_volatile_ram.gb_player_name, account_login.name, 16);

                            pGameCommControl->sas_com->non_volatile_ram.whole_points = account_login.whole_points;
                            pGameCommControl->sas_com->last_session_update_points = account_login.whole_points;
                            pGameCommControl->sas_com->non_volatile_ram.fract_points = account_login.fractional_points; 
                            pGameCommControl->sas_com->non_volatile_ram.inc_points = account_login.session_fractional_points; 
                            pGameCommControl->sas_com->non_volatile_ram.monthly_whole_points = account_login.ltd_points; 
                            pGameCommControl->sas_com->non_volatile_ram.bingo.primary.type = account_login.game_card_type; 
                            pGameCommControl->sas_com->non_volatile_ram.bingo.primary.handle_to_date = account_login.primary_card_handle; 
                            pGameCommControl->sas_com->non_volatile_ram.bingo.primary.games_to_date = account_login.primary_card_games; 
                            pGameCommControl->sas_com->non_volatile_ram.bingo.primary.card = account_login.primary_card_marks; 
                            pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.handle_to_date = account_login.secondary_card_handle; 
                            pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.games_to_date = account_login.secondary_card_games; 
                            pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.card = account_login.secondary_card_marks; 

                            pGameCommControl->sas_com->non_volatile_ram.ses_data.whole_points = pGameCommControl->sas_com->non_volatile_ram.whole_points; 
                            pGameCommControl->sas_com->non_volatile_ram.ses_data.fract_points = pGameCommControl->sas_com->non_volatile_ram.fract_points; 
                            pGameCommControl->sas_com->non_volatile_ram.ses_data.cents_in = pGameCommControl->sas_com->non_volatile_ram.TOTAL_IN; 
                            pGameCommControl->sas_com->non_volatile_ram.ses_data.cents_out = pGameCommControl->sas_com->non_volatile_ram.CENTS_WON; 
                            pGameCommControl->sas_com->non_volatile_ram.ses_data.drop_coins = pGameCommControl->sas_com->non_volatile_ram.TOTAL_DROP; 
                            pGameCommControl->sas_com->non_volatile_ram.ses_data.games = pGameCommControl->sas_com->non_volatile_ram.GAMES_PLAYED;

                    	 	pGameCommControl->sas_com->non_volatile_ram.session_points = 0;

                            GetLocalTime(&system_time);

        	                secs = (unsigned char)system_time.wSecond;
        	                mins = (unsigned char)system_time.wMinute;
        	                hours = (unsigned char)system_time.wHour;

			                pGameCommControl->sas_com->non_volatile_ram.ses_data.timer = (hours * SEC_IN_HR) + (mins * SEC_IN_MIN) + secs;

                            pGameCommControl->sas_com->changeGbSystemState(ON_AVAIL);
                            // save non volatile ram data
                            pGameCommControl->sas_com->saveNonVolatileRam();

                        }
                    }

                	memcpy(&current_tx_message.msg.data[1], account_login.name, 16);
                    memcpy(&current_tx_message.msg.data[17], &account_login.rating, 2);
                    memcpy(&current_tx_message.msg.data[19], &account_login.whole_points, 4);
                    memcpy(&current_tx_message.msg.data[23], &account_login.fractional_points, 2);
                    memcpy(&current_tx_message.msg.data[25], &account_login.session_fractional_points, 2);
                    memcpy(&current_tx_message.msg.data[27], &account_login.bonus, 2);
                    memcpy(&current_tx_message.msg.data[29], &account_login.ltd_points, 4);
                    memcpy(&current_tx_message.msg.data[33], &account_login.pin_number, 2);
                    current_tx_message.msg.data[35] = account_login.status_code;
                    current_tx_message.msg.data[36] = account_login.game_card_type;
                    memcpy(&current_tx_message.msg.data[37], &account_login.primary_card_handle, 4);
                    memcpy(&current_tx_message.msg.data[41], &account_login.primary_card_games, 4);
                    memcpy(&current_tx_message.msg.data[45], &account_login.primary_card_marks, 4);
                    memcpy(&current_tx_message.msg.data[49], &account_login.secondary_card_handle, 4);
                    memcpy(&current_tx_message.msg.data[53], &account_login.secondary_card_games, 4);
                    memcpy(&current_tx_message.msg.data[57], &account_login.secondary_card_marks, 4);
                    memcpy(&current_tx_message.msg.data[61], &account_login.marker_limit, 4);
                    memcpy(&current_tx_message.msg.data[65], &account_login.marker_owed, 4);

                    iii = 68;
					encrypted_data = theApp.EncryptMachineData(&current_tx_message.msg.data[1], &iii);
					memcpy(&current_tx_message.msg.data[1], encrypted_data, iii);

					current_tx_message.msg.length = iii + MINIMUM_ENCRYPTED_SERIAL_MESSAGE_SIZE;
                	current_tx_message.msg.command = ASTERISK_HEADER;
                	current_tx_message.msg.data[0] = NEW_ACCOUNT_INQUIRY_RETURN;
                	current_tx_message.msg.mux_id = pGameDevice->memory_game_config.mux_id;

                    // store account info
                    if (account_login.account_number && account_login.name[0] && !account_login.status_code)
                    {
                        account_login.pin_number = 0;   // don't store pin number for security reasons
                        memcpy(&pGameDevice->account_login_data, &account_login, sizeof(pGameDevice->account_login_data));
                        pGameDevice->account_tier_wait_timer = time(NULL) + 60; // timeout value for tier level printing
                        saveGbLoginChange(&account_login, pGameDevice->memory_game_config.ucmc_id);
                        MachineIdentifiers machine_identifiers;
                        machine_identifiers.mux_id = pGameDevice->memory_game_config.mux_id;
                        machine_identifiers.sas_id = pGameDevice->memory_game_config.sas_id;
                        machine_identifiers.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
						theApp.SendIdcLoginMessage (pGameDevice, &account_login);
                    }
                    else
                    {
                        memset(&pGameDevice->account_login_data, 0, sizeof(pGameDevice->account_login_data));
                        saveGbLoginChange(&account_login, pGameDevice->memory_game_config.ucmc_id);
                    }
                }
                break;

            // QUEUE MUX CARD RETURN MESSAGE
            // Account information from the Stratus to the game
            // rx: account(long), name(char[16]), rating(short), whole points(ulong), fractional points(short),
            //	   session fractional points(short), bonus(short), ltd points(long), pin number(short), status code(char),
            //	   game card type(char), primary card handle(long), primary card games(long), primary card marks(long),
            //	   secondary card handle(long), secondary card games(long), secondary card marks(long), tier level(char)
            // tx: minus account and change endian
            // The account number is not used.
            case MPI_ACCOUNT_LOGIN:
                // error check message length
                if (length < 64)
                {
                    log_message.Format("GameCommControl::ProcessMpiMessage - MPI_ACCOUNT_LOGIN invalid message length, ucmc id %u.", stratus_message.ucmc_id);
                    logMessage(log_message);
                }
                else
                {
                    memset(&account_login, 0, sizeof(account_login));
                    memcpy(&account_login, stratus_message.data, 65);
                	current_tx_message.msg.command = COMMAND_HEADER;
                	current_tx_message.msg.data[0] = ACCOUNT_INQUIRY_RETURN;
                    current_tx_message.msg.length = 67;

                    endian4(account_login.account_number);
                    endian4(account_login.whole_points);
                    endian4(account_login.ltd_points);
                    endian4(account_login.primary_card_handle);
                    endian4(account_login.primary_card_games);
                    endian4(account_login.primary_card_marks);
                    endian4(account_login.secondary_card_handle);
                    endian4(account_login.secondary_card_games);
                    endian4(account_login.secondary_card_marks);
                    endian4(account_login.marker_limit);
                    endian4(account_login.marker_owed);
                    endian2(account_login.fractional_points);
                    endian2(account_login.session_fractional_points);
                    endian2(account_login.bonus);
                    endian2(account_login.pin_number);

//                    if (pGameDevice && !account_login.status_code)
                    if (pGameDevice)
                    {
                        pGameCommControl = theApp.GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);

                        if (pGameCommControl && pGameCommControl->sas_com)
                        {
                            if (!account_login.status_code)
                            {
							    if (pGameCommControl->pHeadGameDevice)
	                                memcpy(&pGameCommControl->pHeadGameDevice->account_login_data, &account_login, sizeof(pGameCommControl->pHeadGameDevice->account_login_data));

                    	        memcpy(pGameCommControl->sas_com->non_volatile_ram.gb_player_name, account_login.name, 16);

                                pGameCommControl->sas_com->non_volatile_ram.whole_points = account_login.whole_points; 
                                pGameCommControl->sas_com->last_session_update_points = account_login.whole_points;
                                pGameCommControl->sas_com->non_volatile_ram.fract_points = account_login.fractional_points; 
                                pGameCommControl->sas_com->non_volatile_ram.inc_points = account_login.session_fractional_points; 
                                pGameCommControl->sas_com->non_volatile_ram.monthly_whole_points = account_login.ltd_points; 
                                pGameCommControl->sas_com->non_volatile_ram.bingo.primary.type = account_login.game_card_type; 
                                pGameCommControl->sas_com->non_volatile_ram.bingo.primary.handle_to_date = account_login.primary_card_handle; 
                                pGameCommControl->sas_com->non_volatile_ram.bingo.primary.games_to_date = account_login.primary_card_games; 
                                pGameCommControl->sas_com->non_volatile_ram.bingo.primary.card = account_login.primary_card_marks; 
                                pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.handle_to_date = account_login.secondary_card_handle; 
                                pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.games_to_date = account_login.secondary_card_games; 
                                pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.card = account_login.secondary_card_marks; 

                                pGameCommControl->sas_com->non_volatile_ram.ses_data.whole_points = pGameCommControl->sas_com->non_volatile_ram.whole_points; 
                                pGameCommControl->sas_com->non_volatile_ram.ses_data.fract_points = pGameCommControl->sas_com->non_volatile_ram.fract_points; 
                                pGameCommControl->sas_com->non_volatile_ram.ses_data.cents_in = pGameCommControl->sas_com->non_volatile_ram.TOTAL_IN; 
                                pGameCommControl->sas_com->non_volatile_ram.ses_data.cents_out = pGameCommControl->sas_com->non_volatile_ram.CENTS_WON; 
                                pGameCommControl->sas_com->non_volatile_ram.ses_data.drop_coins = pGameCommControl->sas_com->non_volatile_ram.TOTAL_DROP; 
                                pGameCommControl->sas_com->non_volatile_ram.ses_data.games = pGameCommControl->sas_com->non_volatile_ram.GAMES_PLAYED;

                    	 	    pGameCommControl->sas_com->non_volatile_ram.session_points = 0;

                                GetLocalTime(&system_time);

        	                    secs = (unsigned char)system_time.wSecond;
        	                    mins = (unsigned char)system_time.wMinute;
        	                    hours = (unsigned char)system_time.wHour;

			                    pGameCommControl->sas_com->non_volatile_ram.ses_data.timer = (hours * SEC_IN_HR) + (mins * SEC_IN_MIN) + secs;

                                pGameCommControl->sas_com->changeGbSystemState(ON_AVAIL);
                                // save non volatile ram data
                                pGameCommControl->sas_com->saveNonVolatileRam();
                            }

                            // update the GB Interface with the login data
                	        current_tx_message.msg.command = CARET_HEADER;
                	        current_tx_message.msg.data[0] = UPDATE_GB_INTERFACE;
                            current_tx_message.msg.length = 27;
                    	    current_tx_message.msg.mux_id = pGameDevice->memory_game_config.mux_id;

                            UpdateGbInterfaceData update_gb_interface_data;

                            memset(&update_gb_interface_data, 0, sizeof(UpdateGbInterfaceData));

                            update_gb_interface_data.whole_points = account_login.whole_points;
                            update_gb_interface_data.primary_card_award = pGameCommControl->sas_com->calculateCardAward (account_login.primary_card_handle,
                                                                                                       account_login.primary_card_games);
                            update_gb_interface_data.secondary_card_award = pGameCommControl->sas_com->calculateCardAward (account_login.secondary_card_handle,
                                                                                                         account_login.secondary_card_games);
                            update_gb_interface_data.current_bank_meter = pGameCommControl->sas_com->non_volatile_ram.current_bank_meter;
                            update_gb_interface_data.max_bet = pGameCommControl->sas_com->non_volatile_ram.max_bet;
                            update_gb_interface_data.door_state = pGameCommControl->sas_com->non_volatile_ram.door_state;

                            update_gb_interface_data.logged_in_flag = TRUE;

                	        memcpy(&current_tx_message.msg.data[1], &update_gb_interface_data, sizeof(update_gb_interface_data));

                            current_tx_message.response_pending = true;
                            current_tx_message.response_time_timer = GetTickCount();
                            current_tx_message.response_time_timer += GAME_RESPONSE_TIME;

                            current_tx_message.msg.header = MESSAGE_HEADER;
                            current_tx_message.msg.sequence = game_message_sequence;
                            current_tx_message.msg.data[current_tx_message.msg.length - MINIMUM_SERIAL_MESSAGE_SIZE] =
                                CalculateMuxCrc((BYTE*)&current_tx_message.msg, current_tx_message.msg.length - 1);
                            TxPortData((BYTE*)&current_tx_message.msg, current_tx_message.msg.length);


                	        current_tx_message.msg.command = COMMAND_HEADER;
                	        current_tx_message.msg.data[0] = ACCOUNT_INQUIRY_RETURN;
                            current_tx_message.msg.length = 67;
                            if (game_message_sequence++ > 0x7F)
                                game_message_sequence = 0;

                        }

                	    current_tx_message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                	    memcpy(&current_tx_message.msg.data[1], account_login.name, 16);
                        memcpy(&current_tx_message.msg.data[17], &account_login.rating, 2);
                        memcpy(&current_tx_message.msg.data[19], &account_login.whole_points, 4);
                        memcpy(&current_tx_message.msg.data[23], &account_login.fractional_points, 2);
                        memcpy(&current_tx_message.msg.data[25], &account_login.session_fractional_points, 2);
                        memcpy(&current_tx_message.msg.data[27], &account_login.bonus, 2);
                        memcpy(&current_tx_message.msg.data[29], &account_login.ltd_points, 4);
                        memcpy(&current_tx_message.msg.data[33], &account_login.pin_number, 2);
                        current_tx_message.msg.data[35] = account_login.status_code;
                        current_tx_message.msg.data[36] = account_login.game_card_type;

                        memcpy(&current_tx_message.msg.data[37], &account_login.primary_card_handle, 4);
                        memcpy(&current_tx_message.msg.data[41], &account_login.primary_card_games, 4);
                        memcpy(&current_tx_message.msg.data[45], &account_login.primary_card_marks, 4);
                        memcpy(&current_tx_message.msg.data[49], &account_login.secondary_card_handle, 4);
                        memcpy(&current_tx_message.msg.data[53], &account_login.secondary_card_games, 4);
                        memcpy(&current_tx_message.msg.data[57], &account_login.secondary_card_marks, 4);

                        // store account info
                        if (account_login.account_number && account_login.name[0] && !account_login.status_code)
                        {
                            account_login.pin_number = 0;   // don't store pin number for security reasons
                            memcpy(&pGameDevice->account_login_data, &account_login, sizeof(pGameDevice->account_login_data));
                            pGameDevice->account_tier_wait_timer = time(NULL) + 60; // timeout value for tier level printing
                            saveGbLoginChange(&account_login, pGameDevice->memory_game_config.ucmc_id);
                            MachineIdentifiers machine_identifiers;
                            machine_identifiers.mux_id = pGameDevice->memory_game_config.mux_id;
                            machine_identifiers.sas_id = pGameDevice->memory_game_config.sas_id;
                            machine_identifiers.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
							theApp.SendIdcLoginMessage (pGameDevice, &account_login);
                        }
                        else
                        {
                            memset(&pGameDevice->account_login_data, 0, sizeof(pGameDevice->account_login_data));
                            saveGbLoginChange(&account_login, pGameDevice->memory_game_config.ucmc_id);
                        }

                    }
                }
                break;

            // redeem Gambler's Bonus points inquiry, returned data from Stratus
            // rx: redeem points 1(ulong), redeem amount 1(short), redeem points 2(ulong), redeem amount 2(short)
            //	   redeem points 3(ulong), redeem amount 3(short), redeem error code(char)
            // tx: same as rx, change endian
            case MPI_REDEEM_INQUIRY:
            	// error check message length
                if (length < 19)
            	{
                    log_message.Format("GameCommControl::ProcessMpiMessage - MPI_REDEEM_INQUIRY invalid message length, ucmc id %u.", stratus_message.ucmc_id);
                    logMessage(log_message);
            	}
                else
                {
                    // assign values and change endian
                    for (iii=0; iii < 4; iii++)
                    {
                        current_tx_message.msg.data[iii + 1] = stratus_message.data[3 - iii];   // redeem points 1
                        current_tx_message.msg.data[iii + 7] = stratus_message.data[9 - iii];   // redeem points 2
                        current_tx_message.msg.data[iii + 13] = stratus_message.data[15 - iii]; // redeem points 3
                    }

                    // assign values and change endian
                    for (iii=0; iii < 2; iii++)
                    {
                        current_tx_message.msg.data[iii + 5] = stratus_message.data[5 - iii];   // redeem amount 1
                        current_tx_message.msg.data[iii + 11] = stratus_message.data[11 - iii]; // redeem amount 2
                        current_tx_message.msg.data[iii + 17] = stratus_message.data[17 - iii]; // redeem amount 3
                    }

                    current_tx_message.msg.data[19] = stratus_message.data[18]; // redeem error code
                    current_tx_message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                    current_tx_message.msg.length = 26;
                    current_tx_message.msg.command = COMMAND_HEADER;
                    current_tx_message.msg.data[0] = V__REDEEM_INQUIRY;

                    if (pGameDevice)
                    {
                        pGameCommControl = theApp.GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);

                        if (pGameCommControl && pGameCommControl->sas_com)
                        {
                            RedeemInquiryResponse redeem_response;

                            memset (&redeem_response, 0, sizeof(RedeemInquiryResponse));
                            memcpy (&redeem_response, &current_tx_message.msg.data[1], sizeof(RedeemInquiryResponse));

                            pGameCommControl->sas_com->non_volatile_ram.redemption_cents[0] = redeem_response.redemption_cents0;
                            pGameCommControl->sas_com->non_volatile_ram.points_redeemed[0] = redeem_response.points_redeemed0;
                            pGameCommControl->sas_com->non_volatile_ram.redemption_cents[1] = redeem_response.redemption_cents1;
                            pGameCommControl->sas_com->non_volatile_ram.points_redeemed[1] = redeem_response.points_redeemed1;
                            pGameCommControl->sas_com->non_volatile_ram.redemption_cents[2] = redeem_response.redemption_cents2;
                            pGameCommControl->sas_com->non_volatile_ram.points_redeemed[2] = redeem_response.points_redeemed2;
                        }
                    }
                }
                break;

            // redeemed Gambler's Bonus points, returned data from Stratus
            // rx: whole points(ulong), fractional points(short), redeem error code(char)
            // tx: same as rx, change endian
            case MPI_REDEEM_REQUEST:
                // error check message length
                if (length < 7)
                {
                    log_message.Format("GameCommControl::ProcessMpiMessage - MPI_REDEEM_REQUEST invalid message length, ucmc id %u.", stratus_message.ucmc_id);
                    logMessage(log_message);
                }
                else
                {
                    // whole points, change endian
                    for (iii=0; iii < 4; iii++)
                        current_tx_message.msg.data[iii + 1] = stratus_message.data[3 - iii];

                    // fractional points, change endian
                    for (iii=0; iii < 2; iii++)
                        current_tx_message.msg.data[iii + 5] = stratus_message.data[5 - iii];

                    current_tx_message.msg.data[7] = stratus_message.data[6];   // redeem error code
                    current_tx_message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                    current_tx_message.msg.length = 14;
                    current_tx_message.msg.command = COMMAND_HEADER;
                    current_tx_message.msg.data[0] = W__REDEEM_REQUEST;

                    if (pGameDevice)
                    {
                        pGameCommControl = theApp.GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);

                        if (pGameCommControl && pGameCommControl->sas_com && (stratus_message.data[6] == 0))
                        {
							switch (pGameDevice->redeem_request.choice)
                            {
                                case 1:
                                    pGameCommControl->sas_com->queueInitiateLegacyBonus (pGameCommControl->sas_com->non_volatile_ram.redemption_cents[0], TRUE);
                                    pGameCommControl->sas_com->non_volatile_ram.REDEEMED_POINTS +=
                                                                                         pGameCommControl->sas_com->non_volatile_ram.redemption_cents[0];
                                    pGameCommControl->sas_com->non_volatile_ram.whole_points -= pGameCommControl->sas_com->non_volatile_ram.points_redeemed[0];
							        pGameCommControl->sas_com->non_volatile_ram.CENTS_WON += pGameCommControl->sas_com->non_volatile_ram.redemption_cents[0];
                                    break;
                                case 2:
                                    pGameCommControl->sas_com->queueInitiateLegacyBonus (pGameCommControl->sas_com->non_volatile_ram.redemption_cents[1], TRUE);
                                    pGameCommControl->sas_com->non_volatile_ram.REDEEMED_POINTS +=
                                                                                         pGameCommControl->sas_com->non_volatile_ram.redemption_cents[1];
                                    pGameCommControl->sas_com->non_volatile_ram.whole_points -= pGameCommControl->sas_com->non_volatile_ram.points_redeemed[1];
							        pGameCommControl->sas_com->non_volatile_ram.CENTS_WON += pGameCommControl->sas_com->non_volatile_ram.redemption_cents[1];
                                    break;
                                case 3:
                                    pGameCommControl->sas_com->queueInitiateLegacyBonus (pGameCommControl->sas_com->non_volatile_ram.redemption_cents[2], TRUE);
                                    pGameCommControl->sas_com->non_volatile_ram.REDEEMED_POINTS +=
                                                                                         pGameCommControl->sas_com->non_volatile_ram.redemption_cents[2];
                                    pGameCommControl->sas_com->non_volatile_ram.whole_points -= pGameCommControl->sas_com->non_volatile_ram.points_redeemed[2];
							        pGameCommControl->sas_com->non_volatile_ram.CENTS_WON += pGameCommControl->sas_com->non_volatile_ram.redemption_cents[2];
                                    break;
                            }
                        }
                        if (pGameDevice->redeem_request.choice)
    						theApp.SendIdcRedeemRequestMessage (pGameDevice);

                        pGameDevice->redeem_request.choice = 0;
                    }
                }
                break;

            // redeemed Gambler's Bonus points, returned data from Stratus
            // rx: whole points(ulong), fractional points(short), redeem error code(char)
            // tx: same as rx, change endian
            case MPI_NEW_REDEEM_REQUEST:
                // error check message length
                if (length < sizeof(RedeemRequestResponse))
                {
                    log_message.Format("GameCommControl::ProcessMpiMessage - MPI_NEW_REDEEM_REQUEST invalid message length, ucmc id %u.", stratus_message.ucmc_id);
                    logMessage(log_message);
                }
                else
                {
                    RedeemRequestResponse redeem_request_response;
                    memcpy(&redeem_request_response, stratus_message.data, sizeof(RedeemRequestResponse));

                    endian4(redeem_request_response.new_whole_points);
                    endian2(redeem_request_response.new_fractional_points);
                    endian4(redeem_request_response.transaction_id);

                    memcpy(&current_tx_message.msg.data[1], &redeem_request_response, sizeof(RedeemRequestResponse));

                    current_tx_message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                    current_tx_message.msg.length = 7 + sizeof(RedeemRequestResponse);
                    current_tx_message.msg.command = COMMAND_HEADER;
                    current_tx_message.msg.data[0] = w__REDEEM_REQUEST;

                    if (pGameDevice)
                    {
                        pGameCommControl = theApp.GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);

                        if (pGameCommControl && pGameCommControl->sas_com && (redeem_request_response.error_code == 0) && 
                            (redeem_request_response.transaction_id == pGameDevice->redeem_request.transaction_id))
                        {
							switch (pGameDevice->redeem_request.choice)
                            {
                                case 1:
                                    pGameCommControl->sas_com->queueInitiateLegacyBonus (pGameCommControl->sas_com->non_volatile_ram.redemption_cents[0], TRUE);
                                    pGameCommControl->sas_com->non_volatile_ram.REDEEMED_POINTS +=
                                                                                         pGameCommControl->sas_com->non_volatile_ram.redemption_cents[0];
                                    pGameCommControl->sas_com->non_volatile_ram.whole_points -= pGameCommControl->sas_com->non_volatile_ram.points_redeemed[0];
							        pGameCommControl->sas_com->non_volatile_ram.CENTS_WON += pGameCommControl->sas_com->non_volatile_ram.redemption_cents[0];
                                    break;
                                case 2:
                                    pGameCommControl->sas_com->queueInitiateLegacyBonus (pGameCommControl->sas_com->non_volatile_ram.redemption_cents[1], TRUE);
                                    pGameCommControl->sas_com->non_volatile_ram.REDEEMED_POINTS +=
                                                                                         pGameCommControl->sas_com->non_volatile_ram.redemption_cents[1];
                                    pGameCommControl->sas_com->non_volatile_ram.whole_points -= pGameCommControl->sas_com->non_volatile_ram.points_redeemed[1];
							        pGameCommControl->sas_com->non_volatile_ram.CENTS_WON += pGameCommControl->sas_com->non_volatile_ram.redemption_cents[1];
                                    break;
                                case 3:
                                    pGameCommControl->sas_com->queueInitiateLegacyBonus (pGameCommControl->sas_com->non_volatile_ram.redemption_cents[2], TRUE);
                                    pGameCommControl->sas_com->non_volatile_ram.REDEEMED_POINTS +=
                                                                                         pGameCommControl->sas_com->non_volatile_ram.redemption_cents[2];
                                    pGameCommControl->sas_com->non_volatile_ram.whole_points -= pGameCommControl->sas_com->non_volatile_ram.points_redeemed[2];
							        pGameCommControl->sas_com->non_volatile_ram.CENTS_WON += pGameCommControl->sas_com->non_volatile_ram.redemption_cents[2];
                                    break;
                            }
                        }

                        if (pGameDevice->redeem_request.choice)
    						theApp.SendIdcRedeemRequestMessage (pGameDevice);

                        pGameDevice->redeem_request.choice = 0;
                    }
                }
                break;

            // redeemed Gambler's Bonus points, returned data from Stratus
            // rx: whole points(ulong), fractional points(short), redeem error code(char)
            // tx: same as rx, change endian
            case MPI_REDEEM_COMPLETE:
                // error check message length
                if (length < sizeof(RedeemCompleteResponse))
                {
                    log_message.Format("GameCommControl::ProcessMpiMessage - MPI_REDEEM_COMPLETE invalid message length, ucmc id %u.", stratus_message.ucmc_id);
                    logMessage(log_message);
                }
                else
                {
                    RedeemCompleteResponse redeem_complete_response;
                    memcpy(&redeem_complete_response, stratus_message.data, sizeof(RedeemCompleteResponse));

                    endian4(redeem_complete_response.account_number);
                    endian4(redeem_complete_response.transaction_id);
                    endian4(redeem_complete_response.whole_points_refund);

                    memcpy(&current_tx_message.msg.data[1], &redeem_complete_response, sizeof(RedeemCompleteResponse));

                    current_tx_message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                    current_tx_message.msg.length = 7 + sizeof(RedeemCompleteResponse);
                    current_tx_message.msg.command = COMMAND_HEADER;
                    current_tx_message.msg.data[0] = x__REDEEM_COMPLETE;

                    if (redeem_complete_response.error_code && pGameDevice)
                    {
                        pGameCommControl = theApp.GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);

                        if (pGameCommControl && pGameCommControl->sas_com && 
                            (redeem_complete_response.transaction_id == pGameDevice->redeem_request.transaction_id))
                        {
                            pGameCommControl->sas_com->non_volatile_ram.whole_points += redeem_complete_response.whole_points_refund;
                            if (pGameCommControl->sas_com->non_volatile_ram.REDEEMED_POINTS > redeem_complete_response.whole_points_refund)
                                pGameCommControl->sas_com->non_volatile_ram.REDEEMED_POINTS -= redeem_complete_response.whole_points_refund;
                            if (pGameCommControl->sas_com->non_volatile_ram.CENTS_WON > (redeem_complete_response.whole_points_refund / 10))
    							pGameCommControl->sas_com->non_volatile_ram.CENTS_WON -= (redeem_complete_response.whole_points_refund / 10);
                            pGameCommControl->updateGbInterfaceData();
                        }
                    }
                }
                break;

            // winning Gambler's Bonus bingo card, returned data from Stratus
            // rx: account(long), paytable amount(long), award code(char)
            // tx: same as rx, change endian
            case MPI_WINNING_BINGO_CARD:
            	ProcessWinningBingoCard (&stratus_message, length, pGameDevice);
                break;
            case MPI_VALIDATE_MEMBER_ID:
                ProcessStratusRxValidateMemberId(&stratus_message, length, pGameDevice);
                break;

        }
    }
    else if (pGameDevice && stratus_message.task_code == SMPI_TASK_PLAYER_ACCOUNTING)
    {
        switch (stratus_message.send_code)
        {

            // QUEUE MUX CARD RETURN MESSAGE
            // Account information from the Stratus to the game
            // rx: account(long), name(char[16]), rating(short), whole points(ulong), fractional points(short),
            //	   session fractional points(short), bonus(short), ltd points(long), pin number(short), status code(char),
            //	   game card type(char), primary card handle(long), primary card games(long), primary card marks(long),
            //	   secondary card handle(long), secondary card games(long), secondary card marks(long), tier level(char),
            //     unused_byte(char), Marker limit(DWORD), Marker owed(DWORD)
            // tx: minus account and change endian
            // The account number is not used.
            case MPI_ACCOUNT_LOGIN_INFO:
                // error check message length
                if (length < 73)
                {
                    log_message.Format("GameCommControl::ProcessMpiMessage - MPI_ACCOUNT_LOGIN_INFO invalid message length, ucmc id %u.", stratus_message.ucmc_id);
                    logMessage(log_message);
                }
                else
                {
                    memcpy(&account_login, stratus_message.data, sizeof(account_login));

                    endian4(account_login.account_number);
                    endian4(account_login.whole_points);
                    endian4(account_login.ltd_points);
                    endian4(account_login.primary_card_handle);
                    endian4(account_login.primary_card_games);
                    endian4(account_login.primary_card_marks);
                    endian4(account_login.secondary_card_handle);
                    endian4(account_login.secondary_card_games);
                    endian4(account_login.secondary_card_marks);
                    endian4(account_login.marker_limit);
                    endian4(account_login.marker_owed);
                    endian2(account_login.fractional_points);
                    endian2(account_login.session_fractional_points);
                    endian2(account_login.bonus);
                    endian2(account_login.pin_number);

                    if (pGameDevice)
                    {
                        pGameCommControl = theApp.GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);

                        if (pGameCommControl->sas_com)
                        {
                            if (!account_login.status_code)
                            {
                    	        memcpy(pGameCommControl->sas_com->non_volatile_ram.gb_player_name, account_login.name, 16);

                                pGameCommControl->sas_com->non_volatile_ram.whole_points = account_login.whole_points; 
                                pGameCommControl->sas_com->last_session_update_points = account_login.whole_points;
                                pGameCommControl->sas_com->non_volatile_ram.fract_points = account_login.fractional_points; 
                                pGameCommControl->sas_com->non_volatile_ram.inc_points = account_login.session_fractional_points; 
                                pGameCommControl->sas_com->non_volatile_ram.monthly_whole_points = account_login.ltd_points; 
                                pGameCommControl->sas_com->non_volatile_ram.bingo.primary.type = account_login.game_card_type; 
                                pGameCommControl->sas_com->non_volatile_ram.bingo.primary.handle_to_date = account_login.primary_card_handle; 
                                pGameCommControl->sas_com->non_volatile_ram.bingo.primary.games_to_date = account_login.primary_card_games; 
                                pGameCommControl->sas_com->non_volatile_ram.bingo.primary.card = account_login.primary_card_marks; 
                                pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.handle_to_date = account_login.secondary_card_handle; 
                                pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.games_to_date = account_login.secondary_card_games; 
                                pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.card = account_login.secondary_card_marks; 

                                if (account_login.account_number && account_login.name[0])
                                {
                                    GbSessionInfoRequest();
                                }
                            }
                        }
                        else
                        {
                	        current_tx_message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                	        memcpy(&current_tx_message.msg.data[1], account_login.name, 16);
                            memcpy(&current_tx_message.msg.data[17], &account_login.rating, 2);
                            memcpy(&current_tx_message.msg.data[19], &account_login.whole_points, 4);
                            memcpy(&current_tx_message.msg.data[23], &account_login.fractional_points, 2);
                            memcpy(&current_tx_message.msg.data[25], &account_login.session_fractional_points, 2);
                            memcpy(&current_tx_message.msg.data[27], &account_login.bonus, 2);
                            memcpy(&current_tx_message.msg.data[29], &account_login.ltd_points, 4);
                            memcpy(&current_tx_message.msg.data[33], &account_login.pin_number, 2);
                            current_tx_message.msg.data[35] = account_login.status_code;
                            current_tx_message.msg.data[36] = account_login.game_card_type;

                            memcpy(&current_tx_message.msg.data[37], &account_login.primary_card_handle, 4);
                            memcpy(&current_tx_message.msg.data[41], &account_login.primary_card_games, 4);
                            memcpy(&current_tx_message.msg.data[45], &account_login.primary_card_marks, 4);
                            memcpy(&current_tx_message.msg.data[49], &account_login.secondary_card_handle, 4);
                            memcpy(&current_tx_message.msg.data[53], &account_login.secondary_card_games, 4);
                            memcpy(&current_tx_message.msg.data[57], &account_login.secondary_card_marks, 4);

					        current_tx_message.msg.length = iii + MINIMUM_ENCRYPTED_SERIAL_MESSAGE_SIZE;
                	        current_tx_message.msg.command = ASTERISK_HEADER;
                	        current_tx_message.msg.data[0] = NEW_ACCOUNT_INQUIRY_RETURN;
                	        current_tx_message.msg.mux_id = pGameDevice->memory_game_config.mux_id;

                        }
                        // store account info
                        if (account_login.account_number && account_login.name[0] && !account_login.status_code)
                        {
                            account_login.pin_number = 0;   // don't store pin number for security reasons
                            memcpy(&pGameDevice->account_login_data, &account_login, sizeof(pGameDevice->account_login_data));
                            pGameDevice->account_tier_wait_timer = time(NULL) + 60; // timeout value for tier level printing
                            saveGbLoginChange(&account_login, pGameDevice->memory_game_config.ucmc_id);
                        }
                    }
                }
                break;

            // GB session info request, returned data from Stratus
            // rx: account(long), name(char[16]), rating(short), whole points(ulong), fractional points(short),
            //	   ltd points(long), pin number(short), status code(char), game card type(char),
            //     primary card handle(long), primary card games(long), primary card marks(long),
            //	   secondary card handle(long), secondary card games(long), secondary card marks(long), tier level(char),
            //     unused byte(char), marker_limit(long), marker_owed(long), session_whole_points(long),
            //     session_fractional_points(short), session_coin_in(long), session_coin_out(long), session_coin_drop(long)
            //     session_games(long), session_seconds(long), session jackpots(char)
            //
            // tx: same as rx, change endian
            case MPI_GB_SESSION_INFO_REQUEST:
                // error check message length
                if (length < 27)
                {
                    log_message.Format("GameCommControl::ProcessMpiMessage - MPI_GB_SESSION_INFO_REQUEST invalid message length, ucmc id %u.", stratus_message.ucmc_id);
                    logMessage(log_message);
                }
                else
                {
                    memset(&gb_session_info_data, 0, sizeof(gb_session_info_data));
                    memcpy(&gb_session_info_data, stratus_message.data, 27);

                    endian4(gb_session_info_data.session_whole_points);
                    endian2(gb_session_info_data.session_fractional_points);
                    endian4(gb_session_info_data.session_coin_in);
                    endian4(gb_session_info_data.session_coin_out);
                    endian4(gb_session_info_data.session_coin_drop);
                    endian4(gb_session_info_data.session_games);
                    endian4(gb_session_info_data.session_seconds);

                    if (pGameDevice)
                    {
                        pGameCommControl = theApp.GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);

                        if (pGameCommControl && pGameCommControl->sas_com)
                        {
                            pGameCommControl->sas_com->non_volatile_ram.ses_data.whole_points = gb_session_info_data.session_whole_points;
                            pGameCommControl->sas_com->non_volatile_ram.ses_data.fract_points = gb_session_info_data.session_fractional_points;

                            if (pGameCommControl->sas_com->non_volatile_ram.TOTAL_IN > gb_session_info_data.session_coin_in)
                                pGameCommControl->sas_com->non_volatile_ram.ses_data.cents_in =
                                    pGameCommControl->sas_com->non_volatile_ram.TOTAL_IN - (gb_session_info_data.session_coin_in * NICKEL);
                            else
                                pGameCommControl->sas_com->non_volatile_ram.ses_data.cents_in = pGameCommControl->sas_com->non_volatile_ram.TOTAL_IN;

                            
                            if (pGameCommControl->sas_com->non_volatile_ram.CENTS_WON > gb_session_info_data.session_coin_out)
                                pGameCommControl->sas_com->non_volatile_ram.ses_data.cents_out =
                                    pGameCommControl->sas_com->non_volatile_ram.CENTS_WON - (gb_session_info_data.session_coin_out * NICKEL); 
                            else
                                pGameCommControl->sas_com->non_volatile_ram.ses_data.cents_out = pGameCommControl->sas_com->non_volatile_ram.CENTS_WON; 

                            if (pGameCommControl->sas_com->non_volatile_ram.TOTAL_DROP > gb_session_info_data.session_coin_drop)
                                pGameCommControl->sas_com->non_volatile_ram.ses_data.drop_coins =
                                    pGameCommControl->sas_com->non_volatile_ram.TOTAL_DROP - (gb_session_info_data.session_coin_drop * NICKEL); 
                            else
                                pGameCommControl->sas_com->non_volatile_ram.ses_data.drop_coins = pGameCommControl->sas_com->non_volatile_ram.TOTAL_DROP; 

                            pGameCommControl->sas_com->non_volatile_ram.ses_data.games = pGameCommControl->sas_com->non_volatile_ram.GAMES_PLAYED - gb_session_info_data.session_games;
                            pGameCommControl->sas_com->non_volatile_ram.ses_data.timer = gb_session_info_data.session_seconds;

                    	 	pGameCommControl->sas_com->non_volatile_ram.session_points = 0;


                            pGameCommControl->sas_com->changeGbSystemState(ON_AVAIL);
                            updateGbInterfaceData();
                        }
                    }
                }
                break;

            // winning Gambler's Bonus bingo card, returned data from Stratus
            // rx: account(long), paytable amount(long), award code(char)
            // tx: same as rx, change endian
            case MPI_WINNING_BINGO_CARD:
            	ProcessWinningBingoCard (&stratus_message, length, pGameDevice);
                break;
        }
    }
    else
    {
        log_message.Format("GameCommControl::ProcessMpiMessage - NULL game pointer or invalid code, %u, ucmc id, %u.",
            stratus_message.task_code, stratus_message.ucmc_id);
        logMessage(log_message);
    }
}

void CGameCommControl::ProcessMpiMarkerMessage()
{
 BYTE *encrypted_data;
 int iii;
 StratusMessageFormat stratus_message = stratus_mpi_rx_marker_queue.front();
 CGameDevice *pGameDevice = GetGamePointer(stratus_message.ucmc_id);
 WORD length = stratus_message.message_length - STRATUS_HEADER_SIZE;
 CString log_message, prt_buf;
 CString filename;

    stratus_mpi_rx_marker_queue.pop();

    if (pGameDevice && stratus_message.task_code == MPI_TASK_PLAYER_ACCOUNTING)
    {
        switch (stratus_message.send_code)
        {
            case MPI_MARKER_REQUEST:
                memcpy(&marker_request, &stratus_message.data[4], sizeof(marker_request));
                endian4(marker_request.credit_limit);
                endian4(marker_request.current_balance);
                endian4(marker_request.amount_advanced);

                memcpy(&current_tx_message.msg.data[1], &marker_request, sizeof(marker_request));

                iii = 26;
				encrypted_data = theApp.EncryptMachineData(&current_tx_message.msg.data[1], &iii);
				memcpy(&current_tx_message.msg.data[1], encrypted_data, iii);

				current_tx_message.msg.length = iii + MINIMUM_ENCRYPTED_SERIAL_MESSAGE_SIZE;
                current_tx_message.msg.command = ASTERISK_HEADER;
                current_tx_message.msg.data[0] = R__REQUEST_MARKER_ADVANCE;
                current_tx_message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                break;

            case MPI_ABORT_MARKER_ADVANCE_REQUEST:
                memcpy(&abort_marker_request, &stratus_message.data[0], sizeof(abort_marker_request));
                endian4(abort_marker_request.transaction_id);
                endian4(abort_marker_request.current_balance);
                endian4(abort_marker_request.credit_limit);

                memcpy(&current_tx_message.msg.data[1], &abort_marker_request, sizeof(abort_marker_request));

                iii = 12;
				encrypted_data = theApp.EncryptMachineData(&current_tx_message.msg.data[1], &iii);
				memcpy(&current_tx_message.msg.data[1], encrypted_data, iii);

				current_tx_message.msg.length = iii + MINIMUM_ENCRYPTED_SERIAL_MESSAGE_SIZE;
                current_tx_message.msg.command = ASTERISK_HEADER;
                current_tx_message.msg.data[0] = A__ABORT_MARKER_ADVANCE;
                current_tx_message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                break;

            case MPI_MARKER_CREDIT:
            case MPI_ABORT_MARKER_CREDIT:
                memcpy(&update_marker_balance, &stratus_message.data[0], sizeof(update_marker_balance));
                endian4(update_marker_balance.current_balance);

                memcpy(&current_tx_message.msg.data[1], &update_marker_balance, sizeof(update_marker_balance));

                iii = 4;
				encrypted_data = theApp.EncryptMachineData(&current_tx_message.msg.data[1], &iii);
				memcpy(&current_tx_message.msg.data[1], encrypted_data, iii);

				current_tx_message.msg.length = iii + MINIMUM_ENCRYPTED_SERIAL_MESSAGE_SIZE;
                current_tx_message.msg.command = ASTERISK_HEADER;
                current_tx_message.msg.data[0] = U__UPDATE_MARKER_BALANCE;
                current_tx_message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                break;


        }
    }
    else
    {
        log_message.Format("GameCommControl::ProcessMpiMessage - NULL game pointer or invalid code, %u, ucmc id, %u.",
            stratus_message.task_code, stratus_message.ucmc_id);
        logMessage(log_message);
    }
}

void CGameCommControl::ProcessWinningBingoCard (StratusMessageFormat* stratus_message, WORD length, CGameDevice *pGameDevice)
{
    CString log_message, prt_buf;
    CGameCommControl *pGameCommControl;
    int iii;
    WinningCardInfo winning_card_info;


    // error check message length
    if (length < 9)
    {
        log_message.Format("GameCommControl::ProcessStratusRxWinningBingoCard - MPI_WINNING_BINGO_CARD invalid message length, ucmc id %u.", stratus_message->ucmc_id);
        logMessage(log_message);
    }
    else
    {

        if (pGameDevice)
        {
            pGameCommControl = theApp.GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);

            memcpy(&winning_card_info, stratus_message->data, sizeof(winning_card_info));

            endian4(winning_card_info.member_id);
            endian4(winning_card_info.win_amount);

            if (pGameCommControl && pGameCommControl->sas_com)
            {

                if (winning_card_info.win_amount)
                {
                    ClearBingoCardRequest(pGameDevice, winning_card_info.member_id, pGameCommControl->sas_com->sas_id);
                    pGameCommControl->sas_com->non_volatile_ram.FOUR_OK_BINGO += winning_card_info.win_amount;
                    pGameCommControl->sas_com->bonus_jackpot_award = winning_card_info.win_amount;
                    // send the cash to the game
                    pGameCommControl->sas_com->queueInitiateLegacyBonus (winning_card_info.win_amount, FALSE);
                    pGameCommControl->sas_com->non_volatile_ram.bingo_card_filled = FALSE; // reset after a hit
					if (winning_card_info.win_amount < 400000)
                    {
                        if (pGameCommControl->sas_com->non_volatile_ram.autopay_enabled)
                        {
        					if (winning_card_info.win_amount < 120000)
        						pGameCommControl->sas_com->non_volatile_ram.CENTS_WON += winning_card_info.win_amount;
                        }
                        else
    						pGameCommControl->sas_com->non_volatile_ram.CENTS_WON += winning_card_info.win_amount;
                    }

                    // update the GB interface 
                    pGameCommControl->updateGbInterfaceWithBingoWinningCardInfo();

                }
				else
				{
					log_message.Format("GameCommControl::ProcessStratusRxWinningBingoCard - MPI_WINNING_BINGO_CARD zero award value %u.", winning_card_info.win_amount);
					logMessage(log_message);
				}
            }
            else
            {
                // assign values and change endian
                for (iii=0; iii < 4; iii++)
                {
                    current_tx_message.msg.data[iii + 1] = stratus_message->data[3 - iii];   // account
                    current_tx_message.msg.data[iii + 5] = stratus_message->data[7 - iii];   // paytable amount
                }

                current_tx_message.msg.data[9] = stratus_message->data[8];   // award code
                current_tx_message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                current_tx_message.msg.length = 16;
                current_tx_message.msg.command = COMMAND_HEADER;
                current_tx_message.msg.data[0] = WINNING_BINGO_CARD;
            }

            theApp.SendIdcWinningBingoCardMessage (&winning_card_info, pGameDevice);
        }
    }

}

void CGameCommControl::ProcessStratusRxValidateMemberId (StratusMessageFormat* stratus_message, WORD length, CGameDevice *pGameDevice)
{
    CString log_message, prt_buf;

    // error check message length
    if (length < 1)
    {
        log_message.Format("GameCommControl::ProcessStratusRxValidateMemberId - MPI_VALIDATE_MEMBER_ID invalid message length, ucmc id %u.", stratus_message->ucmc_id);
        logMessage(log_message);
    }
    else
    {
        if (pGameDevice)
        {
            current_tx_message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
            current_tx_message.msg.length = 7;
            current_tx_message.msg.command = VALIDATE_MEMBER_ID;
            current_tx_message.msg.data[0] = stratus_message->data[0];   // validation code
        }
    }
}


void CGameCommControl::TxPortData(BYTE *data, BYTE length)
{
 bool display_data = false;
 int bytes_sent, last_error;
 GameDataDisplayStruct display_message;

    if (length)
    {
        if (pGameSocket)
        {
			bytes_sent = pGameSocket->Send(data, length);

			if (bytes_sent == SOCKET_ERROR)
			{
				last_error = GetLastError();

				if (last_error != WSAEINPROGRESS && last_error != WSAEWOULDBLOCK)
				{
					pGameSocket->socket_state = WSAENOTCONN;
				}
			}
			else
			{
				length = bytes_sent;
				display_data = true;
			}
        }
        else
        {
            if (hGameSerialPort != INVALID_HANDLE_VALUE)
            {
                if (!WriteFile(hGameSerialPort, data, length, (LPDWORD)&bytes_sent, NULL))
                    game_port_check = 1;
                else
                {
                    if (bytes_sent != length)
                        game_port_check = 1;
                    else
                        display_data = true;
                }
            }
        }

        if (display_data)
        {

			if (theApp.data_display.port_number)
			{
	            memset(&display_message, 0, sizeof(display_message));
	            display_message.port_number = port_info.port_number;
	            display_message.data_type = MACHINE_TRANSMIT;
	            display_message.length = length;
	            memcpy(display_message.data, data, length);
	            theApp.game_data_display_queue.push(display_message);
			}
        }
    }
}

void CGameCommControl::SendGamePollMessage()
{
 #define MAXIMUM_COLLECTION_STATE_COUNTER   5
 CString printer_data;
 CString prt_buf;

    if (pGameDeviceCurrentPollingGame)
    {
        // print account tier data
        if (pGameDeviceCurrentPollingGame->account_tier_wait_timer &&
            pGameDeviceCurrentPollingGame->account_tier_wait_timer < time(NULL) &&
            theApp.memory_settings_ini.print_account_tier_level)
        {
            printer_data.Format(" GB LOGIN\nTIER LEVEL: %c\n%s, %u(%u)\n\n",
                pGameDeviceCurrentPollingGame->account_login_data.tier_level,
                pGameDeviceCurrentPollingGame->account_login_data.name,
                pGameDeviceCurrentPollingGame->memory_game_config.ucmc_id,
                pGameDeviceCurrentPollingGame->memory_game_config.mux_id);
            theApp.printer_interrupt_queue.push(printer_data);

            pGameDeviceCurrentPollingGame->account_tier_wait_timer = 0;
        }

        current_tx_message.msg.mux_id = pGameDeviceCurrentPollingGame->memory_game_config.mux_id;

        switch (pGameDeviceCurrentPollingGame->memory_game_config.collection_state)
        {
            // arm the drop switch
            case START_COLLECTION_STATE:
                pGameDeviceCurrentPollingGame->collection_state_counter++;
                if (pGameDeviceCurrentPollingGame->collection_state_counter > MAXIMUM_COLLECTION_STATE_COUNTER)
                {
                    pGameDeviceCurrentPollingGame->SetGameCollectionState(CLEAR_COLLECTION_STATE);
                    return;
                }
                else
                {
                    current_tx_message.msg.length = 6;
                    current_tx_message.msg.command = START_COLLECTION;
                }
                break;

            // final drop meter reading
            case FORCE_COLLECTION_STATE:
                pGameDeviceCurrentPollingGame->collection_state_counter++;
                if (pGameDeviceCurrentPollingGame->collection_state_counter > MAXIMUM_COLLECTION_STATE_COUNTER)
                {
                    pGameDeviceCurrentPollingGame->SetGameCollectionState(CLEAR_COLLECTION_STATE);
                    return;
                }
                else
                {
                    current_tx_message.msg.command = REQUEST_DROP_METERS;
                    current_tx_message.msg.length = 10;
                    memcpy(&current_tx_message.msg.data[0], &pGameDeviceCurrentPollingGame->memory_game_config.ucmc_id, 4);
                }
                break;

            // meter reading
            case CANCEL_COLLECTION_STATE:
                pGameDeviceCurrentPollingGame->collection_state_counter++;
                if (pGameDeviceCurrentPollingGame->collection_state_counter > MAXIMUM_COLLECTION_STATE_COUNTER)
                {
                    pGameDeviceCurrentPollingGame->SetGameCollectionState(CLEAR_COLLECTION_STATE);
                    return;
                }
                else
                {
                    current_tx_message.msg.command = COMMAND_HEADER;
                    current_tx_message.msg.length = 7;
                    current_tx_message.msg.data[0] = REQUEST_AFTER_DROP_METERS;
                }
                break;
            case CLEAR_COLLECTION_STATE:
            case ARMED_COLLECTION_STATE:	// wait for game reply setting the drop switch
            default:
            	if (theApp.memory_settings_ini.stratus_ip_address && theApp.memory_settings_ini.stratus_tcp_port)
                {
                    if (!theApp.stratusSocket.socket_state)
                    {
                        current_tx_message.msg.command = MUX_IDLE;
                        current_tx_message.msg.length = 6;
                        if (offline_flag)
                        {
                            theApp.PrinterData("\n\nSYSTEM CONNECTION ONLINE\n");
                            theApp.PrinterData(convertTimeStamp(time(NULL)));
                            theApp.PrinterData("\n\n");
                        }
                        offline_flag = FALSE;
                    }
                    else    // send offline poll if no tcp/ip connection
                    {
					    current_tx_message.msg.command = OFFLINE_POLL;
                        current_tx_message.msg.length = 6;
                        if (!offline_flag)
                        {
                            theApp.PrinterData("\n\nSYSTEM CONNECTION OFFLINE\n");
                            theApp.PrinterData(convertTimeStamp(time(NULL)));
                            theApp.PrinterData("\n\n");
                        }
                        offline_flag = TRUE;
                    }
                }
                else
                {
                    current_tx_message.msg.command = MUX_IDLE;
                    current_tx_message.msg.length = 6;
                }
                break;
        }
    }
}

void CGameCommControl::TransmitGameMessage()
{
    memset(&current_tx_message, 0, sizeof(current_tx_message));

    // check message sequence
    if (game_message_sequence++ > 0x7F)
        game_message_sequence = 0;

    if (!stratus_waa_rx_queue.empty())
        ProcessWaaMessage();
    else
    {
        if (!stratus_mpi_rx_marker_queue.empty())
            ProcessMpiMarkerMessage();
        else if (!stratus_mpi_rx_queue.empty())
            ProcessMpiMessage();
        else
        {
            if (!broadcast_queue.empty())
            {
                current_tx_message = broadcast_queue.front();
                broadcast_queue.pop();
            }
            else
            {
                if (!pGameDeviceCurrentPollingGame || !pGameDeviceCurrentPollingGame->pNextGame)
                    pGameDeviceCurrentPollingGame = pHeadGameDevice;
                else
                    pGameDeviceCurrentPollingGame = pGameDeviceCurrentPollingGame->pNextGame;

                // if no game to poll or device polling has been stopped, skip poll
                if (!pGameDeviceCurrentPollingGame || pGameDeviceCurrentPollingGame->memory_game_config.stop_polling)
                    game_message_sequence--;
                else
                {
                    if (!pGameDeviceCurrentPollingGame->transmit_queue.empty())
                    {
                        current_tx_message = pGameDeviceCurrentPollingGame->transmit_queue.front();
                        pGameDeviceCurrentPollingGame->transmit_queue.pop();
                    }
                    else if (!pGameDeviceCurrentPollingGame->low_priority_transmit_queue.empty())
                    {
                        current_tx_message = pGameDeviceCurrentPollingGame->low_priority_transmit_queue.front();
                        pGameDeviceCurrentPollingGame->low_priority_transmit_queue.pop();
                    }
                    else
                        SendGamePollMessage();
                }
            }
        }
    }

    if (current_tx_message.msg.length)
    {
        if (current_tx_message.msg.mux_id)
        {
            current_tx_message.response_pending = true;
            current_tx_message.response_time_timer = GetTickCount();

            // allow a longer response time for this message
            if (current_tx_message.msg.command == COMMAND_HEADER && current_tx_message.msg.data[0] == GAME_CONFIGURATION_DATA)
                current_tx_message.response_time_timer += GAME_RESPONSE_TIME * 3;
            else
                current_tx_message.response_time_timer += GAME_RESPONSE_TIME;
        }

        current_tx_message.msg.header = MESSAGE_HEADER;
        current_tx_message.msg.sequence = game_message_sequence;
        current_tx_message.msg.data[current_tx_message.msg.length - MINIMUM_SERIAL_MESSAGE_SIZE] =
            CalculateMuxCrc((BYTE*)&current_tx_message.msg, current_tx_message.msg.length - 1);
        TxPortData((BYTE*)&current_tx_message.msg, current_tx_message.msg.length);
    }
}

void CGameCommControl::CheckGameResponse()
{
 DWORD invalid_data_type = 0;
 BYTE ack_message[2];
 int bytes_received = 0;
 CString error_message;
 GameDataDisplayStruct data_display;
 EventMessage event_message;
 RxSerialMessage *rx_msg;
 CGameDevice *pGameDevice;

    if (game_receive.index >= MAXIMUM_SERIAL_MESSAGE_SIZE - 1)
    {
        error_message.Format("GameCommControl::CheckGameResponse - port %u, rx buffer exceeded.", port_info.port_number);
        logMessage(error_message);
		invalid_data_type = INVALID_RX_BUFFER_OVERFLOW;
		message_errors.invalid_byte_count++;
    }
    else
    {
        if (pGameSocket)
        {
			bytes_received = pGameSocket->GetBuffer(&game_receive.data[game_receive.index],
				MAXIMUM_SERIAL_MESSAGE_SIZE - game_receive.index);

            if (!bytes_received)
				return;
			else
                game_receive.index += bytes_received;
        }
        else
        {
            if (hGameSerialPort == INVALID_HANDLE_VALUE)
            {
                invalid_data_type = INVALID_RX_INVALID_HANDLE;
                game_port_check = 1;
            }
            else
            {
                if (!ReadFile(hGameSerialPort, &game_receive.data[game_receive.index],
                    MAXIMUM_SERIAL_MESSAGE_SIZE - game_receive.index, (DWORD*)&bytes_received, NULL))
                {
                    error_message.Format("GameCommControl::CheckGameResponse - port %u, serial port error %d.",
                        port_info.port_number, GetLastError());
                    logMessage(error_message);
                    game_port_check = 1;
                    bytes_received = 0;
                    invalid_data_type = INVALID_RX_INVALID_READ;
                }
                else
                {
                    if (!bytes_received)
                        return;
                    else
                        game_receive.index += bytes_received;
                }
            }
        }
    }

    if (bytes_received > 0)
    {
        if (!invalid_data_type)
        {
            // check header
            if (game_receive.data[0] != MESSAGE_HEADER)
            {
                message_errors.header++;
                invalid_data_type = INVALID_RX_MESSAGE_HEADER;
            }
            else
            {
                // make sure we have enough bytes to determine packet length and have entire packet
                if (game_receive.index <= 3 || game_receive.data[3] > game_receive.index)
                    return;
                else
                {
                    // check CRC
                    if (CalculateMuxCrc(game_receive.data, game_receive.index - 1) != game_receive.data[game_receive.index - 1])
                    {
                        message_errors.crc++;
                        invalid_data_type = INVALID_RX_FAILED_CRC;
                    }
                    else
                    {
                        // check for valid data waiting for a reply
                        if (!current_tx_message.response_pending)
                        {
                            message_errors.no_request++;
                            invalid_data_type = INVALID_RX_UNREQUESTED_MSG;
                        }
                        else
                        {
                            rx_msg = (RxSerialMessage*)game_receive.data;

                            // check sequence
                            if (current_tx_message.msg.sequence != rx_msg->msg_id)
                            {
                                message_errors.sequence++;
                                invalid_data_type = INVALID_RX_SEQUENCE_ERROR;
                            }
                            else
                            {
                                // check mux id
                                // special offline case: drink comps, game locks, idle and ack messages,
                                // game puts CONTROLLER_MUX_ADDRESS in return message
								if (current_tx_message.msg.mux_id != rx_msg->mux_id)
								{
									if (rx_msg->mux_id != CONTROLLER_MUX_ADDRESS)
										invalid_data_type = INVALID_RX_OFFLINE_MUX_ID;
									else if (rx_msg->command != GAME_DRINK_COMP && rx_msg->command != ACK_CLEAR_TICKET_FROM_GAME &&
										rx_msg->command != GAME_LOCK_MESSAGE && rx_msg->command != IDLE_MESSAGE &&
										rx_msg->command != ACK_HEADER && rx_msg->command != JACKPOT_MESSAGE &&
										rx_msg->command != GAME_LOCK_TICKET_ADD &&
										rx_msg->command != DEVICE_RESET_MESSAGE && rx_msg->command != PERCENT_HEADER &&
										(rx_msg->command == AMPERSAND_HEADER && rx_msg->data[0] != JACKPOT_GAME_LOCK) &&
										(rx_msg->command == ASTERISK_HEADER && rx_msg->data[0] != q__LOCKED_CASHOUT_EVENT))
											invalid_data_type = INVALID_RX_OFFLINE_COMMAND;
								}

								if (!invalid_data_type)
                                {
                                    // send serial data for display
                                    if (theApp.data_display.port_number)
                                    {
                                        memset(&data_display, 0, sizeof(data_display));
                                        data_display.port_number = port_info.port_number;
                                        data_display.length = rx_msg->length;
                                        memcpy(data_display.data, game_receive.data, data_display.length);

										if (rx_msg->command == OFFLINE_POLL)
											data_display.data_type = INVALID_RX_OFFLINE_POLL;
										else if (rx_msg->command == MUX_IDLE)
											data_display.data_type = INVALID_RX_MUX_IDLE;
										else
											data_display.data_type = MACHINE_VALID_RECEIVE;

                                        theApp.game_data_display_queue.push(data_display);

                                        if (game_receive.index > rx_msg->length)
                                        {
                                            memset(&data_display, 0, sizeof(data_display));
                                            data_display.port_number = port_info.port_number;
                                            data_display.data_type = INVALID_RX_EXTRA_BYTES;
                                            data_display.length = game_receive.index - rx_msg->length;
                                            memcpy(data_display.data, &game_receive.data[rx_msg->length], data_display.length);
                                            theApp.game_data_display_queue.push(data_display);
                                        }
                                    }

                                    // send 2-byte ack message, if not idle message, offline poll or mux idle
//                                    if (rx_msg->command != IDLE_MESSAGE && rx_msg->command != OFFLINE_POLL &&
//                                        rx_msg->command != MUX_IDLE)
                                    if (rx_msg->command != IDLE_MESSAGE && rx_msg->command != OFFLINE_POLL &&
                                        rx_msg->command != MUX_IDLE &&
                                        rx_msg->command != GAME_LOCK_TICKET_ADD &&
                                        !(rx_msg->command == COMMAND_HEADER && rx_msg->data[0] == f__GB_LOGOFF) &&
                                        !(rx_msg->command == COMMAND_HEADER && rx_msg->data[0] == l__BINGO_WINNING_HAND) &&
                                        !(rx_msg->command == ASTERISK_HEADER && rx_msg->data[0] == R__REQUEST_MARKER_ADVANCE) &&
                                        !(rx_msg->command == ASTERISK_HEADER && rx_msg->data[0] == GAME_LOCK_MESSAGE) &&
                                        !(rx_msg->command == ASTERISK_HEADER && rx_msg->data[0] == T__MARKER_AFT_COMPLETE))
                                    {
                                        ack_message[0] = ACK_HEADER;        // header
                                        ack_message[1] = rx_msg->msg_id;    // sequence
                                        TxPortData(ack_message, 2);
                                    }

                                    if (rx_msg->mux_id != current_tx_message.msg.mux_id)
                                        rx_msg->mux_id = current_tx_message.msg.mux_id;

                                    if (ProcessValidSerialMessage(rx_msg, port_info.port_number))
                                    {
                                        memset(&event_message, 0, sizeof(event_message));
                                        event_message.head.time_stamp = time(NULL);
                                        event_message.head.event_type = CONTROLLER_RX;
                                        event_message.head.data_length = rx_msg->length;
                                        event_message.head.port_number = port_info.port_number;
                                        memcpy(event_message.data, game_receive.data, rx_msg->length);

                                        pGameDevice = GetGamePointer(rx_msg->mux_id);
                                        if (pGameDevice)
                                            event_message.head.customer_id = pGameDevice->memory_game_config.customer_id;

                                        theApp.event_message_queue.push(event_message);
                                    }

                                    current_tx_message.response_pending = false;    // rx message okay, so reset response pending flag
                                }
                                else
                                    message_errors.mux_id++;
                            }
                        }
                    }
                }
            }
        }

        if (invalid_data_type)
        {
            if (theApp.data_display.port_number)
            {
                memset(&data_display, 0, sizeof(data_display));
                data_display.port_number = port_info.port_number;
                data_display.data_type = invalid_data_type;
                data_display.length = (BYTE)game_receive.index;
                memcpy(data_display.data, game_receive.data, data_display.length);
                theApp.game_data_display_queue.push(data_display);
            }
        }
    }

    memset(&game_receive, 0, sizeof(game_receive));
}

// process valid received serial messages, return true for message event logging
bool CGameCommControl::ProcessValidSerialMessage(RxSerialMessage *rx_msg, WORD port_number)
{
 bool event_log_message = true;
 MemoryTicketAdd memory_ticket_add;
 CGameDevice *pGameDevice = NULL;
 CString error_message;
 char manufacturer_id[2];
 TxSerialMessage msg;
 IdcBillAcceptedMessage* bill_data;
 DWORD transaction_id;
 DWORD current_credits;

    switch (rx_msg->command)
    {
        case RETURN_SMI_METERS:
        case GAMES_DENOM_METERS:
        case LUCKY_POKER_METERS:
        case GAME_HANDPAY:
            break;

        case ACK_CLEAR_TICKET_FROM_GAME:
        case ACK_HEADER:
            ProcessRxAck();
            event_log_message = false;
			break;

        case GAME_LOCK_MESSAGE:
            ProcessGameLockMessage(rx_msg, port_number);
			break;

        case GAME_LOCK_TICKET_ADD:
            ProcessGameLockMessage(rx_msg, port_number);
            break;

        case GAME_DRINK_COMP:
            processGameDrinkComp(rx_msg);
			break;

        case GAME_COLLECTED:
            GameCollected(rx_msg);
			break;

		case GAME_METERS1:
            event_log_message = processGameMeters(rx_msg->mux_id, rx_msg->data,
                rx_msg->length - MINIMUM_SERIAL_MESSAGE_SIZE, MMI_UPDATE_GAME_METERS);
			break;

		case IDLE_MESSAGE:
            event_log_message = IdleMessage(rx_msg);
            break;

		case JACKPOT_MESSAGE:
			ProcessJackpotMessage(rx_msg);
			break;

        case MACHINE_STATUS_MESSAGE:
            StatusNotificationMessage(rx_msg);
			break;

		case DEVICE_RESET_MESSAGE:
			theApp.GameDateAndTimeUpdate();
            DeviceResetMessage(rx_msg);
			break;

		case TICKET_ADD:
            if (rx_msg->length - MINIMUM_SERIAL_MESSAGE_SIZE < sizeof(memory_ticket_add))
            {
                memset(&memory_ticket_add, 0, sizeof(memory_ticket_add));
                memcpy(&memory_ticket_add, rx_msg->data, rx_msg->length - MINIMUM_SERIAL_MESSAGE_SIZE);
            }
            else
                memcpy(&memory_ticket_add, rx_msg->data, sizeof(memory_ticket_add));

            ProcessTicketAdd(rx_msg, rx_msg->mux_id, &memory_ticket_add, port_number);
			break;

        case VALIDATE_MEMBER_ID:
            ProcessValidateMemberIdMessage(rx_msg);
            break;

        case VALIDATE_QUICK_ENROLL_NUMBER:
            ProcessValidateQuickEnrollNumberMessage(rx_msg);
            break;

		case COMMAND_HEADER:
			switch (rx_msg->data[0])
			{
                case DENOM_METERS_OR_DOOR_EVENT:
				case IGT_GAME_MESSAGE:
				case SDS_GAME_MESSAGE:
                    break;

				case GAME_METERS2:
                case SEND_CURRENT_METERS:
                    event_log_message = processGameMeters(rx_msg->mux_id, &rx_msg->data[1],
					    rx_msg->length - MINIMUM_SERIAL_MESSAGE_SIZE - 1, MMI_UPDATE_GAME_METERS);
					break;

                case SMART_CARD_READER_STATE:
                    ProcessSmartCardReaderState(rx_msg);
                    break;

				case ACCOUNT_INQUIRY:
					AccountInquiry(false, rx_msg);
        			break;

				case WINNING_BINGO_HAND:
                case l__BINGO_WINNING_HAND:
					WinningBingoHand(rx_msg);
        			break;

				case WINNING_BINGO_CARD:
					WinningBingoCard(rx_msg);
                    break;

				case CLEAR_BINGO_CARD_REQUEST:
                case n__NEW_CLEAR_BINGO_CARD_REQUEST:
					ClearBingoCardRequest(rx_msg);
        			break;

				case ACCOUNT_LOGOUT:
                case f__GB_LOGOFF:
					event_log_message = ProcessAccountLogout(rx_msg);
                    break;

				case V__REDEEM_INQUIRY:
					redeemInquiry(rx_msg);
        			break;

				case W__REDEEM_REQUEST:
                case w__REDEEM_REQUEST:
					redeemRequest(rx_msg);
        			break;

                case x__REDEEM_COMPLETE:
					redeemComplete(rx_msg);
        			break;

				// frenzy awards have been discontinued
				case WINNING_WIDE_AREA_AWARD:
					event_log_message = false;
					// WinningWideAreaAward(rx_msg);
        			break;

				// frenzy awards have been discontinued
				case REQUEST_WIDE_AREA_AWARD:
                    event_log_message = false;
					break;

                default:
                    event_log_message = false;
                    break;
			}
			break;

		// frenzy awards have been discontinued
		case CASH_FRENZY_HEADER:
			event_log_message = false;
#if 0
			switch (rx_msg->data[0])
			{
				case RECEIVE_CASH_FRENZY_HIT:
					ProcessCashFrenzyHit(rx_msg);
					break;

				case REQUEST_CASH_FRENZY_AWARD:
					event_log_message = false;
					break;

				default:
                    event_log_message = false;
                    break;
			}
#endif
			break;

        case POUND_HEADER:
            switch (rx_msg->data[0])
            {
                case BILL_METERS:
                    break;

                default:
                    event_log_message = false;
                    break;
            }
            break;

        case PERCENT_HEADER:
			switch (rx_msg->data[0])
			{
				case CARD_DATA:
					event_log_message = false;
                    if (theApp.pGbProManager)
                    {
                        pGameDevice = GetGamePointer((BYTE)rx_msg->mux_id);
                        processGameOldCardData(rx_msg, pGameDevice);
                    }
					break;

				case NEW_CARD_DATA:
					event_log_message = false;
                    if (theApp.pGbProManager)
                    {
                        pGameDevice = GetGamePointer((BYTE)rx_msg->mux_id);
                        processGameNewCardData(rx_msg, pGameDevice);
                    }
					break;

				case GAME_ENTERED:
                    memset(manufacturer_id, 0, 2);
                    pGameDevice = GetGamePointer((BYTE)rx_msg->mux_id);
                    theApp.SendIdcGameEnteredMessage (pGameDevice, (char*)&rx_msg->data[2], (char*)&manufacturer_id);
					break;

				case NEW_GAME_ENTERED:
                    memset(manufacturer_id, 0, 2);
                    pGameDevice = GetGamePointer((BYTE)rx_msg->mux_id);
                    theApp.SendIdcGameEnteredMessage (pGameDevice, (char*)&rx_msg->data[2], (char*)&manufacturer_id);
                    // send ack back to gaming devices
                    memset(&msg, 0, sizeof(msg));
                    msg.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                    msg.msg.length = 7;
                    msg.msg.command = PERCENT_HEADER;
                    msg.msg.data[0] = NEW_GAME_ENTERED;
                    pGameDevice->transmit_queue.push(msg);

					break;

                case BILL_ACCEPTED:
                    bill_data = (IdcBillAcceptedMessage*)&rx_msg->data[1];
                    pGameDevice = GetGamePointer((BYTE)rx_msg->mux_id);
                    theApp.SendIdcBillAcceptedMessage (pGameDevice, bill_data);

					transaction_id = bill_data->transaction_id;
                    // send ack back to gaming devices
                    memset(&msg, 0, sizeof(msg));
                    msg.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                    msg.msg.length = 11;
                    msg.msg.command = PERCENT_HEADER;
                    msg.msg.data[0] = BILL_ACCEPTED;
                    memcpy(&msg.msg.data[1], &transaction_id, sizeof(transaction_id));
                    pGameDevice->transmit_queue.push(msg);
					break;

                case c__CURRENT_CREDITS:
                	memcpy(&current_credits, (DWORD*)&rx_msg->data[1], sizeof(DWORD));
                    pGameDevice = GetGamePointer((BYTE)rx_msg->mux_id);
                    theApp.SendIdcCurrentCreditsMessage (pGameDevice, &current_credits);
                    // send ack back to gaming devices
                    memset(&msg, 0, sizeof(msg));
                    msg.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                    msg.msg.length = 7;
                    msg.msg.command = PERCENT_HEADER;
                    msg.msg.data[0] = c__CURRENT_CREDITS;
                    pGameDevice->transmit_queue.push(msg);

					break;

				default:
					event_log_message = false;
					break;
			}
            break;

		case AMPERSAND_HEADER:
            switch (rx_msg->data[0])
            {
                case JACKPOT_GAME_LOCK:
                    ProcessGameLockMessage(rx_msg, port_number);
                    break;

                default:
                    event_log_message = false;
                    break;
            }
            break;

        case ASTERISK_HEADER:
            if (theApp.memory_settings_ini.stratus_tcp_port & 0x01)
            {
			    // decrypt message
		  	    memcpy(&rx_msg->data[1], theApp.DecryptMachineData(&rx_msg->data[1], rx_msg->length - MINIMUM_ENCRYPTED_SERIAL_MESSAGE_SIZE),
	   			    rx_msg->length - MINIMUM_ENCRYPTED_SERIAL_MESSAGE_SIZE);

                switch (rx_msg->data[0])
                {
                    case C__REQUEST_CUSTOMER_DATA:
					    AccountInquiry(true, rx_msg);
                        break;

                    case q__LOCKED_CASHOUT_EVENT:
                        ProcessGameLockMessage(rx_msg, port_number);
                        break;

                    case R__REQUEST_MARKER_ADVANCE:
                        ProcessMarkerAdvanceRequest(rx_msg);
                        break;

                    case A__ABORT_MARKER_ADVANCE:
                        ProcessAbortMarkerAdvanceRequest(rx_msg);
                        break;

                    case T__MARKER_AFT_COMPLETE:
                        ProcessMarkerAftCompleteMessage(rx_msg);
                        break;
                    default:
                        event_log_message = false;
                        break;
                }
            }
            else
            {
				error_message = "CGameCommControl::ProcessValidSerialMessage() - game sending encrypted message when connected to non-encrypted port.\n";
				logMessage(error_message);
            }

            break;

        case CARET_HEADER:
            switch (rx_msg->data[0])
            {
                case INITIALIZE_GB_INTERFACE:
                    ProcessInitializeGbInterface(rx_msg);
                    break;

				case VIEW_CARDS_INFO_REQUEST:
                    ProcessViewCardsInfoRequest(rx_msg);
        			break;

				case SAS_COMMUNICATION_STATUS:
                    ProcessSasCommunicationsStatusRequest(rx_msg);
					break;

                default:
                    event_log_message = false;
                    break;
            }
            break;

        default:
            event_log_message = false;
            break;
    }

 return event_log_message;
}

void CGameCommControl::ProcessNoGameResponse()
{
 CGameDevice *pGameDevice;
 EventMessage event_message;

    // increment the no response error counter
    message_errors.no_response++;

    // re-queue mux message if no response received. Don't re-queue idle or offline poll messages
    if (current_tx_message.msg.command != MUX_IDLE && current_tx_message.msg.command != OFFLINE_POLL)
    {
        pGameDevice = GetGamePointer(current_tx_message.msg.mux_id);

        // if retry < 2, re-queue message
        if (current_tx_message.retry < 2)
        {
            current_tx_message.retry++;

            if (pGameDevice)
            {
                if ((current_tx_message.msg.command == CARET_HEADER) && (current_tx_message.msg.data[0] == UPDATE_GB_INTERFACE))
                    pGameDevice->low_priority_transmit_queue.push(current_tx_message);
                else
                    pGameDevice->transmit_queue.push(current_tx_message);
            }
        }
        else    // else dump message
        {
            memset(&event_message, 0, sizeof(event_message));

            switch (current_tx_message.msg.command)
            {
                case CLEAR_TICKET_FROM_GAME:
                case START_COLLECTION:
                case REQUEST_DROP_METERS:
                case UNLOCK_GAME:
                    event_message.head.data_length = current_tx_message.msg.length;
                    break;

                case COMMAND_HEADER:
                    switch (current_tx_message.msg.data[0])
                    {
                        case ACCOUNT_INQUIRY_RETURN:
                        case WINNING_BINGO_CARD:
                        case WINNING_WIDE_AREA_AWARD:
                        case V__REDEEM_INQUIRY:
                        case W__REDEEM_REQUEST:
                        case w__REDEEM_REQUEST:
                        case x__REDEEM_COMPLETE:
                        case REQUEST_AFTER_DROP_METERS:
                        case GAME_CONFIGURATION_DATA:
                        case RESET_MUX:
                            event_message.head.data_length = current_tx_message.msg.length;
                            break;
                    }
                    break;

				case CASH_FRENZY_HEADER:
					if (current_tx_message.msg.data[0] == RECEIVE_CASH_FRENZY_HIT)
						event_message.head.data_length = current_tx_message.msg.length;
					break;

                case ASTERISK_HEADER:
					event_message.head.data_length = current_tx_message.msg.length;
					break;
            }

            if (event_message.head.data_length)
            {
                event_message.head.time_stamp = time(NULL);
                event_message.head.event_type = CONTROLLER_TX_NO_ACK;
                event_message.head.port_number = port_info.port_number;

                if (pGameDevice)
                    event_message.head.customer_id = pGameDevice->memory_game_config.customer_id;

                if (event_message.head.data_length > MAXIMUM_SERIAL_MESSAGE_SIZE)
                    memcpy(event_message.data, &current_tx_message.msg, MAXIMUM_SERIAL_MESSAGE_SIZE);
                else
                    memcpy(event_message.data, &current_tx_message.msg, event_message.head.data_length);

                theApp.event_message_queue.push(event_message);
            }

			memset(current_tx_message.msg.data, 0, sizeof(MAXIMUM_SERIAL_MESSAGE_SIZE - MINIMUM_SERIAL_MESSAGE_SIZE)); 
        }
    }

    current_tx_message.response_pending = false;
}

void CGameCommControl::saveDropMeterChange(GameMeters* game_meters, DWORD ucmc_id)
{
    CString filename, data_string;
    StoredDropGameMeters stored_drop_game_meters;

    filename.Format("%s%u", STORED_DROP_METERS, ucmc_id);

    memset(stored_drop_game_meters.time_date_stamp, 0, sizeof(stored_drop_game_meters.time_date_stamp));
    strcpy(stored_drop_game_meters.time_date_stamp, convertTimeStamp(time(NULL)));

    stored_drop_game_meters.coin_in = game_meters->coin_in;
    stored_drop_game_meters.coin_out = game_meters->coin_out;
    stored_drop_game_meters.games = game_meters->games;
    stored_drop_game_meters.coin_drop = game_meters->coin_drop;
    stored_drop_game_meters.hand_pay = game_meters->hand_pay;
    stored_drop_game_meters.total_dollars = game_meters->total_dollars;
    stored_drop_game_meters.jackpot = game_meters->jackpot;

	fileWrite(filename.GetBuffer(0), -1, &stored_drop_game_meters, sizeof(StoredDropGameMeters));

    // purge all records except the newest 10
    while (fileRecordTotal(filename.GetBuffer(0), sizeof(StoredDropGameMeters)) > 10)
        fileDeleteRecord(filename.GetBuffer(0), sizeof(StoredDropGameMeters), 0);

}

void CGameCommControl::saveGbLoginChange(AccountLogin* account_login, DWORD ucmc_id)
{
    CString filename;
    GbLoginData gb_login_data;

    filename.Format("%s%u", LOGIN_DATA, ucmc_id);

    if (account_login->status_code)
    {
        DeleteFile(filename.GetBuffer(0));
    }
    else
    {
        memset(&gb_login_data, 0, sizeof(GbLoginData));

        strcpy(gb_login_data.time_date_stamp, convertTimeStamp(time(NULL)));

        memcpy(gb_login_data.name, account_login->name, 16);
        gb_login_data.rating = account_login->rating;
        gb_login_data.tier_level = account_login->tier_level;

	    fileWrite(filename.GetBuffer(0), 0, &gb_login_data, sizeof(GbLoginData));
    }
}

bool CGameCommControl::processGameMeters(BYTE mux_id, BYTE *meter_data, BYTE meter_length, WORD update_flag)
{
 bool event_log_message = false;
 CString filename;
 GameMeters game_meters, zero_meters;
 StratusMessageFormat stratus_msg;
 CGameDevice *pGameDevice = GetGamePointer(mux_id);

    if (!pGameDevice)
        logMessage("GameCommControl::processGameMeters - NULL game pointer");
    else
    {
        memset(&game_meters, 0, sizeof(game_meters));

        if (meter_length > sizeof(game_meters))
            memcpy(&game_meters, meter_data, sizeof(game_meters));
        else
            memcpy(&game_meters, meter_data, meter_length);


        if (!game_meters.coin_in && !game_meters.coin_out && !game_meters.coin_drop && !game_meters.hand_pay)
            pGameDevice->all_zero_meter_read = TRUE;
        else
        {
            if (pGameDevice->all_zero_meter_read &&
                (pGameDevice->stored_game_meters.current_game_meters.coin_in < game_meters.coin_in) &&
                (pGameDevice->stored_game_meters.current_game_meters.coin_out < game_meters.coin_out) &&
                (pGameDevice->stored_game_meters.current_game_meters.coin_drop < game_meters.coin_drop) &&
                (pGameDevice->stored_game_meters.current_game_meters.hand_pay < game_meters.hand_pay))
                {
                    // send IDC zero meters so they know there was a RAM clear
                    memset(&zero_meters, 0, sizeof(zero_meters));
            		theApp.SendIdcGameMeterMessage (pGameDevice, &zero_meters);
                }
                pGameDevice->all_zero_meter_read = FALSE;

                MachineIdentifiers machine_identifiers;
                machine_identifiers.mux_id = pGameDevice->memory_game_config.mux_id;
                machine_identifiers.sas_id = pGameDevice->memory_game_config.sas_id;
                machine_identifiers.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
		        theApp.SendIdcGameMeterMessage (pGameDevice, &game_meters);

        }

        // log meter message if change or collection
        if (update_flag == MMI_GAME_COLLECTED_METERS ||
            pGameDevice->stored_game_meters.current_game_meters.coin_drop != game_meters.coin_drop ||
            pGameDevice->stored_game_meters.current_game_meters.hand_pay != game_meters.hand_pay ||
            pGameDevice->stored_game_meters.current_game_meters.gb_pts != game_meters.gb_pts ||
            pGameDevice->stored_game_meters.current_game_meters.jackpot != game_meters.jackpot ||
            pGameDevice->stored_game_meters.current_game_meters.payline_progressive_games_played != game_meters.payline_progressive_games_played ||
            pGameDevice->stored_game_meters.current_game_meters.payline_progressive_cents_played != game_meters.payline_progressive_cents_played ||
            pGameDevice->stored_game_meters.current_game_meters.payline_progressive_cents_won != game_meters.payline_progressive_cents_won ||
            pGameDevice->stored_game_meters.current_game_meters.wat_in != game_meters.wat_in ||
            pGameDevice->stored_game_meters.current_game_meters.wat_out != game_meters.wat_out)

        {
            event_log_message = true;
            // if there was a bill in, save the meter change data for GB Trax
            if (pGameDevice->stored_game_meters.current_game_meters.coin_drop != game_meters.coin_drop)
                saveDropMeterChange(&game_meters, pGameDevice->memory_game_config.ucmc_id);
            filename.Format("%s%u", STORED_METERS, mux_id);
            memcpy(&pGameDevice->stored_game_meters.current_game_meters, &game_meters, sizeof(game_meters));
            fileWrite(filename.GetBuffer(0), 0, &pGameDevice->stored_game_meters, sizeof(pGameDevice->stored_game_meters));
        }
        else
            memcpy(&pGameDevice->stored_game_meters.current_game_meters, &game_meters, sizeof(game_meters));


        if (!theApp.stratusSocket.socket_state && ((update_flag == MMI_UPDATE_GAME_METERS) || (update_flag == MMI_GAME_COLLECTED_METERS)))
        {
            endian4(game_meters.coin_in);
            endian4(game_meters.coin_out);
            endian4(game_meters.games);
            endian4(game_meters.coin_drop);
            endian4(game_meters.hand_pay);
            endian4(game_meters.gb_pts);
            endian4(game_meters.total_dollars);
            endian4(game_meters.jackpot);
            endian4(game_meters.payline_progressive_games_played);
            endian4(game_meters.payline_progressive_cents_played);
            endian4(game_meters.payline_progressive_cents_won);
            endian4(game_meters.wat_in);
            endian4(game_meters.wat_out);

            memset(&stratus_msg, 0, sizeof(stratus_msg));
            memcpy(stratus_msg.data, &game_meters, 24); // 1st 6 meter fields
            memcpy(&stratus_msg.data[24], SetTimeStamp(), 14);  // timestamp
            // stratus_msg.data[38];    // 2-byte filler
            memcpy(&stratus_msg.data[40], &game_meters.total_dollars, 28);  // last meter fields

            stratus_msg.message_length = 68 + STRATUS_HEADER_SIZE;
            stratus_msg.send_code = update_flag;

            if (update_flag == MMI_GAME_COLLECTED_METERS)
                stratus_msg.send_code = MMI_GAME_COLLECTED_METERS;

            stratus_msg.task_code = MMI_TASK_GAME_METERS;
            stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
            stratus_msg.mux_id = mux_id;
            stratus_msg.property_id = theApp.memory_location_config.property_id;
            stratus_msg.serial_port = port_info.port_number;
            stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;

		    if (pGameDevice->memory_game_config.club_code)
			    stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		    else
			    stratus_msg.club_code = theApp.memory_location_config.club_code;

            theApp.QueueStratusTxMessage(&stratus_msg, FALSE);
        }
    }

 return event_log_message;
}

//bool CGameCommControl::processGameMeters(BYTE *meter_data, BYTE meter_length, WORD update_flag)
bool CGameCommControl::processGameMeters(WORD update_flag)
{
 bool event_log_message = false;
 CString filename;
 GameMeters game_meters, zero_meters;
 StratusMessageFormat stratus_msg;
 CGameDevice *pGameDevice = 0;

    if (pHeadGameDevice)
        pGameDevice = pHeadGameDevice;

    if (!pGameDevice)
        logMessage("GameCommControl::processGameMeters - NULL game pointer");
    else
    {
        memset(&game_meters, 0, sizeof(game_meters));

        game_meters.coin_in = sas_com->non_volatile_ram.TOTAL_IN / NICKEL;
        game_meters.coin_out = sas_com->non_volatile_ram.TOTAL_OUT / NICKEL;
        game_meters.games = sas_com->non_volatile_ram.GAMES_PLAYED;
        game_meters.coin_drop = sas_com->non_volatile_ram.TOTAL_DROP / NICKEL;
        game_meters.hand_pay = sas_com->non_volatile_ram.TOTAL_ATTEN / NICKEL;
        game_meters.jackpot = (sas_com->non_volatile_ram.TOTAL_ATTEN - sas_com->non_volatile_ram.TOTAL_HAND_PAID_CANCELLED_CREDITS) / NICKEL;
        game_meters.total_dollars = sas_com->non_volatile_ram.TOTAL_BILLS;
		game_meters.gb_pts = sas_com->non_volatile_ram.REDEEMED_POINTS + sas_com->non_volatile_ram.FOUR_OK_BINGO;

        if (!game_meters.coin_in && !game_meters.coin_out && !game_meters.coin_drop && !game_meters.hand_pay)
           pGameDevice->all_zero_meter_read = TRUE;
        else
        {
            if (pGameDevice->all_zero_meter_read &&
                (pGameDevice->stored_game_meters.current_game_meters.coin_in < game_meters.coin_in) &&
                (pGameDevice->stored_game_meters.current_game_meters.coin_out < game_meters.coin_out) &&
                (pGameDevice->stored_game_meters.current_game_meters.coin_drop < game_meters.coin_drop) &&
                (pGameDevice->stored_game_meters.current_game_meters.hand_pay < game_meters.hand_pay))
                {
                    // send IDC zero meters so they know there was a RAM clear
                    memset(&zero_meters, 0, sizeof(zero_meters));
            		theApp.SendIdcGameMeterMessage (pGameDevice, &zero_meters);
                }
                pGameDevice->all_zero_meter_read = FALSE;

                MachineIdentifiers machine_identifiers;
                machine_identifiers.mux_id = pGameDevice->memory_game_config.mux_id;
                machine_identifiers.sas_id = pGameDevice->memory_game_config.sas_id;
                machine_identifiers.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
		        theApp.SendIdcGameMeterMessage (pGameDevice, &game_meters);

        }

        // log meter message if change or collection
        if (update_flag == MMI_GAME_COLLECTED_METERS ||
            pGameDevice->stored_game_meters.current_game_meters.coin_drop != game_meters.coin_drop ||
            pGameDevice->stored_game_meters.current_game_meters.hand_pay != game_meters.hand_pay ||
            pGameDevice->stored_game_meters.current_game_meters.gb_pts != game_meters.gb_pts ||
            pGameDevice->stored_game_meters.current_game_meters.jackpot != game_meters.jackpot)
        {
            event_log_message = true;

            // if there was a bill in, save the meter change data for GB Trax
            if (pGameDevice->stored_game_meters.current_game_meters.coin_drop != game_meters.coin_drop)
                saveDropMeterChange(&game_meters, pGameDevice->memory_game_config.ucmc_id);

            // update the stored meter data on disk
            filename.Format("%s%u", STORED_METERS, pGameDevice->memory_game_config.ucmc_id);
            memcpy(&pGameDevice->stored_game_meters.current_game_meters, &game_meters, sizeof(game_meters));
            fileWrite(filename.GetBuffer(0), 0, &pGameDevice->stored_game_meters, sizeof(pGameDevice->stored_game_meters));
        }
        else
        {
            memcpy(&pGameDevice->stored_game_meters.current_game_meters, &game_meters, sizeof(game_meters));
        }

        if (!theApp.stratusSocket.socket_state && ((update_flag == MMI_UPDATE_GAME_METERS) || (update_flag == MMI_GAME_COLLECTED_METERS)))
        {
            endian4(game_meters.coin_in);
            endian4(game_meters.coin_out);
            endian4(game_meters.games);
            endian4(game_meters.coin_drop);
            endian4(game_meters.hand_pay);
            endian4(game_meters.gb_pts);
            endian4(game_meters.total_dollars);
            endian4(game_meters.jackpot);
            endian4(game_meters.payline_progressive_games_played);
            endian4(game_meters.payline_progressive_cents_played);
            endian4(game_meters.payline_progressive_cents_won);
            endian4(game_meters.wat_in);
            endian4(game_meters.wat_out);

            memset(&stratus_msg, 0, sizeof(stratus_msg));
            memcpy(stratus_msg.data, &game_meters, 24); // 1st 6 meter fields
            memcpy(&stratus_msg.data[24], SetTimeStamp(), 14);  // timestamp
            // stratus_msg.data[38];    // 2-byte filler
            memcpy(&stratus_msg.data[40], &game_meters.total_dollars, 28);  // last meter fields

            stratus_msg.message_length = 68 + STRATUS_HEADER_SIZE;
            stratus_msg.send_code = update_flag;

            if (update_flag == MMI_GAME_COLLECTED_METERS)
                stratus_msg.send_code = MMI_GAME_COLLECTED_METERS;

            stratus_msg.task_code = MMI_TASK_GAME_METERS;
            stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
            stratus_msg.mux_id = 0;
            stratus_msg.property_id = theApp.memory_location_config.property_id;
            stratus_msg.serial_port = port_info.port_number;
            stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;

		    if (pGameDevice->memory_game_config.club_code)
			    stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		    else
			    stratus_msg.club_code = theApp.memory_location_config.club_code;

            theApp.QueueStratusTxMessage(&stratus_msg, FALSE);
        }
    }

 return event_log_message;
}

void CGameCommControl::processCurrentCreditsMeter (ULONG current_credits)
{
 CGameDevice *pGameDevice = 0;

    if (pHeadGameDevice)
        pGameDevice = pHeadGameDevice;

    if (!pGameDevice)
        logMessage("GameCommControl::processCurrentCreditsMeter - NULL game pointer");
    else
    {
        theApp.SendIdcCurrentCreditsMessage (pGameDevice, &current_credits);
    }
}

void CGameCommControl::processBillAcceptedEvent (ULONG bill_denomination_in_cents)
{
 CGameDevice *pGameDevice = 0;
 IdcBillAcceptedMessage bill_accepted_message;

    if (pHeadGameDevice)
        pGameDevice = pHeadGameDevice;

    if (!pGameDevice)
        logMessage("GameCommControl::processBillAcceptedEvent - NULL game pointer");
    else
    {
        bill_accepted_message.denomination_in_cents = bill_denomination_in_cents;
        // generate a transaction_id
        rand_s((unsigned int*)&bill_accepted_message.transaction_id);
        theApp.SendIdcBillAcceptedMessage (pGameDevice, &bill_accepted_message);
    }
}


// device status bits
// bit 1: GAME MALFUNCTION/MALFUNCTION RESET
// bit 2: CHANGE ON/CHANGE OFF
// bit 3: GAME DOOR OPEN/GAME DOOR CLOSED
// bit 4: DROP DOOR OPEN/DROP DOOR CLOSED
// bit 5: GAME DEAD/GAME ALIVE
// bit 6: not used
// bit 7: collection in progress flag
// bit 8: not used
// bit 9: not used
// bit 10: not used
// bit 11: not used
// bit 12: malfunctioning smart card reader
// bit 13: abandoned card in smart card reader
// bit 14: not used
// bit 15: not used
// bit 16: not used
bool CGameCommControl::IdleMessage(RxSerialMessage *rx_msg)
{
 bool event_log_message = false;
 WORD changed_status_bits, received_status_bits;
 CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
 BYTE pulled = 0;

    if (!pGameDevice)
        logMessage("GameCommControl::IdleMessage - NULL game pointer.");
    else
    {
//        memcpy((BYTE*)&received_status_bits, (BYTE*)rx_msg->data, 2);
		received_status_bits = (rx_msg->data[0] << 8) + rx_msg->data[1];

        rx_msg->length++;   // increase message length by one for event logging comparison
    	changed_status_bits = pGameDevice->idle_msg_status_bits ^ received_status_bits;
        memcpy((BYTE*)&rx_msg->data[2], (BYTE*)&changed_status_bits, 2);  // replace CRC and next byte for event logging comparison

        if (changed_status_bits)
		{
    		pGameDevice->idle_msg_status_bits = received_status_bits;

    		if (changed_status_bits & STACKER_REMOVED)
			{

				if (received_status_bits & STACKER_REMOVED)
					pulled = 1;
				theApp.SendIdcStackerPullMessage (pGameDevice, pulled);
			}

			if (((received_status_bits & COLLECT_IN_PROG) ||
				 (pGameDevice->memory_game_config.collection_state == ARMED_COLLECTION_STATE)) && pulled)
                pGameDevice->SetGameCollectionState(FORCE_COLLECTION_STATE);

		}

    	if (changed_status_bits & GAME_MALFUNCTION || changed_status_bits & CHANGE ||
            changed_status_bits & GAME_DOOR || changed_status_bits & GAME_STATE ||
            changed_status_bits & PLAYER_LOGGED_IN || changed_status_bits & MALFUNCTIONING_SMART_CARD_READER ||
    	    changed_status_bits & ABANDONED_SMART_CARD_IN_READER || changed_status_bits & DROP_DOOR ||
			changed_status_bits & STACKER_REMOVED)
                event_log_message = true;

    	if (changed_status_bits & COLLECT_IN_PROG)
        {
            event_log_message = true;
        }
    }

 return event_log_message;
}

// rx: account(DWORD), win code(DWORD), hours(BYTE), minutes(BYTE),
//	   seconds(BYTE), day(BYTE), month(BYTE), year(BYTE)
// tx: same as rx, but change endian and for BYTE's convert from bcd to ascii form
void CGameCommControl::ProcessCashFrenzyHit(RxSerialMessage *rx_msg)
{
 int iii;
 StratusMessageFormat stratus_msg;
 CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);

    if (!pGameDevice)
        logMessage("GameCommControl::ProcessCashFrenzyHit - NULL game pointer.");
    else
    {
        memset(&stratus_msg, 0, sizeof(stratus_msg));

		// change endian
		for (iii=0; iii < 4; iii++)
		{
			stratus_msg.data[iii] = rx_msg->data[4 - iii];	// account(DWORD)
			stratus_msg.data[iii + 4] = rx_msg->data[8 - iii];	// win code(DWORD)
		}

		// hours(BYTE), minutes(BYTE), seconds(BYTE), day(BYTE), month(BYTE), year(BYTE)
		for (iii=0; iii < 6; iii++)
			stratus_msg.data[iii + 8] = (BYTE)(((rx_msg->data[iii + 9] >> 4) * 10) + (rx_msg->data[iii + 9] & 0x0F));	// bcd_to_binary

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

		stratus_msg.send_code = WAA_CASH_FRENZY_JACKPOT;
        stratus_msg.task_code = WAA_TASK_WIDE_AREA_AWARD;
		stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
		stratus_msg.property_id = theApp.memory_location_config.property_id;
		stratus_msg.serial_port = port_info.port_number;
		stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
		stratus_msg.mux_id = rx_msg->mux_id;
		stratus_msg.message_length = 18 + STRATUS_HEADER_SIZE;
		theApp.QueueStratusTxMessage(&stratus_msg, FALSE);

		memset(&stratus_msg, 0, sizeof(stratus_msg));

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

		stratus_msg.send_code = WAA_CASH_FRENZY_QUERY_AFTER_WIN;
		stratus_msg.task_code = WAA_TASK_WIDE_AREA_AWARD;
		stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
		stratus_msg.property_id = theApp.memory_location_config.property_id;
		stratus_msg.serial_port = port_info.port_number;
		stratus_msg.message_length = STRATUS_HEADER_SIZE;
		stratus_msg.data_to_event_log = true;
		theApp.QueueStratusTxMessage(&stratus_msg, FALSE);
	}
}

// QUEUE MUX Wide Area Award Jackpot MESSAGE
// rx: account(long), code(long), jackpot amount(long), hours(uchar), minutes(uchar),
//	   seconds(uchar), day(uchar), month(uchar), year(uchar)
// tx: same as rx, but change endian and for uchar's convert from bcd to ascii form
void CGameCommControl::WinningWideAreaAward(RxSerialMessage *rx_msg)
{
 int iii;
 StratusMessageFormat stratus_msg;
 CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);

    if (!pGameDevice)
        logMessage("GameCommControl::WinningWideAreaAward - NULL game pointer.");
    else
    {
        memset(&stratus_msg, 0, sizeof(stratus_msg));

    	// change endian
    	for (iii=0; iii < 4; iii++)
    	{
    		stratus_msg.data[iii] = rx_msg->data[4 - iii];      // account(long)
    		stratus_msg.data[iii + 4] = rx_msg->data[8 - iii];  // code(long)
    		stratus_msg.data[iii + 8] = rx_msg->data[12 - iii]; // jackpot amount(long)
    	}

    	// hours(uchar), minutes(uchar), seconds(uchar),
    	// day(uchar), month(uchar), year(uchar)
        for (iii=0; iii < 6; iii++)
            stratus_msg.data[iii + 12] = (char)(((rx_msg->data[iii + 13] >> 4) * 10) + (rx_msg->data[iii + 13] & 0x0F));    // bcd_to_binary

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

        stratus_msg.send_code = WAA_JACKPOT;
        stratus_msg.task_code = WAA_TASK_WIDE_AREA_AWARD;
        stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
        stratus_msg.mux_id = rx_msg->mux_id;
        stratus_msg.message_length = 18 + STRATUS_HEADER_SIZE;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.serial_port = port_info.port_number;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
        theApp.QueueStratusTxMessage(&stratus_msg, FALSE);

        memset(&stratus_msg, 0, sizeof(stratus_msg));

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

        stratus_msg.send_code = WAA_QUERY_AFTER_WIN;
        stratus_msg.task_code = WAA_TASK_WIDE_AREA_AWARD;
        stratus_msg.message_length = STRATUS_HEADER_SIZE;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.serial_port = port_info.port_number;
        stratus_msg.data_to_event_log = true;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
        theApp.QueueStratusTxMessage(&stratus_msg, FALSE);
    }
}

// process redeem complete from game
// rx: account(long), transaction id(long), error code(char)
// tx: same as rx, but change endian
void CGameCommControl::redeemComplete(RxSerialMessage *rx_msg)
{
 int iii;
 StratusMessageFormat stratus_msg;
 CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
 RedeemComplete redeem_complete;


    if (!pGameDevice)
        logMessage("GameCommControl::redeemComplete - NULL game pointer.");
    else
    {
		memcpy(&redeem_complete, &rx_msg->data[1], sizeof(RedeemComplete));
		
		memset(&stratus_msg, 0, sizeof(stratus_msg));

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

        stratus_msg.send_code = MPI_REDEEM_COMPLETE;
        stratus_msg.message_length = sizeof(RedeemComplete) + STRATUS_HEADER_SIZE;

        stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
        stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
        stratus_msg.mux_id = rx_msg->mux_id;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.serial_port = port_info.port_number;
        stratus_msg.data_to_event_log = true;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;

        endian4(redeem_complete.account_number);
        endian4(redeem_complete.transaction_id);

		memcpy(stratus_msg.data, &redeem_complete, sizeof(RedeemComplete));

        theApp.QueueStratusTxMessage(&stratus_msg, FALSE);
    }
}

void CGameCommControl::redeemComplete(BYTE error_code)
{
 int iii;
 StratusMessageFormat stratus_msg;
 RedeemComplete redeem_complete;


    if (!pHeadGameDevice)
        logMessage("GameCommControl::redeemComplete - NULL game pointer.");
    else
    {
		memset(&stratus_msg, 0, sizeof(stratus_msg));

		if (pHeadGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pHeadGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

        stratus_msg.send_code = MPI_REDEEM_COMPLETE;
        stratus_msg.message_length = sizeof(RedeemComplete) + STRATUS_HEADER_SIZE;

        stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
        stratus_msg.ucmc_id = pHeadGameDevice->memory_game_config.ucmc_id;
        stratus_msg.mux_id = 0;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.serial_port = port_info.port_number;
        stratus_msg.data_to_event_log = true;
        stratus_msg.customer_id = pHeadGameDevice->memory_game_config.customer_id;

        redeem_complete.account_number = pHeadGameDevice->account_login_data.account_number;
        redeem_complete.transaction_id = pHeadGameDevice->redeem_request.transaction_id;
        if (error_code == 0)
            redeem_complete.error_code = '0';
        else
            redeem_complete.error_code = '1';

        endian4(redeem_complete.account_number);
        endian4(redeem_complete.transaction_id);

		memcpy(stratus_msg.data, &redeem_complete, sizeof(RedeemComplete));

        theApp.QueueStratusTxMessage(&stratus_msg, FALSE);
    }
}

// process Gambler's Bonus points redeem from game
// rx: account(long), redeem action(char), redeem choice(char), whole points(ulong), fractional points(short)
// tx: same as rx, but change endian
void CGameCommControl::redeemRequest(RxSerialMessage *rx_msg)
{
 int iii;
 StratusMessageFormat stratus_msg;
 CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
 CGameCommControl *pGameCommControl;
 RedeemRequest redeem_request;


    if (!pGameDevice)
        logMessage("GameCommControl::redeemRequest - NULL game pointer.");
    else
    {
		// save the message redeem complete message from the game until it is approved on the Stratus
		memcpy(&pGameDevice->redeem_request, &rx_msg->data[1], sizeof(RedeemRequest));
		memcpy(&redeem_request, &rx_msg->data[1], sizeof(RedeemRequest));
		
		memset(&stratus_msg, 0, sizeof(stratus_msg));

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

		if (pGameDevice->memory_game_config.mux_id && (rx_msg->data[0] == w__REDEEM_REQUEST))
        {
            stratus_msg.send_code = MPI_NEW_REDEEM_REQUEST;
            stratus_msg.message_length = sizeof(RedeemRequest) + STRATUS_HEADER_SIZE;
        }
        else
        {
            stratus_msg.send_code = MPI_REDEEM_REQUEST;
            stratus_msg.message_length = 12 + STRATUS_HEADER_SIZE;
        }

        stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
        stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
        stratus_msg.mux_id = rx_msg->mux_id;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.serial_port = port_info.port_number;
        stratus_msg.data_to_event_log = true;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;


        if (pGameDevice)
        {
            pGameCommControl = theApp.GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);

            if (pGameCommControl && pGameCommControl->sas_com)
            {
                redeem_request.account_number = pGameCommControl->sas_com->non_volatile_ram.member_id;
                if (pGameCommControl->sas_com->non_volatile_ram.session_points)
                    redeem_request.session_whole_points = pGameCommControl->sas_com->non_volatile_ram.session_points - 1;
                else
                    redeem_request.session_whole_points = 0;
                redeem_request.session_fractional_points = pGameCommControl->sas_com->non_volatile_ram.ses_data.fract_points;
                pGameCommControl->pHeadGameDevice->redeem_request.transaction_id = redeem_request.transaction_id;
            }
        }

        endian4(redeem_request.account_number);
        endian4(redeem_request.session_whole_points);
        endian2(redeem_request.session_fractional_points);
        endian4(redeem_request.transaction_id);

		memcpy(stratus_msg.data, &redeem_request, sizeof(RedeemRequest));

        theApp.QueueStratusTxMessage(&stratus_msg, FALSE);
    }
}

// redeem Gambler's Bonus points inquiry from game
// rx: account(long), whole points(ulong), fractional points(short)
// tx: same as rx, but change endian
void CGameCommControl::redeemInquiry(RxSerialMessage *rx_msg)
{
 int iii;
 StratusMessageFormat stratus_msg;
 CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
 CGameCommControl *pGameCommControl;

    if (!pGameDevice)
        logMessage("GameCommControl::redeemInquiry - NULL game pointer.");
    else
    {
        memset(&stratus_msg, 0, sizeof(stratus_msg));

    	// change endian
    	for (iii=0; iii < 4; iii++)
    	{
    		stratus_msg.data[iii] = rx_msg->data[4 - iii];      // account
    		stratus_msg.data[iii + 4] = rx_msg->data[8 - iii];  // whole points
    	}

    	// fractional points, change endian
    	stratus_msg.data[8] = rx_msg->data[10];
    	stratus_msg.data[9] = rx_msg->data[9];

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

        stratus_msg.send_code = MPI_REDEEM_INQUIRY;
        stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
        stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
        stratus_msg.mux_id = rx_msg->mux_id;
        stratus_msg.message_length = 10 + STRATUS_HEADER_SIZE;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.serial_port = port_info.port_number;
        stratus_msg.data_to_event_log = true;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;

        if (pGameDevice)
        {
            pGameCommControl = theApp.GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);

            if (pGameCommControl && pGameCommControl->sas_com)
            {

                RedeemInquiry redeem_inquiry;

                redeem_inquiry.account_number = pGameCommControl->sas_com->non_volatile_ram.member_id;
                if (pGameCommControl->sas_com->non_volatile_ram.session_points)
                    redeem_inquiry.session_whole_points = pGameCommControl->sas_com->non_volatile_ram.session_points - 1;
                else
                    redeem_inquiry.session_whole_points = 0;
                redeem_inquiry.session_fractional_points = pGameCommControl->sas_com->non_volatile_ram.ses_data.fract_points;

                endian4(redeem_inquiry.account_number);
                endian4(redeem_inquiry.session_whole_points);
                endian2(redeem_inquiry.session_fractional_points);

                memcpy(stratus_msg.data, &redeem_inquiry, sizeof(redeem_inquiry));

				memset(&pGameDevice->redeem_request, 0, sizeof(RedeemRequest));

                memset(pGameCommControl->sas_com->non_volatile_ram.redemption_cents, 0, 3 * sizeof(unsigned short));
                memset(pGameCommControl->sas_com->non_volatile_ram.points_redeemed, 0, 3 * sizeof(unsigned long));

            }
        }

        theApp.QueueStratusTxMessage(&stratus_msg, FALSE);

    }
}

// account logout message from game to Stratus
// rx: account(long), whole points(long), fractional points(short), coin in(long), coin out(long),
//	   coin drop(long), game count(long), time played(long), primary game card handle(long),
//	   primary game card games(long), primary game card marks(long), jackpot count(uchar),
//	   invalid tries count(char), card type(char), game card type(char), secondary game card handle(long),
//	   secondary game card games(long), secondary game card marks(long)
// tx: same as rx, but change endian
bool CGameCommControl::ProcessAccountLogout(RxSerialMessage *rx_msg)
{
    bool log_event = TRUE;
    CGameCommControl *pGameCommControl = 0;
    CGameDevice *pGameDevice = 0;
    CString message;
    TxSerialMessage msg;

    pGameDevice = GetGamePointer(rx_msg->mux_id);

    if (pGameDevice)
    {
        pGameCommControl = theApp.GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);

        if (pGameCommControl)
        {
			// logout with XP Direct Connect data NOT the GB Interface data
            pGameCommControl->AccountLogout((ULONG)0);
            pGameCommControl->updateGbInterfaceData();
            log_event = FALSE;
            message.Format("CGameCommControl::ProcessAccountLogout - logging out - sas id %d", pGameCommControl->sas_com->sas_id);
            logMessage(message);

             CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);

        }
        else
        {
            AccountLogout(rx_msg);
        }
		if (pGameDevice->memory_game_config.mux_id && (rx_msg->data[0] == f__GB_LOGOFF))
        {
			AccountLogoutData account_logout_data;
			memcpy(&account_logout_data, &rx_msg->data[1], sizeof(account_logout_data));

//            if (pGameDevice->memory_game_config.version != GB_INTERFACE_VERSION)
            {
                memset(&msg, 0, sizeof(msg));
                msg.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                msg.msg.length = 11;
                msg.msg.command = COMMAND_HEADER;
                msg.msg.data[0] = f__GB_LOGOFF;
                memcpy(&msg.msg.data[1], &account_logout_data.account_number, 4);
                pGameDevice->transmit_queue.push(msg);
            }
        }
    }

    return log_event;
}

// account logout message from game to Stratus
// rx: account(long), whole points(long), fractional points(short), coin in(long), coin out(long),
//	   coin drop(long), game count(long), time played(long), primary game card handle(long),
//	   primary game card games(long), primary game card marks(long), jackpot count(uchar),
//	   invalid tries count(char), card type(char), game card type(char), secondary game card handle(long),
//	   secondary game card games(long), secondary game card marks(long)
// tx: same as rx, but change endian
void CGameCommControl::AccountLogout(RxSerialMessage *rx_msg)
{
 int iii;
 StratusMessageFormat stratus_msg;
 CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
 CGameCommControl *pGameCommControl = 0;
 CString filename;
 AccountLogoutData account_logout_data;

    if (!pGameDevice)
        logMessage("GameCommControl::AccountLogout - NULL game pointer.");
    else
    {

        memset(&stratus_msg, 0, sizeof(stratus_msg));

		memcpy(&account_logout_data, &rx_msg->data[1], sizeof(account_logout_data));
        MachineIdentifiers machine_identifiers;
        machine_identifiers.mux_id = pGameDevice->memory_game_config.mux_id;
        machine_identifiers.sas_id = pGameDevice->memory_game_config.sas_id;
        machine_identifiers.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
		theApp.SendIdcLogoutMessage (pGameDevice, &account_logout_data);
/*
        if (theApp.memory_settings_ini.stratus_tcp_port == STRATUS_TEST_PORT &&
            theApp.memory_location_config.allow_player_autologout_for_test == 1)
                return;
*/

        // clear player account data sent to LAT for tier level printing, if present
        if (pGameDevice->account_tier_wait_timer)
            pGameDevice->account_tier_wait_timer = 0;

    	// change endian
    	for (iii=0; iii < 4; iii++)
    	{
            stratus_msg.data[iii] = rx_msg->data[4 - iii];          // account(long)
            stratus_msg.data[iii + 4] = rx_msg->data[8 - iii];      // whole points(long)
            stratus_msg.data[iii + 10] = rx_msg->data[14 - iii];    // coin in(long)
            stratus_msg.data[iii + 14] = rx_msg->data[18 - iii];    // coin out(long)
            stratus_msg.data[iii + 18] = rx_msg->data[22 - iii];    // coin drop(long)
            stratus_msg.data[iii + 22] = rx_msg->data[26 - iii];    // game count(long)
            stratus_msg.data[iii + 26] = rx_msg->data[30 - iii];    // time played(long)
            stratus_msg.data[iii + 30] = rx_msg->data[34 - iii];    // primary game card handle(long)
            stratus_msg.data[iii + 34] = rx_msg->data[38 - iii];    // primary game card games(long)
            stratus_msg.data[iii + 38] = rx_msg->data[42 - iii];    // primary game card marks(long)
            stratus_msg.data[iii + 46] = rx_msg->data[50 - iii];    // secondary game card handle(long)
            stratus_msg.data[iii + 50] = rx_msg->data[54 - iii];    // secondary game card games(long)
            stratus_msg.data[iii + 54] = rx_msg->data[58 - iii];    // secondary game card marks(long)
		}

    	// fractional points(short)
    	stratus_msg.data[8] = rx_msg->data[10];
    	stratus_msg.data[9] = rx_msg->data[9];

		stratus_msg.data[42] = rx_msg->data[43];    // jackpot count(uchar)
		stratus_msg.data[43] = rx_msg->data[44];	// invalid tries count(char)
		stratus_msg.data[45] = rx_msg->data[46];	// game card type(char)
    	memcpy(&stratus_msg.data[58], SetTimeStamp(), 14);

		// card type(char), check for ascii character
        if (rx_msg->data[45] < 0x30)
            stratus_msg.data[44] = (char)(rx_msg->data[45] + 0x30);
        else
            stratus_msg.data[44] = rx_msg->data[45];

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

        stratus_msg.send_code = MPI_ACCOUNT_LOGOUT;
        stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
        stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
        stratus_msg.mux_id = rx_msg->mux_id;
        stratus_msg.message_length = 72 + STRATUS_HEADER_SIZE;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.serial_port = port_info.port_number;
        stratus_msg.data_to_event_log = true;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
		theApp.QueueStratusTxMessage(&stratus_msg, TRUE);
        // reset the login related information after a valid logout
        memset(&pGameDevice->account_login_data, 0, sizeof(AccountLogin));
        saveGbLoginChange(&pGameDevice->account_login_data, pGameDevice->memory_game_config.ucmc_id);

        if (pGameDevice)
        {
            pGameCommControl = theApp.GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);

            if (pGameCommControl && pGameCommControl->sas_com)
            {
                // zero out the session data
                pGameCommControl->sas_com->non_volatile_ram.member_id = 0;
                pGameCommControl->sas_com->non_volatile_ram.pin = 0;
                pGameCommControl->sas_com->non_volatile_ram.session_points = 0;
                pGameCommControl->sas_com->non_volatile_ram.ses_data.fract_points = 0;
                pGameCommControl->sas_com->non_volatile_ram.ses_data.games = 0;
                pGameCommControl->sas_com->non_volatile_ram.ses_data.whole_points = 0; 
                pGameCommControl->sas_com->non_volatile_ram.ses_data.fract_points = 0; 
                pGameCommControl->sas_com->non_volatile_ram.ses_data.cents_in = 0; 
                pGameCommControl->sas_com->non_volatile_ram.ses_data.cents_out = 0; 
                pGameCommControl->sas_com->non_volatile_ram.ses_data.drop_coins = 0; 
                pGameCommControl->sas_com->non_volatile_ram.ses_data.games = 0;
                pGameCommControl->sas_com->non_volatile_ram.session_points = 0;

                pGameCommControl->sas_com->changeGbSystemState(OFF_AVAIL);
                pGameCommControl->sas_com->saveNonVolatileRam();

                if (pHeadGameDevice)
                    memset(&pHeadGameDevice->account_login_data, 0, sizeof(AccountLogin));
            }
        }
    }
}

// account logout message from game to Stratus
void CGameCommControl::AccountLogout(ULONG jackpot_handpay)
{
 StratusMessageFormat stratus_msg;
 CGameDevice *pGameDevice = 0;
 CGameCommControl *pGameCommControl = 0;
 CString filename;
 AccountLogoutData account_logout_data;
 unsigned short pointsf;
 unsigned long pointsw, ses_time;
 SYSTEMTIME system_time;
 unsigned char secs, mins, hours;
 EventMessage event_message;

    if (pHeadGameDevice)
        pGameDevice = pHeadGameDevice;

    if (!pGameDevice)
        logMessage("GameCommControl::AccountLogout - NULL game pointer.");
    else
    {
        if (theApp.memory_settings_ini.stratus_tcp_port == STRATUS_TEST_PORT &&
            theApp.memory_location_config.allow_player_autologout_for_test == 1)
                return;

        // clear player account data sent to LAT for tier level printing, if present
        if (pGameDevice->account_tier_wait_timer)
            pGameDevice->account_tier_wait_timer = 0;

        memset(&account_logout_data, 0, sizeof(account_logout_data));

	    account_logout_data.account_number = sas_com->non_volatile_ram.member_id;

	    if (sas_com->non_volatile_ram.fract_points >= sas_com->non_volatile_ram.ses_data.fract_points)
        {
	      pointsf = sas_com->non_volatile_ram.fract_points - sas_com->non_volatile_ram.ses_data.fract_points;
	      pointsw = sas_com->non_volatile_ram.session_points;
        }
	    else
        {
	      pointsf = (WHOLEPOINT + sas_com->non_volatile_ram.fract_points) - sas_com->non_volatile_ram.ses_data.fract_points;
	      pointsw = sas_com->non_volatile_ram.session_points - 1;
        }

        account_logout_data.session_whole_points = pointsw;
        account_logout_data.session_fractional_points = pointsf;
        account_logout_data.session_coin_in = (sas_com->non_volatile_ram.TOTAL_IN - sas_com->non_volatile_ram.ses_data.cents_in) / NICKEL;
        account_logout_data.session_coin_out = ((sas_com->non_volatile_ram.CENTS_WON - sas_com->non_volatile_ram.ses_data.cents_out) + jackpot_handpay ) / NICKEL;
        account_logout_data.session_coin_drop = (sas_com->non_volatile_ram.TOTAL_DROP - sas_com->non_volatile_ram.ses_data.drop_coins) / NICKEL;
        account_logout_data.session_games = sas_com->non_volatile_ram.GAMES_PLAYED - sas_com->non_volatile_ram.ses_data.games;

        GetLocalTime(&system_time);

	    secs = (unsigned char)system_time.wSecond;
	    mins = (unsigned char)system_time.wMinute;
	    hours = (unsigned char)system_time.wHour;

	    ses_time = (hours * SEC_IN_HR) + (mins * SEC_IN_MIN) + secs;

	
	    if (ses_time < sas_com->non_volatile_ram.ses_data.timer)
	    {
            account_logout_data.session_seconds = (SEC_IN_DAY - sas_com->non_volatile_ram.ses_data.timer) + ses_time; // seconds started before midnight
	    }
	    else
	    {
            account_logout_data.session_seconds = ses_time - sas_com->non_volatile_ram.ses_data.timer; // seconds started before midnight
	    } // end else sestime ...


        account_logout_data.primary_handle = sas_com->non_volatile_ram.bingo.primary.handle_to_date;
        account_logout_data.primary_games = sas_com->non_volatile_ram.bingo.primary.games_to_date;
        account_logout_data.primary_card = sas_com->non_volatile_ram.bingo.primary.card;
        account_logout_data.session_jackpots = sas_com->non_volatile_ram.ses_data.jackpots;
        account_logout_data.invalid_pin_tries = SUCCESS_ENTRY;
        account_logout_data.user_type = USER_TYPE;
        account_logout_data.bingo_game_type = sas_com->non_volatile_ram.bingo.primary.type;
        account_logout_data.secondary_handle = sas_com->non_volatile_ram.bingo.secondary.handle_to_date;
        account_logout_data.secondary_games = sas_com->non_volatile_ram.bingo.secondary.games_to_date;
        account_logout_data.secondary_card = sas_com->non_volatile_ram.bingo.secondary.card;

        memset(&stratus_msg, 0, sizeof(stratus_msg));
        memcpy(stratus_msg.data, &account_logout_data, sizeof(account_logout_data));

        // log the message
        memset(&event_message, 0, sizeof(event_message));
        event_message.head.time_stamp = time(NULL);
        event_message.head.event_type = CONTROLLER_RX;
        event_message.head.data_length = 65;
        event_message.head.port_number = port_info.port_number;
        event_message.head.customer_id = pGameDevice->memory_game_config.customer_id;

        event_message.data[0] = MESSAGE_HEADER;
        event_message.data[1] = sas_com->sas_id;
        event_message.data[2] = 1;
        event_message.data[3] = 65;
        event_message.data[4] = COMMAND_HEADER;
        event_message.data[5] = ACCOUNT_LOGOUT;

        memcpy(&event_message.data[6], &account_logout_data, sizeof(account_logout_data));

        theApp.event_message_queue.push(event_message);

        MachineIdentifiers machine_identifiers;
        machine_identifiers.mux_id = pGameDevice->memory_game_config.mux_id;
        machine_identifiers.sas_id = pGameDevice->memory_game_config.sas_id;
        machine_identifiers.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
		theApp.SendIdcLogoutMessage (pGameDevice, &account_logout_data);

        // Stratusfy the data
        endian4(account_logout_data.account_number);
        endian4(account_logout_data.session_whole_points);
        endian2(account_logout_data.session_fractional_points);
        endian4(account_logout_data.session_coin_in);
        endian4(account_logout_data.session_coin_out);
        endian4(account_logout_data.session_coin_drop);
        endian4(account_logout_data.session_games);
        endian4(account_logout_data.session_seconds);
        endian4(account_logout_data.primary_handle);
        endian4(account_logout_data.primary_games);
        endian4(account_logout_data.primary_card);
        endian4(account_logout_data.secondary_handle);
        endian4(account_logout_data.secondary_games);
        endian4(account_logout_data.secondary_card);

    	memcpy(&account_logout_data.time_stamp, SetTimeStamp(), 14);

        memset(&stratus_msg, 0, sizeof(stratus_msg));
        memcpy(stratus_msg.data, &account_logout_data, sizeof(account_logout_data));

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

        stratus_msg.send_code = MPI_ACCOUNT_LOGOUT;
        stratus_msg.task_code = SMPI_TASK_PLAYER_ACCOUNTING;
        stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
        stratus_msg.mux_id = sas_com->sas_id;
        stratus_msg.message_length = 72 + STRATUS_HEADER_SIZE;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.serial_port = port_info.port_number;
        stratus_msg.data_to_event_log = true;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
		theApp.QueueStratusTxMessage(&stratus_msg, TRUE);
        // reset the login related information after a valid logout
        memset(&pGameDevice->account_login_data, 0, sizeof(AccountLogin));
        // reset the login related information after a valid logout
        pGameDevice = theApp.GetGbInterfaceGamePointer(pGameDevice->memory_game_config.ucmc_id);
        memset(&pGameDevice->account_login_data, 0, sizeof(AccountLogin));

        saveGbLoginChange(&pGameDevice->account_login_data, pGameDevice->memory_game_config.ucmc_id);

        if (pGameDevice && sas_com)
        {
            // zero out the session data
            sas_com->non_volatile_ram.member_id = 0;
            sas_com->non_volatile_ram.pin = 0;
            sas_com->non_volatile_ram.session_points = 0;
            sas_com->non_volatile_ram.ses_data.fract_points = 0;
            sas_com->non_volatile_ram.ses_data.games = 0;
            sas_com->non_volatile_ram.ses_data.whole_points = 0; 
            sas_com->non_volatile_ram.ses_data.fract_points = 0; 
            sas_com->non_volatile_ram.ses_data.cents_in = 0; 
            sas_com->non_volatile_ram.ses_data.cents_out = 0; 
            sas_com->non_volatile_ram.ses_data.drop_coins = 0; 
            sas_com->non_volatile_ram.ses_data.games = 0;
            sas_com->non_volatile_ram.session_points = 0;

			sas_com->changeGbSystemState(OFF_AVAIL);
            sas_com->saveNonVolatileRam();
        }
    }
}


// GB Session update message from game to Stratus
void CGameCommControl::GbSessionUpdate(void)
{
 StratusMessageFormat stratus_msg;
 CGameDevice *pGameDevice = 0;
 CGameCommControl *pGameCommControl = 0;
 CString filename;
 GbSessionUpdateData session_update_data;
 unsigned short pointsf;
 unsigned long pointsw, ses_time;
 SYSTEMTIME system_time;
 unsigned char secs, mins, hours;

    if (pHeadGameDevice)
        pGameDevice = pHeadGameDevice;

    if (!pGameDevice)
        logMessage("GameCommControl::GbSessionUpdate - NULL game pointer.");
    else
    {
        memset(&stratus_msg, 0, sizeof(stratus_msg));
        memset(&session_update_data, 0, sizeof(session_update_data));

	    session_update_data.account_number = sas_com->non_volatile_ram.member_id;

	    if (sas_com->non_volatile_ram.fract_points >= sas_com->non_volatile_ram.ses_data.fract_points)
        {
	      pointsf = sas_com->non_volatile_ram.fract_points - sas_com->non_volatile_ram.ses_data.fract_points;
	      pointsw = sas_com->non_volatile_ram.session_points;
        }
	    else
        {
	      pointsf = (WHOLEPOINT + sas_com->non_volatile_ram.fract_points) - sas_com->non_volatile_ram.ses_data.fract_points;
	      pointsw = sas_com->non_volatile_ram.session_points - 1;
        }

        session_update_data.session_whole_points = pointsw;
        session_update_data.session_fractional_points = pointsf;
        session_update_data.session_coin_in = (sas_com->non_volatile_ram.TOTAL_IN - sas_com->non_volatile_ram.ses_data.cents_in) / NICKEL;
        session_update_data.session_coin_out = (sas_com->non_volatile_ram.CENTS_WON - sas_com->non_volatile_ram.ses_data.cents_out) / NICKEL;
        session_update_data.session_coin_drop = (sas_com->non_volatile_ram.TOTAL_DROP - sas_com->non_volatile_ram.ses_data.drop_coins) / NICKEL;
        session_update_data.session_games = sas_com->non_volatile_ram.GAMES_PLAYED - sas_com->non_volatile_ram.ses_data.games;

        GetLocalTime(&system_time);

	    secs = (unsigned char)system_time.wSecond;
	    mins = (unsigned char)system_time.wMinute;
	    hours = (unsigned char)system_time.wHour;

	    ses_time = (hours * SEC_IN_HR) + (mins * SEC_IN_MIN) + secs;

	    if (ses_time < sas_com->non_volatile_ram.ses_data.timer)
	    {
            session_update_data.session_seconds = (SEC_IN_DAY - sas_com->non_volatile_ram.ses_data.timer) + ses_time;
	    }
	    else
	    {
            session_update_data.session_seconds = ses_time - sas_com->non_volatile_ram.ses_data.timer;
	    } // end else sestime ...

        session_update_data.primary_handle = sas_com->non_volatile_ram.bingo.primary.handle_to_date;
        session_update_data.primary_games = sas_com->non_volatile_ram.bingo.primary.games_to_date;
        session_update_data.primary_card = sas_com->non_volatile_ram.bingo.primary.card;
        session_update_data.session_jackpots = sas_com->non_volatile_ram.ses_data.jackpots;

        session_update_data.secondary_handle = sas_com->non_volatile_ram.bingo.secondary.handle_to_date;
        session_update_data.secondary_games = sas_com->non_volatile_ram.bingo.secondary.games_to_date;
        session_update_data.secondary_card = sas_com->non_volatile_ram.bingo.secondary.card;

        // Stratusfy the data
        endian4(session_update_data.account_number);
        endian4(session_update_data.session_whole_points);
        endian2(session_update_data.session_fractional_points);
        endian4(session_update_data.session_coin_in);
        endian4(session_update_data.session_coin_out);
        endian4(session_update_data.session_coin_drop);
        endian4(session_update_data.session_games);
        endian4(session_update_data.session_seconds);
        endian4(session_update_data.primary_handle);
        endian4(session_update_data.primary_games);
        endian4(session_update_data.primary_card);
        endian4(session_update_data.secondary_handle);
        endian4(session_update_data.secondary_games);
        endian4(session_update_data.secondary_card);

        memcpy(stratus_msg.data, &session_update_data, sizeof(session_update_data));

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

        stratus_msg.send_code = MPI_GB_SESSION_UPDATE;
        stratus_msg.task_code = SMPI_TASK_PLAYER_ACCOUNTING;
        stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
        stratus_msg.mux_id = sas_com->sas_id;
        stratus_msg.message_length = 58 + STRATUS_HEADER_SIZE;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.serial_port = port_info.port_number;
        stratus_msg.data_to_event_log = true;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
		theApp.QueueStratusTxMessage(&stratus_msg, FALSE);
    }
}

// GB login update message from game to Stratus
void CGameCommControl::GbAccountLoginInfoRequest(void)
{
 StratusMessageFormat stratus_msg;
 CGameDevice *pGameDevice = 0;
 CGameCommControl *pGameCommControl = 0;
 CString filename;

    if (pHeadGameDevice)
        pGameDevice = pHeadGameDevice;

    if (!pGameDevice)
        logMessage("GameCommControl::GbAccountLoginInfoRequest - NULL game pointer.");
    else
    {
        memset(&stratus_msg, 0, sizeof(stratus_msg));

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

        stratus_msg.send_code = MPI_ACCOUNT_LOGIN_INFO;
        stratus_msg.task_code = SMPI_TASK_PLAYER_ACCOUNTING;
        stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
		if (sas_com)
	        stratus_msg.mux_id = sas_com->sas_id;
        stratus_msg.message_length = STRATUS_HEADER_SIZE;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.serial_port = port_info.port_number;
        stratus_msg.data_to_event_log = true;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
		theApp.QueueStratusTxMessage(&stratus_msg, FALSE);
    }
}

// GB Session update message from game to Stratus
void CGameCommControl::GbSessionInfoRequest(void)
{
 StratusMessageFormat stratus_msg;
 CGameDevice *pGameDevice = 0;
 CGameCommControl *pGameCommControl = 0;
 CString filename;

    if (pHeadGameDevice)
        pGameDevice = pHeadGameDevice;

    if (!pGameDevice)
        logMessage("GameCommControl::GbSessionInfoRequest - NULL game pointer.");
    else
    {
        memset(&stratus_msg, 0, sizeof(stratus_msg));

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

        stratus_msg.send_code = MPI_GB_SESSION_INFO_REQUEST;
        stratus_msg.task_code = SMPI_TASK_PLAYER_ACCOUNTING;
        stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
        if (sas_com)
            stratus_msg.mux_id = sas_com->sas_id;
        else
            stratus_msg.mux_id = pGameDevice->memory_game_config.mux_id;
        stratus_msg.message_length = STRATUS_HEADER_SIZE;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.serial_port = port_info.port_number;
        stratus_msg.data_to_event_log = true;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
		theApp.QueueStratusTxMessage(&stratus_msg, FALSE);
    }
}

// send clear card message from game to Stratus
// rx: account(long)
// tx: same as rx, but change endian
void CGameCommControl::ClearBingoCardRequest(RxSerialMessage *rx_msg)
{
 int iii;
 StratusMessageFormat stratus_msg;
 CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
 CGameCommControl *pGameCommControl = 0;
 bool write_to_disk = TRUE;
 ClearCardInfo clear_card_info;

    if (!pGameDevice)
        logMessage("GameCommControl::ClearBingoCardRequest - NULL game pointer.");
    else
    {
        memset(&stratus_msg, 0, sizeof(stratus_msg));
        memset(&clear_card_info, 0, sizeof(clear_card_info));

        memcpy(&clear_card_info, &rx_msg->data[1], sizeof(ClearCardInfo));

    	// account(long), change endian
		memcpy(&stratus_msg.data, (BYTE*)&clear_card_info, sizeof(ClearCardInfo));

		endian4(clear_card_info.member_id);
		endian4(clear_card_info.transaction_id);

		memcpy(&stratus_msg.data, (BYTE*)&clear_card_info, sizeof(ClearCardInfo));

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

		if (pGameDevice->memory_game_config.mux_id && (rx_msg->data[0] == n__NEW_CLEAR_BINGO_CARD_REQUEST))
        {
            stratus_msg.send_code = MPI_NEW_CLEAR_BINGO_CARD;
            write_to_disk = FALSE;
        }
        else
            stratus_msg.send_code = MPI_CLEAR_BINGO_CARD;

        stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
        stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
        stratus_msg.mux_id = rx_msg->mux_id;
        stratus_msg.message_length = sizeof(ClearCardInfo) + STRATUS_HEADER_SIZE;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.serial_port = port_info.port_number;
        stratus_msg.data_to_event_log = true;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
        theApp.QueueStratusTxMessage(&stratus_msg, TRUE);

        if (pGameDevice)
        {
            pGameCommControl = theApp.GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);

            if (pGameCommControl && pGameCommControl->sas_com)
            {
                // zero out the session data
                pGameCommControl->sas_com->non_volatile_ram.bingo.primary.card = pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.card;
                pGameCommControl->sas_com->non_volatile_ram.bingo.primary.handle_to_date = pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.handle_to_date;
                pGameCommControl->sas_com->non_volatile_ram.bingo.primary.games_to_date = pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.games_to_date;

            	pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.card = 0;
                pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.handle_to_date = 0;
                pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.games_to_date = 0;

                pGameCommControl->sas_com->saveNonVolatileRam();

                pGameCommControl->GbSessionUpdate();
            }
			else
			{
				if (pGameDevice->memory_game_config.mux_id && (rx_msg->data[0] == n__NEW_CLEAR_BINGO_CARD_REQUEST))
				{
					DWORD account_number;
					memcpy(&account_number, &rx_msg->data[1], sizeof(DWORD));

					TxSerialMessage msg;
					memset(&msg, 0, sizeof(msg));
					msg.msg.mux_id = pGameDevice->memory_game_config.mux_id;
					msg.msg.length = 11;
					msg.msg.command = COMMAND_HEADER;
					msg.msg.data[0] = n__NEW_CLEAR_BINGO_CARD_REQUEST;
					memcpy(&msg.msg.data[1], &account_number, 4);
					pGameDevice->transmit_queue.push(msg);
				}
			}
        }
    }
}

// send clear card message from game to Stratus
// rx: account(long)
// tx: same as rx, but change endian
void CGameCommControl::ClearBingoCardRequest(CGameDevice *pGameDevice, ULONG account, BYTE sas_id)
{
 StratusMessageFormat stratus_msg;
// CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
 CGameCommControl *pGameCommControl = 0;
 EventMessage event_message;

 #pragma pack(1)
 struct
 {
	ULONG account;
 } clear_bingo_card;
 #pragma pack()

    if (!pGameDevice)
        logMessage("GameCommControl::ClearBingoCardRequest - NULL game pointer.");
    else
    {
        memset(&stratus_msg, 0, sizeof(stratus_msg));

        clear_bingo_card.account = account;

        // log the message
        memset(&event_message, 0, sizeof(event_message));
        event_message.head.time_stamp = time(NULL);
        event_message.head.event_type = CONTROLLER_RX;
        event_message.head.data_length = 65;
        event_message.head.port_number = port_info.port_number;
        event_message.head.customer_id = pGameDevice->memory_game_config.customer_id;

        event_message.data[0] = MESSAGE_HEADER;
        event_message.data[1] = sas_id;
        event_message.data[2] = 1;
        event_message.data[3] = 11;
        event_message.data[4] = COMMAND_HEADER;
        event_message.data[5] = CLEAR_BINGO_CARD_REQUEST;

        memcpy(&event_message.data[6], &clear_bingo_card, sizeof(clear_bingo_card));

        theApp.event_message_queue.push(event_message);

		endian4(clear_bingo_card.account);
        memcpy(stratus_msg.data, &clear_bingo_card, sizeof(clear_bingo_card));

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

        stratus_msg.send_code = MPI_CLEAR_BINGO_CARD;
        stratus_msg.task_code = SMPI_TASK_PLAYER_ACCOUNTING;
        stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
        stratus_msg.mux_id = sas_id;
        stratus_msg.message_length = 4 + STRATUS_HEADER_SIZE;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.serial_port = port_info.port_number;
        stratus_msg.data_to_event_log = true;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
        theApp.QueueStratusTxMessage(&stratus_msg, TRUE);

        if (pGameDevice)
        {
            pGameCommControl = theApp.GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);

            if (pGameCommControl && pGameCommControl->sas_com)
            {
                // zero out the session data
                pGameCommControl->sas_com->non_volatile_ram.bingo.primary.card = pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.card;
                pGameCommControl->sas_com->non_volatile_ram.bingo.primary.handle_to_date = pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.handle_to_date;
                pGameCommControl->sas_com->non_volatile_ram.bingo.primary.games_to_date = pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.games_to_date;

            	pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.card = 0;
                pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.handle_to_date = 0;
                pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.games_to_date = 0;

                pGameCommControl->sas_com->saveNonVolatileRam();
            }
        }
    }
}

// Winning bingo card message from game to Stratus
// rx: account(long), paytable amount(long)
// tx: same as rx, but change endian
void CGameCommControl::WinningBingoCard(RxSerialMessage *rx_msg)
{
 int iii;
 StratusMessageFormat stratus_msg;
 CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
 WinningCardInfo winning_card_info;
 bool duplicate_message = FALSE;

    if (!pGameDevice)
        logMessage("GameCommControl::WinningBingoCard - NULL game pointer.");
    else
    {
        memcpy(&winning_card_info.member_id, &rx_msg->data[1], 4);
        memcpy(&winning_card_info.win_amount, &rx_msg->data[5], 4);

        if (pGameDevice->last_winning_card_time && (winning_card_info.member_id == pGameDevice->last_winning_card_info.member_id) && (winning_card_info.win_amount == pGameDevice->last_winning_card_info.win_amount))
        {
            if ((pGameDevice->last_winning_card_time + SEC_IN_DAY) >  time(NULL))
                duplicate_message = TRUE;
        }

        if (!duplicate_message)
        {
            memset(&stratus_msg, 0, sizeof(stratus_msg));

    	    for (iii=0; iii < 4; iii++)
    	    {
    		    stratus_msg.data[iii] = rx_msg->data[4 - iii];      // account
    		    stratus_msg.data[iii + 4] = rx_msg->data[8 - iii];  // paytable amount
    	    }

		    if (pGameDevice->memory_game_config.club_code)
			    stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		    else
			    stratus_msg.club_code = theApp.memory_location_config.club_code;

            stratus_msg.send_code = MPI_WINNING_BINGO_CARD;
            stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
            stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
            stratus_msg.mux_id = rx_msg->mux_id;
            stratus_msg.message_length = 8 + STRATUS_HEADER_SIZE;
            stratus_msg.property_id = theApp.memory_location_config.property_id;
            stratus_msg.serial_port = port_info.port_number;
            stratus_msg.data_to_event_log = true;
            stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
            theApp.QueueStratusTxMessage(&stratus_msg, FALSE);

            memcpy(&pGameDevice->last_winning_card_info, &winning_card_info, sizeof(WinningCardInfo));
            pGameDevice->last_winning_card_time = time(NULL);
        }
    }
}

// Winning bingo card message from game to Stratus
void CGameCommControl::WinningBingoCard(BYTE sas_id, ULONG account, ULONG amount)
{
    StratusMessageFormat stratus_msg;
    CGameDevice *pGameDevice = 0;
    WinningCardInfo winning_card_info;
    bool duplicate_message = FALSE;

 #pragma pack(1)
 struct
 {
	ULONG account;
	ULONG amount;
 } winning_bingo_card;
 #pragma pack()

    if (pHeadGameDevice)
        pGameDevice = pHeadGameDevice;

    if (!pGameDevice)
        logMessage("GameCommControl::WinningBingoCard - NULL game pointer.");
    else
    {
        winning_card_info.member_id = account;
        winning_card_info.win_amount = amount;

        if (pGameDevice->last_winning_card_time && (winning_card_info.member_id == pGameDevice->last_winning_card_info.member_id) && (winning_card_info.win_amount == pGameDevice->last_winning_card_info.win_amount))
        {
            if ((pGameDevice->last_winning_card_time + SEC_IN_DAY) >  time(NULL))
                duplicate_message = TRUE;
        }

        if (!duplicate_message)
        {
            memset(&stratus_msg, 0, sizeof(stratus_msg));

            winning_bingo_card.account = account;
            winning_bingo_card.amount = amount;

		    endian4(winning_bingo_card.account);
		    endian4(winning_bingo_card.amount);

            memcpy(stratus_msg.data, &winning_bingo_card, sizeof(winning_bingo_card));

		    if (pGameDevice->memory_game_config.club_code)
			    stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		    else
			    stratus_msg.club_code = theApp.memory_location_config.club_code;

            stratus_msg.send_code = MPI_WINNING_BINGO_CARD;
            stratus_msg.task_code = SMPI_TASK_PLAYER_ACCOUNTING;
            stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
            stratus_msg.mux_id = sas_id;
            stratus_msg.message_length = 8 + STRATUS_HEADER_SIZE;
            stratus_msg.property_id = theApp.memory_location_config.property_id;
            stratus_msg.serial_port = port_info.port_number;
            stratus_msg.data_to_event_log = true;
            stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
            theApp.QueueStratusTxMessage(&stratus_msg, FALSE);

            memcpy(&pGameDevice->last_winning_card_info, &winning_card_info, sizeof(WinningCardInfo));
            pGameDevice->last_winning_card_time = time(NULL);
        }
    }
}

// Winning bingo hand from game to Stratus
// rx: account(long), code(long), handle(long), games(long), card selection(char)
// tx: same as rx, but change endian
void CGameCommControl::WinningBingoHand(RxSerialMessage *rx_msg)
{
 int iii;
 StratusMessageFormat stratus_msg;
 CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
 bool write_to_disk = TRUE;
 NewWinningHandMessage winning_hand_message;

    if (!pGameDevice)
        logMessage("GameCommControl::WinningBingoHand - NULL game pointer.");
    else
    {
        memset(&stratus_msg, 0, sizeof(stratus_msg));
        memcpy(&winning_hand_message, (NewWinningHandMessage*)&rx_msg->data[1], sizeof(NewWinningHandMessage));

        endian4(winning_hand_message.member_id);
        endian4(winning_hand_message.win_code);
        endian4(winning_hand_message.handle);
        endian4(winning_hand_message.games);
        endian4(winning_hand_message.transaction_id);

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

		if (pGameDevice->memory_game_config.mux_id && (rx_msg->data[0] == l__BINGO_WINNING_HAND))
        {
            stratus_msg.send_code = MPI_NEW_WINNING_BINGO_HAND;
            write_to_disk = FALSE;
            stratus_msg.message_length = sizeof(NewWinningHandMessage) + STRATUS_HEADER_SIZE;
        }
        else
        {
            stratus_msg.send_code = MPI_WINNING_BINGO_HAND;
            stratus_msg.message_length = 17 + STRATUS_HEADER_SIZE;
        }

        stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
        stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
        stratus_msg.mux_id = rx_msg->mux_id;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.serial_port = port_info.port_number;
        stratus_msg.data_to_event_log = true;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;

        memcpy(stratus_msg.data, (BYTE*)&winning_hand_message, sizeof(NewWinningHandMessage));

        theApp.QueueStratusTxMessage(&stratus_msg, write_to_disk);

		if (pGameDevice->memory_game_config.mux_id && (rx_msg->data[0] == l__BINGO_WINNING_HAND))
        {
			DWORD account_number;
			memcpy(&account_number, &rx_msg->data[1], sizeof(DWORD));

            TxSerialMessage msg;
            memset(&msg, 0, sizeof(msg));
            msg.msg.mux_id = pGameDevice->memory_game_config.mux_id;
            msg.msg.length = 11;
            msg.msg.command = COMMAND_HEADER;
            msg.msg.data[0] = l__BINGO_WINNING_HAND;
            memcpy(&msg.msg.data[1], &account_number, 4);
            pGameDevice->transmit_queue.push(msg);
        }

    }
}

void CGameCommControl::WinningBingoHand(BYTE sas_id, ULONG account, ULONG code, ULONG handle, ULONG games, BYTE card_type)
{
 StratusMessageFormat stratus_msg;
 CGameDevice *pGameDevice = 0;
 NewWinningHandMessage winning_hand_message;

    if (pHeadGameDevice)
        pGameDevice = pHeadGameDevice;

    if (!pGameDevice)
        logMessage("GameCommControl::WinningBingoHand - NULL game pointer.");
    else
    {
        memset(&stratus_msg, 0, sizeof(stratus_msg));

        winning_hand_message.member_id = account;
        winning_hand_message.win_code = code;
        winning_hand_message.handle = handle;
        winning_hand_message.games = games;
        winning_hand_message.card_type = card_type;
        // generate a transaction_id
        rand_s((unsigned int*)&winning_hand_message.transaction_id);

		endian4(winning_hand_message.member_id);
		endian4(winning_hand_message.win_code);
		endian4(winning_hand_message.handle);
		endian4(winning_hand_message.games);
		endian4(winning_hand_message.transaction_id);

        memcpy(stratus_msg.data, &winning_hand_message, sizeof(winning_hand_message));

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

        stratus_msg.send_code = MPI_NEW_WINNING_BINGO_HAND;
        stratus_msg.task_code = SMPI_TASK_PLAYER_ACCOUNTING;
        stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
        stratus_msg.mux_id = sas_id;
        stratus_msg.message_length = sizeof(NewWinningHandMessage) + STRATUS_HEADER_SIZE;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.serial_port = port_info.port_number;
        stratus_msg.data_to_event_log = true;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
        theApp.QueueStratusTxMessage(&stratus_msg, TRUE);
    }
}

void CGameCommControl::process4okWin(CGameDevice *pGameDevice, ULONG account, ULONG code, WORD bet, WORD denomination)
{
 StratusMessageFormat stratus_msg;

 #pragma pack(1)
 struct
 {
	ULONG account;
	ULONG code;
    WORD bet;
    WORD denomination;
 } winning_hand_data;
 #pragma pack()

    if (!pGameDevice)
        logMessage("GameCommControl::process4okWin - NULL game pointer.");
    else
    {
        memset(&stratus_msg, 0, sizeof(stratus_msg));

        winning_hand_data.account = account;
        winning_hand_data.code = code;
        winning_hand_data.bet = bet;
        winning_hand_data.denomination = denomination;

		endian4(winning_hand_data.account);
		endian4(winning_hand_data.code);
		endian4(winning_hand_data.bet);
		endian4(winning_hand_data.denomination);

        memcpy(stratus_msg.data, &winning_hand_data, sizeof(winning_hand_data));

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

        stratus_msg.send_code = MPI_4OK_WIN;
        stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
        stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
        stratus_msg.mux_id = pGameDevice->memory_game_config.mux_id;
        stratus_msg.message_length = 12 + STRATUS_HEADER_SIZE;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.serial_port = port_info.port_number;
        stratus_msg.data_to_event_log = true;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
        theApp.QueueStratusTxMessage(&stratus_msg, FALSE);
    }
}

// Account inquiry message from game to Stratus
// rx: account(long), club code(short), pin number(short), enrollment account(long), enrollment pin number(short)
//	   card type(char), transaction code(char)
// tx: same as rx, but change endian
// Transaction code is not used
void CGameCommControl::AccountInquiry(bool new_login_msg, RxSerialMessage *rx_msg)
{
 int iii;
 StratusMessageFormat stratus_msg;
 CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
 CGameCommControl *pGameCommControl;
 CString filename;
 bool middle_of_a_game = FALSE;
 TxSerialMessage message;

    if (!pGameDevice)
		logMessage("GameCommControl::AccountInquiry - NULL game pointer.");
    else
    {
        memset(&stratus_msg, 0, sizeof(stratus_msg));

		for (iii=0; iii < 4; iii++)
		{
			stratus_msg.data[iii] = rx_msg->data[4 - iii];      // account(long), change endian
			stratus_msg.data[8 + iii] = rx_msg->data[12 - iii]; // enrollment account(long), change endian
		}

		for (iii=0; iii < 2; iii++)
		{
			stratus_msg.data[4 + iii] = rx_msg->data[6 - iii];      // club code(short), change endian
			stratus_msg.data[6 + iii] = rx_msg->data[8 - iii];      // pin number(short), change endian
			stratus_msg.data[12 + iii] = rx_msg->data[14 - iii];    // enrollment pin number(short), change endian
		}

        pGameCommControl = theApp.GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);

        if (pGameCommControl && pGameCommControl->sas_com)
        {
            middle_of_a_game = pGameCommControl->sas_com->non_volatile_ram.middle_of_a_game;

            if (!middle_of_a_game)
            {
                pGameCommControl->sas_com->non_volatile_ram.pin = (DWORD)(rx_msg->data[8] << 8) + rx_msg->data[7];

                memcpy((BYTE*)&pGameCommControl->sas_com->non_volatile_ram.member_id, &rx_msg->data[1], sizeof(DWORD));
                // save non volatile ram data
    //                filename.Format("%s%u", GAME_DATA, pGameDevice->memory_game_config.ucmc_id);
    //                fileWrite(filename.GetBuffer(0), 0, &pGameCommControl->sas_com->non_volatile_ram, sizeof(pGameCommControl->sas_com->non_volatile_ram));
                pGameCommControl->sas_com->saveNonVolatileRam();
            }
        }

        if (!middle_of_a_game)
        {
		    // card type(char), check for ascii character
            if (rx_msg->data[15] < 0x30)
                stratus_msg.data[14] = rx_msg->data[15] + 0x30;
            else
                stratus_msg.data[14] = rx_msg->data[15];

            stratus_msg.data[15] = '0'; // transaction code(char)
            memcpy(&stratus_msg.data[16], SetTimeStamp(), 14);

            if (new_login_msg)
                stratus_msg.send_code = MPI_NEW_ACCOUNT_LOGIN;
            else
                stratus_msg.send_code = MPI_ACCOUNT_LOGIN;

		    if (pGameDevice->memory_game_config.club_code)
			    stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		    else
			    stratus_msg.club_code = theApp.memory_location_config.club_code;

            stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
            stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
            stratus_msg.mux_id = rx_msg->mux_id;
            stratus_msg.message_length = 30 + STRATUS_HEADER_SIZE;
            stratus_msg.property_id = theApp.memory_location_config.property_id;
            stratus_msg.serial_port = port_info.port_number;
            stratus_msg.data_to_event_log = true;
            stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
            theApp.QueueStratusTxMessage(&stratus_msg, FALSE);
        }
        else
        {
            // if we are in the middle of a game on a direct connect game, tell the GB Interface to notify the player
            memset(&message, 0, sizeof(message));
            message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
            message.msg.length = 67;
            message.msg.command = COMMAND_HEADER;
            message.msg.data[0] = ACCOUNT_INQUIRY_RETURN;
            // copy the pin to the return message
            message.msg.data[33] = rx_msg->data[7];
            message.msg.data[34] = rx_msg->data[8];
            // flag the middle of a game
            message.msg.data[35] = 'm';

            pGameDevice->transmit_queue.push(message);
        }
    }
}

void CGameCommControl::ProcessSmartCardReaderState(RxSerialMessage *rx_msg)
{
 CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
 EmailMessageFormat email_message;

    if (!pGameDevice)
        logMessage("GameCommControl::ProcessSmartCardReaderState - NULL game pointer.");
    else
    {
        email_message.subject = "XP Controller: ";

        switch (rx_msg->data[1])
        {
            case 1: // stuck sensor
                email_message.type = PLAYER_CARD_STUCK_SENSOR;
                email_message.subject += "Stuck Smart Card Sensor In Smart Card Reader";
                break;

            case 2: // dead reader
                email_message.type = PLAYER_CARD_DEAD_READER;
                email_message.subject += "Dead Smart Card Reader";
                break;

            case 3: // abandoned card
                email_message.type = PLAYER_CARD_ABANDONED_CARD;
                email_message.subject += "Abandoned Player Card In Smart Card Reader";
                break;

            default:
                email_message.type = PLAYER_CARD_STUCK_SENSOR;
                email_message.subject += "Unknown Smart Card Reader Problem";
                break;
        }

        email_message.body.Format("Game Information:\n     Serial Port: %u\n     Mux Id: %u\n"
            "     UCMC Id: %u\n     Version: %u.%2.2u\n\nLocation Information:\n"
            "     Customer Id: %u\n     Name: %s\n     Address: %s\n",
            pGameDevice->memory_game_config.port_number, pGameDevice->memory_game_config.mux_id,
            pGameDevice->memory_game_config.ucmc_id, pGameDevice->memory_game_config.version,
            pGameDevice->memory_game_config.sub_version, pGameDevice->memory_game_config.customer_id,
            theApp.memory_location_config.name, theApp.memory_location_config.address);

        theApp.email_queue.push(email_message);
    }
}

// ticket add message from the game
// rx: ticket id(long), ticket amount(long), time stamp(char[6]), account number(long), taxable event(char)
// tx: ticket id(long), ticket amount(long), account number(long), taxable event(char), time stamp(char[6])
void CGameCommControl::ProcessTicketAdd(RxSerialMessage *rx_msg, BYTE mux_id, MemoryTicketAdd *ticket_add, WORD port_number)
{
 CGameDevice *pGameDevice = GetGamePointer(mux_id, port_number);

    if (!pGameDevice)
        logMessage("GameCommControl::ProcessTicketAdd - NULL game pointer.");
    else
    {
        TicketAdd(pGameDevice, ticket_add);

        if (rx_msg->command == TICKET_ADD)
            checkForCorrespondingGameLock(rx_msg, mux_id, port_number);
    }
}

void CGameCommControl::TicketAdd(CGameDevice *pGameDevice, MemoryTicketAdd *ticket_add)
{
 StratusMessageFormat stratus_msg;

    if (!pGameDevice)
        logMessage("GameCommControl::TicketAdd - NULL game pointer.");
    else
    {
    	// change endian
        endian4(ticket_add->ticket_id);
        endian4(ticket_add->ticket_amount);
        endian4(ticket_add->player_account);

        memset(&stratus_msg, 0, sizeof(stratus_msg));
        memcpy(stratus_msg.data, &ticket_add->ticket_id, 4);
        memcpy(&stratus_msg.data[4], &ticket_add->ticket_amount, 4);
        memcpy(&stratus_msg.data[8], &ticket_add->player_account, 4);
        stratus_msg.data[12] = ticket_add->taxable_flag;    // taxable event: char '1' if taxable, otherwise 0
        memset(&stratus_msg.data[13], '8', 6);  // time stamp, value not used from game

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

        stratus_msg.send_code = TKT_TICKET_ADD;
        stratus_msg.task_code = TKT_TASK_TICKET;
        stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
        stratus_msg.mux_id = pGameDevice->memory_game_config.mux_id;
        stratus_msg.message_length = sizeof(MemoryTicketAdd) + STRATUS_HEADER_SIZE;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.serial_port = port_info.port_number;
        stratus_msg.data_to_event_log = true;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
        theApp.QueueStratusTxMessage(&stratus_msg, TRUE);
    }
}

void CGameCommControl::checkForCorrespondingGameLock(RxSerialMessage *rx_msg, BYTE mux_id, WORD port_number)
{
    CString string_message;
    CGameDevice *pGameDevice;
    DWORD ticket_id;
    DWORD ticket_amount;
    GameLockData* game_lock_data = (GameLockData*)rx_msg->data;
    int file_index;
    FilePaidTicket file_paid_ticket;
    BOOL ticket_paid = FALSE;


    pGameDevice = GetGamePointer(mux_id);

    // get the ticket id and ticket amount from the message
    memcpy(&ticket_id, &rx_msg->data[2], 4);
    ticket_id = game_lock_data->ticket_id;
    memcpy(&ticket_amount, &rx_msg->data[6], 4);
    ticket_amount = game_lock_data->ticket_amount;

    if (!pGameDevice)
    {
        string_message.Format(" INVALID GAME LOCK\nGAME #: %u(mux)\nTICKET: %u\nAMOUNT: %9.2f\nPLEASE CALL FOR SERVICE\n\n",
                                mux_id,
                                ticket_id,
                                (double)ticket_amount / 100);
        theApp.printer_interrupt_queue.push(string_message);
    }
    else
    {
        // 'game lock path + ucmc id-amount-ticket id'
        string_message.Format("%s\\%u-%u-%u",
                              GAME_LOCKS_PATH,
                              pGameDevice->memory_game_config.ucmc_id,
                              ticket_amount,
                              ticket_id);

        // check to see if this ticket has already been paid
        file_index = 0;
        while (fileRead(PAID_TICKETS, file_index, &file_paid_ticket, sizeof(file_paid_ticket)))
        {
            if (pGameDevice->memory_game_config.ucmc_id == file_paid_ticket.memory.memory_game_lock.ucmc_id &&
                ticket_amount == file_paid_ticket.memory.memory_game_lock.ticket_amount &&
                ticket_id == file_paid_ticket.memory.memory_game_lock.ticket_id)
            {
                ticket_paid = TRUE;
                break;
            }
            else
                file_index++;
        }

        if (!ticket_paid)
        {
            CFileFind file_find;
            // if we can't find a game lock, then fake a game lock message to process
            if (!file_find.FindFile(string_message))
            {
                rx_msg->command = GAME_LOCK_MESSAGE;
                game_lock_data->mux_id = mux_id;
                if (ticket_amount >= 120000)
                    game_lock_data->tax_flag = 1;
                else
                    game_lock_data->tax_flag = 0;

                ProcessGameLockMessage(rx_msg, port_number);
            }
        }
 

    }
}

void CGameCommControl::setGameVersion(BYTE version, BYTE subversion)
{
    CGameDevice *pGameDevice = 0;

    if (pHeadGameDevice)
        pGameDevice = pHeadGameDevice;

    if (!pGameDevice)
        logMessage("GameCommControl::setGameVersion - NULL game pointer");
    else
    {
        pGameDevice->memory_game_config.version = version;
        pGameDevice->memory_game_config.sub_version = subversion;
        pGameDevice->WriteModifiedGameConfig();
    }
}


void CGameCommControl::DeviceResetMessage(RxSerialMessage *rx_msg)
{
 WORD meter_update, igt_sds;
 BYTE machine_config_bytes[TOTAL_MACHINE_TYPES + 1][8] = {{0, 0, 0, 0, 0, 0, 0, 0}, BALLY_GAMETYPE,
    CEI_GAMETYPE, IGT_GAMETYPE, WILLIAMS_GAMETYPE, IGT_GAMETYPE_2, BALLY_GAMETYPE_2};
 TxSerialMessage tx_message;
 CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);

    if (!pGameDevice)
        logMessage("GameCommControl::DeviceResetMessage - NULL game pointer.");
    else
    {
        pGameDevice->memory_game_config.version = rx_msg->data[1];
        pGameDevice->memory_game_config.sub_version = rx_msg->data[2];
        pGameDevice->memory_game_config.mux_dip_switches = rx_msg->data[3];
        pGameDevice->WriteModifiedGameConfig();

        if (pGameDevice->memory_game_config.version != GB_INTERFACE_VERSION)
        {
		    if (rx_msg->data[0] & 0x80)
			    processGameMeters(rx_msg->mux_id, &rx_msg->data[4],
			        rx_msg->length - MINIMUM_SERIAL_MESSAGE_SIZE - 4, MMI_UPDATE_GAME_METERS_WITH_PLAYER_LOGGED_IN);
		    else
			    processGameMeters(rx_msg->mux_id, &rx_msg->data[4],
			        rx_msg->length - MINIMUM_SERIAL_MESSAGE_SIZE - 4, MMI_UPDATE_GAME_METERS);
        }

		pGameDevice->idle_msg_status_bits = 0;

        // send game configuration data(turn on/off features, timing values, etc...)
        memset(&tx_message, 0, sizeof(tx_message));
        tx_message.msg.length = 33;
        tx_message.msg.mux_id = rx_msg->mux_id;
        tx_message.msg.command = COMMAND_HEADER;
        tx_message.msg.data[0] = GAME_CONFIGURATION_DATA;

        if (pGameDevice->memory_game_config.machine_type)
        {
            if (pGameDevice->memory_game_config.machine_type > TOTAL_MACHINE_TYPES)
                logMessage("GameCommControl::DeviceResetMessage - machine type index out-of-range.");
            else
                memcpy(&tx_message.msg.data[2], &machine_config_bytes[pGameDevice->memory_game_config.machine_type][0], 8);
        }
        else
        {
            tx_message.msg.data[1] = 5;     // mux param bits
			tx_message.msg.data[1] |= 0x40;	// set message encyrption
            tx_message.msg.data[2] = 0x11;  // mux enable bit

	        if (pGameDevice->memory_game_config.marker_flag)
	            tx_message.msg.data[1] |= 0x02;    // marker enable bit

	        if (theApp.memory_dispenser_config.autopay_allowed)
	            tx_message.msg.data[2] |= 0x04;    // autopay enable bit

	        if (pGameDevice->memory_game_config.jackpot_to_credit_pay)
	            tx_message.msg.data[2] |= 0x08;    // jackpot to credit enable bit

	        if (pGameDevice->memory_game_config.gamblers_bonus_flag)
	            tx_message.msg.data[2] |= 0x20;    // gamblers bonus enable bit

	        if (pGameDevice->memory_game_config.bingo_frenzy_flag)
	            tx_message.msg.data[2] |= 0x40;    // bingo frenzy enable bit

	        if (pGameDevice->memory_game_config.multi_play_poker_flag)
	            tx_message.msg.data[2] |= 0x80;    // multi play poker enable bit

			// send club code if not CEI or SAS box
			if (pGameDevice->memory_game_config.version != 3 && pGameDevice->memory_game_config.version != 7 &&
				pGameDevice->memory_game_config.version != 8)
			{
				if (pGameDevice->memory_game_config.club_code)
					tx_message.msg.data[3] = pGameDevice->memory_game_config.club_code;
				else
					tx_message.msg.data[3] = (BYTE)theApp.memory_location_config.club_code;
			}
        }

        memcpy(&tx_message.msg.data[10], &pGameDevice->memory_game_config.denom_type, 2);
        tx_message.msg.data[12] = (BYTE)port_info.port_number;
        memcpy(&tx_message.msg.data[13], &pGameDevice->memory_game_config.ucmc_id, 4);

        tx_message.msg.data[17] = 3;    // rate at which message display scrolls on mux device
        tx_message.msg.data[18] = 40;   // time for mux to determine offline state * 4
        tx_message.msg.data[23] = 2;    // card timer
        tx_message.msg.data[26] = 80;   // max dollars/game in multiples of $100

        // time for mux to send meter updates during game play * 4
        meter_update = 40;
        memcpy(&tx_message.msg.data[19], &meter_update, 2);

        // time for mux to send meter updates during idle state * 4
        meter_update = 480;
        memcpy(&tx_message.msg.data[21], &meter_update, 2);

        // IGT/SDS
        igt_sds = 2;
        memcpy(&tx_message.msg.data[24], &igt_sds, 2);

        pGameDevice->transmit_queue.push(tx_message);

        pGameDevice->SetDispenserPayoutLimit();
    }
}

void CGameCommControl::StatusNotificationMessage(RxSerialMessage *rx_msg)
{
 DWORD status_notification_id, status_notification_data;
 CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
 EmailMessageFormat email_message;

    if (!pGameDevice)
        logMessage("GameCommControl::StatusNotificationMessage - NULL game pointer.");
    else
    {
        memcpy(&status_notification_id, rx_msg->data, 4);
        memcpy(&status_notification_data, &rx_msg->data[4], 4);

        if (status_notification_id == 2001)
        {
            email_message.type = PROGRESSIVE_LINK_DOWN;
            email_message.subject = "XP Controller: Progressive Link Down";
            email_message.body.Format("Progressive Link Down, Progressive Controller Error\n\nLocation Name: %s\n"
                "Location Address: %s\nUCMC ID (Mux ID): %u (%u)\nProgressive Controller: %u\n"
                "Controller Denomination (Cents Per Credit): %u\n\n", theApp.memory_location_config.name,
                theApp.memory_location_config.address, pGameDevice->memory_game_config.ucmc_id,
                rx_msg->mux_id, (status_notification_data & 0xffff0000) >> 16, status_notification_data & 0xffff);

            theApp.email_queue.push(email_message);
        }
    }
}

// QUEUE MUX JACKPOT MESSAGE
// rx: account(long), jackpot amount(long), slip number(long)
// tx: same as rx, change endian
void CGameCommControl::ProcessJackpotMessage(RxSerialMessage *rx_msg)
{
// StratusMessageFormat stratus_msg;
 CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
 long account, jackpot_amount, slip_number;

    if (!pGameDevice)
        logMessage("GameCommControl::JackpotMessage - NULL game pointer.");
    else
    {
        memcpy (&account, (long*)&rx_msg->data[0], sizeof(long));
        memcpy (&jackpot_amount, (long*)&rx_msg->data[4], sizeof(long));
        memcpy (&slip_number, (long*)&rx_msg->data[8], sizeof(long));

        JackpotMessage(pGameDevice, account, jackpot_amount, slip_number);
    }
}

void CGameCommControl::ProcessJackpotMessage(long account, long jackpot_amount, long slip_number)
{
    CGameDevice *pGameDevice = 0;

    if (pHeadGameDevice)
        pGameDevice = pHeadGameDevice;

    if (!pGameDevice)
        logMessage("GameCommControl::ProcessJackpotMessage - NULL game pointer");
    else
        JackpotMessage(pGameDevice, account, jackpot_amount, slip_number);
}

void CGameCommControl::JackpotMessage(CGameDevice *pGameDevice, long account, long jackpot_amount, long slip_number)
{
 StratusMessageFormat stratus_msg;

 #pragma pack(1)
 struct
 {
	long account;
	long jackpot_amount;
	long slip_number;
 } jackpot_data;
 #pragma pack()


    if (!pGameDevice)
        logMessage("GameCommControl::JackpotMessage - NULL game pointer.");
    else
    {
		memset(&stratus_msg, 0, sizeof(stratus_msg));

		jackpot_data.account = account;
		jackpot_data.jackpot_amount = jackpot_amount;
		jackpot_data.slip_number = slip_number;

		endian4(jackpot_data.account);
		endian4(jackpot_data.jackpot_amount);
		endian4(jackpot_data.slip_number);

        memcpy(stratus_msg.data, &jackpot_data, sizeof(jackpot_data));

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

        stratus_msg.send_code = MPI_JACKPOT;
        stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
        stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
        stratus_msg.mux_id = pGameDevice->memory_game_config.mux_id;
        stratus_msg.message_length = 12 + STRATUS_HEADER_SIZE;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.serial_port = port_info.port_number;
        stratus_msg.data_to_event_log = true;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
        theApp.QueueStratusTxMessage(&stratus_msg, TRUE);
    }
}

void CGameCommControl::ProcessValidateMemberIdMessage(RxSerialMessage *rx_msg)
{
 CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
 DWORD account;

    if (!pGameDevice)
        logMessage("GameCommControl::ProcessValidateMemberIdMessage - NULL game pointer.");
    else
    {
        memcpy (&account, (long*)&rx_msg->data[0], sizeof(account));

        ValidateMemberId(pGameDevice, account);
    }
}

void CGameCommControl::ProcessValidateQuickEnrollNumberMessage(RxSerialMessage *rx_msg)
{
	CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
	DWORD quick_enroll_number, player_number, file_name_number;
	CString file_name, quick_enroll_number_string, seven_digit_file_name;
	CFileFind file_find;
	TxSerialMessage message;
	QuickEnrollFileStruct quick_enroll_file_struct;
	DWORD default_chosen_number = 1; // initialize this to 1 = quick enroll number not found
    bool found = FALSE;

    if (!pGameDevice)
        logMessage("GameCommControl::ProcessValidateQuickEnrollNumberMessage - NULL game pointer.");
    else
    {
		// initialize return message basic fields
		memset(&message, 0, sizeof(message));

		message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
		message.msg.length = 15;
		message.msg.command = VALIDATE_QUICK_ENROLL_NUMBER;

		// get the quick enroll number from the incoming message
        memcpy (&quick_enroll_number, (long*)&rx_msg->data[0], sizeof(quick_enroll_number));

		// find the enrollee's corresponding file on disk
		CString path_and_file = QUICK_ENROLL_PATH + "\\*.*";
		file_find.FindFile(path_and_file);
		while (!found && file_find.FindNextFile())
		{
			file_name = file_find.GetFileName();
			if (file_name.StringLength(file_name.GetBuffer()) > 7)
			{
				seven_digit_file_name = file_name.Left(7);
				file_name_number = atol(seven_digit_file_name);
				if (file_name_number == quick_enroll_number)
					found = TRUE;
			}
			else
			{
				file_name_number = atol(file_name);

				if (file_name_number == quick_enroll_number)
					found = TRUE;
			}
		}
        // check the last file found in the directory if we haven't already found the file
        if (!found)
        {
		    file_name = file_find.GetFileName();

			if (file_name.StringLength(file_name.GetBuffer()) > 7)
			{
				seven_digit_file_name = file_name.Left(7);
				file_name_number = atol(seven_digit_file_name);
				if (file_name_number == quick_enroll_number)
					found = TRUE;
			}
			else
			{
				file_name_number = atol(file_name);

				if (file_name_number == quick_enroll_number)
					found = TRUE;
			}
        }

		// see if we have a file that matches the enrollee's quick enrollment number
//		if (quick_enroll_number_string == seven_digit_file_name)
		if (found)
		{
			path_and_file = QUICK_ENROLL_PATH + "\\" + file_name;

			// read the data in the quick enrollment file
			if (fileRead(path_and_file.GetBuffer(0), 0, &quick_enroll_file_struct, sizeof(quick_enroll_file_struct)))
			{
				// create some temporary strings for each of the elements in the time date stamp
				char year_string[5];
				char month_string[3];
				char day_string[3];
				char hour_string[3];
				char minute_string[3];
				char second_string[3];
				// initialize each string holder
				memset(year_string, 0, 5);
				memset(month_string, 0, 3);
				memset(day_string, 0, 3);
				memset(hour_string, 0, 3);
				memset(minute_string, 0, 3);
				memset(second_string, 0, 3);
				// load the time date stamp info for current time comparison
				memcpy(year_string, &quick_enroll_file_struct.time_date_stamp[0], 4);
				memcpy(month_string, &quick_enroll_file_struct.time_date_stamp[4], 2);
				memcpy(day_string, &quick_enroll_file_struct.time_date_stamp[6], 2);
				memcpy(hour_string, &quick_enroll_file_struct.time_date_stamp[8], 2);
				memcpy(minute_string, &quick_enroll_file_struct.time_date_stamp[10], 2);
				memcpy(second_string, &quick_enroll_file_struct.time_date_stamp[12], 2);
				// check to see if the quick enrollment period has expired
				CTime current_time = CTime::GetCurrentTime();
				CTime file_time(atoi(year_string), atoi(month_string), atoi(day_string), atoi(hour_string),
								atoi(minute_string), atoi(second_string));
				CTimeSpan time_difference;
				if (current_time >= file_time)
					time_difference = current_time - file_time;
				else
					time_difference = 0;
				// if the player has not enrolled within 30 minutes of drivers license swipe, then error out
				if (time_difference.GetTotalMinutes() <= 30)
				{
					default_chosen_number = atol(file_name.GetBuffer());
					player_number = atol(quick_enroll_file_struct.player_number);

				}
				else
				{
					default_chosen_number = 2; // quick enrollment time has expired
					player_number = 0;
				}
				DeleteFile(path_and_file);
				memcpy(&message.msg.data[0], &default_chosen_number, 4);
				memcpy(&message.msg.data[4], &player_number, 4);
				message.msg.data[8] = quick_enroll_file_struct.status; 
				pGameDevice->transmit_queue.push(message);
			}
		}

		memcpy(&message.msg.data[0], &default_chosen_number, 4);
		memcpy(&message.msg.data[4], &player_number, 4);
		pGameDevice->transmit_queue.push(message);
    }
}

void CGameCommControl::ValidateMemberId(CGameDevice *pGameDevice, DWORD member_id)
{
 StratusMessageFormat stratus_msg;
 DWORD account = member_id;

    if (!pGameDevice)
        logMessage("GameCommControl::ValidateMemberIdMessage - NULL game pointer.");
    else
    {
		memset(&stratus_msg, 0, sizeof(stratus_msg));

		endian4(account);

        memcpy(stratus_msg.data, &account, sizeof(account));

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

        stratus_msg.send_code = MPI_VALIDATE_MEMBER_ID;
        stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
        stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
        stratus_msg.mux_id = pGameDevice->memory_game_config.mux_id;
        stratus_msg.message_length = 4 + STRATUS_HEADER_SIZE;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.serial_port = port_info.port_number;
        stratus_msg.data_to_event_log = true;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
        theApp.QueueStratusTxMessage(&stratus_msg, FALSE);
    }
}

// set game collection state
void CGameCommControl::SetGameCollectionState(BYTE state)
{
    CString printer_message;
    CGameDevice *pGameDevice = 0;

    if (pHeadGameDevice)
        pGameDevice = pHeadGameDevice;

    if (!pGameDevice)
        logMessage("GameCommControl::SetGameCollectionState - NULL game pointer");
    else
    {
        pGameDevice->SetGameCollectionState(state);
        printer_message.Format(" NET: GAME %u(%u) COLLECTED\n", pGameDevice->memory_game_config.ucmc_id,
            pGameDevice->memory_game_config.mux_id);
        theApp.printer_interrupt_queue.push(printer_message);
    }
}

BYTE CGameCommControl::GetGameCollectionState(void)
{
    BYTE state;

    CGameDevice *pGameDevice = 0;

    if (pHeadGameDevice)
        pGameDevice = pHeadGameDevice;

    if (!pGameDevice)
        logMessage("GameCommControl::GetGameCollectionState - NULL game pointer");
    else
        state = pGameDevice->memory_game_config.collection_state;

    return state;
}

// rx meters: coin in(long), coin out(long), games(long), coin drop(long), hand pay(long), resv(long), total dollars(long)
void CGameCommControl::GameCollected(RxSerialMessage *rx_msg)
{
 CString printer_message;
 CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);

    if (!pGameDevice)
        logMessage("GameCommControl::GameCollected - NULL game pointer.");
    else
    {
        pGameDevice->SetGameCollectionState(CLEAR_COLLECTION_STATE);
        processGameMeters(rx_msg->mux_id, rx_msg->data,
            rx_msg->length - MINIMUM_SERIAL_MESSAGE_SIZE, MMI_GAME_COLLECTED_METERS);

        if (pGameDevice->memory_game_config.mux_id)
            printer_message.Format(" NET: GAME %u(%u) COLLECTED\n", pGameDevice->memory_game_config.ucmc_id,
                pGameDevice->memory_game_config.mux_id);
        else
            printer_message.Format(" NET: GAME %u(%u) COLLECTED\n", pGameDevice->memory_game_config.ucmc_id,
                pGameDevice->memory_game_config.sas_id);

        theApp.printer_interrupt_queue.push(printer_message);
    }
}

void CGameCommControl::processGameDrinkComp(RxSerialMessage *rx_msg)
{
    struct
    {
        BYTE    amount;
        DWORD   ucmc_id;
    } drink_comp;

    drink_comp.amount = rx_msg->data[0];
    memcpy(&drink_comp.ucmc_id, &rx_msg->data[1], 4);

    GameDrinkComp(drink_comp.amount, drink_comp.ucmc_id);
}

void CGameCommControl::processGameDrinkComp(BYTE dollars)
{
    CGameDevice *pGameDevice = 0;

    if (pHeadGameDevice)
        pGameDevice = pHeadGameDevice;

    if (!pGameDevice)
        logMessage("GameCommControl::processGameDrinkComp - NULL game pointer");
    else
        GameDrinkComp(dollars, pGameDevice->memory_game_config.ucmc_id);
}

void CGameCommControl::GameDrinkComp(BYTE amount, DWORD ucmc_id)
{
 DrinkCompTotal drink_comp_total, drink_comp_shift;
 CString printer_message;
 FileCashierShift current_cashier_shift;

     struct
     {
        BYTE    amount;
        DWORD   ucmc_id;
     } drink_comp;

    memset(&drink_comp_shift, 0, sizeof(drink_comp_shift));
    memset(&drink_comp_total, 0, sizeof(drink_comp_total));

    drink_comp.amount = amount;
    drink_comp.ucmc_id = ucmc_id;

    if (GetFileAttributes(DRINK_COMPS_TOTAL) != 0xFFFFFFFF &&
        fileRead(DRINK_COMPS_TOTAL, 0, &drink_comp_total, sizeof(drink_comp_total)))
    {
        drink_comp_total.number++;
        drink_comp_total.total_amount += drink_comp.amount;
    }
    else
    {
        drink_comp_total.number = 1;
        drink_comp_total.total_amount = drink_comp.amount;
    }

    fileWrite(DRINK_COMPS_TOTAL, 0, &drink_comp_total, sizeof(drink_comp_total));

    if (GetFileAttributes(DRINK_COMPS_SHIFT) != 0xFFFFFFFF &&
        fileRead(DRINK_COMPS_SHIFT, 0, &drink_comp_shift, sizeof(drink_comp_shift)))
    {
        drink_comp_shift.number++;
        drink_comp_shift.total_amount += drink_comp.amount;
    }
    else
    {
        drink_comp_shift.number = 1;
        drink_comp_shift.total_amount = drink_comp.amount;
    }

    fileWrite(DRINK_COMPS_SHIFT, 0, &drink_comp_shift, sizeof(drink_comp_shift));


    if (fileRead(CASHIER_SHIFT_1, 0, &current_cashier_shift, sizeof(current_cashier_shift)))
    {
        current_cashier_shift.drink_comps.number = drink_comp_shift.number;
        current_cashier_shift.drink_comps.total_amount = drink_comp_shift.total_amount;
        fileWrite(CASHIER_SHIFT_1, 0, &current_cashier_shift, sizeof(current_cashier_shift));
    }

    if (theApp.memory_settings_ini.print_drink_comps)
    {
        printer_message.Format(" DRINK COMP\n    GAME:   %u\n    COMP: %9.2f\n\n",
            drink_comp.ucmc_id, (double)drink_comp.amount);

        theApp.printer_interrupt_queue.push(printer_message);
    }
}

void CGameCommControl::ProcessMarkerAdvanceRequest(RxSerialMessage *rx_msg)
{
 CString message_log;
 CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
 StratusMessageFormat stratus_msg;
 TxSerialMessage message;

 #pragma pack(1)
 struct
 {
	DWORD player_account;
	DWORD amount_request;
	DWORD transaction_id;
 } marker_request;
 #pragma pack()

    if (!pGameDevice)
	{
		message_log.Format("GameCommControl::ProcessMarkerAdvanceRequest - NULL game pointer, rx mux %u", rx_msg->mux_id);
        logMessage(message_log);
	}
    else if (rx_msg->length <= MINIMUM_ENCRYPTED_SERIAL_MESSAGE_SIZE)
	{
		message_log.Format("GameCommControl::ProcessMarkerAdvanceRequest - invalid length: %u", rx_msg->length);
		logMessage(message_log);
	}
	else
	{
        // ack game message
        if (pGameDevice && pGameDevice->memory_game_config.mux_id)
        {
            if (pGameDevice->memory_game_config.version != GB_INTERFACE_VERSION)
            {
                memset(&message, 0, sizeof(message));
                message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                message.msg.length = 7;
                message.msg.command = ASTERISK_HEADER;
                message.msg.data[0] = r__REQUEST_MARKER_ADVANCE_ACK;
                pGameDevice->transmit_queue.push(message);
            }
                
        }

		if (rx_msg->length - MINIMUM_ENCRYPTED_SERIAL_MESSAGE_SIZE > sizeof(marker_request))
			memcpy(&marker_request, &rx_msg->data[1], sizeof(marker_request));
		else
			memcpy(&marker_request, &rx_msg->data[1], rx_msg->length - MINIMUM_ENCRYPTED_SERIAL_MESSAGE_SIZE);

		endian4(marker_request.player_account);
		endian4(marker_request.amount_request);
		endian4(marker_request.transaction_id);

        memset(&stratus_msg, 0, sizeof(stratus_msg));

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

        stratus_msg.send_code = MPI_MARKER_REQUEST;
        stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
        stratus_msg.mux_id = rx_msg->mux_id;
        stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.data_to_event_log = true;
        stratus_msg.message_length = sizeof(marker_request) + STRATUS_HEADER_SIZE;
        memcpy(stratus_msg.data, &marker_request, sizeof(marker_request));
        theApp.QueueMarkerStratusTxMessage(&stratus_msg);
    }
}



void CGameCommControl::ProcessMarkerAftCompleteMessage(RxSerialMessage *rx_msg)
{
 CString message_log;
 CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
 StratusMessageFormat stratus_msg;
 CString prt_buf;
 DWORD account_number;
 DWORD previous_balance;
	DWORD current_balance;
	DWORD amount_advanced;
 TxSerialMessage message;
	DWORD transaction_id;

 #pragma pack(1)
 struct
 {

	DWORD credit_limit;
	DWORD current_balance;
	DWORD amount_advanced;
	DWORD transaction_id;
	char name_string[13];
 } marker_aft_complete;
 #pragma pack()

 #pragma pack(1)
 struct
 {
	DWORD transaction_id;
	DWORD player_account;
 } stratus_aft_complete;
 #pragma pack()

    if (!pGameDevice)
	{
		message_log.Format("GameCommControl::ProcessMarkerAftCompleteMessage - NULL game pointer, rx mux %u", rx_msg->mux_id);
        logMessage(message_log);
	}
    else if (rx_msg->length <= MINIMUM_ENCRYPTED_SERIAL_MESSAGE_SIZE)
	{
		message_log.Format("GameCommControl::ProcessMarkerAftCompleteMessage - invalid length: %u", rx_msg->length);
		logMessage(message_log);
	}
	else
	{
        // ack game message
        if (pGameDevice && pGameDevice->memory_game_config.mux_id)
        {
            if (pGameDevice->memory_game_config.version != GB_INTERFACE_VERSION)
            {
                memset(&message, 0, sizeof(message));
                message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                message.msg.length = 7;
                message.msg.command = ASTERISK_HEADER;
                message.msg.data[0] = t__MARKER_AFT_COMPLETE_ACK;
                pGameDevice->transmit_queue.push(message);
            }
                
        }

		if (rx_msg->length - MINIMUM_ENCRYPTED_SERIAL_MESSAGE_SIZE > sizeof(marker_aft_complete))
			memcpy(&marker_aft_complete, &rx_msg->data[1], sizeof(marker_aft_complete));
		else
			memcpy(&marker_aft_complete, &rx_msg->data[1], rx_msg->length - MINIMUM_ENCRYPTED_SERIAL_MESSAGE_SIZE);

        transaction_id = marker_aft_complete.transaction_id;
		endian4(marker_aft_complete.transaction_id);
        memset(&stratus_msg, 0, sizeof(stratus_msg));

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

        stratus_msg.send_code = MPI_MARKER_AFT_COMPLETE;
        stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
        stratus_msg.mux_id = rx_msg->mux_id;
        stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.data_to_event_log = true;
        stratus_msg.message_length = sizeof(stratus_aft_complete) + STRATUS_HEADER_SIZE;
        memcpy(stratus_msg.data, &marker_aft_complete.transaction_id, sizeof(marker_aft_complete.transaction_id));
        account_number = pGameDevice->account_login_data.account_number;
		endian4(account_number);
        memcpy(&stratus_msg.data[4], &account_number, sizeof(account_number));
        theApp.QueueMarkerStratusTxMessage(&stratus_msg);

        // print marker advance receipt
		if (marker_aft_complete.amount_advanced)
		{
	        prt_buf.Format("\n\nMARKER ADVANCE RECEIPT\n");
	        theApp.MarkerPrinterData(prt_buf.GetBuffer(0));
	        theApp.PrinterHeader(2);
	        prt_buf.Format("\nLOCATION ID: %u", pGameDevice->memory_game_config.customer_id);
	        theApp.MarkerPrinterData(prt_buf.GetBuffer(0));
	        prt_buf.Format("\nMACHINE ID: %u", pGameDevice->memory_game_config.ucmc_id);
	        theApp.MarkerPrinterData(prt_buf.GetBuffer(0));
            marker_aft_complete.name_string[12] = 0;
	        prt_buf.Format("\n\nNAME:    %s", marker_aft_complete.name_string);
	        theApp.MarkerPrinterData(prt_buf.GetBuffer(0));
	        prt_buf.Format("\nACCOUNT: %u", pGameDevice->account_login_data.account_number);
	        theApp.MarkerPrinterData(prt_buf.GetBuffer(0));
	        prt_buf.Format("\nTRANSACTION ID: %u%u", pGameDevice->memory_game_config.ucmc_id, transaction_id);
	        theApp.MarkerPrinterData(prt_buf.GetBuffer(0));

			current_balance = marker_aft_complete.current_balance;
			amount_advanced = marker_aft_complete.amount_advanced;

            if (marker_aft_complete.current_balance >= marker_aft_complete.amount_advanced)
                previous_balance = marker_aft_complete.current_balance - marker_aft_complete.amount_advanced;
            else
                previous_balance = marker_aft_complete.amount_advanced - marker_aft_complete.current_balance;
	        prt_buf.Format("\n\nPREVIOUS BALANCE DUE:      $%9.2f", (double)previous_balance / 100);
	        theApp.MarkerPrinterData(prt_buf.GetBuffer(0));

	        prt_buf.Format("\nADVANCE AMOUNT:            $%9.2f", (double)marker_aft_complete.amount_advanced / 100);
	        theApp.MarkerPrinterData(prt_buf.GetBuffer(0));

	        prt_buf.Format("\nNEW BALANCE DUE:           $%9.2f", (double)marker_aft_complete.current_balance / 100);
	        theApp.MarkerPrinterData(prt_buf.GetBuffer(0));




	        prt_buf.Format("\n\nORIGINAL MARKER AMOUNT:    $%9.2f", (double)marker_aft_complete.credit_limit / 100);
	        theApp.MarkerPrinterData(prt_buf.GetBuffer(0));
	        theApp.MarkerPrinterData("\nAVAILABLE WAGERING");
	        prt_buf.Format("\n     ACCOUNT BALANCE:      $%9.2f", ((double)marker_aft_complete.credit_limit - (double)marker_aft_complete.current_balance) / 100);
	        theApp.MarkerPrinterData(prt_buf.GetBuffer(0));

	        prt_buf.Format("\n\n\nPlayer Signature: \n\n");
	        theApp.MarkerPrinterData(prt_buf.GetBuffer(0));
	        prt_buf.Format("\n_______________________________\n\n");
	        theApp.MarkerPrinterData(prt_buf.GetBuffer(0));

	        prt_buf.Format("\n\n\nCashier Signature: \n\n");
	        theApp.MarkerPrinterData(prt_buf.GetBuffer(0));
	        prt_buf.Format("\n_______________________________\n\n");
	        theApp.MarkerPrinterData(prt_buf.GetBuffer(0));

	        theApp.MarkerPrinterData("\n\n\n\n\n\n\n\n\n\n\n");
		}

    }
}


void CGameCommControl::ProcessAbortMarkerAdvanceRequest(RxSerialMessage *rx_msg)
{
 CString message_log;
 CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
 StratusMessageFormat stratus_msg;

 #pragma pack(1)
 struct
 {
	DWORD transaction_id;
	DWORD player_account;
 } marker_abort_request;
 #pragma pack()

    if (!pGameDevice)
	{
		message_log.Format("GameCommControl::ProcessAbortMarkerAdvanceRequest - NULL game pointer, rx mux %u", rx_msg->mux_id);
        logMessage(message_log);
	}
    else if (rx_msg->length <= MINIMUM_ENCRYPTED_SERIAL_MESSAGE_SIZE)
	{
		message_log.Format("GameCommControl::ProcessAbortMarkerAdvanceRequest - invalid length: %u", rx_msg->length);
		logMessage(message_log);
	}
	else
	{
		if (rx_msg->length - MINIMUM_ENCRYPTED_SERIAL_MESSAGE_SIZE > sizeof(marker_abort_request))
			memcpy(&marker_abort_request, &rx_msg->data[1], sizeof(marker_abort_request));
		else
			memcpy(&marker_abort_request, &rx_msg->data[1], rx_msg->length - MINIMUM_ENCRYPTED_SERIAL_MESSAGE_SIZE);

		endian4(marker_abort_request.player_account);
		endian4(marker_abort_request.transaction_id);

        memset(&stratus_msg, 0, sizeof(stratus_msg));

		if (pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

        stratus_msg.send_code = MPI_ABORT_MARKER_ADVANCE_REQUEST;
        stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
        stratus_msg.mux_id = rx_msg->mux_id;
        stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
        stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
        stratus_msg.property_id = theApp.memory_location_config.property_id;
        stratus_msg.data_to_event_log = true;
        stratus_msg.message_length = sizeof(marker_abort_request) + STRATUS_HEADER_SIZE;
        memcpy(stratus_msg.data, &marker_abort_request, sizeof(marker_abort_request));
        theApp.QueueMarkerStratusTxMessage(&stratus_msg);
    }
}

void CGameCommControl::ProcessGameLockMessage(RxSerialMessage *rx_msg, WORD port_number)
{
 CString string_message, printer_message;
 FileGameLock file_game_lock;
 CGameDevice *pGameDevice = 0;
 MemoryTicketAdd memory_ticket_add;
 StratusMarkerCreditPending stratus_marker_credit_pending;
 StratusMessageFormat stratus_msg;
 TxSerialMessage message;

    memset(&file_game_lock, 0, sizeof(file_game_lock));

    switch (rx_msg->command)
    {
        case GAME_LOCK_MESSAGE:
            memcpy(&file_game_lock.game_lock.ticket_id, rx_msg->data, 4);
            memcpy(&file_game_lock.game_lock.ticket_amount, &rx_msg->data[4], 4);
            memcpy(file_game_lock.game_lock.time_stamp, &rx_msg->data[8], 6);
            file_game_lock.game_lock.mux_id = rx_msg->data[14];
            file_game_lock.game_lock.sas_id = 0;

            if (rx_msg->length < 16 + MINIMUM_SERIAL_MESSAGE_SIZE)
            {
                if (file_game_lock.game_lock.ticket_amount >= 120000)
                    file_game_lock.game_lock.taxable_flag = 1;
            }
            else
                file_game_lock.game_lock.taxable_flag = rx_msg->data[15];
            break;

        case GAME_LOCK_TICKET_ADD:
            memcpy(&file_game_lock.game_lock.ticket_id, rx_msg->data, 4);
            memcpy(&file_game_lock.game_lock.ticket_amount, &rx_msg->data[4], 4);
            memcpy(file_game_lock.game_lock.time_stamp, &rx_msg->data[8], 6);
            memcpy(&file_game_lock.game_lock.player_account, &rx_msg->data[16], 4);

            file_game_lock.game_lock.mux_id = rx_msg->data[14];
            file_game_lock.game_lock.sas_id = 0;

            if (rx_msg->length < 16 + MINIMUM_SERIAL_MESSAGE_SIZE)
            {
                if (file_game_lock.game_lock.ticket_amount >= 120000)
                    file_game_lock.game_lock.taxable_flag = 1;
            }
            else
                file_game_lock.game_lock.taxable_flag = rx_msg->data[15];

				pGameDevice = theApp.GetGamePointer(file_game_lock.game_lock.mux_id, port_number);
                if (pGameDevice)
                {
                    file_game_lock.game_lock.ucmc_id = pGameDevice->memory_game_config.ucmc_id;

                    // ack game message
                    if (pGameDevice->memory_game_config.mux_id)
                    {
                        if (pGameDevice->memory_game_config.version != GB_INTERFACE_VERSION)
                        {
                            memset(&message, 0, sizeof(message));
                            message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                            message.msg.length = 10;
                            message.msg.command = GAME_LOCK_TICKET_ADD;
                            memcpy(&message.msg.data[0], &file_game_lock.game_lock.ticket_id, 4);
                            pGameDevice->transmit_queue.push(message);
                        }
                        
                    }
                }

                if (file_game_lock.game_lock.ticket_amount)
                {
                    memset(&memory_ticket_add, 0, sizeof(memory_ticket_add));
                    memory_ticket_add.ticket_id = file_game_lock.game_lock.ticket_id;
                    memory_ticket_add.ticket_amount = file_game_lock.game_lock.ticket_amount - file_game_lock.game_lock.marker_balance;
                    memory_ticket_add.player_account = file_game_lock.game_lock.player_account;
                    memory_ticket_add.taxable_flag = file_game_lock.game_lock.taxable_flag;
                    memcpy(memory_ticket_add.time_stamp, file_game_lock.game_lock.time_stamp, 6);
//                    ProcessTicketAdd(rx_msg, file_game_lock.game_lock.mux_id, &memory_ticket_add);
                    TicketAdd(pGameDevice, &memory_ticket_add);
                }
            break;

		case AMPERSAND_HEADER:
            if (rx_msg->data[0] != JACKPOT_GAME_LOCK)
                return;
            else
            {
                file_game_lock.game_lock.mux_id = rx_msg->data[1];
                file_game_lock.game_lock.sas_id = 0;
                memcpy(&file_game_lock.game_lock.ticket_id, &rx_msg->data[2], 4);
                memcpy(&file_game_lock.game_lock.ticket_amount, &rx_msg->data[6], 4);
                memcpy(&file_game_lock.game_lock.player_account, &rx_msg->data[10], 4);
                memcpy(file_game_lock.game_lock.time_stamp, &rx_msg->data[14], 6);
                file_game_lock.game_lock.taxable_flag = 1;
                file_game_lock.game_lock.jackpot_to_credit_flag = 1;
            }
            break;

        case ASTERISK_HEADER:
            if (rx_msg->data[0] != q__LOCKED_CASHOUT_EVENT)
                return;
            else
            {
                pGameDevice = GetGamePointer(rx_msg->data[23]);

                // ack game message
                if (pGameDevice && pGameDevice->memory_game_config.mux_id)
                {
                    if (pGameDevice->memory_game_config.version != GB_INTERFACE_VERSION)
                    {
                        memset(&message, 0, sizeof(message));
                        message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
                        message.msg.length = 11;
                        message.msg.command = ASTERISK_HEADER;
                        message.msg.data[0] = q__LOCKED_CASHOUT_EVENT;
                        memcpy(&message.msg.data[1], &rx_msg->data[5], 4);
                        pGameDevice->transmit_queue.push(message);
                    }
                        
                }

				file_game_lock.game_lock.mux_id = rx_msg->data[23];
                file_game_lock.game_lock.sas_id = 0;
                file_game_lock.game_lock.taxable_flag = rx_msg->data[24];
                file_game_lock.game_lock.jackpot_to_credit_flag = rx_msg->data[25];
                memcpy(&file_game_lock.game_lock.player_account, &rx_msg->data[1], 4);
                memcpy(&file_game_lock.game_lock.ticket_id, &rx_msg->data[5], 4);
                memcpy(&file_game_lock.game_lock.ticket_amount, &rx_msg->data[9], 4);
                memcpy(&file_game_lock.game_lock.marker_balance, &rx_msg->data[13], 4);
                memcpy(file_game_lock.game_lock.time_stamp, &rx_msg->data[17], 6);
                memcpy(&file_game_lock.game_lock.player_name, &rx_msg->data[26], 13);

//				pGameDevice = theApp.GetGamePointer(file_game_lock.game_lock.mux_id);
				pGameDevice = theApp.GetGamePointer(file_game_lock.game_lock.mux_id, port_number);
                if (pGameDevice)
                    file_game_lock.game_lock.ucmc_id = pGameDevice->memory_game_config.ucmc_id;

                if (file_game_lock.game_lock.ticket_amount > file_game_lock.game_lock.marker_balance)
                {
                    memset(&memory_ticket_add, 0, sizeof(memory_ticket_add));
                    memory_ticket_add.ticket_id = file_game_lock.game_lock.ticket_id;
                    memory_ticket_add.ticket_amount = file_game_lock.game_lock.ticket_amount - file_game_lock.game_lock.marker_balance;
                    memory_ticket_add.player_account = file_game_lock.game_lock.player_account;
                    memory_ticket_add.taxable_flag = file_game_lock.game_lock.taxable_flag;
                    memcpy(memory_ticket_add.time_stamp, file_game_lock.game_lock.time_stamp, 6);
                    ProcessTicketAdd(rx_msg, file_game_lock.game_lock.mux_id, &memory_ticket_add, port_number);
                }

                if (file_game_lock.game_lock.marker_balance)
                {
                    memset(&stratus_marker_credit_pending, 0, sizeof(stratus_marker_credit_pending));

                    stratus_marker_credit_pending.player_account = file_game_lock.game_lock.player_account;

                    if (file_game_lock.game_lock.marker_balance > file_game_lock.game_lock.ticket_amount)
                        stratus_marker_credit_pending.credit_amount = file_game_lock.game_lock.ticket_amount;
                    else
                        stratus_marker_credit_pending.credit_amount = file_game_lock.game_lock.marker_balance;

                    endian4(stratus_marker_credit_pending.player_account);
                    endian4(stratus_marker_credit_pending.credit_amount);

                    memset(&stratus_msg, 0, sizeof(stratus_msg));

					if (pGameDevice && pGameDevice->memory_game_config.club_code)
				        stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
			        else
				        stratus_msg.club_code = theApp.memory_location_config.club_code;

                    stratus_msg.send_code = MPI_MARKER_CREDIT_PENDING;
                    stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
                     stratus_msg.mux_id = file_game_lock.game_lock.mux_id;
					stratus_msg.ucmc_id = file_game_lock.game_lock.ucmc_id;
                    stratus_msg.message_length = sizeof(stratus_marker_credit_pending) + STRATUS_HEADER_SIZE;
                    stratus_msg.property_id = theApp.memory_location_config.property_id;
                    stratus_msg.data_to_event_log = true;
                    stratus_msg.customer_id = file_game_lock.game_lock.location_id;
                    memcpy(stratus_msg.data, &stratus_marker_credit_pending, sizeof(stratus_marker_credit_pending));
                    theApp.QueueMarkerStratusTxMessage(&stratus_msg);
                }
            }
        break;

        default:
            return;
    }

    pGameDevice = GetGamePointer(file_game_lock.game_lock.mux_id);

    if (!pGameDevice)
        logMessage("GameCommControl::ProcessGameLockMessage - NULL game pointer.");
    else
        GameLockMessage(pGameDevice, &file_game_lock);
}

void CGameCommControl::ProcessGameLockMessage(FileGameLock* file_game_lock, MemoryTicketAdd* ticket_add)
{
 CString string_message, printer_message;
 CGameDevice *pGameDevice;

    if (pHeadGameDevice)
        pGameDevice = pHeadGameDevice;

    if (!pGameDevice)
    {
        string_message.Format(" INVALID GAME LOCK\nGAME #: %u(mux)\nTICKET: %u\nAMOUNT: %9.2f\nPLEASE CALL FOR SERVICE\n\n",
            file_game_lock->game_lock.mux_id, file_game_lock->game_lock.ticket_id, (double)file_game_lock->game_lock.ticket_amount / 100);
        theApp.printer_interrupt_queue.push(string_message);
    }
    else
    {
        GameLockMessage(pGameDevice, file_game_lock);
        TicketAdd(pGameDevice, ticket_add);
    }
}

void CGameCommControl::GameLockMessage(CGameDevice *pGameDevice, FileGameLock* file_game_lock)
{
 bool send_to_game_lock_printer;
 CString string_message, printer_message;
 BYTE id;


    if (!pGameDevice)
    {
        string_message.Format(" INVALID GAME LOCK\nGAME #: %u(mux)\nTICKET: %u\nAMOUNT: %9.2f\nPLEASE CALL FOR SERVICE\n\n",
            file_game_lock->game_lock.mux_id, file_game_lock->game_lock.ticket_id, (double)file_game_lock->game_lock.ticket_amount / 100);
        theApp.printer_interrupt_queue.push(string_message);
    }
    else
    {
        // sanity check the amount sent from the game - make sure under $100K
        if (file_game_lock->game_lock.ticket_amount <= 10000000)
        {

            if (file_game_lock->game_lock.taxable_flag < 2)
            {
                file_game_lock->game_lock.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
                file_game_lock->game_lock.game_port = pGameDevice->memory_game_config.port_number;
                file_game_lock->game_lock.location_id = pGameDevice->memory_game_config.customer_id;

                // 'game lock path + ucmc id-amount-ticket id'
                string_message.Format("%s\\%u-%u-%u", GAME_LOCKS_PATH, file_game_lock->game_lock.ucmc_id,
                    file_game_lock->game_lock.ticket_amount, file_game_lock->game_lock.ticket_id);
                fileWrite(string_message.GetBuffer(0), 0, file_game_lock, sizeof(FileGameLock));  

                if (file_game_lock->game_lock.jackpot_to_credit_flag)
                {
                    if (file_game_lock->game_lock.taxable_flag)
                        printer_message = " TAXABLE CREDIT GAME LOCK\n";
                    else
                        printer_message = " NON-TAXABLE CREDIT GAME LOCK\n";
                }
                else
                {
                    if (file_game_lock->game_lock.taxable_flag)
                        printer_message = " JACKPOT GAME LOCK\n";
                    else
                        printer_message = " GAME LOCK\n";
                }

                if (theApp.memory_settings_ini.game_lock_printer_ip_address && pGameDevice->memory_game_config.block_remote_game_unlocks)
                    send_to_game_lock_printer = true;
                else
                    send_to_game_lock_printer = false;

                theApp.flash_active_home_light = TRUE;

				theApp.SendIdcGameLockReceivedMessage (file_game_lock);
                
                if (file_game_lock->game_lock.mux_id)
                    id = file_game_lock->game_lock.mux_id;
                else
                    id = file_game_lock->game_lock.sas_id;

                string_message.Format("GAME #: %u(%u)\nTICKET: %u\nAMOUNT: %7.2f\n",
                    file_game_lock->game_lock.ucmc_id, id,
                    file_game_lock->game_lock.ticket_id, (double)file_game_lock->game_lock.ticket_amount / 100);
                printer_message += string_message;

                if (!file_game_lock->game_lock.jackpot_to_credit_flag && file_game_lock->game_lock.marker_balance)
                {
                    string_message.Format("MARKER AMOUNT: %7.2f\nACCOUNT: %u\n\n",
                        (double)file_game_lock->game_lock.marker_balance / 100, file_game_lock->game_lock.player_account);
                }
                else
                    string_message = "\n";
                printer_message += string_message;

                if (send_to_game_lock_printer)
                {
                    theApp.PrinterHeader(1);
                    theApp.game_lock_printer_queue.push(printer_message);
                }
                else
                    theApp.printer_interrupt_queue.push(printer_message);
            }
            else
            {
                logMessage("CGameCommControl::ProcessGameLockMessage - taxable flag > 1\n");
            }
        }
        else 
            logMessage("CGameCommControl::ProcessGameLockMessage - ticket amount above $20K\n");

    }
}

void CGameCommControl::ProcessInitializeGbInterface(RxSerialMessage *rx_msg)
{
    GbInterfaceInitialize gb_interface_initialize;
    TxSerialMessage tx_message;
    CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
    CGameCommControl *pGameCommControl = 0;

    if (!pGameDevice)
        logMessage("GameCommControl::ProcessInitializeGbInterface - NULL game pointer.");
    else
    {
        pGameCommControl = theApp.GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);

        memset(&gb_interface_initialize, 0, sizeof(GbInterfaceInitialize));

        if (pGameCommControl)
        {
            memcpy(gb_interface_initialize.name, pGameCommControl->sas_com->non_volatile_ram.gb_player_name, MAX_CUSTOMER_NAME);
            gb_interface_initialize.account_number = pGameCommControl->sas_com->non_volatile_ram.member_id;
            gb_interface_initialize.whole_points = pGameCommControl->sas_com->non_volatile_ram.whole_points;
            gb_interface_initialize.primary_card_marks = pGameCommControl->sas_com->non_volatile_ram.bingo.primary.card;
            gb_interface_initialize.primary_card_award = pGameCommControl->sas_com->calculateCardAward (pGameCommControl->sas_com->non_volatile_ram.bingo.primary.handle_to_date,
								                         (unsigned short)pGameCommControl->sas_com->non_volatile_ram.bingo.primary.games_to_date);
            gb_interface_initialize.secondary_card_marks = pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.card;
            gb_interface_initialize.secondary_card_award = pGameCommControl->sas_com->calculateCardAward (pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.handle_to_date,
								                         (unsigned short)pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.games_to_date);
            gb_interface_initialize.pin_number = pGameCommControl->sas_com->non_volatile_ram.pin;
            gb_interface_initialize.club_code = pGameCommControl->sas_com->non_volatile_ram.club_code;
            gb_interface_initialize.manufacturer_id[0] = pGameCommControl->sas_com->non_volatile_ram.manufacturer_id[0];
            gb_interface_initialize.manufacturer_id[1] = pGameCommControl->sas_com->non_volatile_ram.manufacturer_id[1];
            gb_interface_initialize.reserved = 0;

            // send game configuration data(turn on/off features, timing values, etc...)
            memset(&tx_message, 0, sizeof(tx_message));
            tx_message.msg.length = 53;
            tx_message.msg.mux_id = rx_msg->mux_id;
            tx_message.msg.command = CARET_HEADER;
            tx_message.msg.data[0] = INITIALIZE_GB_INTERFACE;

            memcpy(&tx_message.msg.data[1], (BYTE*)&gb_interface_initialize, sizeof(GbInterfaceInitialize));

//            pGameDevice->transmit_queue.push(tx_message);
           pGameDevice->low_priority_transmit_queue.push(tx_message);

            theApp.updateSasCommunicationStatus(pGameCommControl->sas_com->sas_communicating, pGameDevice->memory_game_config.ucmc_id);
        }
    }
}

void CGameCommControl::ProcessViewCardsInfoRequest(RxSerialMessage *rx_msg)
{
    ViewCardsInfo view_cards_info;
    TxSerialMessage tx_message;
    CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
    CGameCommControl *pGameCommControl = 0;

    if (!pGameDevice)
        logMessage("GameCommControl::ProcessViewCardsInfoRequest - NULL game pointer.");
    else
    {
        pGameCommControl = theApp.GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);

        memset(&view_cards_info, 0, sizeof(ViewCardsInfo));

        if (pGameCommControl)
        {
            view_cards_info.primary_card_handle = pGameCommControl->sas_com->non_volatile_ram.bingo.primary.handle_to_date;
            view_cards_info.primary_card_games = pGameCommControl->sas_com->non_volatile_ram.bingo.primary.games_to_date;
            view_cards_info.secondary_card_handle = pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.handle_to_date;
            view_cards_info.secondary_card_games = pGameCommControl->sas_com->non_volatile_ram.bingo.secondary.games_to_date;
            view_cards_info.current_denomination = pGameCommControl->sas_com->non_volatile_ram.current_game_denomination;
            // send game configuration data(turn on/off features, timing values, etc...)
            memset(&tx_message, 0, sizeof(tx_message));
            tx_message.msg.length = 27;
            tx_message.msg.mux_id = rx_msg->mux_id;
            tx_message.msg.command = CARET_HEADER;
            tx_message.msg.data[0] = VIEW_CARDS_INFO_REQUEST;

            memcpy(&tx_message.msg.data[1], (BYTE*)&view_cards_info, sizeof(ViewCardsInfo));

//            pGameDevice->transmit_queue.push(tx_message);
           pGameDevice->low_priority_transmit_queue.push(tx_message);
        }
    }
}


void CGameCommControl::ProcessSasCommunicationsStatusRequest(RxSerialMessage *rx_msg)
{
    TxSerialMessage tx_message;
    CGameDevice *pGameDevice = GetGamePointer(rx_msg->mux_id);
    CGameCommControl *pGameCommControl = 0;

    if (!pGameDevice)
        logMessage("GameCommControl::ProcessSasCommunicationsStatusRequest - NULL game pointer.");
    else
    {
        pGameCommControl = theApp.GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);

        if (pGameCommControl)
        {
            // send game configuration data(turn on/off features, timing values, etc...)
            memset(&tx_message, 0, sizeof(tx_message));
            tx_message.msg.length = 8;
            tx_message.msg.mux_id = rx_msg->mux_id;
            tx_message.msg.command = CARET_HEADER;
            tx_message.msg.data[0] = SAS_COMMUNICATION_STATUS;
			tx_message.msg.data[1] = pGameCommControl->sas_com->sas_communicating;

//            pGameDevice->transmit_queue.push(tx_message);
           pGameDevice->low_priority_transmit_queue.push(tx_message);
        }
    }
}

void CGameCommControl::updateGbInterfaceData (void)
{
    UpdateGbInterfaceData update_gb_interface_data;
    CGameDevice *pGameDevice = 0;

    if (sas_com && sas_com->sas_id)
    {
        CGameDevice *pGameDevice = pHeadGameDevice;

        if (!pGameDevice)
            logMessage("GameCommControl::updateGbInterfaceData - NULL game pointer.");
        else
        {
            memset(&update_gb_interface_data, 0, sizeof(UpdateGbInterfaceData));
	        if ((sas_com->getGbSystemState() == ON_AVAIL) || (sas_com->getGbSystemState() == ON_UNAVAIL))
			{
				update_gb_interface_data.whole_points = sas_com->non_volatile_ram.whole_points;
				update_gb_interface_data.primary_card_award = sas_com->calculateCardAward (sas_com->non_volatile_ram.bingo.primary.handle_to_date,
																						   sas_com->non_volatile_ram.bingo.primary.games_to_date);
				update_gb_interface_data.secondary_card_award = sas_com->calculateCardAward (sas_com->non_volatile_ram.bingo.secondary.handle_to_date,
																							 sas_com->non_volatile_ram.bingo.secondary.games_to_date);
                update_gb_interface_data.logged_in_flag = TRUE;
			}
			else
                update_gb_interface_data.logged_in_flag = FALSE;

			update_gb_interface_data.current_bank_meter = sas_com->non_volatile_ram.current_bank_meter;
			update_gb_interface_data.max_bet = sas_com->non_volatile_ram.max_bet;
            update_gb_interface_data.door_state = sas_com->non_volatile_ram.door_state;

            theApp.updateGbInterfaceData(&update_gb_interface_data, pGameDevice->memory_game_config.ucmc_id);
        }
	}
}

void CGameCommControl::initializeGbInterfaceData (void)
{
    GbInterfaceInitialize gb_interface_initialize;
    CGameDevice *pGameDevice = 0;

    if (sas_com && sas_com->sas_id)
    {
        if ((sas_com->getGbSystemState() == ON_AVAIL) || (sas_com->getGbSystemState() == ON_UNAVAIL))
        {
            CGameDevice *pGameDevice = pHeadGameDevice;

            if (!pGameDevice)
                logMessage("GameCommControl::initializeGbInterfaceData - NULL game pointer.");
            else
            {
                memset(&gb_interface_initialize, 0, sizeof(GbInterfaceInitialize));
                memcpy(gb_interface_initialize.name, sas_com->non_volatile_ram.gb_player_name, MAX_CUSTOMER_NAME);
                gb_interface_initialize.account_number = sas_com->non_volatile_ram.member_id;
                gb_interface_initialize.whole_points = sas_com->non_volatile_ram.whole_points;
                gb_interface_initialize.primary_card_marks = sas_com->non_volatile_ram.bingo.primary.card;
                gb_interface_initialize.primary_card_award = sas_com->calculateCardAward (sas_com->non_volatile_ram.bingo.primary.handle_to_date,
								                             (unsigned short)sas_com->non_volatile_ram.bingo.primary.games_to_date);
                gb_interface_initialize.secondary_card_marks = sas_com->non_volatile_ram.bingo.secondary.card;
                gb_interface_initialize.secondary_card_award = sas_com->calculateCardAward (sas_com->non_volatile_ram.bingo.secondary.handle_to_date,
								                             (unsigned short)sas_com->non_volatile_ram.bingo.secondary.games_to_date);
                gb_interface_initialize.pin_number = sas_com->non_volatile_ram.pin;
                gb_interface_initialize.club_code = sas_com->non_volatile_ram.club_code;
                gb_interface_initialize.manufacturer_id[0] = sas_com->non_volatile_ram.manufacturer_id[0];
                gb_interface_initialize.manufacturer_id[1] = sas_com->non_volatile_ram.manufacturer_id[1];
                gb_interface_initialize.reserved = 0;

                theApp.initializeGbInterfaceData(&gb_interface_initialize, pGameDevice->memory_game_config.ucmc_id);
            }
        }
    }
    else
        logMessage("GameCommControl::initializeGbInterfaceData - sas_com not initialized.");
}

void CGameCommControl::updateSasCommunicationStatus (bool communicating)
{
    CGameDevice *pGameDevice = 0;
    CGameCommControl *pGameCommControl = 0;

    if (sas_com && sas_com->sas_id)
    {
        CGameDevice *pGameDevice = this->pHeadGameDevice;

        if (!pGameDevice)
            logMessage("GameCommControl::updateSasCommunicationStatus - NULL game pointer.");
        else
        {
            theApp.updateSasCommunicationStatus(communicating, pGameDevice->memory_game_config.ucmc_id);
        }
    }
    else
        logMessage("GameCommControl::updateSasCommunicationStatus - sas_com not initialized.");
}

void CGameCommControl::updateGbInterfaceWithBingoWinningHandInfo(ULONG win_code, ULONG handle, ULONG games, BYTE card_type)
{
    BingoWinningHand bingo_winning_hand;
    CGameDevice *pGameDevice = 0;
    CGameCommControl *pGameCommControl = 0;

    if (sas_com && sas_com->sas_id)
    {
        CGameDevice *pGameDevice = pHeadGameDevice;

        if (!pGameDevice)
            logMessage("GameCommControl::updateGbInterfaceWithBingoWinningHandInfo - NULL game pointer.");
        else
        {
            memset(&bingo_winning_hand, 0, sizeof(BingoWinningHand));

            bingo_winning_hand.win_code = win_code;
            bingo_winning_hand.handle = handle;
            bingo_winning_hand.games = games;
            bingo_winning_hand.card_type = card_type;

            theApp.updateGbInterfaceWithBingoWinningHandInfo(&bingo_winning_hand, pGameDevice->memory_game_config.ucmc_id);
        }
    }
    else
        logMessage("GameCommControl::updateGbInterfaceWithBingoWinningHandInfo - sas_com not initialized.");
}

void CGameCommControl::updateGbInterfaceWithBingoWinningCardInfo (void)
{
    CGameDevice *pGameDevice = 0;
    CGameCommControl *pGameCommControl = 0;
    UpdateGbInterfaceWinningCardData update_winning_card_data;

    if (sas_com && sas_com->sas_id)
    {
        CGameDevice *pGameDevice = pHeadGameDevice;

        if (!pGameDevice)
            logMessage("GameCommControl::updateGbInterfaceWithBingoWinningCardInfo - NULL game pointer.");
        else
        {
			update_winning_card_data.primary_card_award = sas_com->calculateCardAward (sas_com->non_volatile_ram.bingo.primary.handle_to_date,
																					   sas_com->non_volatile_ram.bingo.primary.games_to_date);
			update_winning_card_data.secondary_card_award = sas_com->calculateCardAward (sas_com->non_volatile_ram.bingo.secondary.handle_to_date,
																						 sas_com->non_volatile_ram.bingo.secondary.games_to_date);

            theApp.updateGbInterfaceWithBingoWinningCardInfo(&update_winning_card_data, pGameDevice->memory_game_config.ucmc_id);
        }
    }
    else
        logMessage("GameCommControl::updateGbInterfaceWithBingoWinningCardInfo - sas_com not initialized.");
}

void CGameCommControl::SendIdcGameEnteredMessage (char* paytable_id, char* manufacturer_id)
{
    if (pHeadGameDevice)
        theApp.SendIdcGameEnteredMessage (pHeadGameDevice, paytable_id, manufacturer_id);
}

void CGameCommControl::SendIdcStackerPullMessage (BYTE pulled)
{
    if (pHeadGameDevice)
        theApp.SendIdcStackerPullMessage (pHeadGameDevice, pulled);
}


// Process mux ack message
void CGameCommControl::ProcessRxAck()
{
 CGameDevice *pGameDevice = NULL;
 EventMessage event_message;

    if (current_tx_message.msg.mux_id != CONTROLLER_MUX_ADDRESS)
    {
        pGameDevice = GetGamePointer(current_tx_message.msg.mux_id);

        if (!pGameDevice)
            logMessage("GameCommControl::ProcessRxAck - NULL game pointer.");
        else
        {
            if (current_tx_message.msg.command == START_COLLECTION)
                pGameDevice->SetGameCollectionState(ARMED_COLLECTION_STATE);
            else
                if (current_tx_message.msg.command == COMMAND_HEADER &&
                    current_tx_message.msg.data[0] == REQUEST_AFTER_DROP_METERS)
                        pGameDevice->SetGameCollectionState(CLEAR_COLLECTION_STATE);
        }
    }

    memset(&event_message, 0, sizeof(event_message));

    switch (current_tx_message.msg.command)
    {
        case CLEAR_TICKET_FROM_GAME:
        case START_COLLECTION:
        case REQUEST_DROP_METERS:
        case UNLOCK_GAME:
            event_message.head.data_length = current_tx_message.msg.length;
            break;

        case COMMAND_HEADER:
            switch (current_tx_message.msg.data[0])
            {
                case ACCOUNT_INQUIRY_RETURN:
                case WINNING_BINGO_CARD:
                case WINNING_WIDE_AREA_AWARD:
                case V__REDEEM_INQUIRY:
                case W__REDEEM_REQUEST:
                case w__REDEEM_REQUEST:
                case REQUEST_AFTER_DROP_METERS:
                case GAME_CONFIGURATION_DATA:
                case RESET_MUX:
                    event_message.head.data_length = current_tx_message.msg.length;
                    break;
            }
            break;

		case CASH_FRENZY_HEADER:
			if (current_tx_message.msg.data[0] == RECEIVE_CASH_FRENZY_HIT)
				event_message.head.data_length = current_tx_message.msg.length;
			break;

        case ASTERISK_HEADER:
			event_message.head.data_length = current_tx_message.msg.length;
			break;

        case PERCENT_HEADER:
			event_message.head.data_length = current_tx_message.msg.length;
			break;

        case CARET_HEADER:
			event_message.head.data_length = current_tx_message.msg.length;
			break;
    }

    if (event_message.head.data_length)
    {
        event_message.head.time_stamp = time(NULL);
        event_message.head.event_type = CONTROLLER_TX_ACK;
        event_message.head.port_number = port_info.port_number;
        memcpy(event_message.data, &current_tx_message.msg, event_message.head.data_length);

        if (pGameDevice)
            event_message.head.customer_id = pGameDevice->memory_game_config.customer_id;

        theApp.event_message_queue.push(event_message);
    }

    current_tx_message.response_pending = false;
}


DWORD CGameCommControl::getDispenserPayoutLimit (void)
{
    return theApp.memory_dispenser_config.dispenser_payout_limit;
}

bool CGameCommControl::payThroughDispenser (void)
{
    bool dispenser_present = TRUE;

    if ((theApp.memory_dispenser_config.dispenser_type <= DISPENSER_TYPE_NO_DISPENSER) || 
        theApp.memory_dispenser_config.tito)
        dispenser_present = FALSE;

    return dispenser_present;
}

CGameDevice* CGameCommControl::GetGamePointer(BYTE mux_id)
{
 CGameDevice *pGameDevice = pHeadGameDevice;

    while (pGameDevice)
    {
		if (mux_id == pGameDevice->memory_game_config.mux_id)
            return pGameDevice;

        pGameDevice = pGameDevice->pNextGame;
    }

 return NULL;
}

CGameDevice* CGameCommControl::GetGamePointer(BYTE mux_id, WORD port_number)
{
 CGameDevice *pGameDevice = pHeadGameDevice;

    while (pGameDevice)
    {
		if ((mux_id == pGameDevice->memory_game_config.mux_id) && (pGameDevice->memory_game_config.port_number == port_number))
            return pGameDevice;

        pGameDevice = pGameDevice->pNextGame;
    }

 return NULL;
}

CGameDevice* CGameCommControl::GetGamePointer(DWORD ucmc_id)
{
 CGameDevice *pGameDevice = pHeadGameDevice;

    while (pGameDevice)
    {
        if (ucmc_id == pGameDevice->memory_game_config.ucmc_id)
            return pGameDevice;

        pGameDevice = pGameDevice->pNextGame;
    }

 return NULL;
}

void CGameCommControl::OpenGameSerialPort()
{
 CString message;
 COMMTIMEOUTS comm_timeouts;
 DCB comm_control_settings;

    if (hGameSerialPort == INVALID_HANDLE_VALUE)
    {
        message.Format("\\\\.\\COM%u", port_info.port_number);
        hGameSerialPort = CreateFile(message, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);

        if (hGameSerialPort == INVALID_HANDLE_VALUE)
        {
            message.Format("GameCommControl::OpenGameSerialPort - error opening game comm port %u: error %d.",
                port_info.port_number, GetLastError());
            logMessage(message);
            game_port_check = 1;
            return;
        }
    }

    if (!GetCommState(hGameSerialPort, &comm_control_settings))
    {
        message.Format("GameCommControl::OpenGameSerialPort - error getting comm state on port %u: %d.",
            port_info.port_number, GetLastError());
        logMessage(message);
        game_port_check = 1;
    }
    else
    {
        switch (port_info.baud_rate)
        {
            case 110:
                comm_control_settings.BaudRate = CBR_110;
                break;

            case 300:
                comm_control_settings.BaudRate = CBR_300;
                break;

            case 600:
                comm_control_settings.BaudRate = CBR_600;
                break;

            case 1200:
                comm_control_settings.BaudRate = CBR_1200;
                break;

            case 2400:
                comm_control_settings.BaudRate = CBR_2400;
                break;

            case 4800:
                comm_control_settings.BaudRate = CBR_4800;
                break;

            case 9600:
                comm_control_settings.BaudRate = CBR_9600;
                break;

            case 14400:
                comm_control_settings.BaudRate = CBR_14400;
                break;

            case 19200:
                comm_control_settings.BaudRate = CBR_19200;
                break;

            case 38400:
                comm_control_settings.BaudRate = CBR_38400;
                break;

            case 56000:
                comm_control_settings.BaudRate = CBR_56000;
                break;

            case 57600:
                comm_control_settings.BaudRate = CBR_57600;
                break;

            case 115200:
                comm_control_settings.BaudRate = CBR_115200;
                break;

            case 128000:
                comm_control_settings.BaudRate = CBR_128000;
                break;

            case 256000:
                comm_control_settings.BaudRate = CBR_256000;
                break;

            default:
                comm_control_settings.BaudRate = CBR_9600;
                break;
        }

        comm_control_settings.Parity = NOPARITY;
        comm_control_settings.ByteSize = 8;
        comm_control_settings.DCBlength = sizeof(comm_control_settings);
        comm_control_settings.fBinary = 1;
        comm_control_settings.fParity = 0;
        comm_control_settings.fOutxCtsFlow = 0;
        comm_control_settings.fOutxDsrFlow = 0;
        comm_control_settings.fDtrControl = DTR_CONTROL_DISABLE;
        comm_control_settings.fDsrSensitivity = 0;
        comm_control_settings.fTXContinueOnXoff = 1;
        comm_control_settings.fOutX = 0;
        comm_control_settings.fInX = 0;
        comm_control_settings.fErrorChar = 0;
        comm_control_settings.fNull = 0;
        comm_control_settings.fRtsControl = RTS_CONTROL_DISABLE;
        comm_control_settings.fAbortOnError = 0;
        comm_control_settings.StopBits = ONESTOPBIT;

        if (!SetCommState(hGameSerialPort, &comm_control_settings))
        {
            message.Format("GameCommControl::OpenGameSerialPort - error setting comm state on port %u: %d.",
                port_info.port_number, GetLastError());
            logMessage(message);
            game_port_check = 1;
        }
        else
        {
            if (!GetCommTimeouts(hGameSerialPort, &comm_timeouts))
            {
                message.Format("GameCommControl::OpenGameSerialPort - error getting comm timeouts on port %u: %d.",
                    port_info.port_number, GetLastError());
                logMessage(message);
                game_port_check = 1;
            }
            else
            {
                comm_timeouts.ReadIntervalTimeout = 10;
                comm_timeouts.ReadTotalTimeoutMultiplier = 1;
                comm_timeouts.ReadTotalTimeoutConstant = 50;
                comm_timeouts.WriteTotalTimeoutMultiplier = 1;
                comm_timeouts.WriteTotalTimeoutConstant = 50;

                if (!SetCommTimeouts(hGameSerialPort, &comm_timeouts))
                {
                    message.Format("GameCommControl::OpenGameSerialPort - error setting comm timeouts on port %u: %d.",
                        port_info.port_number, GetLastError());
                    logMessage(message);
                    game_port_check = 1;
                }
                else
				{
					EscapeCommFunction(hGameSerialPort, SETDTR);
					EscapeCommFunction(hGameSerialPort, SETRTS);
					game_port_check = 0;
				}
            }
        }
    }
}

void CGameCommControl::CheckConnections()
{
    int last_port_check;
    bool connection_failure = FALSE;
    CString prt_buf;
    CWinThread* SAS_comm_thread_handle = NULL;


    if (!sas_com)
    {
	    if (pGameSocket)
		    last_port_check = pGameSocket->socket_state;
	    else
		    last_port_check = game_port_check;

	    if (last_port_check)
        {
            if (pGameSocket)
                OpenGameTcpIpPort();
            else
                OpenGameSerialPort();
        }
    }
    else
    {
        if (!sas_com->sas_communicating)
        {
            time_t current_time = time(NULL);

            if (current_time > (last_port_check_time + connection_check_second_delay))
            {
                last_port_check_time = current_time;

                if (!sas_com->stop_comm_thread)
                {
                    connection_failure = sas_com->CheckConnections();

                    if (connection_failure)
                    {
                        stop_comm_thread = TRUE;

                        sas_com->closeSASPort();

                        prt_buf.Format("\n\nGAME SAS ID: %u DISCONNECTED\n", sas_com->sas_id);
                        theApp.PrinterData(prt_buf.GetBuffer(0));
                        theApp.PrinterData(convertTimeStamp(time(NULL)));
                        theApp.PrinterData("\n\n");

                    }
                }
                else
                {
                    if (sas_com->sas_id && pHeadGameDevice && pHeadGameDevice->memory_game_config.serial_number)
                        sas_com->openSASPort(sas_com->sas_id, pHeadGameDevice->memory_game_config.serial_number);

                }
                connection_check_second_delay = 60;
            }
        }
    }
}

void CGameCommControl::OpenGameTcpIpPort()
{
 int last_error;
 CString message_string;

	if (pGameSocket)
	{
		if (pGameSocket->m_hSocket == INVALID_SOCKET && !pGameSocket->Create(0, SOCK_STREAM, FD_READ | FD_CLOSE, 0))
		{
			message_string.Format("GameCommControl::OpenGameTcpIpPort - game socket Create port %u, error %d.",
				port_info.port_number, GetLastError());
			logMessage(message_string);
		}

		pGameSocket->socket_state = WSAENOTCONN;

		if (pGameSocket->m_hSocket != INVALID_SOCKET)
		{
			BYTE *ip_address = (BYTE*)&port_info.ip_address;
			message_string.Format("%u.%u.%u.%u", ip_address[0], ip_address[1], ip_address[2], ip_address[3]);

			if (pGameSocket->Connect(message_string, port_info.port_number))
				pGameSocket->socket_state = 0;
			else
			{
				last_error = GetLastError();

				if (last_error == WSAEISCONN)
					pGameSocket->socket_state = 0;
				else if (last_error == WSAEINPROGRESS || last_error == WSAEWOULDBLOCK)
					pGameSocket->socket_state = last_error;
				else
				{
					message_string.Format("GameCommControl::OpenGameTcpIpPort - port %u, CONNECT error %d.", port_info.port_number, last_error);
					logMessage(message_string);
					pGameSocket->Close();
					pGameSocket->socket_state = WSAENOTCONN;
				}
			}
		}
	}
}

void CGameCommControl::logSasRealTimeEventMessage (BYTE sas_event_type)
{
    EventMessage event_message;
    CGameDevice *pGameDevice = pHeadGameDevice;

    if (sas_com && sas_com->sas_id)
    {
        memset(&event_message, 0, sizeof(event_message));
        event_message.head.time_stamp = time(NULL);
        event_message.head.event_type = SAS_REAL_TIME_EVENTS;
        event_message.head.data_length = 2;
        event_message.head.port_number = 0;
        event_message.data[0] = sas_com->sas_id;
        event_message.data[1] = sas_event_type;

        if (pGameDevice)
            event_message.head.customer_id = pGameDevice->memory_game_config.customer_id;

        theApp.event_message_queue.push(event_message);
    }
}

void CGameCommControl::logSasHandpayPendingInfoMessage (HandpayInfoResponse* handpay_info_response)
{
    EventMessage event_message;
    CGameDevice *pGameDevice = pHeadGameDevice;

    if (sas_com && sas_com->sas_id)
    {
        memset(&event_message, 0, sizeof(event_message));
        event_message.head.time_stamp = time(NULL);
        event_message.head.event_type = SAS_HANDPAY_PENDING_INFO_EVENT;
        event_message.head.data_length = sizeof(HandpayInfoResponse);
        event_message.head.port_number = 0;

        memcpy((char*)event_message.data, (char*)handpay_info_response, sizeof(HandpayInfoResponse));

        if (pGameDevice)
            event_message.head.customer_id = pGameDevice->memory_game_config.customer_id;

        theApp.event_message_queue.push(event_message);
    }
}

void CGameCommControl::logSasMeterDataMessage (MeterDataRequestResponse* meter_data_request_response)
{
    EventMessage event_message;
    CGameDevice *pGameDevice = pHeadGameDevice;

    if (sas_com && sas_com->sas_id)
    {
        memset(&event_message, 0, sizeof(event_message));
        event_message.head.time_stamp = time(NULL);
        event_message.head.event_type = SAS_METER_DATA_EVENT;
        event_message.head.data_length = sizeof(MeterDataRequestResponse);
        event_message.head.port_number = 0;

        memcpy((char*)event_message.data, (char*)meter_data_request_response, sizeof(MeterDataRequestResponse));

        if (pGameDevice)
            event_message.head.customer_id = pGameDevice->memory_game_config.customer_id;

        theApp.event_message_queue.push(event_message);
    }
}

void CGameCommControl::logSasBillMeterDataMessage (BillMeterDataRequestResponse* bill_meter_data_request_response)
{
    EventMessage event_message;
    CGameDevice *pGameDevice = pHeadGameDevice;

    if (sas_com && sas_com->sas_id)
    {
        memset(&event_message, 0, sizeof(event_message));
        event_message.head.time_stamp = time(NULL);
        event_message.head.event_type = SAS_BILL_METER_DATA_EVENT;
        event_message.head.data_length = sizeof(BillMeterDataRequestResponse);
        event_message.head.port_number = 0;

        memcpy((char*)event_message.data, (char*)bill_meter_data_request_response, sizeof(BillMeterDataRequestResponse));

        if (pGameDevice)
            event_message.head.customer_id = pGameDevice->memory_game_config.customer_id;

        theApp.event_message_queue.push(event_message);
    }
}

void CGameCommControl::processGameOldCardData(RxSerialMessage *rx_msg, CGameDevice* pGameDevice)
{
    int i;
    DWORD account_number = 0;
    NewCardDataMessage card_data_message;

    if (!pGameDevice)
			logMessage("CGameCommControl::processGameCardData - null game device pointer.");
    else
    {
        account_number = pGameDevice->account_login_data.account_number;

//        if (account_number)
        {
            OldCardDataMessage* old_card_data_message = (OldCardDataMessage*) &rx_msg->data[3];
            // translate the old message structure to the new structure size
            card_data_message.number_of_blocks = old_card_data_message->number_of_blocks;
            card_data_message.block_length = old_card_data_message->block_length;
            for (i = 0; i < MAX_BLOCKS_PER_MESSAGE; i++)
            {
                card_data_message.blocks[i].block_id = old_card_data_message->blocks[i].block_id;
                card_data_message.blocks[i].bet = old_card_data_message->blocks[i].bet;
                card_data_message.blocks[i].denomination = old_card_data_message->blocks[i].denomination;
                card_data_message.blocks[i].card1 = old_card_data_message->blocks[i].card1;
                card_data_message.blocks[i].card2 = old_card_data_message->blocks[i].card2;
                card_data_message.blocks[i].card3 = old_card_data_message->blocks[i].card3;
                card_data_message.blocks[i].card4 = old_card_data_message->blocks[i].card4;
                card_data_message.blocks[i].card5 = old_card_data_message->blocks[i].card5;
            }
            if (card_data_message.number_of_blocks <= 5)
                for (i=0; i < card_data_message.number_of_blocks; i++)
                    processHandData(&card_data_message.blocks[i], pGameDevice);
            memset(pGameDevice->blocks, 0, sizeof(OldBlock) * MAX_BLOCKS_PER_MESSAGE); 
            memcpy(pGameDevice->blocks, &card_data_message.blocks, sizeof(OldBlock) * MAX_BLOCKS_PER_MESSAGE); 
        }
    }
}

void CGameCommControl::processGameNewCardData(RxSerialMessage *rx_msg, CGameDevice* pGameDevice)
{
    int i;
    DWORD account_number = 0;

    if (!pGameDevice)
			logMessage("CGameCommControl::processGameNewCardData - null game device pointer.");
    else
    {
        account_number = pGameDevice->account_login_data.account_number;

        NewCardDataMessage* card_data_message = (NewCardDataMessage*) &rx_msg->data[3];

        if (memcmp(pGameDevice->blocks, card_data_message->blocks, sizeof(NewBlock) * card_data_message->number_of_blocks) != 0)
        {
            if (card_data_message->number_of_blocks <= 5)
                for (i=0; i < card_data_message->number_of_blocks; i++)
                    processHandData(&card_data_message->blocks[i], pGameDevice);
        }
        memset(pGameDevice->blocks, 0, sizeof(NewBlock) * MAX_BLOCKS_PER_MESSAGE); 
        memcpy(pGameDevice->blocks, card_data_message->blocks, sizeof(NewBlock) * MAX_BLOCKS_PER_MESSAGE); 
    }
}

void CGameCommControl::processHandData(unsigned char* hand, WORD bet, WORD denomination)
{
    NewBlock block;
    CGameDevice *pGameDevice = pHeadGameDevice;
    DWORD bonus_points;

    if (!duplicateCardsInHand (hand))
    {
        if (pGameDevice)
        {
            pGameDevice = theApp.GetSasGamePointer(pGameDevice->memory_game_config.ucmc_id);

            if (pGameDevice)
            {
	            block.block_id = 1;
	            block.bet = bet;
	            block.denomination = denomination;
	            block.card1 = hand[0];
	            block.card2 = hand[1];
	            block.card3 = hand[2];
	            block.card4 = hand[3];
	            block.card5 = hand[4];
                block.reserved = 0;

                bonus_points = processHandData(&block, pGameDevice);
                if (bonus_points)
                {
                    this->sas_com->non_volatile_ram.whole_points += bonus_points; 
                }
            }
        }
    }
}

bool CGameCommControl::duplicateCardsInHand (unsigned char* hand)
{
    bool duplicate = FALSE;

    if ((hand[0] == hand[1]) || (hand[0] == hand[2]) || (hand[0] == hand[3]) || (hand[0] == hand[4]) || (hand[0] == hand[5]))
        duplicate = TRUE;

    if ((hand[1] == hand[2]) || (hand[1] == hand[3]) || (hand[1] == hand[4]) || (hand[1] == hand[5]))
        duplicate = TRUE;

    if ((hand[2] == hand[3]) || (hand[2] == hand[4]) || (hand[2] == hand[5]))
        duplicate = TRUE;

    if ((hand[3] == hand[4]) || (hand[3] == hand[5]))
        duplicate = TRUE;
    
    if ((hand[4] == hand[5]))
        duplicate = TRUE;

    return duplicate;
}

// returns bonus points won
DWORD CGameCommControl::processHandData(NewBlock* block, CGameDevice *pGameDevice)
{
    int i;
    BYTE promotion_code = 0;
    DWORD account_number = 0;
    unsigned char cards[5];
    EventMessage event_message;
    DWORD bonus_points = 0;
	GbProWinCategory gbpro_win_category;

    cards[0] = block->card1;
    cards[1] = block->card2;
    cards[2] = block->card3;
    cards[3] = block->card4;
    cards[4] = block->card5;

	if (pGameDevice)
    {
        account_number = pGameDevice->account_login_data.account_number;

        win_evaluation.evaluateHand(cards);

        // the game's gb advantage off flag must be off in order to participate in GB Advantage
        if (!pGameDevice->memory_game_config.gb_advantage_off_flag)
        {
            for (i=0; i < NUMBER_OF_WIN_CATEGORIES; i++)
            {
		        gbpro_win_category = theApp.pGbProManager->getWinCategory(i);    

                if (theApp.pGbProManager->winCategoryActive(i, account_number, block) &&
                    win_evaluation.checkForWin(&gbpro_win_category))
                {
                    
                    if (memcmp(&pGameDevice->last_winning_gba_block, block, sizeof(NewBlock)) != 0)
                    {
                        bonus_points = awardGbaWin (i, pGameDevice, block);
                        memcpy(&pGameDevice->last_winning_gba_block, block, sizeof(NewBlock));
                    }


                    break;
                }
            }
        }

        gbpro_win_category = theApp.pGbProManager->getWinCategory(23);    
        if (win_evaluation.fourOfKindFound(gbpro_win_category.evaluation_descriptor))
        {
            // log four of a kinds events
            memset(&event_message, 0, sizeof(event_message));
            event_message.head.time_stamp = time(NULL);
            event_message.head.event_type = FOUR_OF_A_KIND_EVENT;
            event_message.head.data_length = sizeof(NewBlock);
            if (pGameDevice->memory_game_config.mux_id)
                block->reserved = pGameDevice->memory_game_config.mux_id;
            else
                block->reserved = pGameDevice->memory_game_config.sas_id;
            memcpy(event_message.data, (unsigned char*)block, sizeof(NewBlock));
            theApp.event_message_queue.push(event_message);

            process4okWin(pGameDevice, account_number, get4okWin(cards), block->bet, block->denomination);
        }
    }

    return bonus_points;
}

// returns bonus points won
DWORD CGameCommControl::awardGbaWin (int win_category_index, CGameDevice *pGameDevice, NewBlock* block)
{
    DWORD account_number = 0;
    DWORD money_bet = block->bet * block->denomination;
    DWORD win_multiplier = 0;
    GbProPointsTicketsCash points_tickets_cash;
//	IdcGbAdvantageWinMessage* idc_gb_advantage_win_message;
	IdcGbAdvantageWinMessage idc_gb_advantage_win_message;
	GbProWinCategory gbpro_win_category = theApp.pGbProManager->getWinCategory(win_category_index);    

    memset(&points_tickets_cash, 0, sizeof(points_tickets_cash));

    if (pGameDevice)
	{
		account_number = pGameDevice->account_login_data.account_number;

		points_tickets_cash.player_account = account_number;

		// when min money bet is specified, we need to scale awards accordingly
	//    if (gbpro_win_category.minimum_money_bet)
		if (gbpro_win_category.scalable_award)
		{
			if (money_bet > 100)
			{
				win_multiplier = money_bet / 100;

    			points_tickets_cash.bonus_points = gbpro_win_category.points_award * win_multiplier;
				// cap awards at 100,000 points
				if (points_tickets_cash.bonus_points > 100000)
					points_tickets_cash.bonus_points = 100000;

        		points_tickets_cash.cash_voucher_amount = gbpro_win_category.cash_voucher_award * win_multiplier;
				if (points_tickets_cash.cash_voucher_amount > 100)
					points_tickets_cash.cash_voucher_amount = 100;

        		points_tickets_cash.cashable_credits = gbpro_win_category.cashable_credits * win_multiplier;
        		points_tickets_cash.noncashable_credits = gbpro_win_category.noncashable_credits * win_multiplier;
			}
			else
			{
    			points_tickets_cash.bonus_points = (gbpro_win_category.points_award * money_bet) / 100;
        		points_tickets_cash.cash_voucher_amount = (gbpro_win_category.cash_voucher_award * money_bet) / 100;
        		points_tickets_cash.cashable_credits = (gbpro_win_category.cashable_credits * money_bet) / 100;
        		points_tickets_cash.noncashable_credits = (gbpro_win_category.noncashable_credits * money_bet) / 100;
			}
		}
		else
		{
    		points_tickets_cash.bonus_points = gbpro_win_category.points_award;
    		points_tickets_cash.cash_voucher_amount = gbpro_win_category.cash_voucher_award;
    		points_tickets_cash.cashable_credits = gbpro_win_category.cashable_credits;
    		points_tickets_cash.noncashable_credits = gbpro_win_category.noncashable_credits;
		}

        if (pGameDevice->memory_game_config.mux_id)
    		points_tickets_cash.machine_mux_id = pGameDevice->memory_game_config.mux_id;
        else if (pGameDevice->memory_game_config.sas_id)
    		points_tickets_cash.machine_mux_id = pGameDevice->memory_game_config.sas_id;

		points_tickets_cash.machine_denomination = block->denomination;
		points_tickets_cash.credit_bet = block->bet;
		points_tickets_cash.promotion_id = gbpro_win_category.promotion_code;
		points_tickets_cash.i_rewards_paid_win = gbpro_win_category.i_rewards_paid_win;
		points_tickets_cash.print_drawing_ticket = gbpro_win_category.print_drawing_ticket;
		points_tickets_cash.hand_data[0] = block->card1;
		points_tickets_cash.hand_data[1] = block->card2;
		points_tickets_cash.hand_data[2] = block->card3;
		points_tickets_cash.hand_data[3] = block->card4;
		points_tickets_cash.hand_data[4] = block->card5;
		points_tickets_cash.scalable_award = gbpro_win_category.scalable_award;
		strcpy(points_tickets_cash.promo_string, gbpro_win_category.media_string);

		rand_s((unsigned int*)&points_tickets_cash.transaction_id);

		if (!gbpro_win_category.i_rewards_paid_win)
			ProcessGbProPointsTicketsCash(&points_tickets_cash, pGameDevice);
		else
			points_tickets_cash.bonus_points = 0;

//		idc_gb_advantage_win_message = (IdcGbAdvantageWinMessage*)&points_tickets_cash;
        memcpy(&idc_gb_advantage_win_message, &points_tickets_cash, sizeof(IdcGbAdvantageWinMessage));
        idc_gb_advantage_win_message.cashable_credits = gbpro_win_category.cashable_credits;
        idc_gb_advantage_win_message.noncashable_credits = gbpro_win_category.noncashable_credits;

		if (gbpro_win_category.i_rewards_paid_win)
    		pGameDevice->addGbaInteractiveBonusTransactionId(idc_gb_advantage_win_message.transaction_id);

		theApp.SendIdcGbAdvantageWinMessage(&idc_gb_advantage_win_message, pGameDevice);

    	if (theApp.memory_settings_ini.cms_ip_address &&
    	    theApp.memory_settings_ini.cms_tcp_port &&
    	    !gbpro_win_category.i_rewards_paid_win &&
    	    strlen(pGameDevice->memory_game_config.machine_id))
                theApp.SendCmsGbAdvantageWinMessage (pGameDevice->memory_game_config.ucmc_id, &idc_gb_advantage_win_message);

	}

    return points_tickets_cash.bonus_points;
}

void CGameCommControl::ProcessGbProPointsTicketsCash(GbProPointsTicketsCash *rx_packet, CGameDevice *pGameDevice)
{
 char convert_string[30];
 CashVoucherTickets cash_voucher;
 StratusMessageFormat stratus_msg;
 FileCashierShift current_cashier_shift;
 EventMessage event_message;
 TxSerialMessage tx_message;
 StratusGbProPointsTicketsCash stratus_data;


	if (rx_packet->cash_voucher_amount)
	{
		if (theApp.pGbProManager->memory_gb_pro_settings.cash_voucher_printer_port)
		{
			memset(&cash_voucher, 0, sizeof(cash_voucher));

			if (pGameDevice && pGameDevice->account_login_data.account_number == rx_packet->player_account &&
				pGameDevice->account_login_data.name[0])
					memcpy(cash_voucher.player_name, pGameDevice->account_login_data.name, sizeof(pGameDevice->account_login_data.name));
			else
				sprintf_s(cash_voucher.player_name, sizeof(cash_voucher.player_name), "Ticket Holder");

			if (pGameDevice)
				cash_voucher.game_ucmc_id = pGameDevice->memory_game_config.ucmc_id;

			cash_voucher.player_account = rx_packet->player_account;
    		cash_voucher.cash_voucher_amount = rx_packet->cash_voucher_amount * 100; // send in cents
			cash_voucher.game_mux_id = rx_packet->machine_mux_id;
			cash_voucher.game_denomination = rx_packet->machine_denomination;
			cash_voucher.credits_wagered = rx_packet->credit_bet;
			cash_voucher.promotion_id = rx_packet->promotion_id;
			cash_voucher.time_stamp = time(NULL);
            strcpy(cash_voucher.promo_string, rx_packet->promo_string);
//			SendCvToPrinter(&cash_voucher);
            theApp.pGbProManager->cv_pending_printer_tickets_queue.push(cash_voucher);

			SendMachineAsciiMessage(pGameDevice, 0, cash_voucher.cash_voucher_amount, 0);
		}
		else
			logMessage("CGbProManager::ProcessGbProPointsTicketsCash - error printing Cash Voucher. Printer not configured.");
	}


	if (rx_packet->print_drawing_ticket)
	{
		if (theApp.pGbProManager->memory_gb_pro_settings.cash_voucher_printer_port)
		{
			memset(&cash_voucher, 0, sizeof(cash_voucher));

			if (pGameDevice && pGameDevice->account_login_data.account_number == rx_packet->player_account &&
				pGameDevice->account_login_data.name[0])
					memcpy(cash_voucher.player_name, pGameDevice->account_login_data.name, sizeof(pGameDevice->account_login_data.name));
			else
				sprintf_s(cash_voucher.player_name, sizeof(cash_voucher.player_name), "Ticket Holder");

			if (pGameDevice)
				cash_voucher.game_ucmc_id = pGameDevice->memory_game_config.ucmc_id;

			cash_voucher.player_account = rx_packet->player_account;
    		cash_voucher.cash_voucher_amount = 0; // send in cents
			cash_voucher.game_mux_id = rx_packet->machine_mux_id;
			cash_voucher.game_denomination = rx_packet->machine_denomination;
			cash_voucher.credits_wagered = rx_packet->credit_bet;
			cash_voucher.promotion_id = rx_packet->promotion_id;
			cash_voucher.time_stamp = time(NULL);
            cash_voucher.print_drawing_ticket = rx_packet->print_drawing_ticket;
            strcpy(cash_voucher.promo_string, rx_packet->promo_string);
//			SendCvToPrinter(&cash_voucher);
            theApp.pGbProManager->cv_pending_printer_tickets_queue.push(cash_voucher);

			SendMachineAsciiMessage(pGameDevice, 0, 0, 1);
		}
		else
			logMessage("CGbProManager::ProcessGbProPointsTicketsCash - error printing Cash Voucher. Printer not configured.");
	}


	// send messages to Stratus
	if (rx_packet->bonus_points || rx_packet->cash_voucher_amount || rx_packet->cashable_credits || rx_packet->noncashable_credits)
	{
		memset(&stratus_msg, 0, sizeof(stratus_msg));
		memset(&stratus_data, 0, sizeof(stratus_data));

		stratus_data.account_number = rx_packet->player_account;
		stratus_data.game_credits_played = rx_packet->credit_bet;
		stratus_data.game_denomination = rx_packet->machine_denomination;
		stratus_data.transaction_id = rx_packet->transaction_id;
        if (rx_packet->i_rewards_paid_win)
    		stratus_data.i_rewards_paid_win = '1';
        else 
    		stratus_data.i_rewards_paid_win = '0';

		sprintf_s(convert_string, sizeof(convert_string), "%4.4X", rx_packet->promotion_id);
		memcpy(stratus_data.new_promotion_id, convert_string, 4);

		sprintf_s(convert_string, sizeof(convert_string), "%2.2X%2.2X%2.2X%2.2X%2.2X", rx_packet->hand_data[0],
			rx_packet->hand_data[1], rx_packet->hand_data[2], rx_packet->hand_data[3], rx_packet->hand_data[4]);
		memcpy(stratus_data.card_hand_data, convert_string, 10);

		if (pGameDevice)
		{
			stratus_msg.customer_id = pGameDevice->memory_game_config.customer_id;
			stratus_msg.ucmc_id = pGameDevice->memory_game_config.ucmc_id;
			stratus_msg.mux_id = pGameDevice->memory_game_config.mux_id;
			stratus_msg.serial_port = pGameDevice->memory_game_config.port_number;
		}
		else
		{
			stratus_msg.customer_id = theApp.memory_location_config.customer_id;
			stratus_msg.ucmc_id = 0;
			stratus_msg.serial_port = 1;
		}

		endian4(stratus_data.account_number);
		endian4(stratus_data.game_credits_played);
		endian4(stratus_data.game_denomination);
		endian4(stratus_data.transaction_id);

		if (pGameDevice && pGameDevice->memory_game_config.club_code)
			stratus_msg.club_code = pGameDevice->memory_game_config.club_code;
		else
			stratus_msg.club_code = theApp.memory_location_config.club_code;

		stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
		stratus_msg.send_code = MPI_GB_PRO_POINTS_NEW_MSG;
		stratus_msg.property_id = theApp.memory_location_config.property_id;
		stratus_msg.message_length = sizeof(stratus_data) + STRATUS_HEADER_SIZE;

		if (!rx_packet->bonus_points && !rx_packet->cash_voucher_amount && !rx_packet->cashable_credits && !rx_packet->noncashable_credits)
		{
			memcpy(stratus_msg.data, &stratus_data, sizeof(stratus_data));
			theApp.QueueStratusTxMessage(&stratus_msg, TRUE);
		}
		else
		{
			if (rx_packet->bonus_points)
			{
				stratus_data.account_points = rx_packet->bonus_points;
				endian4(stratus_data.account_points);
				memcpy(stratus_msg.data, &stratus_data, sizeof(stratus_data));
				theApp.QueueStratusTxMessage(&stratus_msg, TRUE);

	            if (rx_packet->player_account && pGameDevice)
		            SendMachineAsciiMessage(pGameDevice, rx_packet->bonus_points, 0, 0);

                if (!pGameDevice)
                {
                    // log manager awarded points event
                    memset(&event_message, 0, sizeof(event_message));
                    event_message.head.time_stamp = time(NULL);
                    event_message.head.event_type = MANAGER_AWARDED_POINTS;
                    event_message.head.data_length = 2* sizeof(DWORD);
                    memcpy(event_message.data, (unsigned char*)&rx_packet->player_account, sizeof(DWORD));
                    memcpy(&event_message.data[4], (unsigned char*)&rx_packet->bonus_points, sizeof(DWORD));
                    theApp.event_message_queue.push(event_message);
                }

				stratus_data.account_points = 0;

                // if manager awarded points
                if (!pGameDevice)
                {
                    if (fileRead(CASHIER_SHIFT_1, 0, &current_cashier_shift, sizeof(current_cashier_shift)))
                    {
                        current_cashier_shift.manager_awarded_points += rx_packet->bonus_points;
                        fileWrite(CASHIER_SHIFT_1, 0, &current_cashier_shift, sizeof(current_cashier_shift));
                    }
                }
			}
			else if (rx_packet->cash_voucher_amount)
			{
				stratus_data.cash_voucher_amount = rx_packet->cash_voucher_amount * 100;
				endian4(stratus_data.cash_voucher_amount);
				memcpy(stratus_msg.data, &stratus_data, sizeof(stratus_data));
				theApp.QueueStratusTxMessage(&stratus_msg, TRUE);
			}
            else if (rx_packet->cashable_credits)
            {
		        stratus_data.cashable_credits = rx_packet->cashable_credits;
		        endian4(stratus_data.cashable_credits);

				memcpy(stratus_msg.data, &stratus_data, sizeof(stratus_data));
				theApp.QueueStratusTxMessage(&stratus_msg, TRUE);

    			SendMachineAsciiMessage(pGameDevice, 0, rx_packet->cashable_credits, 0);
    
                if (pGameDevice && pGameDevice->memory_game_config.mux_id)
                {
			        memset(&tx_message, 0, sizeof(tx_message));
			        tx_message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
			        tx_message.msg.length = 15;
			        tx_message.msg.command = PERCENT_HEADER;
			        tx_message.msg.data[0] = C__CASHABLE_CREDITS_WIN;

                    unsigned int transaction_id;
    				rand_s(&transaction_id);

    				memcpy(&tx_message.msg.data[1], &rx_packet->cashable_credits, sizeof(rx_packet->cashable_credits));
    				memcpy(&tx_message.msg.data[5], &transaction_id, sizeof(DWORD));

        			pGameDevice->transmit_queue.push(tx_message);
                }
                else if (pGameDevice)
                {
                    CGameCommControl *pGameCommControl;

                    pGameCommControl = theApp.GetSasGameCommControlPointer(pGameDevice->memory_game_config.ucmc_id);
                    pGameCommControl->sas_com->queueInitiateLegacyBonus (rx_packet->cashable_credits, FALSE);
					pGameCommControl->sas_com->non_volatile_ram.CENTS_WON += rx_packet->cashable_credits;


                }
            }
            else if (rx_packet->noncashable_credits)
            {
		        stratus_data.transaction_id = rx_packet->transaction_id;
		        stratus_data.noncashable_credits = rx_packet->noncashable_credits;
		        endian4(stratus_data.transaction_id);
		        endian4(stratus_data.noncashable_credits);

				memcpy(stratus_msg.data, &stratus_data, sizeof(stratus_data));
				theApp.QueueStratusTxMessage(&stratus_msg, TRUE);

    			SendMachineAsciiMessage(pGameDevice, 0, rx_packet->noncashable_credits, 0);
            }

		}
	}
}

void CGameCommControl::SendMachineAsciiMessage(CGameDevice *pGameDevice, DWORD points, DWORD amount, BYTE drawing_ticket)
{
 char *line_one, *line_two, string_prefix[30], string_postfix[30];
 int iii;
 TxSerialMessage tx_message;
 GbProAsciiMachineMessagesFile file_ascii_messages;
 char drawing_ticket_line1[] = "CONGRATULATIONS! YOU";
 char drawing_ticket_line2[] = "HAVE WON A DRAWING TICKET";
 DWORD transaction_id;

    if (pGameDevice && !pGameDevice->memory_game_config.mux_id)
		pGameDevice = theApp.GetGbInterfaceGamePointer(pGameDevice->memory_game_config.ucmc_id);

	if (pGameDevice && (points || amount || drawing_ticket))
	{
		if (GetFileAttributes(GB_PRO_ASCII_MESSAGES_FILE) == 0xFFFFFFFF ||
			!fileRead(GB_PRO_ASCII_MESSAGES_FILE, 0, &file_ascii_messages, sizeof(file_ascii_messages)))
		{
			memset(&file_ascii_messages, 0, sizeof(file_ascii_messages));
			sprintf_s(file_ascii_messages.points_text_line_one, sizeof(file_ascii_messages.points_text_line_one), GB_PRO_DISPLAY_LINE_ONE_DEFAULT);
			sprintf_s(file_ascii_messages.points_text_line_two, sizeof(file_ascii_messages.points_text_line_two), GB_PRO_DISPLAY_LINE_TWO_POINTS_DEFAULT);
			sprintf_s(file_ascii_messages.cash_text_line_one, sizeof(file_ascii_messages.cash_text_line_one), GB_PRO_DISPLAY_LINE_ONE_DEFAULT);
			sprintf_s(file_ascii_messages.cash_text_line_two, sizeof(file_ascii_messages.cash_text_line_two), GB_PRO_DISPLAY_LINE_TWO_CASH_DEFAULT);
		}

		if (points)
		{
			line_one = file_ascii_messages.points_text_line_one;
			line_two = file_ascii_messages.points_text_line_two;
		}
		else
		{
            if (amount)
            {
			    line_one = file_ascii_messages.cash_text_line_one;
			    line_two = file_ascii_messages.cash_text_line_two;
            }
            else
            {
                line_one = drawing_ticket_line1;
                line_two = drawing_ticket_line2;
            }
		}

		if (line_one[0] || line_two[0])
		{
			memset(&tx_message, 0, sizeof(tx_message));
			tx_message.msg.mux_id = pGameDevice->memory_game_config.mux_id;
			tx_message.msg.length = 67;
			tx_message.msg.command = PERCENT_HEADER;
			tx_message.msg.data[0] = GB_PRO_ASCII_GAME_MSG;

			for (iii=0; iii < 26; iii++)
			{
				if (!memcmp(&line_one[iii], "$$$$", 4))
				{
					memset(string_prefix, 0, 30);
					memset(string_postfix, 0, 30);

					if (iii > 0)
						memcpy(string_prefix, line_one, iii);

					if (iii < 25)
						memcpy(string_postfix, &line_one[iii + 4], 25 - iii);

					break;
				}
			}

			if (iii >= 26)
				memcpy(&tx_message.msg.data[1], line_one, 28);
			else
			{
				if (points)
					sprintf_s((char*)&tx_message.msg.data[1], MAXIMUM_SERIAL_MESSAGE_SIZE - MINIMUM_SERIAL_MESSAGE_SIZE,
						"%s%u%s", string_prefix, points, string_postfix);
				else
                {
					sprintf_s((char*)&tx_message.msg.data[1], MAXIMUM_SERIAL_MESSAGE_SIZE - MINIMUM_SERIAL_MESSAGE_SIZE,
						"%s%6.2f%s", string_prefix, (double)amount / 100, string_postfix);

                }
			}

			for (iii=0; iii < 26; iii++)
			{
				if (!memcmp(&line_two[iii], "$$$$", 4))
				{
					memset(string_prefix, 0, 30);
					memset(string_postfix, 0, 30);

					if (iii > 0)
						memcpy(string_prefix, line_two, iii);

					if (iii < 25)
						memcpy(string_postfix, &line_two[iii + 4], 25 - iii);

					break;
				}
			}

			if (iii >= 26)
				memcpy(&tx_message.msg.data[29], line_two, 28);
			else
			{
				if (points)
					sprintf_s((char*)&tx_message.msg.data[29], MAXIMUM_SERIAL_MESSAGE_SIZE - MINIMUM_SERIAL_MESSAGE_SIZE - 28,
						"%s%u%s", string_prefix, points, string_postfix);
				else
                {
					sprintf_s((char*)&tx_message.msg.data[29], MAXIMUM_SERIAL_MESSAGE_SIZE - MINIMUM_SERIAL_MESSAGE_SIZE - 28,
						"$%.2f%s", (double)amount / 100, string_postfix);
//						"$%s%6.2f%s", string_prefix, (double)amount / 100, string_postfix);
                }
			}

			if (points)
            {
				memcpy(&tx_message.msg.data[57], &points, 4);
                // generate a transaction_id
//                rand_s((unsigned int*)&transaction_id);
//				memcpy(&tx_message.msg.data[61], &transaction_id, 4);

            }
	
			switch (pGameDevice->memory_game_config.version)
			{
				// Bally V7000
				case 5:
					if (pGameDevice->memory_game_config.sub_version > 24)
					{
						tx_message.msg.length = 71;
						tx_message.msg.data[0] = p__GBA_MSG;
		                // generate a transaction_id
						rand_s((unsigned int*)&transaction_id);
						memcpy(&tx_message.msg.data[61], &transaction_id, 4);
					}
					break;
				// GB SAS Interface
				case 10:
					if (pGameDevice->memory_game_config.sub_version > 58)
					{
						tx_message.msg.length = 71;
						tx_message.msg.data[0] = p__GBA_MSG;
		                // generate a transaction_id
						rand_s((unsigned int*)&transaction_id);
						memcpy(&tx_message.msg.data[61], &transaction_id, 4);
					}
					break;
				// GB Interface
				case 11:
					if (pGameDevice->memory_game_config.sub_version > 6)
					{
						tx_message.msg.length = 71;
						tx_message.msg.data[0] = p__GBA_MSG;
		                // generate a transaction_id
						rand_s((unsigned int*)&transaction_id);
						memcpy(&tx_message.msg.data[61], &transaction_id, 4);
					}
					break;
				// Power Vision
				case 12:
					if (pGameDevice->memory_game_config.sub_version > 1)
					{
						tx_message.msg.length = 71;
						tx_message.msg.data[0] = p__GBA_MSG;
		                // generate a transaction_id
						rand_s((unsigned int*)&transaction_id);
						memcpy(&tx_message.msg.data[61], &transaction_id, 4);
					}
					break;

			}

			pGameDevice->transmit_queue.push(tx_message);
		}
	}
}


// returns 0x0f if no 4ok hit
unsigned char CGameCommControl::get4okWin (unsigned char* hand)
{
    unsigned char fok_rank = NO_4OK;
    int i;
    unsigned char rank_card1, rank_card2, rank_matches;
    unsigned char hand_ranks[5];

    for (i = 0; i < 5; i++)
        hand_ranks[i] = hand[i] & RANK_MASK;

    // set first hand rank to the first rank            
    rank_card1 = hand_ranks[0];    

    // set second hand rank to the next unique rank in the rank array
    for (i = 0; i < 5; i++)
        if (hand_ranks[i] != rank_card1)
        {
            rank_card2 = hand_ranks[i];
            break;
        }

    // count the number of ranks that match the first rank
    rank_matches = 0;

    for (i = 0; i < 5; i++)
        if (hand_ranks[i] == rank_card1)
            rank_matches++;

    // if we found a 4ok then skip checking the second rank matches
    if (rank_matches >= 4)
        fok_rank = rank_card1;
    else
    {
        // count the number of ranks that match the second rank
        rank_matches = 0;

        for (i = 0; i < 5; i++)
            if (hand_ranks[i] == rank_card2)
                rank_matches++;

        if (rank_matches >= 4)
            fok_rank = rank_card2;
    }

    return fok_rank;
}

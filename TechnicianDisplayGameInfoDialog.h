/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    06/24/05    Initial Revision.
*/


#pragma once
#include "afxwin.h"


// CTechnicianDisplayGameInfoDialog dialog

class CTechnicianDisplayGameInfoDialog : public CDialog
{
	DECLARE_DYNAMIC(CTechnicianDisplayGameInfoDialog)

public:
	CTechnicianDisplayGameInfoDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTechnicianDisplayGameInfoDialog();

// Dialog Data
	enum { IDD = IDD_TECHNICIAN_DISPLAY_GAME_INFO_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	afx_msg void OnTimer(UINT nIDEvent);
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedTechnicianDisplayGameInfoExitButton();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	virtual BOOL OnInitDialog();

private:
	CListBox m_game_info_list_box;

	void DisplayInfo(void);

};

/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   07/29/14    Removed interface to the obsolete GB Pro Media Box interface. The IDC takes care of the TV interface now.
Robert Fuller	12/03/13	Added black flush, red flush, any flush, and any straight win types to the GBA win awards.
Robert Fuller   07/02/12    Added ability to award cashable and non-cashable credits for GBA based wins.
Robert Fuller   05/02/12    Added code for future support for the GBA interface to Stations Casino Management System.
Robert Fuller   03/08/12    Call function to process GBA Configurator rx/tx messages from on idle instead of printer processing com thread.
Robert Fuller   03/08/13    Send message to reinit printer template when the Reset button is pressed.
Robert Fuller	02/05/13	Only log iGBA win transactions, not GBA win transactions.
Robert Fuller   09/17/12    Made iGBA win promotion categories active, even when they have not points, vouchers, or drawings specified in GBA.
Robert Fuller   09/17/12    Added iGBA transaction id to differentiate iGBA wins amongst multiple games.
Robert Fuller   09/17/12    Send reward drawing ticket win congratulation messages to the gaming devices.
Robert Fuller   05/31/12    Added code to communicate with the Interactive Display Controller.
Robert Fuller   05/31/12    Get the pointer to the GBI device before sending the GB Advantage award message.
Robert Fuller   06/08/11    Added mods to support GB with the XP/SAS direct connect feature.
Robert Fuller   10/08/10    Made sure that the GbProMediaBoxServerSocket object pointer exist before running the message transmit processor.
Robert Fuller   04/06/10    Add a game configuration flag that turns GB Advantage on/off.
Robert Fuller   02/03/10    Capped the GB Advantage point award at 100,000 and the cash voucher award at $100
Robert Fuller   10/12/09    Send the minimum money played info to the GB Advantage display application.
Robert Fuller   10/05/09    Added code to log a manager awarded points event.
Robert Fuller   09/22/09    Put 4ok win hand data in the event log.
Robert Fuller   09/03/09    Added software mods to support running the XP Controller in stand alone mode which allows locations to operate with basic features without connecting to the main frame.
Robert Fuller   07/29/09    Fixed attribute 3ok fours attribute structure.
Robert Fuller   07/09/09    Support new GB Advantage protocol features (scalable award, init categories after disconnect, custom drawing ticket text)
Robert Fuller   06/09/09    Added code to award GB Advantage wins to non-logged in players depending on configuration selection.
Robert Fuller   06/09/09    Added non-scaled GB Advantage awards when a combination of min bet and min denomination are specified in the GB Advantage configuration descriptor.
Robert Fuller   06/09/09    Added code to support printing a drawing ticket for a GB Advantage win.
Robert Fuller   05/01/09    Removed code associated with now defunct Million Dollar Ticket lottery system.
Robert Fuller   04/08/09    Made sure GB Advantage is active and configured properly before instantiating the media box connection.
Robert Fuller   04/07/09    Adjusted the card data info message processor to accommodate the fixed block size sent.
Robert Fuller   02/24/09    Added new specific flush and face card full house promos.
Robert Fuller   01/29/09    Added code to support the new GB Advantage technician screen.
Robert Fuller   01/28/09    Added the new mini initialization GB Advantage Configurator message.
Robert Fuller   01/27/09    Queue up cash voucher tickets in the pending queue instead of sending directly to the printer to avoid losing pending tickets
                            in paper out situations.
Robert Fuller   01/22/09    Added code to process the new card info block info message.
Robert Fuller   12/19/08    Try opening the GB Advantage Configuration port if we failed to connect last time because of a invalid argument error
Robert Fuller   11/13/08    Compare current hand data with previous hand data message to filter out duplicates.
Robert Fuller   11/04/08    Scale the win award based on the number of dollars bet.
Robert Fuller   10/15/08    Make sure that we have the right game device when evaluating hand data for GB Advantage wins.
Robert Fuller   09/29/08    Added code to print the promotion type string on the cash voucher ticket.
Robert Fuller   09/17/08    Added code to utilize the buzzer feature on the ticket printer.
Robert Fuller   08/26/08    Cash voucher amounts need to sent to the printer in cents NOT dollars.
Robert Fuller   08/19/08    Added code to support the GB Advantage feature.
Dave Elquist    06/15/05    Initial Revision.
*/


// GbProManager.cpp : implementation file
//
#define _CRT_RAND_S

#include "stdafx.h"
#include "xp_controller.h"
#include "GbProManager.h"


static bool first_gb_advantage_init_send = TRUE;

const BYTE TICKET_PRINTER_STATUS_FLAG_3 = 2;
const BYTE SF3_P_PRESENT = 0x08;


// GB Pro Configuration Server to Controller Message Commands
const BYTE SET_PROMOTION_WIN_CATEGORY = 'S';
const BYTE INIT_CATEGORIES_FIRST_TIME = 'I';  // first time inits on start up
const BYTE INIT_CATEGORIES_DISCONNECT = 'i';  // subsequent inits on reconnects do to phone line and modem issues.
const BYTE SET_DRAWING_TICKET_TEXT    = 'D';  // drawing ticket text

const DWORD INVALID_CUSTOMER_ID = 1;
const DWORD CRC_MISMATCH = 2;
const DWORD INVALID_PROMOTION_CODE = 3;

const BYTE MDT_ANY_FOUR_OF_A_KIND_PROMO_CODE = 0xC2;

const BYTE ANY_FOUR_OF_A_KIND_PROMO_CODE = 0xC1;
const BYTE FOUR_OF_A_KIND_TWOS_PROMO_CODE = 0xF2;
const BYTE FOUR_OF_A_KIND_THREES_PROMO_CODE = 0xF3;
const BYTE FOUR_OF_A_KIND_FOURS_PROMO_CODE = 0xF4;
const BYTE FOUR_OF_A_KIND_FIVES_PROMO_CODE = 0xF5;
const BYTE FOUR_OF_A_KIND_SIXES_PROMO_CODE = 0xF6;
const BYTE FOUR_OF_A_KIND_SEVENS_PROMO_CODE = 0xF7;
const BYTE FOUR_OF_A_KIND_EIGHTS_PROMO_CODE = 0xF8;
const BYTE FOUR_OF_A_KIND_NINES_PROMO_CODE = 0xF9;
const BYTE FOUR_OF_A_KIND_TENS_PROMO_CODE = 0xFA;
const BYTE FOUR_OF_A_KIND_JACKS_PROMO_CODE = 0xFB;
const BYTE FOUR_OF_A_KIND_QUEENS_PROMO_CODE = 0xFC;
const BYTE FOUR_OF_A_KIND_KINGS_PROMO_CODE = 0xFD;
const BYTE FOUR_OF_A_KIND_ACES_PROMO_CODE = 0xFE;

const BYTE ANY_STRAIGHT_FLUSH_PROMO_CODE = 0xB3;
const BYTE STRAIGHT_FLUSH_CLUBS_PROMO_CODE = 0x94;
const BYTE STRAIGHT_FLUSH_DIAMONDS_PROMO_CODE = 0x95;
const BYTE STRAIGHT_FLUSH_HEARTS_PROMO_CODE = 0x96;
const BYTE STRAIGHT_FLUSH_SPADES_PROMO_CODE = 0x97;

const BYTE ANY_ROYAL_FLUSH_PROMO_CODE = 0xD1;
const BYTE ROYAL_FLUSH_CLUBS_PROMO_CODE = 0x98;
const BYTE ROYAL_FLUSH_DIAMONDS_PROMO_CODE = 0x99;
const BYTE ROYAL_FLUSH_HEARTS_PROMO_CODE = 0x9a;
const BYTE ROYAL_FLUSH_SPADES_PROMO_CODE = 0x9b;

const BYTE ANY_FULL_HOUSE_PROMO_CODE = 0xA1;
const BYTE FULL_HOUSE_FACE_CARDS_PROMO_CODE = 0x81;
const BYTE FULL_HOUSE_TWOS_PROMO_CODE = 0x82;
const BYTE FULL_HOUSE_THREES_PROMO_CODE = 0x83;
const BYTE FULL_HOUSE_FOURS_PROMO_CODE = 0x84;
const BYTE FULL_HOUSE_FIVES_PROMO_CODE = 0x85;
const BYTE FULL_HOUSE_SIXES_PROMO_CODE = 0x86;
const BYTE FULL_HOUSE_SEVENS_PROMO_CODE = 0x87;
const BYTE FULL_HOUSE_EIGHTS_PROMO_CODE = 0x88;
const BYTE FULL_HOUSE_NINES_PROMO_CODE = 0x89;
const BYTE FULL_HOUSE_TENS_PROMO_CODE = 0x8a;
const BYTE FULL_HOUSE_JACKS_PROMO_CODE = 0x8b;
const BYTE FULL_HOUSE_QUEENS_PROMO_CODE = 0x8c;
const BYTE FULL_HOUSE_KINGS_PROMO_CODE = 0x8d;
const BYTE FULL_HOUSE_ACES_PROMO_CODE = 0x8e;

const BYTE FLUSH_CLUBS_PROMO_CODE = 0x72;
const BYTE FLUSH_DIAMONDS_PROMO_CODE = 0x73;
const BYTE FLUSH_HEARTS_PROMO_CODE = 0x74;
const BYTE FLUSH_SPADES_PROMO_CODE = 0x75;
const BYTE BLACK_FLUSH_PROMO_CODE = 0x76;
const BYTE RED_FLUSH_PROMO_CODE = 0x77;
const BYTE ANY_FLUSH_PROMO_CODE = 0x78;

const BYTE ANY_STRAIGHT_PROMO_CODE = 0x60;

const BYTE ANY_THREE_OF_A_KIND_PROMO_CODE = 0xE1;
const BYTE THREE_OF_A_KIND_TWOS_PROMO_CODE = 0xE2;
const BYTE THREE_OF_A_KIND_THREES_PROMO_CODE = 0xE3;
const BYTE THREE_OF_A_KIND_FOURS_PROMO_CODE = 0xE4;
const BYTE THREE_OF_A_KIND_FIVES_PROMO_CODE = 0xE5;
const BYTE THREE_OF_A_KIND_SIXES_PROMO_CODE = 0xE6;
const BYTE THREE_OF_A_KIND_SEVENS_PROMO_CODE = 0xE7;
const BYTE THREE_OF_A_KIND_EIGHTS_PROMO_CODE = 0xE8;
const BYTE THREE_OF_A_KIND_NINES_PROMO_CODE = 0xE9;
const BYTE THREE_OF_A_KIND_TENS_PROMO_CODE = 0xEA;
const BYTE THREE_OF_A_KIND_JACKS_PROMO_CODE = 0xEB;
const BYTE THREE_OF_A_KIND_QUEENS_PROMO_CODE = 0xEC;
const BYTE THREE_OF_A_KIND_KINGS_PROMO_CODE = 0xED;
const BYTE THREE_OF_A_KIND_ACES_PROMO_CODE = 0xEE;

const BYTE M_MANAGER_AWARDED_POINTS_PROMO_CODE = 0x4D;
const BYTE D_INTERACTIVE_BONUS_DRAWING_PROMO_CODE = 0x44;
const BYTE G_INTERACTIVE_AWARD_PROMO_CODE = 0x47;
const BYTE W_WIN_AMOUNT_BASED_AWARD_PROMO_CODE = 0x57;

static const EvaluationDescriptor any_royal_flush_attr =
{
    1,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    TRUE,  // flush_flag
    TRUE,  // straight_flag
    0,     // face card count 
    0x0a,  // rank_limit
    0  // rank_max
};

static const EvaluationDescriptor royal_flush_clubs_attr =
{
    1,   // primary_rank_attr
    1,   // auxilary_rank_attr
    SUIT_CLUBS,  // suit_attr
    TRUE,  // flush_flag
    TRUE,  // straight_flag
    0,     // face card count 
    0x0a,  // rank_limit
    0  // rank_max
};

static const EvaluationDescriptor royal_flush_hearts_attr =
{
    1,   // primary_rank_attr
    1,   // auxilary_rank_attr
    SUIT_HEARTS,  // suit_attr
    TRUE,  // flush_flag
    TRUE,  // straight_flag
    0,     // face card count 
    0x0a,  // rank_limit
    0  // rank_max
};

static const EvaluationDescriptor royal_flush_diamonds_attr =
{
    1,   // primary_rank_attr
    1,   // auxilary_rank_attr
    SUIT_DIAMONDS,  // suit_attr
    TRUE,  // flush_flag
    TRUE,  // straight_flag
    0,     // face card count 
    0x0a,  // rank_limit
    0  // rank_max
};

static const EvaluationDescriptor royal_flush_spades_attr =
{
    1,   // primary_rank_attr
    1,   // auxilary_rank_attr
    SUIT_SPADES,  // suit_attr
    TRUE,  // flush_flag
    TRUE,  // straight_flag
    0,     // face card count 
    0x0a,  // rank_limit
    0  // rank_max
};

static const EvaluationDescriptor any_straight_flush_attr =
{
    1,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    TRUE,  // flush_flag
    TRUE,  // straight_flag
    0,     // face card count 
    0,  // rank_limit
    0  // rank_max
};

static const EvaluationDescriptor straight_flush_clubs_attr =
{
    1,   // primary_rank_attr
    1,   // auxilary_rank_attr
    SUIT_CLUBS,  // suit_attr
    TRUE,  // flush_flag
    TRUE,  // straight_flag
    0,     // face card count 
    0,  // rank_limit
    0  // rank_max
};

static const EvaluationDescriptor straight_flush_hearts_attr =
{
    1,   // primary_rank_attr
    1,   // auxilary_rank_attr
    SUIT_HEARTS,  // suit_attr
    TRUE,  // flush_flag
    TRUE,  // straight_flag
    0,     // face card count 
    0,  // rank_limit
    0  // rank_max
};

static const EvaluationDescriptor straight_flush_diamonds_attr =
{
    1,   // primary_rank_attr
    1,   // auxilary_rank_attr
    SUIT_DIAMONDS,  // suit_attr
    TRUE,  // flush_flag
    TRUE,  // straight_flag
    0,     // face card count 
    0,  // rank_limit
    0  // rank_max
};

static const EvaluationDescriptor straight_flush_spades_attr =
{
    1,   // primary_rank_attr
    1,   // auxilary_rank_attr
    SUIT_SPADES,  // suit_attr
    TRUE,  // flush_flag
    TRUE,  // straight_flag
    0,     // face card count 
    0,  // rank_limit
    0  // rank_max
};

static const EvaluationDescriptor any_four_of_a_kind_attr =
{
    4,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x02,  // rank_limit
    0x0e  // rank_max
};

static const EvaluationDescriptor four_of_a_kind_twos_attr =
{
    4,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x02,  // rank_limit
    0x02  // rank_max
};

static const EvaluationDescriptor four_of_a_kind_threes_attr =
{
    4,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x03,  // rank_limit
    0x03  // rank_max
};

static const EvaluationDescriptor four_of_a_kind_fours_attr =
{
    4,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x04,  // rank_limit
    0x04  // rank_max
};

static const EvaluationDescriptor four_of_a_kind_fives_attr =
{
    4,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x05,  // rank_limit
    0x05  // rank_max
};

static const EvaluationDescriptor four_of_a_kind_sixes_attr =
{
    4,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x06,  // rank_limit
    0x06  // rank_max
};

static const EvaluationDescriptor four_of_a_kind_sevens_attr =
{
    4,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x07,  // rank_limit
    0x07  // rank_max
};

static const EvaluationDescriptor four_of_a_kind_eights_attr =
{
    4,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x08,  // rank_limit
    0x08  // rank_max
};

static const EvaluationDescriptor four_of_a_kind_nines_attr =
{
    4,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x09,  // rank_limit
    0x09  // rank_max
};

static const EvaluationDescriptor four_of_a_kind_tens_attr =
{
    4,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x0a,  // rank_limit
    0x0a  // rank_max
};

static const EvaluationDescriptor four_of_a_kind_jacks_attr =
{
    4,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x0b,  // rank_limit
    0x0b  // rank_max
};

static const EvaluationDescriptor four_of_a_kind_queens_attr =
{
    4,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x0c,  // rank_limit
    0x0c  // rank_max
};

static const EvaluationDescriptor four_of_a_kind_kings_attr =
{
    4,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x0d,  // rank_limit
    0x0d  // rank_max
};

static const EvaluationDescriptor four_of_a_kind_aces_attr =
{
    4,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x0e,  // rank_limit
    0x0e  // rank_max
};

static const EvaluationDescriptor full_house_all_face_cards_attr =
{
    3,   // primary_rank_attr
    2,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    5,     // face card count 
    0x02,  // rank_limit
    0x0e  // rank_max
};

static const EvaluationDescriptor any_full_house_attr =
{
    3,   // primary_rank_attr
    2,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x02,  // rank_limit
    0x0e  // rank_max
};

static const EvaluationDescriptor full_house_twos_attr =
{
    3,   // primary_rank_attr
    2,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    2,  // rank_limit
    2  // rank_max
};

static const EvaluationDescriptor full_house_threes_attr =
{
    3,   // primary_rank_attr
    2,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    3,  // rank_limit
    3  // rank_max
};

static const EvaluationDescriptor full_house_fours_attr =
{
    3,   // primary_rank_attr
    2,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    4,  // rank_limit
    4  // rank_max
};

static const EvaluationDescriptor full_house_fives_attr =
{
    3,   // primary_rank_attr
    2,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    5,  // rank_limit
    5  // rank_max
};

static const EvaluationDescriptor full_house_sixes_attr =
{
    3,   // primary_rank_attr
    2,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    6,  // rank_limit
    6  // rank_max
};

static const EvaluationDescriptor full_house_sevens_attr =
{
    3,   // primary_rank_attr
    2,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    7,  // rank_limit
    7  // rank_max
};

static const EvaluationDescriptor full_house_eights_attr =
{
    3,   // primary_rank_attr
    2,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    8,  // rank_limit
    8  // rank_max
};

static const EvaluationDescriptor full_house_nines_attr =
{
    3,   // primary_rank_attr
    2,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    9,  // rank_limit
    9  // rank_max
};

static const EvaluationDescriptor full_house_tens_attr =
{
    3,   // primary_rank_attr
    2,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x0a,  // rank_limit
    0x0a  // rank_max
};

static const EvaluationDescriptor full_house_jacks_attr =
{
    3,   // primary_rank_attr
    2,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x0b,  // rank_limit
    0x0b  // rank_max
};

static const EvaluationDescriptor full_house_queens_attr =
{
    3,   // primary_rank_attr
    2,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x0c,  // rank_limit
    0x0c  // rank_max
};

static const EvaluationDescriptor full_house_kings_attr =
{
    3,   // primary_rank_attr
    2,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x0d,  // rank_limit
    0x0d  // rank_max
};

static const EvaluationDescriptor full_house_aces_attr =
{
    3,   // primary_rank_attr
    2,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x0e,  // rank_limit
    0x0e  // rank_max
};

static const EvaluationDescriptor flush_clubs_attr =
{
    1,   // primary_rank_attr
    1,   // auxilary_rank_attr
    SUIT_CLUBS,  // suit_attr
    TRUE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0,  // rank_limit
    0  // rank_max
};

static const EvaluationDescriptor flush_hearts_attr =
{
    1,   // primary_rank_attr
    1,   // auxilary_rank_attr
    SUIT_HEARTS,  // suit_attr
    TRUE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0,  // rank_limit
    0  // rank_max
};

static const EvaluationDescriptor flush_diamonds_attr =
{
    1,   // primary_rank_attr
    1,   // auxilary_rank_attr
    SUIT_DIAMONDS,  // suit_attr
    TRUE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0,  // rank_limit
    0  // rank_max
};

static const EvaluationDescriptor flush_spades_attr =
{
    1,   // primary_rank_attr
    1,   // auxilary_rank_attr
    SUIT_SPADES,  // suit_attr
    TRUE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0,  // rank_limit
    0  // rank_max
};

static const EvaluationDescriptor flush_black_attr =
{
    1,   // primary_rank_attr
    1,   // auxilary_rank_attr
    SUIT_BLACK,  // suit_attr
    TRUE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0,  // rank_limit
    0  // rank_max
};

static const EvaluationDescriptor flush_red_attr =
{
    1,   // primary_rank_attr
    1,   // auxilary_rank_attr
    SUIT_RED,  // suit_attr
    TRUE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0,  // rank_limit
    0  // rank_max
};

static const EvaluationDescriptor flush_any_attr =
{
    1,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    TRUE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0,  // rank_limit
    0  // rank_max
};

static const EvaluationDescriptor any_straight_attr =
{
    1,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    TRUE,  // straight_flag
    0,     // face card count 
    0,  // rank_limit
    0  // rank_max
};

static const EvaluationDescriptor any_three_of_a_kind_attr =
{
    3,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x02,  // rank_limit
    0x0e  // rank_max
};

static const EvaluationDescriptor three_of_a_kind_twos_attr =
{
    3,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x02,  // rank_limit
    0x02  // rank_max
};

static const EvaluationDescriptor three_of_a_kind_threes_attr =
{
    3,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x03,  // rank_limit
    0x03  // rank_max
};

static const EvaluationDescriptor three_of_a_kind_fours_attr =
{
    3,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x04,  // rank_limit
    0x04  // rank_max
};

static const EvaluationDescriptor three_of_a_kind_fives_attr =
{
    3,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x05,  // rank_limit
    0x05  // rank_max
};

static const EvaluationDescriptor three_of_a_kind_sixes_attr =
{
    3,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x06,  // rank_limit
    0x06  // rank_max
};

static const EvaluationDescriptor three_of_a_kind_sevens_attr =
{
    3,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x07,  // rank_limit
    0x07  // rank_max
};

static const EvaluationDescriptor three_of_a_kind_eights_attr =
{
    3,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x08,  // rank_limit
    0x08  // rank_max
};

static const EvaluationDescriptor three_of_a_kind_nines_attr =
{
    3,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x09,  // rank_limit
    0x09  // rank_max
};

static const EvaluationDescriptor three_of_a_kind_tens_attr =
{
    3,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x0a,  // rank_limit
    0x0a  // rank_max
};

static const EvaluationDescriptor three_of_a_kind_jacks_attr =
{
    3,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x0b,  // rank_limit
    0x0b  // rank_max
};

static const EvaluationDescriptor three_of_a_kind_queens_attr =
{
    3,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x0c,  // rank_limit
    0x0c  // rank_max
};

static const EvaluationDescriptor three_of_a_kind_kings_attr =
{
    3,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x0d,  // rank_limit
    0x0d  // rank_max
};

static const EvaluationDescriptor three_of_a_kind_aces_attr =
{
    3,   // primary_rank_attr
    1,   // auxilary_rank_attr
    MAX_CARD_SUIT,  // suit_attr
    FALSE,  // flush_flag
    FALSE,  // straight_flag
    0,     // face card count 
    0x0e,  // rank_limit
    0x0e  // rank_max
};




CGbProManager::CGbProManager()
{
 DWORD file_index;
 FileGbProManagerSettings file_settings;
 CashVoucherTickets file_cv_tickets_message;

    memset(&server_receive, 0, sizeof(server_receive));
    memset(&cv_printer_reset, 0, sizeof(cv_printer_reset));
    memset(&cv_printer_idle_msg_status_bits, 0, sizeof(cv_printer_idle_msg_status_bits));
    memset(&cv_rx_printer_message, 0, sizeof(cv_rx_printer_message));
    memset(&gb_pro_server_receive, 0, sizeof(gb_pro_server_receive));

    cv_printer_poll_wait = 0;
    display_category_index = 0;

    initialGbProWinCatagories();


    if (GetFileAttributes(CV_PENDING_PRINTER_FILE) != 0xFFFFFFFF)
    {
        file_index = 0;
        while (fileRead(CV_PENDING_PRINTER_FILE, file_index, &file_cv_tickets_message, sizeof(file_cv_tickets_message)))
        {
            cv_pending_printer_tickets_queue.push(file_cv_tickets_message);
            file_index++;
        }

        DeleteFile(CV_PENDING_PRINTER_FILE);
    }

    if (!fileRead(GB_PRO_SETTINGS_INI, 0, &file_settings, sizeof(file_settings)))
    {
        memset(&file_settings, 0, sizeof(file_settings));
        fileWrite(GB_PRO_SETTINGS_INI, 0, &file_settings, sizeof(file_settings));
    }

    memcpy(&memory_gb_pro_settings, &file_settings.memory, sizeof(memory_gb_pro_settings));

	hCvPrinterPort	= INVALID_HANDLE_VALUE;

	pCvPrinterSocket = NULL;

    if (memory_gb_pro_settings.cash_voucher_printer_port)
    {
    	if (memory_gb_pro_settings.cash_voucher_printer_ip_address)
		{
        	pCvPrinterSocket = new CMyAsyncSocket;
			OpenCvPrinterTcpIpPort();
		}
		else
			cv_printer_time_check = time(NULL);

        ResetCvPrinter();
		ProcessCvPrinterReset();
    }
}

CGbProManager::~CGbProManager()
{
 CashVoucherTickets file_cv_tickets_message;

	while (!cv_pending_printer_tickets_queue.empty())
	{
		file_cv_tickets_message = cv_pending_printer_tickets_queue.front();
		cv_pending_printer_tickets_queue.pop();
		fileWrite(CV_PENDING_PRINTER_FILE, -1, &file_cv_tickets_message, sizeof(file_cv_tickets_message));
	}

    if (pCvPrinterSocket)
    {
        if (pCvPrinterSocket->m_hSocket != INVALID_SOCKET)
            pCvPrinterSocket->Close();
        Sleep(1000);    // give the socket time to close
        delete pCvPrinterSocket;
    }

    if (hCvPrinterPort != INVALID_HANDLE_VALUE)
        CloseHandle(hCvPrinterPort);
}

void CGbProManager::ResetCvPrinter()
{
 PrinterMessage tx_message;

    sprintf_s(tx_message.data, sizeof(tx_message.data), "^r|^");
    tx_message.length = (int)strlen(tx_message.data);
    cv_printer_queue.push(tx_message);
}

void CGbProManager::turnOnOffCvPrinterBuzzer(bool buzzer_on)
{                                                            
    PrinterMessage tx_message;

    if (buzzer_on)
        sprintf_s(tx_message.data, sizeof(tx_message.data), "^b|1|^");
    else
        sprintf_s(tx_message.data, sizeof(tx_message.data), "^b|0|^");
    tx_message.length = (int)strlen(tx_message.data);
    cv_printer_queue.push(tx_message);
}

void CGbProManager::OpenCvPrinterTcpIpPort()
{
 int last_error;
 CString message_string;

	if (pCvPrinterSocket->m_hSocket == INVALID_SOCKET && !pCvPrinterSocket->Create(0, SOCK_STREAM, FD_READ | FD_CLOSE, 0))
	{
		message_string.Format("CGbProManager::OpenCvPrinterTcpIpPort - printer socket Create error %d.", GetLastError());
		logMessage(message_string);
	}

	pCvPrinterSocket->socket_state = WSAENOTCONN;

	if (pCvPrinterSocket->m_hSocket != INVALID_SOCKET)
	{
		BYTE *ip_address = (BYTE*)&memory_gb_pro_settings.cash_voucher_printer_ip_address;
		message_string.Format("%u.%u.%u.%u", ip_address[0], ip_address[1], ip_address[2], ip_address[3]);

		if (pCvPrinterSocket->Connect(message_string, memory_gb_pro_settings.cash_voucher_printer_port))
			pCvPrinterSocket->socket_state = 0;
		else
		{
			last_error = GetLastError();

			if (last_error == WSAEISCONN)
				pCvPrinterSocket->socket_state = 0;
			else if (last_error == WSAEINPROGRESS || last_error == WSAEWOULDBLOCK)
				pCvPrinterSocket->socket_state = last_error;
			else
			{
				message_string.Format("CGbProManager::OpenCvPrinterTcpIpPort - CONNECT error %d.", last_error);
				logMessage(message_string);
				pCvPrinterSocket->Close();
				pCvPrinterSocket->socket_state = WSAENOTCONN;
			}
		}
	}
}

void CGbProManager::OpenCvPrinterSerialPort()
{
 CString message;
 COMMTIMEOUTS comm_timeouts;
 DCB comm_control_settings;

    if (hCvPrinterPort == INVALID_HANDLE_VALUE)
    {
        message.Format("\\\\.\\COM%u", memory_gb_pro_settings.cash_voucher_printer_port);
        hCvPrinterPort = CreateFile(message, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);

        if (hCvPrinterPort == INVALID_HANDLE_VALUE)
        {
            message.Format("CGbProManager::OpenCvPrinterSerialPort - error opening port, error %d.", GetLastError());
            logMessage(message);
            cv_printer_time_check = time(NULL);
            return;
        }
    }

    if (!GetCommState(hCvPrinterPort, &comm_control_settings))
    {
        message.Format("CGbProManager::OpenCvPrinterSerialPort - error getting comm state: %d.", GetLastError());
        logMessage(message);
        cv_printer_time_check = time(NULL);
    }
    else
    {
        comm_control_settings.BaudRate = CBR_38400;
        comm_control_settings.Parity = NOPARITY;
        comm_control_settings.ByteSize = 8;
        comm_control_settings.DCBlength = sizeof(comm_control_settings);
        comm_control_settings.fBinary = 1;
        comm_control_settings.fParity = 0;
        comm_control_settings.fOutxCtsFlow = 0;
        comm_control_settings.fOutxDsrFlow = 0;
        comm_control_settings.fDtrControl = DTR_CONTROL_DISABLE;
        comm_control_settings.fDsrSensitivity = 0;
        comm_control_settings.fTXContinueOnXoff = 1;
        comm_control_settings.fOutX = 1;
        comm_control_settings.fInX = 0;
        comm_control_settings.fErrorChar = 0;
        comm_control_settings.fNull = 0;
        comm_control_settings.fRtsControl = RTS_CONTROL_DISABLE;
        comm_control_settings.fAbortOnError = 0;
        comm_control_settings.StopBits = ONESTOPBIT;
        comm_control_settings.XonChar = 0x11;
        comm_control_settings.XoffChar = 0x13;

        if (!SetCommState(hCvPrinterPort, &comm_control_settings))
        {
            message.Format("CGbProManager::OpenCvPrinterSerialPort - error setting comm state: %d.", GetLastError());
            logMessage(message);
			cv_printer_time_check = time(NULL);
        }
        else
        {
            if (!GetCommTimeouts(hCvPrinterPort, &comm_timeouts))
            {
                message.Format("CGbProManager::OpenCvPrinterSerialPort - error getting comm timeouts: %d.", GetLastError());
                logMessage(message);
				cv_printer_time_check = time(NULL);
            }
            else
            {
                comm_timeouts.ReadIntervalTimeout = 10;
                comm_timeouts.ReadTotalTimeoutMultiplier = 1;
                comm_timeouts.ReadTotalTimeoutConstant = 50;
                comm_timeouts.WriteTotalTimeoutMultiplier = 1;
                comm_timeouts.WriteTotalTimeoutConstant = 50;

                if (!SetCommTimeouts(hCvPrinterPort, &comm_timeouts))
                {
                    message.Format("CGbProManager::OpenCvPrinterSerialPort - error setting comm timeouts: %d.", GetLastError());
                    logMessage(message);
					cv_printer_time_check = time(NULL);
                }
                else
					cv_printer_time_check = 0;
            }
        }
    }
}

void CGbProManager::CheckCvPrinterReceive()
{
 int bytes_received = 0;
 CString error_message;

    if (pCvPrinterSocket)
    {
        bytes_received = pCvPrinterSocket->GetBuffer((BYTE*)&cv_rx_printer_message.data[cv_rx_printer_message.length],
            (WORD)(sizeof(cv_rx_printer_message.data) - cv_rx_printer_message.length));

	if (!bytes_received)
		return;
	else
            cv_rx_printer_message.length += bytes_received;
    }
    else
    {
        if (hCvPrinterPort == INVALID_HANDLE_VALUE)
			cv_printer_time_check = time(NULL);
        else
        {
            if (!ReadFile(hCvPrinterPort, &cv_rx_printer_message.data[cv_rx_printer_message.length],
                sizeof(cv_rx_printer_message.data) - cv_rx_printer_message.length, (DWORD*)&bytes_received, NULL))
            {
                error_message.Format("CGbProManager::CheckCvPrinterReceive - serial port error %d.", GetLastError());
                logMessage(error_message);
				cv_printer_time_check = time(NULL);
                bytes_received = 0;
            }
            else
            {
                if (!bytes_received)
                    return;
                else
                    cv_rx_printer_message.length += bytes_received;
            }
        }
    }

    if (bytes_received > 0)
    {
        // check header byte
        if (cv_rx_printer_message.data[0] == '*')
        {
            // check for end of message
            for (bytes_received=1; bytes_received < cv_rx_printer_message.length; bytes_received++)
            {
                if (cv_rx_printer_message.data[bytes_received] == '*')
                {
                    cv_rx_printer_message.length = bytes_received + 1;
                    ProcessCvPrinterReceive(&cv_rx_printer_message);
                    memset(&cv_rx_printer_message, 0, sizeof(cv_rx_printer_message));
                }
            }

            return; // wait for rest of message, if pending
        }
    }

    memset(&cv_rx_printer_message, 0, sizeof(cv_rx_printer_message));
}

void CGbProManager::ProcessCvPrinterReceive(PrinterMessage *rx_data)
{
 BYTE buffer_index, rx_cv_printer_idle_msg_status_bits[5];
 int iii;

    if (!memcmp(rx_data->data, "*S|0|", 5))
    {
        buffer_index = 5;
        memset(rx_cv_printer_idle_msg_status_bits, 0, 5);
        while (buffer_index < rx_data->length)
        {
            if (rx_data->data[buffer_index] == '|' && buffer_index + 1 < rx_data->length)
            {
                for (iii=0; iii < 5; iii++)
                {
                    if (!rx_cv_printer_idle_msg_status_bits[iii])
                    {
                        rx_cv_printer_idle_msg_status_bits[iii] = rx_data->data[buffer_index + 1];
                        break;
                    }
                }
            }

            buffer_index++;
        }

        if (memcmp(rx_cv_printer_idle_msg_status_bits, cv_printer_idle_msg_status_bits, 5))
        {
            // if there is a change in the status of SF3_P_PRESENT turn on/off the buzzer
            if ((rx_cv_printer_idle_msg_status_bits[TICKET_PRINTER_STATUS_FLAG_3] & SF3_P_PRESENT) !=
                (cv_printer_idle_msg_status_bits[TICKET_PRINTER_STATUS_FLAG_3] & SF3_P_PRESENT))
            {
                if (rx_cv_printer_idle_msg_status_bits[TICKET_PRINTER_STATUS_FLAG_3] & SF3_P_PRESENT)
                    turnOnOffCvPrinterBuzzer(TRUE);
                else
                    turnOnOffCvPrinterBuzzer(FALSE);
            } 

            memcpy(cv_printer_idle_msg_status_bits, rx_cv_printer_idle_msg_status_bits, 5);
        }
    }
}

void CGbProManager::CheckCvPrinterTransmit()
{
 static time_t printer_check_time;
 PrinterMessage tx_message;
 CashVoucherTickets cv_ticket_queue;

    if (!cv_printer_queue.empty())
    {
        tx_message = cv_printer_queue.front();
        cv_printer_queue.pop();
        CvTxPrinterData(tx_message.data, tx_message.length);
    }
    else
    {
        // check printer status every 5 seconds
        if (printer_check_time < time(NULL))
        {
            tx_message.data[0] = 0x05;
            CvTxPrinterData(tx_message.data, 1);
            printer_check_time = time(NULL) + 5;
        }
        else
        {
            if (!cv_pending_printer_tickets_queue.empty() && ClearToSendCvPrinterMessage())
            {
                cv_ticket_queue = cv_pending_printer_tickets_queue.front();
				cv_pending_printer_tickets_queue.pop();
                SendCvToPrinter(&cv_ticket_queue);
            }

            // reset the flag every 60 seconds, if not already reset
            if (cv_printer_reset.flag_sent && cv_printer_reset.time_sent + 60 < time(NULL))
            {
                memset(&cv_printer_reset, 0, sizeof(cv_printer_reset));
                ProcessCvPrinterReset();
            }
        }
    }
}

void CGbProManager::ProcessCvPrinterReset()
{
 PrinterMessage tx_message;

    // delete all print regions and templates
    memset(&tx_message, 0, sizeof(tx_message));
    sprintf_s(tx_message.data, sizeof(tx_message.data), "^R|$|DR|^");
    tx_message.length = (int)strlen(tx_message.data);
    cv_printer_queue.push(tx_message);

    // define print region 0, location name
	memset(&tx_message, 0, sizeof(tx_message));
	sprintf_s(tx_message.data, sizeof(tx_message.data), "^R|0|R|80|1091|58|1085|0|3|1|8|2|2|0|0|Dynamic Region 0|^");
	tx_message.length = (int)strlen(tx_message.data);
	cv_printer_queue.push(tx_message);

	// define print region 1, location street address
	memset(&tx_message, 0, sizeof(tx_message));
	sprintf_s(tx_message.data, sizeof(tx_message.data), "^R|1|R|138|1087|58|1081|0|3|1|8|2|2|0|0|Dynamic Region 1|^");
	tx_message.length = (int)strlen(tx_message.data);
	cv_printer_queue.push(tx_message);

    // define print region 2, time stamp
	memset(&tx_message, 0, sizeof(tx_message));
	sprintf_s(tx_message.data, sizeof(tx_message.data), "^R|2|R|180|1026|48|962|0|3|1|3|1|1|0|0|Dynamic Region 2|^");
	tx_message.length = (int)strlen(tx_message.data);
	cv_printer_queue.push(tx_message);

    // define print region 3, cash voucher ascii amount
	memset(&tx_message, 0, sizeof(tx_message));
	sprintf_s(tx_message.data, sizeof(tx_message.data), "^R|3|R|333|1084|53|1084|0|3|1|3|1|1|0|0|Dynamic Region 3|^");
	tx_message.length = (int)strlen(tx_message.data);
	cv_printer_queue.push(tx_message);

    // define print region 4, cash voucher amount
	memset(&tx_message, 0, sizeof(tx_message));
	sprintf_s(tx_message.data, sizeof(tx_message.data), "^R|4|R|313|1072|137|1041|0|3|1|8|4|4|0|0|Dynamic Region 4|^");
	tx_message.length = (int)strlen(tx_message.data);
	cv_printer_queue.push(tx_message);

	// define print region 5, ticket promotion string
	memset(&tx_message, 0, sizeof(tx_message));
	sprintf_s(tx_message.data, sizeof(tx_message.data), "^R|5|R|383|1096|48|1095|0|3|1|5|1|1|0|0|Dynamic Region 5|^");
	tx_message.length = (int)strlen(tx_message.data);
	cv_printer_queue.push(tx_message);

	// define print region 6, game denomination and credit wagered
	memset(&tx_message, 0, sizeof(tx_message));
	sprintf_s(tx_message.data, sizeof(tx_message.data), "^R|6|R|419|1056|48|1012|0|3|1|5|1|1|0|0|Dynamic Region 6|^");
	tx_message.length = (int)strlen(tx_message.data);
	cv_printer_queue.push(tx_message);

	// define print region 7, machine id
	memset(&tx_message, 0, sizeof(tx_message));
	sprintf_s(tx_message.data, sizeof(tx_message.data), "^R|7|R|453|1101|48|1101|0|3|1|5|1|1|0|0|Dynamic Region 7|^");
	tx_message.length = (int)strlen(tx_message.data);
	cv_printer_queue.push(tx_message);

	// define print region 8, verified line
	memset(&tx_message, 0, sizeof(tx_message));
	sprintf_s(tx_message.data, sizeof(tx_message.data), "^R|8|R|479|606|2|361|0|3|0|D|1|0|0|0|Line|^");
	tx_message.length = (int)strlen(tx_message.data);
	cv_printer_queue.push(tx_message);

	// define print region 9, witness line
	memset(&tx_message, 0, sizeof(tx_message));
	sprintf_s(tx_message.data, sizeof(tx_message.data), "^R|9|R|481|33|2|361|0|3|0|D|1|0|0|0|Line|^");
	tx_message.length = (int)strlen(tx_message.data);
	cv_printer_queue.push(tx_message);

	// define print region A, "Cashier:"
	memset(&tx_message, 0, sizeof(tx_message));
	sprintf_s(tx_message.data, sizeof(tx_message.data), "^R|A|R|511|966|24|104|0|3|0|7|1|1|0|1|Cashier:|^");
	tx_message.length = (int)strlen(tx_message.data);
	cv_printer_queue.push(tx_message);

	// define print region B, "Winner:"
	memset(&tx_message, 0, sizeof(tx_message));
	sprintf_s(tx_message.data, sizeof(tx_message.data), "^R|B|R|514|391|24|105|0|3|0|7|1|1|0|1|Winner:|^");
	tx_message.length = (int)strlen(tx_message.data);
	cv_printer_queue.push(tx_message);

	// define template 0
	memset(&tx_message, 0, sizeof(tx_message));
	sprintf_s(tx_message.data, sizeof(tx_message.data), "^T|0|R|500|1240|0|1|2|3|4|5|6|7|8|9|A|B|^");
	tx_message.length = (int)strlen(tx_message.data);
	cv_printer_queue.push(tx_message);

	// set template version string
	memset(&tx_message, 0, sizeof(tx_message));
	sprintf_s(tx_message.data, sizeof(tx_message.data), "^z|V|P|CASH-VOUCHER-1.0|^");
	tx_message.length = (int)strlen(tx_message.data);
	cv_printer_queue.push(tx_message);

	// store template to printer's flash memory
	memset(&tx_message, 0, sizeof(tx_message));
	sprintf_s(tx_message.data, sizeof(tx_message.data), "^z|$|^");
	tx_message.length = (int)strlen(tx_message.data);
	cv_printer_queue.push(tx_message);
}

void CGbProManager::initialGbProWinCatagories (void)
{
    int i;

    for (i=0; i < NUMBER_OF_WIN_CATEGORIES; i++)
        memset(&gbpro_win_categories[i], 0, sizeof(GbProWinCategory));

    // Royal Flushes
    gbpro_win_categories[0].promotion_code = ROYAL_FLUSH_CLUBS_PROMO_CODE;
    gbpro_win_categories[0].evaluation_descriptor = &royal_flush_clubs_attr;
    strcpy_s(gbpro_win_categories[0].media_string, MAX_MEDIA_STRING_LENGTH, "royal_flush_clubs");
    gbpro_win_categories[1].promotion_code = ROYAL_FLUSH_DIAMONDS_PROMO_CODE;
    gbpro_win_categories[1].evaluation_descriptor = &royal_flush_diamonds_attr;
    strcpy_s(gbpro_win_categories[1].media_string, MAX_MEDIA_STRING_LENGTH, "royal_flush_diamonds");
    gbpro_win_categories[2].promotion_code = ROYAL_FLUSH_HEARTS_PROMO_CODE;
    gbpro_win_categories[2].evaluation_descriptor = &royal_flush_hearts_attr;
    strcpy_s(gbpro_win_categories[2].media_string, MAX_MEDIA_STRING_LENGTH, "royal_flush_hearts");
    gbpro_win_categories[3].promotion_code = ROYAL_FLUSH_SPADES_PROMO_CODE;
    gbpro_win_categories[3].evaluation_descriptor = &royal_flush_spades_attr;
    strcpy_s(gbpro_win_categories[3].media_string, MAX_MEDIA_STRING_LENGTH, "royal_flush_spades");
    gbpro_win_categories[4].promotion_code = ANY_ROYAL_FLUSH_PROMO_CODE;
    gbpro_win_categories[4].evaluation_descriptor = &any_royal_flush_attr;
    strcpy_s(gbpro_win_categories[4].media_string, MAX_MEDIA_STRING_LENGTH, "any_royal_flush");

    // Straight Flushes
    gbpro_win_categories[5].promotion_code = STRAIGHT_FLUSH_CLUBS_PROMO_CODE;
    gbpro_win_categories[5].evaluation_descriptor = &straight_flush_clubs_attr;
    strcpy_s(gbpro_win_categories[5].media_string, MAX_MEDIA_STRING_LENGTH, "straight_flush_clubs");
    gbpro_win_categories[6].promotion_code = STRAIGHT_FLUSH_DIAMONDS_PROMO_CODE;
    gbpro_win_categories[6].evaluation_descriptor = &straight_flush_diamonds_attr;
    strcpy_s(gbpro_win_categories[6].media_string, MAX_MEDIA_STRING_LENGTH, "straight_flush_diamonds");
    gbpro_win_categories[7].promotion_code = STRAIGHT_FLUSH_HEARTS_PROMO_CODE;
    gbpro_win_categories[7].evaluation_descriptor = &straight_flush_hearts_attr;
    strcpy_s(gbpro_win_categories[7].media_string, MAX_MEDIA_STRING_LENGTH, "straight_flush_hearts");
    gbpro_win_categories[8].promotion_code = STRAIGHT_FLUSH_SPADES_PROMO_CODE;
    gbpro_win_categories[8].evaluation_descriptor = &straight_flush_spades_attr;
    strcpy_s(gbpro_win_categories[8].media_string, MAX_MEDIA_STRING_LENGTH, "straight_flush_spades");
    gbpro_win_categories[9].promotion_code = ANY_STRAIGHT_FLUSH_PROMO_CODE;
    gbpro_win_categories[9].evaluation_descriptor = &any_straight_flush_attr;
    strcpy_s(gbpro_win_categories[9].media_string, MAX_MEDIA_STRING_LENGTH, "any_straight_flush");

    // Four of a Kinds    
    gbpro_win_categories[10].promotion_code = FOUR_OF_A_KIND_ACES_PROMO_CODE;
    gbpro_win_categories[10].evaluation_descriptor = &four_of_a_kind_aces_attr;
    strcpy_s(gbpro_win_categories[10].media_string, MAX_MEDIA_STRING_LENGTH, "fok_aces");
    gbpro_win_categories[11].promotion_code = FOUR_OF_A_KIND_KINGS_PROMO_CODE;
    gbpro_win_categories[11].evaluation_descriptor = &four_of_a_kind_kings_attr;
    strcpy_s(gbpro_win_categories[11].media_string, MAX_MEDIA_STRING_LENGTH, "fok_kings");
    gbpro_win_categories[12].promotion_code = FOUR_OF_A_KIND_QUEENS_PROMO_CODE;
    gbpro_win_categories[12].evaluation_descriptor = &four_of_a_kind_queens_attr;
    strcpy_s(gbpro_win_categories[12].media_string, MAX_MEDIA_STRING_LENGTH, "fok_queens");
    gbpro_win_categories[13].promotion_code = FOUR_OF_A_KIND_JACKS_PROMO_CODE;
    gbpro_win_categories[13].evaluation_descriptor = &four_of_a_kind_jacks_attr;
    strcpy_s(gbpro_win_categories[13].media_string, MAX_MEDIA_STRING_LENGTH, "fok_jacks");
    gbpro_win_categories[14].promotion_code = FOUR_OF_A_KIND_TENS_PROMO_CODE;
    gbpro_win_categories[14].evaluation_descriptor = &four_of_a_kind_tens_attr;
    strcpy_s(gbpro_win_categories[14].media_string, MAX_MEDIA_STRING_LENGTH, "fok_tens");
    gbpro_win_categories[15].promotion_code = FOUR_OF_A_KIND_NINES_PROMO_CODE;
    gbpro_win_categories[15].evaluation_descriptor = &four_of_a_kind_nines_attr;
    strcpy_s(gbpro_win_categories[15].media_string, MAX_MEDIA_STRING_LENGTH, "fok_nines");
    gbpro_win_categories[16].promotion_code = FOUR_OF_A_KIND_EIGHTS_PROMO_CODE;
    gbpro_win_categories[16].evaluation_descriptor = &four_of_a_kind_eights_attr;
    strcpy_s(gbpro_win_categories[16].media_string, MAX_MEDIA_STRING_LENGTH, "fok_eights");
    gbpro_win_categories[17].promotion_code = FOUR_OF_A_KIND_SEVENS_PROMO_CODE;
    gbpro_win_categories[17].evaluation_descriptor = &four_of_a_kind_sevens_attr;
    strcpy_s(gbpro_win_categories[17].media_string, MAX_MEDIA_STRING_LENGTH, "fok_sevens");
    gbpro_win_categories[18].promotion_code = FOUR_OF_A_KIND_SIXES_PROMO_CODE;
    gbpro_win_categories[18].evaluation_descriptor = &four_of_a_kind_sixes_attr;
    strcpy_s(gbpro_win_categories[18].media_string, MAX_MEDIA_STRING_LENGTH, "fok_sixes");
    gbpro_win_categories[19].promotion_code = FOUR_OF_A_KIND_FIVES_PROMO_CODE;
    gbpro_win_categories[19].evaluation_descriptor = &four_of_a_kind_fives_attr;
    strcpy_s(gbpro_win_categories[19].media_string, MAX_MEDIA_STRING_LENGTH, "fok_fives");
    gbpro_win_categories[20].promotion_code = FOUR_OF_A_KIND_FOURS_PROMO_CODE;
    gbpro_win_categories[20].evaluation_descriptor = &four_of_a_kind_fours_attr;
    strcpy_s(gbpro_win_categories[20].media_string, MAX_MEDIA_STRING_LENGTH, "fok_fours");
    gbpro_win_categories[21].promotion_code = FOUR_OF_A_KIND_THREES_PROMO_CODE;
    gbpro_win_categories[21].evaluation_descriptor = &four_of_a_kind_threes_attr;
    strcpy_s(gbpro_win_categories[21].media_string, MAX_MEDIA_STRING_LENGTH, "fok_threes");
    gbpro_win_categories[22].promotion_code = FOUR_OF_A_KIND_TWOS_PROMO_CODE;
    gbpro_win_categories[22].evaluation_descriptor = &four_of_a_kind_twos_attr;
    strcpy_s(gbpro_win_categories[22].media_string, MAX_MEDIA_STRING_LENGTH, "fok_twos");
    gbpro_win_categories[23].promotion_code = ANY_FOUR_OF_A_KIND_PROMO_CODE;
    gbpro_win_categories[23].evaluation_descriptor = &any_four_of_a_kind_attr;
    strcpy_s(gbpro_win_categories[23].media_string, MAX_MEDIA_STRING_LENGTH, "any_fok");


    // Full Houses
    gbpro_win_categories[24].promotion_code = FULL_HOUSE_FACE_CARDS_PROMO_CODE;
    gbpro_win_categories[24].evaluation_descriptor = &full_house_all_face_cards_attr;
    strcpy_s(gbpro_win_categories[24].media_string, MAX_MEDIA_STRING_LENGTH, "full_house_all_face_cards");
    gbpro_win_categories[25].promotion_code = FULL_HOUSE_ACES_PROMO_CODE;
    gbpro_win_categories[25].evaluation_descriptor = &full_house_aces_attr;
    strcpy_s(gbpro_win_categories[25].media_string, MAX_MEDIA_STRING_LENGTH, "full_house_aces");
    gbpro_win_categories[26].promotion_code = FULL_HOUSE_KINGS_PROMO_CODE;
    gbpro_win_categories[26].evaluation_descriptor = &full_house_kings_attr;
    strcpy_s(gbpro_win_categories[26].media_string, MAX_MEDIA_STRING_LENGTH, "full_house_kings");
    gbpro_win_categories[27].promotion_code = FULL_HOUSE_QUEENS_PROMO_CODE;
    gbpro_win_categories[27].evaluation_descriptor = &full_house_queens_attr;
    strcpy_s(gbpro_win_categories[27].media_string, MAX_MEDIA_STRING_LENGTH, "full_house_queens");
    gbpro_win_categories[28].promotion_code = FULL_HOUSE_JACKS_PROMO_CODE;
    gbpro_win_categories[28].evaluation_descriptor = &full_house_jacks_attr;
    strcpy_s(gbpro_win_categories[28].media_string, MAX_MEDIA_STRING_LENGTH, "full_house_jacks");
    gbpro_win_categories[29].promotion_code = FULL_HOUSE_TENS_PROMO_CODE;
    gbpro_win_categories[29].evaluation_descriptor = &full_house_tens_attr;
    strcpy_s(gbpro_win_categories[29].media_string, MAX_MEDIA_STRING_LENGTH, "full_house_tens");
    gbpro_win_categories[30].promotion_code = FULL_HOUSE_NINES_PROMO_CODE;
    gbpro_win_categories[30].evaluation_descriptor = &full_house_nines_attr;
    strcpy_s(gbpro_win_categories[30].media_string, MAX_MEDIA_STRING_LENGTH, "full_house_nines");
    gbpro_win_categories[31].promotion_code = FULL_HOUSE_EIGHTS_PROMO_CODE;
    gbpro_win_categories[31].evaluation_descriptor = &full_house_eights_attr;
    strcpy_s(gbpro_win_categories[31].media_string, MAX_MEDIA_STRING_LENGTH, "full_house_eights");
    gbpro_win_categories[32].promotion_code = FULL_HOUSE_SEVENS_PROMO_CODE;
    gbpro_win_categories[32].evaluation_descriptor = &full_house_sevens_attr;
    strcpy_s(gbpro_win_categories[32].media_string, MAX_MEDIA_STRING_LENGTH, "full_house_sevens");
    gbpro_win_categories[33].promotion_code = FULL_HOUSE_SIXES_PROMO_CODE;
    gbpro_win_categories[33].evaluation_descriptor = &full_house_sixes_attr;
    strcpy_s(gbpro_win_categories[33].media_string, MAX_MEDIA_STRING_LENGTH, "full_house_sixes");
    gbpro_win_categories[34].promotion_code = FULL_HOUSE_FIVES_PROMO_CODE;
    gbpro_win_categories[34].evaluation_descriptor = &full_house_fives_attr;
    strcpy_s(gbpro_win_categories[34].media_string, MAX_MEDIA_STRING_LENGTH, "full_house_fives");
    gbpro_win_categories[35].promotion_code = FULL_HOUSE_FOURS_PROMO_CODE;
    gbpro_win_categories[35].evaluation_descriptor = &full_house_fours_attr;
    strcpy_s(gbpro_win_categories[35].media_string, MAX_MEDIA_STRING_LENGTH, "full_house_fours");
    gbpro_win_categories[36].promotion_code = FULL_HOUSE_THREES_PROMO_CODE;
    gbpro_win_categories[36].evaluation_descriptor = &full_house_threes_attr;
    strcpy_s(gbpro_win_categories[36].media_string, MAX_MEDIA_STRING_LENGTH, "full_house_threes");
    gbpro_win_categories[37].promotion_code = FULL_HOUSE_TWOS_PROMO_CODE;
    gbpro_win_categories[37].evaluation_descriptor = &full_house_twos_attr;
    strcpy_s(gbpro_win_categories[37].media_string, MAX_MEDIA_STRING_LENGTH, "full_house_twos");
    gbpro_win_categories[38].promotion_code = ANY_FULL_HOUSE_PROMO_CODE;
    gbpro_win_categories[38].evaluation_descriptor = &any_full_house_attr;
    strcpy_s(gbpro_win_categories[38].media_string, MAX_MEDIA_STRING_LENGTH, "any_full_house");

    // Flushes
    gbpro_win_categories[39].promotion_code = FLUSH_CLUBS_PROMO_CODE;
    gbpro_win_categories[39].evaluation_descriptor = &flush_clubs_attr;
    strcpy_s(gbpro_win_categories[39].media_string, MAX_MEDIA_STRING_LENGTH, "flush_clubs");
    gbpro_win_categories[40].promotion_code = FLUSH_DIAMONDS_PROMO_CODE;
    gbpro_win_categories[40].evaluation_descriptor = &flush_diamonds_attr;
    strcpy_s(gbpro_win_categories[40].media_string, MAX_MEDIA_STRING_LENGTH, "flush_diamonds");
    gbpro_win_categories[41].promotion_code = FLUSH_HEARTS_PROMO_CODE;
    gbpro_win_categories[41].evaluation_descriptor = &flush_hearts_attr;
    strcpy_s(gbpro_win_categories[41].media_string, MAX_MEDIA_STRING_LENGTH, "flush_hearts");
    gbpro_win_categories[42].promotion_code = FLUSH_SPADES_PROMO_CODE;
    gbpro_win_categories[42].evaluation_descriptor = &flush_spades_attr;
    strcpy_s(gbpro_win_categories[42].media_string, MAX_MEDIA_STRING_LENGTH, "flush_spades");
    gbpro_win_categories[43].promotion_code = BLACK_FLUSH_PROMO_CODE;
    gbpro_win_categories[43].evaluation_descriptor = &flush_black_attr;
    strcpy_s(gbpro_win_categories[43].media_string, MAX_MEDIA_STRING_LENGTH, "flush_black");
    gbpro_win_categories[44].promotion_code = RED_FLUSH_PROMO_CODE;
    gbpro_win_categories[44].evaluation_descriptor = &flush_red_attr;
    strcpy_s(gbpro_win_categories[44].media_string, MAX_MEDIA_STRING_LENGTH, "flush_red");
    gbpro_win_categories[45].promotion_code = ANY_FLUSH_PROMO_CODE;
    gbpro_win_categories[45].evaluation_descriptor = &flush_any_attr;
    strcpy_s(gbpro_win_categories[45].media_string, MAX_MEDIA_STRING_LENGTH, "flush_any");
    gbpro_win_categories[46].promotion_code = ANY_STRAIGHT_PROMO_CODE;
    gbpro_win_categories[46].evaluation_descriptor = &any_straight_attr;
    strcpy_s(gbpro_win_categories[46].media_string, MAX_MEDIA_STRING_LENGTH, "any_straight");

    // Three of a Kinds
    gbpro_win_categories[47].promotion_code = THREE_OF_A_KIND_ACES_PROMO_CODE;
    gbpro_win_categories[47].evaluation_descriptor = &three_of_a_kind_aces_attr;
    strcpy_s(gbpro_win_categories[47].media_string, MAX_MEDIA_STRING_LENGTH, "tok_aces");
    gbpro_win_categories[48].promotion_code = THREE_OF_A_KIND_KINGS_PROMO_CODE;
    gbpro_win_categories[48].evaluation_descriptor = &three_of_a_kind_kings_attr;
    strcpy_s(gbpro_win_categories[48].media_string, MAX_MEDIA_STRING_LENGTH, "tok_kings");
    gbpro_win_categories[49].promotion_code = THREE_OF_A_KIND_QUEENS_PROMO_CODE;
    gbpro_win_categories[49].evaluation_descriptor = &three_of_a_kind_queens_attr;
    strcpy_s(gbpro_win_categories[49].media_string, MAX_MEDIA_STRING_LENGTH, "tok_queens");
    gbpro_win_categories[50].promotion_code = THREE_OF_A_KIND_JACKS_PROMO_CODE;
    gbpro_win_categories[50].evaluation_descriptor = &three_of_a_kind_jacks_attr;
    strcpy_s(gbpro_win_categories[50].media_string, MAX_MEDIA_STRING_LENGTH, "tok_jacks");
    gbpro_win_categories[51].promotion_code = THREE_OF_A_KIND_TENS_PROMO_CODE;
    gbpro_win_categories[51].evaluation_descriptor = &three_of_a_kind_tens_attr;
    strcpy_s(gbpro_win_categories[51].media_string, MAX_MEDIA_STRING_LENGTH, "tok_tens");
    gbpro_win_categories[52].promotion_code = THREE_OF_A_KIND_NINES_PROMO_CODE;
    gbpro_win_categories[52].evaluation_descriptor = &three_of_a_kind_nines_attr;
    strcpy_s(gbpro_win_categories[52].media_string, MAX_MEDIA_STRING_LENGTH, "tok_nines");
    gbpro_win_categories[53].promotion_code = THREE_OF_A_KIND_EIGHTS_PROMO_CODE;
    gbpro_win_categories[53].evaluation_descriptor = &three_of_a_kind_eights_attr;
    strcpy_s(gbpro_win_categories[53].media_string, MAX_MEDIA_STRING_LENGTH, "tok_eights");
    gbpro_win_categories[54].promotion_code = THREE_OF_A_KIND_SEVENS_PROMO_CODE;
    gbpro_win_categories[54].evaluation_descriptor = &three_of_a_kind_sevens_attr;
    strcpy_s(gbpro_win_categories[54].media_string, MAX_MEDIA_STRING_LENGTH, "tok_sevens");
    gbpro_win_categories[55].promotion_code = THREE_OF_A_KIND_SIXES_PROMO_CODE;
    gbpro_win_categories[55].evaluation_descriptor = &three_of_a_kind_sixes_attr;
    strcpy_s(gbpro_win_categories[55].media_string, MAX_MEDIA_STRING_LENGTH, "tok_sixes");
    gbpro_win_categories[56].promotion_code = THREE_OF_A_KIND_FIVES_PROMO_CODE;
    gbpro_win_categories[56].evaluation_descriptor = &three_of_a_kind_fives_attr;
    strcpy_s(gbpro_win_categories[56].media_string, MAX_MEDIA_STRING_LENGTH, "tok_fives");
    gbpro_win_categories[57].promotion_code = THREE_OF_A_KIND_FOURS_PROMO_CODE;
    gbpro_win_categories[57].evaluation_descriptor = &three_of_a_kind_fours_attr;
    strcpy_s(gbpro_win_categories[57].media_string, MAX_MEDIA_STRING_LENGTH, "tok_fours");
    gbpro_win_categories[58].promotion_code = THREE_OF_A_KIND_THREES_PROMO_CODE;
    gbpro_win_categories[58].evaluation_descriptor = &three_of_a_kind_threes_attr;
    strcpy_s(gbpro_win_categories[58].media_string, MAX_MEDIA_STRING_LENGTH, "tok_threes");
    gbpro_win_categories[59].promotion_code = THREE_OF_A_KIND_TWOS_PROMO_CODE;
    gbpro_win_categories[59].evaluation_descriptor = &three_of_a_kind_twos_attr;
    strcpy_s(gbpro_win_categories[59].media_string, MAX_MEDIA_STRING_LENGTH, "tok_twos");
    gbpro_win_categories[60].promotion_code = ANY_THREE_OF_A_KIND_PROMO_CODE;
    gbpro_win_categories[60].evaluation_descriptor = &any_three_of_a_kind_attr;
    strcpy_s(gbpro_win_categories[60].media_string, MAX_MEDIA_STRING_LENGTH, "any_tok");
}

void CGbProManager::ProcessSetPromotionWinCategory (BYTE *buffer, BYTE message_id, bool send_to_idc)
{
    IdcGbAdvantageWinCategory* category = (IdcGbAdvantageWinCategory*)buffer;
    int promotion_code_index = GetPromotionCodeIndex(category->promotion_code);

    if (promotion_code_index < NUMBER_OF_WIN_CATEGORIES)
    {
        gbpro_win_categories[promotion_code_index].points_award = category->points_award;
        gbpro_win_categories[promotion_code_index].cash_voucher_award = category->cash_voucher_award;
        gbpro_win_categories[promotion_code_index].gb_login_not_required = category->gb_login_not_required;
        gbpro_win_categories[promotion_code_index].print_drawing_ticket = category->print_drawing_ticket;
        gbpro_win_categories[promotion_code_index].scalable_award = category->scalable_award;
        gbpro_win_categories[promotion_code_index].start_time = category->start_time;
        gbpro_win_categories[promotion_code_index].end_time = category->end_time;
        gbpro_win_categories[promotion_code_index].minimum_money_bet = category->minimum_money_bet;
        gbpro_win_categories[promotion_code_index].minimum_bet = category->minimum_bet;
        gbpro_win_categories[promotion_code_index].minimum_denomination = category->minimum_denomination;
        gbpro_win_categories[promotion_code_index].i_rewards_paid_win = category->i_rewards_paid_win;
		gbpro_win_categories[promotion_code_index].cashable_credits = category->cashable_credits * 100;
		gbpro_win_categories[promotion_code_index].noncashable_credits = category->noncashable_credits * 100;

        memset (&gbpro_win_categories[promotion_code_index].drawing_ticket_string, 0, sizeof(gbpro_win_categories[promotion_code_index].drawing_ticket_string));

		if (send_to_idc)
			theApp.SendIdcSetPromotionWinCategoryMessage ((IdcGbAdvantageWinCategory*) &gbpro_win_categories[promotion_code_index]);
    }
    else
    {
        logMessage("CGbProManager::ProcessSetPromotionWinCategory - invalid promotion code");
    }
}

int CGbProManager::GetPromotionCodeIndex (BYTE promotion_code)
{
	int i;
	int index = 0xFF;

    for (i=0; i < NUMBER_OF_WIN_CATEGORIES; i++)
        if (gbpro_win_categories[i].promotion_code == promotion_code)
            index = i;

    return index;
}

void CGbProManager::CheckGbProPorts()
{
	if (memory_gb_pro_settings.cash_voucher_printer_port)
	{
		if (pCvPrinterSocket && pCvPrinterSocket->socket_state)
			OpenCvPrinterTcpIpPort();

		if (!pCvPrinterSocket && cv_printer_time_check)
			OpenCvPrinterSerialPort();
	}
}


BYTE CGbProManager::GetNextDisplayCategoryIndex (void)
{
    int i;
    BYTE next_display_category_index = ACTIVE_GB_PRO_WIN_CATEGORY_NOT_FOUND;

    for (i=display_category_index + 1; i < NUMBER_OF_WIN_CATEGORIES; i++)
    {
        if (winCategoryActive(i))
        {
            next_display_category_index = i;
            break;
        }
    }

    if (next_display_category_index == ACTIVE_GB_PRO_WIN_CATEGORY_NOT_FOUND)
    {
        for (i=0; i < display_category_index; i++)
        {
            if (winCategoryActive(i))
            {
                next_display_category_index = i;
                break;
            }
        }
    }

    return next_display_category_index;
}

void CGbProManager::ProcessGbProRequests()
{
	if (memory_gb_pro_settings.cash_voucher_printer_port && ((pCvPrinterSocket && !pCvPrinterSocket->socket_state) ||
		(!pCvPrinterSocket && !cv_printer_time_check)))
	{
		CheckCvPrinterReceive();
		CheckCvPrinterTransmit();
	}
}

void CGbProManager::CvTxPrinterData(char *data, int length)
{
 int bytes_sent, last_error;

    if (length)
    {
        if (pCvPrinterSocket)
        {
            bytes_sent = pCvPrinterSocket->Send(data, length);

			if (bytes_sent == SOCKET_ERROR)
			{
				last_error = GetLastError();

				if (last_error != WSAEINPROGRESS && last_error != WSAEWOULDBLOCK)
				{
					pCvPrinterSocket->Close();
					pCvPrinterSocket->socket_state = WSAENOTCONN;
				}
			}
		}
		else if (hCvPrinterPort != INVALID_HANDLE_VALUE)
		{
			if (!WriteFile(hCvPrinterPort, data, length, (LPDWORD)&bytes_sent, NULL))
				cv_printer_time_check = time(NULL);
			else if (bytes_sent != length)
				cv_printer_time_check = time(NULL);
		}
	}
}

char* CGbProManager::GetAsciiAmountString(DWORD amount)
{
 static char return_string[1000];
 char cents_string[100], amount_string[100], thousands_place, hundreds_place, tens_place, ones_place;
 div_t cents_calculation;

    memset(&return_string, 0, sizeof(return_string));

    cents_calculation = div(amount, 100);
    sprintf_s(cents_string, sizeof(cents_string), " DOLLARS AND %d CENTS", cents_calculation.rem);

    amount /= 100;
    sprintf_s(amount_string, sizeof(amount_string), "%u", amount);

    thousands_place = '0';
    hundreds_place  = '0';
    tens_place      = '0';
    ones_place      = '0';

    switch (strlen(amount_string))
    {
        case 1:
            ones_place = amount_string[0];
            break;

        case 2:
            tens_place = amount_string[0];
            ones_place = amount_string[1];
            break;

        case 3:
            hundreds_place = amount_string[0];
            tens_place     = amount_string[1];
            ones_place     = amount_string[2];
            break;

        case 4:
            thousands_place = amount_string[0];
            hundreds_place  = amount_string[1];
            tens_place      = amount_string[2];
            ones_place      = amount_string[3];
            break;
    }

    if (thousands_place == '0' && hundreds_place == '0' && tens_place == '0' && ones_place == '0')
        sprintf_s(return_string, sizeof(return_string), "ZERO");
    else
    {
        if (thousands_place == '1')
            sprintf_s(return_string, sizeof(return_string), "ONE THOUSAND ");
        else if (thousands_place == '2')
            sprintf_s(return_string, sizeof(return_string), "TWO THOUSAND ");
        else if (thousands_place == '3')
            sprintf_s(return_string, sizeof(return_string), "THREE THOUSAND ");
        else if (thousands_place == '4')
            sprintf_s(return_string, sizeof(return_string), "FOUR THOUSAND ");
        else if (thousands_place == '5')
            sprintf_s(return_string, sizeof(return_string), "FIVE THOUSAND ");
        else if (thousands_place == '6')
            sprintf_s(return_string, sizeof(return_string), "SIX THOUSAND ");
        else if (thousands_place == '7')
            sprintf_s(return_string, sizeof(return_string), "SEVEN THOUSAND ");
        else if (thousands_place == '8')
            sprintf_s(return_string, sizeof(return_string), "EIGHT THOUSAND ");
        else if (thousands_place == '9')
            sprintf_s(return_string, sizeof(return_string), "NINE THOUSAND ");

        if (hundreds_place == '1')
            strcat_s(return_string, sizeof(return_string), "ONE HUNDRED ");
        else if (hundreds_place == '2')
            strcat_s(return_string, sizeof(return_string), "TWO HUNDRED ");
        else if (hundreds_place == '3')
            strcat_s(return_string, sizeof(return_string), "THREE HUNDRED ");
        else if (hundreds_place == '4')
            strcat_s(return_string, sizeof(return_string), "FOUR HUNDRED ");
        else if (hundreds_place == '5')
            strcat_s(return_string, sizeof(return_string), "FIVE HUNDRED ");
        else if (hundreds_place == '6')
            strcat_s(return_string, sizeof(return_string), "SIX HUNDRED ");
        else if (hundreds_place == '7')
            strcat_s(return_string, sizeof(return_string), "SEVEN HUNDRED ");
        else if (hundreds_place == '8')
            strcat_s(return_string, sizeof(return_string), "EIGHT HUNDRED ");
        else if (hundreds_place == '9')
            strcat_s(return_string, sizeof(return_string), "NINE HUNDRED ");

        if (tens_place == '1')
        {
            if (ones_place == '0')
                strcat_s(return_string, sizeof(return_string), "TEN");
            else if (ones_place == '1')
                strcat_s(return_string, sizeof(return_string), "ELEVEN");
            else if (ones_place == '2')
                strcat_s(return_string, sizeof(return_string), "TWELVE");
            else if (ones_place == '3')
                strcat_s(return_string, sizeof(return_string), "THIRTEEN");
            else if (ones_place == '4')
                strcat_s(return_string, sizeof(return_string), "FOURTEEN");
            else if (ones_place == '5')
                strcat_s(return_string, sizeof(return_string), "FIFTEEN");
            else if (ones_place == '6')
                strcat_s(return_string, sizeof(return_string), "SIXTEEN");
            else if (ones_place == '7')
                strcat_s(return_string, sizeof(return_string), "SEVENTEEN");
            else if (ones_place == '8')
                strcat_s(return_string, sizeof(return_string), "EIGHTEEN");
            else if (ones_place == '9')
                strcat_s(return_string, sizeof(return_string), "NINETEEN");
        }
        else
        {
            memset(amount_string, 0, sizeof(amount_string));
            if (tens_place == '2')
                sprintf_s(amount_string, sizeof(amount_string), "TWENTY");
            else if (tens_place == '3')
                sprintf_s(amount_string, sizeof(amount_string), "THRITY");
            else if (tens_place == '4')
                sprintf_s(amount_string, sizeof(amount_string), "FOURTY");
            else if (tens_place == '5')
                sprintf_s(amount_string, sizeof(amount_string), "FIFTY");
            else if (tens_place == '6')
                sprintf_s(amount_string, sizeof(amount_string), "SIXTY");
            else if (tens_place == '7')
                sprintf_s(amount_string, sizeof(amount_string), "SEVENTY");
            else if (tens_place == '8')
                sprintf_s(amount_string, sizeof(amount_string), "EIGHTY");
            else if (tens_place == '9')
                sprintf_s(amount_string, sizeof(amount_string), "NINETY");

            if (strlen(amount_string))
            {
                strcat_s(return_string, sizeof(return_string), amount_string);
                if (ones_place != '0')
                    strcat_s(return_string, sizeof(return_string), "-");
            }

            if (ones_place == '1')
                strcat_s(return_string, sizeof(return_string), "ONE");
            else if (ones_place == '2')
                strcat_s(return_string, sizeof(return_string), "TWO");
            else if (ones_place == '3')
                strcat_s(return_string, sizeof(return_string), "THREE");
            else if (ones_place == '4')
                strcat_s(return_string, sizeof(return_string), "FOUR");
            else if (ones_place == '5')
                strcat_s(return_string, sizeof(return_string), "FIVE");
            else if (ones_place == '6')
                strcat_s(return_string, sizeof(return_string), "SIX");
            else if (ones_place == '7')
                strcat_s(return_string, sizeof(return_string), "SEVEN");
            else if (ones_place == '8')
                strcat_s(return_string, sizeof(return_string), "EIGHT");
            else if (ones_place == '9')
                strcat_s(return_string, sizeof(return_string), "NINE");
        }
    }

    strcat_s(return_string, sizeof(return_string), cents_string);

 return return_string;
}

void CGbProManager::SendCvToPrinter(CashVoucherTickets *pCashVoucherTickets)
{
 char location_name[MAXIMUM_CLUB_NAME_FOR_MUX + 1], location_address[MAXIMUM_CLUB_ADDRESS_FOR_MUX + 1];
 size_t string_length;
 CString am_or_pm, closing_am_or_pm, format_string, message_string;
 PrinterMessage tx_message;
 struct tm time_struct;

	// check location name length
	memset(location_name, 0, sizeof(location_name));
	if (strlen(theApp.memory_location_config.name) < sizeof(location_name))
		string_length = strlen(theApp.memory_location_config.name);
	else
		string_length = sizeof(location_name) - 1;
	memcpy(location_name, theApp.memory_location_config.name, string_length);

	// check location address length
	memset(location_address, 0, sizeof(location_address));
	if (strlen(theApp.memory_location_config.address) < sizeof(location_address))
		string_length = strlen(theApp.memory_location_config.address);
	else
		string_length = sizeof(location_name) - 1;
	memcpy(location_address, theApp.memory_location_config.address, string_length);

	message_string.Format("^P|0|1|%s|%s|", location_name, location_address);

	if (localtime_s(&time_struct, &pCashVoucherTickets->time_stamp))
		format_string.Format("UNKNOWN TIME STAMP: %u|", pCashVoucherTickets->time_stamp);
	else
	{
	    if (time_struct.tm_hour < 12)
    	    am_or_pm = "am";
	    else
    	{
        	if (time_struct.tm_hour > 12)
            	time_struct.tm_hour -= (WORD)12;
	        am_or_pm = "pm";
    	}

		format_string.Format("Time: %2.2d:%2.2d:%2.2d %s   Date: %d/%d/%2.2d |", time_struct.tm_hour, time_struct.tm_min,
			time_struct.tm_sec, am_or_pm, time_struct.tm_mon + 1, time_struct.tm_mday, time_struct.tm_year - 100);
	}

	message_string += format_string;

    if (pCashVoucherTickets->print_drawing_ticket && (pCashVoucherTickets->cash_voucher_amount == 0))
    {
        int promotion_code_index = GetPromotionCodeIndex(pCashVoucherTickets->promotion_id);

        if ((promotion_code_index < NUMBER_OF_WIN_CATEGORIES) && (strlen(gbpro_win_categories[promotion_code_index].drawing_ticket_string) > 0))
        {
    	    format_string.Format("%s|$%4.2f|", gbpro_win_categories[promotion_code_index].drawing_ticket_string, 0);
        }
        else
    	    format_string.Format("%s|$%4.2f|", "Drawing Ticket", 0);
    }
    else
    {
	    format_string.Format("%s|$%4.2f|", GetAsciiAmountString(pCashVoucherTickets->cash_voucher_amount),
            (double)pCashVoucherTickets->cash_voucher_amount / 100);
    }
    message_string += format_string;

    message_string += "Promotion: ";

    if (strlen(pCashVoucherTickets->promo_string))
    	message_string += pCashVoucherTickets->promo_string;

	format_string.Format("|Denomination: $%4.2f  Bet: %u  Machine ID: %u|",
		(double)pCashVoucherTickets->game_denomination / 100, pCashVoucherTickets->credits_wagered,
		pCashVoucherTickets->game_ucmc_id);
    message_string += format_string;

    if (pCashVoucherTickets->player_name[0])
        format_string.Format("Player: %s  Account: %u|||||^", pCashVoucherTickets->player_name, pCashVoucherTickets->player_account);
    else
        format_string.Format("Account: %u|||||^", pCashVoucherTickets->player_account);
    message_string += format_string;

	// send brief message to lat
	if (!pCashVoucherTickets->suppress_printing_on_lat)
	{
		format_string.Format("\nCASH VOUCHER\nAmount $%7.2f\nMACHINE ID: %u\nDENOM $%5.2f\n"
			"CREDITS WAGERED %u\n%d/%d/%2.2d  %2.2d:%2.2d:%2.2d %s\n\n",
			(double)pCashVoucherTickets->cash_voucher_amount / 100, pCashVoucherTickets->game_ucmc_id,
			(double)pCashVoucherTickets->game_denomination / 100, pCashVoucherTickets->credits_wagered,
			time_struct.tm_mon + 1, time_struct.tm_mday, time_struct.tm_year - 100,
			time_struct.tm_hour, time_struct.tm_min, time_struct.tm_sec, am_or_pm);

        theApp.PrinterHeader(0);
        theApp.PrinterData(format_string.GetBuffer(0));
	}

	memset(&tx_message, 0, sizeof(tx_message));
	tx_message.length = message_string.GetLength();
	memcpy(&tx_message.data, message_string.GetBuffer(0), tx_message.length);
	cv_printer_queue.push(tx_message);
}

bool CGbProManager::ClearToSendCvPrinterMessage()
{
 PrinterMessage tx_message;

    // clear printer reset or power up flag
    if (cv_printer_idle_msg_status_bits[4] & 0x01)
    {
        if (!cv_printer_reset.flag_sent)
        {
            sprintf_s(tx_message.data, sizeof(tx_message.data), "^C|^");
            tx_message.length = (int)strlen(tx_message.data);
            cv_printer_queue.push(tx_message);

            cv_printer_reset.flag_sent = true;
            cv_printer_reset.time_sent = time(NULL);
        }
    }

    if (cv_printer_poll_wait > 3 && cv_printer_idle_msg_status_bits[0] & 0x40 &&
        cv_printer_idle_msg_status_bits[1] & 0x40 && cv_printer_idle_msg_status_bits[2] & 0x40 &&
        cv_printer_idle_msg_status_bits[3] & 0x40 && cv_printer_idle_msg_status_bits[4] & 0x40 &&
        !(cv_printer_idle_msg_status_bits[0] & 0x3F) && !(cv_printer_idle_msg_status_bits[1] & 0x3F) &&
        !(cv_printer_idle_msg_status_bits[2] & 0x3F) && !(cv_printer_idle_msg_status_bits[3] & 0x06) &&
        !(cv_printer_idle_msg_status_bits[4] & 0x04))
    {
        cv_printer_poll_wait = 0;
        return true;
    }
    else
        cv_printer_poll_wait++;

 return false;
}




bool CGbProManager::winCategoryActive (int win_category_index, DWORD account_number, NewBlock* block)
{
    bool active = FALSE;
    time_t current_time = time(NULL);

	DWORD points_award = gbpro_win_categories[win_category_index].points_award;
	DWORD cash_voucher_award = gbpro_win_categories[win_category_index].cash_voucher_award;
	DWORD start_time = gbpro_win_categories[win_category_index].start_time;
	DWORD end_time = gbpro_win_categories[win_category_index].end_time;
	DWORD minimum_money_bet = gbpro_win_categories[win_category_index].minimum_money_bet;
	BYTE minimum_bet = gbpro_win_categories[win_category_index].minimum_bet;
	WORD minimum_denomination = gbpro_win_categories[win_category_index].minimum_denomination;
	BYTE promotion_code = gbpro_win_categories[win_category_index].promotion_code;
    BYTE gb_login_not_required = gbpro_win_categories[win_category_index].gb_login_not_required;
    BYTE print_drawing_ticket = gbpro_win_categories[win_category_index].print_drawing_ticket;
	BYTE  i_rewards_paid_win = gbpro_win_categories[win_category_index].i_rewards_paid_win;
    DWORD cashable_credits = gbpro_win_categories[win_category_index].cashable_credits;
    DWORD noncashable_credits = gbpro_win_categories[win_category_index].noncashable_credits;

	if (((theApp.memory_settings_ini.cms_ip_address && theApp.memory_settings_ini.cms_tcp_port) || !gb_login_not_required) &&
		!account_number)
	{
		active = FALSE;
	}
	else
	{
		// quick check to see if gb win
		if ((current_time >= (time_t)start_time) && (end_time >= (DWORD)current_time))
		{
			if ((points_award || cash_voucher_award || print_drawing_ticket || i_rewards_paid_win || cashable_credits || noncashable_credits) &&
				(account_number || gb_login_not_required))
			{
				if (minimum_money_bet)
				{
					if ((block->bet * block->denomination) >= minimum_money_bet)
						active = TRUE;
				}
				else
				{
					if ((block->bet >= minimum_bet) && (block->denomination >= minimum_denomination))
						active = TRUE;
				}
			}
		}    
	}

    return active;
}

bool CGbProManager::winCategoryActive (int win_category_index)
{
    bool active = FALSE;
    time_t current_time = time(NULL);

	DWORD points_award = gbpro_win_categories[win_category_index].points_award;
	DWORD cash_voucher_award = gbpro_win_categories[win_category_index].cash_voucher_award;
    BYTE  print_drawing_ticket = gbpro_win_categories[win_category_index].print_drawing_ticket;
	DWORD start_time = gbpro_win_categories[win_category_index].start_time;
	DWORD end_time = gbpro_win_categories[win_category_index].end_time;
	BYTE  i_rewards_paid_win = gbpro_win_categories[win_category_index].i_rewards_paid_win;
    DWORD cashable_credits = gbpro_win_categories[win_category_index].cashable_credits;
    DWORD noncashable_credits = gbpro_win_categories[win_category_index].noncashable_credits;

    // quick check to see if gb win
    if ((current_time >= (time_t)start_time) && (end_time >= (DWORD)current_time))
    {
        if (points_award || cash_voucher_award || print_drawing_ticket || i_rewards_paid_win || cashable_credits || noncashable_credits)
                active = TRUE;
    }    

    return active;
}

GbProWinCategory CGbProManager::getWinCategory (UINT category)
{
    if (category <  NUMBER_OF_WIN_CATEGORIES)
        return (gbpro_win_categories[category]);
}

char* CGbProManager::GetGbProPointsTicketsCashPromoString(WORD promotion_id)
{
	int index =	GetPromotionCodeIndex ((BYTE)promotion_id);

	char promo_string [MAX_MEDIA_STRING_LENGTH];
	memset(promo_string, 0, sizeof(MAX_MEDIA_STRING_LENGTH));

    if (index < NUMBER_OF_WIN_CATEGORIES)
	{
		GbProWinCategory win_category = getWinCategory (index);
		strcpy(promo_string, win_category.media_string);
	}

	return promo_string;
}

bool CGbProManager::isGbaHandPromotion (WORD promotion_id)
{
    bool hand_promo = TRUE;

    switch (promotion_id)
    {
        case G_INTERACTIVE_AWARD_PROMO_CODE:
        case W_WIN_AMOUNT_BASED_AWARD_PROMO_CODE:
            hand_promo = FALSE;
            break;
    }

    return hand_promo;
}


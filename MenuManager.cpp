/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller	12/04/13	Allow manager awarded points on the XP Controller if there is not an IDC ip address or game tender client available.
Robert Fuller	07/03/13	Added code to launch the old GB Trax application if there is no IDC and the GameTenderClient.exe file is not present on the controller hardware.
Robert Fuller   03/08/13    Removed obsolete Location Collection code.
Robert Fuller   09/17/12    Hide the GB Trax button in the Menu Manager screen if there is a Game Tender exe on the XPC box.
Robert Fuller   02/07/12    Make sure GB Advantage is configured on before accessing the class pointer potentially uninitialized class pointer
Robert Fuller   06/28/11    Allow manager to access the dispenser functions in the Collection screen.
Robert Fuller   03/23/10    Hide the button that displays the manager awarded points screen for non-manager level log in numbers.
Robert Fuller   09/03/09    Added software mods to support running the XP Controller in stand alone mode which allows locations to operate with basic features without connecting to the main frame.
Robert Fuller   07/27/09    Put the mechanism for the Manager to post GB points to the player account on separate screen.
Robert Fuller   07/06/09    Hide some controls in the manager menu when the controller is configured for a slave.
Robert Fuller   06/09/09    Added mechanism to accomidate manager awarded promotion points.
Robert Fuller   06/09/09    Added code for custom picture buttons.
Robert Fuller   10/02/08    Allow the Manager to clear out the ticket pay pending file when a pending ticket can not be paid.
Robert Fuller   08/20/08    initialize the manager version info pointer when destroying window.
Robert Fuller   01/22/08    Send the first/last name of who is logged into XP Controller when starting GB Trax.
Dave Elquist    07/15/05    Added GB Pro support.
Dave Elquist    05/16/05    Initial Revision.
*/


// MenuManager.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include ".\menumanager.h"


// CMenuManager dialog

IMPLEMENT_DYNAMIC(CMenuManager, CDialog)
CMenuManager::CMenuManager(CWnd* pParent /*=NULL*/)
	: CDialog(CMenuManager::IDD, pParent)
{
    pManagerReports     = NULL;
    pManagerSetup       = NULL;
    pManagerVersionInfo = NULL;
    pGbProManagerDialog = NULL;
    pManagerAwardPointsDialog = NULL;
    pManagerCollection = NULL;

    current_edit_focus  = 0;
    manager_ucmc_id     = 0;

	VERIFY(keypad0.LoadBitmaps(_T("SKEYPAD0U"), _T("SKEYPAD0D")));
	VERIFY(keypad1.LoadBitmaps(_T("SKEYPAD1U"), _T("SKEYPAD1D")));
	VERIFY(keypad2.LoadBitmaps(_T("SKEYPAD2U"), _T("SKEYPAD2D")));
	VERIFY(keypad3.LoadBitmaps(_T("SKEYPAD3U"), _T("SKEYPAD3D")));
	VERIFY(keypad4.LoadBitmaps(_T("SKEYPAD4U"), _T("SKEYPAD4D")));
	VERIFY(keypad5.LoadBitmaps(_T("SKEYPAD5U"), _T("SKEYPAD5D")));
	VERIFY(keypad6.LoadBitmaps(_T("SKEYPAD6U"), _T("SKEYPAD6D")));
	VERIFY(keypad7.LoadBitmaps(_T("SKEYPAD7U"), _T("SKEYPAD7D")));
	VERIFY(keypad8.LoadBitmaps(_T("SKEYPAD8U"), _T("SKEYPAD8D")));
	VERIFY(keypad9.LoadBitmaps(_T("SKEYPAD9U"), _T("SKEYPAD9D")));
	VERIFY(keypadC.LoadBitmaps(_T("SCLEARU"), _T("SCLEARD")));

	VERIFY(reports.LoadBitmaps(_T("SREPORTSU"), _T("SREPORTSD")));
	VERIFY(setup.LoadBitmaps(_T("SSETUPU"), _T("SSETUPD")));
	VERIFY(version.LoadBitmaps(_T("SVERSIONU"), _T("SVERSIOND")));
	VERIFY(exit.LoadBitmaps(_T("SEXITU"), _T("SEXITD")));

	VERIFY(clear_pending_tickets_button.LoadBitmaps(_T("SCLEARPU"), _T("SCLEARPD")));
	VERIFY(m_unlock_game_button.LoadBitmaps(_T("SUNLOCKU"), _T("SUNLOCKD")));

	VERIFY(maintenance_button.LoadBitmaps(_T("MAINTENANCEU"), _T("MAINTENANCED")));

}

CMenuManager::~CMenuManager()
{
    if (pManagerReports)
    {
        pManagerReports->DestroyWindow();
        delete pManagerReports;
    }

    if (pManagerSetup)
    {
        pManagerSetup->DestroyWindow();
        delete pManagerSetup;
    }

    if (pManagerVersionInfo)
    {
        pManagerVersionInfo->DestroyWindow();
        delete pManagerVersionInfo;
    }

    if (pGbProManagerDialog)
    {
        pGbProManagerDialog->DestroyWindow();
        delete pGbProManagerDialog;
    }

    if (pManagerAwardPointsDialog)
    {
        pManagerAwardPointsDialog->DestroyWindow();
        delete pManagerAwardPointsDialog;
    }

    if (pManagerCollection)
    {
        pManagerCollection->DestroyWindow();
        delete pManagerCollection;
    }
}

void CMenuManager::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MANAGER_UNLOCK_UCMC_ID_EDIT, m_unlock_game_edit);
	DDX_Control(pDX, IDC_MANAGER_GB_PRO_BUTTON, m_gb_pro_button);
}


BEGIN_MESSAGE_MAP(CMenuManager, CDialog)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_EXIT_MANAGER_BUTTON, OnBnClickedExitManagerButton)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_MANAGER_UNLOCK_GAME_BUTTON, OnBnClickedManagerUnlockGameButton)
	ON_BN_CLICKED(IDC_MANAGER_REPORTS_BUTTON, OnBnClickedManagerReportsButton)
	ON_BN_CLICKED(IDC_MANAGER_SETUP_BUTTON, OnBnClickedManagerSetupButton)
	ON_BN_CLICKED(IDC_MANAGER_VERSION_INFO_BUTTON, OnBnClickedManagerVersionInfoButton)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON1, OnBnClickedManagerMenuButton1)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON2, OnBnClickedManagerMenuButton2)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON3, OnBnClickedManagerMenuButton3)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON4, OnBnClickedManagerMenuButton4)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON5, OnBnClickedManagerMenuButton5)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON6, OnBnClickedManagerMenuButton6)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON7, OnBnClickedManagerMenuButton7)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON8, OnBnClickedManagerMenuButton8)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON9, OnBnClickedManagerMenuButton9)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON0, OnBnClickedManagerMenuButton0)
	ON_BN_CLICKED(IDC_MANAGER_MENU_CLEAR_BUTTON, OnBnClickedManagerMenuClearButton)
	ON_EN_SETFOCUS(IDC_MANAGER_UNLOCK_UCMC_ID_EDIT, OnEnSetfocusManagerUnlockUcmcIdEdit)
	ON_BN_CLICKED(IDC_MANAGER_GB_PRO_BUTTON, OnBnClickedManagerGbProButton)
	ON_BN_CLICKED(IDC_MANAGER_CLEAR_PENDING_TICKETS_BUTTON, OnBnClickedManagerClearPendingTicketsButton)
    ON_BN_CLICKED(IDC_MANAGER_MAINTENANCE_BUTTON, OnBnClickedManagerMaintenanceButton)
END_MESSAGE_MAP()


void CMenuManager::OnBnClickedExitManagerButton()
{
    theApp.current_dialog_return = IDD_MAIN_MENU_DIALOG;
    DestroyWindow();
}

void CMenuManager::OnMouseMove(UINT nFlags, CPoint point)
{
    theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

void CMenuManager::OnBnClickedManagerUnlockGameButton()
{
 GameUnlockQueue game_unlock_message;

    if (manager_ucmc_id)
    {
        memset(&game_unlock_message, 0, sizeof(game_unlock_message));
        game_unlock_message.ucmc_id = manager_ucmc_id;
        game_unlock_message.manager_override = 1;
        theApp.user_interface_game_unlock_queue.push(game_unlock_message);
    }
}

void CMenuManager::OnBnClickedManagerReportsButton()
{
    if (pManagerReports)
    {
        pManagerReports->DestroyWindow();
        delete pManagerReports;
    }

    pManagerReports = new CManagerReports;
    pManagerReports->Create(IDD_MANAGER_REPORTS_DIALOG);
}

void CMenuManager::OnBnClickedManagerSetupButton()
{
    if (pManagerSetup)
    {
        pManagerSetup->DestroyWindow();
        delete pManagerSetup;
    }

    pManagerSetup = new CManagerSetup;
    pManagerSetup->Create(IDD_MANAGER_SETUP_DIALOG);
}

void CMenuManager::OnBnClickedManagerVersionInfoButton()
{
    if (pManagerVersionInfo)
    {
        pManagerVersionInfo->DestroyWindow();
        delete pManagerVersionInfo;
        pManagerVersionInfo = NULL;
    }

    pManagerVersionInfo = new CManagerVersionInfo;
    pManagerVersionInfo->Create(IDD_MANAGER_VERSION_INFO_DIALOG);
}

void CMenuManager::ProcessButtonClick(BYTE button_press)
{
 CString edit_string;

//    if (current_edit_focus == IDC_MANAGER_UNLOCK_UCMC_ID_EDIT)
    {
        manager_ucmc_id = manager_ucmc_id * 10 + button_press;
        edit_string.Format("%u", manager_ucmc_id);
        SetDlgItemText(IDC_MANAGER_UNLOCK_UCMC_ID_EDIT, edit_string);
	}
}

void CMenuManager::OnBnClickedManagerMenuButton1()
{
    ProcessButtonClick(1);
}

void CMenuManager::OnBnClickedManagerMenuButton2()
{
    ProcessButtonClick(2);
}

void CMenuManager::OnBnClickedManagerMenuButton3()
{
    ProcessButtonClick(3);
}

void CMenuManager::OnBnClickedManagerMenuButton4()
{
    ProcessButtonClick(4);
}

void CMenuManager::OnBnClickedManagerMenuButton5()
{
    ProcessButtonClick(5);
}

void CMenuManager::OnBnClickedManagerMenuButton6()
{
    ProcessButtonClick(6);
}

void CMenuManager::OnBnClickedManagerMenuButton7()
{
    ProcessButtonClick(7);
}

void CMenuManager::OnBnClickedManagerMenuButton8()
{
    ProcessButtonClick(8);
}

void CMenuManager::OnBnClickedManagerMenuButton9()
{
    ProcessButtonClick(9);
}

void CMenuManager::OnBnClickedManagerMenuButton0()
{
    ProcessButtonClick(0);
}

void CMenuManager::OnBnClickedManagerMenuClearButton()
{
//    if (current_edit_focus == IDC_MANAGER_UNLOCK_UCMC_ID_EDIT)
    {
        manager_ucmc_id = 0;
        SetDlgItemText(IDC_MANAGER_UNLOCK_UCMC_ID_EDIT, "");
    }
}

void CMenuManager::OnEnSetfocusManagerUnlockUcmcIdEdit()
{
    current_edit_focus = IDC_MANAGER_UNLOCK_UCMC_ID_EDIT;
}

void CMenuManager::OnEnSetfocusManagerTillAmountEdit()
{
    current_edit_focus = IDC_MANAGER_MEMBER_NUMBER_EDIT;
}

BOOL CMenuManager::OnInitDialog()
{
	CDialog::OnInitDialog();
    CWnd* item;

//    SetDialogBkColor(RGB(255, 0, 0), RGB(0, 255, 0));

    VERIFY(keypad0.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON0, this));
    keypad0.SizeToContent();
    VERIFY(keypad1.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON1, this));
    keypad1.SizeToContent();
    VERIFY(keypad2.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON2, this));
    keypad2.SizeToContent();
    VERIFY(keypad3.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON3, this));
    keypad3.SizeToContent();
    VERIFY(keypad4.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON4, this));
    keypad4.SizeToContent();
    VERIFY(keypad5.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON5, this));
    keypad5.SizeToContent();
    VERIFY(keypad6.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON6, this));
    keypad6.SizeToContent();
    VERIFY(keypad7.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON7, this));
    keypad7.SizeToContent();
    VERIFY(keypad8.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON8, this));
    keypad8.SizeToContent();
    VERIFY(keypad9.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON9, this));
    keypad9.SizeToContent();
    VERIFY(keypadC.SubclassDlgItem(IDC_MANAGER_MENU_CLEAR_BUTTON, this));
    keypadC.SizeToContent();

    VERIFY(reports.SubclassDlgItem(IDC_MANAGER_REPORTS_BUTTON, this));
    reports.SizeToContent();
    VERIFY(setup.SubclassDlgItem(IDC_MANAGER_SETUP_BUTTON, this));
    setup.SizeToContent();
    VERIFY(version.SubclassDlgItem(IDC_MANAGER_VERSION_INFO_BUTTON, this));
    version.SizeToContent();

    VERIFY(maintenance_button.SubclassDlgItem(IDC_MANAGER_MAINTENANCE_BUTTON, this));
    maintenance_button.SizeToContent();

    VERIFY(exit.SubclassDlgItem(IDC_EXIT_MANAGER_BUTTON, this));
    exit.SizeToContent();

    VERIFY(clear_pending_tickets_button.SubclassDlgItem(IDC_MANAGER_CLEAR_PENDING_TICKETS_BUTTON, this));
    clear_pending_tickets_button.SizeToContent();
    VERIFY(m_unlock_game_button.SubclassDlgItem(IDC_MANAGER_UNLOCK_GAME_BUTTON, this));
    m_unlock_game_button.SizeToContent();


    if (theApp.memory_settings_ini.masters_ip_address)
    {
        keypad0.DestroyWindow();
        keypad1.DestroyWindow();
        keypad2.DestroyWindow();
        keypad3.DestroyWindow();
        keypad4.DestroyWindow();
        keypad5.DestroyWindow();
        keypad6.DestroyWindow();
        keypad7.DestroyWindow();
        keypad8.DestroyWindow();
        keypad9.DestroyWindow();
        keypadC.DestroyWindow();

        m_unlock_game_groupbox.DestroyWindow();
        m_unlock_game_text.DestroyWindow();
        m_unlock_game_edit.DestroyWindow();
        m_unlock_game_button.DestroyWindow();
        maintenance_button.DestroyWindow();
        setup.DestroyWindow();
        clear_pending_tickets_button.DestroyWindow();

        item = 0;
        item = GetDlgItem(IDC_UNLOCK_GAME);
        if (item)
            item->ShowWindow(SW_HIDE);
        item = 0;
        item = GetDlgItem(IDC_UCMC_ID_TEXT);
        if (item)
            item->ShowWindow(SW_HIDE);
        item = 0;
    }
    else
    {
//	    if (!theApp.memory_settings_ini.stratus_ip_address && !theApp.memory_settings_ini.stratus_tcp_port)
    	if (!theApp.memory_settings_ini.stratus_ip_address || (theApp.current_user.user.id / 10000 != MANAGER) ||
    	    theApp.memory_settings_ini.idc_ip_address || (GetFileAttributes(GAME_TENDER_FILE) != 0xFFFFFFFF))
        {
       	    maintenance_button.ShowWindow(SW_HIDE);
        }
        else
            maintenance_button.ShowWindow(SW_SHOW);

    }

    if (theApp.stratusSocket.socket_state ||
        (theApp.current_user.user.id / 10000 != MANAGER && theApp.current_user.user.id / 10000 != ENGINEERING))
            m_gb_pro_button.DestroyWindow();

	if (GetFileAttributes(VLORA_FILE) == 0xFFFFFFFF)
		m_manager_vlora_button.DestroyWindow();

 return TRUE;
}

void CMenuManager::OnPaint() 
{
	CRect rect;
	GetClientRect(&rect);
	CPaintDC dc(this); // device context for painting

	CDialog::OnPaint();

    dc.FillSolidRect(&rect, 0x00000000);
}

void CMenuManager::OnBnClickedManagerGbProButton()
{
    if (pGbProManagerDialog)
    {
        pGbProManagerDialog->DestroyWindow();
        delete pGbProManagerDialog;
    }

    pGbProManagerDialog = new CGbProManagerDialog;
    pGbProManagerDialog->Create(IDD_MANAGER_GB_PRO_DIALOG);
}

void CMenuManager::OnBnClickedManagerClearPendingTicketsButton()
{
	EnableWindow(FALSE);
	MessageWindow(true, "CLEAR ALL PENDING TICKETS?", "ARE YOU SURE YOU WANT TO CLEAR ALL PENDING TICKETS?");

	while (theApp.message_window_queue.empty())
	    theApp.OnIdle(0);

	EnableWindow();

	if (theApp.message_window_queue.front() == IDOK)
	{
        DeleteFile(PENDING_TICKETS);
	}
}

void CMenuManager::OnBnClickedManagerMaintenanceButton()
{
    if (theApp.memory_settings_ini.activate_gb_advantage_support && theApp.pGbProManager)
    {
        if (pManagerAwardPointsDialog)
        {
            pManagerAwardPointsDialog->DestroyWindow();
            delete pManagerAwardPointsDialog;
            pManagerAwardPointsDialog = NULL;
        }

        pManagerAwardPointsDialog = new CManagerAwardPoints;
        pManagerAwardPointsDialog->Create(IDD_MANAGER_AWARD_POINTS_DIALOG);
    }
}

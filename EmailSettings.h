/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    07/15/05    Initial Revision.
*/


#pragma once
#include "afxwin.h"
#include "EmailFlags.h"


// CEmailSettings dialog

class CEmailSettings : public CDialog
{
	DECLARE_DYNAMIC(CEmailSettings)

public:
	CEmailSettings(CWnd* pParent = NULL);   // standard constructor
	virtual ~CEmailSettings();
	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_TECHNICIAN_EMAIL_ALERTS_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedEmailButton1();
	afx_msg void OnBnClickedEmailButton2();
	afx_msg void OnBnClickedEmailButton3();
	afx_msg void OnBnClickedEmailButton4();
	afx_msg void OnBnClickedEmailButton5();
	afx_msg void OnBnClickedEmailButton6();
	afx_msg void OnBnClickedEmailButton7();
	afx_msg void OnBnClickedEmailButton8();
	afx_msg void OnBnClickedEmailButton9();
	afx_msg void OnBnClickedEmailButton0();
	afx_msg void OnBnClickedEmailAddButton();
	afx_msg void OnBnClickedEmailRemoveButton();
	afx_msg void OnBnClickedEmailClearButton();
	afx_msg void OnBnClickedEmailAlertsButton();
	afx_msg void OnBnClickedEmailExitButton();
	afx_msg void OnCbnSelchangeEmailSettingsAddressCombo();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);

// private functions
private:
    void ProcessButtonClick(char *button_press);

// private variables
private:
	CComboBox m_email_list_combobox;

    CEmailFlags *pEmailFlags;
};

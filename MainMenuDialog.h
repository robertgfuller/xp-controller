/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller	12/04/13	Launch game tender in the main menu after being in the screen 30 seconds.
Robert Fuller   06/09/09    Added code for custom picture buttons.
Dave Elquist    07/15/05    Changed resource id tag.
Dave Elquist    04/15/05    Initial Revision.
*/


#pragma once
#include "xp_controller.h"
#include "afxwin.h"
#include "FixedButton.h"

// CMainMenuDlg dialog

class CMainMenuDlg : public CDialog
{
	DECLARE_DYNAMIC(CMainMenuDlg)

public:
	CMainMenuDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMainMenuDlg();

// Dialog Data
	enum { IDD = IDD_MAIN_MENU_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT nIDEvent);
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedCashierMenuButton();
	afx_msg void OnBnClickedManagerMenuButton();
	afx_msg void OnBnClickedCollectorMenuButton();
	afx_msg void OnBnClickedTechnicianMenuButton();
	afx_msg void OnBnClickedEngineerMenuButton();
	afx_msg void OnBnClickedLogoutMenuButton();

private:
	CFixedButton cashier;
	CFixedButton manager;
	CFixedButton collector;
	CFixedButton technician;
	CFixedButton engineer;
	CFixedButton exit;
};

// PracConfigDialog.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include "PracConfigDialog.h"


// CPracConfigDialog dialog

IMPLEMENT_DYNAMIC(CPracConfigDialog, CDialog)

CPracConfigDialog::CPracConfigDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CPracConfigDialog::IDD, pParent)
{
	pDisplayGameCommControl = NULL;
}

CPracConfigDialog::~CPracConfigDialog()
{
}

void CPracConfigDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PRAC_TX_LIST, m_prac_tx_listbox);
	DDX_Control(pDX, IDC_PRAC_RX_LIST, m_prac_rx_listbox);
}


BEGIN_MESSAGE_MAP(CPracConfigDialog, CDialog)
	ON_BN_CLICKED(IDC_PRAC_CONFIG_EXIT_BUTTON, &CPracConfigDialog::OnBnClickedPracConfigExitButton)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_PRAC_CONFIG_KEYBOARD_BUTTON, &CPracConfigDialog::OnBnClickedPracConfigKeyboardButton)
	ON_BN_CLICKED(IDC_IP_AND_PRAC_PORT_SAVE_BUTTON, &CPracConfigDialog::OnBnClickedIpAndPracPortSaveButton)
	ON_BN_CLICKED(IDC_MACHINE_PORT_TO_ADD_PRAC_NEXT_BUTTON, &CPracConfigDialog::OnBnClickedMachinePortToAddPracNextButton)
END_MESSAGE_MAP()


// CPracConfigDialog message handlers

void CPracConfigDialog::OnBnClickedPracConfigExitButton()
{
	pDisplayGameCommControl = NULL;
	DestroyWindow();
}

void CPracConfigDialog::OnMouseMove(UINT nFlags, CPoint point)
{
	theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

void CPracConfigDialog::OnBnClickedPracConfigKeyboardButton()
{
	ShellExecute(NULL, "open", "osk.exe", NULL, NULL, SW_SHOWMAXIMIZED);
}

void CPracConfigDialog::OnBnClickedIpAndPracPortSaveButton()
{
 BYTE *ip_address_ptr;
 WORD ip_port;
 DWORD ip_address;
 CString edit_string;
 FileMemoryPracConfig file_settings;
 CGameCommControl *pGameCommControl;

	GetDlgItemText(IDC_PRAC_IP_AND_PORT_EDIT, edit_string);

	// check pDisplayGameCommControl
	pGameCommControl = theApp.pHeadGameCommControl;
	while (pGameCommControl)
	{
		if (pDisplayGameCommControl == pGameCommControl)
			break;
		else
			pGameCommControl = pGameCommControl->pNextGameCommControl;
	}
	if (!pGameCommControl)
		pDisplayGameCommControl = NULL;

	if (pDisplayGameCommControl && edit_string.GetLength())
	{
		theApp.GetIpAddressAndPort(edit_string, &ip_address, &ip_port);
		ip_address_ptr = (BYTE*)&ip_address;
		edit_string.Format("IP Address: %u.%u.%u.%u\nIP Port: %u\nA system reboot is required for changes.",
			ip_address_ptr[0], ip_address_ptr[1], ip_address_ptr[2], ip_address_ptr[3], ip_port);

		EnableWindow(FALSE);
		MessageWindow(true, "SAVE DATA?", edit_string);
		while (theApp.message_window_queue.empty())
			theApp.OnIdle(0);
		EnableWindow();

		if (theApp.message_window_queue.front() == IDOK)
		{
			edit_string.Format("%u.%u.%u.%u:%u", ip_address_ptr[0], ip_address_ptr[1], ip_address_ptr[2], ip_address_ptr[3], ip_port);
			SetDlgItemText(IDC_PRAC_IP_AND_PORT_EDIT, edit_string);

			memset(&file_settings, 0, sizeof(file_settings));
			file_settings.memory.ip_address = ip_address;
			file_settings.memory.port_number = ip_port;

			edit_string.Format("%s%u", GB_PRO_PRAC_CONFIG_FILE, pDisplayGameCommControl->port_info.port_number);
			if (fileWrite(edit_string.GetBuffer(0), 0, &file_settings, sizeof(file_settings)))
			{
				if (!pDisplayGameCommControl->pMemoryPracConfig)
					pDisplayGameCommControl->pMemoryPracConfig = new MemoryPracConfig;

				pDisplayGameCommControl->pMemoryPracConfig->ip_address = ip_address;
				pDisplayGameCommControl->pMemoryPracConfig->port_number = ip_port;
			}
		}
	}
}

void CPracConfigDialog::OnBnClickedMachinePortToAddPracNextButton()
{
 BYTE *ip_address;
 CString edit_string;
 CGameCommControl *pGameCommControl;

	SetDlgItemText(IDC_MACHINE_PORT_TO_ADD_PRAC_EDIT, "");
	SetDlgItemText(IDC_PRAC_IP_AND_PORT_EDIT, "");

	if (!pDisplayGameCommControl)
		pDisplayGameCommControl = theApp.pHeadGameCommControl;
	else
	{
		// verify pointer in case port is removed while viewing
		pGameCommControl = theApp.pHeadGameCommControl;

		while (pGameCommControl)
		{
			if (pDisplayGameCommControl == pGameCommControl)
				break;
			else
				pGameCommControl = pGameCommControl->pNextGameCommControl;
		}

		if (!pGameCommControl)
			pDisplayGameCommControl = NULL;
	}

	if (pDisplayGameCommControl)
	{
		m_prac_tx_listbox.SetHorizontalExtent(4000);
		m_prac_rx_listbox.SetHorizontalExtent(4000);

		edit_string.Format("%u", pDisplayGameCommControl->port_info.port_number);
		SetDlgItemText(IDC_MACHINE_PORT_TO_ADD_PRAC_EDIT, edit_string);

		if (pDisplayGameCommControl->pMemoryPracConfig)
		{
			ip_address = (BYTE*)&pDisplayGameCommControl->pMemoryPracConfig->ip_address;
			edit_string.Format("%u.%u.%u.%u:%u", ip_address[0], ip_address[1], ip_address[2], ip_address[3],
				pDisplayGameCommControl->pMemoryPracConfig->port_number);
			SetDlgItemText(IDC_PRAC_IP_AND_PORT_EDIT, edit_string);
		}
	}
}

/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   09/30/08    Changed the variable type of the drivers license from DWORD to double to accomidate a 10 digit value.
Robert Fuller   06/20/08    Made sure that the social security number is less than 999999999
Robert Fuller   03/20/08    Added code to support the manager approval feature.
Dave Elquist    07/15/05    Changed user level calculation.
Dave Elquist    06/09/05    Initial Revision.
*/


// TaxInformation.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include "TaxInformation.h"
#include ".\taxinformation.h"


// CTaxInformation dialog

IMPLEMENT_DYNAMIC(CTaxInformation, CDialog)
CTaxInformation::CTaxInformation(bool *got_tax_information, DWORD *ss_number, double *license, CWnd* pParent /*=NULL*/)
	: CDialog(CTaxInformation::IDD, pParent)
{
    current_edit_focus = 0;
    social_security_number = 0;
    drivers_license_number = 0;
    manager_user_id = 0;
    manager_password = 0;
    pSocialSecurityNumber = ss_number;
    pDriversLicenseNumber = license;
    pGotTaxInformation = got_tax_information;
}

CTaxInformation::~CTaxInformation()
{
}

void CTaxInformation::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CASHIER_SSN_DL_STATIC1, m_idc_static1);
	DDX_Control(pDX, IDC_CASHIER_SSN_DL_STATIC2, m_idc_static2);
	DDX_Control(pDX, IDC_CASHIER_SSN_DL_STATIC3, m_idc_static3);
	DDX_Control(pDX, IDC_CASHIER_SSN_DL_MANAGER_USER_ID_EDIT, m_idc_edit1);
	DDX_Control(pDX, IDC_CASHIER_SSN_DL_MANAGER_PASSWORD_EDIT, m_idc_edit2);
}


BEGIN_MESSAGE_MAP(CTaxInformation, CDialog)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_CASHIER_DL_SSN_BUTTON1, OnBnClickedCashierDlSsnButton1)
	ON_BN_CLICKED(IDC_CASHIER_DL_SSN_BUTTON2, OnBnClickedCashierDlSsnButton2)
	ON_BN_CLICKED(IDC_CASHIER_DL_SSN_BUTTON3, OnBnClickedCashierDlSsnButton3)
	ON_BN_CLICKED(IDC_CASHIER_DL_SSN_BUTTON4, OnBnClickedCashierDlSsnButton4)
	ON_BN_CLICKED(IDC_CASHIER_DL_SSN_BUTTON5, OnBnClickedCashierDlSsnButton5)
	ON_BN_CLICKED(IDC_CASHIER_DL_SSN_BUTTON6, OnBnClickedCashierDlSsnButton6)
	ON_BN_CLICKED(IDC_CASHIER_DL_SSN_BUTTON7, OnBnClickedCashierDlSsnButton7)
	ON_BN_CLICKED(IDC_CASHIER_DL_SSN_BUTTON8, OnBnClickedCashierDlSsnButton8)
	ON_BN_CLICKED(IDC_CASHIER_DL_SSN_BUTTON9, OnBnClickedCashierDlSsnButton9)
	ON_BN_CLICKED(IDC_CASHIER_DL_SSN_BUTTON0, OnBnClickedCashierDlSsnButton0)
	ON_BN_CLICKED(IDC_CASHIER_DL_SSN_ENTER_BUTTON, OnBnClickedCashierDlSsnEnterButton)
	ON_BN_CLICKED(IDC_CASHIER_DL_SSN_CLEAR_BUTTON, OnBnClickedCashierDlSsnClearButton)
	ON_EN_SETFOCUS(IDC_CASHIER_SSN_EDIT, OnEnSetfocusCashierSsnEdit)
	ON_EN_SETFOCUS(IDC_CASHIER_DL_EDIT, OnEnSetfocusCashierDlEdit)
    ON_EN_SETFOCUS(IDC_CASHIER_SSN_DL_MANAGER_USER_ID_EDIT, OnEnSetfocusManagerUsernameEdit)
    ON_EN_SETFOCUS(IDC_CASHIER_SSN_DL_MANAGER_PASSWORD_EDIT, OnEnSetfocusManagerPasswordEdit)

END_MESSAGE_MAP()


void CTaxInformation::OnMouseMove(UINT nFlags, CPoint point)
{
	theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

void CTaxInformation::ProcessButtonClick(BYTE button_press)
{
 CString edit_string;
    switch (current_edit_focus)
    {
        case IDC_CASHIER_SSN_EDIT:
            if ((social_security_number * 10 + button_press) <= 999999999)
            {
                social_security_number = social_security_number * 10 + button_press;
                edit_string.Format("%u", social_security_number);
                SetDlgItemText(IDC_CASHIER_SSN_EDIT, edit_string);
            }
            break;
        case IDC_CASHIER_DL_EDIT:
            if ((drivers_license_number * 10 + button_press) <= 9999999999)
            {
                drivers_license_number = drivers_license_number * 10 + button_press;
                edit_string.Format("%10.0f", drivers_license_number);
                SetDlgItemText(IDC_CASHIER_DL_EDIT, edit_string);
            }
            break;
        case IDC_CASHIER_SSN_DL_MANAGER_USER_ID_EDIT:
            manager_user_id = manager_user_id * 10 + button_press;
            edit_string.Format("%u", manager_user_id);
            SetDlgItemText(IDC_CASHIER_SSN_DL_MANAGER_USER_ID_EDIT, edit_string);
            break;
        case IDC_CASHIER_SSN_DL_MANAGER_PASSWORD_EDIT:
            manager_password = manager_password * 10 + button_press;
            edit_string.Format("%u", manager_password);
            SetDlgItemText(IDC_CASHIER_SSN_DL_MANAGER_PASSWORD_EDIT, edit_string);
            break;
    }

}

void CTaxInformation::OnBnClickedCashierDlSsnButton1()
{
    ProcessButtonClick(1);
}

void CTaxInformation::OnBnClickedCashierDlSsnButton2()
{
    ProcessButtonClick(2);
}

void CTaxInformation::OnBnClickedCashierDlSsnButton3()
{
    ProcessButtonClick(3);
}

void CTaxInformation::OnBnClickedCashierDlSsnButton4()
{
    ProcessButtonClick(4);
}

void CTaxInformation::OnBnClickedCashierDlSsnButton5()
{
    ProcessButtonClick(5);
}

void CTaxInformation::OnBnClickedCashierDlSsnButton6()
{
    ProcessButtonClick(6);
}

void CTaxInformation::OnBnClickedCashierDlSsnButton7()
{
    ProcessButtonClick(7);
}

void CTaxInformation::OnBnClickedCashierDlSsnButton8()
{
    ProcessButtonClick(8);
}

void CTaxInformation::OnBnClickedCashierDlSsnButton9()
{
    ProcessButtonClick(9);
}

void CTaxInformation::OnBnClickedCashierDlSsnButton0()
{
    ProcessButtonClick(0);
}

void CTaxInformation::OnBnClickedCashierDlSsnEnterButton()
{
 CString edit_string, prt_buf;
 ControllerUser manager_user;

    *pSocialSecurityNumber = 0;
    *pDriversLicenseNumber = 0;

    if (theApp.memory_dispenser_config.autopay_manager_approval)
    {
        memset(&manager_user, 0, sizeof(manager_user));
        GetDlgItemText(IDC_CASHIER_SSN_DL_MANAGER_USER_ID_EDIT, edit_string);
        manager_user.user.id = atoi(edit_string);
        GetDlgItemText(IDC_CASHIER_SSN_DL_MANAGER_PASSWORD_EDIT, edit_string);
        manager_user.user.password = atoi(edit_string);

        if (!manager_user.user.id || !manager_user.user.password ||
            manager_user.user.id / 10000 != MANAGER)
        {
            prt_buf.Format("INVALID MANAGER ID: %u\nTRY AGAIN", manager_user.user.id);
            MessageWindow(false, "TAX INFORMATION", prt_buf);
            Sleep(1000);
            social_security_number = 0;
            drivers_license_number = 0;
        }
        else
        {
            EnableWindow(false);

            if ((!theApp.memory_settings_ini.masters_ip_address && theApp.CheckLoginPassword(&manager_user)) ||
                (theApp.memory_settings_ini.masters_ip_address && theApp.CheckSlaveLoginPassword(&manager_user)))
            {
                prt_buf.Format(" AUTOPAY MANAGER VERIFY: %u\n", manager_user.user.id);
                theApp.PrinterData(prt_buf.GetBuffer(0));
            }
            else
            {
                MessageWindow(false, "TAX INFORMATION", "INVALID MANAGER ID\nTRY AGAIN");
                Sleep(1000);
                social_security_number = 0;
                drivers_license_number = 0;
            }

            EnableWindow();
        }
    }

    *pSocialSecurityNumber = social_security_number;
    *pDriversLicenseNumber = drivers_license_number;
    *pGotTaxInformation = true;
    DestroyWindow();
}

void CTaxInformation::OnBnClickedCashierDlSsnClearButton()
{
    if (current_edit_focus == IDC_CASHIER_SSN_EDIT)
    {
        social_security_number = 0;
        SetDlgItemText(IDC_CASHIER_SSN_EDIT, "");
    }
    else if (current_edit_focus == IDC_CASHIER_DL_EDIT)
    {
        drivers_license_number = 0;
        SetDlgItemText(IDC_CASHIER_DL_EDIT, "");
    }

    switch (current_edit_focus)
    {
        case IDC_CASHIER_SSN_EDIT:
            social_security_number = 0;
            SetDlgItemText(IDC_CASHIER_SSN_EDIT, "");
            break;
        case IDC_CASHIER_DL_EDIT:
            drivers_license_number = 0;
            SetDlgItemText(IDC_CASHIER_DL_EDIT, "");
            break;
        case IDC_CASHIER_SSN_DL_MANAGER_USER_ID_EDIT:
            manager_user_id = 0;
            SetDlgItemText(IDC_CASHIER_SSN_DL_MANAGER_USER_ID_EDIT, "");
            break;
        case IDC_CASHIER_SSN_DL_MANAGER_PASSWORD_EDIT:
            manager_password = 0;
            SetDlgItemText(IDC_CASHIER_SSN_DL_MANAGER_PASSWORD_EDIT, "");
            break;
    }

}

void CTaxInformation::OnEnSetfocusCashierSsnEdit()
{
    current_edit_focus = IDC_CASHIER_SSN_EDIT;
}

void CTaxInformation::OnEnSetfocusCashierDlEdit()
{
    current_edit_focus = IDC_CASHIER_DL_EDIT;
}

void CTaxInformation::OnEnSetfocusManagerUsernameEdit()
{
    current_edit_focus = IDC_CASHIER_SSN_DL_MANAGER_USER_ID_EDIT;
}

void CTaxInformation::OnEnSetfocusManagerPasswordEdit()
{
    current_edit_focus = IDC_CASHIER_SSN_DL_MANAGER_PASSWORD_EDIT;
}

BOOL CTaxInformation::OnInitDialog()
{
	CDialog::OnInitDialog();

    if (!theApp.memory_dispenser_config.autopay_manager_approval)
    {
        m_idc_static1.DestroyWindow();
        m_idc_static2.DestroyWindow();
        m_idc_static3.DestroyWindow();
        m_idc_edit1.DestroyWindow();
        m_idc_edit2.DestroyWindow();
    }

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   07/15/09    Increased the number of shifts to print on the shift report from 4 to 21.
Dave Elquist    07/15/05    Moved function.
Dave Elquist    06/09/05    Initial Revision.
*/


#pragma once
#include "afxwin.h"


// CManagerSetup dialog

class CManagerSetup : public CDialog
{
	DECLARE_DYNAMIC(CManagerSetup)

public:
	CManagerSetup(CWnd* pParent = NULL);   // standard constructor
	virtual ~CManagerSetup();
	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_MANAGER_SETUP_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	afx_msg void OnPaint();

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedManagerSoundForGamelocksTestButton();
	afx_msg void OnBnClickedManagerSetupExitButton();
	afx_msg void OnBnClickedManagerPlayerTierCheck();
	afx_msg void OnBnClickedManagerPrintDrinkCompsCheck();
	afx_msg void OnBnClickedManagerForceCashierShiftsCheck();
	afx_msg void OnBnClickedManagerSetupCashierShiftPrintingCheck();
	afx_msg void OnBnClickedManagerPrintFullShiftReportCheck();

private:
	CButton m_force_cashier_shifts_check;
	CButton m_print_drink_comps_check;
	CButton m_print_player_tier_level_check;
	CButton m_sound_for_gamelock_check;
	CButton m_sound_for_gamelock_test_button;
	CButton m_allow_cashier_to_print_shift_check;
	CButton m_print_full_shift_report;
};

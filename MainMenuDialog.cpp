/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller	12/04/13	Launch game tender in the main menu after being in the screen 30 seconds.
Robert Fuller   06/09/09    Added code for custom picture buttons.
Dave Elquist    07/15/05    Changed user level calculation. Changed resource id tag.
Dave Elquist    04/15/05    Initial Revision.
*/

// MainMenuDialog.cpp : implementation file
//


#include "stdafx.h"
#include ".\mainmenudialog.h"

#define TIMEOUT_TO_GAME_TENDER_MSECS 30 * 1000

// CMainMenuDialog dialog

IMPLEMENT_DYNAMIC(CMainMenuDlg, CDialog)
CMainMenuDlg::CMainMenuDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMainMenuDlg::IDD, pParent)
{
	VERIFY(cashier.LoadBitmaps(_T("CASHIERU"), _T("CASHIERD")));
	VERIFY(manager.LoadBitmaps(_T("MANAGERU"), _T("MANAGERD")));
	VERIFY(collector.LoadBitmaps(_T("COLLECTORU"), _T("COLLECTORD")));
	VERIFY(technician.LoadBitmaps(_T("TECHNICIANU"), _T("TECHNICIAND")));
	VERIFY(engineer.LoadBitmaps(_T("ENGINEERU"), _T("ENGINEERD")));
	VERIFY(exit.LoadBitmaps(_T("EXITU"), _T("EXITD")));
}

CMainMenuDlg::~CMainMenuDlg()
{
}

void CMainMenuDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BOOL CMainMenuDlg::OnInitDialog()
{
 CString window_title;

    CDialog::OnInitDialog();

    VERIFY(cashier.SubclassDlgItem(IDC_CASHIER_BUTTON, this));
    cashier.SizeToContent();
    VERIFY(manager.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON, this));
    manager.SizeToContent();
    VERIFY(collector.SubclassDlgItem(IDC_COLLECTOR_MENU_BUTTON, this));
    collector.SizeToContent();
    VERIFY(technician.SubclassDlgItem(IDC_TECHNICIAN_MENU_BUTTON, this));
    technician.SizeToContent();
    VERIFY(engineer.SubclassDlgItem(IDC_ENGINEER_MENU_BUTTON, this));
    engineer.SizeToContent();
    VERIFY(exit.SubclassDlgItem(IDC_LOGOUT_MENU_BUTTON, this));
    exit.SizeToContent();

    switch (theApp.current_user.user.id / 10000)
    {
        case CASHIER:
            manager.DestroyWindow();
            collector.DestroyWindow();
            technician.DestroyWindow();
            engineer.DestroyWindow();
            window_title.Format("CASHIER     %s %s", theApp.current_user.user.first_name, theApp.current_user.user.last_name);
            break;

        case MANAGER:
            collector.DestroyWindow();
            technician.DestroyWindow();
            engineer.DestroyWindow();
            window_title.Format("MANAGER     %s %s", theApp.current_user.user.first_name, theApp.current_user.user.last_name);
            break;

        case COLLECTOR:
            technician.DestroyWindow();
            engineer.DestroyWindow();
            window_title.Format("COLLECTOR     %s %s", theApp.current_user.user.first_name, theApp.current_user.user.last_name);
            break;

        case TECHNICIAN:
            engineer.DestroyWindow();
            window_title.Format("TECHNICIAN     %s %s", theApp.current_user.user.first_name, theApp.current_user.user.last_name);
            break;

        case SUPERVISOR:
            engineer.DestroyWindow();
            window_title.Format("SUPERVISOR     %s %s", theApp.current_user.user.first_name, theApp.current_user.user.last_name);
            break;

        case ENGINEERING:
            window_title.Format("ENGINEER     %s %s", theApp.current_user.user.first_name, theApp.current_user.user.last_name);
            break;

        default:
            window_title.Format("UNKNOWN     %s %s", theApp.current_user.user.first_name, theApp.current_user.user.last_name);
            break;
    }

   	if (theApp.memory_settings_ini.idc_ip_address && theApp.memory_settings_ini.idc_tcp_port && !(GetFileAttributes(GAME_TENDER_FILE) == 0xFFFFFFFF))
	    SetTimer(GAME_TENDER_TIMER, TIMEOUT_TO_GAME_TENDER_MSECS, 0);

    SetDlgItemText(IDC_MAIN_MENU_TITLE_STATIC, window_title);

 return true;
}

void CMainMenuDlg::OnPaint() 
{
	CRect rect;
	GetClientRect(&rect);
	CPaintDC dc(this); // device context for painting

	CDialog::OnPaint();

    dc.FillSolidRect(&rect, 0x00000000);
}

BEGIN_MESSAGE_MAP(CMainMenuDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_CASHIER_BUTTON, OnBnClickedCashierMenuButton)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON, OnBnClickedManagerMenuButton)
	ON_BN_CLICKED(IDC_COLLECTOR_MENU_BUTTON, OnBnClickedCollectorMenuButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_MENU_BUTTON, OnBnClickedTechnicianMenuButton)
	ON_BN_CLICKED(IDC_ENGINEER_MENU_BUTTON, OnBnClickedEngineerMenuButton)
	ON_BN_CLICKED(IDC_LOGOUT_MENU_BUTTON, OnBnClickedLogoutMenuButton)
END_MESSAGE_MAP()


void CMainMenuDlg::OnMouseMove(UINT nFlags, CPoint point)
{
    theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

void CMainMenuDlg::OnBnClickedCashierMenuButton()
{
    theApp.current_dialog_return = IDD_CASHIER_MENU_DIALOG;
    DestroyWindow();
}

void CMainMenuDlg::OnBnClickedManagerMenuButton()
{
    theApp.current_dialog_return = IDD_MANAGER_MENU_DIALOG;
    DestroyWindow();
}

void CMainMenuDlg::OnBnClickedCollectorMenuButton()
{
    theApp.current_dialog_return = IDD_COLLECTOR_MENU_DIALOG;
    DestroyWindow();
}

void CMainMenuDlg::OnBnClickedTechnicianMenuButton()
{
    theApp.current_dialog_return = IDD_TECHNICIAN_MENU_DIALOG;
    DestroyWindow();
}

void CMainMenuDlg::OnBnClickedEngineerMenuButton()
{
    theApp.current_dialog_return = IDD_ENGINEER_MENU_DIALOG;
    DestroyWindow();
}

void CMainMenuDlg::OnBnClickedLogoutMenuButton()
{
    theApp.current_dialog_return = IDD_LOGON_DIALOG;
    DestroyWindow();
}

void CMainMenuDlg::OnTimer(UINT nIDEvent)
{
    switch (nIDEvent)
    {
		case GAME_TENDER_TIMER:
			KillTimer(GAME_TENDER_TIMER);
        	if (theApp.memory_settings_ini.idc_ip_address && theApp.memory_settings_ini.idc_tcp_port && !(GetFileAttributes(GAME_TENDER_FILE) == 0xFFFFFFFF))
                if (!theApp.IdcSocket.socket_state || (theApp.IdcSocket.socket_state == WSAEISCONN))
        			ShellExecute(NULL, "open", GAME_TENDER_FILE, NULL, NULL, SW_SHOWMAXIMIZED);
			break;
    }

	CDialog::OnTimer(nIDEvent);
}

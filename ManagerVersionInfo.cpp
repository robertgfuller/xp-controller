/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller	05/02/12	Added support for the 4 cassette Puloon dispenser
Robert Fuller   05/02/12    Added code for future support for the GBA interface to Stations Casino Management System.
Robert Fuller   03/08/12    Added code to print configurable IDC ip addresses
Robert Fuller   03/08/12    Added code to print payout mechanism (tito, or dispenser).
Robert Fuller   06/02/10    Added code to support the new Puloon LCDM-2000 dual denomination dispenser. 
Robert Fuller   01/19/10    Added code to support the new Puloon dispenser.
Robert Fuller   09/22/08    Added code to display the ip address and port settings for the One Arm Bandit Host connection.
Robert Fuller   08/19/08    Added code to support the GB Advantage feature.
Robert Fuller   05/02/08    Added code to support CD2000 dispenser.
Dave Elquist    07/15/05    Changed dispenser(lat) message from the Stratus.
Dave Elquist    06/09/05    Initial Revision.
*/


// ManagerVersionInfo.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include "ManagerVersionInfo.h"
#include ".\managerversioninfo.h"


// CManagerVersionInfo dialog

IMPLEMENT_DYNAMIC(CManagerVersionInfo, CDialog)
CManagerVersionInfo::CManagerVersionInfo(CWnd* pParent /*=NULL*/)
	: CDialog(CManagerVersionInfo::IDD, pParent)
{
}

CManagerVersionInfo::~CManagerVersionInfo()
{
}

void CManagerVersionInfo::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_VERSION_INFORMATION_LIST, m_version_information_listbox);
}


BEGIN_MESSAGE_MAP(CManagerVersionInfo, CDialog)
	ON_WM_PAINT()
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_MANAGER_VERSION_INFO_BUTTON, OnBnClickedManagerVersionInfoButton)
END_MESSAGE_MAP()


// CManagerVersionInfo message handlers

void CManagerVersionInfo::OnMouseMove(UINT nFlags, CPoint point)
{
    theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

void CManagerVersionInfo::OnBnClickedManagerVersionInfoButton()
{
	DestroyWindow();
}

BOOL CManagerVersionInfo::OnInitDialog()
{
 char hostname[MAX_COMPUTERNAME_LENGTH + 1];
 DWORD hostname_length = MAX_COMPUTERNAME_LENGTH;
 BYTE *pIpAddress;
 UINT client_port;
 CString client_address, edit_string;

	CDialog::OnInitDialog();

    edit_string.Format("XP CONTROLLER v%d.%s", VERSION, SUBVERSION);
	m_version_information_listbox.AddString(edit_string);

    if (theApp.stratusSocket.socket_state)
        m_version_information_listbox.AddString("CONNECTION INACTIVE.");
    else
        m_version_information_listbox.AddString("CONNECTION ACTIVE.");

    switch (theApp.memory_dispenser_config.dispenser_type)
    {
        case DISPENSER_TYPE_NOT_SET:
            m_version_information_listbox.AddString("DISPENSER TYPE NOT SET.");
            break;

        case DISPENSER_TYPE_NO_DISPENSER:
            m_version_information_listbox.AddString("NO DISPENSER.");
            break;

        case DISPENSER_TYPE_CASH_DRAWER:
            m_version_information_listbox.AddString("CASH DRAWER.");
            break;

        case DISPENSER_TYPE_JCM_20:
            m_version_information_listbox.AddString("$20 JCM DISPENSER.");
            break;

        case DISPENSER_TYPE_JCM_100:
            m_version_information_listbox.AddString("$100 JCM DISPENSER.");
            break;

        case DISPENSER_TYPE_ECASH:
            m_version_information_listbox.AddString("ECASH DISPENSER.");
            break;

        case DISPENSER_TYPE_CD2000:
            m_version_information_listbox.AddString("CD2000 DISPENSER.");
            break;

        case DISPENSER_TYPE_DIEBOLD:
            m_version_information_listbox.AddString("DIEBOLD DISPENSER.");
            break;

        case DISPENSER_TYPE_LCDM1000_20:
            m_version_information_listbox.AddString("UCMC1000DS DISPENSER.");
            break;

        case DISPENSER_TYPE_LCDM2000_20_100:
            m_version_information_listbox.AddString("UCMC2000DS DISPENSER.");
            break;

        case DISPENSER_TYPE_LCDM4000_20_100:
            m_version_information_listbox.AddString("UCMC4000DS DISPENSER.");
            break;

        default:
            m_version_information_listbox.AddString("DISPENSER TYPE UNKNOWN, ERROR.");
            break;
    }

    if (theApp.memory_dispenser_config.autopay_allowed)
        m_version_information_listbox.AddString("AUTOPAY IS TURNED ON.");
    else
        m_version_information_listbox.AddString("AUTOPAY IS TURNED OFF.");

    if (theApp.memory_dispenser_config.autopay_manager_approval)
        m_version_information_listbox.AddString("MANAGER APPROVAL IS REQUIRED FOR EACH AUTOPAY.");
    else
        m_version_information_listbox.AddString("MANAGER APPROVAL NOT REQUIRED FOR AUTOPAY.");


    if (theApp.memory_dispenser_config.tito)
        m_version_information_listbox.AddString("GAME LOCKS PAID THROUGH TITO");
    else
        m_version_information_listbox.AddString("GAME LOCKS PAID THROUGH DISPENSER");

    if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_ECASH ||
        theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_DIEBOLD ||
        theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_CD2000)
    {
        if (theApp.memory_dispenser_config.dispense_100s_when_20s_low)
        {
            m_version_information_listbox.AddString("THE DISPENSER WILL DISPENSE $100 BILLS");
            m_version_information_listbox.AddString("WHEN THE $20 BILLS ARE LOW.");
        }
        else
        {
            m_version_information_listbox.AddString("THE DISPENSER WILL NOT DISPENSE $100 BILLS");
            m_version_information_listbox.AddString("WHEN THE $20 BILLS ARE LOW.");
        }
    }

    if (theApp.memory_settings_ini.print_drink_comps)
        m_version_information_listbox.AddString("DRINK COMPS WILL BE PRINTED.");
    else
        m_version_information_listbox.AddString("DRINK COMPS WILL NOT BE PRINTED.");

    if (theApp.memory_settings_ini.force_cashier_shifts)
        m_version_information_listbox.AddString("CASHIERS WILL BE FORCED TO USE SHIFTS.");
    else
        m_version_information_listbox.AddString("CASHIERS WILL NOT USE SHIFTS.");

    if (theApp.memory_settings_ini.print_account_tier_level)
        m_version_information_listbox.AddString("PLAYER ACCOUNT TIER INFORMATION WILL BE PRINTED.");
    else
        m_version_information_listbox.AddString("PLAYER ACCOUNT TIER INFORMATION WILL NOT BE PRINTED.");

    edit_string.Format("DISPENSER COMM PORT: %u", theApp.memory_dispenser_config.comm_port);
	m_version_information_listbox.AddString(edit_string);
    edit_string.Format("PRINTER COMM PORT: %u", theApp.memory_settings_ini.printer_comm_port);
	m_version_information_listbox.AddString(edit_string);

    pIpAddress = (BYTE*)&theApp.memory_settings_ini.marker_printer_ip_address;
    edit_string.Format("MARKER IP ADDRESS:PORT: %u.%u.%u.%u:%u", pIpAddress[0], pIpAddress[1],
        pIpAddress[2], pIpAddress[3], theApp.memory_settings_ini.marker_printer_port);
	m_version_information_listbox.AddString(edit_string);

    switch (theApp.memory_dispenser_config.dispenser_type)
    {
        case DISPENSER_TYPE_ECASH:
        case DISPENSER_TYPE_CD2000:
        case DISPENSER_TYPE_DIEBOLD:
            edit_string.Format("$100 IMPRESS: %9.2f", (double)theApp.dispenserControl.memory_dispenser_values.impress_amount1 / 100);
        	m_version_information_listbox.AddString(edit_string);
            edit_string.Format("$20  IMPRESS: %9.2f", (double)theApp.dispenserControl.memory_dispenser_values.impress_amount2 / 100);
        	m_version_information_listbox.AddString(edit_string);
            edit_string.Format("$100 AMOUNT LEFT: %9.2f",
            	(((double)theApp.dispenserControl.memory_dispenser_values.impress_amount1 -
                (double)theApp.dispenserControl.memory_dispenser_values.dispense_total1) -
                (double)(theApp.dispenserControl.memory_dispenser_values.reject_bills1 * 100)) / 100);
        	m_version_information_listbox.AddString(edit_string);

            edit_string.Format("$20  AMOUNT LEFT: %9.2f", (((double)theApp.dispenserControl.memory_dispenser_values.impress_amount2 -
                (double)theApp.dispenserControl.memory_dispenser_values.dispense_total2) -
                (double)(theApp.dispenserControl.memory_dispenser_values.reject_bills2 * 20)) / 100);
        	m_version_information_listbox.AddString(edit_string);
            break;

        case DISPENSER_TYPE_CASH_DRAWER:
            edit_string.Format("IMPRESS: $%9.2f", (double)theApp.dispenserControl.memory_dispenser_values.impress_amount1 / 100);
        	m_version_information_listbox.AddString(edit_string);
            edit_string.Format("AMOUNT LEFT: $%9.2f", ((double)theApp.dispenserControl.memory_dispenser_values.impress_amount1 -
                (double)theApp.dispenserControl.memory_dispenser_values.tickets_amount) / 100);
        	m_version_information_listbox.AddString(edit_string);
            break;

        case DISPENSER_TYPE_JCM_20:
            edit_string.Format("$20 IMPRESS: $%9.2f", (double)theApp.dispenserControl.memory_dispenser_values.impress_amount1 / 100);
        	m_version_information_listbox.AddString(edit_string);
            edit_string.Format("AMOUNT LEFT: $%9.2f", ((double)theApp.dispenserControl.memory_dispenser_values.impress_amount1 -
                (double)theApp.dispenserControl.memory_dispenser_values.dispense_grand_total -
                (double)(theApp.dispenserControl.memory_dispenser_values.reject_bills1 * 20)) / 100);
        	m_version_information_listbox.AddString(edit_string);
            break;

        case DISPENSER_TYPE_JCM_100:
            edit_string.Format("$100 IMPRESS: $%9.2f", (double)theApp.dispenserControl.memory_dispenser_values.impress_amount1 / 100);
        	m_version_information_listbox.AddString(edit_string);
            edit_string.Format("AMOUNT LEFT: $%9.2f", ((double)theApp.dispenserControl.memory_dispenser_values.impress_amount1 -
                (double)theApp.dispenserControl.memory_dispenser_values.dispense_grand_total -
                (double)(theApp.dispenserControl.memory_dispenser_values.reject_bills1 * 100)) / 100);
        	m_version_information_listbox.AddString(edit_string);
            break;

    }

    m_version_information_listbox.AddString("AUTO LOGOUT TIMEOUTS IN SECONDS:");
    edit_string.Format("    CASHIER: %u", theApp.memory_settings_ini.auto_logout_timeouts[CASHIER]);
   	m_version_information_listbox.AddString(edit_string);
    edit_string.Format("    MANAGER: %u", theApp.memory_settings_ini.auto_logout_timeouts[MANAGER]);
   	m_version_information_listbox.AddString(edit_string);
    edit_string.Format("    COLLECTOR: %u", theApp.memory_settings_ini.auto_logout_timeouts[COLLECTOR]);
   	m_version_information_listbox.AddString(edit_string);
    edit_string.Format("    TECHNICIAN: %u", theApp.memory_settings_ini.auto_logout_timeouts[TECHNICIAN]);
   	m_version_information_listbox.AddString(edit_string);
    edit_string.Format("    SUPERVISOR: %u", theApp.memory_settings_ini.auto_logout_timeouts[SUPERVISOR]);
   	m_version_information_listbox.AddString(edit_string);

    if (theApp.stratusSocket.GetSockName(client_address, client_port))
    {
        edit_string.Format("LOCAL IP ADDRESS: %s", client_address);
        m_version_information_listbox.AddString(edit_string);
    }

    if (theApp.memory_settings_ini.masters_ip_address)
        pIpAddress = (BYTE*)&theApp.memory_settings_ini.masters_ip_address;
    else
        pIpAddress = (BYTE*)&theApp.memory_settings_ini.stratus_ip_address;

    edit_string.Format("STRATUS IP ADDRESS:PORT: %u.%u.%u.%u:%u", pIpAddress[0], pIpAddress[1],
        pIpAddress[2], pIpAddress[3], theApp.memory_settings_ini.stratus_tcp_port);
    m_version_information_listbox.AddString(edit_string);

    m_version_information_listbox.AddString("CUSTOMER CONTROL SERVER IP ADDRESS:PORT");
    pIpAddress = (BYTE*)&theApp.memory_settings_ini.customer_control_server_ip_address;
    edit_string.Format("     %u.%u.%u.%u:%u", pIpAddress[0], pIpAddress[1],
        pIpAddress[2], pIpAddress[3], theApp.memory_settings_ini.customer_control_server_tcp_port);
    m_version_information_listbox.AddString(edit_string);

    edit_string.Format("CUSTOMER CONTROL CLIENT PORT: %u", theApp.memory_settings_ini.customer_control_client_port);
    m_version_information_listbox.AddString(edit_string);

	if (theApp.pGbProManager)
	{
		m_version_information_listbox.AddString("GB ADVANTAGE CONFIGURATOR IP ADDRESS:PORT");
		pIpAddress = (BYTE*)&theApp.pGbProManager->memory_gb_pro_settings.gb_pro_configurator_ip_address;
		edit_string.Format("     %u.%u.%u.%u:%u", pIpAddress[0], pIpAddress[1],
			pIpAddress[2], pIpAddress[3], theApp.pGbProManager->memory_gb_pro_settings.gb_pro_configurator_tcp_port);
		m_version_information_listbox.AddString(edit_string);
	}

    pIpAddress = (BYTE*)&theApp.memory_settings_ini.cms_ip_address;

    edit_string.Format("CMS IP ADDRESS:PORT: %u.%u.%u.%u:%u", pIpAddress[0], pIpAddress[1],
        pIpAddress[2], pIpAddress[3], theApp.memory_settings_ini.cms_tcp_port);
    m_version_information_listbox.AddString(edit_string);

    pIpAddress = (BYTE*)&theApp.memory_settings_ini.idc_ip_address;

    edit_string.Format("IDC IP ADDRESS:PORT: %u.%u.%u.%u:%u", pIpAddress[0], pIpAddress[1],
        pIpAddress[2], pIpAddress[3], theApp.memory_settings_ini.idc_tcp_port);
    m_version_information_listbox.AddString(edit_string);

    if (theApp.memory_settings_ini.game_lock_printer_ip_address)
    {
        pIpAddress = (BYTE*)&theApp.memory_settings_ini.game_lock_printer_ip_address;
        edit_string.Format("GAME LOCK PRINTER IP ADDRESS: %u.%u.%u.%u", pIpAddress[0], pIpAddress[1],
        pIpAddress[2], pIpAddress[3]);
        m_version_information_listbox.AddString(edit_string);
    }

    if (theApp.memory_settings_ini.activate_gb_advantage_support)
        edit_string = "GB ADVANTAGE IS ACTIVATED";
    else
        edit_string = "GB ADVANTAGE IS TURNED OFF";

	m_version_information_listbox.AddString(edit_string);

    if (theApp.memory_settings_ini.bypass_game_tender_automated_w2g)
        edit_string = "GAME TENDER AUTOMATED W2Gs ARE NOT MANDATORY";
    else
        edit_string = "GAME TENDER AUTOMATED W2Gs ARE MANDATORY";

	m_version_information_listbox.AddString(edit_string);

    edit_string.Format("SYSTEM UCMC ID: %u", theApp.memory_dispenser_config.ucmc_id);
   	m_version_information_listbox.AddString(edit_string);

    if (GetComputerName(hostname, &hostname_length))
        edit_string.Format("HOSTNAME: %s", hostname);
    else
        edit_string = "HOSTNAME: UNKNOWN";

    m_version_information_listbox.AddString(edit_string);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CManagerVersionInfo::OnPaint() 
{
	CRect rect;
	GetClientRect(&rect);
	CPaintDC dc(this); // device context for painting

	CDialog::OnPaint();

    dc.FillSolidRect(&rect, 0x00000000);
}

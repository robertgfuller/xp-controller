#pragma once
#include "afxwin.h"


// CDispenserDialog dialog

class CDispenserDialog : public CDialog
{
	DECLARE_DYNAMIC(CDispenserDialog)

public:
	CDispenserDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDispenserDialog();

// Dialog Data
	enum { IDD = IDD_DISPENSER_INFO_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

// public functions
public:
	afx_msg void OnBnClickedDispenserDialogExitButton();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedDispenserResetButton();
	afx_msg void OnBnClickedDispenserStatusButton();

// public variables
public:
    bool destroy_dialog;

	CButton m_display_dispenser_raw_data;
	CButton m_display_dispenser_verbose_data;
	CListBox m_dispenser_display_listbox;
	virtual BOOL OnInitDialog();
};

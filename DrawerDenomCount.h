/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    07/15/05    Moved function.
Dave Elquist    06/09/05    Initial Revision.
*/


#pragma once


// CDrawerDenomCount dialog

class CDrawerDenomCount : public CDialog
{
	DECLARE_DYNAMIC(CDrawerDenomCount)

public:
	CDrawerDenomCount(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDrawerDenomCount();

// Dialog Data
	enum { IDD = IDD_CASHIER_DRAWER_DENOM_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedDrawerDenominationButton1();
	afx_msg void OnBnClickedDrawerDenominationButton2();
	afx_msg void OnBnClickedDrawerDenominationButton3();
	afx_msg void OnBnClickedDrawerDenominationButton4();
	afx_msg void OnBnClickedDrawerDenominationButton5();
	afx_msg void OnBnClickedDrawerDenominationButton6();
	afx_msg void OnBnClickedDrawerDenominationButton7();
	afx_msg void OnBnClickedDrawerDenominationButton8();
	afx_msg void OnBnClickedDrawerDenominationButton9();
	afx_msg void OnBnClickedDrawerDenominationButton0();
	afx_msg void OnBnClickedDrawerDenominationEnterButton();
	afx_msg void OnBnClickedDrawerDenominationCancelButton();
	afx_msg void OnBnClickedDrawerDenominationClearButton();
	afx_msg void OnEnSetfocusDrawerDenominationPennyEdit();
	afx_msg void OnEnSetfocusDrawerDenominationNickelEdit();
	afx_msg void OnEnSetfocusDrawerDenominationDimeEdit();
	afx_msg void OnEnSetfocusDrawerDenominationQuarterEdit();
	afx_msg void OnEnSetfocusDrawerDenominationHalfEdit();
	afx_msg void OnEnSetfocusDrawerDenominationDollarEdit();
	afx_msg void OnEnSetfocusDrawerDenominationHardOtherEdit();
	afx_msg void OnEnSetfocusDrawerDenominationOneEdit();
	afx_msg void OnEnSetfocusDrawerDenominationFiveEdit();
	afx_msg void OnEnSetfocusDrawerDenominationTenEdit();
	afx_msg void OnEnSetfocusDrawerDenominationTwentyEdit();
	afx_msg void OnEnSetfocusDrawerDenominationFiftyEdit();
	afx_msg void OnEnSetfocusDrawerDenominationHundredEdit();
	afx_msg void OnEnSetfocusDrawerDenominationSoftOtherEdit();
	afx_msg void OnEnSetfocusDrawerTotalAmountEdit();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);

// private functions
private:
    void ProcessButtonClick(BYTE button_press);

// private variables
private:
    DWORD current_edit_focus, total_amount, hard_penny, hard_nickel, hard_dime,
        hard_quarter, hard_half, hard_dollar, hard_other, soft_one, soft_five,
        soft_ten, soft_twenty, soft_fifty, soft_hundred, soft_other;
};

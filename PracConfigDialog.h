#pragma once
#include "afxwin.h"


// CPracConfigDialog dialog

class CPracConfigDialog : public CDialog
{
	DECLARE_DYNAMIC(CPracConfigDialog)

public:
	CPracConfigDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPracConfigDialog();

// Dialog Data
	enum { IDD = IDD_PRAC_CONFIG_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedPracConfigExitButton();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedPracConfigKeyboardButton();
	afx_msg void OnBnClickedIpAndPracPortSaveButton();
	afx_msg void OnBnClickedMachinePortToAddPracNextButton();

// public variables
public:
	CGameCommControl *pDisplayGameCommControl;

	CListBox m_prac_tx_listbox;
	CListBox m_prac_rx_listbox;
};

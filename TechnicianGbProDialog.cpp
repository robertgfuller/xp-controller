/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   05/04/09    Remove MDT code.
Dave Elquist    06/24/05    Initial Revision.
*/


// TechnicianGbProDialog.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include "TechnicianGbProDialog.h"
#include ".\techniciangbprodialog.h"


// CTechnicianGbProDialog dialog

IMPLEMENT_DYNAMIC(CTechnicianGbProDialog, CDialog)
CTechnicianGbProDialog::CTechnicianGbProDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CTechnicianGbProDialog::IDD, pParent)
{
}

CTechnicianGbProDialog::~CTechnicianGbProDialog()
{
}

void CTechnicianGbProDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TECHNICIAN_GB_PRO_LIST, m_technician_gb_pro_listbox);
}


BEGIN_MESSAGE_MAP(CTechnicianGbProDialog, CDialog)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_TECHNICIAN_GB_PRO_PRINTER_STATUS_BUTTON, OnBnClickedTechnicianGbProPrinterStatusButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_GB_PRO_PRINTER_RESET_BUTTON, OnBnClickedTechnicianGbProPrinterResetButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_GB_PRO_PRINTER_QUEUE_BUTTON, OnBnClickedTechnicianGbProPrinterQueueButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_GB_PRO_SERVER_STATUS_BUTTON, OnBnClickedTechnicianGbProServerStatusButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_GB_PRO_SERVER_QUEUE_BUTTON, OnBnClickedTechnicianGbProServerQueueButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_GB_PRO_EXIT_BUTTON, OnBnClickedTechnicianGbProExitButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_GB_PRO_TEST_MDT_PRINTER_BUTTON, &CTechnicianGbProDialog::OnBnClickedTechnicianGbProTestMdtPrinterButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_GB_PRO_MDT_SERVER_SAVE_BUTTON, &CTechnicianGbProDialog::OnBnClickedTechnicianGbProMdtServerSaveButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_GB_PRO_MDT_PRINTER_SAVE_BUTTON, &CTechnicianGbProDialog::OnBnClickedTechnicianGbProMdtPrinterSaveButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_GB_PRO_MDT_KEYBOARD_BUTTON, &CTechnicianGbProDialog::OnBnClickedTechnicianGbProMdtKeyboardButton)
END_MESSAGE_MAP()


// CTechnicianGbProDialog message handlers

void CTechnicianGbProDialog::OnMouseMove(UINT nFlags, CPoint point)
{
	theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

void CTechnicianGbProDialog::OnBnClickedTechnicianGbProPrinterStatusButton()
{
}

void CTechnicianGbProDialog::OnBnClickedTechnicianGbProPrinterResetButton()
{
}

void CTechnicianGbProDialog::OnBnClickedTechnicianGbProPrinterQueueButton()
{
}

void CTechnicianGbProDialog::OnBnClickedTechnicianGbProServerStatusButton()
{
}

void CTechnicianGbProDialog::OnBnClickedTechnicianGbProServerQueueButton()
{
}

void CTechnicianGbProDialog::OnBnClickedTechnicianGbProExitButton()
{
    DestroyWindow();
}

void CTechnicianGbProDialog::OnBnClickedTechnicianGbProTestMdtPrinterButton()
{
}

void CTechnicianGbProDialog::OnBnClickedTechnicianGbProMdtServerSaveButton()
{
}

void CTechnicianGbProDialog::OnBnClickedTechnicianGbProMdtPrinterSaveButton()
{
}

BOOL CTechnicianGbProDialog::OnInitDialog()
{
	return TRUE;  // return TRUE unless you set the focus to a control
}

void CTechnicianGbProDialog::OnBnClickedTechnicianGbProMdtKeyboardButton()
{
}

#pragma once
#include "afxwin.h"


// CCashVoucherDialog dialog

class CCashVoucherDialog : public CDialog
{
	DECLARE_DYNAMIC(CCashVoucherDialog)

public:
	CCashVoucherDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCashVoucherDialog();
	virtual BOOL OnInitDialog();

	afx_msg void OnBnClickedCashVoucherExitButton();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedCashVoucherPrinterResetButton();
	afx_msg void OnBnClickedCashVoucherPrinterStatusButton();
	afx_msg void OnBnClickedCashVoucherPrinterQueueButton();
	afx_msg void OnBnClickedCashVoucherTestPrinterButton();
	afx_msg void OnBnClickedCashVoucherPrinterKeyboardButton();
	afx_msg void OnBnClickedCashVoucherPrinterSaveButton();

// Dialog Data
	enum { IDD = IDD_CASH_VOUCHER_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:
	CListBox m_cash_voucher_printer_listbox;
	afx_msg void OnBnClickedCashVoucherDeletePendingTicketsButton();
};

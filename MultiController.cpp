/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    05/11/05    Initial Revision.
*/

// MultiController.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include "MultiController.h"


IMPLEMENT_DYNCREATE(CMultiControllerServer, CAsyncSocket)
IMPLEMENT_DYNCREATE(CMultiControllerClient, CMyAsyncSocket)


// CMultiControllerClient
CMultiControllerClient::CMultiControllerClient(CMultiControllerServer *pSocket /* = NULL */)
{
    delete_socket = false;
    pServerSocket = pSocket;

    memset(&rx_buffer, 0, sizeof(rx_buffer));
}

CMultiControllerClient::~CMultiControllerClient()
{
}

void CMultiControllerClient::OnClose(int nErrorCode)
{
 int iii;
 CString message, client_address;
 UINT client_port;

    for (iii=0; iii < MAXIMUM_MULTI_CONTROLLER_CONNECTIONS; iii++)
    {
        if (pServerSocket && pServerSocket->pClientSocket[iii] && pServerSocket->pClientSocket[iii]->m_hSocket == m_hSocket)
        {
            delete_socket = true;

            if (GetPeerName(client_address, client_port))
            {
                message = "CMultiControllerClient::OnClose - address ";
                message += client_address;
                logMessage(message);
            }
            else
            {
                message.Format("CMultiControllerClient::OnClose - error getting socket address, error %d.", GetLastError());
                logMessage(message);
            }
        }
    }

	CMyAsyncSocket::OnClose(nErrorCode);
}


// CMultiControllerServer
CMultiControllerServer::CMultiControllerServer()
{
 int iii;

    for (iii=0; iii < MAXIMUM_MULTI_CONTROLLER_CONNECTIONS; iii++)
        pClientSocket[iii] = NULL;
}

CMultiControllerServer::~CMultiControllerServer()
{
 int iii;

    for (iii=0; iii < MAXIMUM_MULTI_CONTROLLER_CONNECTIONS; iii++)
    {
        if (pClientSocket[iii])
        {
            if (pClientSocket[iii]->m_hSocket != INVALID_SOCKET)
                pClientSocket[iii]->Close();

			Sleep(1000);
            delete pClientSocket[iii];
        }
    }
}

void CMultiControllerServer::OnAccept(int nErrorCode)
{
 bool client_found;
 int iii, file_index;
 UINT client_port;
 CString message, client_address, compare_address;
 MasterSlaveConfig file_record;
 CMultiControllerClient *pNewSocket = new CMultiControllerClient(this);

    if (!Accept(*pNewSocket))
    {
        message.Format("CMultiControllerServer::OnAccept - error %d.", GetLastError());
        logMessage(message);
    }
    else
    {
        // get ip address
        if (pNewSocket->GetPeerName(client_address, client_port))
            logMessage("CMultiControllerServer::OnAccept - GetPeerName: " + client_address);
        else
        {
            message.Format("CMultiControllerServer::OnAccept - GetPeerName error: %d.", GetLastError());
            logMessage(message);
            client_address = "0.0.0.0";
        }

        for (iii=0; iii < MAXIMUM_MULTI_CONTROLLER_CONNECTIONS; iii++)
        {
            if (!pClientSocket[iii])
            {
                pClientSocket[iii] = pNewSocket;
                pNewSocket = NULL;
                break;
            }
        }

        if (pNewSocket)
            logMessage("CMultiControllerServer::OnAccept - maximum connnections exceeded: " + client_address);
        else
            logMessage("CMultiControllerServer::OnAccept - initial connection: " + client_address);
    }

    // verify ip address
    if (!pNewSocket)
    {
        client_found = false;
        file_index = 0;

        while (fileRead(MASTER_CONFIG, file_index, &file_record, sizeof(file_record)))
        {
            BYTE *ip_address = (BYTE*)&file_record.ip_address;
            compare_address.Format("%u.%u.%u.%u", ip_address[0], ip_address[1], ip_address[2], ip_address[3]);

            if (compare_address == client_address)
            {
                logMessage("CMultiControllerServer::OnAccept - connection verified: " + client_address);
                client_found = true;
                break;
            }
            else
                file_index++;
        }

        if (!client_found)
        {
            if (pClientSocket[iii]->m_hSocket != INVALID_SOCKET)
                pClientSocket[iii]->Close();

            delete pClientSocket[iii];
            pClientSocket[iii] = NULL;
        }
    }
    else
    {
        if (pNewSocket->m_hSocket != INVALID_SOCKET)
            pNewSocket->Close();

        delete pNewSocket;
    }

    CAsyncSocket::OnAccept(nErrorCode);
}

void CMultiControllerServer::OnClose(int nErrorCode)
{
 int iii;

    for (iii=0; iii < MAXIMUM_MULTI_CONTROLLER_CONNECTIONS; iii++)
    {
        if (pClientSocket[iii])
        {
            if (pClientSocket[iii]->m_hSocket != INVALID_SOCKET)
                pClientSocket[iii]->Close();
            delete pClientSocket[iii];
            pClientSocket[iii] = NULL;
        }
    }

	CAsyncSocket::OnClose(nErrorCode);
}

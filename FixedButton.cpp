#include "stdafx.h"
#include "FixedButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CFixedButton::CFixedButton( void )
{
	m_fSent = FALSE;
}

CFixedButton::~CFixedButton( void )
{
}

BEGIN_MESSAGE_MAP(CFixedButton, CButton)
	//{{AFX_MSG_MAP(CFixedButton)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

LRESULT CFixedButton::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	switch( message )
	{
		case WM_LBUTTONUP:
		{
			if( TRUE == m_fSent )
			{
				this->SendMessage( BM_SETSTATE, 0 );
				m_fSent = FALSE;

				CWnd* pParent = GetParent();
				if( NULL != pParent )
				{
					::SendMessage( pParent->m_hWnd, WM_COMMAND, (BN_CLICKED<<16) | this->GetDlgCtrlID(), (LPARAM)this->m_hWnd );
				}
			}

			break;
		}
		case WM_LBUTTONDBLCLK:
		{
			this->SendMessage( BM_SETSTATE, 1 );
			m_fSent = TRUE;

			return 0;
		}
	}

	return CBitmapButton::DefWindowProc( message, wParam, lParam );
}

/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   12/08/09    Once a day we purge the "dated paid ticket" file by removing all tickets in the file that over 14 days old.
Robert Fuller   06/09/09    Added code for custom picture buttons.
Robert Fuller   04/08/09    Change shift paradigm to only allow a single shift per login id to be active at a time.
Dave Elquist    07/15/05    Changed resource id tag.
Dave Elquist    04/14/05    Initial Revision.
*/


// xp_controllerDlg.h : header file
//


#pragma once
#include "afxcmn.h"

#include "FixedButton.h"


// CControllerDlg dialog
class CControllerDlg : public CDialog
{
// Construction
public:
	CControllerDlg(CWnd* pParent = NULL);	// standard constructor
    ~CControllerDlg();

// Dialog Data
	enum { IDD = IDD_LOGON_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedOneLogonButton();
	afx_msg void OnBnClickedTwoLogonButton();
	afx_msg void OnBnClickedThreeLogonButton();
	afx_msg void OnBnClickedFourLogonButton();
	afx_msg void OnBnClickedFiveLogonButton();
	afx_msg void OnBnClickedSixLogonButton();
	afx_msg void OnBnClickedSevenLogonButton();
	afx_msg void OnBnClickedEightLogonButton();
	afx_msg void OnBnClickedNineLogonButton();
	afx_msg void OnBnClickedZeroLogonButton();
	afx_msg void OnBnClickedClearLogonButton();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);

// private class variables
private:
    BYTE login_keypad_counter;
	CFixedButton keypad0;
	CFixedButton keypad1;
	CFixedButton keypad2;
	CFixedButton keypad3;
	CFixedButton keypad4;
	CFixedButton keypad5;
	CFixedButton keypad6;
	CFixedButton keypad7;
	CFixedButton keypad8;
	CFixedButton keypad9;
	CFixedButton keypadC;

    CRect client_rect;

// private class functions
private:
    void ProcessKeypadInput(char *number);
};

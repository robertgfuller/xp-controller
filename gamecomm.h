/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   11/25/13    Added a transaction id to the GBA win congrats message so that gaming device can determine a duplicate message.
Robert Fuller   11/25/13    Added a transaction id to the winning hand message so we can identify duplicate winning hand messages.
Robert Fuller   11/25/13    Added a transaction id to the clear card message so we can identify duplicate clear card messages.
Robert Fuller   11/20/13    Added code to send IDC the 4ok bingo winning card message. 
Robert Fuller   11/20/13    Added code to send IDC the bill denomination when a bill is accepted in the gaming machine. 
Robert Fuller   11/20/13    Added code to send IDC the value of the credit meter when it is fetched in real time via the SAS current credits message. 
Robert Fuller   11/20/13    Added a transaction id in the redemption complete message to allow gaming devices to identify duplicate messages.
Robert Fuller   11/20/13    Added GB point redemption transactions to the CENTS WON meter for direct connect games, so that the session Dollar Out meter values are accurate and consistent amongst platforms
Robert Fuller	07/02/13	Added a communication interface to the Station's Casino Casino Management System (CMS).
Robert Fuller	05/10/13	Added support for the new game entered message processing.
Robert Fuller   03/08/13    Added code to support the configurable dispenser payout limit..
Robert Fuller   03/08/13    Eliminated the individual Game Control processing threads, and called new function from OnIdle process all rx/tx messages.
Robert Fuller   03/08/13    Added new logout message with more enhanced acking message.
Robert Fuller	02/04/13	Keep track of iGBA transaction ids in a linked list instead of fixed array. Only log iGBA win transactions, not GBA win transactions.
Robert Fuller   09/17/12    Send stacker pull and game entered messages to the IDC.
Robert Fuller   09/17/12    Added iGBA transaction id to differentiate iGBA wins amongst multiple games.
Robert Fuller   09/17/12    Make sure we are ignoring GB Interface devices when traversing through the game device linked list.
Robert Fuller   05/31/12    Added code to check duplicate 4ok bingo win messages sent from the games.
Robert Fuller   05/31/12    Added code to support the new Quick Enrollment feature.
Robert Fuller   05/31/12    Added code to communicate with the Interactive Display Controller.
Robert Fuller   03/05/12    Added message so GBI could Request SAS Com Status every couple minutes if the sas communicating flag is not set.
Robert Fuller   03/05/12    Fixed the session point value in the Redeem Points Complete Message. 
Robert Fuller   03/05/12    Added View Cards message to send necessary data to the GB Interface.
Robert Fuller   03/05/12    Added new GB message to validate a member id selected during the enrollment process.
Robert Fuller   03/05/12    Send the new primary and secondary 4ok bingo awards in the winning card message.
Robert Fuller   01/27/12    Send an update GB Interface data message when there is a login on a GB Interface game.
Robert Fuller   01/27/12    Added code to send the GB Initialization data to the GB Interface after a 4ok winning hand event.
Robert Fuller   01/27/12    Do NOT put GB point redemption money on the game if there is an error in the redemption complete message.
Robert Fuller   01/27/12    Added code to send the GB Initialization data to the GB Interface after a 4ok winning hand event.
Robert Fuller   08/03/11    Removed unnecessary data fields in the update GB Interface message.
Robert Fuller   08/03/11    Added code to support Location Collection features.
Robert Fuller   08/03/11    Deprioritized GB Interface update messages.
Robert Fuller   06/08/11    Added mods to support GB with the XP/SAS direct connect feature.
Robert Fuller   04/26/11    Added code to log SAS communication events.
Robert Fuller   10/07/10    Added code to recover from a USB to RS232 disconnect.
Robert Fuller   08/09/10    Added mods to support the XP/SAS direct connect feature.
Robert Fuller   04/01/10    Added code necessary to abort a marker advance if the game never receives the advance messages from the system.
Robert Fuller   09/03/09    Added code to relay game configuration data for enabled games to the Stratus.
Robert Fuller   04/08/09    Save game meter snapshot in the force processors.
Robert Fuller	04/07/09	Changed structure name.
Robert Fuller   01/22/09    Added support for the new card data info message.
Robert Fuller   11/13/08    Compare current hand data with previous hand data message to filter out duplicates.
Robert Fuller   10/01/08    When a ticket add is received, see if the game lock file exists. If not create a gamelock internally.
Dave Elquist    07/15/05    Added Marker support.
Dave Elquist    04/14/05    Initial Revision.
*/


#ifndef GAMECOMM_H
#define GAMECOMM_H 1


#pragma once

#include "utility.h"
#include "MyAsyncSocket.h"

#ifndef SAS_COMM_H
#include "SAS_Comm.h"
#endif

#include "WinEvaluation.h"


// device status bits
#define GAME_MALFUNCTION                    0x0001  // ATTENDANT REQUEST
#define CHANGE                              0x0002  // CHANGE ON/CHANGE OFF
#define GAME_DOOR                           0x0004  // GAME DOOR OPEN/GAME DOOR CLOSED
#define DROP_DOOR                           0x0008  // DROP DOOR OPEN/DROP DOOR CLOSED
#define GAME_STATE                          0x0010  // GAME DEAD/GAME ALIVE
#define COLLECT_APPROVED      				0x0020
#define COLLECT_IN_PROG		   				0x0040	// collection in progress flag
#define PLAYER_LOGGED_IN                    0x0080  // player currently logged in game
#define ROM_CRC_ERROR						0x0100
#define HOPPER_LOW							0x0200
#define STACKER_REMOVED						0x0400
#define MALFUNCTIONING_SMART_CARD_READER    0x0800  // malfunctioning smart card reader

#define ABANDONED_SMART_CARD_IN_READER      0x1000  // abandoned card in smart card reader

// game meter data constant for MMI_TASK_GAME_METERS
#define MMI_UPDATE_GAME_METERS							1
#define MMI_UPDATE_GAME_METERS_WITH_PLAYER_LOGGED_IN	2
#define MMI_GAME_COLLECTED_METERS						9

// serial constants
#define MESSAGE_HEADER              0xFF
#define CONTROLLER_MUX_ADDRESS      0xFE
#define COMMAND_HEADER              '!'
#define POUND_HEADER                '#'
#define PERCENT_HEADER              '%'
#define AMPERSAND_HEADER            '&'
#define PAYLINE_PROGRESSIVE_HEADER  '@'
#define GB_ADVANTAGE_HEADER			'@'
#define ASTERISK_HEADER             '*' // encrypted message header
#define CASH_FRENZY_HEADER			'Z'
#define CARET_HEADER				'^' // XP to GB Interface communication
#define CMS_COMMAND_HEADER          '~'
#define GAME_RESPONSE_TIME          275

// commands used by both serial tx and rx
#define ACK_HEADER  'A'

// commands used by both serial tx and rx, preceded by COMMAND_HEADER
#define MAGNETIC_CARD_READER_STATISTICS 'E' // not used
#define WINNING_BINGO_CARD              'G'
#define WINNING_WIDE_AREA_AWARD         'L'
#define V__REDEEM_INQUIRY   			'V'
#define W__REDEEM_REQUEST   			'W'
#define w__REDEEM_REQUEST 				'w'
#define x__REDEEM_COMPLETE 				'x'

// commands used by both serial tx and rx, preceded by ASTERISK_HEADER
#define R__REQUEST_MARKER_ADVANCE   'R'
#define A__ABORT_MARKER_ADVANCE     'A'
#define T__MARKER_AFT_COMPLETE      'T'
#define U__UPDATE_MARKER_BALANCE    'U'

// tx serial commands
#define OFFLINE_POLL            '$'
#define MUX_IDLE                '?'
#define CLEAR_TICKET_FROM_GAME  'B'
#define REQUEST_DROP_METERS     'E'
#define START_COLLECTION        'O'
#define UNLOCK_GAME             'Q'
#define SDS_GAME_MESSAGE		'S'
#define DATE_TIME_NAME_ADDRESS  'T'
#define S__SET_DISPENSER_PAYOUT_LIMIT	'S'

// tx serial command preceded by PERCENT_HEADER
#define GB_PRO_ASCII_GAME_MSG       'P'
#define C__CASHABLE_CREDITS_WIN     'C'

// tx serial commands preceded by COMMAND_HEADER
#define RESET_MUX                   'B'
#define ACCOUNT_INQUIRY_RETURN      'C'
#define TRANSMIT_WIDE_AREA_AWARD    'H'
#define KIOSK_INQUIRY_RETURN		'K' // old Mountain View command, not used
#define DISPLAYED_MESSAGES          'M'
#define GAME_CONFIGURATION_DATA     'O'
#define REQUEST_AFTER_DROP_METERS   'T'

// tx serial commands preceded by ASTERISK_HEADER
#define NEW_ACCOUNT_INQUIRY_RETURN  'C'

// tx serial commands preceded by CASH_FRENZY_HEADER
#define TRANSMIT_CASH_FRENZY_AWARD	'H'
#define VERIFY_CASH_FRENZY_HIT		'L'

// rx serial commands
#define ACK_CLEAR_TICKET_FROM_GAME  'b'
#define GAME_DRINK_COMP             'd'
#define GAME_COLLECTED              'e'
#define f__GB_LOGOFF                'f'
#define GAME_LOCK_TICKET_ADD		'g'
#define GAME_HANDPAY                'h'
#define IDLE_MESSAGE                'i'
#define JACKPOT_MESSAGE             'j'
#define MACHINE_STATUS_MESSAGE      'k'
#define l__BINGO_WINNING_HAND		'l'
#define GAME_METERS1                'm'
#define n__NEW_CLEAR_BINGO_CARD_REQUEST	'n'
#define p__GBA_MSG			        'p'

#define GAME_LOCK_MESSAGE           'q'
#define DEVICE_RESET_MESSAGE        'r'
#define r__REQUEST_MARKER_ADVANCE_ACK   'r'
#define t__MARKER_AFT_COMPLETE_ACK  't'
#define TICKET_ADD                  't'
#define VALIDATE_MEMBER_ID			'v'
#define VALIDATE_QUICK_ENROLL_NUMBER	'V'
#define RETURN_SMI_METERS           'R'
#define GAMES_DENOM_METERS			'['
#define LUCKY_POKER_METERS			'{'

// rx serial commands preceded by COMMAND_HEADER
#define SMART_CARD_READER_STATE     'c'
#define ACCOUNT_INQUIRY             'C'
#define DENOM_METERS_OR_DOOR_EVENT  'D'
#define WINNING_BINGO_HAND			'F'
#define REQUEST_WIDE_AREA_AWARD		'H'
#define GAME_METERS2				'M'
#define SEND_CURRENT_METERS			'N'
#define CLEAR_BINGO_CARD_REQUEST	'Q'
#define IGT_GAME_MESSAGE			'R'
#define SDS_GAME_MESSAGE			'S'
#define ACCOUNT_LOGOUT				'U'

// rx serial commands preceded by POUND_HEADER
#define BILL_METERS                 'b'

// rx serial commands preceded by AMPERSAND_HEADER
#define JACKPOT_GAME_LOCK   'q'

// rx serial commands preceded by ASTERISK_HEADER
#define C__REQUEST_CUSTOMER_DATA    'C'
#define q__LOCKED_CASHOUT_EVENT     'q'

// rx serial commands preceded by CASH_FRENZY_HEADER
#define REQUEST_CASH_FRENZY_AWARD	'H'
#define RECEIVE_CASH_FRENZY_HIT		'L'

// rx serial command preceded by PERCENT_HEADER
#define CARD_DATA                   '2'
#define GAME_ENTERED                '3'
#define NEW_CARD_DATA               '4'
#define GAME_CONFIGURATION          '5'
#define NEW_GAME_ENTERED            '6'
#define BILL_ACCEPTED               'b'
#define c__CURRENT_CREDITS			'c'

// prac rx serial commands
#define GB_PRO_POINTS_TICKETS_CASH	'X'

// rx serial commands preceded by CARET_HEADER
#define INITIALIZE_GB_INTERFACE   'i'
#define UPDATE_GB_INTERFACE		  'u'
#define VIEW_CARDS_INFO_REQUEST	  'v'
#define BINGO_WINNING_HAND		  'F'
#define BINGO_WINNING_CARD		  'G'
#define SAS_COMMUNICATION_STATUS  'S'

// IDC specific messages
#define P__SET_PROMOTION_WIN_CATEGORY	'P'
#define W__GB_ADVANTAGE_WIN			   	'W'

#define F__FILL_VALUES 				   	'F'
#define D__DISPENSE_INFO				'D'

#define C__GAME_LOCK_COMPLETE			'C'
#define G__GAME_LOCK_RECEIVED			'G'


#define P__TICKET_PRINTED				'P'
#define R__TICKET_REDEEMED				'R'

#define b__BILL_ACCEPTED				'b'

#define d__MANUEL_DISPENSE_INFO			'd'

#define L__USER_LOGIN_LOGOFF			'L'

#define S__STACKER_PULL					'S'

#define M__GBA_MESSAGE					'M'

#define c__CURRENT_CREDITS				'c'

// CMS messages
#define L__LOGIN_STATUS_REQUEST			'L'
#define V__XPC_VERSION_AND_CONFIGURATION_INFO	'V'
#define S__XPC_CMS_CONNECTION_STATUS	'S'


#define V7000_VERSION				5		
#define GB_SAS_INTERFACE_VERSION	10		
#define GB_INTERFACE_VERSION		11		

#define MAX_INTERACTIVE_BONUS_TRANSACTION_IDS	10 

#pragma pack(1)
struct TxSerialMessage
{
    struct
    {
        BYTE header;
        BYTE mux_id;
        BYTE sequence;
        BYTE length;
        BYTE command;
        BYTE data[MAXIMUM_SERIAL_MESSAGE_SIZE - MINIMUM_SERIAL_MESSAGE_SIZE + 1];   // add 1 for crc
    } msg;

    int     retry;
    bool    response_pending;
    DWORD   response_time_timer;
};

struct RxSerialMessage
{
    BYTE header;
    BYTE mux_id;
    BYTE msg_id;
    BYTE length;
    BYTE command;
    BYTE data[MAXIMUM_SERIAL_MESSAGE_SIZE - MINIMUM_SERIAL_MESSAGE_SIZE + 1];   // add 1 for crc
};

struct GameLockData
{
	DWORD ticket_id;
	DWORD ticket_amount;
	BYTE hour;
	BYTE minute;
	BYTE second;
	BYTE day;
	BYTE month;
	BYTE year;
	BYTE mux_id;
	BYTE tax_flag;
};

struct RxPracSerialMessage
{
    BYTE header;
    BYTE msg_id;
    BYTE length;
    BYTE command;
    BYTE data[MAXIMUM_SERIAL_MESSAGE_SIZE - MINIMUM_SERIAL_MESSAGE_SIZE + 1];   // add 1 for crc
};

struct GameMeters
{
    DWORD coin_in;
    DWORD coin_out;
    DWORD games;
    DWORD coin_drop;
    DWORD hand_pay;
    DWORD gb_pts;
    DWORD total_dollars;
    DWORD jackpot;
    DWORD payline_progressive_games_played;
    DWORD payline_progressive_cents_played;
    DWORD payline_progressive_cents_won;
    DWORD wat_in;
    DWORD wat_out;
};

struct StoredGameMeters
{
    GameMeters  current_game_meters;
    GameMeters  last_drop_game_meters;
	BYTE mux_id;
};

struct StoredDropGameMeters
{
    char time_date_stamp[30];
    DWORD coin_in;
    DWORD coin_out;
    DWORD games;
    DWORD coin_drop;
    DWORD hand_pay;
    DWORD total_dollars;
    DWORD jackpot;
};

struct GbLoginData
{
    char time_date_stamp[30];
    char  name[16];
    WORD rating;
    char  tier_level;
};

struct UpdateGbInterfaceData
{
	ULONG whole_points;
//	ULONG primary_handle_to_date;
//	ULONG primary_games_to_date;
//	ULONG primary_card;
	ULONG primary_card_award;
//	ULONG secondary_handle_to_date;
//	ULONG secondary_games_to_date;
//	ULONG secondary_card;
	ULONG secondary_card_award;
//	ULONG session_whole_points;
//	ULONG session_cents_in;
//	ULONG session_cents_out;
//	ULONG session_drop_coins;
//	ULONG session_games;
//	ULONG session_timer;
//	ULONG current_game_denomination;
	ULONG current_bank_meter;
//	WORD fract_points;
//	WORD session_fract_points;
	WORD max_bet;
//	BYTE session_total_jackpots;
	BYTE logged_in_flag;
	BYTE door_state;
};

struct UpdateGbInterfaceWinningCardData
{
	ULONG primary_card_award;
	ULONG secondary_card_award;
};

struct BingoWinningHand
{
	ULONG win_code;
	ULONG handle;
	ULONG games;
	BYTE card_type;
};

struct WinningCardInfo
{
	ULONG member_id;
	ULONG win_amount;
	BYTE funding_type;
};

struct NewWinningHandMessage
{
	DWORD member_id;
	DWORD win_code;
	DWORD handle;
	DWORD games;
	BYTE  card_type;
	BYTE  reserved1;
	BYTE  reserved2;
	BYTE  reserved3;
	DWORD transaction_id;
};

struct ClearCardInfo
{
	ULONG member_id;
	ULONG transaction_id;
};

struct RedeemInquiry
{
	DWORD account_number;
    DWORD session_whole_points;
    WORD session_fractional_points;
};

struct RedeemInquiryResponse
{
	DWORD points_redeemed0;
	WORD redemption_cents0;
	DWORD points_redeemed1;
	WORD redemption_cents1;
	DWORD points_redeemed2;
	WORD redemption_cents2;
	BYTE redeem_error;
};


struct RedeemRequest
{
	DWORD account_number;
	BYTE error_code;
	BYTE choice;
    DWORD session_whole_points;
    WORD session_fractional_points;
	WORD reserved;
    DWORD transaction_id;
};

struct RedeemRequestResponse
{
    DWORD new_whole_points;
    WORD  new_fractional_points;
	BYTE  error_code;
	BYTE  reserved;
    DWORD transaction_id;
};

struct RedeemComplete
{
	DWORD account_number;
    DWORD transaction_id;
	BYTE error_code;
};

struct RedeemCompleteResponse
{
	DWORD account_number;
    DWORD transaction_id;
    DWORD whole_points_refund;
	BYTE  error_code;
};

struct AccountLogoutData
{
	DWORD account_number;
    DWORD session_whole_points;
    WORD session_fractional_points;
    DWORD session_coin_in;
    DWORD session_coin_out;
    DWORD session_coin_drop;
    DWORD session_games;
    DWORD session_seconds;
    DWORD primary_handle;
    DWORD primary_games;
    DWORD primary_card;
    BYTE session_jackpots;
    BYTE invalid_pin_tries;
    BYTE user_type;
    BYTE bingo_game_type;
    DWORD secondary_handle;
    DWORD secondary_games;
    DWORD secondary_card;
	BYTE time_stamp [14];
	BYTE reserved [2];
};

struct GbSessionUpdateData
{
    ULONG account_number;
    ULONG session_whole_points;
    USHORT session_fractional_points;
    ULONG session_coin_in;
    ULONG session_coin_out;
    ULONG session_coin_drop;
    ULONG session_games;
    ULONG session_seconds;
    ULONG primary_handle;
    ULONG primary_games;
    ULONG primary_card;
    BYTE session_jackpots;
    BYTE reserved1;
    BYTE reserved2;
    BYTE reserved3;
    ULONG secondary_handle;
    ULONG secondary_games;
    ULONG secondary_card;
};

struct GbSessionInfoData
{
    // session specific data
    ULONG session_whole_points;
    USHORT session_fractional_points;
    ULONG session_coin_in;
    ULONG session_coin_out;
    ULONG session_coin_drop;
    ULONG session_games;
    ULONG session_seconds;
    BYTE session_jackpots;
};

struct QuickEnrollFileStruct
{
	char time_date_stamp[14];
	char comma1;
	char player_number[8];
	char comma2;
	char status; // 'T' = taken, 'A' = available
};

#pragma pack()


// CGameDevice
class CGameDevice
{
// public class functions
public:
	CGameDevice(MemoryGameConfig *game_config);
	~CGameDevice();

    void SetGameCollectionState(BYTE state);
	void SaveGameMeterDropSnapshot (void);
    void WriteModifiedGameConfig();
    void ResetGame();
	void SetDispenserPayoutLimit();
	void initializeGbInterfaceData(GbInterfaceInitialize* gb_interface_initialize);
	void updateGbInterfaceData(UpdateGbInterfaceData* update_gb_interface_data);
	void updateSasCommunicationStatus(bool communicating);
	void updateGbInterfaceWithBingoWinningHandInfo(BingoWinningHand* bingo_winning_hand);
	void updateGbInterfaceWithBingoWinningCardInfo (UpdateGbInterfaceWinningCardData* winning_card_data);

	void addGbaInteractiveBonusTransactionId (DWORD transaction_id);
	bool removeGbaInteractiveBonusTransactionId (DWORD transaction_id);
	void removeGbaInteractiveBonusTransactionId (iGBALinkListWrapper* piGBALinkListWrapper);

// public class variables
public:
    CGameDevice         *pNextGame;
    MemoryGameConfig    memory_game_config;
    BYTE                collection_state_counter;
	StoredGameMeters	stored_game_meters;
	unsigned char  game_status;

    // idle message status bits
    // bit 1: GAME MALFUNCTION/MALFUNCTION RESET
    // bit 2: CHANGE ON/CHANGE OFF
    // bit 3: GAME DOOR OPEN/GAME DOOR CLOSED
    // bit 4: DROP DOOR OPEN/DROP DOOR CLOSED
    // bit 5: GAME DEAD/GAME ALIVE
    // bit 6: COLLECTION APPROVED (not used)
    // bit 7: COLLECTION IN PRGRESS
    // bit 8: PLAYER LOGGED IN
    // bit 9: ROM CRC ERROR
    // bit 10: HOPPER LOW (not used)
    // bit 11: STACKER REMOVED
    // bit 12: malfunctioning smart card reader
    // bit 13: abandoned card in smart card reader
    // bit 14: not used
    // bit 15: not used
    // bit 16: not used
    WORD idle_msg_status_bits;

    queue <TxSerialMessage, deque<TxSerialMessage> > transmit_queue;
    queue <TxSerialMessage, deque<TxSerialMessage> > low_priority_transmit_queue;

	HANDLE low_priority_queue_semiphore;


    // structure holding player account info
    AccountLogin account_login_data;

	CmsLoginMessage cms_account_login_data;

    // timer for account tier level printing
    time_t account_tier_wait_timer;

	// used to store the last processed hand data for poker games
    NewBlock blocks[MAX_BLOCKS_PER_MESSAGE];

	NewBlock last_winning_gba_block;

	RedeemRequest redeem_request;

    WinningCardInfo last_winning_card_info;
    time_t last_winning_card_time;

	iGBALinkListWrapper* pHeadiGBALinkListWrapper;

	// testing
    time_t tx_sent_time;

    GameMeters last_game_meters_sent_to_idc;
	bool meter_wobbled;
	bool all_zero_meter_read;

};


// CGameCommControl
class CGameCommControl
{
// public functions
public:
	CGameCommControl();
	CGameCommControl(GameCommPortInfo *pGameCommPortInfo, MemoryGameConfig *pMemoryGameConfig);
	~CGameCommControl();

    void OpenGameSerialPort();
    void OpenGameTcpIpPort();
	void CheckConnections();

    CGameDevice* GetGamePointer(BYTE mux_id);
	CGameDevice* GetGamePointer(BYTE mux_id, WORD port_number);
    CGameDevice* GetGamePointer(DWORD ucmc_id);

    // general message processing functions
    void TxPortData(BYTE *data, BYTE length);
    void TransmitGameMessage();
    void CheckGameResponse();
    bool ProcessValidSerialMessage(RxSerialMessage *rx_msg, WORD port_number);
    void ProcessNoGameResponse();
    void ProcessWaaMessage();
    void ProcessMpiMessage();
	void ProcessMpiMarkerMessage();
    void SendGamePollMessage();
	void ProcessWinningBingoCard (StratusMessageFormat* stratus_message, WORD length, CGameDevice *pGameDevice);
	void ProcessStratusRxValidateMemberId (StratusMessageFormat* stratus_message, WORD length, CGameDevice *pGameDevice);
	void ProcessRxTxGameMessages (void);

    // message command processing functions
    void ProcessRxAck();
	void GameLockMessage(CGameDevice *pGameDevice, FileGameLock* file_game_lock);
	void ProcessGameLockMessage(FileGameLock* file_game_lock, MemoryTicketAdd* ticket_add);
    void ProcessGameLockMessage(RxSerialMessage *rx_msg, WORD port_number);
    void ProcessMarkerAdvanceRequest(RxSerialMessage *rx_msg);
    void ProcessAbortMarkerAdvanceRequest(RxSerialMessage *rx_msg);
	void ProcessMarkerAftCompleteMessage(RxSerialMessage *rx_msg);
    void processGameDrinkComp(RxSerialMessage *rx_msg);
	void processGameDrinkComp(BYTE dollars);
	void GameDrinkComp(BYTE amount, DWORD ucmc_id);
    void GameCollected(RxSerialMessage *rx_msg);
    void ProcessJackpotMessage(RxSerialMessage *rx_msg);
	void ProcessJackpotMessage(long account, long jackpot_amount, long slip_number);
	void JackpotMessage(CGameDevice *pGameDevice, long account, long jackpot_amount, long slip_number);
    void StatusNotificationMessage(RxSerialMessage *rx_msg);
    void DeviceResetMessage(RxSerialMessage *rx_msg);
	void setGameVersion(BYTE version, BYTE subversion);
	void ProcessTicketAdd(RxSerialMessage *rx_msg, BYTE mux_id, MemoryTicketAdd *ticket_add, WORD port_number);
	void TicketAdd(CGameDevice *pGameDevice, MemoryTicketAdd *ticket_add);
	void ProcessValidateMemberIdMessage(RxSerialMessage *rx_msg);
	void ProcessValidateQuickEnrollNumberMessage(RxSerialMessage *rx_msg);
	void ValidateMemberId(CGameDevice *pGameDevice, DWORD member_id);
    void ProcessSmartCardReaderState(RxSerialMessage *rx_msg);
    void AccountInquiry(bool new_login_msg, RxSerialMessage *rx_msg);
    void WinningBingoHand(RxSerialMessage *rx_msg);
	void WinningBingoHand(BYTE sas_id, ULONG account, ULONG code, ULONG handle, ULONG games, BYTE card_type);
    void WinningBingoCard(RxSerialMessage *rx_msg);
    void WinningBingoCard(BYTE sas_id, ULONG account, ULONG amount);
    void ClearBingoCardRequest(RxSerialMessage *rx_msg);
	void ClearBingoCardRequest(CGameDevice *pGameDevice, ULONG account, BYTE sas_id);
    bool ProcessAccountLogout(RxSerialMessage *rx_msg);
    void AccountLogout(RxSerialMessage *rx_msg);
    void AccountLogout(ULONG jackpot_handpay);
	void GbSessionUpdate(void);
	void GbSessionInfoRequest(void);
	void GbAccountLoginInfoRequest(void);
    void redeemInquiry(RxSerialMessage *rx_msg);
    void redeemRequest(RxSerialMessage *rx_msg);
    void redeemComplete(RxSerialMessage *rx_msg);
	void redeemComplete(BYTE error_code);
    void WinningWideAreaAward(RxSerialMessage *rx_msg);
	void ProcessCashFrenzyHit(RxSerialMessage *rx_msg);
    bool IdleMessage(RxSerialMessage *rx_msg);
    void SetGameCollectionState(BYTE state);
	BYTE GetGameCollectionState(void);
	// GB Interface Processors
	void ProcessInitializeGbInterface(RxSerialMessage *rx_msg);
	void ProcessViewCardsInfoRequest(RxSerialMessage *rx_msg);
	void ProcessSasCommunicationsStatusRequest(RxSerialMessage *rx_msg);
	void process4okWin(CGameDevice *pGameDevice, ULONG account, ULONG code, WORD bet, WORD denomination);
	void saveDropMeterChange(GameMeters* game_meters, DWORD ucmc_id);
	void saveGbLoginChange(AccountLogin* account_login, DWORD ucmc_id);

    // Stratus interface functions
	bool processGameMeters(BYTE mux_id, BYTE *meter_data, BYTE meter_length, WORD update_flag);
	bool processGameMeters(WORD update_flag);


	// SAS communication interface functions
	void updateSasCommunicationStatus(bool communicating);
	void logSasRealTimeEventMessage (BYTE sas_event_type);
	void logSasHandpayPendingInfoMessage (HandpayInfoResponse* handpay_info_response);
	void logSasMeterDataMessage (MeterDataRequestResponse* meter_data_request_response);
	void logSasBillMeterDataMessage (BillMeterDataRequestResponse* bill_meter_data_request_response);
	void processHandData(unsigned char* hand, WORD bet, WORD denomination);
	unsigned char get4okWin (unsigned char* hand);
	void updateGbInterfaceData (void);
	void initializeGbInterfaceData (void);

	DWORD getDispenserPayoutLimit (void);
	bool payThroughDispenser (void);
	void SendIdcGameEnteredMessage (char* paytable_id, char* manufacturer_id);
	void SendIdcStackerPullMessage (BYTE pulled);

	// GBA Poker Hand Win Evaluation functions
	void ProcessGbProPointsTicketsCash(GbProPointsTicketsCash *rx_packet, CGameDevice *pGameDevice);
	void SendMachineAsciiMessage(CGameDevice *pGameDevice, DWORD points, DWORD amount, BYTE drawing_ticket);

	void processCurrentCreditsMeter (ULONG current_credits);
	void processBillAcceptedEvent (ULONG bill_denomination_in_cents);

private:
	void checkForCorrespondingGameLock(RxSerialMessage *rx_msg, BYTE mux_id, WORD port_number);

	void updateGbInterfaceWithBingoWinningHandInfo(ULONG win_code, ULONG handle, ULONG games, BYTE card_type);
	void updateGbInterfaceWithBingoWinningCardInfo (void);

	// GBA Poker Hand Win Evaluation functions
	void processGameNewCardData(RxSerialMessage *rx_msg, CGameDevice* pGameDevice);
	void processGameOldCardData(RxSerialMessage *rx_msg, CGameDevice* pGameDevice);
	DWORD processHandData(NewBlock* block, CGameDevice *pGameDevice);
	DWORD awardGbaWin (int win_category_index, CGameDevice *pGameDevice, NewBlock* block);
	bool duplicateCardsInHand (unsigned char* hand);


	CWinEvaluation win_evaluation;

// public variables
public:
    CGameDevice         *pHeadGameDevice;
    CGameCommControl    *pNextGameCommControl;
    CGameCommControl    *pPreviousGameCommControl;
    GameCommPortInfo    port_info;
    GameMessageErrors   message_errors;
	MemoryPracConfig	*pMemoryPracConfig;

	int game_port_check;
	int prac_port_check;
    time_t last_port_check_time;

	CMyAsyncSocket	*pGameSocket;
	HANDLE			hGameSerialPort;

	CMyAsyncSocket	*pPracSocket;
	HANDLE			hPracSerialPort;

    TxSerialMessage	current_tx_message;

    BYTE			game_message_sequence;
    CGameDevice		*pGameDeviceCurrentPollingGame;

	SerialDeviceReceive	game_receive;
	SerialDeviceReceive prac_receive;

	bool stop_comm_thread;

	CSAS_Comm* sas_com;

	bool sas_comm_thread_active;

	UINT connection_check_second_delay;

	int last_port_check;
	DWORD milli_second_time;


    queue <StratusMessageFormat, deque<StratusMessageFormat> > stratus_mpi_rx_queue;
    queue <StratusMessageFormat, deque<StratusMessageFormat> > stratus_mpi_rx_marker_queue;
    queue <StratusMessageFormat, deque<StratusMessageFormat> > stratus_waa_rx_queue;
    queue <TxSerialMessage, deque<TxSerialMessage> > broadcast_queue;

	friend CSAS_Comm;

};

#endif

/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    06/24/05    Initial Revision.
*/


#pragma once


// CCashierGbPro dialog

class CCashierGbPro : public CDialog
{
	DECLARE_DYNAMIC(CCashierGbPro)

public:
	CCashierGbPro(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCashierGbPro();

// Dialog Data
	enum { IDD = IDD_CASHIER_GB_PRO_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedCashierMdtPayButton();
	afx_msg void OnBnClickedCashierMdtCheckButton();
	afx_msg void OnBnClickedCashierMdtDrawResultsButton();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedCashierMdtExitButton();
	afx_msg void OnBnClickedCashierMdtButton1();
	afx_msg void OnBnClickedCashierMdtButton2();
	afx_msg void OnBnClickedCashierMdtButton3();
	afx_msg void OnBnClickedCashierMdtButton4();
	afx_msg void OnBnClickedCashierMdtButton5();
	afx_msg void OnBnClickedCashierMdtButton6();
	afx_msg void OnBnClickedCashierMdtButton7();
	afx_msg void OnBnClickedCashierMdtButton8();
	afx_msg void OnBnClickedCashierMdtButton9();
	afx_msg void OnBnClickedCashierMdtButton0();
	afx_msg void OnBnClickedCashierMdtClearButton();
	afx_msg void OnEnSetfocusCashierMdtDayEdit();
	afx_msg void OnEnSetfocusCashierMdtNumberEdit();

// private functions
private:
    void ProcessButtonClick(BYTE button_press);

// private variables
private:
    DWORD current_edit_focus;
};

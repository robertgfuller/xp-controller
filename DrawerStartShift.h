/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    07/15/05    Moved function.
Dave Elquist    06/14/05    Initial Revision.
*/


#pragma once


// CDrawerStartShift dialog

class CDrawerStartShift : public CDialog
{
	DECLARE_DYNAMIC(CDrawerStartShift)

public:
	CDrawerStartShift(int *drawer_balance, CWnd* pParent = NULL);   // standard constructor
	virtual ~CDrawerStartShift();

// Dialog Data
	enum { IDD = IDD_CASHIER_DRAWER_START_SHIFT_DENOM_COUNT_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedCashierDrawerBalanceButton1();
	afx_msg void OnBnClickedCashierDrawerBalanceButton2();
	afx_msg void OnBnClickedCashierDrawerBalanceButton3();
	afx_msg void OnBnClickedCashierDrawerBalanceButton4();
	afx_msg void OnBnClickedCashierDrawerBalanceButton5();
	afx_msg void OnBnClickedCashierDrawerBalanceButton6();
	afx_msg void OnBnClickedCashierDrawerBalanceButton7();
	afx_msg void OnBnClickedCashierDrawerBalanceButton8();
	afx_msg void OnBnClickedCashierDrawerBalanceButton9();
	afx_msg void OnBnClickedCashierDrawerBalanceButton0();
	afx_msg void OnBnClickedCashierDrawerBalanceClearButton();
	afx_msg void OnBnClickedCashierDrawerBalanceEnterButton();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);

// private functions
private:
    void ProcessButtonClick(BYTE button_press);

// private variables
private:
    int *beginning_balance, entered_balance;
};
